<?php

/**
 * 各种验证码类
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class identifying {
	
	public $type_array = array(
			'mobile',//手机验证码，发送短信
			'email' ,//邮箱验证码
			);
	public $type;
	public $to;
	public $len;
	public $uid;
	public $model;
	
	//验证时数据
	//最后一条数据
	public $last_fetch = null;
	public $timeOut = 18000;
	public $code = null;
	
	/**
	 * 构造函数
	 * @param string $type 验证码类型
	 * @param string $to   发送给什么 （手机号码 /邮箱）
	 * @param int    $len  验证码长度
	 */
	public function __construct($type,$to,$len,$uid = 0,$model = 'common_member_identifying'){
		$this->type = in_array($type,$this->type_array) ? $type : 'mobile';
		$this->to = $to;
		$this->len = $len;
		$this->uid = $uid;
		$this->model = C::t($model);
	}
	
	//检查手机格式
	public function checkFormat(){
		if($this->type == 'mobile'){
			return check_mobile_number($this->to);
		}else{
			return check_email_format($this->to);
		}
	}
	
	//检查是否已被注册
	//已经被注册：TRUE
	//未被注册：FALSE
	public function checkHaveOne(){
		$sql = sprintf("input_type = '{$this->type}' AND input_value = '%s' AND is_verify = 1", $this->to);
		if($this->model->count_by_where($sql)){
			return TRUE;
		}
		return FALSE;
	}
	
	/**
	 * 检查一段时间内能发多少条
	 * @param int $times 能发多少条
	 * @param int $arae 时间戳 例如1小时=3600
	 */
	public function checkTimesInArea($times = 3,$area = 3600){
		$check_onehour_times = $this->model->fetch_all_by_input_limit($this->to,0,$times);
		if(count($check_onehour_times) >= $settimes && TIMESTAMP < $check_onehour_times[count($check_onehour_times)-1]['addtime'] + $area ){
			return FALSE;
		}
		return TRUE;
	}
	
	/**
	 * 检查多少之内能发一次
	 * @param int $seconds 多少秒
	 */
	public function checkTimesInSecound($seconds){
		$mobileIdentifying = $this->model->fetch_by_mobile($this->to);
		//检查时间
		if ((TIMESTAMP < $mobileIdentifying["addtime"] + $seconds) && $mobileIdentifying['is_verify'] == 0){
			return FALSE;
		}
		return TRUE;
	}
	
	/**
	 * 发送验证码
	 */
	public function send(){
		//生成验证码
		$identifying_code = $this->create_code();
		//发信息
		if($this->type == 'mobile'){
			$msg_content = '您的手机验证码为：'.$identifying_code.'。如非本人操作，请及时反馈在线客服。';
			$sms = new SMS();
  			$res = $sms->sendMsg($msg_content, $this->to);
			if (strcasecmp($res->returnstatus, 'success') === 0){
			    //保存结果到数据库
			    if($this->model->insert(
						array(
							'uid'			=> $this->uid,
							"input_type"	=> 'mobile',
							"input_value"	=> $this->to,
							'code'			=> $identifying_code,
							'is_verify'		=> 0,
							"addtime"		=> TIMESTAMP,
						)))
				{
					return TRUE;
			    }else{
			    	return FALSE;
			    }
			}else{
				return FALSE;
			}
		}
		//发邮件
		else{

			$email_register_message = lang('email', 'app_email_register_message', array(
				'code' => $identifying_code,
			));
			
			require_once libfile('function/mail');
			if(sendmail("{$this->to} <{$this->to}>", lang('email', 'app_email_register_subject'), $email_register_message)) {
				if($this->model->insert(
						    array(
						    	'uid'			=> $this->uid,
								"input_type"	=> 'email',
								"input_value"	=> $this->to,
								'code'			=> $identifying_code,
								'is_verify'		=> 0,
								"addtime"		=> TIMESTAMP,
				))){
					return TRUE;
			    }else{
			    	return FALSE;
			    }
			}
			return FALSE;
			
		}
		
	}
	
	//验证时使用*****************************************
	
	
	public function getLastFetch(){
		if($this->last_fetch != null ){
			return $this->last_fetch;
		}
		return $this->last_fetch = $this->model->fetch_last($this->to,$this->uid);
	}
	
	//检查是否过时，默认5分钟 ( 60*5 )
	public function checkTimeOut($time = 300){
		$this->timeOut = $time;
		$res = $this->getLastFetch();
		if(!$res["addtime"] || TIMESTAMP > intval($res["addtime"]) + $time ){
			return FALSE;
		}
		return TRUE;
	}
	
	/**
	 * 检查是否匹配
	 * @param int $code 验证码
	 */
	public function checkMatch($code){
		$this->code = $code;
		$res = $this->getLastFetch();
		if($res['input_value']!=$this->to || $res['code']!=$code){
			return FALSE;
		}
		return TRUE;
	}
	
	/**
	 * 写入数据库,更改is_verify字段
	 */
	public function setSureVerify($uid=0){
		if(!$this->checkTimeOut($this->timeOut)) return FALSE;
		if(!$this->checkMatch($this->code)) return FALSE;
		$res = $this->getLastFetch();
		$updata_fild = array('is_verify'=>1,'uid'=>$uid ? $uid : $this->uid);
		return $this->model->update_identifying_by_id($res['id'],$updata_fild);
	}
	
	/**
	 * 更新discuz原有的手机字段
	 */
	public function updateDZmobile($uid=0){
		if($this->type == 'mobile'){
			$uid = $uid ? intval($uid) : $this->uid;
			return C::t('common_member_profile')->update_mobile_by_uid($uid,$this->to);
		}
		return FALSE;
	}
	
	public static function deleteIdentifyByUid($uid){
		if(is_array($uid)){
			foreach($uid as $k => $id){
				$uid[$k] = intval($id);
			}
		}else{
			$uid = intval($uid);
		}
		return C::t('common_member_identifying')->delete_by_uid($uid);
	}
	
	//返回数据库操作对象来查询
	public function getModel(){
		return $this->model;
	}
	
	private function create_code(){
		return rand(pow(10,($this->len)-1), pow(10,$this->len)-1);
	}
	
}
