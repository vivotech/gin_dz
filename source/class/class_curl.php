<?php

if(!defined('IN_DISCUZ') && !defined('IN_UC')) {
	exit('Access Denied');
}

class curl{
	private $options = array();
	private $url = '';
	private $_webpage = '';
	private $_status = '';
	private $ch;
	public function __construct($options){
		global $_G;
		$options_default = array(
			'type'=>'pc',
			'follow_location'=>true,
			'timeout'=>30,
			'max_redirecs'=>4,
			'cookie_file_location' => DISCUZ_ROOT.'data'.DS.'cookies'.DS.$_G['uid'].'.txt',
			'referer'=>'',
			'x_requested_with'=>''
		);
		foreach($options_default as $k=>$val){
			if(isset($options[$k])){
				$this->options[$k] = $options[$k];
			}else{
				$this->options[$k] = $val;
			}
		}
		
		if (strtolower($this->options['type']) == 'mobile'){
			$this->options['ua'] = 'Mozilla/5.0 (Linux; Android 4.2.2; GT-I9505 Build/JDQ39) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.59 Mobile Safari/537.36';
		}else{
			$this->options['ua'] = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.76 Safari/537.36';
		}
	}
	
	//默认使用首页保存COOKIES
	
	public function request_init($url, $type = 'get', $post_data = array()){
		$this->url = $url;
		$type = strtolower($type);
		$this->ch = curl_init(); 
		
		if($type == 'post'){
			$headers = array('Expect:', 'Content-Type: multipart/form-data');
			if(version_compare(PHP_VERSION,'5.6.0','>=')){
				curl_setopt ($this->ch, CURLOPT_SAFE_UPLOAD, false);
			}
		}else{
			$headers = array('Expect:');
		}

		curl_setopt($this->ch, CURLOPT_URL, $this->url);
		curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($this->ch, CURLOPT_TIMEOUT, $this->options['timeout']);
		curl_setopt($this->ch, CURLOPT_MAXREDIRS, $this->options['max_redirecs']);
		curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, $this->options['follow_location']); 
		curl_setopt($this->ch, CURLOPT_COOKIEJAR, $this->options['cookie_file_location']); 
		curl_setopt($this->ch, CURLOPT_COOKIEFILE, $this->options['cookie_file_location']); 
		
		if($type == 'post'){
			curl_setopt($this->ch , CURLOPT_POST , true);
			curl_setopt($this->ch , CURLOPT_POSTFIELDS , $post_data);
		} 
		
		curl_setopt($this->ch, CURLOPT_USERAGENT, $this->options['ua']); 
		curl_setopt($this->ch, CURLOPT_REFERER, $this->_referer);
	}

	public function request_action(){
		$this->_webpage = curl_exec($this->ch); 
		$this->_status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE); 

		curl_close($this->ch);
		return $this->_webpage;
	}

	public function set_referer($referer){
		$this->options['referer'] = $referer;
	}
	
	public function set_x_requested_with($x_requested_with){
		$this->options['x_requested_with'] = $x_requested_with;
	}
	
	public function set_httpheader($arr){
		curl_setopt($this->ch, CURLOPT_HTTPHEADER, $arr);
	}
}