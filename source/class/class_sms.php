<?php

/**
 * 短信类
 */
if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

Class SMS{
	private $userid = null;
	private $account = '';
	private $password = '';
	private $url = '';
  private $deadline = 300;
  public static $timeout = 60;
	
	public function __construct($userid = 0, $account = '', $password = '', $url = ''){
    
    $app = C::app();
    
		if($userid){
			$this->userid = $userid;
		} else {
      $this->userid = $app->config['sms']['SMS_USERID'];
    }
    
		if($account){
			$this->account = $account;
		} else {
      $this->account = $app->config['sms']['SMS_ACCOUNT'];
    }
    
		if($password){
			$this->password = $password;
		} else {
      $this->password = $app->config['sms']['SMS_PASSWORD'];
    }
    
		if($url){
			$this->url = $url;
		} else {
      $this->url = $app->config['sms']['SMS_URL'];
    }
    
	}
	
	/**
	 * @name 发送消息
	 * @param string $msg
	 * @param string $mobile
	 * @param string $sendtime 不定时发送，值为0，定时发送，输入格式YYYYMMDDHHmmss的日期值
	 * @return object
	 */
	public function sendMsg($msg, $mobile, $sendtime = ''){
		$post_data = $this->createData();
		$post_data['content'] = $msg;
		$post_data['mobile'] = $mobile;
		$post_data['sendtime'] = $sendtime;

		$obj = $this->action('send', $post_data);
		return $obj; 
	}
	
	/**
	 * @name 创建发送的数据对象
	 * @return array
	 */
	private function createData(){
		$post_data = array();
		$post_data['userid'] = $this->userid;
		$post_data['account'] = $this->account;
		$post_data['password'] = $this->password;
		
		return $post_data;
	}
	
	/**
	 * @name 发送到服务器
	 * @param array $post_data
	 * @return SimpleXMLElement
	 */
	private function action($action, $post_data){
		$url = $this->url.$action;
		$o = '';
		foreach ($post_data as $k=>$v)
		{
			$o.="$k=".urlencode($v).'&';
		}
		$post_data = substr($o,0,-1);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //如果需要将结果直接返回到变量里，那加上这句。
		
		$result = curl_exec($ch);
		$obj = $this->parseResult($result);
		
		return $obj;
	}
	
	/**
	 * @name 解析返回的结果
	 * @param string $result_str
	 * @return SimpleXMLElement
	 */
	private function parseResult($result_str = ''){
		$xmlobj = simplexml_load_string($result_str);
		return $xmlobj;
	}
  
  public function getTimeOut()
  {
    return $this->timeout;
  }
  
  
	
}