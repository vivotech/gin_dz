<?php

/**
 * app使用的model_forum_post
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class model_forum_post_app extends model_forum_post {
	
	/**
	 * 复写showmessage来调用sendAppMessage 返回 app 所需要的json
	 */
	
	public $app_showmessage = 'sendAppMessage';
	
	public function showmessage(){
		if(!empty($this->app_showmessage) && is_callable($this->app_showmessage)) {
			$p = func_get_args();
			if(is_string($this->app_showmessage)) {
				$fn = $this->app_showmessage;
				switch (func_num_args()) {
					case 1:	return $fn(RES_NO,$p[0]); break;
					case 2:	return $fn(RES_NO,$p[0], is_array($p[1])?$p[1]:array($p[1]));break;
					case 3:	return $fn(RES_NO,array($p[0],$p[2]),$p[1]);break;
					case 4:	return $fn(RES_NO,array($p[0],$p[2]), array_merge(is_array($p[1])?$p[1]:array($p[1]),is_array($p[3])?$p[3]:array($p[3]),is_array($p[3])?$p[3]:array($p[3])));break;
				}
			}
		}
	}


}
?>