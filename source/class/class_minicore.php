<?php

define('IN_DISCUZ', true);
define('DISCUZ_ROOT', substr(dirname(__FILE__), 0, -12));

if(function_exists('spl_autoload_register')) {
	spl_autoload_register(array('minicore', 'autoload'));
} else {
	function __autoload($class) {
		return minicore::autoload($class);
	}
}

global $_G;
minicore::createCore();

class minicore{
	
	var $config = array();
	var $var = array();
	
	public static function createCore(){
		static $object;
		if(empty($object)) {
			$object = new self();
		}
		return $object;
	}
	
	public function __construct(){
		$this->init();
	}
	
	public function init(){
		$this->_init_env();
		$this->_init_config();
		$this->_init_setting();
		$this->_init_output();
	}
	
	private function _init_env(){
		global $_G;
		error_reporting(E_ERROR);
		if(PHP_VERSION < '5.3.0') {
			set_magic_quotes_runtime(0);
		}

		define('MAGIC_QUOTES_GPC', function_exists('get_magic_quotes_gpc') && get_magic_quotes_gpc());
		define('ICONV_ENABLE', function_exists('iconv'));
		define('MB_ENABLE', function_exists('mb_convert_encoding'));
		define('EXT_OBGZIP', function_exists('ob_gzhandler'));
		define('DS', DIRECTORY_SEPARATOR);
		
		define('TIMESTAMP', time());

		global $_G;
		$_G = array(
			'config' => array(),
			'setting' => array(),
			'cache'=> array(),
		);
		
		$_G['isHTTPS'] = ($_SERVER['HTTPS'] && strtolower($_SERVER['HTTPS']) != 'off') ? true : false;
		$_G['siteurl'] = htmlspecialchars('http'.($_G['isHTTPS'] ? 's' : '').'://'.$_SERVER['HTTP_HOST'].$sitepath.'/');
		
		$this->var = & $_G;
	}
	
	private function _init_config(){
		$_config = array();
		include DISCUZ_ROOT.'./config/config_global.php';
		$this->config = & $_config;
		$this->var['config'] = & $_config;
	}
	
	private function _init_setting(){
		loadcache('setting');
		//配置文件
		$this->var['setting'] = unserialize($this->var['setting']);
		//微信配置
		$this->var['wechatconfig'] = $wechatconfig = unserialize($this->var['setting']['wechatconfig']);
		//微支付配置
		$this->var['wechatpay'] = $wechatpay = unserialize($this->var['setting']['wechatpay']);
		//证书实际地址
		$this->var['wechatpay']['SSLCERT_PATH'] = DISCUZ_ROOT . $this->var['wechatpay']['SSLCERT_PATH'];
		$this->var['wechatpay']['SSLKEY_PATH'] = DISCUZ_ROOT . $this->var['wechatpay']['SSLKEY_PATH'];
		//读取默认公众号
		$this->var['wechat'] = $defaultWechat = unserialize($this->var['setting']['wechat']);
	}
	
	private function _init_output() {
		define('CHARSET', $this->config['output']['charset']);
		if($this->config['output']['forceheader']) {
			@header('Content-Type: text/html; charset='.CHARSET);
		}
	}
	
	public static function autoload($class) {
		$class = strtolower($class);
		if(strpos($class, '_') !== false) {
			list($folder) = explode('_', $class);
			$file = 'class/'.$folder.'/'.$class;
		} else {
			$file = 'class/'.$class;
		}
		$path = DISCUZ_ROOT.'source/'.$file.'.php';
		if(is_file($path) && file_exists($path)){
			@include_once $path;
		}
	}
	
}



//方法
function M($name){
	global $_G;
	static $obj = array();
	if(empty($obj[$name])){
		$db = new custom_db();
		$obj[$name] = new custom_db_model($name , $db);
	}
	return $obj[$name];
}

function savecache($cachename, $data) {
	$save_data = array(
			'cname' => $cachename,
			'ctype' => is_array($data) ? 1 : 0,
			'dateline' => TIMESTAMP,
			'data' => is_array($data) ? serialize($data) : $data,
		);
	M('common_syscache')->add($save_data,array(),TRUE);
}

function loadcache($cachenames, $force = false) {
	global $_G;
	static $loadedcache = array();
	$cachenames = is_array($cachenames) ? $cachenames : array($cachenames);
	$caches = array();
	foreach ($cachenames as $k) {
		if(!isset($loadedcache[$k]) || $force) {
			$caches[] = $k;
			$loadedcache[$k] = true;
		}
	}

	if(!empty($caches)) {
		$where['cname'] = array('IN',$caches);
		$cachedata = M('common_syscache')->where($where)->select();
		foreach($cachedata as $value) {
			$cname = $value['cname'];
			$data = $value['data'];
			if($cname == 'setting') {
				$_G['setting'] = $data;
			} elseif($cname == 'usergroup_'.$_G['groupid']) {
				$_G['cache'][$cname] = $_G['group'] = $data;
			} elseif($cname == 'style_default') {
				$_G['cache'][$cname] = $_G['style'] = $data;
			} elseif($cname == 'grouplevels') {
				$_G['grouplevels'] = $data;
			} else {
				$_G['cache'][$cname] = $data;
			}
		}
	}
	return true;
}

function libfile($libname, $folder = '') {
	$libpath = '/source/'.$folder;
	if(strstr($libname, '/')) {
		list($pre, $name) = explode('/', $libname);
		$path = "{$libpath}/{$pre}/{$pre}_{$name}";
	} else {
		$path = "{$libpath}/{$libname}";
	}
	return preg_match('/^[\w\d\/_]+$/i', $path) ? realpath(DISCUZ_ROOT.$path.'.php') : false;
}

function I($name,$defalut='',$filter='',$inarray=array()){
	$res = '';
	if(!empty($defalut)){
		$res = !isset($_REQUEST[$name]) ? $defalut : $_REQUEST[$name];
	}
	if(!empty($filter) && function_exists($filter)){
		$res = $filter($res);
	}else{
		$res = htmlspecialchars($res);
	}
	if(!empty($inarray) && is_array($inarray)){
		$res = in_array($res, $inarray) ? $res : $defalut;
	}
	return $res;
}


/**
 * URL拼凑
 */
    function U($entance_mod_action,$param=array(),$full_url=FALSE){
        global $_G;
        if(empty($entance_mod_action)){
		return '/';
	}
	list($php,$mod,$action) = explode('/', $entance_mod_action);
	//分析param
	$url_param = '';
	if(is_array($param)){
		foreach($param as $k=>$v){
			$url_param .= '&'.$k.'='.$v;
		}
	}elseif(is_string($param)){
		if(substr($param,0,1)!='&'){
			$url_param .= '&';
		}
		$url_param .= $param;
	}
	
	$type=1;
	//正常URL
	if($type=1){
		return ($full_url?$_G['siteurl']:'/').$php.'.php'.(!empty($mod)?'?mod='.$mod:'').(!empty($mod) && !empty($action)?('&action='.$action):'').$url_param;
	}
}

/**
 * 调试专用 linux下生成文件
 */
function vivolog($filename,$data){
	file_put_contents(DISCUZ_ROOT.'data'.DS.$filename.'.log', date('Y-m-d H:i',time()).'：'.$data."\r\n",FILE_APPEND);
}
