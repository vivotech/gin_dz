<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: discuz_table.php 30321 2012-05-22 09:09:35Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}


class discuz_submeter_table extends discuz_table
{

	//分表配置
	public $table_num = 5; //默认分5表
	public $_origin_table;//分表名称
	public $custom_db = null;
	
	public function __construct($para = array()) {
		if(!empty($para)) {
			$this->_table = $para['table'];
			$this->_pk = $para['pk'];
		}
		if(isset($this->_pre_cache_key) && (($ttl = getglobal('setting/memory/'.$this->_table)) !== null || ($ttl = $this->_cache_ttl) !== null) && memory('check')) {
			$this->_cache_ttl = $ttl;		//各缓存过期时间
			$this->_allowmem = true;
		}
		$this->set_hash_name();
		$this->custom_db = new custom_db();		//不使用discuz的DB类，discuz DB类含安全限制
		$this->_init_extend();
		parent::__construct();
	}
	
	//求余分表
	public function set_hash_name($uid=0){
		global $_G;
		$uid = $uid==0 ? $_G['uid'] : $uid;
		$this->_table = $this->_origin_table . '_' .( $uid % $this->table_num );
	}
	
}

?>