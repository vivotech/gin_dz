<?php

/**
 * 	Thinkphp数据库操作类
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class custom_db{
	var $link_id = null;
	var $settings = array();
	var $quiet = 0;
	var $version = '';
	var $queryCount = 0;
    var $queryTime  = '';
	var $error = '';
	var $pre = '';
	var $pres = '';
	
	function __construct($db = '1'){
		global $_G;
		
		$cfg = $_G['config']['db'][$db];
		if(empty($cfg)) {
			exit("没有找到名为数据库配置项.");
		}
		
		$charset = strtolower(str_replace('-', '', $cfg['dbcharset'] ? $cfg['dbcharset'] : 'utf8' ));
		
		$this->pre = $cfg['tablepre'];
		$this->dbname = $cfg['dbname'];
		$this->pres = '`'.$this->dbname.'`.`'.$this->pre;
		
		$this->settings = array(
            'dbhost'   => $cfg['dbhost'] == 'localhost' ? '127.0.0.1' : $cfg['dbhost'],
            'dbuser'   => $cfg['dbuser'],
            'dbpw'     => $cfg['dbpw'],
            'dbname'   => $cfg['dbname'],
            'charset'  => $charset,
            'pconnect' => $cfg['pconnect']
        );
	}
	
	function T( $name ){
		return $this->pres .$name .'`';
	}

	function getError(){
		return $this->error;
	}

	function connect(){
		if ($this->settings['pconnect'])
        {
           $this->link_id = @mysql_pconnect($this->settings['dbhost'], $this->settings['dbuser'], $this->settings['dbpw']);
        }
        else
        {
            if (PHP_VERSION >= '4.2')
            {
                $this->link_id = @mysql_connect($this->settings['dbhost'], $this->settings['dbuser'], $this->settings['dbpw'], true);
            }
            else
            {
                $this->link_id = @mysql_connect($this->settings['dbhost'], $this->settings['dbuser'], $this->settings['dbpw']);
            }
        }
        
		if (!$this->link_id)
		{
			$this->error = "Can't Connect MySQL Server($dbhost)!";
			return false;
		}
		
		define('MYSQL_VERSION' , mysql_get_server_info($this->link_id));
		$this->version = MYSQL_VERSION;
		$this->set_mysql_charset($this->settings['charset']);
		
		$this->starttime = time();
		if ($this->settings['dbname']){
			return $this->select_database($this->settings['dbname']);
		}
		return true;

    }

	/**
     +----------------------------------------------------------
     * 取得数据表的字段信息
     +----------------------------------------------------------
     * @access public
     +----------------------------------------------------------
     */
    public function getFields($tableName) {
        $result =   $this->getAll('SHOW COLUMNS FROM '.$tableName);
        $info   =   array();
        if($result) {
            foreach ($result as $key => $val) {
                $info[$val['Field']] = array(
                    'name'    => $val['Field'],
                    'type'    => $val['Type'],
                    'notnull' => (bool) ($val['Null'] === ''), // not null is empty, null is yes
                    'default' => $val['Default'],
                    'primary' => (strtolower($val['Key']) == 'pri'),
                    'autoinc' => (strtolower($val['Extra']) == 'auto_increment'),
                );
            }
        }
        return $info;
    }
	
	function select_database($dbname)
    {
    	if (mysql_select_db($dbname, $this->link_id) === false )
        {
            $this->error = "Can't select MySQL database($dbname)!";
            return false;
        }
        return true;
    }
    
	function getQueryCount(){return $this->queryCount;}
	
	function set_mysql_charset($charset)
    {
        /* 如果mysql 版本是 4.1+ 以上，需要对字符集进行初始化 */
        if (MYSQL_VERSION > '4.1')
        {
            if (in_array(strtolower($charset), array('gbk', 'big5', 'utf-8', 'utf8')))
            {
                $charset = str_replace('-', '', $charset);
            }
            if ($charset != 'latin1')
            {
                mysql_query("SET character_set_connection=$charset, character_set_results=$charset, character_set_client=binary", $this->link_id);
            }
        }
    }
    
    function fetch_array($query, $result_type = MYSQL_ASSOC){  return mysql_fetch_array($query, $result_type); }
	
    function query($sql, $type = ''){
		if ($this->link_id === NULL){
			$this->connect();
		}
		
		$this->queryCount++;
		
		if ($this->queryTime == '')
        {
            if (PHP_VERSION >= '5.0.0')
            {
                $this->queryTime = microtime(true);
            }
            else
            {
                $this->queryTime = microtime();
            }
        }
		
		if (PHP_VERSION >= '4.3' && ($t = time()) > $this->starttime + 3)
        {
            mysql_ping($this->link_id);
            $this->starttime = $t;
        }
        
		
		if (!($query = mysql_query($sql, $this->link_id)) && $type != 'SILENT')
        {
			$str = "Message: MySQL Query Error<br />".
					"SQL:".$sql."<br />".
					'Error: '.mysql_error($this->link_id)."<br />".
					'Errno: '.mysql_errno($this->link_id);
			$this->error = $str;
            return false;
        }
		return $query;
	}
	
	function error(){
		return mysql_error($this->link_id);
	}
	
	function errno(){
		return mysql_errno($this->link_id);
	}
	
	function affected_rows()
    {
        return mysql_affected_rows($this->link_id);
    }
    
	function result($query, $row)
    {
        return @mysql_result($query, $row);
    }
    
	function num_rows($query)
    {
        return mysql_num_rows($query);
    }
    
	function insert_id()
    {
        return mysql_insert_id($this->link_id);
    }
    
	function fetchRow($query)
    {
        return mysql_fetch_assoc($query);
    }
    
	function fetch_fields($query)
    {
        return mysql_fetch_field($query);
    }
    
	function version()
    {
        return $this->version;
    }
    
	function close()
    {
        return mysql_close($this->link_id);
    }
    
    
	function getOne($sql, $limited = false)
    {
        if ($limited == true)
        {
            $sql = trim($sql . ' LIMIT 1');
        }
		
        $res = $this->query($sql);
        if ($res !== false)
        {
            $row = mysql_fetch_row($res);

            if ($row !== false)
            {
                return $row[0];
            }
            else
            {
                return '';
            }
        }
        return false;
    }
    
	function getAll($sql , $field = '')
    {
        $res = $this->query($sql);
        if ($res !== false)
        {
            $arr = array();
            while ($row = mysql_fetch_assoc($res))
            {
            	if($field){
            		$arr[$row[$field]] = $row;
            	}else{
            		$arr[] = $row;
            	}
            }
            return $arr;
        }
        else
        {
            return false;
        }
    }
    
    function fetch($res){
    	return mysql_fetch_assoc($res);
    }
    
    function fetch_num($res){
    	return mysql_fetch_row($res);
    }
    
	function getRow($sql, $limited = false)
    {
        if ($limited == true)
        {
            $sql = trim($sql . ' LIMIT 1');
        }

        $res = $this->query($sql);
        if ($res !== false)
        {
            return mysql_fetch_assoc($res);
        }
        return false;
    }
	
    function getCol($sql)
    {
        $res = $this->query($sql);
        if ($res !== false)
        {
            $arr = array();
            while ($row = mysql_fetch_row($res))
            {
                $arr[] = $row[0];
            }
            return $arr;
        }
        return false;
    }
}