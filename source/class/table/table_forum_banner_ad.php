<?php

if (!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_forum_banner_ad extends discuz_table {
	public $_table = 'forum_banner_ad';
	public $_pk = 'baid';
	
	public function fetch_all($type = '', $limit = 0, $orderType = 'DESC') {
		$type = $type==''?'':" where type = '{$type}'";
		$sql = sprintf('select * from %s '.$type.' order by displayorder ' . $orderType, DB::table($this -> _table));
		if ($limit) {
			$sql .= ' limit 0,' . (int)$limit;
		}
		return DB::fetch_all($sql);
	}

	public function fetch_row_by_tid($tid) {
		$sql = sprintf('select * from %s where tid = %d limit 1', DB::table($this -> _table), $tid);
		return DB::fetch_first($sql);
	}

	public function insert($data) {
		return DB::insert($this -> _table, $data);
	}

	public function update($data, $condition = '') {
		return DB::update($this -> _table, $data, $condition);
	}

}
