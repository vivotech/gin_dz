<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: table_forum_forum.php 33548 2013-07-04 08:19:27Z laoguozhang $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_forum_custom_activity_reward extends discuz_table
{
	public function __construct() {

		$this->_table = 'forum_custom_activity_reward';
		$this->_pk    = 'carid';

		parent::__construct();
	}
	
	public function fetch_all_by_caid($caid, $orderby = 'carid'){
		$caid = $caid ? intval($caid) : 0;
		$sql = 'SELECT * FROM '.DB::table($this->_table).' WHERE caid = '.$caid.' ORDER BY '.$orderby;
		return $this->query_all(DB::query($sql));
	}
	
	private function query_all($query){
		$data = array();
		while($product = DB::fetch($query)){
			$data[] = $product;
		}
		return $data;
	}
}