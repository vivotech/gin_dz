<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: table_common_member_security.php 27449 2012-02-01 05:32:35Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_common_member_gallery extends discuz_table
{
	public function __construct() {

		$this->_table = 'common_member_gallery';
		$this->_pk    = 'gid';

		parent::__construct();
	}
	
	public function fetch_all_by_uid($uid){
		if(!is_array($uid)){
			$uid = array($uid);
		}
		$sql = 'SELECT * FROM '.DB::table($this->_table).' WHERE'.DB::field('uid', $uid);
		return $this->query_all(DB::query($sql));
	}
	
	public function fetch_by_uid($uid){
		$sql = 'SELECT * FROM '.DB::table($this->_table).' WHERE uid = '.$uid.' ORDER BY isdefault DESC';
		return DB::fetch_first($sql);
	}
	
	private function query_all($query){
		$data = array();
		while($product = DB::fetch($query)){
			$data[] = $product;
		}
		return $data;
	}
}