<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: table_common_syscache.php 31119 2012-07-18 04:21:20Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_common_member_address extends discuz_table
{
	
	public function __construct() {
		$this->_table = 'common_member_address';
		$this->_pk    = 'uaid';
		
		parent::__construct();
	}
  
	public function fetch_by_uaid($uaid){
		$sql = sprintf("SELECT * FROM %s WHERE uaid=%d limit 1", DB::table($this->_table), $uaid);
		return DB::fetch_first($sql);
	}
	
	public function fetch_all_by_uid($uid){
		$uid = intval($uid);
		if($uid > 0){
			$sql = 'SELECT * FROM '.DB::table($this->_table).' WHERE uid = '.$uid.' ORDER BY lastusetime DESC,uaid';
			$query = DB::query($sql);
			return $this->query_all($query);
		}else{
			return false;
		}
	}
	
	public function delete_by_uid($uaid, $uid = 0){
		$uaid = intval($uaid);
		$uid = intval($uid);
		$sql = 'DELETE FROM '.DB::table($this->_table).' WHERE uaid = '.$uaid.' AND uid = '.$uid;
		return DB::query($sql);
	}
	
	private function query_all($query){
		$data = array();
		while($product = DB::fetch($query)){
			$data[] = $product;
		}
		return $data;
	}
}