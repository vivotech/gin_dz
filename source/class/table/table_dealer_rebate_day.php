<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_dealer_rebate_day extends discuz_table {
	
	public function __construct() {
		$this->_table = 'dealer_rebate_day';

		parent::__construct();
	}
	
	/**
	 * 按经销商和年月日获取 经销商分成日统计缓存
	 */
	public function fetch_by_duid_day($duid, $year, $month, $day) {
		$sql = sprintf("SELECT * FROM %s
			WHERE `duid` = %d and `year` = '%s' and `month` = '%s'
			and `day` = '%s'"
			, DB::table($this->_table), $duid, $year, $month, $day);
		return DB::fetch_first($sql);
	}
	
	public function fetch_all_by_duid_year_month($duid, $year, $month){
		$sql = sprintf('SELECT * FROM %s WHERE `duid` = %d AND `year` = %d AND `month` = %d ORDER BY day',
						DB::table($this->_table), $duid, $year, $month);
		return $this->query_all(DB::query($sql));
	}
	
	public function delete_by_duid_day($duid, $year, $month, $day) {
		$sql = sprintf("delete from %s
		    where `duid` = %d and `year` = '%s' and `month` = '%s'
			and `day` = '%s'", DB::table($this->_table), $duid, $year, $month, $day);
		return DB::query($sql);
	}
	
	private function query_all($query){
		$data = array();
		while($product = DB::fetch($query)){
			$data[] = $product;
		}
		return $data;
	}
}

