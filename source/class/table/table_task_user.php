<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_task_user extends discuz_submeter_table{
	
	
	public function __construct() {
		//分表处理
		$this->_origin_table = 'task_user';
		$this->_pk    = 'tuid';
		
		$this->_pre_cache_key = 'task_user_';
		$this->_cache_ttl = 60*30;
		$this->_allowmem = memory('check');
		
		parent::__construct();
	}
	
	public function fetch_by_uid_tid($uid,$tid){
		$item = $this->fetch_cache("{$tid}_{$uid}");
		if(!$item){
			$this->set_hash_name($uid);
			$sql = "SELECT * FROM ".DB::table($this->_table)." WHERE uid = '{$uid}' AND tid = '{$tid}' ";
			$item = DB::fetch_first($sql);
			if($item){
				$this->store_cache("{$tid}_{$uid}", $item);
			}
		}
		return $item;
	}
	
	public function update_by_uid_tid($uid,$tid,$data){
		$this->set_hash_name($uid);
		$ret = DB::update($this->_table, $data, "  uid = '{$uid}' AND tid = '{$tid}' ");
		if($ret){
			$this->update_cache("{$tid}_{$uid}",$ret);
		}
		return $ret;
	}
	
	public function count_all_by_tid($tid){
		$sql = 'SELECT COUNT(*) FROM (';
		for($i=0;$i<$this->table_num;$i++){
			$sql .= ( $i!=0?' UNION ' : '' )." SELECT * FROM ".DB::table($this->_origin_table.'_'.$i)." WHERE tid = '{$tid}' ";
		}
		$sql .= ' ) t ';
		return $this->custom_db->getOne($sql);
	}
	
	public function fetch_all_by_tid($tid,$start=-1,$pagesize=-1,$order=''){
		$sql = '';
		for($i=0;$i<$this->table_num;$i++){
			$sql .= ( $i!=0?' UNION ' : '' )." SELECT * FROM ".DB::table($this->_origin_table.'_'.$i)." WHERE tid = '{$tid}'";
		}
		$sql .= !empty($order) ? " ORDER BY $order " : '';
		$sql .= $start!=-1&&$pagesize!=-1 ? " LIMIT  $start , $pagesize " : '' ;
		$temp = $this->custom_db->getAll($sql);
	}
	
}