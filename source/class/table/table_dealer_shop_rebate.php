<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: table_common_admincp_cmenu.php 27806 2012-02-15 03:20:46Z svn_project_zhangjie $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_dealer_shop_rebate extends discuz_table
{
	public function __construct() {
		$this->_table = 'dealer_shop_rebate';
		$this->_pk    = 'sid';

		parent::__construct();
	}
	
	public function fetch_all_by_sid_btime_etime($sid, $btime, $etime, $start = -1, $limit = 0, $orderby = ''){
		$sql = 'SELECT * FROM '.DB::table($this->_table).'
					WHERE `sid` = '.$sid.'
					AND `dateline` >= '.$btime.'
					AND `dateline` <= '.$etime;
		if(!empty($orderby)){
			$sql .= ' ORDER BY '.$orderby;
		}
		if($start >= 0 && $limit > 0){
			$sql .= ' limit '.$start.','.$limit;
		}
		return $this->query_all(DB::query($sql));
	}
	
	public function count_by_sid_btime_etime($sid, $btime, $etime){
		$sql = 'SELECT count(*) AS c FROM '.DB::table($this->_table).'
					WHERE `sid` = '.$sid.'
					AND `dateline` >= '.$btime.'
					AND `dateline` <= '.$etime;
		return DB::result_first($sql);
	}
	
	public function sum_by_sid_btime_etime($sid, $btime, $etime, $sumfield = 'rebate'){
		$sql = 'SELECT sum(`'.$sumfield.'`) AS s FROM '.DB::table($this->_table).'
					WHERE `sid` = '.$sid.'
					AND `dateline` >= '.$btime.'
					AND `dateline` <= '.$etime;
		return DB::result_first($sql);
	}

	private function query_all($query){
		$data = array();
		while($item = DB::fetch($query)){
			$data[] = $item;
		}
		return $data;
	}
}

?>