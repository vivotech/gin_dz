<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_task extends discuz_table{
	
	
	public function __construct() {

		$this->_table = 'task';
		$this->_pk    = 'tid';
		
		$this->_pre_cache_key = 'task_';	//缓存前序
		$this->_cache_ttl = 60 * 15 ;	//缓存时间 ，15分钟
		
		$this->_allowmem = memory('check');
		
		parent::__construct();
	}
	
	public function fetch_all_list(){
		$list = $this->fetch_cache('list','task_');
		if(!$list){
			$sql  = "SELECT * FROM ".DB::table($this->_table)." ORDER BY tid DESC ";
			$list = DB::fetch_all($sql);
			$this->store_cache('list', $list, 60*5 ,'task_');
		}
		return $list;
	}
	
	public function fetch_list($where = '',$start = -1 , $pagesize = -1){
		$sql  = "SELECT * FROM ".DB::table($this->_table).
				(empty($where) ? '' : " WHERE $where").( $start!=-1&&$pagesize!=-1 ? " LIMIT $start , $pagesize " : '');
		$list = DB::fetch_all($sql);
		if($list){
			foreach($list as $k=>$l){
				//玩家数量
				$list[$k]['players_count'] = C::t('task_rule_user')->count_all_by_tid($l['tid']);
				//奖品列表
				$list[$k]['prizes'] = C::t('task_prize')->fetch_all_by_tid($l['tid']);
			}
		}
		return $list;
	}
	
	//复写fetch方法，写入缓存
	public function fetch($id, $force_from_db = false){
		$data = array();
		if(!empty($id)) {
			if($force_from_db || ($data = $this->fetch_cache($id,$this->_pre_cache_key)) === false) {
				$data = DB::fetch_first('SELECT * FROM '.DB::table($this->_table).' WHERE '.DB::field($this->_pk, $id));
				//规则
				$data['rules'] = array();
				$rules = C::t('task_rule')->fetch_all_by_tid($id);
				foreach($rules as $rk=>$rv){
					$data['rules'][$rv['actionid']] = $rv;
				}
				
				//奖品
				$data['prizes'] = C::t('task_prize')->fetch_all_by_tid($id);
				//保存缓存
				if(!empty($data)) $this->store_cache($id, $data,$this->_cache_ttl,$this->_pre_cache_key);
			}
		}
		return $data;
	}
	
}