<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: table_common_syscache.php 31119 2012-07-18 04:21:20Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_common_product_onsell_discount extends discuz_table
{
	
	public function __construct() {
		$this->_table = 'common_product_onsell_discount';
		$this->_pk    = '';

		parent::__construct();
	}
	
	/**
	 * 获取全局定义规则
	 */
	public function get_global_discount_setting(){
		$sql = 'SELECT * FROM '.DB::table($this->_table).' WHERE spid = 0';
		$query = DB::query($sql);
		$data = array();
		while($discount = DB::fetch($query)){
			$data[$discount['groupid']] = $discount;
		}
		return $data;
	}
	
	public function fetch_all_by_spid($spid,$where='',$order=''){
		$where = empty($where) ? ' WHERE spid = '.$spid : " WHERE spid = '{$spid}' AND ".$where;
		$order = empty($order) ? '' : " ORDER BY {$order} ";
		$sql = 'SELECT * FROM '.DB::table($this->_table).$where.$order;
		$query = DB::query($sql);
		return $this->query_all($query);
	}
	
	public function delete_by_spid($spid){
		$sql = 'DELETE FROM '.DB::table($this->_table).' WHERE spid = '.$spid;
		return DB::query($sql);
	}
	
	private function query_all($query){
		$data = array();
		while($product = DB::fetch($query)){
			$data[] = $product;
		}
		return $data;
	}
}