<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: table_common_admincp_cmenu.php 27806 2012-02-15 03:20:46Z svn_project_zhangjie $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_dealer_sale_relation extends discuz_table
{
	public function __construct() {

		$this->_table = 'dealer_sale_relation';
		$this->_pk    = 'srid';

		parent::__construct();
	}

	public function fetch_by_sid($sid){
		$sql = 'SELECT * FROM '.DB::table($this->_table).' WHERE `sid` = '.$sid.' ORDER BY srid';
		return DB::fetch_first($sql);
	}
	
	public function fetch_by_uid_pdid_sid($uid, $pdid, $sid){
		$sql = 'SELECT * FROM '.DB::table($this->_table).' 
					WHERE `uid` = '.$uid.'
					AND `pdid` = '.$pdid.'
					AND `sid` = '.$sid.'
					ORDER BY srid';
		return DB::fetch_first($sql);
	}
	
	public function fetch_by_uid_pdid($uid, $pdid){
		$sql = 'SELECT * FROM '.DB::table($this->_table).' 
					WHERE `uid` = '.$uid.'
					AND `pdid` = '.$pdid.'
					ORDER BY srid';
		return DB::fetch_first($sql);
	}

	private function query_all($query){
		$data = array();
		while($product = DB::fetch($query)){
			$data[] = $product;
		}
		return $data;
	}
}

?>