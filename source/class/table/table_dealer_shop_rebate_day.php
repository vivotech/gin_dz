<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: table_common_admincp_cmenu.php 27806 2012-02-15 03:20:46Z svn_project_zhangjie $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_dealer_shop_rebate_day extends discuz_table
{
	public function __construct() {
		$this->_table = 'dealer_shop_rebate_day';
		$this->_pk    = 'sid';

		parent::__construct();
	}
	
	/**
	 * @name (暂不能用)
	 * @param $sid
	 * @param $bdate 格式:xxxx-xx-xx
	 * @param $edate 格式:xxxx-xx-xx
	 */
	public function fetch_all_by_sid_bdate_edate($sid, $bdate, $edate){
		
		$sql = 'SELECT * FROM '.DB::table($this->_table).'
					WHERE `sid` = \''.$sid.'\'
					';
	}

	public function fetch_by_sid_year_month_day($sid, $year, $month, $day){
		$sql = 'SELECT * FROM '.DB::table($this->_table).'
					WHERE `sid` = '.$sid.'
					AND `year` = \''.$year.'\'
					AND `month` = \''.$month.'\'
					AND `day` = \''.$day.'\'';
		return DB::fetch_first($sql);
	}
	
	public function fetch_all_by_sid_year_month($sid, $year, $month, $orderby = 'day'){
		$sql = 'SELECT * FROM '.DB::table($this->_table).'
					WHERE `sid` = '.$sid.'
					AND `year` = \''.$year.'\'
					AND `month` = \''.$month.'\'';
		if(!empty($orderby)){
			$sql .= ' ORDER BY '.$orderby;
		}
		return $this->query_all(DB::query($sql));
	}
	
	public function delete_by_sid_year_month_day($sid, $year, $month, $day){
		$sql = 'DELETE FROM '.DB::table($this->_table).'
					WHERE `sid` = '.$sid.'
					AND `year` = \''.$year.'\'
					AND `month` = \''.$month.'\'
					AND `day` = \''.$day.'\'';
		return DB::query($sql);
	}
	
	private function query_all($query){
		$data = array();
		while($product = DB::fetch($query)){
			$data[] = $product;
		}
		return $data;
	}
}

?>