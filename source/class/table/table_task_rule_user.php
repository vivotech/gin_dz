<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_task_rule_user extends discuz_submeter_table{
	
	public function __construct() {
		$this->_origin_table = 'task_rule_user';
		$this->_pk    = 'truid';
		parent::__construct();
	}
	
	
	public function count_all_by_tid($tid){
		$sql = 'SELECT COUNT(t.*) FROM ( ';
		for($i=0;$i<$this->table_num;$i++){
			$sql .= ( $i!=0?' UNION ALL ' : '' )." SELECT * FROM ".DB::table($this->_origin_table.'_'.$i)." WHERE tid = '{$tid}' ";
		}
		$sql .= ' ) t ';
		return $this->custom_db->getOne($sql);
	}

    public function count_all_by_uid_tid($uid,$tid){
        $sql = 'SELECT COUNT(t.truid) FROM ( ';
        for($i=0;$i<$this->table_num;$i++){
            $sql .= ( $i!=0?' UNION ALL ' : '' )." SELECT * FROM ".DB::table($this->_origin_table.'_'.$i)." WHERE tid = '{$tid}' AND task_uid = '{$uid}' ";
        }
        $sql .= ' ) t ';
        return $this->custom_db->getOne($sql);

    }

    public function fetch_all_by_uid_tid($uid,$tid,$page,$pagesize){
        $sql = 'SELECT t.* FROM ( ';
        for($i=0;$i<$this->table_num;$i++){
            $sql .= ( $i!=0?' UNION ALL ' : '' )." SELECT * FROM ".DB::table($this->_origin_table.'_'.$i)." WHERE tid = '{$tid}' AND task_uid = '{$uid}' ";
        }
        $sql .= ' ) t LIMIT '.($pagesize*($page-1)).','.$pagesize;
        return $this->custom_db->getAll($sql);
    }

	public function fetch_condition($condition){
		$where = $this->parse_condition($condition);
        if(!$where)
            return false;
		$sql = "SELECT * FROM ".DB::table($this->_table)." WHERE {$where}";
		return DB::fetch_first($sql);
	}
	
}