<?php

/**
 * 微信账号
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}


class table_wechats extends discuz_table{
	
	public function __construct() {

		$this->_table = 'wechats';
		$this->_pk    = 'weid';

		parent::__construct();
	}
	
	function fetch_by_hash($hash){
		$hash = addslashes($hash);
		$sql = "SELECT * FROM ".DB::table($this->_table)." WHERE hash='{$hash}'";
		return DB::fetch_first($sql);
	}
	
}
