<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_dealer_rebate_month extends discuz_table {
	
	public function __construct() {
		$this->_table = 'dealer_rebate_month';

		parent::__construct();
	}
	
	/**
	 * @name 按经销商和年月获取 经销商分成月统计缓存
	 */
	public function fetch_by_duid_month($duid, $year, $month) {
		$sql = sprintf("SELECT * FROM %s WHERE `duid` = %d and `year` = '%s' and `month` = '%s'"
						, DB::table($this->_table), $duid, $year, $month);
		return DB::fetch_first($sql);
	}
	
	/**
	 * @name 按经销商和年月获取 经销商分成月统计缓存
	 */
	public function delete_by_duid_month($duid, $year, $month) {
		$sql = sprintf("DELETE FROM %s WHERE `duid` = %d and `year` = '%d' and `month` = '%d'", 
						DB::table($this->_table), $duid, $year, $month, $day);
		return DB::query($sql);
	}
	
}

