<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_task_rule extends discuz_table{
	
	public function __construct() {

		$this->_table = 'task_rule';
		$this->_pk    = 'trid';
		
		parent::__construct();
		
		$this->_pre_cache_key = 'task_rule_';	//缓存前序
		$this->_cache_ttl = 60*30 ;			//缓存时间 ，30分钟
		$this->_allowmem = memory('check');
	
	}

	public function fetch_all_by_tid($tid){
		$sql = "SELECT * FROM ".DB::table($this->_table)." WHERE tid = '{$tid}' ";
		$list = DB::fetch_all($sql);
		return $list;
	}
	
	public function delete_by_tid($tids){
		$tids = is_array($tids) ? $tids : array($tids);
		foreach($tids as $tid){
			DB::delete($this->_table, DB::field('tid', $tid), null, $unbuffered);
		}
	}
	
}