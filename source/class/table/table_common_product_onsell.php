<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: table_common_syscache.php 31119 2012-07-18 04:21:20Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_common_product_onsell extends discuz_table
{
	
	public function __construct() {
		$this->_table = 'common_product_onsell';
		$this->_pk    = 'spid';

		parent::__construct();
	}
	
	/**
	 * @name 获取正在上架的商品数量
	 */
	public function count_onsell($psid = 0){
		$sql = 'select count(sp.spid) AS c 
					FROM '.DB::table($this->_table).' sp';
		if(!empty($psid)){
			$sql .= ' LEFT JOIN '.DB::table('common_product').' p ON sp.pdid = p.pdid';
		} 
		$sql .= ' WHERE sp.`closed` = 0 AND (sp.`expiration` > ' . TIMESTAMP.' OR sp.`expiration` <= 0)';
		if(!empty($psid)){
			$sql .= ' AND p.`psid` = '.$psid;
		}
		$count = DB::fetch_first($sql);
		return $count['c'];
	}
	
	public function fetch_all_page($page = 1, $pagesize = 20 ,$where = '' , $order = '' ){
		$where = ( empty($where) || $where == '' ) ? '' :  ' AND  '.$where ;
		$order = ( empty($order) || $order == '' ) ? '  sp.closed, sp.spid DESC ' : $order ;
		$sql = 'SELECT sp.* , p.name , p.costprice , p.media_list, p.mobile_banner , ps.series
					FROM '.DB::table($this->_table).' sp
					LEFT JOIN '.DB::table('common_product').' p ON sp.pdid = p.pdid
					LEFT JOIN '.DB::table('common_product_series').' ps ON p.psid = ps.psid 
					WHERE p.onsell = 0 '.$where .' ORDER BY '.$order;
		if($pagesize > 0){
			$sql .= ' LIMIT '.(($page - 1) * $pagesize).','.$pagesize;
		}
		$query = DB::query($sql);
		return $this->query_all($query);
	}
	
	public function onsell_state_change($closed = 0, $spid = 0){
		$spidlist = (is_array($spid)) ? $spid : array($spid);
		$sql = 'UPDATE '.DB::table($this->_table).' SET closed = '.$closed;
		$where = ' WHERE ';
		foreach($spidlist as $k=>$spid){
			if($k > 0) $where .= ' OR ';
			$where .= ''.$this->_pk.' = '.$spid;
		}
		
		return DB::query($sql.$where);
	}

	public function fetch_detail($spid){
		$spidlist = (is_array($spid)) ? $spid : array($spid);
		$sql = 'SELECT sp.*, p.name, p.costprice, p.psid, p.media_list, p.mobile_banner
					FROM '.DB::table($this->_table).' sp
					LEFT JOIN '.DB::table('common_product').' p ON sp.pdid = p.pdid';
		$where = ' WHERE ';
		$i = 0;
		foreach($spidlist as $l){
			if($i != 0) $where .= ' OR ';
			$where .= ' spid = '.$l;
			$i++;
		}
		$query = DB::query($sql.$where);
		return $this->query_all($query);
	}
  
  public function fetch_by_spid($spid)
  {
    return DB::fetch_first("SELECT * FROM %t WHERE spid=%d limit 1", array($this->_table, $spid));
  }
  
  public function fetch_spids_by_pdid($pdid)
  {
    $rows = DB::fetch_all("SELECT spid FROM %t WHERE pdid=%d", array($this->_table, $pdid));
    
    $ids = array();
    foreach ($rows as $row)
    {
      $ids[] = $row['spid'];
    }
    
    return $ids;
  }
  
  public function fetch_spid_by_pdid($pdid)
  {
    $row = DB::fetch_first("SELECT spid FROM %t WHERE pdid=%d limit 1", array($this->_table, $pdid));
    if ($row)
    {
      return $row['spid'];
    }
    
    return false;
  }
  
	private function query_all($query){
		$data = array();
		while($product = DB::fetch($query)){
			$data[] = $product;
		}
		return $data;
	}
	
	/**
	 * 获取一个在售的积分商品
	 */
	public function fetch_one_integral_product() {
		
		$sql = sprintf("SELECT * FROM %s WHERE type = 1 AND closed = 0",
		    DB::table($this->_table), time());
		return DB::fetch_first($sql);
		
	}
	
	
}