<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: table_common_admincp_cmenu.php 27806 2012-02-15 03:20:46Z svn_project_zhangjie $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_dealer_member extends discuz_table
{
	public function __construct() {

		$this->_table = 'dealer_member';
		$this->_pk    = 'duid';

		parent::__construct();
	}

	public function fetch_by_uid($uid){
		$sql = 'SELECT * FROM '.DB::table($this->_table).' WHERE `uid` = '.$uid.' ORDER BY duid';
		return DB::fetch_first($sql);
	}
	
	public function fetch_by_duid($duid){
		$sql = 'SELECT * FROM '.DB::table($this->_table).' WHERE `duid` = '.$duid.' ORDER BY duid';
		return DB::fetch_first($sql);
	}
	
	public function fetch_child_dealers() {
		$sql = sprintf('select * from %s where duid > 1', DB::table($this->_table));
		return DB::fetch_all($sql);
	}

	private function query_all($query){
		$data = array();
		while($product = DB::fetch($query)){
			$data[] = $product;
		}
		return $data;
	}
	
	/**
	 * 标记为已删除
	 */
	public function delete_dealer($duid) {
		$opt = $this->update($duid, array('deleted'=>1));
		return $opt;
	}
	
	/**
	 * 不允许删除记录
	 */
	public function delete($val, $unbuffered = false) {
		return false;
	}
	
	
	
}

?>