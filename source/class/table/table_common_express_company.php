<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: table_common_syscache.php 31119 2012-07-18 04:21:20Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_common_express_company extends discuz_table
{
	
	public function __construct() {
		$this->_table = 'common_express_company';
		$this->_pk    = 'ecid';
		
		parent::__construct();
	}
  
  public function fetch_by_ecid($ecid){
    $sql = sprintf("SELECT * FROM %s WHERE ecid=%d limit 1", DB::table($this->_table), $ecid);
    return DB::fetch_first($sql);
  }
	
	public function fetch_all_table(){
		$sql = 'SELECT * FROM '.DB::table($this->_table).' ORDER BY ecid';
		$query = DB::query($sql);
		$data = array();
		while($ec = DB::fetch($query)){
			$data[] = $ec;
		}
		return $data;
	}
}