<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: table_common_admincp_cmenu.php 27806 2012-02-15 03:20:46Z svn_project_zhangjie $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_dealer_member_shop extends discuz_table
{
	public function __construct() {

		$this->_table = 'dealer_member_shop';
		$this->_pk    = 'sid';

		parent::__construct();
	}

	public function fetch_by_sid($sid){
		$sql = 'SELECT * FROM '.DB::table($this->_table).' WHERE `sid` = '.$sid;
		return DB::fetch_first($sql);
	}

	public function fetch_by_duid($duid){
		$sql = 'SELECT * FROM '.DB::table($this->_table).' WHERE `duid` = '.$duid.' ORDER BY duid';
		return DB::fetch_first($sql);
	}
	
	public function fetch_all_by_duid($duid){
		$sql = 'SELECT * FROM '.DB::table($this->_table).' WHERE `duid` = '.$duid.' ORDER BY duid';
		return $this->query_all(DB::query($sql));
	}
	
	private function query_all($query){
		$data = array();
		while($product = DB::fetch($query)){
			$data[] = $product;
		}
		return $data;
	}
	
	public function fetch_all_filters($filters = array())
	{
		$where_items = array();
		foreach ($filters as $k => $v) {
			$where_items[] = sprintf('`%s`=%s', $k, is_string($v) ? "'$v'" : $v);
		}
		
		$sql = sprintf('SELECT * FROM %s', DB::table($this->_table));
		if ( count($where_items) ) {
			$sql .= ' where ' . implode(' AND ', $where_items);
		}
		
		return $this->query_all(DB::query($sql));
	}
	
}

?>