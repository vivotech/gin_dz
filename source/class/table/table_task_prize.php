<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_task_prize extends discuz_table{
	
	public function __construct() {

		$this->_table = 'task_prize';
		$this->_pk    = 'tpid';
		parent::__construct();
	
	}
	
	public function fetch_all_by_tid($tid){
		$sql = "SELECT * FROM ".DB::table($this->_table)." WHERE tid = '{$tid}' ";
		$list = DB::fetch_all($sql);
		foreach($list as $k=>$v){
			switch($v['ptype']){
				case 1: $list[$k]['award'] = '积分奖励【'.$v['num'].'分】';break;
				case 2: $list[$k]['award'] = '优惠券';break;
				case 3: 
					$award = C::t('common_product_onsell')->fetch_detail($v['pid']);
					$list[$k]['award'] = '实物奖品【'.$award[0]['subject'].'】';
					break;
			}
		}
		return $list;
	}
	
	public function delete_by_tid($tids){
		$tids = is_array($tids) ? $tids : array($tids);
		foreach($tids as $tid){
			DB::delete($this->_table, DB::field('tid', $tid), null, $unbuffered);
		}
	}
	
}