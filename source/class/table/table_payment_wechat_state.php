<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_payment_wechat_state extends discuz_table {
	
	public function __construct() {
		$this->_table = 'payment_wechat_state';
		$this->_pk = 'pws_id';
		parent::__construct();
	}
	
	public function fetch_by_oid($oid){
		$oid = intval($oid);
		$sql = "SELECT * FROM ".DB::table($this->_table)." WHERE oid = '{$oid}' ";
		return DB::fetch_first($sql);
	}
	
	
}