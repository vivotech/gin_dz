<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: table_common_syscache.php 31119 2012-07-18 04:21:20Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_common_member_order_state extends discuz_table
{
	
	public function __construct() {
		$this->_table = 'common_member_order_state';
		$this->_pk    = 'sid';
		
		parent::__construct();
	}
  
  public function fetch_all_by_oid($oid, $order = '', $desc = false)
  {
    $sql = "SELECT * FROM %t WHERE oid=%d";
    if ($order)
    {
      $sql .= " ORDER BY " . $order;
      if ($desc)
      {
        $sql .= " DESC";
      }
    }
    return DB::fetch_all($sql, array($this->_table, $oid));
  }
  
}
