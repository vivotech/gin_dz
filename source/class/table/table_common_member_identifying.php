<?php

/** 
 * APP 注册时验证表
 * 
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_common_member_identifying extends discuz_table
{
  static public $table = 'common_member_identifying';
  
  public function __construct() {
  		$this->_table = self::$table;
		$this->_pk    = 'id';
		parent::__construct();
  }
  
  public function fetch_all_by_input_limit($input,$start = 0,$limit = 5,$where='',$order="addtime"){
  		$where = empty($where) ? " WHERE input_value = '{$input}' ":" WHERE input_value = '{$input}' AND ".$where;
  		$sql = 'SELECT * FROM '.DB::table(self::$table).$where." ORDER BY {$order} DESC LIMIT {$start},{$limit}";
  		$query = DB::query($sql);
		return $this->query_all($query);
  }
  
  public function fetch_last($input,$uid=0){
  	  return DB::fetch_first("SELECT * FROM %t WHERE input_value = %s AND uid = '{$uid}' ORDER BY `addtime` DESC limit 1", array(self::$table, $input));
  }
  
  
  public function fetch_by_mobile($mobile){
  	  return DB::fetch_first("SELECT * FROM %t WHERE input_value = %s AND input_type = 'mobile' ORDER BY `addtime` DESC limit 1", array(self::$table, $mobile));
  }
  
  public function fetch_by_email($email){
  	  return DB::fetch_first("SELECT * FROM %t WHERE input_value = %s AND input_type = 'email' ORDER BY `addtime` DESC limit 1", array(self::$table, $email));
  }
  
  public function fetch_verified_mobile_by_uid($uid) {
  	  $sql = sprintf("SELECT * FROM %s WHERE uid=%d AND input_type='email' AND is_verify=1", DB::table(self::$table), $uid);
  	  return DB::fetch_first($sql);
  }
  
  public function count_by_where($wheresql = '') {
		$wheresql = !empty($wheresql) ? ' WHERE '.$wheresql : '';
		return DB::result_first('SELECT COUNT(*) FROM '.DB::table(self::$table).$wheresql);
  }
  
  public function  fetch_by_where($wheresql = '') {
		$wheresql = !empty($wheresql) ? ' WHERE '.$wheresql : '';
		return DB::fetch_all('SELECT * FROM '.DB::table(self::$table).$wheresql);
  }
  
  public function update_identifying_by_id($id, $data = array()){
    return DB::update(self::$table, $data, array("id" => $id));
  }
  
  private function query_all($query){
	$data = array();
	while($product = DB::fetch($query)){
		$data[] = $product;
	}
	return $data;
  }

  public function delete_by_uid($uid){
  	$ret = false;
	if(isset($uid)) {
		$ret = DB::delete($this->_table, DB::field('uid', $uid), null, FALSE);
	}
	return $ret;
  }
  
}


