<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_badge_member extends discuz_table
{
	public function __construct() {

		$this->_table = 'badge_member';
		$this->_pk    = 'bmid';

		parent::__construct();
	}
	
	/**
	 * @name 获取用户获得指定徽章的记录
	 * @param String  $badge 徽章ID
	 * @param Int  $uid 用户ID
	 * @return Array|false
	 */
	public function fetch_by_badge_uid($badge, $uid) {
		return DB::fetch_first("SELECT * FROM %t WHERE badge=%s AND uid=%d",
		  array($this->_table, $badge, $uid));
	}

    /**
     * @name 获取用户获得的所有徽章的记录
     * @param Int   $uid 用户ID
     * @return Array|false
     */
    public function fetch_by_uid($uid) {
        return DB::fetch_all("SELECT * FROM %t WHERE uid=%d",
            array($this->_table, $uid));
    }
	
	
}




