<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: table_common_syscache.php 31119 2012-07-18 04:21:20Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_common_product_series extends discuz_table
{
	
	public function __construct() {
		$this->_table = 'common_product_series';
		$this->_pk    = 'psid';

		parent::__construct();
	}
	
	public function fetch_all(){
		$data = array();
		$query = DB::query('SELECT * FROM '.DB::table($this->_table));
		while($series = DB::fetch($query)){
			$data[] = $series;
		}
		return $data;
	}
	
	public function fetch_all_page($page = 1, $pagesize = 20, $where = '' , $order = 'displayorder DESC, psid ASC'){
		$where = ( empty($where) || $where == '' ) ? '' :  ' WHERE '.$where ;
		$sql = 'SELECT * FROM '.DB::table($this->_table). $where . ' ORDER BY '.$order;
		if($pagesize > 0){
			$sql .= ' LIMIT '.(($page - 1) * $pagesize).','.$pagesize;
		}
		$query = DB::query($sql);
		return $this->query_all($query);
	}
	
	public function fetch_series_num($type = '', $psup = ''){
		$psupsql = $psup ? DB::field('psup', $psup).' AND ' : '';
		$addwhere = $type == 'group' ? "`status`='3'" : "`status`<>3";
		return DB::result_first("SELECT COUNT(*) FROM ".DB::table($this->_table)." WHERE $psupsql $addwhere");
	}
	
	public function fetch_all_series_for_sub_order(){
		return DB::fetch_all("SELECT ps.psid, ps.type, ps.status, ps.series, ps.psup, ps.displayorder FROM ".DB::table($this->_table)." ps WHERE ps.status<>'3' ORDER BY ps.type<>'group', ps.displayorder");
	}
	
	function get_series_by_psid($psid, $field = '') {
		static $serieslist = array();
		$return = array();
		if(!array_key_exists($psid, $serieslist)) {
			$serieslist[$psid] = DB::fetch_first("SELECT * FROM ".DB::table($this->_table)." WHERE psid=%d", array($psid));
			if(!is_array($serieslist[$psid])) {
				$serieslist[$psid] = array();
			}
		}

		if(!empty($field)) {
			$return = isset($serieslist[$psid][$field]) ? $serieslist[$psid][$field] : null;
		} else {
			$return = $serieslist[$psid];
		}
		return $return;
	}
	
	private function query_all($query){
		$data = array();
		while($product = DB::fetch($query)){
			$data[] = $product;
		}
		return $data;
	}

    public function fetch_by_series($series) {

        $sql = sprintf("SELECT * FROM %s WHERE series='%s'", DB::table($this->_table), $series);

        return DB::fetch_first($sql);

    }

    public function fetch_by_pk($id) {
        $sql = sprintf("SELECT * FROM %s WHERE %s=%s",
            DB::table($this->_table), $this->_pk, is_string($id) ? "'$id'" : $id
        );
        return DB::fetch_first($sql);
    }

    /**
     * 检查参数一是否参数二的祖先节点
     * @param $a
     * @param $b
     */
    public static function isParents($a, $b) {

        $parents = self::getParents($b);
        var_dump($b);

        if (!$parents) {
            return false;
        }

        foreach ($parents as $v) {
            if ( $a['psid'] == $b['psup'] ) {
                return true;
            }
        }

        return false;
    }

    /**
     * @name 获取指定系列的祖先节点记录
     * @param Array  $series 商品系列记录
     * @return Array
     */
    public static function getParents($series)
    {
        $parent = self::getParent($series);

        if ($parent)
        {
            $parent_parents = self::getParents($parent);
            if ($parent_parents)
            {
                return array_merge(array($parent), $parent_parents);
            }
        }
        return array($parent);
    }

    /**
     * @name 获取指定系列的直系父节点记录
     * @param Array  $series 商品系列记录
     * @return false|Array
     */
    public static function getParent($series)
    {
        $table = new self;
        return $table->fetch_by_pk($series['psup']);
    }

}