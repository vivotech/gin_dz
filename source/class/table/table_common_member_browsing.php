<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: table_common_admincp_cmenu.php 27806 2012-02-15 03:20:46Z svn_project_zhangjie $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_common_member_browsing extends discuz_table
{
	public function __construct() {

		$this->_table = 'common_member_browsing';
		$this->_pk    = 'uid';

		parent::__construct();
	}
	
	/**
	 * 一段时间内的总记录数
	 */
	public function count_by_sid_btime_etime($sid, $btime, $etime){
		$sql = 'SELECT count(*) AS c FROM '.DB::table($this->_table).'
					WHERE `sid` = '.$sid.'
					AND `dateline` >= '.$btime.'
					AND `dateline` <= '.$etime;
		return DB::result_first($sql);
	}
	
	/**
	 * 统计按产品数统计
	 * 一段时间内的按产品分组的记录数
	 */
	public function count_pd_by_sid_btime_etime($sid, $btime, $etime, $start = -1, $limits = 0, $orderby = ''){
		$sql = 'select count(*) AS c, pdid FROM '.DB::table($this->_table).'
					WHERE sid = '.$sid.' 
					AND `dateline` >= '.$btime.'
					AND `dateline` <= '.$etime.'
					GROUP BY pdid 
					ORDER BY c '.$orderby;
		if($start >= 0 && $limits > 0){
			$sql .= ' LIMIT '.$start.','.$limits;
		}
		return $this->query_all(DB::query($sql));
	}
	
	// 浏览记录明细
	public function fetch_all_by_sid_btime_etime($sid, $btime, $etime, $start = 0, $limits = 20, $orderby = ''){
		$sql = 'SELECT a.*,b.nickname, c.name AS pdname FROM '.DB::table($this->_table).' AS a
					LEFT JOIN '.DB::table('common_product').' AS c ON a.pdid = c.pdid
					LEFT JOIN '.DB::table('common_member_profile').' AS b ON a.uid = b.uid
					WHERE a.sid = '.$sid.'
					AND a.dateline >= '.$btime.'
					AND a.dateline <= '.$etime.'
					ORDER BY a.dateline DESC';
		if($start >= 0 && $limits > 0){
			$sql .= ' LIMIT '.$start.','.$limits;
		}
		return $this->query_all(DB::query($sql));
	}
	
	/**
	 * @name 分组获取记录个数
	 * @param Int $btime  时间段开始时间
	 * @param Int $etime  时间段结束时间
	 * @param Array $filters  字段过滤的数组
	 * @param String $groupBy  用于 group by 的字段名
	 * @param Int $limit  限制记录数
	 * @return Array
	 */
	public function count_group_btime_etime_filters($btime, $etime, $filters, $groupBy, $limit = 0) {
		
		$sql = sprintf('SELECT `%s`, count(dateline) as `count` FROM %s WHERE dateline >= %d AND dateline<= %d',
			$groupBy, DB::table($this->_table), $btime, $etime);
		
		$filter_items = array();
		foreach ($filters as $k => $v) {
			$filter_items[] = sprintf('`%s`=%s', $k, is_string($v) ? "'$v'" : $v);
		}
		
		$sql .= ' AND ' . implode(' AND ', $filter_items);
		
		if ($groupBy) {
			$sql .= sprintf(' GROUP BY `%s`', $groupBy);
		}

		$sql .= ' order by count desc';
		
		if ($limit) {
			$sql .= ' LIMIT ' . $limit;
		}
		
		return $this->query_all(DB::query($sql));
	}

	private function query_all($query){
		$data = array();
		while($product = DB::fetch($query)){
			$data[] = $product;
		}
		return $data;
	}
}

?>