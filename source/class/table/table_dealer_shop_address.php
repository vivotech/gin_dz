<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: table_common_admincp_cmenu.php 27806 2012-02-15 03:20:46Z svn_project_zhangjie $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_dealer_shop_address extends discuz_table
{
	public function __construct() {

		$this->_table = 'dealer_shop_address';
		$this->_pk    = 'sid';

		parent::__construct();
	}

	public function fetch_by_provinceid_cityid($provinceid, $cityid){
		$sql = 'SELECT * FROM '.DB::table($this->_table).' 
				WHERE `provinceid` = '.$provinceid.' 
				AND `cityid` = '.$cityid.' 
				ORDER BY sid';
		return DB::fetch_first($sql);
	}
	
	private function query_all($query){
		$data = array();
		while($product = DB::fetch($query)){
			$data[] = $product;
		}
		return $data;
	}
}

?>