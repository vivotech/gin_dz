<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: table_common_syscache.php 31119 2012-07-18 04:21:20Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_common_product extends discuz_table
{
	
	public $po_table = 'common_product_onsell';
	public $pod_table= 'common_product_onsell_discount';
	public $ps_table = 'common_product_series';
	
	public function __construct() {
		$this->_table = 'common_product';
		$this->_pk    = 'pdid';

		parent::__construct();
	}
	
	public function fetch_all_data(){
		$sql = 'SELECT p.*, ps.series 
					FROM '.DB::table($this->_table).' p 
					LEFT JOIN '.DB::table('common_product_series').' ps ON p.psid = ps.psid
					ORDER BY p.pdid DESC';
		$query = DB::query($sql);
		return $this->query_all($query);
	}
	
	public function fetch_all_page($page = 1, $pagesize = 20, $where = '' , $order = 'p.pdid DESC'){
		$where = ( empty($where) || $where == '' ) ? '' :  ' WHERE '.$where ;
		$sql = 'SELECT p.*, ps.series 
					FROM '.DB::table($this->_table).' p 
					LEFT JOIN '.DB::table('common_product_series').' ps ON p.psid = ps.psid '.
					$where . ' ORDER BY '.$order;
		if($pagesize > 0){
			$sql .= ' LIMIT '.(($page - 1) * $pagesize).','.$pagesize;
		}
		$query = DB::query($sql);
		return $this->query_all($query);
	}
	
	public function fetch_all_by_psid($psid = 0){
		$sql = 'SELECT p.*, ps.series
					FROM '.DB::table($this->_table).' p
					LEFT JOIN '.DB::table('common_product_series').' ps ON p.psid = ps.psid
					WHERE p.psid = '.$psid.'
					ORDER BY p.pdid DESC';
		$query = DB::query($sql);
		return $this->query_all($query);
	}
  
  public function fetch_all_by_oid($oid = 0){
    $sql = 'SELECT p.*, od.*
    			FROM '.DB::table('common_member_order_detail').' AS od
    			LEFT JOIN '.DB::table('common_product_onsell').' AS po ON po.spid = od.spid
    			LEFT JOIN '.DB::table($this->_table).' AS p ON p.pdid = po.pdid
    			WHERE od.oid = '.$oid;
    return DB::fetch_all($sql);
  }
  
  public function fetch_by_pdid($pdid){
    $sql = sprintf("SELECT * FROM %s WHERE pdid=%d limit 1", DB::table($this->_table), $pdid);
    return DB::fetch_first($sql);
  }

	private function query_all($query){
		$data = array();
		while($product = DB::fetch($query)){
			$data[] = $product;
		}
		return $data;
	}
	
	/**
	 * 获取上架商品数据
	 */
	public function fetch_onsell_product_by_pdid($pid){
		$pid = is_array($pid) ? $pid : array($pid);
		$sql = "SELECT p.* , po.* FROM ".DB::table($this->_table).' AS p  '.
			   "LEFT JOIN ".DB::table($this->po_table)." AS po ON p.pdid = po.pdid ".
			   "WHERE po.closed = 0 AND p.pdid IN (".implode(",", $pid).") ORDER BY po.spid DESC ";
		$query = DB::query($sql);
		return $this->query_all($query);
	}
	
}