<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: table_common_syscache.php 31119 2012-07-18 04:21:20Z zhangguosheng $
 */

if (!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_common_member_shopcart extends discuz_table {

	public function __construct() {
		$this -> _table = 'common_member_shopcart';
		$this -> _pk = 'uid';

		parent::__construct();
	}

    public function count_all_by_uid($uid){
        return $this->count_where("where uid = '$uid'");
    }

	public function fetch_all_by_uid($uid, $orderby = 'dateline') {
		$sql = 'SELECT * FROM '.DB::table($this->_table).'
					WHERE uid = '.$uid;
		if(!empty($orderby)){
			$sql .= ' ORDER BY '.$orderby;
		}
		return $this -> query_all(DB::query($sql));
	}
	
	public function fetch_by_uid_spid($uid, $spid){
		$sql = 'SELECT * FROM '.DB::table($this->_table).'
					WHERE uid = '.$uid.'
					AND spid = '.$spid;
		return DB::fetch_first($sql);
	}
	
	public function update_num_by_uid_spid($uid, $spid, $num){
		$sql = 'UPDATE '.DB::table($this->_table).' SET amount = '.$num.'
					WHERE uid = '.$uid.'
					AND spid = '.$spid;
		return DB::query($sql);
	}
	
	public function update_by_uid_spid($uid, $spid, $arr){
		if(!is_array($arr)){
			return false;
		}
		$sql = 'UPDATE '.DB::table($this->_table).' SET ';
		$i = 0;
		foreach($arr as $k=>$v){
			if($i > 0){
				$sql .= ',';
			}
			$sql .= ' `'.$k.'` = \''.$v.'\'';
			$i++;
		}
		$sql .=' WHERE uid = '.$uid.' AND spid = '.$spid;
		return DB::query($sql);
	}

	public function count_where($where = '') {
		$sql = 'SELECT count(*) AS c FROM ' . DB::table($this -> _table) . ' ' . $where;
		return DB::result_first($sql);
	}
	
	public function delete_by_uid_spid($uid, $spid){
		$sql = 'DELETE FROM '.DB::table($this->_table).' 
					WHERE uid = '.$uid.'
					AND spid = '.$spid;
		return DB::query($sql);
	}
	
	public function delete_by_uid($uid){
		$sql = 'DELETE FROM '.DB::table($this->_table).' 
					WHERE uid = '.$uid;
		return DB::query($sql);
	}

	private function query_all($query) {
		$data = array();
		while ($product = DB::fetch($query)) {
			$data[] = $product;
		}
		return $data;
	}
}
