<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: table_common_member_security.php 27449 2012-02-01 05:32:35Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_common_member_gallery_image extends discuz_table
{
	public function __construct() {

		$this->_table = 'common_member_gallery_image';
		$this->_pk    = 'giid';
		
		parent::__construct();
	}
	
	public function fetch_all_by_uid_gid($uid, $gid, $start = 0, $limits = 15){
		if(!$uid || !$gid){
			return array();
		}
		$sql = 'SELECT * FROM '.DB::table($this->_table).' 
					WHERE uid = '.$uid.' AND gid = '.$gid.'
					ORDER BY giid DESC LIMIT '.$start.','.$limits;
		return $this->query_all(DB::query($sql));
	}
	
	public function count_by_uid_gid($uid, $gid){
		if(!$uid || !$gid){
			return 0;
		}
		$sql = 'SELECT count(giid) AS c FROM '.DB::table($this->_table).'
					WHERE uid = '.$uid.' AND gid = '.$gid;
		return DB::result_first($sql);
	}
	
	private function query_all($query){
		$data = array();
		while($product = DB::fetch($query)){
			$data[] = $product;
		}
		return $data;
	}
}