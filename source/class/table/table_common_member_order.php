<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: table_common_syscache.php 31119 2012-07-18 04:21:20Z zhangguosheng $
 */

if (!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_common_member_order extends discuz_table {

    public static $table_name = 'common_member_order';

	public function __construct() {
		$this -> _table = 'common_member_order';
		$this -> _pk = 'oid';

		parent::__construct();
	}

	public function fetch_all_page($page = 0, $pagesize = 20, $where = '' , $order = 'o.oid desc') {
		$sql = 'SELECT o.* , u.username, ua.name AS receiver, ua.province, ua.city, ua.dist, ua.community, ua.address, ua.phone ,mp.nickname
					FROM ' . DB::table($this -> _table) . ' o
					LEFT JOIN ' . DB::table('common_member') . ' u ON o.uid = u.uid
					LEFT JOIN ' . DB::table('common_member_address') . ' ua ON o.uaid = ua.uaid
					LEFT JOIN ' . DB::table('common_member_profile'). ' mp ON o.uid = mp.uid ';
		$sql .= $where;
		$sql .= ' ORDER BY '.$order;
		if ($page > 0) {
			$sql .= ' LIMIT ' . (($page - 1) * $pagesize) . ',' . $pagesize;
		}
		$query = DB::query($sql);
		return $this -> query_all($query);
	}

	/**
	 * 获取多条记录
	 * @param Array $options  查询选项
	 * $options['start']  开始的记录
	 * $options['limit']  限制查询的记录数
	 * $options['order']  排序字段
	 * $options['desc']  逆向排序
	 * $options['filters']  过滤数组，每一项以“field_name<'string'”的格式
	 * @return Array  多条记录
	 */
	public static function fetch_rows($options = array()) {
		// get params
		$start = isset($options['start']) ? $options['start'] : 0;
		$limit = isset($options['limit']) ? $options['limit'] : 0;
		$order = isset($options['order']) ? $options['order'] : '';
		$desc = isset($options['desc']) ? $options['desc'] : false;
		$filters = isset($options['filters']) ? $options['filters'] : array();

		// set sql
		$table = new self;
		$sql = 'SELECT * FROM ' . DB::table($table -> _table);
		if (count($filters)) {
			$sql .= ' WHERE ' . implode(' AND ', $filters);
		}
		if ($order) {
			$sql .= ' ORDER BY ' . $order;
			if ($desc) {
				$sql .= ' DESC';
			}
		}
		if ($limit) {
			$sql .= ' LIMIT ' . $start . ',' . $limit;
		}

		// fetch
		return DB::fetch_all($sql);
	}

	/**
	 * 获取多条记录的行数
	 * @param type $filters  过滤数组，每一项以“field_name<'string'”的格式
	 * @return Int  行数
	 * 复杂的条件查询，可使用 count_where 函数。
	 */
	public static function count_rows($filters = array()) {
		$table = new self;
		$sql = 'SELECT count(' . $table -> _pk . ') as count FROM ' . DB::table($table -> _table);

		if (count($filters)) {
			$sql .= ' WHERE ' . implode(' AND ', $filters);
		}

		$row = DB::fetch_first($sql);

		if ($row) {
			return $row['count'];
		}

		return false;
	}

	/**
	 * get table rows by keywords  通过关键词获取表的多条记录
	 * @param $keywords String  关键词
	 * @param $filters Array  过滤数组
	 * @return Array
	 */
	public static function fetch_rows_by_keywords($keywords, $filters = array()) {
		// create new table object
		$table = new self;

		// set select sql string
		$sql = sprintf("select oid from %s where ", DB::table($table -> _table));

		// set sql where string
		$where = '';
		foreach ($filters as $k => $v) {
			if ($where) {
				$where .= ' AND ';
			}
			$where .= $k . '=';
			if (is_string($v)) {
				$where .= sprintf("'%s'", $v);
			} else {
				$where .= $v;
			}
		}
		$sql .= $where;

		$sql .= ' AND ( ';

		// find order by id
		if ((int)$keywords > 0) {
			$sql .= 'o_openid=' . $keywords . ' OR ';
		}

		// find order by product name
		$sql .= "oid in (
		    select oid from " . DB::table('common_member_order_detail') . "
		    where spid in (
		        select spid from " . DB::table('common_product_onsell') . "
		        where pdid in (
		            select pdid from " . DB::table('common_product') . "
		            where name like '%" . $keywords . "%'
		            )
		        )
		    )";

		// end where "and" string
		$sql .= ')';

		// Discuz DB 类不允许子查询方式
		$result = mysql_query($sql);

		if ($result) {
			$rows = array();
			while ($row = mysql_fetch_assoc($result)) {
				$rows[] = $row;
			}
			return $rows;
		} else {
			return array();
		}
	}

	public function count_where($where = '') {
		$sql = 'SELECT count(*) AS c FROM ' . DB::table($this -> _table) . ' ' . $where;
		return DB::result_first($sql);
	}

	public function fetch_by_oid($oid, $uid = 0) {
		return DB::fetch_first("SELECT * FROM %t WHERE oid=%d AND uid=%d limit 1", array($this -> _table, $oid, $uid));
	}
	
	public function fetch_by_openid($openid, $uid = 0) {
		return DB::fetch_first("SELECT * FROM %t WHERE o_openid=%d AND uid=%d limit 1", array($this -> _table, $openid, $uid));
	}
	
	public function update_paystate_by_oid($oid,$paystate=0){
		$data = array('paystate'=>$paystate);
		return DB::update($this->_table, $data, array($this -> _pk => intval($oid)));
	}
	
	/**
	 * @name 获取订单状态的where条件
	 */
	public static function get_state_where_condition($statetype, $tableAlias = ''){
//		$allow_statetype = array(
//			'wait',
//			'waitpay',
//			'waitdelivery',
//			'delivering',
//			'finished'
//		);
        $allow_statetype = array(
			'unfinished',
			'finished',
			'cancle'
		);
		$tableAlias = $tableAlias ? $tableAlias.'.' : '';
		$str = '';
		if(in_array($statetype, $allow_statetype)){
//			switch ($statetype){
//				case 'wait':
//					$str .= ' ('.$tableAlias.'state >= 0 AND '.$tableAlias.'state < 6)';
//					break;
//				case 'waitpay':
//					$str .= ' ('.$tableAlias.'payment > 0 AND '.$tableAlias.'paystate = 0)';
//					break;
//				case 'waitdelivery':
//					//未付款定义：货到付款订单 或者 已经完成在线支付的订单
//					$str .= ' ('.$tableAlias.'state >= 0 AND '.$tableAlias.'state <= 3 AND ( ( payment > 0 AND paystate = 1 ) OR payment = 0 ))';
//					break;
//				case 'delivering':
//					$str .= ' ('.$tableAlias.'state >= 4 AND '.$tableAlias.'state <= 5)';
//					break;
//				case 'finished':
//					$str .= ' ('.$tableAlias.'state = 6)';
//					break;
//				default:
//					$str = '';
//			}
            switch($statetype){
                case 'unfinished':
                    $str .= ' ('.$tableAlias.'state >= 0 AND '.$tableAlias.'state < 6)';
                    break;
                case 'finished':
					$str .= ' ('.$tableAlias.'state = 6)';
                    break;
                case 'cancle':
                    $str .= ' ('.$tableAlias.'state = -2)';
                    break;
                default:
                    $str = '';
            }
		}
		
		return $str;
	}

	private function query_all($query) {
		$data = array();
		while ($product = DB::fetch($query)) {
			$data[] = $product;
		}
		return $data;
	}

}
