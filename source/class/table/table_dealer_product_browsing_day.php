<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: table_common_admincp_cmenu.php 27806 2012-02-15 03:20:46Z svn_project_zhangjie $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_dealer_product_browsing_day extends discuz_table
{
	public function __construct() {
		$this->_table = 'dealer_product_browsing_day';
		$this->_pk    = 'sid';

		parent::__construct();
	}

	/**
	 * @name 通过店铺ID和时间戳获取记录
	 * @param Int $sid  店铺ID
	 * @param Int $dateline  日期线
	 */
	public function fetch_by_sid_timeline($sid, $dateline){
		$sql = 'SELECT * FROM '.DB::table($this->_table).'
					WHERE `sid` = '.$sid.'
					AND `dateline` = \''.$dateline.'\'';
		return DB::fetch_first($sql);
	}
	
	public function fetch_all_by_sid_timeline($sid, $dateline, $orderby = 'rate DESC'){
		$sql = 'SELECT * FROM '.DB::table($this->_table).'
					WHERE `sid` = '.$sid.'
					AND `dateline` = \''.$dateline.'\'';
		if(!empty($orderby)){
			$sql .= ' ORDER BY '.$orderby;
		}
		return $this->query_all(DB::query($sql));
	}
	
	public function delete_by_sid_timeline($sid, $dateline){
		$sql = 'DELETE FROM '.DB::table($this->_table).'
					WHERE `sid` = '.$sid.'
					AND `dateline` = \''.$dateline.'\'';
		return DB::query($sql);
	}
	
	private function query_all($query){
		$data = array();
		while($product = DB::fetch($query)){
			$data[] = $product;
		}
		return $data;
	}
}

?>