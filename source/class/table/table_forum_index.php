<?php
	
	/**
	 * 首页设置
	 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_forum_index extends discuz_table {
	
	private $types = array();
	
	public function __construct() {

		$this->_table = 'forum_index';
		$this->_pk    = 'id';
		$this->types = array(
				'row1_left',	//第一排  左
				'row1_right',	//第一排  右
				'row2_left',	//第二排  左
				'row2_right',	//第二排  右
				'row3',			//第三排
				'row4',			//第四排
				'row5',			//第五排
			);
		parent::__construct();
	}
	
	public function fetch_list_row_by_type($type){
//		$type = in_array($type, $this->types) ? $type : 'row1_left';
		return DB::fetch_all("SELECT * FROM ".DB::table($this->_table)." WHERE type = '{$type}' ORDER BY displayorder DESC ");
	}
	
	public function fetch_list($thread_forum = false , $thread_fields = array()){
		//是否需要查询thread表
		if($thread_forum){
			if(is_array($thread_fields) && !empty($thread_fields)){
				foreach($thread_fields as $k=>$v){
					if(!is_string($v)){
						unset($thread_fields[$k]);
					}else{
						$thread_fields[$k] = 't.'.$v;
					}
				}
				$thread_fields = implode(',', $thread_fields);
			}else{
				$thread_fields = 't.*';
			}
			$sql = "SELECT i.*,{$thread_fields} FROM ".DB::table($this->_table)." AS i LEFT JOIN ".DB::table('forum_thread')." AS t ON i.tid = t.tid ORDER BY i.displayorder DESC";
		}else{
			$sql = "SELECT * FROM ".DB::table($this->_table)." ORDER BY displayorder DESC";
		}
		$res = DB::fetch_all($sql);
		$list = array(); 
		foreach($res as $k=>$item){
			$list[$item['type']][] = $item;
		}
		return $list;
	}
	
	public function insert($data)
	{
	    return DB::insert($this->_table, $data);
	}
	
	public function update($data, $condition = '')
	{
		return DB::update($this->_table, $data, $condition);
	}
	
}
