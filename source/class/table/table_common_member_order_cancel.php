<?php


/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: table_common_syscache.php 31119 2012-07-18 04:21:20Z zhangguosheng $
 */

if (!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_common_member_order_cancel extends discuz_table {
			

	public function __construct() {
		$this -> _table = 'common_member_order_cancel';
		$this -> _pk = 'ocid';

		parent::__construct();
	}
	
	public function fetch_all_page($page = 0, $pagesize = 20, $where = '' , $order = 'oc.ocid') {
		$sql = 'SELECT oc.*, o.o_openid, o.totalprice, o.state AS ostate , u.username, ua.name AS receiver, ua.address, ua.phone
					FROM ' . DB::table($this -> _table) . ' oc
					LEFT JOIN '.DB::table('common_member_order').' o ON oc.oid = o.oid
					LEFT JOIN ' . DB::table('common_member') . ' u ON o.uid = u.uid
					LEFT JOIN ' . DB::table('common_member_address') . ' ua ON o.uaid = ua.uaid';
		$sql .= $where;
		$sql .= ' ORDER BY '.$order;
		if ($page > 0) {
			$sql .= ' LIMIT ' . (($page - 1) * $pagesize) . ',' . $pagesize;
		}
		$query = DB::query($sql);
		return $this -> query_all($query);
	}

	public function count_where($where = '') {
		$sql = 'SELECT count(*) AS c FROM ' . DB::table($this -> _table) . ' ' . $where;
		return DB::result_first($sql);
	}

	private function query_all($query) {
		$data = array();
		while ($product = DB::fetch($query)) {
			$data[] = $product;
		}
		return $data;
	}

	public function fetch_by_oid_uid($oid, $uid) {
		return DB::fetch_first("SELECT * FROM %t WHERE oid=%d AND uid=%d limit 1", array($this -> _table, $oid, $uid));
	}
	
	public function fetch_by_oid($oid, $uid = 0) {
		return DB::fetch_first("SELECT * FROM %t WHERE oid=%d AND uid=%d limit 1", array($this -> _table, $oid, $uid));
	}
	
}

