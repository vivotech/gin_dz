<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_member_finish_beginner_guide extends discuz_table
{
	public static $available_guide_page = array(
		'home',                 // 首页
        'mobile_viewthread'     // 手机版主题查看页
	);
	
	public static function guide_page_is_available($guide_page) {
		if (in_array($guide_page, self::$available_guide_page)) {
			
			return true;
		}
		return false;
	}
	
	public function __construct() {

		$this->_table = 'member_finish_beginner_guide';
		$this->_pk    = 'mfbgid';

		parent::__construct();
	}
	
	public function fetch_by_uid($uid) {
		return DB::fetch_first("SELECT * FROM %t WHERE uid=%d",
		  array($this->_table, $uid));
	}
	
	
}

