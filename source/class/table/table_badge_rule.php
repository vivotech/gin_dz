<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_badge_rule extends discuz_table
{
	public function __construct() {

		$this->_table = 'badge_rule';
		$this->_pk    = 'badge';

		parent::__construct();
	}
	
	public function fetch_all_data() {
		return DB::fetch_all('SELECT * FROM %t ORDER BY displayorder', array($this->_table));
	}
	
	
}





