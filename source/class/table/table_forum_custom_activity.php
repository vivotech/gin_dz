<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: table_forum_forum.php 33548 2013-07-04 08:19:27Z laoguozhang $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_forum_custom_activity extends discuz_table
{
	public function __construct() {

		$this->_table = 'forum_custom_activity';
		$this->_pk    = 'caid';
		parent::__construct();
	}
	
	public function fetch_by_caid($caid){
		return DB::fetch_first('SELECT * FROM %t WHERE caid=%d', array($this->_table, $caid));
	}
	
	public function fetch_by_fid($fid){
		return DB::fetch_first('SELECT * FROM %t WHERE fid=%d', array($this->_table, $fid));
	}
	
	public function fetch_all_by_fid($fids){
		if(!is_array($fids)){
			$fids = array($fids);
		}
		return DB::fetch_all('SELECT * FROM '.DB::table($this->_table).' WHERE '.DB::field('fid', $fids));
	}
	
	public function fetch_all_activity($where = '', $limit = 0, $start = 0){
		$sql = 'SELECT * FROM '.DB::table($this->_table);
		if($where){
			$sql .= $where;
		}
		$sql .= ' ORDER BY caid DESC';
		if($limit > 0){
			$sql .= ' LIMIT '.$start.','.$limit;
		}
		return $this->query_all(DB::query($sql));
	}
	
	public function delete_by_fid($fid){
		$fid = intval($fid);
		$sql = 'DELETE FROM '.DB::table($this->_table).' WHERE fid = '.$fid;
		return DB::query($sql);
	}
	
	private function query_all($query){
		$data = array();
		while($product = DB::fetch($query)){
			$data[] = $product;
		}
		return $data;
	}
}