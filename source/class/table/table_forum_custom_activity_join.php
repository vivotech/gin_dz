<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: table_forum_forum.php 33548 2013-07-04 08:19:27Z laoguozhang $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_forum_custom_activity_join extends discuz_table
{
	public function __construct() {

		$this->_table = 'forum_custom_activity_join';
		$this->_pk    = 'acjid';

		parent::__construct();
	}
	
	public function fetch_all_by_caid($caid){
		$caid = $caid ? intval($caid) : 0;
		$sql = 'SELECT * FROM '.DB::table($this->_table).' WHERE caid = '.$caid;
		return $this->query_all(DB::query($sql));
	}
	
	public function fetch_by_caid_uid($caid,$uid){
		return DB::fetch_first("SELECT * FROM %t WHERE caid=%d AND uid=%d", array($this->_table, intval($caid), intval($uid)));
	}
	
	/**
	 * 获取用户参加的所有活动的记录
	 * @param Int $uid  user ID
	 * @return Array
	 */
	public function fetch_all_by_uid($uid){
		$sql = 'SELECT * FROM '.DB::table($this->_table).' WHERE uid = '.$uid;
		return $this->query_all(DB::query($sql));
	}
	
	private function query_all($query){
		$data = array();
		while($product = DB::fetch($query)){
			$data[] = $product;
		}
		return $data;
	}
}