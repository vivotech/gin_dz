<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: table_common_syscache.php 31119 2012-07-18 04:21:20Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_common_member_order_detail extends discuz_table
{

	
	public function __construct() {
		$this->_table = 'common_member_order_detail';
		$this->_pk    = 'odid';

		parent::__construct();
	}
  
  public function fetch_all_by_oid($oid)
  {
  	global $_G;
	if(!$_G['cache']['product']){
		loadcache('product');
	}
  	$sql = 'SELECT a.* , b.pdid, b.subject
  				FROM '.DB::table($this->_table).' a
  				LEFT JOIN '.DB::table('common_product_onsell').' b ON a.spid = b.spid
  				WHERE a.oid = '.$oid;
    $order_detail_list = DB::fetch_all($sql);
	if(count($order_detail_list) > 0){
		foreach($order_detail_list as $k=>$l){
			$order_detail_list[$k]['product'] = $_G['cache']['product'][$l['pdid']];
		}
	}
	return $order_detail_list;
  }

    public function fetch_all_by_oids($oids)
    {
        $sql = sprintf("select * from %s where oid in (%s)", $this->_table,
            is_array($oids) ? implode(',', $oids) : $oids);

        $rows = DB::fetch_all($sql);
        return $rows;
    }
	
	public function fetch_num_by_spid($spid, $uid = 0){
		$spid = intval($spid);
		$uid = intval($uid);
		if($spid > 0 && $uid > 0){
			$sql = 'SELECT sum(a.amount) AS s
						FROM '.DB::table($this->_table).' a
						LEFT JOIN '.DB::table('common_member_order').' b ON a.oid = b.oid
						WHERE a.spid = '.$spid.'
						AND b.uid = '.$uid;
			$sum = DB::result_first($sql);
			return $sum;
		}
	}
}