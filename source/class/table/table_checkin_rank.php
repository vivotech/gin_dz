<?php
/**
 * Created by PhpStorm.
 * User: wen
 * Date: 2015/7/22
 * Time: 17:50
 */

if(!defined('IN_DISCUZ')) {
    exit('Access Denied');
}

require_once DISCUZ_ROOT."/source/plugin/gsignin/table/table_gsignin_member.php";

class table_checkin_rank extends discuz_table
{

    public static $table_name = 'checkin_rank';

    function __construct() {

        $this->_table = 'checkin_rank';
        $this->_pk    = 'uid';

        parent::__construct();
    }

    public function fetch_all_data() {
        return DB::fetch_all('SELECT * FROM %t ORDER BY displayorder', array($this->_table));
    }

    public function fetch_by_uid($uid) {
        return DB::fetch_first('SELECT * FROM %t WHERE uid=%d', array($this->_table, $uid));
    }

    public function getUpdateTime()
    {
        $sql = sprintf("select UPDATE_TIME from information_schema.tables where TABLE_NAME = '%s';", DB::table($this->_table));

        return DB::fetch_all($sql);
    }

    public function update_all_data()
    {
        $sql = sprintf("DELETE FROM %s", DB::table($this->_table));
        DB::query($sql);

        $data = C::t('gsignin_member')->fetch_rows('uid', '', 'total desc, lasttime');

        $rank_index = 1;
        foreach ($data as $v)
        {
            C::t('checkin_rank')->insert(array('uid'   => $v['uid'], 'rank'  => $rank_index));
            $rank_index++;
        }

    }

}

