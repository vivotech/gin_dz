<?php

/**
 * 微信规则
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}


class table_wechat_rule_keyword extends discuz_table{
	
	public function __construct() {

		$this->_table = 'wechat_rule_keyword';
		$this->_pk    = 'id';
		parent::__construct();
		
	}
	
	
}
