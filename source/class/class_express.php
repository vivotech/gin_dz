<?php

/**
 * 快递类
 */
if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

Class Express{
	static private $express = NULL;
	private $aicha_id = 0;
	private $aicha_secret_key = '';
	private $error = '';

    static private $error_code = array(
        //0无错误，1单号不存在，2验证码错误，3链接查询服务器失败，4程序内部错误，5程序执行错误，6快递单号格式错误，7快递公司错误，10未知错误，20API错误，21API被禁用，22API查询量耗尽
        0 => '无错误',
        1 => '单号不存在',
        2 => '验证码错误',
        3 => '链接查询服务器失败',
        4 => '程序内部错误',
        5 => '程序执行错误',
        6 => '快递单号格式错误',
        7 => '快递公司错误',
        10 => '未知错误',
        20 => 'API错误',
        21 => 'API被禁用',
        22 => 'API查询量耗尽'
    );

	private function __construct(){
		$app = C::app();
		
		$this->aicha_id = $app->config['express']['aicha_id'];
		$this->aicha_secret_key = $app->config['express']['aicha_secret_key'];
		
		return $this;
	}
	
	/**
	 * @name 查询快递
	 * @param $express_com string 快递公司代号
	 * @param $express_num string 快递号
	 * @return int 当前的状态
	 */
	static public function query_express($express_com, $express_num){
		if(empty(self::$express)){
			self::$express = new Express();
		}
        $pre_key = 'express_'.$express_com.'_';
        // 缓存
        if($data = memory('get', $express_num, $pre_key)){
            return $data;
        }
        $res = json_decode(self::$express->send_aicha_request($express_com, $express_num), 1);
        //如果获取成功，缓存
        if($res['errCode']==0){
            memory('set', $express_num, $res, 60*60*1, $pre_key);
        }
        self::$express->error = $res['errCode'];
        return $res;
	}
	
	/**
	 * @name 获取错误信息
	 */
	static public function get_errmsg(){
		return self::$error_code[self::$express->error];
	}
	
	/**
	 * @name 获取错误信息
	 */
	static public function get_error(){
		if(empty(self::$express)){
			return 0;
		}else{
			return self::$express->error;
		}
	}
	
	private function send_aicha_request($express_com, $express_num){
		$id = self::$express->aicha_id;
		$secret_key = self::$express->aicha_secret_key;
		$com = $express_com;
		$nu = $express_num;
		$type = 'json';
		$encode = 'utf8';
		$ord = 'asc';
		$gateway = sprintf('http://api.ickd.cn/?id=%s&secret=%s&com=%s&nu=%s&type=%s&encode=%s&ord=%s',
							$id,
							$secret_key,
							$com,
							$nu,
							$type,
							$encode,
							$ord);
		$ch = curl_init($gateway);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);
		$resp = curl_exec($ch);
		$errmsg = curl_error($ch);
        if($errmsg){
            exit($errmsg);
        }
		curl_close($ch);
		return $resp;
	}

}

/**
 * @name 快递公司代码
 * 
 * 快递公司	代码
	EMS快递	ems
	申通快递	shentong
	圆通快递	yuantong
	中通快递	zhongtong
	汇通快递	huitong
	天天快递	tiantian
	韵达快递	yunda
	顺丰快递	shunfeng
	宅急送快递	zhaijisong
	CCES快递	cces
	中国邮政平邮	pingyou
	星晨急便	xingchen
	速尔快递	sure
	快捷快递	kuaijie
	中铁快运	zhongtie
	AAE快递	aae
	安捷快递	anjie
	安信达快递	anxinda
	程光快递	chengguang
	大田物流	datian
	德邦物流	debang
	DHL快递	dhl
	DPEX快递	dpex
	D速快递	dsu
	国际Fedex	fedex
	Fedex国内	fedexcn
	原飞航物流	feihang
	丰达快递	fengda
	飞康达快递	fkd
	飞快达快递	fkdex
	天地华宇物流	huayu
	佳吉快运	jiaji
	佳怡物流	jiayi
	加运美快递	jiayunmei
	晋越快递	jinyue
	联昊通物流|快递	lianhaotong
	龙邦快递	longbang
	民航快递	minhang
	港中能达	nengda
	OCS快递	ocs
	全晨快递	quanchen
	全峰快递	quanfeng
	全日通快递	quanritong
	全一快递	quanyi
	如风达快递	rufeng
	盛丰物流	shengfeng
	盛辉物流	shenghui
	TNT快递	tnt
	UPS	ups
	万家物流	wanjia
	新邦物流	xinbang
	鑫飞鸿速递	xinfeihong
	信丰快递	xinfeng
	亚风快递	yafeng
	一邦快递	yibang
	优速快递	yousu
	远成物流	yuancheng
	元智捷诚	yuanzhi
	越丰快递	yuefeng
	运通中港快递	yuntong
	源伟丰	ywfex
	中邮物流	zhongyou
 */