<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class bubble_wine_badge_rule extends badge_rule {
	
	public function canObtain($uid) {

        $product_amount = getBuyedProductSerieCount($uid, '气泡酒');

        $can_obtain = $this->getCanObtainLevel($product_amount, $uid);

        return $can_obtain;
	}
	
}


