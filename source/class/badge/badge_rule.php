<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

$ds = DIRECTORY_SEPARATOR;

require_once libfile('function/badge');

require_once dirname(__FILE__) . $ds . 'checkin_badge_rule.php';
require_once dirname(__FILE__) . $ds . 'share_badge_rule.php';

abstract class badge_rule {
	
	public $badge = '';

    public static $badge_values = array(
        'checkin',      // 签到达人
        'bubble_wine',  // 气泡酒达人
        'share',        // 分享达人
        'comment',      // 吐槽达人
        'favorite',     // 收藏达人
        'finishtask',   // 任务达人
        'saygood'       // 点赞达人
    );
	
	public $name = '';
	
	public $displayorder = 1;
	
	public $desc = '';
	
	public $lv1 = '';
	
	public $lv1_limit = 0;
	
	public $lv1_image = '';
	
	public $lv2 = '';
	
	public $lv2_limit = 0;
	
	public $lv2_image = '';
	
	public $lv3 = '';
	
	public $lv3_limit = 0;
	
	public $lv3_image = '';
	
	public $lv4 = '';
	
	public $lv4_limit = 0;
	
	public $lv4_image = '';
	
	public $lv5 = '';
	
	public $lv5_limit = 0;
	
	public $lv5_image = '';
	
	public $lv6 = '';
	
	public $lv6_limit = 0;
	
	public $lv6_image = '';
	
	public $lv7 = '';
	
	public $lv7_limit = 0;
	
	public $lv7_image = '';
	
	static public $badge_list = array(
		'share', 'bubble_wine', 'checkin',
		'comment', 'favorite', 'finishtask'
	);
	
	static public $instances = array();
	
	function __construct($options) {
		foreach ($options as $k => $v) {
			if (isset($this->$k)) {
				$this->$k = $v;
			}
		}
	}
	
	/**
	 * @name 获取类实例
	 * @param String  $type 徽章类型
	 */
	static public function getInstance($type) {
		
		// 不支持的徽章类型
		if (!in_array($type, self::$badge_list)) {
			return false;
		}
		
		// 实例存在直接返回
		if ( isset(self::$instances[$type]) ) {
			return self::$instances[$type];
		}
		
		// 获取徽章记录数据
		$data = getBadgeById($type);
		
		// 没有任何记录
		if (!$data) {
			return null;
		}
		
		$class_name = $type.'_badge_rule';
		$ins = new $class_name($data);
		
		self::$instances[$type] = $ins;
		
		return $ins;
	}
	
	/**
	 * @name 检查是否可以获取该徽章
	 * @param Int  $uid 用户 ID
	 */
	abstract public function canObtain($uid);
	
	/**
	 * @name 获取徽章
	 * @param Int  $uid 用户 ID
	 */
	public function obtain($uid) {
		
		$obtain_level = $this->canObtain($uid);
		if ( !$obtain_level ) {
			return false;
		}
		
		// 获取用户获得徽章记录
		$badge_member = C::t('badge_member')->fetch_by_badge_uid($this->badge, $uid);
		
		if ( $badge_member ) {
			$opt = C::t('badge_member')->update($badge_member['bmid'], array(
				'lv'	=> $obtain_level,
				'lv'.$obtain_level.'_dateline'	=> time()
			));
		} else {
			$opt = C::t('badge_member')->insert(array(
				'uid'	=> $uid,
				'badge'	=> $this->badge,
				'lv'	=> $obtain_level,
				'lv'.$obtain_level.'_dateline'	=> time()
			));
		}
		
		if ($opt) {
			return true;
		} else {
			return false;
		}
		
	}

    /**
     * @name 获取可以获得的徽章级别
     * @param Number    $value 数字值
     */
    public function getCanObtainLevel($value, $uid) {

        $checkin_level = 0;
        for ($i=1; $i<=7; $i++) {
            $lv_attr = 'lv'.$i.'_limit';
            $lv_limit = $this->$lv_attr;
            if ( $lv_limit && $value >= $lv_limit ) {
                $checkin_level = $i;
            }
        }

        if (!$checkin_level) {		// 还没有到达最低的条件
            return false;
        } else {
            // 获取用户获得徽章记录
            $badge_member = C::t('badge_member')->fetch_by_badge_uid($this->badge, $uid);

            if ( $badge_member && $checkin_level <= $badge_member['lv'] ) {		// 还是原来那一个级别
                return false;
            }

            return $checkin_level;		// 可以获得

        }

        return $checkin_level;
    }
	
}


