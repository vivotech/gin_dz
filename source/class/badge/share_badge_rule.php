<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class share_badge_rule extends badge_rule {
	
	public function canObtain($uid) {
		
		$share_count = C::t("forum_thread")->count_by_authorid($uid);

        // 检查用户可以获得的徽章级别
        $can_obtain_level = getMatchLevel($share_count);

        return $can_obtain_level;
	}
	
}
