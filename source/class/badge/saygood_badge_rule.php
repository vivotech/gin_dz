<?php
/**
 * Created by PhpStorm.
 * User: wen
 * Date: 2015/6/29
 * Time: 14:58
 */

class saygood_badge_rule extends badge_rule {

    /**
     * @param Int $uid
     * @return bool
     */
    public function canObtain($uid) {

        $saygood_total = getSaygoodTotal($uid, $type = 'all');

        // 检查用户可以获得的徽章级别
        $can_obtain_level = getMatchLevel($saygood_total);

        return $can_obtain_level;
    }

}

