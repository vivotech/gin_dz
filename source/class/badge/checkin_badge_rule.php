<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

require_once DISCUZ_ROOT."/source/plugin/gsignin/table/table_gsignin_member.php";
require_once DISCUZ_ROOT."/source/plugin/gsignin/function/function.php";

class checkin_badge_rule extends badge_rule {
	
	public function canObtain($uid) {
		
		// 获取签到总数
		$gsignin_member = C::t('gsignin_member')->fetch_by_uid($uid);
		if (!$gsignin_member) {
			return false;
		}
		$checkin_total = $gsignin_member['total'];

        // 检查用户可以获得的徽章级别
        $can_obtain_level = $this->getCanObtainLevel($checkin_total, $uid);

        return $can_obtain_level;
	}
	
}
