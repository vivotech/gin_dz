<?php
/**
 * Created by PhpStorm.
 * User: wen
 * Date: 2015/6/29
 * Time: 12:04
 */

if(!defined('IN_DISCUZ')) {
    exit('Access Denied');
}



class comment_badge_rule extends badge_rule {

    /**
     * @param Int $uid
     * @return bool
     */
    public function canObtain($uid) {

        $comment_count = table_forum_post::count_rows(array(
            'first' => 0,
            'authorid'  => $uid
        ));

        // 检查用户可以获得的徽章级别
        $can_obtain_level = getMatchLevel($comment_count);

        return $can_obtain_level;
    }

}

