<?php

/**
 * APP认证类
 */

if (!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class app_logging_ctl {
	//数据返回类型
	public $returntype = 'JSON';
	
	function logging_ctl() {
		require_once   libfile('function/misc');
		loaducenter();
	}

	function logging_more($questionexist, $secchecklogin2 = 0) {
		global $_G;
		if (empty($_GET['lssubmit'])) {
			return;
		}
		$auth = authcode($_GET['username'] . "\t" . $_GET['password'] . "\t" . ($questionexist ? 1 : 0), 'ENCODE', $_G['config']['security']['authkey']);
		$js = '<script type="text/javascript">showWindow(\'login\', \'member.php?mod=logging&action=login&auth=' . rawurlencode($auth) . '&referer=' . rawurlencode(dreferer()) . (!empty($_GET['cookietime']) ? '&cookietime=1' : '') . '\')</script>';
		showmessage('location_login', '', array('type' => 1), array('extrajs' => $js));
	}

	//用户登陆
	function on_login() {
		global $_G;
		
		//已登陆处理
		if ($_G['uid']) {
			$referer = dreferer();
			$ucsynlogin = $this -> setting['allowsynlogin'] ? uc_user_synlogin($_G['uid']) : '';
			$param = array('username' => $_G['member']['username'], 'usergroup' => $_G['group']['grouptitle'], 'uid' => $_G['member']['uid'] ,'location'=>$referer ? $referer : './');
			return sendAppMessage(RES_YES,'app_login_succeed', $param, $this->returntype);
		}
		
		//验证码处理，APP不要了
//		list($seccodecheck) = seccheck('login');
//		
//		if (!empty($_GET['auth'])) {
//			$dauth = authcode($_GET['auth'], 'DECODE', $_G['config']['security']['authkey']);
//			list(, , , $secchecklogin2) = explode("\t", $dauth);
//			if ($secchecklogin2) {
//				$seccodecheck = true;
//			}
//		}
//		$seccodestatus = !empty($_GET['lssubmit']) ? false : $seccodecheck;

		//邀请人信息
		$invite = getinvite();

		if (!empty($_GET['auth'])) {
			list($_GET['username'], $_GET['password']) = daddslashes(explode("\t", authcode($_GET['auth'], 'DECODE', $_G['config']['security']['authkey'])));
		}
		
		//登陆验证码
		$loginhash = !empty($_GET['loginhash']) && preg_match('/^\w+$/', $_GET['loginhash']) ? $_GET['loginhash'] : '';
		
		//登陆多次测试
        /*
		if (!($_G['member_loginperm'] = logincheck($_GET['username']))) {
			captcha::report($_G['clientip']);
			return sendAppMessage(RES_NO, 'login_strike',null,$this->returntype);
		}*/
		
		if ($_GET['fastloginfield']) {
			$_GET['loginfield'] = $_GET['fastloginfield'];
		}
		$_G['uid'] = $_G['member']['uid'] = 0;
		$_G['username'] = $_G['member']['username'] = $_G['member']['password'] = '';
		if (!$_GET['password'] || $_GET['password'] != addslashes($_GET['password'])) {
			return sendAppMessage(RES_NO, 'profile_passwd_illegal',null,$this->returntype);
		}

		//登陆方法
		$result = userlogin($_GET['username'], $_GET['password'], $_GET['questionid'], $_GET['answer'], $this -> setting['autoidselect'] ? 'auto' : $_GET['loginfield'], $_G['clientip']);
		$uid = $result['ucresult']['uid'];

//		if (!empty($_GET['lssubmit']) && ($result['ucresult']['uid'] == -3 || $seccodecheck)) {
		if (!empty($_GET['lssubmit']) && ($result['ucresult']['uid'] == -3)) {
			$_GET['username'] = $result['ucresult']['username'];
			$this -> logging_more($result['ucresult']['uid'] == -3);
		}

		//账号未激活状态处理
		if ($result['status'] == -1) {
			//是否开启快速启动
//			if (!$this -> setting['fastactivation']) {
//				//未开启快速激活，需要客户去
//				$auth = authcode($result['ucresult']['username'] . "\t" . FORMHASH, 'ENCODE');
//				showmessage('location_activation', 'member.php?mod=' . $this -> setting['regname'] . '&action=activation&auth=' . rawurlencode($auth) . '&referer=' . rawurlencode(dreferer()), array(), array('location' => true));
//			} else {
				//已经开启快速激活
				$init_arr = explode(',', $this -> setting['initcredits']);
				$groupid = $this -> setting['regverify'] ? 8 : $this -> setting['newusergroupid'];

				C::t('common_member') -> insert($uid, $result['ucresult']['username'], md5(random(10)), $result['ucresult']['email'], $_G['clientip'], $groupid, $init_arr);
				$result['member'] = getuserbyuid($uid);
				$result['status'] = 1;
//			}
		}

		//账号正常
		if ($result['status'] > 0) {

			if ($this -> extrafile && file_exists($this -> extrafile)) {
				require_once $this -> extrafile;
			}
			//设置登陆信息
			//记录 cookies / session / 检查积分
			$credits = setloginstatus($result['member'], $_GET['cookietime'] ? 2592000 : 0);
			//检查是否允许关注？？
			checkfollowfeed();

			//用户组强制登陆，如QQ，邮箱等
			if ($_G['group']['forcelogin']) {
				if ($_G['group']['forcelogin'] == 1) {
					clearcookies();
					return sendAppMessage(RES_NO, 'location_login_force_qq',null,$this->returntype);
				} elseif ($_G['group']['forcelogin'] == 2 && $_GET['loginfield'] != 'email') {
					clearcookies();
					return sendAppMessage(RES_NO, 'location_login_force_mail',null,$this->returntype);
				}
			}

			//记录登陆时间
			if ($_G['member']['lastip'] && $_G['member']['lastvisit']) {
				dsetcookie('lip', $_G['member']['lastip'] . ',' . $_G['member']['lastvisit']);
			}
			//将登陆时间写入数据库
			C::t('common_member_status') -> update($_G['uid'], array('lastip' => $_G['clientip'], 'port' => $_G['remoteport'], 'lastvisit' => TIMESTAMP, 'lastactivity' => TIMESTAMP));

			$ucsynlogin = $this -> setting['allowsynlogin'] ? uc_user_synlogin($_G['uid']) : '';

			$pwold = false;
			if ($this -> setting['strongpw'] && !$this -> setting['pwdsafety']) {
				if (in_array(1, $this -> setting['strongpw']) && !preg_match("/\d+/", $_GET['password'])) {
					$pwold = true;
				}
				if (in_array(2, $this -> setting['strongpw']) && !preg_match("/[a-z]+/", $_GET['password'])) {
					$pwold = true;
				}
				if (in_array(3, $this -> setting['strongpw']) && !preg_match("/[A-Z]+/", $_GET['password'])) {
					$pwold = true;
				}
				if (in_array(4, $this -> setting['strongpw']) && !preg_match("/[^a-zA-z0-9]+/", $_GET['password'])) {
					$pwold = true;
				}
			}

			//如果不是超级管理员组别
			if ($_G['member']['adminid'] != 1) {
				if ($this -> setting['accountguard']['loginoutofdate'] && $_G['member']['lastvisit'] && TIMESTAMP - $_G['member']['lastvisit'] > 90 * 86400) {
					C::t('common_member') -> update($_G['uid'], array('freeze' => 2));
					C::t('common_member_validate') -> insert(array('uid' => $_G['uid'], 'submitdate' => TIMESTAMP, 'moddate' => 0, 'admin' => '', 'submittimes' => 1, 'status' => 0, 'message' => '', 'remark' => '', ), false, true);
					manage_addnotify('verifyuser');
					//您当前的帐号已经太长时间未登录网站已经被冻结，必须验证邮箱后才能解除冻结状态
					//TODO 接口中暂时永不上
					showmessage('location_login_outofdate', 'home.php?mod=spacecp&ac=profile&op=password&resend=1', array('type' => 1), array('showdialog' => true, 'striptags' => false, 'locationtime' => true));
				}

				if ($this -> setting['accountguard']['loginpwcheck'] && $pwold) {
					$freeze = $pwold;
					if ($this -> setting['accountguard']['loginpwcheck'] == 2 && $freeze) {
						C::t('common_member') -> update($_G['uid'], array('freeze' => 1));
					}
				}
			}

			//验证码判断
//			$seccheckrule = &$_G['setting']['seccodedata']['rule']['login'];
//			if ($seccheckrule['allow'] == 2) {
//				if ($seccheckrule['nolocal']) {
//					require_once   libfile('function/misc');
//					$lastipConvert = process_ipnotice(convertip($_G['member']['lastip']));
//					$nowipConvert = process_ipnotice(convertip($_G['clientip']));
//					if ($lastipConvert != $nowipConvert && stripos($lastipConvert, $nowipConvert) == false && stripos($nowipConvert, $lastipConvert) == false) {
//						$seccodecheck = true;
//					}
//				}
//				if (!$seccodecheck && $seccheckrule['pwsimple'] && $pwold) {
//					$seccodecheck = true;
//				}
//				if (!$seccodecheck && $seccheckrule['outofday'] && $_G['member']['lastvisit'] && TIMESTAMP - $_G['member']['lastvisit'] > $seccheckrule['outofday'] * 86400) {
//					$seccodecheck = true;
//				}
//				if (!$seccodecheck && $_G['member_loginperm'] < 4) {
//					$seccodecheck = true;
//				}
//				if (!$seccodecheck && $seccheckrule['numiptry']) {
//					$seccodecheck = failedipcheck($seccheckrule['numiptry'], $seccheckrule['timeiptry']);
//				}
//				if ($seccodecheck && !$secchecklogin2) {
//					clearcookies();
//					$auth = authcode($_GET['username'] . "\t" . $_GET['password'] . "\t" . ($_GET['questionid'] ? 1 : 0) . "\t1", 'ENCODE', $_G['config']['security']['authkey']);
//					$location = 'member.php?mod=logging&action=login&auth=' . rawurlencode($auth) . '&referer=' . rawurlencode(dreferer()) . (!empty($_GET['cookietime']) ? '&cookietime=1' : '');
//					//请输入验证码后继续登录
//					return sendAppMessage(RES_NO, 'login_seccheck2', $location);
//				}
//			}

			//如果有邀请人
			if ($invite['id']) {
				$result = C::t('common_invite') -> count_by_uid_fuid($invite['uid'], $uid);
				if (!$result) {
					C::t('common_invite') -> update($invite['id'], array('fuid' => $uid, 'fusername' => $_G['username']));
					updatestat('invite');
				} else {
					$invite = array();
				}
			}
			if ($invite['uid']) {
				require_once   libfile('function/friend');
				friend_make($invite['uid'], $invite['username'], false);
				dsetcookie('invite_auth', '');
				if ($invite['appid']) {
					updatestat('appinvite');
				}
			}
			
			//生成app_sign调用类
//			$_sign_model = new app_sign();
			$param = array(
//				'_sign'=> $_sign_model->create_sign(),
				'username' => $result['ucresult']['username'],
				'usergroup' => $_G['group']['grouptitle'], 
				'uid' => $_G['member']['uid'], 
				'groupid' => $_G['groupid'], 
				'avatar'=> avatar($_G['member']['uid'],'middle',TRUE),
				'credits' => $_G['member']['credits'],
				'addcredits' => app_get_return_credict($credits),
				'syn' => $ucsynlogin ? 1 : 0,
			);

			$extra = array('showdialog' => true, 'locationtime' => true, 'extrajs' => $ucsynlogin);

			//判断返回信息
			if (!$freeze || !$this -> setting['accountguard']['loginpwcheck']) {
				//当账户正常的时候
				$loginmessage = $_G['groupid'] == 8 ? 'login_succeed_inactive_member' : 'app_login_succeed';
				$location = $invite || $_G['groupid'] == 8 ? 'home.php?mod=space&do=home' : dreferer();
			} else {
				$loginmessage = 'login_succeed_password_change';
				$location = 'home.php?mod=spacecp&ac=profile&op=password';
				$_GET['lssubmit'] = 0;
			}
			
			//查找更多数据
			$param = array_merge($param,$_G['member']);
			
			$param['location'] = $location;
			return sendAppMessage(RES_YES, $loginmessage, $param,$this->returntype);
			

		} else {
			$password = preg_replace("/^(.{" . round(strlen($_GET['password']) / 4) . "})(.+?)(.{" . round(strlen($_GET['password']) / 6) . "})$/s", "\\1***\\3", $_GET['password']);
			$errorlog = dhtmlspecialchars(TIMESTAMP . "\t" . ($result['ucresult']['username'] ? $result['ucresult']['username'] : $_GET['username']) . "\t" . $password . "\t" . "Ques #" . intval($_GET['questionid']) . "\t" . $_G['clientip']);
			writelog('illegallog', $errorlog);
			//loginfailed($_GET['username']);
			//failedip();
			$fmsg = $result['ucresult']['uid'] == '-3' ? (empty($_GET['questionid']) || $answer == '' ? 'login_question_empty' : 'login_question_invalid') : 'login_invalid';

            if ( $_G['member_loginperm'] > 1) {
				return sendAppMessage(RES_NO, array($fmsg, array('loginperm' => $_G['member_loginperm'] - 1)));
			} elseif ($_G['member_loginperm'] == -1) {
				//抱歉，您输入的密码有误
				return sendAppMessage(RES_NO, 'login_password_invalid',null,$this->returntype);
			} else {
				//密码错误次数过多，请 15 分钟后重新登录
				return sendAppMessage(RES_NO, 'login_strike',null,$this->returntype);
			}
		}

	}

	function on_logout() {
		global $_G;

		clearcookies();
		$_G['groupid'] = $_G['member']['groupid'] = 7;
		$_G['uid'] = $_G['member']['uid'] = 0;
		$_G['username'] = $_G['member']['username'] = $_G['member']['password'] = '';
		$_G['setting']['styleid'] = $this -> setting['styleid'];

		return sendAppMessage(RES_YES,'app_logout_succeed', array('formhash' => FORMHASH, 'referer' => rawurlencode(dreferer())),$this->returntype);
		
	}

}

class app_register_ctl {
	//数据返回类型
	public $returntype = 'JSON';
	var $showregisterform = 1;
	public $after_register_array = array();
	
	function register_ctl() {
		global $_G;
		if ($_G['setting']['bbclosed']) {
			if (($_GET['action'] != 'activation' && !$_GET['activationauth']) || !$_G['setting']['closedallowactivation']) {
				//抱歉，目前站点禁止新用户注册
				return sendAppMessage(RES_NO,'app_register_disable', null, $this->returntype );
			}
		}

		loadcache(array('modreasons', 'stamptypeid', 'fields_required', 'fields_optional', 'fields_register', 'ipctrl'));
		require_once   libfile('function/misc');
		require_once   libfile('function/profile');
		if (!function_exists('sendmail')) {
			include   libfile('function/mail');
		}
		loaducenter();
	}

	function on_register($datas=array(),$auto_login=TRUE) {
		global $_G;
		
		//加载ucenter类
		loaducenter();
		
		if(empty($datas)){
			$type = I('type','mobile');
			$input = I('input','');
			$password = I('password','');
		}else{
			$type = $datas['type'];
			$input = $datas['input'];
			$password = $datas['password'];
		}
		
		//自动判断吗?
		$type = strstr($input,'@') ? 'email' : 'mobile';
			
		//开始注册
		$_GET['username'] = $input;
		$_GET['password'] = $password;
		$_GET['password2'] = $password;
		
		if($type == 'email'){
			$_GET['email'] = $input;
		}
		
		//如果已经登陆
		if (!$this -> setting['regclosed'] && (!$this -> setting['regstatus'] || !$this -> setting['ucactivation'])) {
			if ($_GET['action'] == 'activation' || $_GET['activationauth']) {
				if (!$this -> setting['ucactivation'] && !$this -> setting['closedallowactivation']) {
					//抱歉，目前禁止激活
					return sendAppMessage(RES_NO,'app_register_disable_activation', null, $this->returntype );
				}
			} elseif (!$this -> setting['regstatus']) {
				return sendAppMessage(RES_NO,!$this -> setting['regclosemessage'] ? 'app_register_disable' : str_replace(array("\r", "\n"), '', $this -> setting['regclosemessage']));
			}
		}

		$bbrules = &$this -> setting['bbrules'];
		$bbrulesforce = &$this -> setting['bbrulesforce'];
		$bbrulestxt = &$this -> setting['bbrulestxt'];
		$welcomemsg = &$this -> setting['welcomemsg'];
		$welcomemsgtitle = &$this -> setting['welcomemsgtitle'];
		$welcomemsgtxt = &$this -> setting['welcomemsgtxt'];
		$regname = $this -> setting['regname'];
		
		//新用户注册验证
		if ($this -> setting['regverify']) {
			
			//验证白名单
			if ($this -> setting['areaverifywhite']) {
				$location = $whitearea = '';
				$location = trim(convertip($_G['clientip'], "./"));
				if ($location) {
					$whitearea = preg_quote(trim($this -> setting['areaverifywhite']), '/');
					$whitearea = str_replace(array("\\*"), array('.*'), $whitearea);
					$whitearea = '.*' . $whitearea . '.*';
					$whitearea = '/^(' . str_replace(array("\r\n", ' '), array('.*|.*', ''), $whitearea) . ')$/i';
					if (@preg_match($whitearea, $location)) {
						$this -> setting['regverify'] = 0;
					}
				}
			}
			
			if ($_G['cache']['ipctrl']['ipverifywhite']) {
				foreach (explode("\n", $_G['cache']['ipctrl']['ipverifywhite']) as $ctrlip) {
					if (preg_match("/^(" . preg_quote(($ctrlip = trim($ctrlip)), '/') . ")/", $_G['clientip'])) {
						$this -> setting['regverify'] = 0;
						break;
					}
				}
			}
		}
		
		//邀请码配置
		$invitestatus = false;
		if ($this -> setting['regstatus'] == 2) {
			if ($this -> setting['inviteconfig']['inviteareawhite']) {
				$location = $whitearea = '';
				$location = trim(convertip($_G['clientip'], "./"));
				if ($location) {
					$whitearea = preg_quote(trim($this -> setting['inviteconfig']['inviteareawhite']), '/');
					$whitearea = str_replace(array("\\*"), array('.*'), $whitearea);
					$whitearea = '.*' . $whitearea . '.*';
					$whitearea = '/^(' . str_replace(array("\r\n", ' '), array('.*|.*', ''), $whitearea) . ')$/i';
					if (@preg_match($whitearea, $location)) {
						$invitestatus = true;
					}
				}
			}
			
			if ($this -> setting['inviteconfig']['inviteipwhite']) {
				foreach (explode("\n", $this->setting['inviteconfig']['inviteipwhite']) as $ctrlip) {
					if (preg_match("/^(" . preg_quote(($ctrlip = trim($ctrlip)), '/') . ")/", $_G['clientip'])) {
						$invitestatus = true;
						break;
					}
				}
			}
		}
		
		//如果设置了验证白名单
		$groupinfo = array();
		if ($this -> setting['regverify']) {
			$groupinfo['groupid'] = 8;
		} else {
			$groupinfo['groupid'] = $this -> setting['newusergroupid'];
		}

//		//验证码设置
//		list($seccodecheck, $secqaacheck) = seccheck('register');
//		
//		$fromuid = !empty($_G['cookie']['promotion']) && $this -> setting['creditspolicy']['promotion_register'] ? intval($_G['cookie']['promotion']) : 0;
		$username = isset($_GET['username']) ? $_GET['username'] : '';
		if($type=='email'){
			$_temp_username = explode('@', $username);
			$username = $_temp_username[0];
		}
//		$bbrulehash = $bbrules ? substr(md5(FORMHASH), 0, 8) : '';
//		$auth = $_GET['auth'];
//		
//		//获取邀请码
//		if (!$invitestatus) {
//			$invite = getinvite();
//		}
//		
//		//是否先验证邮箱后注册
//		$sendurl = $this -> setting['sendregisterurl'] ? true : false;
//		if ($sendurl) {
//			if (!empty($_GET['hash'])) {
//				$_GET['hash'] = preg_replace("/[^\[A-Za-z0-9_\]%\s+-\/=]/", '', $_GET['hash']);
//				$hash = explode("\t", authcode($_GET['hash'], 'DECODE', $_G['config']['security']['authkey']));
//				if (is_array($hash) && isemail($hash[0]) && TIMESTAMP - $hash[1] < 259200) {
//					$sendurl = false;
//				}
//			}
//		}
		
		//注册开始
//		if (!submitcheck('regsubmit', 0, $seccodecheck, $secqaacheck)) {
//
//			if ($_GET['action'] == 'activation') {
//				$auth = explode("\t", authcode($auth, 'DECODE'));
//				if (FORMHASH != $auth[1]) {
//					showmessage('register_activation_invalid', 'member.php?mod=logging&action=login');
//				}
//				$username = $auth[0];
//				$activationauth = authcode("$auth[0]\t" . FORMHASH, 'ENCODE');
//				$sendurl = false;
//			}
//
//			if (!$sendurl) {
//
//				if ($fromuid) {
//					$member = getuserbyuid($fromuid);
//					if (!empty($member)) {
//						$fromuser = dhtmlspecialchars($member['username']);
//					} else {
//						dsetcookie('promotion');
//					}
//				}
//
//				if ($_GET['action'] == 'activation') {
//					$auth = dhtmlspecialchars($auth);
//				}
//
//				if ($seccodecheck) {
//					$seccode = random(6, 1);
//				}
//
//				$username = dhtmlspecialchars($username);
//
//				$htmls = $settings = array();
//				foreach ($_G['cache']['fields_register'] as $field) {
//					$fieldid = $field['fieldid'];
//					$html = profile_setting($fieldid, array(), false, false, true);
//					if ($html) {
//						$settings[$fieldid] = $_G['cache']['profilesetting'][$fieldid];
//						$htmls[$fieldid] = $html;
//					}
//				}
//
//				$navtitle = $this -> setting['reglinkname'];
//
//				if ($this -> extrafile && file_exists($this -> extrafile)) {
//					require_once $this -> extrafile;
//				}
//			}
//			$bbrulestxt = nl2br("\n$bbrulestxt\n\n");
//			$dreferer = dreferer();
//
//			include   template($this -> template);
//
//		} else {
			
//			//是否开启先验证邮箱后注册
//			$activationauth = array();
//			if (isset($_GET['activationauth']) && $_GET['activationauth']) {
//				$activationauth = explode("\t", authcode($_GET['activationauth'], 'DECODE'));
//				if ($activationauth[1] != FORMHASH) {
//					//抱歉，激活失败，请重新登录验证需要激活的用户
//					showmessage('register_activation_invalid', 'member.php?mod=logging&action=login');
//				}
//				$sendurl = false;
//			}
//			if (!$activationauth && $sendurl) {
//				app_checkemail($_GET['email']);
//			}
//			if ($sendurl) {
//				$hashstr = urlencode(authcode("$_GET[email]\t$_G[timestamp]", 'ENCODE', $_G['config']['security']['authkey']));
//				$registerurl = "{$_G[siteurl]}member.php?mod=" . $this -> setting['regname'] . "&amp;hash={$hashstr}&amp;email={$_GET[email]}";
//				$email_register_message = lang('email', 'email_register_message', array('bbname' => $this -> setting['bbname'], 'siteurl' => $_G['siteurl'], 'url' => $registerurl));
//				if (!sendmail("$_GET[email] <$_GET[email]>", lang('email', 'email_register_subject'), $email_register_message)) {
//					runlog('sendmail', "$_GET[email] sendmail failed.");
//				}
//				//感谢您注册 {bbname}，<br />系统给您发送了一封带有注册地址的邮件，快去登录邮箱获取注册链接进行下一步注册吧
//				showmessage('register_email_send_succeed', dreferer(), array('bbname' => $this -> setting['bbname']), array('showdialog' => false, 'msgtype' => 3, 'closetime' => 10));
//			}
			
//			//判断是否已经发送邮件
//			$emailstatus = 0;
//			if ($this -> setting['sendregisterurl'] && !$sendurl) {
//				$_GET['email'] = strtolower($hash[0]);
//				$this -> setting['regverify'] = $this -> setting['regverify'] == 1 ? 0 : $this -> setting['regverify'];
//				if (!$this -> setting['regverify']) {
//					$groupinfo['groupid'] = $this -> setting['newusergroupid'];
//				}
//				$emailstatus = 1;
//			}
			
//			//邀请码注册
//			if ($this -> setting['regstatus'] == 2 && empty($invite) && !$invitestatus) {
//				//抱歉，本站目前暂时不允许用户直接注册，需要有效的邀请码才能注册
//				return sendAppMessage(RES_NO,'not_open_registration_invite',null,$this->returntype);
//			}
			
//			if ($bbrules && $bbrulehash != $_POST['agreebbrule']) {
//				//您必须同意服务条款后才能注册
//				return sendAppMessage(RES_NO,'register_rules_agree',null,$this->returntype);
//			}
			
//			//用户激活操作
//			$activation = array();
//			if (isset($_GET['activationauth']) && $activationauth && is_array($activationauth)) {
//				if ($activationauth[1] == FORMHASH && !($activation = uc_get_user($activationauth[0]))) {
//					//抱歉，激活失败，请重新登录验证需要激活的用户
//					return sendAppMessage(RES_NO,'register_activation_invalid', 'member.php?mod=logging&action=login' ,$this->returntype);
//				}
//			}
			
			//不是激活的时候，即注册
			if (!$activation) {
				$usernamelen = dstrlen($username);
				if ($usernamelen < 3) {
					//抱歉，您输入的用户名小于 3 个字符，请输入一个较长的用户名
					return sendAppMessage(RES_NO,'profile_username_tooshort',$username,$this->returntype);
				} elseif ($usernamelen > 32) {
					//抱歉，您的用户名超过 15 个字符，请输入一个较短的用户名
					return sendAppMessage(RES_NO,'profile_username_toolong',$username,$this->returntype);
				}

				//检查用户名
				if (uc_get_user(addslashes($username)) && !C::t('common_member') -> fetch_uid_by_username($username) && !C::t('common_member_archive') -> fetch_uid_by_username($username)) {
					//该用户名已被注册
					return sendAppMessage(RES_NO,'profile_username_duplicate',array('username'=>$username),$this->returntype);
				}
				//检查最小密码长度
				if ($this -> setting['pwlength']) {
					if (strlen($_GET['password']) < $this -> setting['pwlength']) {
						return sendAppMessage(RES_NO,array('profile_password_tooshort',array('pwlength'=>$this->setting['pwlength'])),array('mix_password_len'=>$this -> setting['pwlength']),$this->returntype);
					}
				}
				//检查密码的复杂度
				if ($this -> setting['strongpw']) {
					$strongpw_str = array();
					if (in_array(1, $this -> setting['strongpw']) && !preg_match("/\d+/", $_GET['password'])) {
						$strongpw_str[] = lang('member/template', 'strongpw_1');
					}
					if (in_array(2, $this -> setting['strongpw']) && !preg_match("/[a-z]+/", $_GET['password'])) {
						$strongpw_str[] = lang('member/template', 'strongpw_2');
					}
					if (in_array(3, $this -> setting['strongpw']) && !preg_match("/[A-Z]+/", $_GET['password'])) {
						$strongpw_str[] = lang('member/template', 'strongpw_3');
					}
					if (in_array(4, $this -> setting['strongpw']) && !preg_match("/[^a-zA-z0-9]+/", $_GET['password'])) {
						$strongpw_str[] = lang('member/template', 'strongpw_4');
					}
					if ($strongpw_str) {
						return sendAppMessage(RES_NO,array(FALSE,lang('member/template', 'password_weak') . implode(',', $strongpw_str)),null,$this->returntype);
					}
				}
				
				//判断email是否合法
				$email = strtolower(trim($_GET['email']));
				
				if (empty($this -> setting['ignorepassword'])) {
					if ($_GET['password'] !== $_GET['password2']) {
						return sendAppMessage(RES_NO,'profile_passwd_notmatch',null,$this->returntype);
					}

					if (!$_GET['password'] || $_GET['password'] != addslashes($_GET['password'])) {
						return sendAppMessage(RES_NO,'profile_passwd_illegal',null,$this->returntype);
					}
					$password = $_GET['password'];
				} else {
					$password = md5(random(10));
				}
			}
			
			$censorexp = '/^(' . str_replace(array('\\*', "\r\n", ' '), array('.*', '|', ''), preg_quote(($this -> setting['censoruser'] = trim($this -> setting['censoruser'])), '/')) . ')$/i';

			if ($this -> setting['censoruser'] && @preg_match($censorexp, $username)) {
				//用户名包含被系统屏蔽的字符
				return sendAppMessage(RES_NO,'profile_username_protect',null,$this->returntype);
			}

			if ($this -> setting['regverify'] == 2 && !trim($_GET['regmessage'])) {
				//抱歉，您尚未填写必填项目或必填项目格式不正确
				return sendAppMessage(RES_NO,'profile_required_info_invalid',null,$this->returntype);
			}

			if ($_G['cache']['ipctrl']['ipregctrl']) {
				foreach (explode("\n", $_G['cache']['ipctrl']['ipregctrl']) as $ctrlip) {
					if (preg_match("/^(" . preg_quote(($ctrlip = trim($ctrlip)), '/') . ")/", $_G['clientip'])) {
						$ctrlip = $ctrlip . '%';
						$this -> setting['regctrl'] = $this -> setting['ipregctrltime'];
						break;
					} else {
						$ctrlip = $_G['clientip'];
					}
				}
			} else {
				$ctrlip = $_G['clientip'];
			}
			
			//判断 同一IP注册间隔限制
			if ($this -> setting['regctrl']) {
				if (C::t('common_regip') -> count_by_ip_dateline($ctrlip, $_G['timestamp'] - $this -> setting['regctrl'] * 3600)) {
					//抱歉，您的 IP 地址在 {regctrl} 小时内无法注册
					return sendAppMessage(RES_NO,array('register_ctrl',array('regctrl' => $this -> setting['regctrl'])), array('can_reg_time'=> $this->setting['regctrl']), $this->returntype );
				}
			}
			
			//判断同一IP在24小时允许注册的最大次数
			$setregip = null;
			if ($this -> setting['regfloodctrl']) {
				$regip = C::t('common_regip') -> fetch_by_ip_dateline($_G['clientip'], $_G['timestamp'] - 86400);
				if ($regip) {
					if ($regip['count'] >= $this -> setting['regfloodctrl']) {
						//抱歉，IP 地址在 24 小时内只能注册 {regfloodctrl} 次
						return sendAppMessage(RES_NO,array('register_flood_ctrl', array('regfloodctrl' => $this -> setting['regfloodctrl'])),array('can_reg_time'=>$this -> setting['regfloodctrl']),$this->returntype);
					} else {
						$setregip = 1;
					}
				} else {
					$setregip = 2;
				}
			}
			
			//用户资料检测，APP当中不需要
//			$profile = $verifyarr = array();
//			foreach ($_G['cache']['fields_register'] as $field) {
//				if (defined('IN_MOBILE')) {
//					break;
//				}
//				$field_key = $field['fieldid'];
//				$field_val = $_GET['' . $field_key];
//				if ($field['formtype'] == 'file' && !empty($_FILES[$field_key]) && $_FILES[$field_key]['error'] == 0) {
//					$field_val = true;
//				}
//
//				if (!profile_check($field_key, $field_val)) {
//					$showid = !in_array($field['fieldid'], array('birthyear', 'birthmonth')) ? $field['fieldid'] : 'birthday';
//					showmessage($field['title'] . lang('message', 'profile_illegal'), '', array(), array('showid' => 'chk_' . $showid, 'extrajs' => $field['title'] . lang('message', 'profile_illegal') . ($field['formtype'] == 'text' ? '<script type="text/javascript">' . '$(\'registerform\').' . $field['fieldid'] . '.className = \'px er\';' . '$(\'registerform\').' . $field['fieldid'] . '.onblur = function () { if(this.value != \'\') {this.className = \'px\';$(\'chk_' . $showid . '\').innerHTML = \'\';}}' . '</script>' : '')));
//				}
//				if ($field['needverify']) {
//					$verifyarr[$field_key] = $field_val;
//				} else {
//					$profile[$field_key] = $field_val;
//				}
//			}

			
			
			//用户注册
			if (!$activation) {
				$uid = uc_user_register(addslashes($username), $password, $email, $questionid, $answer, $_G['clientip']);
				if ($uid <= 0) {
					if ($uid == -1) {
						//用户名包含敏感字符
						return sendAppMessage(RES_NO,'profile_username_illegal',$username,$this->returntype);
					} elseif ($uid == -2) {
						//用户名包含被系统屏蔽的字符
						return sendAppMessage(RES_NO,'profile_username_protect',$username,$this->returntype);
					} elseif ($uid == -3) {
						//该用户名已被注册
						return sendAppMessage(RES_NO,'profile_username_duplicate',$username,$this->returntype);
					} elseif ($uid == -4) {
						//Email 地址无效
						return sendAppMessage(RES_NO,'profile_email_illegal',$email,$this->returntype);
					} elseif ($uid == -5) {
						//抱歉，Email 包含不可使用的邮箱域名
						return sendAppMessage(RES_NO,'profile_email_domain_illegal',$email,$this->returntype);
					} elseif ($uid == -6) {
						//该 Email 地址已被注册
						return sendAppMessage(RES_NO,'profile_email_duplicate',$email,$this->returntype);
					} else {
						//未定义操作
						return sendAppMessage(RES_NO,'undefined_action',null,$this->returntype);
					}
				}
			} else {
				
				list($uid, $username, $email) = $activation;
			}
			
			
			
			$_G['username'] = $username;
			if (getuserbyuid($uid, 1)) {
				if (!$activation) {
					uc_user_delete($uid);
				}
				//抱歉，用户 ID {uid} 已被占用
				return sendAppMessage(RES_NO,'profile_uid_duplicate', array('uid' => $uid),$this->returntype);
			}

			$password = md5(random(10));
			$secques = $questionid > 0 ? random(8) : '';

			if (isset($_POST['birthmonth']) && isset($_POST['birthday'])) {
				$profile['constellation'] = get_constellation($_POST['birthmonth'], $_POST['birthday']);
			}
			if (isset($_POST['birthyear'])) {
				$profile['zodiac'] = get_zodiac($_POST['birthyear']);
			}

//			if ($_FILES) {
//				$upload = new discuz_upload();
//
//				foreach ($_FILES as $key => $file) {
//					$field_key = 'field_' . $key;
//					if (!empty($_G['cache']['fields_register'][$field_key]) && $_G['cache']['fields_register'][$field_key]['formtype'] == 'file') {
//
//						$upload -> init($file, 'profile');
//						$attach = $upload -> attach;
//
//						if (!$upload -> error()) {
//							$upload -> save();
//
//							if (!$upload -> get_image_info($attach['target'])) {
//								@unlink($attach['target']);
//								continue;
//							}
//
//							$attach['attachment'] = dhtmlspecialchars(trim($attach['attachment']));
//							if ($_G['cache']['fields_register'][$field_key]['needverify']) {
//								$verifyarr[$key] = $attach['attachment'];
//							} else {
//								$profile[$key] = $attach['attachment'];
//							}
//						}
//					}
//				}
//			}

			if ($setregip !== null) {
				if ($setregip == 1) {
					C::t('common_regip') -> update_count_by_ip($_G['clientip']);
				} else {
					C::t('common_regip') -> insert(array('ip' => $_G['clientip'], 'count' => 1, 'dateline' => $_G['timestamp']));
				}
			}

			if ($invite && $this -> setting['inviteconfig']['invitegroupid']) {
				$groupinfo['groupid'] = $this -> setting['inviteconfig']['invitegroupid'];
			}

			$init_arr = array('credits' => explode(',', $this -> setting['initcredits']), 'profile' => $profile, 'emailstatus' => $emailstatus);

			C::t('common_member') -> insert($uid, $username, $password, $email, $_G['clientip'], $groupinfo['groupid'], $init_arr);
			if ($emailstatus) {
				updatecreditbyaction('realemail', $uid);
			}
			if ($verifyarr) {
				$setverify = array('uid' => $uid, 'username' => $username, 'verifytype' => '0', 'field' => serialize($verifyarr), 'dateline' => TIMESTAMP, );
				C::t('common_member_verify_info') -> insert($setverify);
				C::t('common_member_verify') -> insert(array('uid' => $uid));
			}

			require_once   libfile('cache/userstats', 'function');
			build_cache_userstats();

			if ($this -> extrafile && file_exists($this -> extrafile)) {
				require_once $this -> extrafile;
			}

			if ($this -> setting['regctrl'] || $this -> setting['regfloodctrl']) {
				C::t('common_regip') -> delete_by_dateline($_G['timestamp'] - ($this -> setting['regctrl'] > 72 ? $this -> setting['regctrl'] : 72) * 3600);
				if ($this -> setting['regctrl']) {
					C::t('common_regip') -> insert(array('ip' => $_G['clientip'], 'count' => -1, 'dateline' => $_G['timestamp']));
				}
			}

			$regmessage = dhtmlspecialchars($_GET['regmessage']);
			if ($this -> setting['regverify'] == 2) {
				C::t('common_member_validate') -> insert(array('uid' => $uid, 'submitdate' => $_G['timestamp'], 'moddate' => 0, 'admin' => '', 'submittimes' => 1, 'status' => 0, 'message' => $regmessage, 'remark' => '', ), false, true);
				manage_addnotify('verifyuser');
			}
			
			//默认登录
			if($auto_login){
				require_once libfile('function/member');
				$credits = setloginstatus(array('uid' => $uid, 'username' => $_G['username'], 'password' => $password, 'groupid' => $groupinfo['groupid'], ), 0);
				include_once   libfile('function/stat');
				updatestat('register');
			}

			if ($invite['id']) {
				$result = C::t('common_invite') -> count_by_uid_fuid($invite['uid'], $uid);
				if (!$result) {
					C::t('common_invite') -> update($invite['id'], array('fuid' => $uid, 'fusername' => $_G['username'], 'regdateline' => $_G['timestamp'], 'status' => 2));
					updatestat('invite');
				} else {
					$invite = array();
				}
			}
			if ($invite['uid']) {
				if ($this -> setting['inviteconfig']['inviteaddcredit']) {
					updatemembercount($uid, array($this -> setting['inviteconfig']['inviterewardcredit'] => $this -> setting['inviteconfig']['inviteaddcredit']));
				}
				if ($this -> setting['inviteconfig']['invitedaddcredit']) {
					updatemembercount($invite['uid'], array($this -> setting['inviteconfig']['inviterewardcredit'] => $this -> setting['inviteconfig']['invitedaddcredit']));
				}
				require_once   libfile('function/friend');
				friend_make($invite['uid'], $invite['username'], false);
				notification_add($invite['uid'], 'friend', 'invite_friend', array('actor' => '<a href="home.php?mod=space&uid=' . $invite['uid'] . '" target="_blank">' . $invite['username'] . '</a>'), 1);

				space_merge($invite, 'field_home');
				if (!empty($invite['privacy']['feed']['invite'])) {
					require_once   libfile('function/feed');
					$tite_data = array('username' => '<a href="home.php?mod=space&uid=' . $_G['uid'] . '">' . $_G['username'] . '</a>');
					feed_add('friend', 'feed_invite', $tite_data, '', array(), '', array(), array(), '', '', '', 0, 0, '', $invite['uid'], $invite['username']);
				}
				if ($invite['appid']) {
					updatestat('appinvite');
				}
			}

			if ($welcomemsg && !empty($welcomemsgtxt)) {
				$welcomemsgtitle = replacesitevar($welcomemsgtitle);
				$welcomemsgtxt = replacesitevar($welcomemsgtxt);
				if ($welcomemsg == 1) {
					$welcomemsgtxt = nl2br(str_replace(':', '&#58;', $welcomemsgtxt));
					notification_add($uid, 'system', $welcomemsgtxt, array('from_id' => 0, 'from_idtype' => 'welcomemsg'), 1);
				} elseif ($welcomemsg == 2) {
					sendmail_cron($email, $welcomemsgtitle, $welcomemsgtxt);
				} elseif ($welcomemsg == 3) {
					sendmail_cron($email, $welcomemsgtitle, $welcomemsgtxt);
					$welcomemsgtxt = nl2br(str_replace(':', '&#58;', $welcomemsgtxt));
					notification_add($uid, 'system', $welcomemsgtxt, array('from_id' => 0, 'from_idtype' => 'welcomemsg'), 1);
				}
			}

			if ($fromuid) {
				updatecreditbyaction('promotion_register', $fromuid);
				dsetcookie('promotion', '');
			}
			dsetcookie('loginuser', '');
			dsetcookie('activationauth', '');
			dsetcookie('invite_auth', '');

			$url_forward = dreferer();
			$refreshtime = 3000;
			switch($this->setting['regverify']) {
				case 1 :
					$idstring = random(6);
					$authstr = $this -> setting['regverify'] == 1 ? "$_G[timestamp]\t2\t$idstring" : '';
					C::t('common_member_field_forum') -> update($_G['uid'], array('authstr' => $authstr));
					$verifyurl = "{$_G[siteurl]}member.php?mod=activate&amp;uid={$_G[uid]}&amp;id=$idstring";
					$email_verify_message = lang('email', 'email_verify_message', array('username' => $_G['member']['username'], 'bbname' => $this -> setting['bbname'], 'siteurl' => $_G['siteurl'], 'url' => $verifyurl));
					if (!sendmail("$username <$email>", lang('email', 'email_verify_subject'), $email_verify_message)) {
						runlog('sendmail', "$email sendmail failed.");
					}
					//感谢您注册 {bbname}，<br />系统给您发送了一封激活邮件，快去登录邮箱激活账号吧
					$message = 'register_email_verify';
					$locationmessage = 'register_email_verify_location';
					$refreshtime = 10000;
					break;
				case 2 :
					//感谢您的注册，站点开启了人工验证注册用户，请等待审核
					$message = 'register_manual_verify';
					$locationmessage = 'register_manual_verify_location';
					break;
				default :
					//感谢您注册 {bbname}，现在将以 {usergroup} 身份登录站点
					$message = 'app_register_succeed';
					$locationmessage = 'register_succeed_location';
					break;
			}
			//生成app_sign调用类
//			$_sign_model = new app_sign();
			$param = array(
				'bbname' => $this -> setting['bbname'], 
				'username' => $_G['username'], 
				'usergroup' => $_G['group']['grouptitle'], 
				'uid' => $_G['uid'],
				'avatar'=> avatar($_G['member']['uid'],'middle',TRUE) , 
			);
			
			//登录后才有的信息
			if($auto_login){
				$param['addcredits'] =  app_get_return_credict($credits);
				$param = array_merge($param,$_G['member']);
			}
			
			if (strpos($url_forward, $this -> setting['regname']) !== false || strpos($url_forward, 'buyinvitecode') !== false) {
				$url_forward = 'forum.php';
			}
			$href = str_replace("'", "\'", $url_forward);
			$param['url_forward'] = $url_forward;
			
			
			//执行回调，传入数据
			if(isset($this->after_register_array) && !empty($this->after_register_array)){
				foreach($this->after_register_array as $k => $v){
					if(method_exists($this, $k)){
						$param[$k] = $this->$k($param,$v);
					}
				}
			}
			
			return sendAppMessage(RES_YES, $message , $param, $this->returntype);
		}
//	}
	
}

class app_crime_action_ctl {

	static $actions = array('all', 'crime_delpost', 'crime_warnpost', 'crime_banpost', 'crime_banspeak', 'crime_banvisit', 'crime_banstatus', 'crime_avatar', 'crime_sightml', 'crime_customstatus');

	function crime_action_ctl() {
	}

	function & instance() {
		static $object;
		if (empty($object)) {
			$object = new crime_action_ctl();
		}
		return $object;
	}

	function recordaction($uid, $action, $reason) {
		global $_G;

		$uid = intval($uid);
		$key = array_search($action, self::$actions);
		if ($key === FALSE) {
			return false;
		}
		$insert = array('uid' => $uid, 'operatorid' => $_G['uid'], 'operator' => $_G['username'], 'action' => $key, 'reason' => $reason, 'dateline' => $_G['timestamp']);
		C::t('common_member_crime') -> insert($insert);
		return true;
	}

	function getactionlist($uid) {
		$uid = intval($uid);
		$clist = array();
		foreach (C::t('common_member_crime')->fetch_all_by_uid($uid) as $c) {
			$c['action'] = self::$actions[$c['action']];
			$clist[] = $c;
		}
		return $clist;
	}

	function getcount($uid, $action) {
		$uid = intval($uid);
		$key = array_search($action, self::$actions);
		if ($key === FALSE) {
			return 0;
		}
		return C::t('common_member_crime') -> count_by_uid_action($uid, $key);
	}

	function search($action, $username, $operator, $startime, $endtime, $reason, $start, $limit) {
		$action = intval($action);
		$operator = daddslashes(trim($operator));
		$starttime = $starttime ? strtotime($starttime) : 0;
		$endtime = $endtime ? (strtotime($endtime) + 3600 * 24) : 0;
		$reason = daddslashes(trim($reason));
		$start = intval($start);
		$limit = intval($limit);

		if (!empty($username)) {
			$uid = C::t('common_member') -> fetch_uid_by_username($username);
			$wheresql[] = "uid='$uid'";
		}

		if ($action) {
			$wheresql[] = "action='$action'";
		}
		if ($operator) {
			$wheresql[] = "operator='$operator'";
		}
		if ($starttime) {
			$wheresql[] = "dateline>='$starttime'";
		}
		if ($endtime) {
			$wheresql[] = "dateline<='$endtime'";
		}
		if ($reason) {
			$wheresql[] = "reason LIKE '%$reason%'";
		}

		if ($wheresql) {
			$wheresql = 'WHERE ' . implode(' AND ', $wheresql);
		} else {
			$wheresql = '';
		}
		$clist = array();
		$count = C::t('common_member_crime') -> count_by_where($wheresql);

		if ($count) {
			$uids = array();
			foreach (C::t('common_member_crime')->fetch_all_by_where($wheresql, $start, $limit) as $crime) {
				$crime['action'] = self::$actions[$crime['action']];
				$clist[] = $crime;
				$uids[$crime['uid']] = $crime['uid'];
			}
			$members = C::t('common_member') -> fetch_all($uids, false, 0);
			foreach ($clist as $key => $crime) {
				$crime['username'] = $members[$crime['uid']]['username'];
				$clist[$key] = $crime;
			}
		}
		return array($count, $clist);
	}

}
?>