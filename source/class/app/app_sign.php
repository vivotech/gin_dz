<?php

/**
 * APP sign认证类
 * 使用discuz中的 authcode 函数来生成和验证_sign
 */

class app_sign{
	
	//authcode的秘钥
	private $sign_key = 'vivo201503230931';
	public $_sign = '';
	public $session_model = null;
	//初始化
	function __construct($session_model = null , $sign_key = ''){
		//如果sign_key为空，则指定生成key
		$this->sign_key = ( empty($sign_key) || $sign_key == '' ) ? $this->sign_key : $sign_key;
		$this->session_model = is_null($session_model) ? C::app()->session : $session_model ;
	}
	
	//生成验证sign
	function create_sign($username = '',$password = ''){
		global $_G;
		$username = ( empty($username) || $username == '' ) ? $_G['member']['username'] : $username ;
		$password = ( empty($password) || $password == '' ) ? $_G['member']['password'] : $password ;
		$return = $this->_sign = md5($username.$password.time());
		$this->session_model->set('app_sign',$return);
		$this->session_model->update();
		return $return;
	}
	
	//判断生成的_sign是否正确
	function check_sign($_sign , $username = '',$password = '' ){
		if(empty($_sign)) return FALSE;
		$username = ( empty($username) || $username == '' ) ? $_G['member']['username'] : $username ;
		$password = ( empty($password) || $password == '' ) ? $_G['member']['password'] : $password ;
		$session_sign = $this->session_model->get('app_sign');
		if($_sign == $session_sign){
			return TRUE;
		}
		return FALSE;
	}
	
	
}
 