<?php

if (!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class  wechat_exception extends Exception {
	
	public function errorMessage()
	{
		return $this->getMessage();
	}

}

?>