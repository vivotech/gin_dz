<?php

/**
 * 处理微信消息
 * 404
 */

class wechat_handler{
	
	public $wechat;
	public $reponser;
	public $from; 	//我们的ID
	public $to;		//接收信息方ID
	
	function __construct($wechat=null,$to='',$from=''){
		global $_G;
		$this->wechat = $wechat==null ? $_G['wechat'] : $wechat;
		$this->from = $from;
		$this->to = $to;
		$this->reponser = new wechat_reponse($this);
	}
	
	//所有事件捕捉
	public function receiveAllStart($param){
		
	}
	
	//处理完成后操作
	public function receiveAllEnd($param){
		
	}
	
	//消息事件:文字
	public function msg_text($param){
		$keyword = new wechat_keyword($param['content'],$this->wechat,$this->reponser);
		$keyword->response();
		//如果找不到关键词，则转发消息到多客服
		$this->reponser->getXML4CustomerService($param);
	}
	
	//消息事件:图片
	public function msg_image($param){
		$this->reponser->getXml4Txt('朋友，我收到你的图片！');
	}
	
	//消息事件:视频
	public function msg_video($param){
		$this->reponser->getXml4Txt('朋友，我收到你的视频！');
	}
	
	//消息事件:语音
	public function msg_voice($param){
		$this->reponser->getXml4Txt('朋友，我收到你的语音！');
	}
	
	//消息事件:小视频
	public function msg_shortvideo($param){
		$this->reponser->getXml4Txt('朋友，我收到你的小视频！');
	}
	
	//消息事件:连接
	public function msg_link($param){
		
	}
	
	public function msg_location($param){
		
	}
	
	//上报地理位置
	public function event_location($param){
		$wechat = $this->wechat;
		$insertData = array(
						'weid'		=> $wechat['weid'],
						'openid'	=> $this->to,
						'latitude'	=> $param['la'],
						'longitude'	=> $param['lo'],
						'precision'	=> $param['p'],
						'addtime'	=> TIMESTAMP
					);
		$model = M('wechat_member_location');
		$item = $model->where("openid='{$insertData[openid]}'")->find();
		if(!$item || $item['addtime'] < strtotime(date('Y-m-d',TIMESTAMP))){
			//每天记录一次
			M('wechat_member_location')->add($insertData);
		}
	}
	
	//自定义菜单点击事件
	public function event_click($param){
		$keyword = new wechat_keyword($param['key'],$this->wechat,$this->reponser);
		$keyword->response();
	}
	
	//关注公众号回复
	public function event_subscribe($param){
		$wechat = $this->wechat;
		$this->reponser->getXml4Txt($wechat['welcome']);
		//带参数的二维码
		if(isset($param['key'])){
			$qrcode = new wechat_qrcode($wechat,$this->reponser,$param['key']);
			$qrcode ->action(FALSE);
		}
	}
	
	public function event_scan($param){
		$qrcode = new wechat_qrcode($this->wechat,$this->reponser,$param['key']);
		$qrcode ->action();
	}
	
	//消息模板回调提醒
	public function event_templatesendjobfinish($param){
		$msg_id = $param['msg_id'];
		switch(strtolower($param['status'])){
			case 'success':$status=1;break;
			case 'failed:user block':$status=2;break;
			case 'failed: system failed':$status=3;break;
		}
		M('common_member_wechat_templatemsg')->where("msgid='{$msg_id}'")->save(array('state'=>$status));
	}
	
	
}
