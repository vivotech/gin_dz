<?php

//消息模板类

if (!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class wechat_template{
	
	public $wechatObj;
	public $touser = null;
	public $order = null;
	public $errmsg = '';
	public $msgid = '';
	
	function __construct($wechatObj = null){
		global $_G;
		if($wechatObj==null){
			if($_G['wechatObj']){
				$this->wechatObj = $_G['wechatObj'];
			}
		}else{
			$this->wechatObj = $wechatObj;
		}
		if(empty($this->wechatObj)){
			throw new wechat_exception("对象为空，请检查");
		}
	}
	
	public function setOrder($order){
		$this->order = $order;
	}
	
	//订单确认消息
	public function sendOrderSure($touser){
		$order = $this->order;
		require_once libfile('function/order');
		$remark =  '您使用的是“'.get_payment_name($order['payment']).'”，'.($order['payment']==0 ? '我们会尽快给你发货哦~':($order['paystate']==0?'您还没有付款，点击本信息进入付款！':'您已经付款了，我们会尽快发货哦！'))."点击查看订单状态";
		$innerData = array(
			'first'=>array('value'=>'感谢您在意大利原生活中进行购物，我们已经收到您的订单','color'=>'#000000'),
			'orderno'=>array('value'=>$order['o_openid'],'color'=>'#000000'),
			'amount'=>array('value'=>$order['goodcost'].'元','color'=>'#000000'),
			'remark'=>array('value'=>$remark,'color'=>'#000000'),
		);
		$template_id = 'b3yegwsaNLfCfsXlR_2pQ7CrLxm0zrz-DZgF-HNDECs';
		$url = U('forum/order/detail',array('order_id'=>$order['oid']),TRUE);
		$data = $this->baseTemplate($touser,$template_id,$url,'#FF0000',$innerData);
		$res = $this->send('ordersure',$order['oid'],$data);
		return $res;
	}
	
	//订单付款成功通知
	public function sendOrderPaySure($touser,$paytime){
		$order = $this->order;
		require_once libfile('function/order');
		$innerData = array(
			'first'=>array('value'=>'我们已经收到您的货款，感谢您对意大利原生活的支持！','color'=>'#000000'),
			'keyword1'=>array('value'=>$order['o_openid'],'color'=>'#000000'),
			'keyword2'=>array('value'=>date('Y-m-d H:s',$paytime),'color'=>'#000000'),
			'keyword3'=>array('value'=>$order['goodcost'].'元','color'=>'#000000'),
			'keyword4'=>array('value'=>get_payment_name($order['payment']),'color'=>'#000000'),
			'remark'=>array('value'=>'我们会尽快给你发货哦~点击查看订单状态','color'=>'#000000'),
		);
		$template_id = 'NDfHfqT7TS9gOos2aTv9GIXJf4zRWJAqubl_99QbPjo';
		$url = U('forum/order/detail',array('order_id'=>$order['oid']),TRUE);
		$data = $this->baseTemplate($touser,$template_id,$url,'#FF0000',$innerData);
		$res = $this->send('orderpaysure',$order['oid'],$data);
		return $res;
	}
	
	//订单发货通知
	public function sendOrderShippingSure($touser){
		global $_G;
		$order = $this->order;
		if(!$_G['cache']['express_company']){
			loadcache('express_company');
		}
		$innerData = array(
			'first'=>array('value'=>'您的订单已发货，感谢您对意大利原生活的支持！','color'=>'#000000'),
			'keyword1'=>array('value'=>$order['o_openid'],'color'=>'#000000'),
			'keyword2'=>array('value'=>$_G['cache']['express_company'][$order['expresscom']]['ecname'],'color'=>'#000000'),
			'keyword3'=>array('value'=>$order['expresscode'],'color'=>'#000000'),
			'remark'=>array('value'=>'点击查看快递情况','color'=>'#000000'),
		);
		$template_id = 'G_3OAA7wGYkOzVoY5_WL1dkTWS1MAtasPmchZu3Oq0c';
		$url = U('forum/order/detail',array('order_id'=>$order['oid']),TRUE);
		$data = $this->baseTemplate($touser,$template_id,$url,'#FF0000',$innerData);
		$res = $this->send('ordershippingsure',$order['oid'],$data);
		return $res;
	}
	
	//买家申请退货
	public function sendReturnOrderSure($touser){
		global $_G;
		$order = $this->order;
		
		$orderProducts = C::t('common_member_order_detail')->fetch_all_by_oid($order['oid']);
		
		$innerData = array(
			'first'=>array('value'=>'买家申请退货','color'=>'#000000'),
			'keyword1'=>array('value'=>$order['o_openid'],'color'=>'#000000'),
			'keyword2'=>array('value'=>$orderProducts[0]['subject'].(count($orderProducts)>1?'等':''),'color'=>'#000000'),
			'keyword3'=>array('value'=>$order['goodcost'],'color'=>'#000000'),
			'remark'=>array('value'=>'您的退货申请我们已经收到，我们会尽快进行处理！','color'=>'#000000'),
		);
		
		$template_id = 'PDStSnsFS3VT_vVh32X4x1CgklEhXx8eufVvir7v-MQ';
		$url = U('forum/order/detail',array('order_id'=>$order['oid']),TRUE);
		$data = $this->baseTemplate($touser,$template_id,$url,'#FF0000',$innerData);
		$res = $this->send('returnordersure',$order['oid'],$data);
	}
	
	//退货审核通知
	public function sendReturnOrderReview($touser,$type=1,$reason=''){
		global $_G;
		$order = $this->order;
		$orderProducts = C::t('common_member_order_detail')->fetch_all_by_oid($order['oid']);
		
		$innerData = array(
			'first'=>array('value'=>'订单号：'.$order['o_openid'],'color'=>'#FF0000'),
			'keyword1'=>array('value'=>$type==1?'审核通过':'审核驳回','color'=>$type==1?'#00A352':'#FF0000'),
			'keyword2'=>array('value'=>$orderProducts[0]['subject'].(count($orderProducts)>1?'等':''),'color'=>'#000000'),
			'keyword3'=>array('value'=>$order['goodcost'],'color'=>'#000000'),
			'keyword4'=>array('value'=>$reason,'color'=>'#000000'),
			'keyword5'=>array('value'=>date('Y-m-d H:i',TIMESTAMP),'color'=>'#000000'),
			'remark'=>array('value'=>'感谢您的支持。如有疑问，请详询客服热线：0757-82263380','color'=>'#000000'),
		);
		
		$template_id = 'YfLevk0Y7LuFrFLh146nbNnikKgC_8Q7kOJrBJu0ntg';
		$url = U('forum/order/detail',array('order_id'=>$order['oid']),TRUE);
		$data = $this->baseTemplate($touser,$template_id,$url,'#FF0000',$innerData);
		$res = $this->send('returnorderreview',$order['oid'],$data);
	}
	
	public function send($type,$target_id,$data){
		if(!$this->checkMsg($type, $target_id)){
			$accessToken = $this->wechatObj->getAccessToken();
			$url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token={$accessToken}";
			$result = wechat_client::post($url, $data);
			$res = $this->checkResult(json_decode($result,TRUE));
//			file_put_contents('/alidata/www/ginye/gin_dz/data/tempMSG.log', $type.'-'.$target_id.'-'.$data."\r\n",FILE_APPEND);
			$this->markMsg($type, $target_id);
			return $res;
		}
		return FALSE;
	}
	
	private function checkResult($res){
		$this->errmsg = $res['errmsg'];
		$this->msgid = $res['msgid'];
		return $res['errcode']==0 ? TRUE : FALSE;
	}
	
	private function baseTemplate($touser,$template_id,$url='',$topcolor='#FF0000',$data){
		$json = array(
			"touser"=>$touser,
			"template_id"=>$template_id,
            "url"=>$url,
            "topcolor"=>$topcolor,
            "data"=>$data
		);
		return json_encode($json,JSON_UNESCAPED_UNICODE);
	}
	
	public function markMsg($type,$target_id,$msgid=null){
		$wxtm = array(
			'type'		=>	$type,
			'target_id' =>	$target_id,
			'msgid'		=>	$msgid==null ? $this->msgid : $msgid,
			'state'		=>	0,
			'timeline'	=>	TIMESTAMP
		);
		return M('common_member_wechat_templatemsg')->add($wxtm);
	}
	
	public function checkMsg($type,$target_id){
		return M('common_member_wechat_templatemsg')->where("type='$type' and target_id = '$target_id'")->find();
	}
	
	
}
