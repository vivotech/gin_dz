<?php

if (!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class wechat_appclient extends wechat_client{
	
	public $accessToken = '';
	
	function __construct($accessToken=''){
		$this->accessToken = $accessToken;
	}
	
	function getUserInfoById($openid,$accessToken=''){
		$accessToken = empty($accessToken) ? $this->accessToken : $accessToken;
		$url = "https://api.weixin.qq.com/sns/userinfo?access_token={$accessToken}&openid={$openid}";
		$res = json_decode(self::get($url),TRUE);
		return self::checkIsSuc($res) ? $res : null;
	}
	
}
