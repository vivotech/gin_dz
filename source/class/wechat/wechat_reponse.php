<?php

/**
 * 关键词规则
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class wechat_reponse {
	
	public $handler = null;
	public $to = null;
	public $from = null;
	
	public function __construct($handler){
		$this->handler = $handler;
		$this->to = $this->handler->to;
		$this->from = $this->handler->from;
	}
	
	private function _format2xml($nodes) {
		$xml = '<xml>'
			. '<ToUserName><![CDATA[%s]]></ToUserName>'
			. '<FromUserName><![CDATA[%s]]></FromUserName>'
			. '<CreateTime>%s</CreateTime>'
			. '%s'
			. '</xml>';
		$return = sprintf(
			$xml, $this->handler->to, $this->handler->from, TIMESTAMP, $nodes
		);
		return $return;
	}

	public function getXml4Txt($txt) {
		$xml = '<MsgType><![CDATA[text]]></MsgType>'
			. '<Content><![CDATA[%s]]></Content>';
		echo self::_format2xml(
				sprintf(
					$xml, $txt
				)
		);
	}

	public function getXml4ImgByMid($mid) {
		$xml = '<MsgType><![CDATA[image]]></MsgType>'
			. '<Image>'
			. '<MediaId><![CDATA[%s]]></MediaId>'
			. '</Image>';
		echo self::_format2xml(
				sprintf(
					$xml, $mid
				)
		);
	}

	public function getXml4VoiceByMid($mid) {
		$xml = '<MsgType><![CDATA[voice]]></MsgType>'
			. '<Voice>'
			. '<MediaId><![CDATA[%s]]></MediaId>'
			. '</Voice>';
		echo self::_format2xml(
				sprintf(
					$xml, $mid
				)
		);
	}

	public function getXml4VideoByMid($mid, $title, $desc = '') {
		$desc = '' !== $desc ? $desc : $title;
		$xml = '<MsgType><![CDATA[video]]></MsgType>'
			. '<Video>'
			. '<MediaId><![CDATA[%s]]></MediaId>'
			. '<Title><![CDATA[%s]]></Title>'
			. '<Description><![CDATA[%s]]></Description>'
			. '</Video>';

		echo self::_format2xml(
				sprintf(
					$xml, $mid, $title, $desc
				)
		);
	}

	public function getXml4MusicByUrl($url, $thumbmid, $title, $desc = '', $hqurl = '') {
		$xml = '<MsgType><![CDATA[music]]></MsgType>'
			. '<Music>'
			. '<Title><![CDATA[%s]]></Title>'
			. '<Description><![CDATA[%s]]></Description>'
			. '<MusicUrl><![CDATA[%s]]></MusicUrl>'
			. '<HQMusicUrl><![CDATA[%s]]></HQMusicUrl>'
			. '<ThumbMediaId><![CDATA[%s]]></ThumbMediaId>'
			. '</Music>';

		echo self::_format2xml(
				sprintf(
					$xml, $title, '' === $desc ? $title : $desc, $url, $hqurl ? $hqurl : $url, $thumbmid
				)
		);
	}

	public function getXml4RichMsgByArray($list) {
		$max = 10;
		$i = 0;
		$ii = count($list);
		$list_xml = '';
		while ($i < $ii && $i < $max) {
			$item = $list[$i++];
			$list_xml .=
				sprintf(
				'<item>'
				. '<Title><![CDATA[%s]]></Title> '
				. '<Description><![CDATA[%s]]></Description>'
				. '<PicUrl><![CDATA[%s]]></PicUrl>'
				. '<Url><![CDATA[%s]]></Url>'
				. '</item>', $item['title'], $item['desc'], $item['pic'], $item['url']
			);
		}

		$xml = '<MsgType><![CDATA[news]]></MsgType>'
			. '<ArticleCount>%s</ArticleCount>'
			. '<Articles>%s</Articles>';

		echo self::_format2xml(
				sprintf(
					$xml, $i, $list_xml
				)
		);
	}
	
	//转发消息到多客服
	function getXML4CustomerService($param,$tokefu=''){
		$xml = '<xml>'
			. '<ToUserName><![CDATA[%s]]></ToUserName>'
			. '<FromUserName><![CDATA[%s]]></FromUserName>'
			. '<CreateTime>%s</CreateTime>'
			. '<MsgType><![CDATA[transfer_customer_service]]></MsgType>'
			. (!empty($tokefu)&&strstr($tokefu,'@')?'<TransInfo><KfAccount><![CDATA['.$tokefu.']]></KfAccount></TransInfo>':'')
			. '</xml>';
		$return = sprintf(
			$xml, $param['from'], $param['to'], $param['time']
		);
		echo $return;
	}
	
}
