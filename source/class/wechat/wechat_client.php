<?php

if (!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class wechat_client{

	// SETTING
	// Tail this file N U will see.
	public static $_URL_API_ROOT = 'https://api.weixin.qq.com';
	public static $_URL_FILE_API_ROOT = 'http://file.api.weixin.qq.com';
	public static $_URL_QR_ROOT = 'http://mp.weixin.qq.com';
	public static $_QRCODE_TICKET_DEFAULT_ID = 1;
	public static $ERRCODE_MAP = array(
	    '-1'	=> '系统繁忙，此时请开发者稍候再试',
	    '0'		=> '请求成功',
	    '40001' => '获取access_token时AppSecret错误，或者access_token无效。',
	    '40002' => '不合法的凭证类型',
	    '40003' => '不合法的OpenID，请开发者确认OpenID（该用户）是否已关注公众号，或是否是其他公众号的OpenID',
	    '40004' => '不合法的媒体文件类型',
	    '40005' => '不合法的文件类型',
	    '40006' => '不合法的文件大小',
	    '40007' => '不合法的媒体文件id',
	    '40008' => '不合法的消息类型',
	    '40009' => '不合法的图片文件大小',
	    '40010' => '不合法的语音文件大小',
	    '40011' => '不合法的视频文件大小',
	    '40012' => '不合法的缩略图文件大小',
	    '40013' => '不合法的AppID，请开发者检查AppID的正确性，避免异常字符，注意大小写',
	    '40014' => '不合法的access_token',
	    '40015' => '不合法的菜单类型',
	    '40016' => '不合法的按钮个数',
	    '40017' => '不合法的按钮个数',
	    '40018' => '不合法的按钮名字长度',
	    '40019' => '不合法的按钮KEY长度',
	    '40020' => '不合法的按钮URL长度',
	    '40021' => '不合法的菜单版本号',
	    '40022' => '不合法的子菜单级数',
	    '40023' => '不合法的子菜单按钮个数',
	    '40024' => '不合法的子菜单按钮类型',
	    '40025' => '不合法的子菜单按钮名字长度',
	    '40026' => '不合法的子菜单按钮KEY长度',
	    '40027' => '不合法的子菜单按钮URL长度',
	    '40028' => '不合法的自定义菜单使用用户',
	    '40029' => '不合法的oauth_code',
	    '40030' => '不合法的refresh_token',
	    '40031' => '不合法的openid列表',
	    '40032' => '不合法的openid列表长度',
	    '40033' => '不合法的请求字符，不能包含\uxxxx格式的字符',
	    '40035' => '不合法的参数',
	    '40038' => '不合法的请求格式',
	    '40039' => '不合法的URL长度',
	    '40050' => '不合法的分组id',
	    '40051' => '分组名字不合法',
	    '40117'	=> '分组名字不合法',
		'40118'	=> 'media_id大小不合法',
		'40119'	=> 'button类型错误',
		'40120'	=> 'button类型错误',
		'40121'	=> '不合法的media_id类型',
	    '41001' => '缺少access_token参数',
	    '41002' => '缺少appid参数',
	    '41003' => '缺少refresh_token参数',
	    '41004' => '缺少secret参数',
	    '41005' => '缺少多媒体文件数据',
	    '41006' => '缺少media_id参数',
	    '41007' => '缺少子菜单数据',
	    '41008' => '缺少oauth code',
	    '41009' => '缺少openid',
	    '42001' => 'access_token超时，请检查access_token的有效期',
	    '42002' => 'refresh_token超时',
	    '42003' => 'oauth_code超时',
	    '43001' => '需要GET请求',
	    '43002' => '需要POST请求',
	    '43003' => '需要HTTPS请求',
	    '43004' => '需要接收者关注',
	    '43005' => '需要好友关系',
	    '44001' => '多媒体文件为空',
	    '44002' => 'POST的数据包为空',
	    '44003' => '图文消息内容为空',
	    '44004' => '文本消息内容为空',
	    '45001' => '多媒体文件大小超过限制',
	    '45002' => '消息内容超过限制',
	    '45003' => '标题字段超过限制',
	    '45004' => '描述字段超过限制',
	    '45005' => '链接字段超过限制',
	    '45006' => '图片链接字段超过限制',
	    '45007' => '语音播放时间超过限制',
	    '45008' => '图文消息超过限制',
	    '45009' => '接口调用超过限制',
	    '45010' => '创建菜单个数超过限制',
	    '45015' => '回复时间超过限制',
	    '45016' => '系统分组，不允许修改',
	    '45017' => '分组名字过长',
	    '45018' => '分组数量超过上限',
	    '46001' => '不存在媒体数据',
	    '46002' => '不存在的菜单版本',
	    '46003' => '不存在的菜单数据',
	    '46004' => '不存在的用户',
	    '47001' => '解析JSON/XML内容错误',
	    '48001' => 'api功能未授权，请确认公众号已获得该接口，可以在公众平台官网-开发者中心页中查看接口权限',
	    '50001' => '用户未授权该api',
	    '50002' => '用户受限，可能是违规后接口被封禁',
	    '61451' => '参数错误(invalid parameter)',
	    '61452' => '无效客服账号(invalid kf_account)',
	    '61453'	=>'客服帐号已存在(kf_account exsited)',
		'61454'	=> '客服帐号名长度超过限制(仅允许10个英文字符，不包括@及@后的公众号的微信号)(invalid kf_acount length)',
		'61455'	=> '客服帐号名包含非法字符(仅允许英文+数字)(illegal character in kf_account)',
		'61456'	=> '客服帐号个数超过限制(10个客服账号)(kf_account count exceeded)',
		'61457'	=> '无效头像文件类型(invalid file type)',
		'61450'	=> '系统错误(system error)',
		'61500'	=> '日期格式错误',
		'61501'	=> '日期范围错误',
		'9001001' => 'POST数据参数不合法',
		'9001002' => '远端服务不可用',
		'9001003' => 'Ticket不合法',
		'9001004' => '获取摇周边用户信息失败',
		'9001005' => '获取商户信息失败',
		'9001006' =>'获取OpenID失败',
		'9001007' => '上传文件缺失',
		'9001008' => '上传素材的文件类型不合法',
		'9001009' => '上传素材的文件尺寸不合法',
		'9001010' => '上传失败',
		'9001020' =>'帐号不合法',
		'9001021' => '已有设备激活率低于50%，不能新增设备',
		'9001022' => '设备申请数不合法，必须为大于0的数字',
		'9001023' => '已存在审核中的设备ID申请',
		'9001024' => '一次查询设备ID数量不能超过50',
		'9001025' =>'设备ID不合法',
		'9001026' => '页面ID不合法',
		'9001027' => '页面参数不合法',
		'9001028' =>'一次删除页面ID数量不能超过10',
		'9001029' => '页面已应用在设备中，请先解除应用关系再删除',
		'9001030' =>'一次查询页面ID数量不能超过50',
		'9001031' => '时间区间不合法',
		'9001032' => '保存设备与页面的绑定关系参数错误',
		'9001033' => '门店ID不合法',
		'9001034' => '设备备注信息过长',
		'9001035' => '设备申请参数不合法',
		'9001036' => '查询起始值begin不合法'
	);
	public static $_USERINFO_LANG = 'en';
	// DATA
	private $_appid;
	private $_appsecret;
	private static $_accessTokenCache = array();
	private static $ERROR_LOGS = array();
	public static $ERROR_NO = 0;

	public function __construct($appid, $appsecret = '') {
		if ($appsecret) {
			$this->_appid = $appid;
			$this->_appsecret = $appsecret;
		} else {
			//通过weid获取公众账号信息
			$info = wechat_base::getWechatByWeid($appid);
			$this->_appid = $info['key'];
			$this->_appsecret = $info['secret'];
		}
	}
	
	public static function error() {
		return self::$ERRCODE_MAP[self::$ERROR_NO] ? self::$ERRCODE_MAP[self::$ERROR_NO] : self::$ERROR_NO;
	}

	public static function checkIsSuc($res,$debug=false) {
		$result = true;
		if (is_string($res)) {
			$res = json_decode($res, true);
			if($debug){
				return $res;
			}
		}
		if (isset($res['errcode']) && ( 0 !== (int) $res['errcode'])) {
			array_push(self::$ERROR_LOGS, $res);
			$result = false;
			self::$ERROR_NO = $res['errcode'];
		}
		return $result;
	}

	/**
	 * @method get
	 * @static
	 * @param  {string}
	 * @return {string|boolen}
	 */
	public static function get($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		# curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

		if (!curl_exec($ch)) {
			error_log(curl_error($ch));
			$data = '';
		} else {
			$data = curl_multi_getcontent($ch);
		}
		curl_close($ch);
		return $data;
	}

	/**
	 * @method post
	 * @static
	 * @param  {string}        $url URL to post data to
	 * @param  {string|array}  $data Data to be post
	 * @return {string|boolen} Response string or false for failure.
	 */
	public static function post($url, $data) {
		if (!function_exists('curl_init')) {
			return '';
		}
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		# curl_setopt( $ch, CURLOPT_HEADER, 1);

		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		$data = curl_exec($ch);
		if (!$data) {
			error_log(curl_error($ch));
		}
		curl_close($ch);
		return $data;
	}

	public function getAccessToken($tokenOnly = 1, $nocache = 0) {
		global $_G;
		$myTokenInfo = null;
		$appid = $this->_appid;
		$appsecret = $this->_appsecret;
		$cachename = 'wechatat_' . $appid;
		loadcache($cachename);

		if ($nocache || empty(self::$_accessTokenCache[$appid])) {
			self::$_accessTokenCache[$appid] = $_G['cache'][$cachename];
		}

		// check cache
		if (!empty(self::$_accessTokenCache[$appid])) {
			$myTokenInfo = self::$_accessTokenCache[$appid];
			if (time() < $myTokenInfo['expiration']) {
				return $tokenOnly ? $myTokenInfo['token'] : $myTokenInfo;
			}
		}

		// get new token
		$url = self::$_URL_API_ROOT . "/cgi-bin/token?grant_type=client_credential&appid=$appid&secret=$appsecret";

		$json = self::get($url);
		$res = json_decode($json, true);

		if (self::checkIsSuc($res)) {
			// update cache
			self::$_accessTokenCache[$appid] = $myTokenInfo = array(
			    'token' => $res['access_token'],
			    'expiration' => time() + (int) $res['expires_in']
			);
			savecache($cachename, $myTokenInfo);
		}
		return $tokenOnly ? $myTokenInfo['token'] : $myTokenInfo;
	}

	public function setAccessToken($tokenInfo) {
		if ($tokenInfo) {
			$appid = $this->_appid;
			self::$_accessTokenCache[$appid] = array(
			    'token' => $tokenInfo['token'],
			    'expire' => $tokenInfo['expire']
			);
		}
	}
	
	/******* JS-SDK *******/
	public function getJsApiTicket(){
		global $_G;
		$appid = $this->_appid;
		$appsecret = $this->_appsecret;
		$cachename = 'wxjsticket_' . $appid;
		loadcache($cachename);
		$access_token = $this->getAccessToken();
		$url = self::$_URL_API_ROOT ."/cgi-bin/ticket/getticket?type=jsapi&access_token={$access_token}";
		if(empty($_G['cache'][$cachename]) || time() > $_G['cache'][$cachename]['expiration']){
			$json = self::get($url);
			$res = json_decode($json, true);
			if(strtolower($res['errmsg'])=='ok'){
				$data['ticket'] = $res['ticket'];
				$data['expiration'] = time() + 7000;
				//缓存到系统
				savecache($cachename,$data);
				return $data['ticket'];
			}
		}else{
			return $_G['cache'][$cachename]['ticket'];
		}
	}
	
	public function getJsApiSignPackage(){
		$ticket = $this->getJsApiTicket();
		// 注意 URL 一定要动态获取，不能 hardcode.
    	$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    	$url = "$protocol$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$timestamp = TIMESTAMP;
    	$nonceStr = $this->createNonceStr();
		// 这里参数的顺序要按照 key 值 ASCII 码升序排序
	    $string = "jsapi_ticket={$ticket}&noncestr={$nonceStr}&timestamp={$timestamp}&url={$url}";
	    $signature = sha1($string);
	    $signPackage = array(
	      "appId"     => $this->_appid,
	      "nonceStr"  => $nonceStr,
	      "timestamp" => $timestamp,
	      "url"       => $url,
	      "signature" => $signature,
	      "rawString" => $string,
	    );
	    return $signPackage; 
	}
	
	private function createNonceStr($length = 16) {
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		$str = "";
	    for ($i = 0; $i < $length; $i++) {
	      $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
	    }
	    return $str;
	}
	
	/*********   JS-SDK end  ***********/
	
	// *************** media file upload/download ************
	public function upload($type, $file_path, $mediaidOnly = 1) {
		$access_token = $this->getAccessToken();
		$url = self::$_URL_FILE_API_ROOT . "/cgi-bin/media/upload?access_token=$access_token&type=$type";

		$res = self::post($url, array('media' => "@$file_path"));
		$res = json_decode($res, true);

		if (self::checkIsSuc($res)) {
			return $mediaidOnly ? $res['media_id'] : $res;
		}
		return null;
	}

	public function download($mid) {
		$access_token = $this->getAccessToken();
		$url = self::$_URL_FILE_API_ROOT . "/cgi-bin/media/get?access_token=$access_token&media_id=$mid";

		return self::get($url);
	}

	// *************** MENU ******************
	public function getMenu() {

		$access_token = $this->getAccessToken();
		$url = self::$_URL_API_ROOT . "/cgi-bin/menu/get?access_token=$access_token";

		$json = self::get($url);

		$res = json_decode($json, true);
		if (self::checkIsSuc($res)) {
			return $res;
		}
		return null;
	}

	public function deleteMenu() {
		$access_token = $this->getAccessToken();
		$url = self::$_URL_API_ROOT . "/cgi-bin/menu/delete?access_token=$access_token";

		$res = self::get($url);
		return self::checkIsSuc($res);
	}

	public function setMenu($myMenu) {
		$access_token = $this->getAccessToken();
		$url = self::$_URL_API_ROOT . "/cgi-bin/menu/create?access_token=$access_token";

		if (defined('JSON_UNESCAPED_UNICODE')) {
			$json = is_string($myMenu) ? $myMenu : json_encode($myMenu, JSON_UNESCAPED_UNICODE);
		} else {
			$json = is_string($myMenu) ? $myMenu : json_encode($myMenu);
		}

		$json = urldecode($json);
		$res = self::post($url, $json);

		return self::checkIsSuc($res);
	}

	// *************** 主动发送消息 ******************
	private function _send($to, $type, $data) {
		$access_token = $this->getAccessToken();
		$url = self::$_URL_API_ROOT . "/cgi-bin/message/custom/send?access_token=$access_token";

		$json = json_encode(
			array(
			    'touser' => $to,
			    'msgtype' => $type,
			    $type => $data
			),
			JSON_UNESCAPED_UNICODE
		);

		$res = self::post($url, $json);

		return self::checkIsSuc($res);
	} 

	public function sendTextMsg($to, $msg) {
		return $this->_send($to, 'text', array('content' => $msg));
	}

	public function sendImgMsg($to, $mid) {
		return $this->_send($to, 'image', array('media_id' => $mid));
	}

	public function sendVoice($to, $mid) {
		return $this->_send($to, 'voice', array('media_id' => $mid));
	}

	public function sendVideo($to, $mid, $title, $desc) {
		return $this->_send($to, 'video', array(
			    'media_id' => $mid,
			    'title' => $title,
			    'description' => $desc
		));
	}

	public function sendMusic($to, $url, $thumb_mid, $title, $desc = '', $hq_url = '') {
		return $this->_send($to, 'music', array(
			    'media_id' => $mid,
			    'title' => $title,
			    'description' => $desc || $title,
			    'musicurl' => $url,
			    'thumb_media_id' => $thumb_mid,
			    'hqmusicurl' => $hq_url || $url
		));
	}
	
	//发送图文
	public function sendRichMsg($to, $articles) {

		return $this->_send($to, 'news', array(
			    'articles' => self::_filterForRichMsg($articles)
		));
	}
	
	static private function _filterForRichMsg($articles) {
		$i = 0;
		$ii = count($articles);
		$list = array('title', 'desc', 'url', 'pic');
		$result = array();
		while ($i < $ii) {
			$currentArticle = $articles[$i++];
			try {
				array_push($result, array(
				    'title' => $currentArticle['title'],
				    'description' => $currentArticle['desc'],
				    'url' => $currentArticle['url'],
				    'picurl' => $currentArticle['pic']
				));
			} catch (Exception $e) {

			}
		}
		return $result;
	}

	public function uploadNews($articles) {
		$i = 0;
		$ii = count($articles);
		$result = array();
		while ($i < $ii) {
			$currentArticle = $articles[$i++];
			try {
				array_push($result, array(
				    'thumb_media_id' => $currentArticle['thumb_media_id'],
				    'title' => $this->convertToUtf($currentArticle['title']),
				    'content' => $this->convertToUtf($currentArticle['content']),
				    'author' => $this->convertToUtf($currentArticle['author']),
				    'content_source_url' => $this->convertToUtf($currentArticle['url']),
				    'digest' => $this->convertToUtf($currentArticle['desc']),
				    'show_cover_pic' => 1
				));
			} catch (Exception $e) {

			}
		}

		$access_token = $this->getAccessToken();
		$url = self::$_URL_API_ROOT . "/cgi-bin/media/uploadnews?access_token=$access_token";
		if (defined('JSON_UNESCAPED_UNICODE')) {
			$json = json_encode(array('articles' => $result), JSON_UNESCAPED_UNICODE);
		} else {
			$json = json_encode(array('articles' => $result));
		}

		$json = urldecode($json);

		$res = self::post($url, $json);
		if (self::checkIsSuc($res)) {
			return json_decode($res, true);
		} else {
			return false;
		}
	}

	public function sendMassMsg($msg) {
		$access_token = $this->getAccessToken();
		$url = self::$_URL_API_ROOT . "/cgi-bin/message/mass/sendall?access_token=$access_token";
		$post = array();
		$post['filter'] = array('group_id' => $msg['group_id']);
		if ($msg['type'] == 'media') {
			$post['mpnews'] = array('media_id' => $msg['media_id']);
			$post['msgtype'] = 'mpnews';
		} else {
			$post['text'] = array('content' => $this->convertToUtf($msg['text']));
			$post['msgtype'] = 'text';
		}

		if (defined('JSON_UNESCAPED_UNICODE')) {
			$json = json_encode($post, JSON_UNESCAPED_UNICODE);
		} else {
			$json = json_encode($post);
		}

		$json = urldecode($json);

		$res = self::post($url, $json);
		if (self::checkIsSuc($res)) {
			return json_decode($res, true);
		} else {
			return false;
		}
	}

	function convertToUtf($str) {
		return urlencode(diconv($str, CHARSET, 'UTF-8'));
	}


	// *************** followers admin ******************
	// follower group
	public function createGroup($name) {
		$access_token = $this->getAccessToken();
		$url = self::$_URL_API_ROOT . "/cgi-bin/groups/create?access_token=$access_token";

		$res = self::post($url, json_encode(array(
			    'group' => array('name' => $name)
		)));

		$res = json_decode($res, true);
		return self::checkIsSuc($res) ? $res['group']['id'] : null;
	}

	public function renameGroup($gid, $name) {
		$access_token = $this->getAccessToken();
		$url = self::$_URL_API_ROOT . "/cgi-bin/groups/update?access_token=$access_token";

		$res = self::post($url, json_encode(array(
			    'group' => array(
				'id' => $gid,
				'name' => $name
			    )
		)));

		$res = json_decode($res, true);
		return self::checkIsSuc($res);
	}

	public function moveUserById($uid, $gid) {
		$access_token = $this->getAccessToken();
		$url = self::$_URL_API_ROOT . "/cgi-bin/groups/members/update?access_token=$access_token";

		$res = self::post(
				$url, json_encode(
					array(
					    'openid' => $mid,
					    'to_groupid' => $gid
					)
				)
		);

		$res = json_decode($res, true);
		return self::checkIsSuc($res);
	}

	public function getAllGroups() {
		$access_token = $this->getAccessToken();
		$url = self::$_URL_API_ROOT . "/cgi-bin/groups/get?access_token=$access_token";

		$res = json_decode(self::get($url), true);

		if (self::checkIsSuc($res)) {
			return $res['groups'];
		} else {
			return null;
		}
	}

	public function getGroupidByUserid($uid) {
		$access_token = $this->getAccessToken();
		$url = self::$_URL_API_ROOT . "/cgi-bin/groups/getid?access_token=$access_token";

		$res = self::post($url, json_encode(array(
			    'openid' => $mid
		)));

		$res = json_decode($res, true);
		return self::checkIsSuc($res) ? $res['groupid'] : null;
	}

	// *************** Followers info ******************
	public function getUserInfoById($uid, $lang = '') {
		if (!$lang) {
			$lang = self::$_USERINFO_LANG;
		}
		$access_token = $this->getAccessToken();
		$url = self::$_URL_API_ROOT . "/cgi-bin/user/info?access_token=$access_token&openid=$uid&lang=$lang";

		$res = json_decode(self::get($url), true);

		return self::checkIsSuc($res) ? $res : null;
	}

	public function getFollowersList($next_id = '') {
		$access_token = $this->getAccessToken();
		$extend = '';
		if ($next_id) {
			$extend = "&next_openid=$next_id";
		}
		$url = self::$_URL_API_ROOT . "/cgi-bin/user/get?access_token=${access_token}$extend";

		$res = json_decode(
			self::get($url), true
		);

		return self::checkIsSuc($res) ? array(
		    'total' => $res['total'],
		    'list' => $res['data']['openid'],
		    'next_id' => isset($res['next_openid']) ? $res['next_openid'] : null
			) : null;
	}

	// ************************** OAuth *****************
	public function getOAuthConnectUri($redirect_uri, $state = '', $scope = 'snsapi_base') {
		$redirect_uri = urlencode($redirect_uri);
		$url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid={$this->_appid}&redirect_uri={$redirect_uri}&response_type=code&scope={$scope}&state={$state}#wechat_redirect";
		return $url;
	}

	public function getAccessTokenByCode($code) {
		$url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid={$this->_appid}&secret={$this->_appsecret}&code=$code&grant_type=authorization_code";
		$res = json_decode(self::get($url), true);
		return $res;
	}

	public function refreshAccessTocken($refresh_token) {
		$url = "https://api.weixin.qq.com/sns/oauth2/refresh_token?appid={$this->_appid}&grant_type=refresh_token&refresh_token=$refresh_token";
		$res = json_decode(self::get($url), true);
		return $res;
	}

	public function getUserInfoByAuth($access_token, $openid, $lang = 'zh_CN') {
		$url = "https://api.weixin.qq.com/sns/userinfo?access_token={$access_token}&openid={$openid}&lang={$lang}";
		$res = json_decode(self::get($url), true);
		return $res;
	}

	// ************************** qr code *****************
	public static function getQrcodeImgByTicket($ticket) {
		return self::get($this->getQrcodeImgUrlByTicket($ticket));
	}

	public static function getQrcodeImgUrlByTicket($ticket) {
		$ticket = urlencode($ticket);
		return self::$_URL_QR_ROOT . "/cgi-bin/showqrcode?ticket=$ticket";
	}

	public function getQrcodeTicket($options = array()) {
		$access_token = $this->getAccessToken();

		$scene_id = isset($options['scene_id']) ? (int) $options['scene_id'] : 0;
		$expire = isset($options['expire']) ? (int) $options['expire'] : 0;
		$ticketOnly = isset($options['ticketOnly']) ? $options['ticketOnly'] : 0;

		$url = self::$_URL_API_ROOT . "/cgi-bin/qrcode/create?access_token=$access_token";
		$data = array(
		    'action_name' => 'QR_LIMIT_SCENE',
		    'action_info' => array(
				'scene' => array(
				    'scene_id' => $scene_id
				)
		    )
		);
		if ($expire) {
			$data['expire_seconds'] = $expire;
			$data['action_name'] = 'QR_SCENE';
		}

		if ($data['action_name'] == 'QR_LIMIT_SCENE' && $scene_id > 100000) {
			$data['action_info']['scene']['scene_id'] = self::$_QRCODE_TICKET_DEFAULT_ID;
		}

		$data = json_encode($data);

		$res = self::post($url, $data);
		$res = json_decode($res, true);
		
		if (self::checkIsSuc($res)) {
			return $ticketOnly ? $res['ticket'] : array(
			    'ticket' => $res['ticket'],
			    'expire' => $res['expire_seconds'],
			    'url'	 => $res['url']
			);
		}
		return null;
	}
	
	/************** 素材管理 ***************/
	
	function getResourseCount($type = 'all'){
		$access_token = $this->getAccessToken();
		$url = self::$_URL_API_ROOT . "/cgi-bin/material/get_materialcount?access_token=$access_token";
		$res = self::get($url);
		$res = json_decode($res, true);
		
		$type_arr = array('image','video','voice','news','all');
		$type = strtolower($type);
		$type = in_array($type, $type_arr) ? $type : 'all';
		if(self::checkIsSuc($res)){
			if($type=='all'){
				return $res;
			}else{
				return $res[$type.'_count'];
			}
		}
		return  FALSE;
	}
	
	//获取素材列表
	function getResourseList($type = 'news',$start = 0,$pagesize = 20){
		$type_arr = array('image','video','voice','news');
		$type = in_array($type, $type_arr) ? $type : 'news';
		$start = $start < 0 ? 0 : $start;
		$pagesize = $pagesize > 20 ? 20 : ( $pagesize < 1 ? 1 : $pagesize );
		
		$access_token = $this->getAccessToken();
		$url = self::$_URL_API_ROOT . "/cgi-bin/material/batchget_material?access_token=$access_token";
		$data = array(
			'type'	=> $type,
			'offset'=> $start,
			'count'	=> $pagesize
		);
		$data = json_encode($data);
		$res = self::post($url, $data);
		$res = json_decode($res, true);
		
		return self::checkIsSuc($res) ? $res : FALSE;
	}
	
	function getResourseItem( $media_id, $type = 'image' ){
		global $_G;
		$access_token = $this->getAccessToken();
		$url = self::$_URL_API_ROOT . "/cgi-bin/material/get_material?access_token=$access_token";
		$data = json_encode(array('media_id'=>$media_id));
		$res = self::post($url, $data);
		
		$type_arr = array('image','video','voice','news');
		$type = in_array($type, $type_arr) ? $type : 'image';
		//成功
		if(self::checkIsSuc($res)){
			//如果是图片 或者 语音 ，保存到本地并返回地址
			if($type=='image' || $type == 'voice'){
				$path = DISCUZ_ROOT.'data'.DS.'wechat'.DS.$type.DS;
				dmkdir($path);
				$filename = $media_id . ($type == 'image' ? '.jpg' : '.mp3');
				file_put_contents($path.$filename, $res);
				return $_G['siteurl'].'data/wechat/'.$type.'/'.$filename;
			}else{
				//如果是 图文 或者 视频，则直接返回JSON
				return json_decode($res, true);
			}
		}
		
		return false;
	}
	
	
}