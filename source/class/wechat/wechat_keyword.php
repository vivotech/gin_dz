<?php

/**
 * 处理微信消息
 * 404
 */

class wechat_keyword{
	
	public $keyword = '';
	public $reponser = null;
	public $wechat = null;
	public $wechatObj = null;
	
	function __construct($keyword,$wechat,$reponser){
		$this->keyword = $keyword;
		$this->reponser = $reponser;
		$this->wechat = $wechat;
		$this->wechatObj = buildWechatClientObj($wechat);
	}
	
	function response($passive=TRUE , $keyword = ''){
		//查找关键词
		$wechat = $this->wechat;
		$keyword = empty($keyword) ? $this->keyword : $keyword;
		
		$keyword = M('wechat_rule_keyword')->where(" keyword = '{$keyword}' AND keytype = 1 AND status = 1 ")->order("displayorder DESC")->find();
		if(!$keyword){
			//查找包含
			$keyword = M('wechat_rule_keyword')->where(" keyword LIKE '%{$keyword}%' AND keytype = 2 AND status = 1 ")->find();
		}
		if($keyword){
			$rule = M('wechat_rule')->find($keyword['rid']);
			if($rule['weid']==$wechat['weid']){
				//文字回复
				if($rule['type']==0){
					//被动回复
					if($passive){
						$this->reponser->getXml4Txt($rule['reply']);
					}else{
					//主动回复
						$this->wechatObj->sendTextMsg($this->reponser->to, $rule['reply']);
					}
				}
				//图文回复
				elseif($rule['type']==1){
					$new = M('wechat_reply_image')->find($rule['reply']);
					if($new){
						$richtext = array();
						$richtext[] = array('title'=>$new['title'],'desc'=>$new['description'],'pic'=>$new['thumb'],'url'=>empty($new['url'])?U('forum/wechat/article',array('newsid'=>$new['id']),TRUE):$new['url']);
						$news = M('wechat_reply_image')->where("parentid='{$new[id]}'")->select();
						if($news){
							foreach($news as $k=>$v){
								$richtext[] = array('title'=>$v['title'],'desc'=>$v['description'],'pic'=>$v['thumb'],'url'=>empty($v['url'])?U('forum/wechat/article',array('newsid'=>$v['id']),TRUE):$v['url']);
							}
						}
						//被动回复
						if($passive){
							$this->reponser->getXml4RichMsgByArray($richtext);
						}else{
						//主动回复
							$this->wechatObj->sendRichMsg($this->reponser->to, $richtext);
						}
					}
				}
			}
		}
		
	}
	
}
