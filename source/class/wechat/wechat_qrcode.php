<?php

/**
 * 带参数的二维码
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class wechat_qrcode{
	
	public $model = null;
	public $wechat = null;
	public $reponser = null;
	public $scene_id = 0;
	
	function __construct($wechat,$reponser,$scene_id=0){
		$this->wechat = $wechat;
		$this->reponser = $reponser;
		$this->model = M(__CLASS__);
		$this->scene_id = $scene_id;
	}
	
	function action($passive = true , $scene_id=0){
		$scene_id = $this->scene_id ? $this->scene_id : $scene_id;
		$row = $this->getRowBySceneId($scene_id);
		if(!$row){
			return FALSE;
		}
		
		//开始执行，如果func==0,为关键词处理
		if($row['func']==0){
			$keyword = new wechat_keyword($row['param'],$this->wechat,$this->reponser);
			$keyword->response(FALSE);
		}
		
	}
	
	function getRowBySceneId($scene_id){
		return $this->model->where("scene_id = '{$scene_id}'")->find();
	}
	
	
}
