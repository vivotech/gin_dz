<?php

if (!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class wechat_server{
	
	private $wechat;
	private $_token;

	/**
	 * ****** 所有事件  ******
	 * receiveAllStart
	 * receiveMsg::text
	 * receiveMsg::location
	 * receiveMsg::image
	 * receiveMsg::video
	 * receiveMsg::link
	 * receiveMsg::voice
	 * receiveEvent::subscribe
	 * receiveEvent::unsubscribe
	 * receiveEvent::scan
	 * receiveEvent::location
	 * receiveEvent::click
	 * receiveAllEnd
	 * accessCheckSuccess
	 * 404
	 *
	 */

	public function __construct($wechat) {
		$this->wechat = $wechat;
		$this->_token = $wechat['token'];
		$this->accessDataPush();
	}

	private function _handler($type,$param = array()) {
		global $_G;
		//使用handler类处理消息
		if(!class_exists('wechat_handler')){
			return null;
		}
		$handler = new wechat_handler($this->wechat,$param['from'],$param['to']);
		if (!method_exists($handler, $type)) {
			return null;
		}
		return $handler->$type($param);
	}

	private function _checkSignature() {
		$signature = $_GET["signature"];
		$timestamp = $_GET["timestamp"];
		$nonce = $_GET["nonce"];

		$token = $this->_token;
		$tmpArr = array($token, $timestamp, $nonce);
		sort($tmpArr, SORT_STRING);
		$tmpStr = implode($tmpArr);
		$tmpStr = sha1($tmpStr);

		return $tmpStr == $signature;
	}

	private function _handlePostObj($postObj) {
		$MsgType = strtolower((string) $postObj->MsgType);
		$result = array(
		    'from' => (string) htmlspecialchars($postObj->FromUserName),
		    'to' => (string) htmlspecialchars($postObj->ToUserName),
		    'time' => (int) $postObj->CreateTime,
		    'type' => (string) $MsgType
		);

		if (property_exists($postObj, 'MsgId')) {
			$result['id'] = $postObj->MsgId;
		}
		
		switch ($result['type']) {
			case 'text':
				$result['content'] = (string) $postObj->Content; 
				break;
			
			case 'location':
				$result['X'] = (float) $postObj->Location_X; 
				$result['Y'] = (float) $postObj->Location_Y; 
				$result['S'] = (float) $postObj->Scale;      
				$result['I'] = (string) $postObj->Label;     
				break;

			case 'image':
				$result['url'] = (string) $postObj->PicUrl;  
				$result['mid'] = (string) $postObj->MediaId; 
				break;
				
			case 'shortvideo': //小视频
			case 'video':	//视频
				$result['mid'] = (string) $postObj->MediaId;      
				$result['thumbmid'] = (string) $postObj->ThumbMediaId; 
				break;
				
			case 'link':
				$result['title'] = (string) $postObj->Title;       
				$result['desc'] = (string) $postObj->Description; 
				$result['url'] = (string) $postObj->Url;  
				break;

			case 'voice':
				$result['mid'] = (string) $postObj->MediaId;    
				$result['format'] = (string) $postObj->Format;      
				if (property_exists($postObj, 'Recognition')) {
					$result['txt'] = (string) $postObj->Recognition; 
				}
				break; 

			case 'event':
				
				$result['event'] = strtolower((string) $postObj->Event);
				switch ($result['event']) {

					case 'subscribe': 
					case 'scan': 
						if (property_exists($postObj, 'EventKey')) {
							$result['key'] = str_replace(
								'qrscene_', '', (string) $postObj->EventKey
							);
							$result['ticket'] = (string) $postObj->Ticket;
						}
						break;
					case 'location': 
						$result['la'] = (string) $postObj->Latitude;
						$result['lo'] = (string) $postObj->Longitude;
						$result['p'] = (string) $postObj->Precision;
						break;

					case 'click':
						$result['key'] = (string) $postObj->EventKey;
						break;
					case 'masssendjobfinish':
						$result['msg_id'] = (string) $postObj->MsgID;
						$result['status'] = (string) $postObj->Status;
						$result['totalcount'] = (string) $postObj->TotalCount;
						$result['filtercount'] = (string) $postObj->FilterCount;
						$result['sentcount'] = (string) $postObj->SentCount;
						$result['errorcount'] = (string) $postObj->ErrorCount;
						break;
					//消息模板回调提醒
					case 'templatesendjobfinish' : 
						$result['msg_id'] = (string) $postObj->MsgID;
						$result['status'] = (string) $postObj->Status;
						break;
				}
				break;
				
		}

		return $result;
	}

	private function accessDataPush() {
		if (!$this->_checkSignature()) {
			if (!headers_sent()) {
				header('HTTP/1.1 404 Not Found');
				header('Status: 404 Not Found');
			}
			$this->_handler('404');
			return;
		}
		
		$postdata = file_get_contents("php://input");
		if ($postdata) {
			if (!$this->_checkSignature()) {
				return;
			}
			$postObj = simplexml_load_string($postdata, 'SimpleXMLElement', LIBXML_NOCDATA);
			$postObj = $this->_handlePostObj($postObj);
			
			$this->_handler('receiveAllStart', $postObj);
			
			// Call Special Request Handle Function
			if (isset($postObj['event'])) {
				$hookName = 'event_' . $postObj['event'];
			} else {
				$hookName = 'msg_' . $postObj['type'];
			}
			$this->_handler($hookName, $postObj);

			$this->_handler('receiveAllEnd', $postObj);
		} elseif (isset($_GET['echostr'])) {

			$this->_handler('accessCheckSuccess');
			// avoid of xss
			if (!headers_sent()) {
				header('Content-Type: text/plain');
			}
			echo preg_replace('/[^a-z0-9]/i', '', $_GET['echostr']);
		}
	}

}

