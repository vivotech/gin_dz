<?php

if (!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class wechat_base {
	
	public static function updateWechatMember($userInfo){
		global $_G;
		if(is_array($userInfo)&&isset($userInfo['unionid'])){
			$insertData = array(
				'weid'		=> $_G['wechat']['weid'],
				'uid'		=> 0,		//用户未绑定为0
				'openid'	=> $userInfo['openid'],
				'subscribe' => $userInfo['subscribe'],
				'nickname'	=> $userInfo['nickname'],
				'sex'		=> $userInfo['sex'],
				'province'	=> $userInfo['province'],
				'language'	=> $userInfo['language'],
				'city'		=> $userInfo['city'],
				'country'	=> $userInfo['country'],
				'headimgurl'=> $userInfo['headimgurl'],
				'subscribe_time' => $userInfo['subscribe_time'] ,
				'unionid'	=> $userInfo['unionid'],
				'addtime'	=> TIMESTAMP,
			);
			require_once libfile('function/wechat');
			$info = getWechatMemberByUnionid($userInfo['unionid']);
			if($info){
				//保持UID不更改
				$insertData['uid'] = $info['uid'];
				//头像比对,如果头像和之前的不一样，修改isheadimglocal字段为0，让forum/personal页面重新本地化
				if($insertData['headimgurl']!=$info['headimgurl']){
					$insertData['isheadimglocal'] = 0;
				}
				C::t('common_member_wechat')->update($info['id'],$insertData);
			}else{
				C::t('common_member_wechat')->insert($insertData);
			}
			return $insertData;
		}
		return FALSE;
	}
	
	public static function bindOpenId($uid, $openid, $isregister = 0) {
		require_once libfile('function/wechat');
		$info = getWechatMemberByOpenid($openid);
		if($info){
			return updataWechatMemberByID($info['id'],array('uid'=>intval($uid)));
		}
		return FALSE;
	}

	public static function updateAppInfo($extId, $appId = '', $appSecret = '') {
		global $_G;
		$wechatappInfos = unserialize($_G['setting']['wechatappInfos']);
		if ($appId) {
			$wechatappInfos[$extId] = array('appId' => $appId, 'appSecret' => $appSecret);
		} else {
			unset($wechatappInfos[$extId]);
		}
		$settings = array('wechatappInfos' => serialize($wechatappInfos));
		C::t('common_setting')->update_batch($settings);
		updatecache('setting');
	}
	
	public static function getWechatByWeid($weid){
		return C::t('wechats')->fetch(intval($weid));
	}

	public static function updateResponse($data, $extId = '') {
		$response = self::getResponse($extId);
		foreach ($data as $key => $value) {
			if ($value) {
				if ($value['plugin'] && $value['include'] && $value['class'] && $value['method']) {
					$response[$key] = $value;
				}
			} else {
				unset($response[$key]);
			}
		}
		if (!$extId) {
			$settings = array('wechatresponse' => serialize($response));
		} else {
			global $_G;
			$wechatresponseExts = unserialize($_G['setting']['wechatresponseExts']);
			if ($data) {
				$wechatresponseExts[$extId] = $response;
			} else {
				unset($wechatresponseExts[$extId]);
			}
			$settings = array('wechatresponseExts' => serialize($wechatresponseExts));
		}
		C::t('common_setting')->update_batch($settings);
		updatecache('setting');
		return $response;
	}

	public static function getResponse($extId = '') {
		global $_G;
		if (!$extId) {
			return unserialize($_G['setting']['wechatresponse']);
		} else {
			$wechatresponseExts = unserialize($_G['setting']['wechatresponseExts']);
			return $wechatresponseExts[$extId];
		}
	}

	public static function updateRedirect($value) {
		if (!$value || $value['plugin'] && $value['include'] && $value['class'] && $value['method']) {
			$settings = array('wechatredirect' => $value);
			C::t('common_setting')->update_batch($settings);
			updatecache('setting');
		}
	}

	public static function getRedirect() {
		global $_G;
		return unserialize($_G['setting']['wechatredirect']);
	}

	public static function getViewPluginId() {
		global $_G;
		return $_G['setting']['wechatviewpluginid'];
	}

	public static function updateViewPluginId($value) {
		$settings = array('wechatviewpluginid' => $value);
		C::t('common_setting')->update_batch($settings);
		updatecache('setting');
	}

	public static function updateAPIHook($datas) {
		$apihook = self::getAPIHook();
		foreach ($datas as $data) {
			foreach ($data as $key => $value) {
				if (!$value['plugin']) {
					continue;
				}
				list($module, $hookname) = explode('_', $key);
				if ($value['include'] && $value['class'] && $value['method']) {
					$v = $value;
					unset($v['plugin']);
					$v['allow'] = 1;
					$apihook[$module][$hookname][$value['plugin']] = $v;
				} else {
					unset($apihook[$module][$hookname][$value['plugin']]);
				}
			}
		}
		$settings = array('mobileapihook' => serialize($apihook));
		C::t('common_setting')->update_batch($settings);
		updatecache('setting');
		return $apihook;
	}

	public static function getAPIHook($getplugin = '') {
		global $_G;
		$data = unserialize($_G['setting']['mobileapihook']);
		if (!$getplugin) {
			return $data;
		} else {
			foreach ($data as $key => $hooknames) {
				foreach ($hooknames as $hookname => $plugins) {
					foreach ($plugins as $plugin => $value) {
						if ($getplugin != $plugin) {
							unset($data[$key][$hookname][$plugin]);
						}
					}
				}
			}
			return $data;
		}
	}

	public static function delAPIHook($getplugin) {
		if (!$getplugin) {
			return;
		}
		$getplugins = (array) $getplugin;
		$apihook = self::getAPIHook();
		foreach ($apihook as $key => $hooknames) {
			foreach ($hooknames as $hookname => $plugins) {
				foreach ($plugins as $plugin => $value) {
					if (in_array($plugin, $getplugins)) {
						unset($apihook[$key][$hookname][$plugin]);
					}
				}
			}
		}
		$settings = array('mobileapihook' => serialize($apihook));
		C::t('common_setting')->update_batch($settings);
		updatecache('setting');
		return $apihook;
	}

	public static function getPluginUrl($pluginid, $param = array()) {
		global $_G;
		if (in_array('plugin', $_G['setting']['rewritestatus'])) {
			$url = $_G['siteurl'] . rewriteoutput('plugin', 1, 'wechat', 'access') . '?';
		} else {
			$url = $_G['siteurl'] . 'plugin.php?id=wechat:access&';
		}
		$url .= 'pluginid=' . urlencode($pluginid) . '&param=' . urlencode(base64_encode(http_build_query($param)));
		return $url;
	}

}