<?php

/**
 * APP认证类
 */

if (!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

include_once libfile('app/member','class');

class wechat_logging_ctl extends app_logging_ctl {
	
}

class wechat_register_ctl extends  app_register_ctl {
	
	function updataWechatMember($param,$ext_param){
		require_once libfile('function/wechat');
		//更新微信用户表关联
		updataWechatMemberByID($ext_param['id'],array('uid'=>$param['uid']));
		//更新用户昵称
		C::t('common_member_profile')->update_by_uid($param['uid'],array('nickname'=>$ext_param['nickname']));
		//更新用户头像
//		require_once libfile('function/member');
//		setAvatarFromRemoteUrl($ext_param['headimgurl'],$param['uid']);
		return array('headimgurl'=>$ext_param['headimgurl'],'uid'=>$param['uid']);
	}
	
}

?>