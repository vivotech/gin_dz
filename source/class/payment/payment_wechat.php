<?php

if (!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class payment_wechat extends payment_base{
	
	public function __construct($orderInfo){
		parent::__construct($orderInfo);
        global $_G;
		$this->payconfig['SSLCERT_PATH'] = DISCUZ_ROOT . $this->payconfig['SSLCERT_PATH'];
		$this->payconfig['SSLKEY_PATH'] = DISCUZ_ROOT . $this->payconfig['SSLKEY_PATH'];
        $_G['wechatpay'] = $this->payconfig;
	}
	
	public function pay(){
		global $_G;

		if(!$this->check_payment()){
			showmessage('支付方式不支持！');
		}
		
		require_once libfile('wechat/pay','class');
		$orderInfo = $this->orderInfo;

		//使用统一支付接口
		$unifiedOrder = new UnifiedOrder_pub();
		
		//微信js - api支付
		if(IN_WECHAT){
			
			$out_trade_no = $orderInfo['o_openid'];//订单ID,确保订单只支付一次
			$unifiedOrder->setParameter("body","意大利原生活，订单号：{$out_trade_no}");//商品描述
			$unifiedOrder->setParameter("out_trade_no","$out_trade_no");//商户订单号 
			//处理total_fee，单位为分，必须为整数值
			$total_fee = $orderInfo['totalprice'] * 100;
			$unifiedOrder->setParameter("total_fee","$total_fee");//总金额
			
			$paramAttach = array('oid'=>$orderInfo['oid'],'weid'=>$_G['wechat']['weid']);
			$paramAttach = json_encode($paramAttach,JSON_UNESCAPED_UNICODE);
			$unifiedOrder->setParameter("attach","$paramAttach");//订单ID
			$unifiedOrder->setParameter("notify_url",$_G['config']['wechatpay']['NOTIFY_URL']);//通知地址 
			
			//在微信内部，使用JS_API
			$openid = $_G['openid'];
			$unifiedOrder->setParameter("openid","$openid");//用户Openid
			$unifiedOrder->setParameter("trade_type","JSAPI");//交易类型
			
			$prepay_id = $unifiedOrder->getPrepayId();
			
			$jsApi = new JsApi_pub();
			$jsApi->setPrepayId($prepay_id);
			$jsApiParameters = $jsApi->getParameters();
			$jsApiParametersArray = json_decode($jsApiParameters,TRUE);

            $return_url = U("forum/order/state",array('oid' =>$orderInfo['oid']));
            $cancel_url = U("forum/order/detail",array('order_id'=>$orderInfo['oid']));
            //返回数据
            return <<<EOF

        <script >
            wx.ready(function () {
                wx.chooseWXPay({
                    timestamp: {$jsApiParametersArray['timeStamp']},
                    nonceStr: '{$jsApiParametersArray['nonceStr']}',
                    package: '{$jsApiParametersArray['package']}',
                    signType: '{$jsApiParametersArray['signType']}',
                    paySign: '{$jsApiParametersArray['paySign']}',
                    success: function (res) {
                        // 支付成功后的回调函数
                        location.href='{$return_url}';
                    },
                    cancel:function(res){
                        // 支付取消后的回调函数
                        location.href='{$cancel_url}';
                    }
                });
            });
        </script>

EOF;
		}
		//微信native原生支付
		else{
			
			//使用统一支付接口
			$out_trade_no = $orderInfo['o_openid'];//订单ID,确保订单只支付一次
			$unifiedOrder->setParameter("body","意大利原生活，订单号：{$out_trade_no}");//商品描述
			$unifiedOrder->setParameter("out_trade_no","$out_trade_no");//商户订单号 
			//处理total_fee，单位为分，必须为整数值
			$total_fee = $orderInfo['totalprice']*100;
			$unifiedOrder->setParameter("total_fee","$total_fee");//总金额
			
			$paramAttach = array('oid'=>$orderInfo['oid'],'weid'=>$_G['wechat']['weid']);
			$paramAttach = json_encode($paramAttach,JSON_UNESCAPED_UNICODE);
			$unifiedOrder->setParameter("attach","$paramAttach");//订单ID
			$unifiedOrder->setParameter("notify_url",$_G['config']['wechatpay']['NOTIFY_URL']);//通知地址 
			$unifiedOrder->setParameter("trade_type","NATIVE");//交易类型
			
			//获取统一支付接口结果
			$unifiedOrderResult = $unifiedOrder->getResult();
			//商户根据实际情况设置相应的处理流程
			if ($unifiedOrderResult["return_code"] == "FAIL") {
				showmessage("通信出错：".$unifiedOrderResult['return_msg']);
			}
			elseif($unifiedOrderResult["result_code"] == "FAIL"){
				showmessage("预支付失败（错误代码：".$unifiedOrderResult['err_code']."，错误代码描述：".$unifiedOrderResult['err_code_des'].'）');
			}
			elseif($unifiedOrderResult["code_url"] != NULL){
				//从统一支付接口获取到code_url
                $code_url = $unifiedOrderResult["code_url"];
                $wording = IN_MOBILE ? '请使用微信扫描二维码支付' : '请使用微信扫描二维码支付';
//				include  template('forum/order_pay');
                return <<<EOF

    <style type="text/css">
		body{
			background: #001A35;
			color: #fff;
		}
	</style>
	<script src="/static/js/qrcode.js"></script>
	<script>

        var url = "{$code_url}";
        //参数1表示图像大小，取值范围1-10；参数2表示质量，取值范围'L','M','Q','H'
        var qr = qrcode(10, 'M');
        qr.addData(url);
        qr.make();
        var wording=document.createElement('p');
        wording.innerHTML = "{$wording}";
        var code=document.createElement('DIV');
        code.innerHTML = qr.createImgTag(8);

        jQuery(function(){
            jQuery('.order-pay-page').empty().append(code).append(wording);
            jQuery('.order-pay-page').find('img').removeAttr('width').removeAttr('height');
            jQuery('.order-pay-page').find('p').css({'text-align':'center','font-size':'20px','margin-top':'10px'});
        });

		var timer = null;
		check_paystate();

		function check_paystate(){
			var _url = '/forum.php?mod=order&action=state';
			$.post(_url,{'oid':{$orderInfo['oid']},'ajax':1,'remote':0},function(data){
				var _data = $.parseJSON(data);
				var _order= _data.data;
				if(_data.result && _order.paystate == 1){
					$('#order_msg').html('<p>支付成功！！</p>');
					clearTimeout(timer);
					setTimeout(function(){
						//3秒后跳转
						location.href='/forum.php?mod=order&action=detail&order_id={$orderInfo["oid"]}';
					},3000);
				}else{
					//3秒轮询一次
					timer = setTimeout('check_paystate()',3000);
				}
			});
		}


	</script>

EOF;

			}
		}
		
	}

    /**
     * 发起主动查询
     * @param int $remote 是否远程操作
     */
    public function checkStatus($remote = 0){
        $order = $this->orderInfo;
        //当订单处于未支付的时候，调用微支付接口处理
        if($order['paystate']==0 && $remote==1){

            require_once libfile('wechat/pay','class');
            //使用订单查询接口
            $orderQuery = new OrderQuery_pub();
            $orderQuery->setParameter("out_trade_no","$order[o_openid]");//商户订单号
            $orderQueryResult = $orderQuery->getResult();

            if ($orderQueryResult["return_code"] == "FAIL") {
                //通信出错，重新查询
                dheader('location:'.U('forum/order/state','oid='.$order['oid']));
//				echo "通信出错：".$orderQueryResult['return_msg']."<br>";
//				exit();
            }
            elseif($orderQueryResult["result_code"] == "FAIL"){
                //直接判断为没有订单
                $page_show  = 'no_order';
//				echo "错误代码：".$orderQueryResult['err_code']."<br>";
//				echo "错误代码描述：".$orderQueryResult['err_code_des']."<br>";
            }
            else{
                $attach = json_decode($orderQueryResult['attach'],TRUE);
                if($attach['oid'] == $oid){
                    require_once libfile('function/wechat');
                    setWXPayState($oid,$attach['weid'],$orderQueryResult);
                    if(strtoupper(trim($orderQueryResult['trade_state']))=='SUCCESS'){
                        $order['paystate'] = 1;
                    }
                }
            }
        }

        return $order;

    }
	
}
