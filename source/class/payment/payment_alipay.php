<?php


if (!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class payment_alipay extends payment_base{

	function __construct($orderInfo){
		parent::__construct($orderInfo);
		//添加其他支付宝配置
		//签名方式 不需修改
		$this->payconfig['sign_type']    = strtoupper('MD5');
		//字符编码格式 目前支持 gbk 或 utf-8
		$this->payconfig['input_charset']= strtolower('utf-8');
		//ca证书路径地址，用于curl中ssl校验
		$this->payconfig['cacert']    = DISCUZ_ROOT.'data'.DS.'cacert'.DS.'alipay_cacert.pem';
		//访问模式,根据自己的服务器是否支持ssl访问，若支持请选择https；若不支持请选择http
		$this->payconfig['transport']    = 'http';
	}
	
	public function run($ajax=0){
		global $_G;
		//手动引入类
		require libfile('alipay/submit','class');
		
		$alipaySubmit = new alipay_submit($this->payconfig);
		
		//支付类型
        $payment_type = "1";
        //必填，不能修改
        //服务器异步通知页面路径
        $notify_url = $_G['siteurl']."payment.php";
        //需http://格式的完整路径，不能加?id=123这类自定义参数

        //页面跳转同步通知页面路径
        $return_url = $_G['siteurl']."alipay_return.php";
        //需http://格式的完整路径，不能加?id=123这类自定义参数，不能写成http://localhost/

        //商户订单号
        $out_trade_no = $this->orderInfo['o_openid'];
        //商户网站订单系统中唯一订单号，必填

        //订单名称
        $subject = "意大利原生活，订单号：{$out_trade_no}";
        //必填

        //付款金额
        $total_fee = $orderInfo['totalprice'];
        //必填

        //订单描述
        $body = "意大利原生活，订单号：{$out_trade_no}";
        //商品展示地址
        //http://192.168.1.88/forum.php?mod=product&action=view&spid=1
        $show_url = U('forum/product/view',array('spid'=>$orderInfo['spid']));
        //需以http://开头的完整路径，例如：http://www.商户网址.com/myorder.html

        //防钓鱼时间戳
        $anti_phishing_key = $alipaySubmit->query_timestamp();
        //若要使用请调用类文件submit中的query_timestamp函数

        //客户端的IP地址
        $exter_invoke_ip = $_G['clientip'];
        //非局域网的外网IP地址，如：221.0.0.1

		//构造要请求的参数数组，无需改动
		$parameter = array(
				"service" => "create_direct_pay_by_user",
				"partner" => trim($this->payconfig['partner']),
				"seller_email" => trim($this->payconfig['seller_email']),
				"payment_type"	=> $payment_type,
				"notify_url"	=> $notify_url,
				"return_url"	=> $return_url,
				"out_trade_no"	=> $out_trade_no,
				"subject"	=> $subject,
				"total_fee"	=> $total_fee,
				"body"	=> $body,
				"show_url"	=> $show_url,
				"anti_phishing_key"	=> $anti_phishing_key,
				"exter_invoke_ip"	=> $exter_invoke_ip,
				"_input_charset"	=> trim(strtolower($this->payconfig['input_charset']))
		);
		
		$html_text = $alipaySubmit->buildRequestForm($parameter,"get", "确认");
		echo $html_text;
	}

    public function checkStatus($remote=0){
        $order = $this->orderInfo;

        //查询，这里不做了

        return $order;
    }
	
}
