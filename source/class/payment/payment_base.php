<?php

if (!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

abstract class payment_base{
	
	public $orderInfo;
	public $payment;
	public $payconfig;
	
	public function __construct($orderInfo){
		global $_G;
		if(!$_G['cache']['payment']){
			loadcache('payment');
		}
		$this->payment = $_G['cache']['payment'][$orderInfo['payment']];
		$this->orderInfo = $orderInfo;
		$this->payconfig = unserialize($this->payment['payconfig']);
	}
	
	public function check_payment($payment=null){
		if($payment!=null){
			$this->payment = $payment;
		}
		if($this->payment['status']==0){
			return FALSE;
		}
		if($this->payment['is_mobile']==0 && defined('IN_MOBILE')){
			return FALSE;
		}
		return TRUE;
	}
	
	//发起支付
	abstract public function pay();

    //主动查询
    abstract public function checkStatus($remote=0);

}
