<?php

//微信后台
if(!defined('IN_DISCUZ') || !defined('IN_ADMINCP')) {
	exit('Access Denied');
}

require libfile('function/wechat');

$operation_array = array('userlist','douser','msg','menu','resourse','emoji','ajax','qrcode');

if(!in_array($operation, $operation_array)){
	cpmsg('没有该operation');
}

if($operation == 'userlist'){
	
	$tpl->do = $do = I('do','list','',array('list','detail','delete'));
	$tpl->shownav('topic', '微信账号管理');
	$topmenu = array(
		array('微信用户列表', 'wechat&operation=userlist&do=list' , $operation=='userlist'&&$do=='list'),
		array('搜索', 'wechat&operation=userlist&do=search' , $operation=='userlist'&&$do=='search'),
	);
	$tpl->showsubmenu('微信账号管理', $topmenu);
	$model = M('common_member_wechat');
	
	if($do=='list'){
		$pagesize = I('pagesize',10,'intval');
		$count = $model->count();
		$tpl->list = $list = $model->limit($pagesize*($_G['page']-1).','.$pagesize)->select();
		$tpl->multipage = multi($count, $pagesize, $_G['page'], 'admin.php?action=wechat&operation=userlist&do=list');
	}
	//详情
	elseif($do=='detail'){
		$wechatuid = I('wechatuid',0,'intval');
		$tpl->user = $user = $model->find($wechatuid);
	}
	//删除
	elseif($do=='delete'){
		$wechatuid = I('wechatuid',0,'intval');
		if($model->delete($wechatuid)){
			dheader('location:'.adminurl('wechat/userlist'));
		}
	}
}
//用户操作
elseif($operation=='douser'){
	$do = I('do','unbinduser');
	$id = I('id',0,'intval');
	
	if($do=='unbinduser'){
		$res = M('common_member_wechat')->where("id='{$id}'")->save(array('uid'=>0));
		cpmsg($res?'解绑成功！':'解绑失败！',adminurl('wechat/userlist',array('page'=>I('returnpage',1,'intval'))),$res?'success':'error');
	}
	
	exit();
}
//菜单设置
elseif($operation=='menu'){
	
	$tpl->do = $do = I('do','list','',array('list','search'));
	$tpl->shownav('topic', '微信账号管理');
	
	if(empty($_G['wechat'])) {
		cpmsg('请先设置默认公众号！！',adminurl('wechatbase/list',array('page'=>1)),'error');
	}
	
	$wechat =  $_G['wechat'];
//	print_r($wechat);
	$wechatObj = getDefalutWechatClient();

	if(IS_POST) {
		//模拟关键字搜索
		$do = I('do');
		if($do == 'search_key') {
			$condition = '';
			$key_word = trim($_GET['key_word']);
			if(!empty($key_word)) {
				$condition = " AND content LIKE '%{$key_word}%' ";
			}
			
			$data = pdo_fetchall('SELECT content FROM ' . tablename('rule_keyword') . " WHERE (weid = 0 OR weid = :weid) AND status != 0 " . $condition . ' ORDER BY weid DESC, displayorder DESC LIMIT 200', array(':weid' => $_W['weid']));
			$exit_da = array();
			if(!empty($data)) {
				foreach($data as $da) {
					$exit_da[] = $da['content'];
				}
			}
			exit(json_encode($exit_da));
		}
		
		if($do == 'remove') {
			$ret = $acc->menuDelete();
			if(is_error($ret)) {
				message($ret['message'], 'refresh');
			} else {
				isetcookie($menusetcookie, '', -500);
				message('已经成功删除菜单，请重新创建。', 'refresh');
			}
		}
		
		if($do == 'refresh') {
			isetcookie($menusetcookie, '', -500);
			message('已清空缓存，将重新从公众平台接口获取菜单信息。', 'refresh');
		}
	
		
		//$_POST['do']中保存菜单数据
		$mDat = $do;
		$mDat = htmlspecialchars_decode($mDat);
		$menus = json_decode($mDat, true);
		
		if(!is_array($menus)) {
			cpmsg_error('操作非法.');
		}
		
		foreach($menus as &$m) {
			$m['name'] = preg_replace_callback('/\:\:([0-9a-zA-Z_-]+)\:\:/', create_function('$matches', 'return utf8_bytes(hexdec($matches[1]));'), $m['name']);
			$m['name'] = urlencode($m['name']);
			if(isset($m['url']) && !empty($m['url'])){
				$m['url'] = smartUrlEncode($m['url']);
			}
			if(is_array($m['sub_button'])) {
				foreach($m['sub_button'] as &$s) {
					$s['name'] = preg_replace_callback('/\:\:([0-9a-zA-Z_-]+)\:\:/', create_function('$matches', 'return utf8_bytes(hexdec($matches[1]));'), $s['name']);
					$s['name'] = urlencode($s['name']);
					if(!empty($s['url'])){
						$s['url'] = smartUrlEncode($s['url']);
					}
				}
			}
		}
		$ms = array();
		$ms['button'] = $menus;
		$ret = $wechatObj->setMenu($ms);
		if(!$ret) {
			cpmsg_error(wechat_client::error());
		} else {
			C::t('wechats')->update($wechat['weid'],array('menuset'=>htmlspecialchars(json_encode($menus,JSON_UNESCAPED_UNICODE))));
			//更新设置缓存
			setDefaultWechat($wechat['weid']);
			cpmsg('已经成功创建菜单. ', adminurl('wechat/menu') ,'success');
		}
	}
	
	
	$dat = $wechat['menuset'];
//	$dat = htmlspecialchars_decode($dat);
	$menus = @json_decode($dat, true);
	
	if(empty($menus) || !is_array($menus)) {
		$menus = $wechatObj->getMenu();
		if(is_null($menus) && wechat_client::$ERROR_NO != '46003') {
			//如果不是显示 “菜单不存在46003”
			cpmsg_error(wechat_client::error());
		}
	}
	
	if(empty($menus) || !is_array($menus)) {
		cpmsg_error('获取菜单数据失败，请重试！');
	}
	if(is_string($menus['menu'])) {
		$menus['menu'] = @json_decode($menus['menu'], true);
	}
	if(!is_array($menus['menu'])) {
		$menus['menu'] = array();
	}
	
	
	if(is_array($menus['menu']['button'])) {
		foreach($menus['menu']['button'] as &$m) {
			if(isset($m['url'])) {
				$m['url'] = urldecode($m['url']);
			}
			if(isset($m['key'])) {
				$m['forward'] = $m['key'];
			}
			if(is_array($m['sub_button'])) {
				foreach($m['sub_button'] as &$s) {
					if(isset($s['url'])){
						$s['url']=urldecode($s['url']);
					}
					$s['forward'] = $s['key'];
				}
			}
		}
	}
	
	$tpl->menus = $menus;
}
//消息设置
elseif($operation=='msg'){
	
	if(empty($_G['wechat'])){
		cpmsg_error('请先设置默认账号！');
	}	
	$wechat = $_G['wechat'];
	
	$tpl->do = $do = I('do','list','');
	$tpl->shownav('topic', '微信公众号消息管理');
	$topmenu = array(
		array('普通回复列表', 'wechat&operation=msg&do=list' , $operation=='msg'&&$do=='list'),
		array('新增普通回复', 'wechat&operation=msg&do=add' , $operation=='msg'&&$do=='add'),
		array('特殊回复', 'wechat&operation=msg&do=special' , $operation=='msg'&&$do=='special'),
	);
	$tpl->showsubmenu('消息管理', $topmenu);
	
	$rule_model = M('wechat_rule');
	$rule_keyword_model = M('wechat_rule_keyword');
	$image_reply_model = M('wechat_reply_image');
	$music_reply_model = M('wechat_reply_music');
	
	//普通回复列表
	if($do=='list'){
		
		$pagesize = I('pagesize',10,'intval');
		$page = $_G['page'];
		$ruleCount = $rule_model->count();
		$list = $rule_model->limit($pagesize*($page-1).','.$pagesize)->select();
		foreach($list as $k=>$v){
			$keyword = $rule_keyword_model->where("rid='{$v[id]}'")->field('keyword,keytype')->find();
			$list[$k]['keyword']=$keyword['keyword'];
			$list[$k]['keytype']=$keyword['keytype'];
		}
		$tpl->multi = multi($ruleCount, $pagesize, $page, 'wechat&operation=msg&do=list');
		$tpl->list = $list;
	}
	//新增回复
	elseif($do == 'add'){
		$rid = I('rid',0,'intval');
		
		if(IS_POST){
			$insertData = array(
						'weid'	=> $wechat['weid'],
						'name'	=> I('name'),
						'type'	=> I('type',0,'intval'),
						'reply' => I('reply'),
					);
			if(!$rid){
				$rid = $rule_model->add($insertData);
			}else{
				$rule_model->where("id='{$rid}'")->save($insertData);
			}
			
			//关键词处理
			$rule_keyword_model->where("rid={$rid}")->delete(); //删除原有
			foreach($_POST['keytype'] as $ktk=>$ktv){
				$keywordData = array(
							'rid'		=> $rid,
							'keyword'	=> $_POST['keyword'][$ktk],
							'keytype'	=> $ktv
						);
				$rule_keyword_model->add($keywordData);
			}
			
			cpmsg('保存成功！',adminurl('wechat/msg/list'),'success');
			exit();
		}
		
		$item = $rule_model->find($rid);
		if($item){
			$item['keyword'] = json_encode($rule_keyword_model->where("rid={$rid}")->select(),JSON_UNESCAPED_UNICODE);
		}
		$tpl->item = $item;
		
	}
	//删除
	elseif($do == 'delete'){
		$rid = I('rid',0,'intval');
		$item = $rule_model->find($rid);
		if(!$item || $item['weid']!=$wechat['weid']){
			cpmsg_error('该规则不存在！');
		}
		$rule_model->delete($rid);
		$rule_keyword_model->where("rid='{$rid}'")->delete();
		cpmsg('删除成功！',adminurl('wechat/msg'),'success');
	}
	//特殊回复
	elseif($do == 'special'){
		if(IS_POST){
			$data = array(
				'welcome' => I('welcome','')
			);
			C::t('wechats')->update($_G['wechat']['weid'],$data);
			setDefaultWechat($_G['wechat']['weid']);
			ajax_return(1,'保存成功！');
		}
	}
}
//素材管理
elseif($operation=='resourse'){
	
	$tpl->ajax = $ajax = I('ajax',0,'intval',array(0,1));
	$tpl->do = $do = I('do','list','',array('list','add'));
	$tpl->type = $type = I('type','image');
	$tpl->shownav('topic', '微信账号管理');
	$topmenu = array(
		array('图文素材', 'wechat&operation=resourse&do=list&type=image' , $operation=='resourse'&&$do=='list'&&$type=='image'),
//		array('语音素材', 'wechat&operation=resourse&do=list&type=music' , $operation=='resourse'&&$do=='list'&&$type=='music'),
		array(array('menu' => '添加素材', 'submenu' => array(
			'image' => array('添加图文素材', 'wechat&operation=resourse&do=add&type=image' , $operation=='resourse'&&$do=='add'&&$type=='music'),
//			'music' => array('添加语音素材' ,'wechat&operation=resourse&do=add&type=music' , $operation=='resourse'&&$do=='add'&&$type=='music'),
		))),
	);
	$tpl->showsubmenu('素材管理', $topmenu);
	$model = M('wechat_reply_'.$type);
	
	if($do == 'list'){
		$pagesize = I('pagesize',10,'intval');
		if($type=='image'){
			$count = $model->where("parentid=0")->count();
			$list = $model->where("parentid=0")->limit($pagesize*($_G['page']-1).','.$pagesize)->select();
			foreach($list as $k=>$v){
				$list[$k]['child'] = $model->where("parentid=$v[id]")->select();
			}
		}elseif($type=='music'){
			$count = $model->count();
			$list = $model->limit($pagesize*($_G['page']-1).','.$pagesize)->select();
		}
		$tpl->multi = multi($count, $pagesize, $_G['page'], 'wechat&operation=resourse&do=list&type='.$type);
		$tpl->list = $list;
	}
	//添加素材
	elseif($do == 'add'){
		$tpl->id = $id = I('id',0,'intval');
		$tpl->parentid = $parentid = I('parentid',0,'intval');
		if(!IS_POST){
			//如果提交的是子图文
			if($parentid > 0){
				$soncount = $model->where("parentid='$parentid'")->count();
				if($soncount+1>1){
					cpmsg_error('子图文最多不能超过7条');
				}
			}
		}
		if(IS_POST){
			$insertData = array(
					'parentid'	=> $parentid,
					'title'		=>I('title'),
					'description'=> I('description'),
					'thumb'		=> I('thumb'),
					'content'	=> htmlspecialchars($_POST['content']),
					'url'		=> I('url')
				);
			if($id){
				$model->where("id='{$id}'")->save($insertData);
			}else{
				$id = $model->add($insertData);
			}
			cpmsg('保存成功！',adminurl('wechat/resourse/list',array('type'=>$type)),'success');
		}
		
		$tpl->item = $item = $model->find($id);
	}
	//删除
	elseif($do == 'delete'){
		$id = I('id',0,'intval');
		$item = $model->find($id);
		if(!$item){
			cpmsg_error('素材不存在！');
		}
		if($model->delete($id)){
			cpmsg('删除成功！',adminurl('wechat/resourse'),'success');
		}
		cpmsg_error('删除失败！');
	}
	
}
//带参数的二维码
elseif($operation == 'qrcode'){
	
	if(empty($_G['wechat'])){
		cpmsg_error('请先设置默认账号！');
	}
	
	$tpl->do = $do = I('do','list','',array('list','add','delete'));
	$tpl->shownav('topic', '微信账号管理');
	$topmenu = array(
		array('二维码列表', 'wechat&operation=qrcode&do=list' , $operation=='qrcode'&&$do=='list'),
		array('添加二维码', 'wechat&operation=qrcode&do=add' , $operation=='qrcode'&&$do=='add'),
	);
	$tpl->showsubmenu('带参数的二维码管理', $topmenu);
	
	$model = M('wechat_qrcode');
	if($do == 'list'){
		$tpl->list = $list = $model->where("weid='{$_G[wechat][weid]}'")->select();
		
	}elseif($do == 'add'){
		
		if(IS_POST){
			
			$wxobj = $_G['wechatObj'];
			
			$data = array(
					'weid' => $_G['wechat']['weid'],
					'type' => I('type',0),
					'func' => I('func',0),
					'param'=> $_REQUEST['param'],
					
			);
			
			$lastest= $model->where("type='{$data[type]}'")->order(" scene_id DESC ")->find();
			
			$qrdata = array(
				'scene_id'	=> intval($lastest ? $lastest['scene_id'] : 0) + 1,
				'expire'	=> $data['type']==0 ? 604800 : 0 ,
			);
			
			$data['scene_id'] = $qrdata['scene_id'];
			$data['expire'] = time() + $qrdata['expire'];
			
			$return = $wxobj->getQrcodeTicket($qrdata);
			if($return==null){
				ajax_return(0,wechat_client::error());
			}
			
			$data['ticket'] = $return['ticket'];
			$data['url'] = $return['url'];
			
			$model->add($data);
			
			ajax_return(1,'保存成功！');
			
		}
	}
	elseif($do == 'delete'){
		$id = I('id',0,'intval');
		if($model->delete($id)){
			cpmsg('删除成功',adminurl('wechat/qrcode'),'success');
		}
	}
}

//获取表情
elseif($operation == 'emoji'){
	$tpl->display($operation);
	exit();
}
//AJAX操作
elseif($operation=='ajax'){
	//获取资源信息
	if($do=='get_resourse'){
		$type = I('type',1);
		$type = $type==1?'image':'music';
		$list = M('wechat_reply_'.$type)->field('id,title')->where('parentid=0')->select();
		ajax_return($list ? 1 : 0,'',$list);
	}
	exit();
}

$tpl->display($operation,$action);

?>


