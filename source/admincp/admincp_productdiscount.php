<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: admincp_card.php 29335 2012-04-05 02:08:34Z cnteacher $
 */

if(!defined('IN_DISCUZ') || !defined('IN_ADMINCP')) {
	exit('Access Denied');
}
$operation = empty($operation) ? 'default' : $operation;

if($operation == 'default'){
	// 默认折扣管理
	cpheader();
	
	$rulelist = C::t('common_product_onsell_discount')->get_global_discount_setting();
	$usergroups = '';
	$usergrouplist = array();
	foreach(C::t('common_usergroup')->range_orderby_creditshigher() as $group) {
		if($group['type'] == 'member'){
			$usergrouplist[] = $group;
			$usergroups .= showtablerow('', array('class="td25"'), array(
				'<input type="hidden" name="inputgroupid[]" value="'.$group['groupid'].'">'.$group['grouptitle'],
				'<input type="radio" name="inputoperationtype_'.$group['groupid'].'" value="0" '.(($rulelist[$group['groupid']]['operationtype'] == 0)?'checked':'').'>'.cplang('product_onsell_discount_operationtype_discount').'&nbsp;&nbsp;&nbsp;'.'<input type="radio" name="inputoperationtype_'.$group['groupid'].'" value="1" '.(($rulelist[$group['groupid']]['operationtype'] == 1)?'checked':'').'>'.cplang('product_onsell_discount_operationtype_fixed'),
				'<input type="text" name="inputnum[]" value="'.$rulelist[$group['groupid']]['num'].'">'
			), TRUE);
		}
	}

	shownav('product', 'product_discount_admin');
	showsubmenu('product_discount_admin');
	showtips('product_discount_admin_tips');
	
	showformheader("productdiscount&operation=update");
	showtableheader(cplang('usergroups_list'));

	showsubtitle(array(
		'usergroups_edit_basic_title',
		'product_onsell_discount_operationtype',
		'product_onsell_discount_num'
		),'',array(
			'style="width:160px;"',
			'style="width:130px;"',
			''
		)
	);
	echo $usergroups;
	showsubmit('deletesubmit', cplang('submit'), ($tmpsearch_condition ? '<input type="checkbox" name="chkall" onclick="checkAll(\'prefix\', this.form, \'uidarray\');if(this.checked){$(\'deleteallinput\').style.display=\'\';}else{$(\'deleteall\').checked = false;$(\'deleteallinput\').style.display=\'none\';}" class="checkbox">'.cplang('select_all') : ''), ' &nbsp;&nbsp;&nbsp;<span id="deleteallinput" style="display:none"><input id="deleteall" type="checkbox" name="deleteall" class="checkbox">'.cplang('members_search_deleteall', array('membernum' => $membernum)).'</span>', $multipage);

	showtablefooter();
	showformfooter();
}elseif($operation == 'update'){
	cpheader();
	
	$inputgroupid = $_GET['inputgroupid'];
	$inputnum = $_GET['inputnum'];
	
	if(count($inputgroupid) > 0){
		$rulelist = C::t('common_product_onsell_discount')->get_global_discount_setting();
		foreach($inputgroupid as $k=>$groupid){
			$operationtype = (isset($_GET['inputoperationtype_'.$groupid])) ? $_GET['inputoperationtype_'.$groupid] : 0;
			if(isset($rulelist[$groupid])){
				// 更新
				$sql = 'UPDATE '.DB::table('common_product_onsell_discount').' SET operationtype = '.$operationtype.', num = '.$inputnum[$k].' WHERE spid=0 AND groupid='.$groupid;
				DB::query($sql);
			}else{
				// 插入
				$insert_data = array(
					'groupid'=>$groupid,
					'operationtype'=>$operationtype,
					'num'=>$inputnum[$k]
				);
				C::t('common_product_onsell_discount')->insert($insert_data);
			}
		}
		
		cpmsg('product_discount_default_submit_succeed', 'action=productdiscount&operation=default', 'succeed');
	}else{
		cpmsg('product_discount_default_submit_valid', '', 'error');
	}
	exit;
}
