/**
 * 默认JS
 */

var BROWSER = {};
var CSSLOADED = [];
var USERAGENT = navigator.userAgent.toLowerCase();
browserVersion({'ie':'msie','firefox':'','chrome':'','opera':'','safari':'','mozilla':'','webkit':'','maxthon':'','qq':'qqbrowser','rv':'rv'});
if(BROWSER.safari || BROWSER.rv) {
	BROWSER.firefox = true;
}
BROWSER.opera = BROWSER.opera ? opera.version() : 0;

HTMLNODE = document.getElementsByTagName('head')[0].parentNode;
if(BROWSER.ie) {
	BROWSER.iemode = parseInt(typeof document.documentMode != 'undefined' ? document.documentMode : BROWSER.ie);
	HTMLNODE.className = 'ie_all ie' + BROWSER.iemode;
}

function browserVersion(types) {
	var other = 1;
	for(i in types) {
		var v = types[i] ? types[i] : i;
		if(USERAGENT.indexOf(v) != -1) {
			var re = new RegExp(v + '(\\/|\\s|:)([\\d\\.]+)', 'ig');
			var matches = re.exec(USERAGENT);
			var ver = matches != null ? matches[2] : 0;
			other = ver !== 0 && v != 'mozilla' ? 0 : other;
		}else {
			var ver = 0;
		}
		eval('BROWSER.' + i + '= ver');
	}
	BROWSER.other = other;
}

function loadcss(cssname) {
	if(!CSSLOADED[cssname]) {
		var csspath = (typeof csspath == 'undefined' ? 'data/cache/style_' : csspath);
		if(!document.getElementById('css_' + cssname)) {
			css = document.createElement('link');
			css.id = 'css_' + cssname,
			css.type = 'text/css';
			css.rel = 'stylesheet';
			css.href = csspath + STYLEID + '_' + cssname + '.css?' + VERHASH;
			var headNode = document.getElementsByTagName("head")[0];
			headNode.appendChild(css);
		} else {
			document.getElementById('css_' + cssname).href = csspath + STYLEID + '_' + cssname + '&' + VERHASH;
		}
		CSSLOADED[cssname] = 1;
	}
}

function doane(event, preventDefault, stopPropagation) {
	var preventDefault = isUndefined(preventDefault) ? 1 : preventDefault;
	var stopPropagation = isUndefined(stopPropagation) ? 1 : stopPropagation;
	e = event ? event : window.event;
	if(!e) {
		e = getEvent();
	}
	if(!e) {
		return null;
	}
	if(preventDefault) {
		if(e.preventDefault) {
			e.preventDefault();
		} else {
			e.returnValue = false;
		}
	}
	if(stopPropagation) {
		if(e.stopPropagation) {
			e.stopPropagation();
		} else {
			e.cancelBubble = true;
		}
	}
	return e;
}

function isUndefined(variable) {
	return typeof variable == 'undefined' ? true : false;
}

function getEvent() {
	if(document.all) return window.event;
	func = getEvent.caller;
	while(func != null) {
		var arg0 = func.arguments[0];
		if (arg0) {
			if((arg0.constructor  == Event || arg0.constructor == MouseEvent) || (typeof(arg0) == "object" && arg0.preventDefault && arg0.stopPropagation)) {
				return arg0;
			}
		}
		func=func.caller;
	}
	return null;
}

function fetchOffset(obj, mode) {
	var left_offset = 0, top_offset = 0, mode = !mode ? 0 : mode;

	if(obj.getBoundingClientRect && !mode) {
		var rect = obj.getBoundingClientRect();
		var scrollTop = Math.max(document.documentElement.scrollTop, document.body.scrollTop);
		var scrollLeft = Math.max(document.documentElement.scrollLeft, document.body.scrollLeft);
		if(document.documentElement.dir == 'rtl') {
			scrollLeft = scrollLeft + document.documentElement.clientWidth - document.documentElement.scrollWidth;
		}
		left_offset = rect.left + scrollLeft - document.documentElement.clientLeft;
		top_offset = rect.top + scrollTop - document.documentElement.clientTop;
	}
	if(left_offset <= 0 || top_offset <= 0) {
		left_offset = obj.offsetLeft;
		top_offset = obj.offsetTop;
		while((obj = obj.offsetParent) != null) {
			position = getCurrentStyle(obj, 'position', 'position');
			if(position == 'relative') {
				continue;
			}
			left_offset += obj.offsetLeft;
			top_offset += obj.offsetTop;
		}
	}
	return {'left' : left_offset, 'top' : top_offset};
}

function showajax(_action,op, subop, param, _width, _height) {
	_width = _width || '500px';
	_height = _height || '550px';
	var index = $.layer({
		type: 2,
		shadeClose: true,
		title: false,
		shade: [0.3, '#000'],
		offset: ['20px', ''],
		area: [_width,_height],
		close: function(index){
			if(window.closing_reload_parent){
				window.closing_reload_parent = false;
				location.reload();
			}
		},
		iframe: {
			src: '/admin.php?action='+_action+'&operation=' + op + '&do=' + subop + '&ajax=1&' + param
		}
	});
	return index;
}

function confirm_delete_simple(_url){
	layer.confirm('确定删除吗？', function(){
		location.href=_url;
	});
}

function confirm_delete(_action,_op,_subop,_id) {
	layer.confirm('确定删除吗？', function(){
		$.getJSON(
			'/admin.php?action='+_action+'&operation='+_op+'&do='+_subop+'&id=' + _id,
			function(data) {
				alert(data.msg);
				if (data.result == 1) {
					location.reload();
				}
			}
		);
	});
}

//function selectthreadlist(_forum_selector,_thread_selector,_default_fid,_default_tid){
//	$('#'+_forum_selector).select2({
//			language: "zh-CN",
//	});
//	sethreadlist(_thread_selector,_default_fid,_default_tid);
//	$('#'+_forum_selector).change(function(){
//		var _fid = $(this).val();
//		sethreadlist(_thread_selector,_fid,_default_tid);
//	});
//}
//
//function sethreadlist(_thread_selector,_fid,_default_tid){
//	_default_tid = _default_tid || -1;
//	var _url = '/admin.php?action=setindex&op=ajax_getthread_by_fid&fid='+_fid;
//	$('#'+_thread_selector).empty().select2({
//			language: "zh-CN",
//			ajax: {
//		        url: _url,
//		        dataType: 'json',
//		        quietMillis: 250,
//		        data: function (term, page) { // page is the one-based page number tracked by Select2
//		            return {
//		                q: term, //search term
//		                page: page // page number
//		            };
//		        },
//		        results: function (data, page) {
//		            var more = (page * 30) < data.total_count; // whether or not there are more results available
//		            // notice we return the value of more so Select2 knows if more results can be loaded
//		            return { results: data.items, more: more };
//		        }
//		    },
//		    formatResult: function(data){
//		    	alert(1);
//		    }
//	});
//}

function selectthreadlist(_forum_selector,_thread_selector,_default_fid,_default_tid){
	//设置默认
	sethreadlist(_thread_selector,_default_fid,_default_tid);
	$('#'+_forum_selector).change(function(){
		var _fid = $(this).val();
		sethreadlist(_thread_selector,_fid,_default_tid);
	});
}

function sethreadlist(_thread_selector,_fid,_default_tid){
	_default_tid = _default_tid || -1;
	$('#'+_thread_selector).empty();
	$.getJSON('/admin.php?action=setindex&op=ajax_getthread_by_fid&fid='+_fid,function(data){
		if(data.result==1){
			var list = data.list;
			var this_index = 0;
			$.each(list,function(i){
				if(_default_tid == list[i]['tid']){
					this_index = i;
				}
				$('#'+_thread_selector).append('<option value="'+list[i]['tid']+'" '+( _default_tid == list[i]['tid'] ? 'selected' : '' )+' >'+list[i]['subject']+'</option>');
			});
			$('#'+_thread_selector).get(0).selectedIndex = this_index;
			$('#'+_thread_selector).select2("val",list[this_index]['tid']);
			
		}
	});
}

/*
	打开远程地址
	@params string url 目标远程地址
	@params string title 打开窗口标题，为空则不显示标题。可在返回的HTML定义<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>控制关闭
	@params object options 打开窗口的属性配置，可选项backdrop,show,keyboard,remote,width,height。具体参考bootcss模态对话框的options说明
	@params object events 窗口的一些回调事件，可选项show,shown,hide,hidden,confirm。回调函数第一个参数对话框JQ对象。具体参考bootcss模态对话框的on说明.

	@demo ajaxshow('url', 'title', {'show' : true}, {'hidden' : function(obj) {obj.remove();}});
*/
function ajaxshow(url, title, options, events) {
	var modalobj = $('#modal-message');
	var defaultoptions = {'remote' : url, 'show' : true};
	var defaultevents = {};
	var option = $.extend({}, defaultoptions, options);
	var events = $.extend({}, defaultevents, events);

	if(modalobj.length == 0) {
		$(document.body).append('<div id="modal-message" class="modal hide fade" tabindex="-1" role="dialog" aria-hidden="true" style="position:absolute;"></div>');
		var modalobj = $('#modal-message');
	}
	html = (typeof title != 'undefined' ? '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><h3 id="myModalLabel">'+title+'</h3></div>' : '') +
			'<div class="modal-body"></div>' +
			'<div class="modal-footer">'+(typeof events['confirm'] == 'function' ? '<a href="#" class="btn btn-primary confirm">确定</a>' : '') + '<a href="#" class="btn" data-dismiss="modal" aria-hidden="true">关闭</a></div>';
	modalobj.html(html);
	if (typeof option['width'] != 'undeinfed' && option['width'] > 0) {
		modalobj.css({'width' : option['width'], 'marginLeft' : 0 - option['width'] / 2});
	}
	if (typeof option['height'] != 'undeinfed' && option['height'] > 0) {
		modalobj.find('.modal-body').css({'max-height' : option['height']});
	}
	if (events) {
		for (i in events) {
			if (typeof events[i] == 'function') {
				modalobj.on(i, events[i]);
			}
		}
	}
	modalobj.on('hidden', function(){modalobj.remove();});
	if (typeof events['confirm'] == 'function') {
		modalobj.find('.confirm', modalobj).on('click', events['confirm']);
	}
	return modalobj.modal(option);
}


//图文信息编辑
/*
	根据html数据创建一个ITEM节点
*/
function buildAddForm(id, targetwrap) {
	var sourceobj = $('#' + id);
	var html = $('<div class="item">');
	id = id.split('-')[0];
	var size = $('.item').size();
	var htmlid = id + '-item-' + size;
	while (targetwrap.find('#' + htmlid).size() >= 1) {
		var htmlid = id + '-item-' + size++;
	}
	html.html(sourceobj.html().replace(/\(\$itemid\)/gm, htmlid));
	html.attr('id', htmlid);
	targetwrap.append(html);
	return html;
}

/*
	切换一个节点的编辑状态和显示状态
*/
function doEditItem(itemid) {
	$('#append-list .item').each(function(){
		$('#form', $(this)).css('display', 'none');
		$('#show', $(this)).css('display', 'block');		
	});
	var parent = $('#' + itemid);
	$('#form', parent).css('display', 'block');
	$('#show', parent).css('display', 'none');	
}

function doDeleteItem(itemid, deleteurl) {
	if (confirm('删除操作不可恢复，确认删除吗？')){
		if (deleteurl) {
			ajaxopen(deleteurl, function(){
				$('#' + itemid).remove();
			});
		} else {
			$('#' + itemid).remove();
		}	
	}
	return false;
}

function doDeleteItemImage(obj, url) {
	ajaxopen(url, function(){
		$(obj).parent().parent().find('#upload-file-view').html('');
	});
	return false;
}

function ajaxSubmit(_btnid,_formid,_url,_callback){
	$('#'+_btnid).click(function(){
		$.post(
			_url,
			$('#'+_formid).serialize(),
			function(data){
				var _data = $.parseJSON(data);
				_callback(_data);
			}
		);
	});
}
