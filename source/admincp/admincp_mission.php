<?php

/**
 * 任务操作类
 */

if(!defined('IN_DISCUZ') || !defined('IN_ADMINCP')) {
	exit('Access Denied');
}

require_once libfile('function/mission');

$operation_array = array('task');

if(!in_array($operation, $operation_array)){
	cpmsg('没有该operation');
}

if($operation=='task'){
	
	$do = empty($do) ? 'list' : $do;
	$tpl->shownav('topic', '游戏任务');
	$topmenu = array(
		array('全部列表', 'mission&operation=task&do=list' , $operation=='task'&&$do=='list'),
		array('添加任务', 'mission&operation=task&do=add' , $operation=='task'&&$do=='add'),
	);
	$tpl->showsubmenu('游戏任务', $topmenu);
	$model  = C::t('task');
	
	//列表任务
	if($do=='list'){
		$where = '';
		$pagesize = I('pagesize',15,'intval');
		$count = $model->count($where);
		$list = $model->fetch_list($where , ($_G['page']-1)*$pagesize , $pagesize);
		$tpl->multi = multi($count, $pagesize, $_G['page'], 'admin.php?action=mission&operation=task&do=list' );
		$tpl->list = $list;
	}
	
	//添加任务
	elseif($do=='add'){
		$tid = I('tid',0,'intval');
		$task_model = C::t('task');
		$task_rule_model = C::t('task_rule');
		$task_prize_model = C::t('task_prize');
		
		if(IS_POST){
			
			$task = array(
				'name'		=> I('name'),
				'pic'		=> I('pic'),
				'type'		=> I('type'),
				'timetype'	=> I('timetype'),
				'starttime'	=> strtotime(I('starttime')),
				'endtime'	=> strtotime(I('endtime')),
				'finish'	=> I('finish'),
			);
			
			$rule = array();
			foreach($_POST['rule-actionid'] as $rk=>$rv){
				$rule[] = array('actionid'=>$rv,'aval'=>$_POST['rule-aval'][$rk],'desc'=>$_POST['rule-desc'][$rk]);
 			}
			$prize = array();
			foreach($_POST['prize-ptype'] as $pk=>$pv){
				$prize[] = array('ptype'=>$pv,'num'=>$_POST['prize-num'][$pk],'pid'=>$_POST['prize-pid'][$pk]);
			}
			
			//新增
			if(!$tid){
				$tid = $task_model->insert($task,TRUE);
			}else{
				$task_model->update($tid,$task);
				$task_rule_model->delete_by_tid($tid);
				$task_prize_model->delete_by_tid($tid);
			}
			
			foreach($rule as $rk=>$rv){
				$rule[$rk]['tid'] = $tid;
				$task_rule_model->insert($rule[$rk]);
			}
			foreach($prize as $pk=>$pv){
				$prize[$pk]['tid'] = $tid;
				$task_prize_model->insert($prize[$pk]);
			}
			
			cpmsg('保存成功！',adminurl('mission/task'),'success');
			exit();
		}

		$tpl->item = $item = $task_model->fetch($tid);
		$tpl->rule = $rule = $task_rule_model->fetch_all_by_tid($tid);
		$tpl->prize = $prize = $task_prize_model->fetch_all_by_tid($tid);
		
	}
	
}
$tpl->do = $do;
$tpl->display($operation,$action);