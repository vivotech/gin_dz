<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: admincp_card.php 29335 2012-04-05 02:08:34Z cnteacher $
 */

if(!defined('IN_DISCUZ') || !defined('IN_ADMINCP')) {
	exit('Access Denied');
}
$operation = empty($operation) ? 'add' : $operation;

if($operation == 'add'){
	cpheader();
	if(!submitcheck('addsubmit')) {
		
		require_once libfile('function/productserieslist');
		require_once libfile('function/group');
		$seriesselect = '';
		$seriesselect = productseriesselect(false, 0, 0, 1);
		
		$usergrouplist = array();
		$usergroupselect = '';
		$usergroupcriditsselect = '';
		foreach(C::t('common_usergroup')->range_orderby_creditshigher() as $group) {
			if($group['type'] == 'member'){
				$usergrouplist[] = $group;
				$usergroupcriditsselect .= '<option value="'.$group['groupid'].'">'.$group['grouptitle'].'('.$group['creditshigher'].'~'.$group['creditslower'].')</option>';
				$usergroupselect .= '<option value="'.$group['groupid'].'">'.$group['grouptitle'].'</option>';
			}
		}
		// 获取第一品类的商品
		if(!isset($_G['cache']['productseries'])){
			loadcache('productseries');
			if(!isset($_G['cache']['productseries'])){
				updatecache('productseries');
			}
		}
		$seriescache = $_G['cache']['productseries'];
		$firstseriesid = 0;
		foreach($seriescache as $s){
			if($s['type'] == 'series'){
				$firstseriesid = $s['psid'];
				break;
			}
		}
		$product_list = C::t('common_product')->fetch_all_by_psid($firstseriesid);
		$productselect = '';
		if(count($product_list) > 0){
			foreach($product_list as $k=>$p){
				$productselect .= '<option value="'.$p['pdid'].'">'.$p['name'].'</option>';
			}
		}
		$typeselect = '<option value="0" selected>人民币商品</option><option value="1">积分商品</option>';
		
		
		shownav('product', 'product_onsell_add');
		showsubmenu('product_onsell_add');
		echo '<script src="'.$_G['setting']['jspath'].'calendar.js?'.$_G['setting']['verhash'].'" type="text/javascript"></script>';
		showformheader('productonsell&operation=add');
		showtableheader('','','id="formtable"');
		showsetting('product_onsell_type', '', '', '<select name="newtype" id="newtype">'.$typeselect.'</select>');
		showsetting('productseries_name', '', '', '<select name="newseriesid" id="newseriesid">'.$seriesselect.'</select>');
		showsetting('product_name', 'newpdid', '', '<select name="newpdid" id="newpdid">'.$productselect.'</select>');
		showsetting('product_onsell_subject', 'newsubject', '', 'text');
		showsetting('product_onsell_dateline', 'newdateline', date('Y-m-d'), 'text', 'readonly');
		showsetting('product_onsell_expiration', 'newexpiration', date('Y-m-d'), 'calendar');
		showsetting('product_onsell_totalitems', 'newtotalitems', '0', 'text');
		showsetting('product_onsell_limit', 'newlimit', '0', 'text');
		showsetting('product_onsell_groupidlimit', 'newgroupidlimit', '', '<select name="newgroupidlimit" id="newgroupidlimit">'.$usergroupcriditsselect.'</select>');
		showsetting('product_onsell_price', 'newprice', '0.0', 'text');
		showsetting('product_onsell_cost', 'newcost', '0.0', 'text');
		showsetting('product_onsell_ishidden', 'newishidden', false, 'radio');
		showsetting('product_onsell_discount', 'newdiscount', true, 'radio');
		showsetting('product_onsell_discounttype', array('newdiscounttype', array(
			array(0, $lang['product_onsell_discounttype_group']),
			array(1, $lang['product_onsell_discounttype_credit'])
		)), 0, 'mradio');
		$grouptr = showtablerow('class="grouptr"',array('',''),array(
			'<a href="###" class="addtr" onclick="addtr(\'group\');">新增一条规则</a>&nbsp;&nbsp;&nbsp;'.cplang('usergroup').'：<select name="newgroupid[]">'.$usergroupselect.'</select>',
			get_tr(0)
		),true);
		echo $grouptr;
		$credittr = showtablerow('class="credittr" style="display:none;"',array('',''),array(
			'<a href="###" class="addtr" onclick="addtr(\'credit\');">新增一条规则</a>&nbsp;&nbsp;&nbsp;'.cplang('credits').'：<input type="text" name="newcredit[]">',
			get_tr(1)
		),true);
		echo $credittr;
		showsubmit('addsubmit');
		showtablefooter();
		showformfooter();
		
		$adminscript = ADMINSCRIPT;

		onsell_page_js('submit_addsubmit', $adminscript, $grouptr, $credittr);
	}else{
		$newtype = trim($_GET['newtype']);
		$newpdid = trim($_GET['newpdid']);
		$newsubject = trim($_GET['newsubject']);
		$newdateline = TIMESTAMP;
		$newexpiration = strtotime(trim($_GET['newexpiration']));
		$newtotalitems = trim($_GET['newtotalitems']);
		$newlimit = trim($_GET['newlimit']);
		$newgroupidlimit = trim($_GET['newgroupidlimit']);
		$newprice = trim($_GET['newprice']);
		$newdiscount = trim($_GET['newdiscount']);  // 是否使用默认模板
		$newdiscounttype = trim($_GET['newdiscounttype']);  // 优惠核算标准
		$newgroupid = $_GET['newgroupid'];  // 组ID
		$newcredit = $_GET['newcredit'];  // 积分
		$newoperationtype = $_GET['newoperationtype']; // 优惠操作方法
		$newnum = $_GET['newnum']; // 优惠数
		$newcost = $_GET['newcost'];
		$newishidden = $_GET['newishidden'];
		
		if(!$newpdid || $newtotalitems <= 0 || $newprice <= 0){
			cpmsg('product_onsell_add_invalid', '', 'error');
		}
		
		$insert_data = array(
			'type'=>$newtype,
			'pdid'=>$newpdid,
			'subject'=>$newsubject,
			'dateline'=>$newdateline,
			'expiration'=>$newexpiration,
			'totalitems'=>$newtotalitems,
			'limit'=>$newlimit,
			'groupid'=>$newgroupidlimit,
			'cost'=>$newcost,
			'price'=>$newprice,
			'discount'=>$newdiscount,
			'discounttype'=>$newdiscounttype,
			'ishidden'=>$newishidden
		);
		$spid = C::t('common_product_onsell')->insert($insert_data, 1);
		if($spid <= 0){
			cpmsg('product_add_db_invalid', '', 'error');
		}else{
			// 优惠方案
			if($newdiscount == 0){
				// 重整newoperationtype
				if(count($newoperationtype) > 0){
					$operationtypelist = array();
					foreach($newoperationtype AS $v){
						$operationtypelist[] = $v;
					}
					foreach($operationtypelist as $k=>$operationtype){
						$insert_data = array(
							'spid'=>$spid,
							'type'=>$newdiscounttype,
							'operationtype'=>$operationtype,
							'num'=>$newnum[$k]
						);
						if($newdiscounttype == 0){
							$insert_data['groupid'] = $newgroupid[$k];
						}else if($newdiscounttype == 1){
							$insert_data['credit'] = $newcredit[$k];
						}
						C::t('common_product_onsell_discount')->insert($insert_data);
					}
				}
			}
		}
		
		updatecache('productonsell');
		cpmsg('product_add_succeed', 'action=productonsell&operation=admin', 'succeed');
	}
}elseif ($operation == 'edit'){
	cpheader();
	if(!submitcheck('editsubmit')) {
		// 读出该商品的数据
		$spid = $_GET['spid'];
		if(empty($spid) || !is_numeric($spid)){
			cpmsg('product_onsell_edit_loaderr', '', 'error');
		}
		$spid = intval($spid);
		$splist = C::t('common_product_onsell')->fetch_detail($spid);
		$thissp = $splist[0];
		// 读出该商品的优惠规则
		$discountlist = C::t('common_product_onsell_discount')->fetch_all_by_spid($thissp['spid']);
		
		require_once libfile('function/productserieslist');
		require_once libfile('function/group');
		$seriesselect = '';
		$seriesselect = productseriesselect(false, 0, $thissp['psid'], 1);
		
		$usergrouplist = array();
		$usergroupselect = '';
		$usergroupcriditsselect = '';
		foreach(C::t('common_usergroup')->range_orderby_creditshigher() as $group) {
			if($group['type'] == 'member'){
				$usergrouplist[] = $group;
				$is_selected = ($group['groupid'] == $thissp['groupid']) ? 'selected' : 0;
				$usergroupcriditsselect .= '<option value="'.$group['groupid'].'" '.$is_selected.'>'.$group['grouptitle'].'('.$group['creditshigher'].'~'.$group['creditslower'].')</option>';
				$usergroupselect .= '<option value="'.$group['groupid'].'">'.$group['grouptitle'].'</option>';
			}
		}
		
		// 获取对应品类的商品
		$selectedseriesid = $thissp['psid'];
		$product_list = C::t('common_product')->fetch_all_by_psid($selectedseriesid);
		$productselect = '';
		if(count($product_list) > 0){
			foreach($product_list as $k=>$p){
				if($p['pdid'] == $thissp['pdid']){
					$productselect .= '<option value="'.$p['pdid'].'" selected>'.$p['name'].'</option>';
				}else{
					$productselect .= '<option value="'.$p['pdid'].'">'.$p['name'].'</option>';
				}
			}
		}
		if($thissp['type'] == 1){
			$typeselect = '<option value="0">人民币商品</option><option value="1" selected>积分商品</option>';
		}else{
			$typeselect = '<option value="0" selected>人民币商品</option><option value="1">积分商品</option>';
		}
		
		shownav('product', 'product_onsell_edit');
		showsubmenu('product_onsell_edit');
		echo '<script src="'.$_G['setting']['jspath'].'calendar.js?'.$_G['setting']['verhash'].'" type="text/javascript"></script>';
		showformheader('productonsell&operation=edit&spid='.$spid);
		showtableheader('','','id="formtable"');
		showsetting('product_onsell_type', '', '', '<select name="newtype" id="newtype">'.$typeselect.'</select>');
		showsetting('productseries_name', '', '', '<select name="newseriesid" id="newseriesid">'.$seriesselect.'</select>');
		showsetting('product_name', 'newpdid', '', '<select name="newpdid" id="newpdid">'.$productselect.'</select>');
		showsetting('product_onsell_subject', 'newsubject', $thissp['subject'], 'text');
		showsetting('product_onsell_dateline', 'newdateline', date('Y-m-d', $thissp['dateline']), 'text', 'readonly');
		showsetting('product_onsell_expiration', 'newexpiration', date('Y-m-d', $thissp['expiration']), 'calendar');
		showsetting('product_onsell_totalitems', 'newtotalitems', $thissp['totalitems'], 'text');
		showsetting('product_onsell_limit', 'newlimit', $thissp['limit'], 'text');
		showsetting('product_onsell_groupidlimit', 'newgroupidlimit', '', '<select name="newgroupidlimit" id="newgroupidlimit">'.$usergroupcriditsselect.'</select>');
		showsetting('product_onsell_price', 'newprice', $thissp['price'], 'text');
		showsetting('product_onsell_cost', 'newcost', $thissp['cost'], 'text');
		showsetting('product_onsell_ishidden', 'newishidden', $thissp['ishidden'], 'radio');
		showsetting('product_onsell_discount', 'newdiscount', $thissp['discount'], 'radio');
		showsetting('product_onsell_discounttype', array('newdiscounttype', array(
			array(0, $lang['product_onsell_discounttype_group']),
			array(1, $lang['product_onsell_discounttype_credit'])
		)), $thissp['discounttype'], 'mradio');
		
		$trcount = count($discountlist) + 1;
		if(count($discountlist) > 0){
			foreach($discountlist as $k=>$l){
				if($thissp['discounttype'] == 0){
					$tempusergroupselect = usergroupselect($usergrouplist, $l['groupid']);
					showtablerow('class="grouptr"',array('',''),array(
						'<a href="###" class="addtr" onclick="addtr(\'group\');">新增一条规则</a>&nbsp;&nbsp;&nbsp;'.cplang('usergroup').'：<select name="newgroupid[]">'.$tempusergroupselect.'</select>',
						get_tr($k, $l['operationtype'], $l['num'])
					));
				}elseif ($thissp['discounttype'] == 1){
					showtablerow('class="credittr"',array('',''),array(
						'<a href="###" class="addtr" onclick="addtr(\'credit\');">新增一条规则</a>&nbsp;&nbsp;&nbsp;'.cplang('credits').'：<input type="text" name="newcredit[]" value="'.$l['credit'].'">',
						get_tr($k, $l['operationtype'], $l['num'])
					));
				}
			}

			if($thissp['discounttype'] == 0){
				echo get_creditrow('', 'none', $trcount -1);
			}else{
				echo get_grouprow($usergroupselect, 'none', $trcount -1 );
			}
		}else{
			echo get_grouprow($usergroupselect, 'table-row', 0, 0);
			echo get_creditrow('', 'none', 1, 0);
			$trcount = 2;
		}
		
		showsubmit('editsubmit');
		showtablefooter();
		showformfooter();
		
		$adminscript = ADMINSCRIPT;
		$grouptr = get_grouprow($usergroupselect, 'none', 0, 0);
		$credittr = get_creditrow('', 'none', 0, 0);
		onsell_page_js('submit_editsubmit',$adminscript, $grouptr, $credittr, $trcount);
	}else{
		$spid = trim($_GET['spid']);
		$newtype = trim($_GET['newtype']);
		$newpdid = trim($_GET['newpdid']);
		$newsubject = trim($_GET['newsubject']);
		$newexpiration = strtotime(trim($_GET['newexpiration']));
		$newtotalitems = trim($_GET['newtotalitems']);
		$newlimit = trim($_GET['newlimit']);
		$newgroupidlimit = trim($_GET['newgroupidlimit']);
		$newprice = trim($_GET['newprice']);
		$newdiscount = trim($_GET['newdiscount']);  // 是否使用默认模板
		$newdiscounttype = trim($_GET['newdiscounttype']);  // 优惠核算标准
		$newgroupid = $_GET['newgroupid'];  // 组ID
		$newcredit = $_GET['newcredit'];  // 积分
		$newoperationtype = $_GET['newoperationtype']; // 优惠操作方法
		$newnum = $_GET['newnum']; // 优惠数
		$newcost = $_GET['newcost'];
		$newishidden = $_GET['newishidden'];
		
		if(!$spid || !$newpdid || $newtotalitems <= 0 || $newprice <= 0){
			cpmsg('product_onsell_edit_invalid', '', 'error');
		}
		
		$update_data = array(
			'type'=>$newtype,
			'pdid'=>$newpdid,
			'subject'=>$newsubject,
			'expiration'=>$newexpiration,
			'totalitems'=>$newtotalitems,
			'limit'=>$newlimit,
			'groupid'=>$newgroupidlimit,
			'cost'=>$newcost,
			'price'=>$newprice,
			'discount'=>$newdiscount,
			'discounttype'=>$newdiscounttype,
			'ishidden'=>$newishidden
		);
		$affect_rows = C::t('common_product_onsell')->update($spid, $update_data);
		if($affect_rows <= 0){
			cpmsg('product_edit_db_invalid', '', 'error');
		}else{
			C::t('common_product_onsell_discount')->delete_by_spid($spid);
			// 优惠方案
			if($newdiscount == 0){
				// 重整newoperationtype
				if(count($newoperationtype) > 0){
					$operationtypelist = array();
					foreach($newoperationtype AS $v){
						$operationtypelist[] = $v;
					}
					foreach($operationtypelist as $k=>$operationtype){
						$insert_data = array(
							'spid'=>$spid,
							'type'=>$newdiscounttype,
							'operationtype'=>$operationtype,
							'num'=>$newnum[$k]
						);
						if($newdiscounttype == 0){
							$insert_data['groupid'] = $newgroupid[$k];
						}else if($newdiscounttype == 1){
							$insert_data['credit'] = $newcredit[$k];
						}
						C::t('common_product_onsell_discount')->insert($insert_data);
					}
				}
			}
		}
		
		updatecache('productonsell');
		cpmsg('product_edit_succeed', 'action=productonsell&operation=admin', 'succeed');
	}
}elseif ($operation == 'ajaxgetproduct'){
	// ajax获取该类别下的商品
	$psid = trim($_GET['psid']);
	
	if($psid > 0){
		$product_list = C::t('common_product')->fetch_all_by_psid($psid);
	}else{
		$product_list = array();
	}
	$return = $product_list;
	echo json_encode($return, JSON_UNESCAPED_UNICODE );
	exit;
}elseif ($operation == 'ajaxgetproductonsell'){
	// ajax获取该类别下的在售商品
	$psid = trim($_GET['psid']);
	
	$productonsell_list = array();
	if($psid > 0){
		$product_list = C::t('common_product')->fetch_all_by_psid($psid);
		if(count($product_list) > 0){
			require_once libfile('function/product');
			$pdids = array();
			foreach($product_list as $p){
				$pdids = $p['pdid'];
			}
			$productonsell_list = getProductList($pdids);
		} 
	}

	$return = $productonsell_list;
	echo json_encode($return, JSON_UNESCAPED_UNICODE);
	exit;
}elseif ($operation == 'admin'){
	// 管理上架商品
	cpheader();
	
	$type_arr = array('人民币商品', '积分商品');
	$page = max(1, $_G['page']);
	$pagesize = 20;
	$productnum = C::t('common_product_onsell')->count();

	$products = '';
	if($productnum > 0) {
		$multipage = multi($productnum, $pagesize, $page, ADMINSCRIPT."?action=productonsell&operation=admin");

		$allproduct = C::t('common_product_onsell')->fetch_all_page($page, $pagesize);
		foreach($allproduct as $k=>$product) {
			if($product['closed'] == 1){
				$state = '<span style="color:'.$lang['colorerror'].'">中途下架</span>';
				$tableoperation = "<a href=\"".ADMINSCRIPT."?action=productonsell&operation=onsellchange&closed=0&spidarray=$product[spid]\" class=\"act\">$lang[productonsellon]</a>";
			}else if($product['closed'] == 2){
				$state = '<span style="color:'.$lang['colorvalid'].'">售完下架</span>';
				$tableoperation = "<a href=\"".ADMINSCRIPT."?action=productonsell&operation=add\" class=\"act\">$lang[productonsellredo]</a>";
			}else if($product['closed'] == 0){
				$state = '<span style="color:'.$lang['colornormal'].'">在售</span>';
				$tableoperation = "<a href=\"".ADMINSCRIPT."?action=productonsell&operation=edit&spid=".$product['spid']."\" class=\"act\" target=\"_blank\">$lang[onsellmanage]</a>"."<a href=\"".ADMINSCRIPT."?action=productonsell&operation=onsellchange&spidarray=$product[spid]\" class=\"act\">$lang[productonselloff]</a>";
			}
			$products .= showtablerow('', array('class="td25"'), array(
				"<input type=\"checkbox\" name=\"spidarray[]\" value=\"$product[spid]\"".($product['onsell'] == 1 ? 'disabled' : '')." class=\"checkbox\">",
				$type_arr[$product['type']],
				"<a href=\"forum.php?mod=product&pdid=$product[pdid]\" target=\"_blank\">$product[subject]</a>",
				"<a href=\"forum.php?mod=product&pdid=$product[pdid]\" target=\"_blank\">$product[name]</a>",
				$product['series'],
				$product['costprice'],
				$product['price'],
				$product['totalitems'],
				$product['amount'],
				$product['limit'],
				date('Y-m-d',$product['dateline']),
				($product['expiration'] > 0) ? date('Y-m-d',$product['expiration']) : '无限期',
				$state,
				$tableoperation
			), TRUE);
		}
	}

	shownav('product', 'product_onsell_admin');
	showsubmenu('product_onsell_admin');
	
	showformheader("productonsell&operation=onsellchange");
	showtableheader(cplang('product_list'));

	if($productnum) {
		showsubtitle(array(
		'', 
		'product_onsell_type',
		'product_onsell_subject', 
		'product_name', 
		'productseries_name', 
		'product_costprice', 
		'product_onsell_price', 
		'product_onsell_totalitems', 
		'product_onsell_amount', 
		'product_onsell_limit', 
		'product_onsell_dateline', 
		'product_onsell_expiration', 
		'product_onsell_state', 
		'')
		);
		echo $products;
		showsubmit('deletesubmit', cplang('productonselloff'), ($tmpsearch_condition ? '<input type="checkbox" name="chkall" onclick="checkAll(\'prefix\', this.form, \'uidarray\');if(this.checked){$(\'deleteallinput\').style.display=\'\';}else{$(\'deleteall\').checked = false;$(\'deleteallinput\').style.display=\'none\';}" class="checkbox">'.cplang('select_all') : ''), ' &nbsp;&nbsp;&nbsp;<span id="deleteallinput" style="display:none"><input id="deleteall" type="checkbox" name="deleteall" class="checkbox">'.cplang('members_search_deleteall', array('membernum' => $membernum)).'</span>', $multipage);
	}
	showtablefooter();
	showformfooter();
}else if($operation == 'onsellchange'){
	// 管理上架商品
	cpheader();
	
	$spid = $_GET['spidarray'];
	$closed = (isset($_GET['closed'])) ? $_GET['closed'] : 1;
	if(!$spid){
		cpmsg('product_onsell_closed_invalid', '', 'error');
	}
	$spidlist = (is_array($spid)) ? $spid :array($spid);
	
	$affect_nums = C::t('common_product_onsell')->onsell_state_change($closed, $spidlist);
	
	if(count($spidlist) == $affect_nums){
		cpmsg('product_onsell_closed_succeed', 'action=productonsell&operation=admin', 'succeed');
	}else{
		if($affect_nums == 0){
			cpmsg('product_onsell_closed_zero', '', 'error');
		}else{
			cpmsg('product_onsell_closed_inequal', '', 'error');
		}	
	}
	exit;
}

function onsell_page_js($submitname ,$adminscript, $grouptr, $credittr, $trnums = 2){
	$grouptr = str_replace(array("\n",'"'), array('','\"'), $grouptr);
	$credittr = str_replace(array("\n",'"'), array('','\"'), $credittr);
	echo <<<EOT
<script type="text/javascript">
var trnums = {$trnums};
function addtr(str){
	var isappend = false;
	if(str == 'group'){
		var tr = "{$grouptr}";
		isappend = true;
	}else if(str == 'credit'){
		var tr = "{$credittr}";
		isappend = true;
	}
	if(isappend){
		var trobj = \$jq(tr);
		trobj
		.find("input[type='radio']").each(function(index){
			var _this = \$jq(this),
				name = _this.attr("name");
			name = name.replace(/newoperationtype\[[0-9]+\]/i, "newoperationtype[" + trnums + "]");
			_this.attr("name", name);
		});
		trobj
		.css("display","table-row")
		.insertBefore(\$jq("#{$submitname}").parent().parent().parent());
		
		trnums++;
	}
	return false;
}
\$jq(function(){
	// 选中产品类型,更换产品
	\$jq("#newseriesid").change(function(){
		var _this = \$jq(this);
		\$jq.ajax({
			"url":"{$adminscript}?action=productonsell&operation=ajaxgetproduct",
			"data":{
				"psid":_this.val()
			},
			"dataType":"json",
			success:function(data){
				var l = data,
					select = \$jq("#newpdid");
				select.html("");
				for(var i = 0; i < l.length; i++){
					var option = '<option value="' + l[i]['pdid'] + '">' + l[i]['name'] + '</option>';
					\$jq(option).appendTo(select);
				}
			}
		});
	});
	
	// 选中核算标准,切换规则输入框
	\$jq("li").click(function(){
		var _this = \$jq(this),
			radio = _this.find("input[name='newdiscounttype']");
		if(radio.length > 0){
			var type = radio[0].value;
			if(type == 0){
				\$jq("#formtable").find(".credittr").css("display","none");
				\$jq("#formtable").find(".grouptr").css("display","table-row");
			}else if(type == 1){
				\$jq("#formtable").find(".grouptr").css("display","none");
				\$jq("#formtable").find(".credittr").css("display","table-row");
			}
		}
	});
	
	// 提交时处理无效输入框
	\$jq("#cpform").submit(function(){
		var table = \$jq("#formtable");
		var pdid = \$jq("#newpdid").val();
		if(pdid <= 0){
			alert("请选择有效的商品");
			return false;
		}else{
			// 获取优惠核算标准选择
				var radio = table.find("input[name='newdiscounttype']"),
				type = 0;
			for(var i = 0; i < radio.length; i++){
				var r = \$jq(radio[i]);
				if(r.prop("checked")){
					type = r.val();
					break;
				}
			}
			if(type == 0){
				table.find("tr.credittr").remove();
			}else if(type == 1){
				table.find("tr.grouptr").remove();
			}
		}
		
		var newcostInput = table.find("input[name='newcost']"),
			newpriceInput = table.find("input[name='newprice']"),
			newcost = parseFloat(newcostInput.val()),
			newprice = parseFloat(newpriceInput.val());
		if(newcost >= newprice){
			if(!confirm("成本价比售价高，将导致经销商返点为负，确认提交？")){
				newcostInput.focus();
				return false;
			}
		}
	});
});
</script>
EOT;
}

function get_grouprow($usergroupselect, $display = 'table-row', $num = 0, $checkid = 0, $inputnum = ''){
	return showtablerow('class="grouptr" style="display:'.$display.';"',array('',''),array(
				'<a href="###" class="addtr" onclick="addtr(\'group\');">新增一条规则</a>&nbsp;&nbsp;&nbsp;'.cplang('usergroup').'：<select name="newgroupid[]">'.$usergroupselect.'</select>',
				get_tr($num, $checkid, $inputnum)
			),true);
}

function get_creditrow($credit, $display = 'table-row', $num = 0, $checkid = 0, $inputnum = ''){
	return showtablerow('class="credittr" style="display:'.$display.';"',array('',''),array(
			'<a href="###" class="addtr" onclick="addtr(\'credit\');">新增一条规则</a>&nbsp;&nbsp;&nbsp;'.cplang('credits').'：<input type="text" name="newcredit[]" value="'.$credit.'">',
			get_tr($num, $checkid, $inputnum)
	),true);
}

function get_tr($num, $checkid = 0, $inputnum = ''){
	return cplang('product_onsell_discount_operationtype').'：<input type="radio" name="newoperationtype['.$num.']" value="0" '.(($checkid == 0) ? 'checked':'').'>'.cplang('product_onsell_discount_operationtype_discount').'<input type="radio" name="newoperationtype['.$num.']" value="1" '.(($checkid == 1) ? 'checked':'').'>'.cplang('product_onsell_discount_operationtype_fixed').'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.cplang('product_onsell_discount_num').'：<input type="text" name="newnum[]" value="'.$inputnum.'">';
}

function usergroupselect($usergrouplist, $selectid = 0){
	$usergroupselect = '';
	foreach($usergrouplist as $k=>$group){
		if($group['groupid'] == $selectid){
			$usergroupselect .= '<option value="'.$group['groupid'].'" selected>'.$group['grouptitle'].'</option>';
		}else{
			$usergroupselect .= '<option value="'.$group['groupid'].'">'.$group['grouptitle'].'</option>';
		}
	}
	return $usergroupselect;
}
