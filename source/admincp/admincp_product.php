<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: admincp_card.php 29335 2012-04-05 02:08:34Z cnteacher $
 */

if(!defined('IN_DISCUZ') || !defined('IN_ADMINCP')) {
	exit('Access Denied');
}


define('PRODUCT_IMAGE_NUM', 6);
define('PRODUCT_IMAGE_DIR', 'product');

cpheader();

$operation = empty($operation) ? 'add' : $operation;

if($operation == 'add') {
	if(!submitcheck('addsubmit')) {
		setqiniuconfig();
		echo '<script src="static/js/vivo/ueditor/ueditor.config.js"></script>';
		echo '<script src="static/js/vivo/ueditor/ueditor.all.min.js"></script>';
		echo '<script src="static/js/vivo/ueditor/lang/zh-cn/zh-cn.js"></script>';
		echo '<script src="static/js/calendar.js?" type="text/javascript"></script>';
		
		require_once libfile('function/productserieslist');
		$seriesselect = array();
		$seriesselect = productseriesselect(false, 0, 0, 1);
		
		shownav('product', 'product_add');
		showsubmenu('product_add');
		showformheader('product&operation=add', 'enctype');
		showtableheader();
		showsetting('product_name', 'newproductname', '', 'text');
		showsetting('productseries_name', '', '', '<select name="newseriesid">'.$seriesselect.'</select>');
		showsetting('product_costprice', 'newcostprice', '0.00', 'text');
		showsetting('product_area', 'newarea', 'Italy', 'text');
		showsetting('product_unit', 'newunit', '', 'text');
		
		//规格
		echo '<tr><td class="td27" colspan="2">商品规格</td></tr>'.
			 '<tr class="noborder"><td id="product_spec_box"></td></tr>';
		$_spec_url = adminurl('ajax/productspec');
		echo <<<EOF
			<script>
				jQuery(function(){
					jQuery('select[name="newseriesid"]').change(function(){
						var _v = jQuery(this);
						get_spec(_v.val());
					});
					get_spec();
				});
				
				function get_spec(spec_id){
					jQuery.post('{$_spec_url}',{'psid':spec_id},function(data){
						jQuery('#product_spec_box').html(data);
					});
				}
				
			</script>
EOF;
		
		showsetting('product_desc', 'newdesc', '', 'richtext');
		
		showtablerow('',array('colspan="2" class="td27" s="1"'),array(cplang('product_image_main').':'));
		showqiniuupload('productimg_0', '', false);
		showtablerow('',array('colspan="2" class="td27" s="1"'),array(cplang('product_image').':'));
		for($i = 1; $i <= PRODUCT_IMAGE_NUM; $i++){
			showqiniuupload('productimg_'.$i, '', false);
		}
		showtablerow('',array('colspan="2" class="td27" s="1"'),array(cplang('product_mobile_image_main').':'));
		showqiniuupload('newmobile_banner', '', false);
		showsubmit('addsubmit');
		showtablefooter();
		showformfooter();

		js_get_ueditor('newdesc');
	} else {
		$newproductname = trim($_GET['newproductname']);
		$newseriesid = trim($_GET['newseriesid']);
		$newcostprice = trim($_GET['newcostprice']);
		$newarea = trim($_GET['newarea']);
		$newunit = trim($_GET['newunit']);
		$newdesc = trim($_GET['newdesc']);
		$newmobile_banner = trim($_GET['newmobile_banner']);
		$media_list = '';

		if(!$newproductname || !$newseriesid || !$newcostprice) {
			cpmsg('product_add_invalid', '', 'error');
		}

		// rich text handler
		$bbcodeModel = new bbcode();
		$newdesc = $bbcodeModel->html2bbcode(stripslashes($newdesc));
		// uploadPic
		$media_list = array();
		$filecontainername = 'productimg_';
		$imgcount = PRODUCT_IMAGE_NUM + 1;
		for($i = 0; $i < $imgcount; $i++){
			if(!empty($_GET[$filecontainername.$i])){
				$media_list[$i] = array(
					'media_type'=>'0',
					'media_url'=>dhtmlspecialchars($_GET[$filecontainername.$i]),
					'media_desc'=>'',
					'media_duration'=>''
				);
			}
		}
		$media_list = htmlspecialchars(json_encode($media_list, JSON_UNESCAPED_UNICODE));

		$insert_data = array(
			'psid'=>$newseriesid,
			'name'=>$newproductname,
			'costprice'=>$newcostprice,
			'area'=>$newarea,
			'unit'=>$newunit,
			'spec'=>serialize(I('spec')),
			'desc'=>$newdesc,
			'media_list'=>$media_list,
			'mobile_banner'=>$newmobile_banner
		);
		$pdid = C::t('common_product')->insert($insert_data, 1);
		if($pdid <= 0) {
			cpmsg('product_add_db_invalid', '', 'error');
		}
		
		updatecache('product');
		cpmsg('product_add_succeed', 'action=product&operation=admin', 'succeed');
	}
}else if($operation == 'edit'){
	if(!submitcheck('editsubmit')) {
		setqiniuconfig();
		echo '<script src="static/js/vivo/ueditor/ueditor.config.js"></script>';
		echo '<script src="static/js/vivo/ueditor/ueditor.all.min.js"></script>';
		echo '<script src="static/js/vivo/ueditor/lang/zh-cn/zh-cn.js"></script>';
		echo '<script src="static/js/calendar.js?" type="text/javascript"></script>';
		echo '<style>.product-img{ display:block; width:150px; height:150px; margin:0px auto;}</style>';
		$pdid = empty($_GET['pdid']) ? 0 : intval($_GET['pdid']);
		
		if(!$_G['cache']['product']){
			loadcache('product');
		}
		$this_product = $_G['cache']['product'][$pdid];
		$bbcodeModel = new bbcode();
		$this_product['desc'] = $bbcodeModel->bbcode2html(htmlspecialchars_decode($this_product['desc']), 2);
		$this_product['media_list'] = json_decode(htmlspecialchars_decode($this_product['media_list']), 1);

		require_once libfile('function/productserieslist');
		$seriesselect = array();
		$seriesselect = productseriesselect(false, 0, $this_product['psid'], 1);
		
		shownav('product', 'product_edit');
		showsubmenu('product_edit');
		showformheader('product&operation=edit', 'enctype');
		showtableheader();
		echo '<input type="hidden" name="pdid" value="'.$pdid.'">';
		showsetting('product_name', 'newproductname', $this_product['name'], 'text');
		showsetting('productseries_name', '', '', '<select name="newseriesid">'.$seriesselect.'</select>');
		showsetting('product_costprice', 'newcostprice', $this_product['costprice'], 'text');
		showsetting('product_area', 'newarea', $this_product['area'], 'text');
		showsetting('product_unit', 'newunit', $this_product['unit'], 'text');
		//规格
		echo '<tr><td class="td27" colspan="2">商品规格</td></tr>'.
			 '<tr class="noborder"><td id="product_spec_box"></td></tr>';
		$default = json_encode(unserialize($this_product['spec']));
		$_spec_url = adminurl('ajax/productspec');
		$formhash = FORMHASH;
		echo <<<EOF
			<script>
				jQuery(function(){
					jQuery('select[name="newseriesid"]').change(function(){
						var _v = jQuery(this);
						get_spec(_v.val());
					});
					get_spec({$this_product[psid]});
				});
				
				function get_spec(spec_id){
					jQuery.post('{$_spec_url}',{'psid':spec_id,'spec':'{$default}','formhash':'{$formhash}'},function(data){
						jQuery('#product_spec_box').html(data);
					});
				}
				
			</script>
EOF;
		
		showsetting('product_desc', 'newdesc', $this_product['desc'], 'richtext');
		showtablerow('',array('colspan="2" class="td27" s="1"'),array(cplang('product_image_main').':'));
		showqiniuupload('productimg_0', $this_product['media_list'][0]['media_url'], false);
		showtablerow('',array('colspan="2" class="td27" s="1"'),array(cplang('product_image').':'));
		for($i = 1; $i <= PRODUCT_IMAGE_NUM; $i++){
			showqiniuupload('productimg_'.$i, $this_product['media_list'][$i]['media_url'], false);
		}
		showtablerow('',array('colspan="2" class="td27" s="1"'),array(cplang('product_mobile_image_main').':'));
		showqiniuupload('newmobile_banner', $this_product['mobile_banner'], false);
		showsubmit('editsubmit');
		showtablefooter();
		showformfooter();

		js_get_ueditor('newdesc');
	} else {
		$pdid = intval($_GET['pdid']);
		$newproductname = trim($_GET['newproductname']);
		$newseriesid = trim($_GET['newseriesid']);
		$newcostprice = trim($_GET['newcostprice']);
		$newarea = trim($_GET['newarea']);
		$newunit = trim($_GET['newunit']);
		$newdesc = trim($_GET['newdesc']);
		$newmobile_banner = trim($_GET['newmobile_banner']);
		
		if(!$_G['cache']['product']){
			loadcache('product');
		}
		$media_list = $_G['cache']['product'][$pdid]['media_list'];
		$media_list = json_decode(htmlspecialchars_decode($media_list), 1);

		if(!$pdid || !$newproductname || !$newseriesid || !$newcostprice) {
			cpmsg('product_edit_invalid', '', 'error');
		}

		// rich text handler
		$bbcodeModel = new bbcode();
		$newdesc = $bbcodeModel->html2bbcode(stripslashes($newdesc));
		// uploadPic
		$filecontainername = 'productimg_';
		$imgcount = PRODUCT_IMAGE_NUM + 1;
		for($i = 0; $i < $imgcount; $i++){
			if(!empty($_GET[$filecontainername.$i])){
				$media_list[$i] = array(
					'media_type'=>'0',
					'media_url'=>dhtmlspecialchars($_GET[$filecontainername.$i]),
					'media_desc'=>'',
					'media_duration'=>''
				);
			}
		}
		$media_list = htmlspecialchars(json_encode($media_list, JSON_UNESCAPED_UNICODE));
		//规格
		$spec = serialize(I('spec'));
		$update_data = array(
			'psid'=>$newseriesid,
			'name'=>$newproductname,
			'costprice'=>$newcostprice,
			'area'=>$newarea,
			'unit'=>$newunit,
			'spec'=>$spec,
			'desc'=>$newdesc,
			'media_list'=>$media_list,
			'mobile_banner'=>$newmobile_banner
		);
		$affect_rows = C::t('common_product')->update($pdid, $update_data);
		if($affect_rows <= 0) {
			cpmsg('product_edit_db_invalid', '', 'error');
		}
		
		updatecache('product');
		cpmsg('product_edit_succeed', 'action=product&operation=admin', 'succeed');
	}
}else if($operation == 'admin'){
	
	// 管理商品
	$page = max(1, $_G['page']);
	$pagesize = 20;
	
	$productnum = C::t('common_product')->count();
	
	$products = '';
	if($productnum > 0) {
		$multipage = multi($productnum, $pagesize, $page, ADMINSCRIPT."?action=product&operation=admin");

		$allproduct = C::t('common_product')->fetch_all_page($page, $pagesize);
		foreach($allproduct as $k=>$product) {
			$products .= showtablerow('', array('class="td25"'), array(
				"<input type=\"checkbox\" name=\"pdidarray[]\" value=\"$product[psid]\"".($product['onsell'] == 1 ? 'disabled' : '')." class=\"checkbox\">",
				"<a href=\"forum.php?mod=product&pdid=$product[pdid]\" target=\"_blank\">$product[name]</a>",
				$product['series'],
				$product['costprice'],
				$product['area'],
				$product['unit'],
				"<a href=\"".ADMINSCRIPT."?action=productonsell&operation=admin&pdid=$product[pdid]\" class=\"act\">$lang[product_onsell_search]</a>".
				"<a href=\"".ADMINSCRIPT."?action=product&operation=edit&pdid=$product[pdid]\" class=\"act\" >$lang[productedit]</a>".
				"<a href=\"".ADMINSCRIPT."?action=product&operation=clean&pdid=$product[pdid]&page=$page\" class=\"act\">$lang[productdelete]</a>"
			), TRUE);
		}
	}

	shownav('product', 'product_admin');
	showsubmenu('product_admin');
	
	showformheader("product&operation=clean");
	showtableheader(cplang('product_list'));

	if($productnum) {
		showsubtitle(array('', 'product_name', 'productseries_name', 'product_costprice', 'product_area', 'product_unit', ''));
		echo $products;
		showsubmit('deletesubmit', cplang('delete'), ($tmpsearch_condition ? '<input type="checkbox" name="chkall" onclick="checkAll(\'prefix\', this.form, \'uidarray\');if(this.checked){$(\'deleteallinput\').style.display=\'\';}else{$(\'deleteall\').checked = false;$(\'deleteallinput\').style.display=\'none\';}" class="checkbox">'.cplang('select_all') : ''), ' &nbsp;&nbsp;&nbsp;<span id="deleteallinput" style="display:none"><input id="deleteall" type="checkbox" name="deleteall" class="checkbox">'.cplang('members_search_deleteall', array('membernum' => $membernum)).'</span>', $multipage);
	}
	showtablefooter();
	showformfooter();
}else if($operation == 'clean'){
	// 删除商品
	$pdid = empty($_GET['pdid']) ? 0 : intval($_GET['pdid']);
	$page = max(1, $_G['page']);
	
	if(!$_G['cache']['product']){
		loadcache('product');
	}
	$this_product = $_G['cache']['product'][$pdid];
	if(!$pdid || !$this_product) {
		cpmsg('product_delete_invalid', '', 'error');
	}
	
	// 删除图片
	$this_product['media_list'] = json_decode(htmlspecialchars_decode($this_product['media_list']), 1);
	foreach($this_product['media_list'] as $media){
		$filepath = $_G['setting']['attachdir'].PRODUCT_IMAGE_DIR.'/'.$media['media_url'];
		if(file_exists($filepath)){
			unlink($filepath);
		}
	}
	
	// 删除已上架的商品
	$spids = C::t('common_product_onsell')->fetch_spids_by_pdid($pdid);
	foreach($spids as $spid){
		// 删除上架产品的折扣
		if($spid > 0){
			DB::delete('common_product_onsell_discount', DB::field('spid', $spid), null, false);
			C::t('common_product_onsell')->delete($spid);
		}
	}
	
	// 删除产品
	$res = C::t('common_product')->delete($pdid);
	
	$return_url = 'action=product&operation=admin&page='.$page;
	if($res === FALSE){
		cpmsg('product_delete_db_invalid', $return_url, 'error');
	}else{
		updatecache('product');
		cpmsg('product_delete_succeed', $return_url, 'succeed');
	}
	exit;
}else if($operation == 'threadsconnectproduct'){
	// 主题链接商品
	$tid = intval($_GET['tid']);
	if(!submitcheck('editsubmit')){
		
		require_once libfile('function/productserieslist');
		require_once libfile('function/product');
		
		if(!$tid){
			cpmsg('product_thread_connect_error', '', 'error');
		}
		
		// 获取主题信息
		$thread = C::t('forum_thread')->fetch($tid);
		$thread['spid'] = empty($thread['spid']) ? 0 : intval($thread['spid']);
		if($thread['spid'] <= 0){
			// 获取产品类型
			if(!isset($_G['cache']['productseries'])){
				loadcache('productseries');
			}
			$seriescache = $_G['cache']['productseries'];
			$selectseriesid = 0;
			foreach($seriescache as $s){
				if($s['type'] == 'series'){
					$selectseriesid = $s['psid'];
					break;
				}
			}
			$selectspid = 0;
		}else{
			$selectspid = $thread['spid'];
			$thispd = getProduct($selectspid);
			$selectseriesid = $thispd['psid'];
		}
		
		$seriesselect = productseriesselect(false, 0, $selectseriesid, 1);
		$product_list = C::t('common_product')->fetch_all_by_psid($selectseriesid);
		$pdids = array();
		$productselect = '';
		if(count($product_list) > 0){
			foreach($product_list as $k=>$p){
				$pdids[] = $p['pdid'];
			}
		}
		$productonsell_list = getProductList($pdids);
		if(count($productonsell_list) > 0){
			foreach($productonsell_list as $k=>$sp){
				if($sp['spid'] == $selectspid){
					$productselect .= '<option value="'.$sp['spid'].'" selected>('.$sp['subject'].')'.$sp['name'].'</option>';
				}else{
					$productselect .= '<option value="'.$sp['spid'].'">('.$sp['subject'].')'.$sp['name'].'</option>';
				}
			}
		}
		
		shownav('product', 'threads_product');
		showsubmenu('threads_product');
		showformheader('product&operation=threadsconnectproduct&tid='.$tid);
		showtableheader();
		showsetting('subject', 'newproductname', $thread['subject'], 'text', 'readonly');
		showsetting('productseries_name', '', '', '<select name="newseriesid" id="newseriesid">'.$seriesselect.'</select>');
		showsetting('product_name', 'newspid', '', '<select name="newspid" id="newspid">'.$productselect.'</select>');
		showsubmit('editsubmit');
		showtablefooter();
		showformfooter();
		
		select_series_change_product_script();
	}else{
		$spid = intval($_GET['newspid']);
		
		if(!$spid){
			cpmsg('product_thread_connect_error', '', 'error');
		}
		$update_data = array(
			'spid'=>$spid
		);
		$affect_rows = C::t('forum_thread')->update($tid, $update_data);
		if($affect_rows > 0){
			cpmsg('product_thread_connect_succeed', '', 'succeed');
		}else{
			cpmsg('product_thread_connect_fail', 'action=product&operation=threadsconnectproduct&tid='.$tid, 'error');
		}
	}
}

function echo_img($media_list, $index = 0, $class_name = 'product-img'){
	global $_G;
	if(!empty($media_list[$index]['media_url'])){
		return '<img class="'.$class_name.'" src="'.$_G['setting']['attachurl'].'product/'.$media_list[$index]['media_url'].'">';
	}
	return '';
}

// 实例化ueditor
function js_get_ueditor($id){
		echo <<<EOT
<script type="text/javascript">
	var ue = UE.getEditor('{$id}',{
		  toolbars:[[
		  		'source',
	            'insertimage','|','undo', 'redo', '|',
	            'fontsize', 'indent', '|',
	            'horizontal', 'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'blockquote', 'pasteplain', '|', 
			    'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', '|',
	            'justifyleft', 'justifycenter', 'justifyright', '|', 
	            'preview', 'drafts'
	        ]]
			,saveInterval: 30000
			,elementPathEnabled : false
	});
</script>
EOT;
}
