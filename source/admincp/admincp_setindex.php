<?php
	
	/**
	 * 首页配置
	 */
//error_reporting(E_ALL);

if(!defined('IN_DISCUZ') || !defined('IN_ADMINCP')) {
		exit('Access Denied');
}

$op = I('operation','default');
$subop = I('do');
//设置顶部
$tpl->shownav('topic', '首页管理');
$topmenu = array(
	
	array(array('menu' => '排版设计', 'submenu' => array(
		'default' => array('首页文章', 'setindex&operation=default', $op=='default'),
		'default2' => array('坚野初试精华区', 'setindex&operation=alist&type=fid57', $operation == 'replies'),
	))),
	
	array(array('menu' => '轮播图设置', 'submenu' => array(
		'banner' => array('首页广告图', 'setindex&operation=banner&type=index' , $op=='banner'&&$_GET['type']=='index'),
		'banner2' => array('坚野初试广告图' ,'setindex&operation=banner&type=fid57' , $op=='banner'&&$_GET['type']=='fid57'),
		'banner3' => array('坚野悬赏广告图' ,'setindex&operation=banner&type=fid59' , $op=='banner'&&$_GET['type']=='fid59'),
	))),
	
);
$tpl->showsubmenu('首页管理', $topmenu);

//编辑首页
if($op=='default'){
	$tpl->forumindex = $list = C::t('forum_index')->fetch_list();
	$tpl->display($op,$action);
}
//普通文章编辑
elseif($op=='alist'){
	$tpl->type = $type = I('type','');
	if($type=='fid57'){
		$tpl->title = '坚野初试 精华区编辑';
	}
	$tpl->list = C::t('forum_index')->fetch_list_row_by_type($type);
	$tpl->display($op,$action);
}
//设置首页 AJAX
elseif($op == 'setindex'){
	
	require_once libfile('function/forumlist');
	$ajax = I('ajax',1,'intval',array(0,1));
	
	if($subop == 'save'){
		//如果不为空
		if(!empty($_POST)){
			$id = I('post.id',0,'intval');
			$data = array(
					'type' => I('post.type'),
					'fid'  => (int)I('post.fid'),
					'tid'  => (int)I('post.tid'),
					'title'=> I('post.title'),
					'message'=> I('post.message'),
					'url'  => I('post.url'),
					'displayorder' => (int)I('post.displayorder'),
					'addtime' => time()
				);
			if($id==0){
			//新增
				$res = C::t('forum_index')->insert($data);
			}else{
			//更新	
			 	$res = C::t('forum_index')->update($data,"id='{$id}'");
			}
			updatecache('forumindex');
			return_json(array('result'=>$res?1:0,'msg'=>$res?($id?'更新成功':'新增成功'):'操作失败！'));
		}
	}elseif($subop == 'multi_save'){
	//批量修改
		$edit_array = array('displayorder');
		if(!empty($_POST)){
			foreach($_POST as $k=>$v){
				if(in_array($k,$edit_array)){
					foreach($_POST[$k] as $kk=>$vv){
						C::t('forum_index')->update(array($k=>$vv),"id='{$kk}'");
					}
				}
			}
			updatecache('forumindex');
			return_json(array('result'=>1,'msg'=>'更新成功'));
		}
//		$res = C::t('forum_index')->update($data,"id='{$id}'");
		
	}elseif($subop == 'delete'){
		$id = I('id',0,'intval');
		$res = C::t('forum_index')->delete($id);
		updatecache('forumindex');
		return_json(array('result'=>$res?1:0,'msg'=>$res?'删除成功！':'删除失败！'));
	}
	elseif($subop == 'list'){
		$tpl->type = $type = I('type','');
		$tpl->list = C::t('forum_index')->fetch_list_row_by_type($type);
	}
	elseif($subop == 'detail'){
		$id = I('id',0,'intval');
		$tpl->type = $type = I('type','');
		$tpl->item = C::t('forum_index')->fetch($id);
	}
	$tpl->assign('subop',$subop);
	$tpl->display($op,$action);
	
}
//首页广告图
elseif($op=='banner'){
	$tpl->type = $type = I('type','index');
	$tpl->list = C::t('forum_banner_ad')->fetch_all($type);
	$tpl->display($op,$action);
}
//设置banner
elseif($op=='setbanner'){
	require_once libfile('function/post');
	require_once libfile('function/banner');
	require_once libfile('function/forumlist');
	
	$id = I('id',0,'intval');
	$type = I('type','index');
	//保存资料
	if($subop=='save'){
		if(!empty($_POST)){
			$baid = I('post.baid',0,'intval');
			//更新信息
			if($baid!=0 && getBannerByid($baid)){
				$opt = updateBanner(array(
				  'type'=> $type,
				  'fid'	=> (int)I('post.fid'),
			      'tid'   => (int)I('post.tid'),
			      'title' => I('post.title'),
			      'url'   => I('post.url'),
			      'displayorder'  => I('post.displayorder'),
			      'dateline'  => time()
			    ), 'baid='.$baid);
			}
			//插入新的
			else{
				$opt = insertBanner(array(
				  'type'=> $type,
				  'fid'	=> (int)I('post.fid'),
			      'tid'   => (int)I('post.tid'),
			      'title' => I('post.title'),
			      'url'   => I('post.url'),
			      'displayorder'  =>  I('post.displayorder'),
			      'dateline'  => time()
			    ));
			}
			updatecache('forum_banner_ad');
			return_json(array('result'=>$opt?1:0,'msg'=>$opt?($baid?'修改成功':'新增成功'):'修改失败~'));
		}
		return_json(array('result'=>0,'msg'=>'没有数据，请重试'));
	}
	//删除
	elseif($subop=='delete'){
		$opt = deleteBannerByid($id);
		updatecache('forum_banner_ad');
		return_json(array('result'=>$opt?1:0,'msg'=>$opt?'删除成功':'删除失败~'));
	}
	$tpl->assign('type',$type);
	$tpl->banner = getBannerByid($id);
	$tpl->display($op,$action);
}
//通过 fid获取帖子列表
elseif($op == 'ajax_getthread_by_fid'){
	$fid = I('fid',0,'intval');
	$list= C::t('forum_thread')->fetch_all_by_fid($fid,0,0,0,array('displayorder',array('>=','0')));
	return_json(array('result'=>$list?1:0,'list'=>$list?$list:null));
}
//通过tid获取帖子内容
elseif($op == 'ajax_getthread_by_tid'){
	$tid = I('tid',0,'intval');
	$item = C::t('forum_thread')->fetch($tid);
	if($item){
		$item['media_list'] =  json_decode(htmlspecialchars_decode($item['media_list']),true);
	}
	return_json(array('result'=>$item?1:0,'item'=>$item?$item:null));
}

?>
