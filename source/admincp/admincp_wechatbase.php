<?php

//微信后台
if(!defined('IN_DISCUZ') || !defined('IN_ADMINCP')) {
	exit('Access Denied');
}

require libfile('function/wechat');

$operation_array = array('base','list','post','setDefault','wepay','uploadcert');

if(!in_array($operation, $operation_array)){
	cpmsg('没有该operation');
}

$tpl->shownav('topic', '微信账号管理');
$topmenu = array(
	array('初始化配置', 'wechatbase&operation=base' , $operation=='base'),
	array('公众号列表', 'wechatbase&operation=list' , $operation=='list'),
	array('新增公众号', 'wechatbase&operation=post' , $operation=='post'),
//	array('微信支付',	'wechatbase&operation=wepay', $operation=='wepay'),
); 
$tpl->showsubmenu('微信账号管理', $topmenu);

$model = C::t('wechats');

//配置
if($operation == 'base'){
	//提交
	if(submitcheck('submit')){
		$configdata = array(
				'open' => I('open',0,'intval',array(0,1)),
		);
		$settings = array('wechatconfig' => $configdata);
		C::t('common_setting')->update_batch($settings);
		updatecache('setting');
		dheader('Location:'.adminurl('wechatbase/base'));
	}
	$tpl->wechatconfig = $_G['wechatconfig'];
}
//公众号list
elseif($operation == 'list'){
	$tpl->list = M('wechats')->select();
}
//新增微信
elseif($operation=='post'){
	
	$tpl->id = $id = I('id',0,'intval');
	
	if (submitcheck('submit')) {
		
		$data = array(
			'uid' => $_G['uid'],
			'name' => I('name'),
			'account' => I('account'),
			'accountlink' => I('accountlink'),
			'original' => I('original'),
			'token' => I('wetoken'),
			'level' => intval(I('level')),
			'key' => I('key'),
			'secret' => I('secret'),
			'signature' => '',
			'country' => '',
			'province' => '',
			'city' => '',
			'welcome' => '',
			'default' => '',
			'lastupdate' => '0',
			'default_period' => '0',
		);
		
		if(empty($data['name'])) {
			cpmsg('必须输入公众号的名称！','', 'error');
		}
		
		//修改
		if (!empty($id)) {
			$update = array(
				'name' => $data['name'],
				'account' => $data['account'],
				'accountlink' => $data['accountlink'],
				'original' => $data['original'],
				'token' => $data['token'],
				'EncodingAESKey' => I('EncodingAESKey'),
				'level' => $data['level'],
				'key' => $data['key'],
				'secret' => $data['secret'],
			);
			//保存图片
//			if (!empty($basicinfo['headimg'])) {
//				file_write('headimg_'.$id.'.jpg', $basicinfo['headimg']);
//			}
//			if (!empty($basicinfo['qrcode'])) {
//				file_write('qrcode_'.$id.'.jpg', $basicinfo['qrcode']);
//			}
			$change_id = $model->update($id , $update);
		} else {
		//新增
			$data['hash'] = random(5);
			$data['token'] = random(32);
			$data['EncodingAESKey'] = random(43);
			if ($change_id = $model->insert($data)) {
				//保存图片
//				if (!empty($basicinfo['headimg'])) {
//					file_write('headimg_'.$id.'.jpg', $basicinfo['headimg']);
//				}
//				if (!empty($basicinfo['qrcode'])) {
//					file_write('qrcode_'.$id.'.jpg', $basicinfo['qrcode']);
//				}
//				if (!empty($data['parentid'])) {
//					$subwechats = pdo_fetchall("SELECT weid FROM ".tablename('wechats')." WHERE parentid = :parentid", array(':parentid' => $data['parentid']), 'weid');
//					if (!empty($subwechats)) {
//						pdo_update('wechats', array('subwechats' => implode(',', array_keys($subwechats))), array('weid' => $data['parentid']));
//					}
//				}

			}
		}
//		if (!empty($_FILES['qrcode']['tmp_name'])) {
//			$_W['uploadsetting'] = array();
//			$_W['uploadsetting']['image']['folder'] = '';
//			$_W['uploadsetting']['image']['extentions'] = array('jpg');
//			$_W['uploadsetting']['image']['limit'] = $_W['config']['upload']['image']['limit'];
//			$upload = file_upload($_FILES['qrcode'], 'image', "qrcode_{$id}");
//		}
//		if (!empty($_FILES['headimg']['tmp_name'])) {
//			$_W['uploadsetting'] = array();
//			$_W['uploadsetting']['image']['folder'] = '';
//			$_W['uploadsetting']['image']['extentions'] = array('jpg');
//			$_W['uploadsetting']['image']['limit'] = $_W['config']['upload']['image']['limit'];
//			$upload = file_upload($_FILES['headimg'], 'image', "headimg_{$id}");
//		}
		//如果是默认账号，则需要更新缓存
		if($_G['wechat']['weid']==$id){
			setDefaultWechat($id);
		}
		cpmsg( !empty($id) ? '更新公众号成功！':'已成功接入公众平台！' , adminurl('wechatbase/list'), !$change_id ? 'error' : 'success');
	}
	
	
	$wechat = array();
	if (!empty($id) && $id!=0) {
		$wechat = $model->fetch($id);
	}
	$tpl->assign('wechat',$wechat);
	
}
//设置默认
elseif($operation=='setDefault'){
	$weid = I('id',0,'intval');
	$res = setDefaultWechat($weid);
	cpmsg($res?'设置成功':'设置失败，没有该公众号！',adminurl('wechatbase/list'),$res?'success':'error');
}
////微信支付配置
//elseif($operation == 'wepay'){
//	//提交
//	if(submitcheck('submit')){
//		
//		$configdata = array(
//			'MCHID' => I('MCHID'),
//			'paykey'=> I('paykey'),
//			'APPID' => I('APPID'),	
//			'APPSECRET'=> I('APPSECRET'),
//			'CURL_TIMEOUT'=> I('CURL_TIMEOUT'),
//			'SSLCERT_PATH'=> I('SSLCERT_PATH'),
//			'SSLKEY_PATH' => I('SSLKEY_PATH'),
//		);
//		
//		$targetFolder = 'data/cacert';
//		$targetPath = DISCUZ_ROOT . $targetFolder;
//		if (!empty($_FILES)) {
////			print_r($_FILES);exit();
//			$fileTypes = array('pem');
//			foreach($_FILES as $fkey=>$fval){
//				//上传成功
//				if($_FILES[$fkey]['error']==0){
//					$tempFile = $_FILES[$fkey]['tmp_name'];
//					$targetFile = rtrim($targetPath,'/') . '/' . $_FILES[$fkey]['name'];
//					$fileParts = pathinfo($_FILES[$fkey]['name']);
//					if (in_array($fileParts['extension'],$fileTypes)) {
//						move_uploaded_file($tempFile,$targetFile);
//					}
//					$configdata[$fkey] = $targetFolder.'/' . $_FILES[$fkey]['name'];
//				}
//			}
//		}
//		$settings = array('wechatpay' => $configdata);
//		C::t('common_setting')->update_batch($settings);
//		updatecache('setting');
//		dheader('Location:'.adminurl('wechatbase/wepay'));
//	}
//	
//	$tpl->item = $_G['wechatpay'];
//	
//}

$tpl->display($operation,$action);

?>


