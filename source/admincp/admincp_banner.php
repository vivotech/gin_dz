<?php

require_once libfile('function/post');
require_once libfile('function/banner');

// get banner  获取广告
$tid = isset($_GET['tid']) ? filter_input(INPUT_GET, 'tid', FILTER_VALIDATE_INT) : 0;
$banner = getBannerByTid($tid);

$post_tid = isset($_POST['tid']) ? filter_input(INPUT_POST, 'tid', FILTER_VALIDATE_INT) : 0;

// save data  如果有 POST 的tid 则保存数据
if ( $post_tid )
{
  if ($post_tid != $tid)
  {
    exit();
  }
  
  // 获取数据
  $title  = filter_input(INPUT_POST, 'title');
  $url    = filter_input(INPUT_POST, 'url');
  $displayorder = filter_input(INPUT_POST, 'displayorder');
  
  // update record  更新数据库记录
  if ($banner)
  {
    $opt = updateBanner(array(
      'title' => $title,
      'url'   => $url,
      'displayorder'  => $displayorder,
      'dateline'  => time()
    ), 'tid='.$post_tid);
  }
  // insert new record  插入新的数据库记录
  else
  {
    $opt = insertBanner(array(
      'tid'   => $post_tid,
      'title' => $title,
      'url'   => $url,
      'displayorder'  => $displayorder,
      'dateline'  => time()
    ));
  }
  
  $message = '';
  if ($opt)
  {
    // 重新获取记录
    $banner = getBannerByTid($tid);
    $message = '保存成功';
  }
  else
  {
    $message = '保存失败';
  }
}

$banner_baid = $banner_tid = 0;
$banner_title = $banner_url = '';
$banner_dateline = $banner_displayorder = 0;

if ($banner)
{
  $banner_baid  = $banner['baid'];
  $banner_tid   = $banner['tid'];
  $banner_title = $banner['title'];
  $banner_url   = $banner['url'];
  $banner_dateline  = $banner['dateline'];
  $banner_displayorder  = $banner['displayorder'];
}

$firstPost = getThreadFirstPost($tid);

cpheader();

shownav('topic', 'banner');
showsubmenu('banner');

?>
<?php if ($message) : ?>
<div class="message"><?php echo $message; ?></div>
<?php endif; ?>

<link rel="stylesheet" href="/static/css/mobile/dialog.css" />
<form action="" method="post">
  <table class="tb tb2">
    <tr class="rowform">
      <td class="td27">
        <label>帖子</label>
      </td>
    </tr>
    <tr class="rowform">
      <td>
        <div>
          <?php if ($tid) : ?>
          <span><?php echo $firstPost ? $firstPost['subject'] : ''; echo '(ID: '.$tid.')'; ?></span>
          <input id="form_tid" type="hidden" name="tid" value="<?php echo $tid; ?>" />
          <?php else : ?>
          <input id="form_tid" type="text" name="tid" class="txt" />
          <?php endif; ?>
        </div>
      </td>
    </tr>
    <tr class="rowform">
      <td class="td27">
        <label>Banner 标题</label>
      </td>
    </tr>
    <tr class="rowform">
      <td>
        <div>
          <input type="text" name="title" class="txt" required="" value="<?php echo $banner_title; ?>" />
        </div>
      </td>
    </tr>
    <tr class="rowform">
      <td class="td27">
        <label>Banner 图片</label>
      </td>
    </tr>
    <tr class="rowform">
      <td>
        <div id="form_image_url_container">
          <div>
            <img id="form_image" src="<?php echo $banner_url; ?>" alt="预览图片" style="max-width: 160px; height: auto;" />
          </div>
          <div>
            <input id="form_image_url" type="text" name="url" class="txt" value="<?php echo $banner_url; ?>" style="width: 500px;" />
            <button type="button" id="qiniuImageUpload">上传图片</button>
          </div>
        </div>
      </td>
    </tr>
    <tr class="rowform">
      <td class="td27">
        <label>显示的顺序号</label>
      </td>
    </tr>
    <tr class="rowform">
      <td>
        <div>
          <input type="text" name="displayorder" class="txt" value="<?php echo $banner_displayorder; ?>" />
        </div>
      </td>
    </tr>
    <tr class="rowform">
      <td class="td27">
        <label>最后修改时间</label>
      </td>
    </tr>
    <tr class="rowform">
      <td>
        <div>
          <span><?php echo $banner_dateline ? date("Y-m-d H:i:s", $banner_dateline) : '没有'; ?></span>
        </div>
      </td>
    </tr>
    <tr>
      <td>
        <input type="submit" class="btn" value="保存">
      </td>
    </tr>
  </table>
</form>

<script src="/static/js/vivo/mobile/tools.js"></script>
<script src="/static/js/vivo/mobile/app.js"></script>
<script src="/static/js/vivo/plupload-2.1.2/js/plupload.full.min.js"></script>
<script src="/static/js/vivo/qiniu-js-sdk/src/qiniu.min.js"></script>
<script>
  var bgUploader = Qiniu.uploader({
	    runtimes: 'html5,flash,html4',    //上传模式,依次退化
	    browse_button: 'qiniuImageUpload',       //上传选择的点选按钮ID，**必需**
	    uptoken_url: 'forum.php?mod=ajax&action=getQiNiuToken',
	    domain: 'http://7u2qq8.com1.z0.glb.clouddn.com/',
	    //container: 'form_image_url_container',           //上传区域DOM ID，默认是browser_button的父元素，
	    max_file_size: '2mb',           //最大文件体积限制
	    flash_swf_url: '../plupload-2.1.2/js/Moxie.swf',  //引入flash,相对路径
	    max_retries: 3,                   //上传失败最大重试次数
	    dragdrop: true,                   //开启可拖曳上传
	    drop_element: 'container',        //拖曳上传区域元素的ID，拖曳文件或文件夹后可触发上传
	    chunk_size: '4mb',                //分块上传时，每片的体积
	    auto_start: true,                 //选择文件后自动上传，若关闭需要自己绑定事件触发上传
	    init: {
	        'FileUploaded': function(up, file, info) {
	        	var domain = up.getOption('domain');
	        	var res = jQuery.parseJSON(info);
	        	var sourceLink = domain + res.key;
	        	jQuery("#form_image").attr("src", sourceLink);
            	jQuery("#form_image_url").val(sourceLink);
            
            // remove progress
            var dialog_id = "progressDialog-" + file.id;
            removeProgressDialog(dialog_id);
	        },
	        'Error': function(up, err, errTip) {
	          alert(errTip);
	        },
          'BeforeUpload': function(up, file)
          {
            // create progress
            var dialog_id = "progressDialog-" + file.id;
            createProgressDialog(dialog_id, file.percent);
          },
	        'UploadComplete': function() {
	               //队列文件处理完毕后,处理相关的事情
	        },
          'UploadProgress': function(up, file)
          {
            // update progress
            var dialog_id = "progressDialog-" + file.id;
            updateProgressDialog(dialog_id, file.percent)
            //log(file.percent);
          },
	        'Key': function(up, file) {
	            // 若想在前端对每个文件的key进行个性化处理，可以配置该函数
	            // 该配置必须要在 unique_names: false , save_key: false 时才生效
              var date = new Date();
              var key = "banner/pid"+jQuery("#form_tid").val()+"_"+date.getFullYear()+(date.getMonth()+1)+date.getDate()+date.getTime();
              // do something with key here
              return key;
	        }
	    }
	});
  
</script>