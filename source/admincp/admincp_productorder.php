<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: admincp_card.php 29335 2012-04-05 02:08:34Z cnteacher $
 */

if(!defined('IN_DISCUZ') || !defined('IN_ADMINCP')) {
	exit('Access Denied');
}

$operation = empty($operation) ? 'admin' : $operation;

if($operation == 'admin') {
	cpheader();
	// 管理订单
	$page = max(1, $_G['page']);
	$pagesize = 20;
	
	$ordernum = C::t('common_member_order')->count();
	
	$orders = '';
	if($ordernum > 0) {
		require_once libfile('function/order');
		$multipage = multi($ordernum, $pagesize, $page, ADMINSCRIPT."?action=productorder&operation=admin");

		$allorder = C::t('common_member_order')->fetch_all_page($page, $pagesize);
		foreach($allorder as $k=>$order) {
			$orders .= showtablerow('', array('class="td25"'), array(
				"<input type=\"checkbox\" name=\"oidarray[]\" value=\"$order[oid]\"".($order['onsell'] == 1 ? 'disabled' : '')." class=\"checkbox\">",
				$order['o_openid'],
				"<a href=\"home.php?mod=space&uid=$order[uid]\" target=\"_blank\">$order[nickname]</a>",
				$order['totalprice'],
				date('Y-m-d H:i',$order['dateline']),
				get_payment_name($order['payment']).($order['payment']>1 ? get_paystate_name($order['paystate']):''),
				get_state_name($order['state']),
				$order['note'],
				"<a href=\"".ADMINSCRIPT."?action=productorder&operation=edit&oid=$order[oid]\" class=\"act\">$lang[product_order_edit]</a>"
			), TRUE);
		}
	}

	shownav('product', 'product_order_admin');
	showsubmenu('product_admin');
	
	showformheader("productorder&operation=changestate&page=".$page."&referer=admin");
	showtableheader(cplang('product_order_list'));

	if($ordernum) {
		showsubtitle(array('', 'product_order_openid', 'product_order_username', 'product_order_totalprice', 'product_order_dateline', 'product_order_payment' , 'product_order_state',  'product_order_note', ''));
		echo $orders;
		showtablerow('',array(''),array('<input type="checkbox" name="chkall" onclick="checkAll(\'prefix\', this.form, \'oidarray\');" class="checkbox">'.cplang('select_all')));
	}else{
		showsubtitle(array(cplang('product_order_list_empty')));
	}
	showtablefooter();
	
	if(!$_G['cache']['express_company']){
		loadcache('express_company');
	}
	$express_company_list = $_G['cache']['express_company'];
	$express_companyselects = '<option value="0">请选择</option>';
	foreach($express_company_list as $l){
		$express_companyselects .= '<option value="'.$l['ecid'].'">'.$l['ecname'].'</option>';
	}
	showtableheader(cplang('product_order_changestate_option'));
	showtablerow('',array('',''),array(
		'<input type="radio" name="state" value="0">'.cplang('product_order_state_0'),
		''
	));
	showtablerow('',array('',''),array(
		'<input type="radio" name="state" value="1">'.cplang('product_order_state_1'),
		''
	));
	showtablerow('',array('',''),array(
		'<input type="radio" name="state" value="2">'.cplang('product_order_state_2'),
		''
	));
	showtablerow('',array('',''),array(
		'<input type="radio" name="state" value="3">'.cplang('product_order_state_3'),
		''
	));
	showtablerow('',array('',''),array(
		'<input type="radio" name="state" value="4">'.cplang('product_order_state_4'),
    cplang('product_order_expresscom').'：<select name="newexpresscom">'.$express_companyselects.'</select>，'.cplang('product_order_expresscode').'：<input type="text" name="newexpresscode" value="">'
	));
	showtablerow('',array('',''),array(
		'<input type="radio" name="state" value="5">'.cplang('product_order_state_5'),
		''
	));
	showtablerow('',array('',''),array(
		'<input type="radio" name="state" value="6">'.cplang('product_order_state_6'),
		''
	));
	showsubmit('changesubmit', cplang('product_order_changestate'), $multipage);
	showtablefooter();
	showformfooter();
	exit;

}elseif ($operation == 'delivery'){
	cpheader();
	// 订单发货
	$page = max(1, $_G['page']);
	$pagesize = 15;
	
	$ordernum = C::t('common_member_order')->count_where(' WHERE state >= 0 AND state <= 3');
	
	$orders = '';
	if($ordernum > 0) {
		require_once libfile('function/order');
		$multipage = multi($ordernum, $pagesize, $page, ADMINSCRIPT."?action=productorder&operation=delivery");

		$allorder = C::t('common_member_order')->fetch_all_page($page, $pagesize, ' WHERE o.state >= 0 AND o.state <= 3 AND ( o.payment = 1 OR (o.payment > 1 AND o.paystate = 1 ) )');
		foreach($allorder as $k=>$order) {
			$orders .= showtablerow('', array('class="td25"'), array(
				"<input type=\"checkbox\" name=\"oidarray[]\" value=\"$order[oid]\"".($order['onsell'] == 1 ? 'disabled' : '')." class=\"checkbox orderCheck\">",
				$order['o_openid'],
                "<a href=\"home.php?mod=space&uid=$order[uid]\" target=\"_blank\">$order[nickname]</a>",
				$order['totalprice'],
				date('Y-m-d', $order['dateline']),
                get_payment_name($order['payment']).($order['payment']>1 ? get_paystate_name($order['paystate']):''),
				get_state_name($order['state']),
				$order['note'],
				'<p><span class="title">'.$lang[member_address_address].'：</span><span class="data">'.getFullAddress($order).'</span></p><p><span class="title">'.$lang[member_address_name].'：</span><span class="data">'.$order['receiver'].'</span></p><p><span class="title">'.$lang[member_address_phone].'：</span><span class="data">'.$order['phone'].'</span></p>'
			), TRUE);
		}
	}
	
	shownav('product', 'product_order_admin_delivery');
	showsubmenu('product_order_admin_delivery');
	
	showformheader("productorder&operation=changestate&page=".$page."&referer=delivery");
	showtableheader(cplang('product_order_list'));

	if($ordernum) {
		showsubtitle(array('', 'product_order_openid', 'product_order_username', 'product_order_totalprice', 'product_order_dateline','product_order_payment' , 'product_order_state', 'product_order_note', 'member_address_info', ''));
		echo $orders;
	}else{
		showsubtitle(array(cplang('product_order_list_empty')));
	}
	showtablefooter();
	
	echo_product_list_container();
	echo_product_list_jsfunction();
	
	if(!$_G['cache']['express_company']){
		loadcache('express_company');
	}
	$express_company_list = $_G['cache']['express_company'];
	$express_companyselects = '<option value="0">请选择</option>';
	foreach($express_company_list as $l){
		$express_companyselects .= '<option value="'.$l['ecid'].'">'.$l['ecname'].'</option>';
	}
	showtableheader(cplang('product_order_changestate_option'));
	showtablerow('',array('',''),array(
		'<input type="radio" name="state" value="4" checked>'.$lang['product_order_state_4'],
    	$lang['product_order_expresscom'].'：<select name="newexpresscom">'.$express_companyselects.'</select>，'.$lang['product_order_expresscode'].'：<input type="text" name="newexpresscode" value="">'
	));
	showsubmit('changesubmit', cplang('product_order_changestate'), $multipage);
	showtablefooter();
	showformfooter();
	exit;
}elseif ($operation == 'cancel'){
	cpheader();
	if(!submitcheck('cancelsubmit')){
		// 取消订单申请
		$page = max(1, $_G['page']);
		$pagesize = 15;
		
		$ordernum = C::t('common_member_order_cancel')->count_where();
		
		$orders = '';
		if($ordernum > 0) {
			require_once libfile('function/order');
			$multipage = multi($ordernum, $pagesize, $page, ADMINSCRIPT."?action=productorder&operation=cancel");
	
			$allorder = C::t('common_member_order_cancel')->fetch_all_page($page, $pagesize);
			foreach($allorder as $k=>$order) {
				$checkbox = ($order['state'] == 0) ? "<input type=\"checkbox\" name=\"ocidarray[]\" value=\"$order[ocid]|$order[oid]\" class=\"checkbox orderCheck\">" : '';
				$orders .= showtablerow('', array('class="td25"'), array(
					$checkbox,
					$order['o_openid'],
					//"<a href=\"home.php?mod=space&uid=$order[uid]\" target=\"_blank\">$order[username]</a>",
					$order['totalprice'],
					date('Y-m-d H:i', $order['dateline']),
					get_cancel_type_name($order['type']),
					$order['note'],
					$order['expresscode'],
					'<p><span class="title">'.$lang[member_address_address].'：</span><span class="data">'.getFullAddress($order).'</span></p><p><span class="title">'.$lang[member_address_name].'：</span><span class="data">'.$order['receiver'].'</span></p><p><span class="title">'.$lang[member_address_phone].'：</span><span class="data">'.$order['phone'].'</span></p>',
					get_state_name($order['ostate'])
				), TRUE);
			}
		}
		
		shownav('product', 'product_order_admin_cancel');
		showsubmenu('product_order_admin_cancel');
		
		showformheader("productorder&operation=cancel&page=".$page);
		showtableheader(cplang('product_order_list'));
	
		if($ordernum) {
			showsubtitle(array('', 'product_order_openid', 'product_order_totalprice', 'product_order_cancel_dateline', 'product_order_cancel_type', 'product_order_cancel_note', 'product_order_expresscode', 'member_address_info', 'product_order_state'));
			echo $orders;
		}else{
			showsubtitle(array(cplang('product_order_list_empty')));
		}
		showtablefooter();
		
		showtableheader(cplang('product_order_changestate_option'));
		showsetting('product_order_iscancel', 'newstate', true, 'radio');
		showsetting('product_order_cancel_reason', 'newreason', '', 'textarea');
		showsubmit('cancelsubmit', cplang('product_order_changestate'), $multipage);
		showtablefooter();
		showformfooter();
	}else{
		$ocids = $_GET['ocidarray'];
		$newstate = intval($_GET['newstate']);
		$newreason = $_GET['newreason'];
		$page = intval($_GET['page']);
		$ocids = (is_array($ocids)) ? $ocids : array($ocids);
		
		if($newstate){
			$ostate = -2;
			$ocstate = 1;
		}else{
			$ostate = -3;
			$ocstate = 2;
		}
		
		require_once libfile('function/order');
		foreach($ocids as $v){
			list($this_ocid, $this_oid) = explode('|', $v);
			change_state($this_oid, $ostate);
			$update_data = array(
				'state'=>$ocstate,
				'reason'=>$newreason
			);
			C::t('common_member_order_cancel')->update($this_ocid, $update_data);
			//发送消息模板
			changeOrderStatuMessage($this_oid,'returnordersure',$update_data);
		}
		cpmsg('product_order_change_succeed', 'action=productorder&operation=cancel&page='.$page, 'succeed');
	}
	exit;
	
}elseif ($operation == 'changestate'){
	cpheader();
	if(submitcheck('changesubmit')){
		$oids = $_GET['oidarray'];
		$state = intval($_GET['state']);
		$page = intval($_GET['page']);
		$newexpresscom = intval($_GET['newexpresscom']);
		$newexpresscode = $_GET['newexpresscode'];
		$oids = (is_array($oids)) ? $oids : array($oids);
		
		require_once libfile('function/order');
		$res = true;
		foreach($oids as $oid){
			$issucceed = change_state($oid, $state);
			if(!$issucceed) $res = false;
		}
		if($newexpresscom > 0){
			$update_data = array('expresscom'=>$newexpresscom, 'expresscode'=>$newexpresscode);
			C::t('common_member_order')->update($oids, $update_data);
			//更改订单状态消息提醒（微信模板消息,短讯消息）
			changeOrderStatuMessage($oids,'orderdelivery');
		}
		cpmsg('product_order_change_succeed', 'action=productorder&operation='.I('referer','admin').'&page='.$page, 'succeed');
	}else{
		cpmsg('product_order_change_valid','','error');
	}
	exit;
}elseif ($operation == 'ajaxGetOrderDetail'){
	$oid = intval($_GET['oid']);
	$return = array();
	if($oid > 0){
		$product_list = C::t('common_member_order_detail')->fetch_all_by_oid($oid);
		
		if(!$_G['cache']['product']){
			loadcache('product');
		}

		foreach($product_list as $p){
			$pdonsell = C::t('common_product_onsell')->fetch($p['spid']);

			$pd = $_G['cache']['product'][$pdonsell['pdid']];
			$pd['amount'] = $p['amount'];
			$return[] = $pd;
		}
	}
	echo json_encode($return, JSON_UNESCAPED_UNICODE);
	exit;
}

// 订单容器
function echo_product_list_container(){
	showtableheader(cplang('product_order_detail'), 'product-list-container');
	showsubtitle(array(
		cplang('product_name'),cplang('product_order_detail_num')
	));
	showtablefooter();
}

// 选中订单，获取产品，并注入容器
function echo_product_list_jsfunction(){
	echo <<<EOT
<script type="text/javascript">
jQuery(function(){
	jQuery("input.orderCheck").change(function(){
		var _this = jQuery(this),
			isChecked = _this.prop("checked"),
			oid = _this.val();
		if(isChecked){
			jQuery.ajax({
				"url":"admin.php",
				"data":{
					"action":"productorder",
					"operation":"ajaxGetOrderDetail",
					"oid":oid
				},
				"dataType":"json",
				"success":function(data){
					var pd = data,
						table = jQuery(".product-list-container");
					for(var i = 0; i < pd.length; i++){
						var tr = '<tr data-content="' + oid + '"><td>' + pd[i]['name'] + '</td><td>' + pd[i]['amount'] + '</td></tr>';
						jQuery(tr).appendTo(table);
					}
				}
			})
		}else{
			jQuery(".product-list-container").find("tr[data-content='" + oid + "']").remove();
		}
	});
});
</script>
EOT;
}
