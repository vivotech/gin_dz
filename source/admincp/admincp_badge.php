<?php

/**
 * 徽章管理页面
 */

if(!defined('IN_DISCUZ') || !defined('IN_ADMINCP')) {
	exit('Access Denied');
}

require_once libfile('function/url');
require_once libfile('function/badge');

$operation_array = array('index', 'edit', 'delete');
$operation = getgpc('operation');
if (empty($operation))
{
	$operation = 'index';
}
$action = getgpc('action');
if(!in_array($operation, $operation_array)){
	redirect('/');
}

// 徽章列表
if ( $operation === "index" )
{
	$topmenu = array(
		array('徽章列表', $action.'&operation=index' , $operation=='index'),
		array('新建徽章', $action.'&operation=edit' , $operation=='edit')
	);
	$tpl->showsubmenu('徽章管理', $topmenu);
	
	$badge_list = getBadges();
	
	$tpl->assign('badge_list', $badge_list);
	$tpl->display('index', 'badge');
}
// 徽章编辑
else if ( $operation === "edit" ) {
	
	$message = '';
	$name = getgpc('name');
	$badge_id = getgpc('badge_id');
	
	// 保存数据
	if ($name)
	{
		$check_result = true;
		
		$task = getgpc('task');
		
		// 检查任务类型
		if ( !in_array($task, array('insert', 'update')))
		{
			$message = '不明的操作。';
			$check_result = false;
		}
		
		// 插入时徽章ID不能为空
		if ( $task == 'insert' && empty($badge_id) )
		{
			$message = '徽章ID不能为空。';
			$check_result = false;
		}
		
		// 插入时徽章ID不能重复
		if ( $task == 'insert' && getBadgeById($badge_id) ) {
			$message = '该徽章ID已经存在，不能重复。';
			$check_result = false;
		}
		
		if ($check_result)
		{
			// 模型数据
			$model_data = array(
				'name'	=> $name,
				'displayorder'	=> (int)getgpc('displayorder'),
				'desc'			=> getgpc('desc'),
                'state'         => getgpc('state')
			);
			
			if ($task =='insert')
			{
				$model_data['badge'] = $badge_id;
			}
			
			for ($i=1; $i<=7; $i++)
			{
				$model_data['lv'.$i]	= getgpc('lv'.$i);
				$model_data['lv'.$i.'_limit']	= (int)getgpc('lv'.$i.'_limit');
				$model_data['lv'.$i.'_image']	= getgpc('lv'.$i.'_image');
			}
			
			// 更新或插入新记录
			if ( $task == 'update' )
			{
				$operation_result = C::t('badge_rule')->update($badge_id, $model_data);
			} else {
				$operation_result = C::t('badge_rule')->insert($model_data);
			}
			
			if ($operation_result)
			{
				$message = '保存成功';
				if ( $task == 'insert' )
				{
					$url = '/admin.php?action='.$action.'&operation=edit&badge_id='.$badge_id;
					redirect($url);
				}
				updatecache('badgeslist');
			} else {
				$message = '保存失败';
			}
		}
			
	}
	
	// 开始渲染编辑页面
	
	// 添加菜单
	$topmenu = array(
		array('徽章列表', $action.'&operation=index' , $operation=='index'),
		array($badge_id ? '编辑徽章' : '新建徽章', $action.'&operation=edit' , $operation=='edit')
	);
	$tpl->showsubmenu('徽章管理', $topmenu);
	
	// 获取徽章记录
	$badge = $badge_id ? getBadgeById($badge_id) : null;
	
	// display
	$tpl->assign('message', $message);
	$tpl->assign('badge', $badge);
	$tpl->assign('badge_id', $badge_id);
	$tpl->display('edit', 'badge');
}
// 删除
else if ( $operation === "delete" ) {
	
	$badge_id = getgpc('badge_id');
	
	$index_url = '/admin.php?action=badge';
	
	if ( empty($badge_id) )
	{
		redirect($index_url);
		exit;
	}
	
	// 获取徽章记录
	$badge = getBadgeById($badge_id);
	if ( !$badge )
	{
		redirect($index_url);
		exit;
	}

	// 删除记录
	C::t('badge_rule')->delete($badge['badge']);
	
	// 更新缓存
	updatecache('badgeslist');
	
	// 回到索引页
	redirect($index_url);

}


