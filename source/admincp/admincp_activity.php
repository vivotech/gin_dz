<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: admincp_forums.php 34181 2013-10-29 08:23:15Z nemohou $
 */

if(!defined('IN_DISCUZ') || !defined('IN_ADMINCP')) {
	exit('Access Denied');
}

cpheader();

$operation = empty($operation) ? 'admin' : $operation;
$fid = intval(getgpc('fid'));

if($operation == 'admin') {
	if(!submitcheck('editsubmit')) {
		shownav('forum', 'forums_activity_admin');
		showsubmenu('forums_activity_admin');
		showtips('forums_admin_tips');

		require_once libfile('function/forumlist');
		$forums = str_replace("'", "\'", forumselect(false, 0, 0, 1));

?>
<script type="text/JavaScript">
var forumselect = '<?php echo $forums;?>';
var rowtypedata = [
	[[1, ''], [1,'<input type="text" class="txt" name="newcatorder[]" value="0" />', 'td25'], [5, '<div><input name="newcat[]" value="<?php cplang('forums_admin_add_category_name', null, true);?>" size="20" type="text" class="txt" /><a href="javascript:;" class="deleterow" onClick="deleterow(this)"><?php cplang('delete', null, true);?></a></div>']],
	[[1, ''], [1,'<input type="text" class="txt" name="neworder[{1}][]" value="0" />', 'td25'], [5, '<div class="board"><input name="newforum[{1}][]" value="<?php cplang('forums_admin_add_forum_name', null, true);?>" size="20" type="text" class="txt" /><a href="javascript:;" class="deleterow" onClick="deleterow(this)"><?php cplang('delete', null, true);?></a><select name="newinherited[{1}][]"><option value=""><?php cplang('forums_edit_newinherited', null, true);?></option>' + forumselect + '</select></div>']],
	[[1, ''], [1,'<input type="text" class="txt" name="neworder[{1}][]" value="0" />', 'td25'], [5, '<div class="childboard"><input name="newforum[{1}][]" value="<?php cplang('forums_admin_add_forum_name', null, true);?>" size="20" type="text" class="txt" /><a href="javascript:;" class="deleterow" onClick="deleterow(this)"><?php cplang('delete', null, true);?></a>&nbsp;<label><input name="inherited[{1}][]" type="checkbox" class="checkbox" value="1">&nbsp;<?php cplang('forums_edit_inherited', null, true);?></label></div>']],
];
</script>
<?php
		showformheader('activity');
		echo '<div style="height:30px;line-height:30px;"><a href="javascript:;" onclick="show_all()">'.cplang('show_all').'</a> | <a href="javascript:;" onclick="hide_all()">'.cplang('hide_all').'</a> <input type="text" id="srchforumipt" class="txt" /> <input type="submit" class="btn" value="'.cplang('search').'" onclick="return srchforum()" /></div>';
		showtableheader('');
		showsubtitle(array('', 'display_order', 'forums_admin_name', '', 'forums_moderators', ''));

		$forumcount = C::t('forum_forum')->fetch_forum_num();

		$query = C::t('forum_forum')->fetch_all_forum_for_sub_order();
		$groups = $forums = $subs = $fids = $showed = array();
		foreach($query as $forum) {
			if($forum['type'] == 'group') {
				$groups[$forum['fid']] = $forum;
			} elseif($forum['type'] == 'sub') {
				$subs[$forum['fup']][] = $forum;
			} else {
				$forums[$forum['fup']][] = $forum;
			}
			$fids[] = $forum['fid'];
		}

		foreach ($groups as $id => $gforum) {
			$toggle = $forumcount > 50 && count($forums[$id]) > 2;
			$showed[] = showforum($gforum, 'group', '', $toggle);
			if(!empty($forums[$id])) {
				foreach ($forums[$id] as $forum) {
					$showed[] = showforum($forum);
					$lastfid = 0;
					if(!empty($subs[$forum['fid']])) {
						foreach ($subs[$forum['fid']] as $sub) {
							$showed[] = showforum($sub, 'sub');
							$lastfid = $sub['fid'];
						}
					}
					showforum($forum, $lastfid, 'lastchildboard');
				}
			}
			showforum($gforum, '', 'lastboard');
		}

		if(count($fids) != count($showed)) {
			foreach($fids as $fid) {
				if(!in_array($fid, $showed)) {
					C::t('forum_forum')->update($fid, array('fup' => '0', 'type' => 'forum'));
				}
			}
		}

		showforum($gforum, '', 'last');

		showsubmit('editsubmit');
		showtablefooter();
		showformfooter();

	} else {
		$usergroups = array();
		$query = C::t('common_usergroup')->range();
		foreach($query as $group) {
			$usergroups[$group['groupid']] = $group;
		}

		if(is_array($_GET['order'])) {
			foreach($_GET['order'] as $fid => $value) {
				C::t('forum_forum')->update($fid, array('name' => $_GET['name'][$fid], 'displayorder' => $_GET['order'][$fid]));
			}
		}

		if(is_array($_GET['newcat'])) {
			foreach($_GET['newcat'] as $key => $forumname) {
				if(empty($forumname)) {
					continue;
				}
				$fid = C::t('forum_forum')->insert(array('type' => 'group', 'name' => $forumname, 'status' => 1, 'displayorder' => $_GET['newcatorder'][$key]), 1);
				C::t('forum_forumfield')->insert(array('fid' => $fid));
			}
		}

		$table_forum_columns = array('fup', 'type', 'name', 'status', 'displayorder', 'styleid', 'allowsmilies',
			'allowhtml', 'allowbbcode', 'allowimgcode', 'allowanonymous', 'allowpostspecial', 'alloweditrules',
			'alloweditpost', 'modnewposts', 'recyclebin', 'jammer', 'forumcolumns', 'threadcaches', 'disablewatermark', 'disablethumb',
			'autoclose', 'simple', 'allowside', 'allowfeed', 'is_special_template', 'special_template');
		$table_forumfield_columns = array('fid', 'attachextensions', 'threadtypes', 'viewperm', 'postperm', 'replyperm',
			'getattachperm', 'postattachperm', 'postimageperm');

		if(is_array($_GET['newforum'])) {

			foreach($_GET['newforum'] as $fup => $forums) {

				$fupforum = C::t('forum_forum')->get_forum_by_fid($fup);
				if(empty($fupforum)) continue;

				if($fupforum['fup']) {
					$groupforum = C::t('forum_forum')->get_forum_by_fid($fupforum['fup']);
				} else {
					$groupforum = $fupforum;
				}

				foreach($forums as $key => $forumname) {

					if(empty($forumname) || strlen($forumname) > 50) continue;

					$forum = $forumfields = array();
					$inheritedid = !empty($_GET['inherited'][$fup]) ? $fup : (!empty($_GET['newinherited'][$fup][$key]) ? $_GET['newinherited'][$fup][$key] : '');

					if(!empty($inheritedid)) {

						$forum = C::t('forum_forum')->get_forum_by_fid($inheritedid);
						$forumfield =  C::t('forum_forum')->get_forum_by_fid($inheritedid, null, 'forumfield');

						foreach($table_forum_columns as $field) {
							$forumfields[$field] = $forum[$field];
						}

						foreach($table_forumfield_columns as $field) {
							$forumfields[$field] = $forumfield[$field];
						}

					} else {
						$forumfields['allowsmilies'] = $forumfields['allowbbcode'] = $forumfields['allowimgcode'] = 1;
						$forumfields['allowpostspecial'] = 1;
						$forumfields['allowside'] = 0;
						$forumfields['allowfeed'] = 0;
						$forumfields['recyclebin'] = 1;
					}

					$forumfields['fup'] = $fup ? $fup : 0;
					$forumfields['type'] = $fupforum['type'] == 'forum' ? 'sub' : 'forum';
					$forumfields['styleid'] = $groupforum['styleid'];
					$forumfields['name'] = $forumname;
					$forumfields['status'] = 1;
					$forumfields['displayorder'] = $_GET['neworder'][$fup][$key];
					if($forumfields['type'] == 'sub'){
						$forumfields['is_special_template'] = 1;
						$forumfields['special_template'] = 'forumdisplay_activity_detail';
					}else{
						$forumfields['is_special_template'] = 0;
						$forumfields['special_template'] = '';
					}

					$data = array();
					foreach($table_forum_columns as $field) {
						if(isset($forumfields[$field])) {
							$data[$field] = $forumfields[$field];
						}
					}

					$forumfields['fid'] = $fid = C::t('forum_forum')->insert($data, 1);

					$data = array();
					$forumfields['threadtypes'] = copy_threadclasses($forumfields['threadtypes'], $fid);
					foreach($table_forumfield_columns as $field) {
						if(isset($forumfields[$field])) {
							$data[$field] = $forumfields[$field];
						}
					}

					C::t('forum_forumfield')->insert($data);

					foreach(C::t('forum_moderator')->fetch_all_by_fid($fup, false) as $mod) {
						if($mod['inherited'] || $fupforum['inheritedmod']) {
							C::t('forum_moderator')->insert(array('uid' => $mod['uid'], 'fid' => $fid, 'inherited' => 1), false, true);
						}
					}
				}
			}
		}


		updatecache('forums');

		cpmsg('forums_update_succeed', 'action=activity', 'succeed');
	}
}else if($operation == 'set_activity'){
	if(!submitcheck('setsubmit')) {
		$fid = $_GET['fid'] ? intval($_GET['fid']) : 0;
		
		$this_forum = C::t('forum_forum')->get_forum_by_fid($fid);
		if(!$this_forum){
			cpmsg('forums_activity_set_loaderr', '', 'error');
		}
		
		$input_forum = array(
			'forumname'=>$this_forum['name'],
			'newisactivity'=>0,
			'newtype'=>0,
			'newclosed'=>0,
			'newname'=>$this_forum['name'],
			'newdesc'=>'活动介绍',
			'newbtimeline'=>date('Y-m-d'),
			'newetimeline'=>'',
			'newpic'=>'/static/image/vivo/logo_yellow.jpg',
			'spid'=>0
		);
		
		// 读取旧数据
		$activity_data = C::t('forum_custom_activity')->fetch_by_fid($fid);
		if(!empty($activity_data)){
			$input_forum['newisactivity'] = 1;
			$input_forum['newtype'] = $activity_data['type'];
			$input_forum['newclosed'] = $activity_data['closed'];
			$input_forum['newname'] = $activity_data['name'];
			$input_forum['newdesc'] = $activity_data['desc'];
			$input_forum['newpic'] = $activity_data['pic'];
			$input_forum['newbtimeline'] = $activity_data['btimeline'] > 0 ? date('Y-m-d', $activity_data['btimeline']) : '';
			$input_forum['newetimeline'] = $activity_data['etimeline'] > 0 ? date('Y-m-d', $activity_data['etimeline']) : '';
			$input_forum['color'] = $activity_data['color'];
			$input_forum['spid'] = $activity_data['spid'];
			$input_forum['rank'] = $activity_data['rank'];
		}else{
			$activity_data['caid'] = 0;
		}
		
		require_once libfile('function/forumactivity');
		$typeselect = froumactivitytypeselect($input_forum['newtype']);
		
		require_once libfile('function/productserieslist');
		require_once libfile('function/product');
		if($input_forum['spid'] <= 0){
			// 获取产品类型
			if(!isset($_G['cache']['productseries'])){
				loadcache('productseries');
			}
			$seriescache = $_G['cache']['productseries'];
			$selectseriesid = 0;
			foreach($seriescache as $s){
				if($s['type'] == 'series'){
					$selectseriesid = $s['psid'];
					break;
				}
			}
			$selectspid = 0;
		}else{
			$selectspid = $input_forum['spid'];
			$thispd = getProduct($selectspid);
			$selectseriesid = $thispd['psid'];
		}
		
		require_once libfile('function/productserieslist');
		$seriesselect = productseriesselect(false, 0, $selectseriesid, 1);
		
		$product_list = C::t('common_product')->fetch_all_by_psid($selectseriesid);
		$pdids = array();
		$productselect = '';
		if(count($product_list) > 0){
			foreach($product_list as $k=>$p){
				$pdids[] = $p['pdid'];
			}
		}
		$productonsell_list = getProductList($pdids);
		if(count($productonsell_list) > 0){
			foreach($productonsell_list as $k=>$sp){
				if($sp['spid'] == $selectspid){
					$productselect .= '<option value="'.$sp['spid'].'" selected>('.$sp['subject'].')'.$sp['name'].'</option>';
				}else{
					$productselect .= '<option value="'.$sp['spid'].'">('.$sp['subject'].')'.$sp['name'].'</option>';
				}
			}
		}
		
		echo '<script src="'.$_G['setting']['jspath'].'calendar.js?'.$_G['setting']['verhash'].'" type="text/javascript"></script>';
		
		// 初始化奖励设置tr
		$rewardtr = showtablerow('class="rewardtr"', array('',''), get_reward_tr(), true);
		$rewardtr = str_replace("\n", '', $rewardtr);
		echo <<<EOT
<script type="text/javascript">
function addRewardTr(){
	var tr = jQuery('{$rewardtr}');
	tr.insertBefore(jQuery("#submit_setsubmit").parent().parent().parent());
}
function removeRewardTr(obj){
	var o = jQuery(obj);
	o.parent().parent().remove();
}
</script>
EOT;
		setqiniuconfig();
		setcolorpicker();
		shownav('forum', 'forums_activity_set');
		showsubmenu('forums_activity_set');
		showformheader('activity&operation=set_activity', 'enctype');
		showtableheader();
		showsetting('forums_activity_set_forumname', 'forumname', $input_forum['forumname'], 'text', 'readonly');
		showcolorpicker('标题颜色', 'color',$input_forum['color']);
		showsetting('forums_activity_set_switch', 'newisactivity', $input_forum['newisactivity'], 'radio');
		showsetting('forums_activity_set_type', '', '', '<select name="newtype">'.$typeselect.'</select>');
		showsetting('forums_activity_set_closed', 'newclosed', $input_forum['newclosed'], 'radio');
		showsetting('forums_activity_set_name', 'newname', $input_forum['newname'], 'text');
		showqiniuupload('newpic', $input_forum['newpic']);
		showsetting('forums_activity_set_desc', 'newdesc', $input_forum['newdesc'], 'textarea');
		showsetting('forums_activity_set_btimeline', 'newbtimeline', $input_forum['newbtimeline'], 'calendar');
		showsetting('forums_activity_set_etimeline', 'newetimeline', $input_forum['newetimeline'], 'calendar');
		showsetting('productseries_name', '', '', '<select name="newseriesid" id="newseriesid">'.$seriesselect.'</select>');
		showsetting('product_name', 'newspid', '', '<select name="newspid" id="newspid">'.$productselect.'</select>');
		//排序
		$input_forum['rank'] = !empty($input_forum['rank'])?$input_forum['rank']:'[L]*1+[R]*1+[G]*1+[F]*1';
		$rank_desc = '[L]：浏览量 ；[R]：回复数；[G]：点赞数；[F]：收藏数<br>【相乘得数字代表所占的权重】';
		showsetting('forums_activity_rank', 'rank', $input_forum['rank'], 'text','','',$rank_desc);
		
		if(!isset($activity_data['awarded']) || $activity_data['awarded'] <= 0){
			echo '<tr><td colspan="2" class="td27" s="1">活动奖项设定：(颁奖后将无法设定)</td></tr>';
			if($activity_data['caid'] > 0){
				$rewards = C::t('forum_custom_activity_rewardrule')->fetch_all_by_caid($activity_data['caid']);
				if(count($rewards) > 0){
					foreach($rewards as $r){
						showtablerow('class="rewardtr"', array('',''), get_reward_tr($r));
					}
				}
			}
			echo $rewardtr;
		}
		echo '<input type="hidden" name="fid" value="'.$fid.'">';
		echo '<input type="hidden" name="caid" value="'.$activity_data['caid'].'">';
		showsubmit('setsubmit');
		showtablefooter();
		showformfooter();

		select_series_change_product_script();
	} else {
		$fid = trim($_GET['fid']);
		$caid = trim($_GET['caid']);
		$newisactivity = trim($_GET['newisactivity']);
		$newtype = trim($_GET['newtype']);
		$newclosed = trim($_GET['newclosed']);
		$newname = trim($_GET['newname']);
		$newdesc = trim($_GET['newdesc']);
		$newpic = trim($_GET['newpic']);
		$newbtimeline = trim($_GET['newbtimeline']);
		$newetimeline = trim($_GET['newetimeline']);
		$color = I('color','');
		$rank = I('rank','[L]*1+[R]*1+[G]*1+[F]*1');
		$newseriesid = I('newseriesid');
		$newspid = I('newspid');
		
		if(!$fid){
			cpmsg('forums_activity_set_loaderr', '', 'error');
		}
		
		if($rank){
			if( strstr($rank,'[L]*')==FALSE || strstr($rank,'[R]*')==FALSE
				|| strstr($rank,'[G]*')==FALSE || strstr($rank,'[F]*')==FALSE){
				cpmsg('forums_activity_rank_error','','error');
			}
		}
		
		$result = false;
		
		if($newisactivity){
			if($caid){
				$update_data = array(
					'fid'=>$fid,
					'uid'=>$_G['uid'],
					'type'=>$newtype,
					'closed'=>$newclosed,
					'name'=>$newname,
					'desc'=>$newdesc,
					'pic'=>$newpic,
					'btimeline'=>strtotime($newbtimeline),
					'etimeline'=>strtotime($newetimeline),
					'color'=>$color,
					'spid'=>$newspid,
					'rank'=>$rank
				);
				$affect_rows = C::t('forum_custom_activity')->update($caid, $update_data);
				$result = true;
			}else{
				$insert_data = array(
					'fid'=>$fid,
					'uid'=>$_G['uid'],
					'type'=>$newtype,
					'closed'=>$newclosed,
					'name'=>$newname,
					'desc'=>$newdesc,
					'pic'=>$newpic,
					'btimeline'=>strtotime($newbtimeline),
					'etimeline'=>strtotime($newetimeline),
					'color'=>$color,
					'spid'=>$newspid,
					'rank'=>$rank
				);
				$caid = C::t('forum_custom_activity')->insert($insert_data, 1);
				if($caid > 0){
					$result = true;
				}
			}
			// 更新奖励规则
			if($caid){
				$this_activity = C::t('forum_custom_activity')->fetch($caid);
				if($this_activity['awarded'] == 0){
					$newrewardname = $_GET['newrewardname'];
					$newrewardcredittype = $_GET['newrewardcredittype'];
					$newrewardcredit = $_GET['newrewardcredit'];
					$newrewardtotalnum = $_GET['newrewardtotalnum'];
					
					// 删除旧规则
					C::t('forum_custom_activity_rewardrule')->delete_by_caid($caid);
					
					// 插入新规则
					if(count($newrewardcredit) > 0){
						foreach($newrewardcredit as $k=>$v){
							if(!empty($newrewardname[$k]) && !empty($newrewardcredittype[$k]) && !empty($v) && !empty($newrewardtotalnum[$k])){
								$insert_data = array(
									'caid'=>$caid,
									'name'=>$newrewardname[$k],
									'credittype'=>$newrewardcredittype[$k],
									'credit'=>$v,
									'totalnum'=>$newrewardtotalnum[$k]
								);
								C::t('forum_custom_activity_rewardrule')->insert($insert_data);
							}
						}
					}
				}
			}
			updatecache('forumactivity');
		}else{
			// 删除活动论坛记录
			// 删除活动论坛参加记录
		}
		if($result){
			cpmsg('forums_activity_set_succeed', 'action=activity&operation=admin', 'succeed');
		}else{
			cpmsg('forums_activity_set_error', '', 'error');
		}
	}
}else if($operation == 'award'){
	if(!submitcheck('rewardsubmit')) {
		$fid = $_GET['fid'] ? intval($_GET['fid']) : 0;
		
		// 读取旧数据
		$activity_data = C::t('forum_custom_activity')->fetch_by_fid($fid);
		// 读取奖励数据
		$rewards = C::t('forum_custom_activity_rewardrule')->fetch_all_by_caid($activity_data['caid']);
		if(count($rewards) <= 0){
			cpmsg('forums_activity_reward_nodata', '', 'error');
		}else{
			$rewards_data = array();
			foreach($rewards as $k=>$reward){
				$rewards_data[$reward['carrid']] = $reward;
			}
			$rewards = $rewards_data;
		}
		// 读取参与人员
		$join_us = C::t('forum_custom_activity_join')->fetch_all_by_caid($activity_data['caid']);
		// 读取已颁奖数据
		$awarded_rewards = C::t('forum_custom_activity_reward')->fetch_all_by_caid($activity_data['caid']);
		foreach($awarded_rewards as $k=>$awarded_reward){
			$rewards[$awarded_reward['carrid']]['awarded'][] = $awarded_reward;
		}
		
		echo '<link rel="stylesheet" href="/static/jquery/jquery-ui-1.10.3/themes/base/jquery-ui.css">';
		echo '<script src="/static/jquery/jquery-ui-1.10.3/ui/jquery-ui.js"></script>';
		shownav('forum', 'forums_activity_reward');
		showsubmenu('forums_activity_reward');
		showformheader('activity&operation=award', 'enctype');
		showtableheader();
		echo '<tr><td colspan="2" class="td27" s="1">活动名称：'.$activity_data['name'].'</td></tr>';
		foreach($rewards as $k=>$reward){
			echo '<tr><td colspan="2" class="td27" s="1">'.$reward['name'].'&nbsp;&nbsp;('.$_G['setting']['extcredits'][$reward['credittype']]['title'].'：'.$reward['credit'].')</td></tr>';
			for($i = 0; $i < $reward['totalnum']; $i++){
				if(isset($reward['awarded'][$i]['uid'])){
					$input_arr = array('carrid'=>$reward['carrid'], 'key'=>$i, 'nickname'=>$reward['awarded'][$i]['nickname'], 'uid'=>$reward['awarded'][$i]['uid']);
				}else{
					$input_arr = array('carrid'=>$reward['carrid'],'key'=>$i);
				}
				echo get_reward_input($input_arr);
			}
		}
		echo '<input type="hidden" name="fid" value="'.$fid.'">';
		echo '<input type="hidden" name="caid" value="'.$activity_data['caid'].'">';
		showsubmit('rewardsubmit');
		showtablefooter();
		showformfooter();
?>
<style>
.ui-autocomplete {
  max-height: 300px;
  overflow-y: auto;
  overflow-x: hidden;
}
* html .ui-autocomplete {
  height: 300px;
}
</style>
<script type="text/javascript">
var userDatas = [
<?php
if(count($join_us) > 0){
	foreach($join_us as $k=>$u){
		if($k > 0) echo ',';
		echo '{"value":"'.$u['nickname'].'","uid":"'.$u['uid'].'"}';
	}
}
?>
];
jQuery(".rewardusername").autocomplete({
	minLength: 1,
	source:userDatas,
	focus:function(event, ui){
		jQuery(this).val(ui.item.value);
		return false;
	},
	select: function(event, ui){
		jQuery(this).parent().siblings(".vtop").find(".uid").val(ui.item.uid);
	}
});
</script>
<?php
	}else{
		$fid = ($_GET['fid']) ? intval($_GET['fid']) : 0;
		$caid = ($_GET['caid']) ? intval($_GET['caid']) : 0;
		$rewards = C::t('forum_custom_activity_rewardrule')->fetch_all_by_caid($caid);
		if(count($rewards) <= 0){
			cpmsg('forums_activity_reward_nodata', '', 'error');
		}
		
		$awarded = false;
		
		foreach($rewards as $k=>$reward){
			for($i = 0; $i < $reward['totalnum']; $i++){
				// 判断有否设置颁奖
				$uid = $_GET['uid_'.$reward['carrid'].'_'.$i];
				$nickname = $_GET['nickname_'.$reward['carrid'].'_'.$i];
				$oldrewardsnum = $rewards[$k]['num'];
				
				if($rewards[$k]['num'] < $rewards[$k]['totalnum']){
					$awarded = true;
					// 新增颁奖记录
					$insert_data = array(
						'caid'=>$caid,
						'carrid'=>$reward['carrid'],
						'uid'=>$uid,
						'nickname'=>$nickname
					);
					C::t('forum_custom_activity_reward')->insert($insert_data);
					// 颁发奖项
					updatemembercount($uid, array($reward['credittype'] => intval($reward['credit'])), false, 'AAC', $fid);
					$rewards[$k]['num']++;
				}
				
				// 更新已颁奖数目
				if($oldrewardsnum != $rewards[$k]['num']){
					$update_data = array('num'=>$rewards[$k]['num']);
					C::t('forum_custom_activity_rewardrule')->update($reward['carrid'], $rewards[$k]['num']);
				}
			}
		}
		// 设置原活动为已颁奖状态，使其无法修改
		if($awarded){
			$update_data = array('awarded'=>1);
			C::t('forum_custom_activity')->update($caid, $update_data);
		}
		
		cpmsg('forums_activity_reward_succeed', 'action=activity', 'succeed');
	}
}

function showforum(&$forum, $type = '', $last = '', $toggle = false) {

	global $_G;
	
	if($last == '') {

		$navs = array();
		foreach(C::t('common_nav')->fetch_all_by_navtype_type(0, 5) as $nav) {
			$navs[] = $nav['identifier'];
		}
		$return = '<tr class="hover">'.
			'<td class="td25"'.($type == 'group' ? ' onclick="toggle_group(\'group_'.$forum['fid'].'\', $(\'a_group_'.$forum['fid'].'\'))"' : '').'>'.($type == 'group' ? '<a href="javascript:;" id="a_group_'.$forum['fid'].'">'.($toggle ? '[+]' : '[-]').'</a>' : '').'</td>
			<td class="td25"><input type="text" class="txt" name="order['.$forum['fid'].']" value="'.$forum['displayorder'].'" /></td><td>';
		if($type == 'group') {
			$return .= '<div class="parentboard">';
			$_G['fg'] = !empty($_G['fg']) ? intval($_G['fg']) : 0;
			$_G['fg']++;
		} elseif($type == '') {
			$return .= '<div class="board">';
		} elseif($type == 'sub') {
			$return .= '<div id="cb_'.$forum['fid'].'" class="childboard">';
		}

		$boardattr = '';
		if(!$forum['status']  || $forum['password'] || $forum['redirect'] || in_array($forum['fid'], $navs)) {
			$boardattr = '<div class="boardattr">';
			$boardattr .= $forum['status'] ? '' : cplang('forums_admin_hidden');
			$boardattr .= !$forum['password'] ? '' : ' '.cplang('forums_admin_password');
			$boardattr .= !$forum['redirect'] ? '' : ' '.cplang('forums_admin_url');
			$boardattr .= !in_array($forum['fid'], $navs) ? '' : ' '.cplang('misc_customnav_parent_top');
			$boardattr .= '</div>';
		}

		if($forum['type'] == 'sub'){
			$this_activity = C::t('forum_custom_activity')->fetch_by_fid($forum['fid']);
			if($this_activity){
				if($this_activity['closed']){
					$this_activity_op = '<a href="'.ADMINSCRIPT.'?action=activity&operation=set_activity&fid='.$forum['fid'].'" title="'.cplang('forums_edit_comment').'" class="act">'.cplang('forums_activity_admin_actionclosed').'</a>';
				}else{
					$this_activity_op = '<a href="'.ADMINSCRIPT.'?action=activity&operation=set_activity&fid='.$forum['fid'].'" title="'.cplang('forums_edit_comment').'" class="act">'.cplang('forums_activity_admin_actionopened').'</a>';
				}
			}else{
				$this_activity_op = '<a href="'.ADMINSCRIPT.'?action=activity&operation=set_activity&fid='.$forum['fid'].'" title="'.cplang('forums_edit_comment').'" class="act">'.cplang('forums_activity_admin_action').'</a>';
			}
			$this_activity_op .= '<a href="'.ADMINSCRIPT.'?action=activity&operation=award&fid='.$forum['fid'].'" class="act">'.cplang('forums_activity_admin_reward').'</a>';
		}else{
			$this_activity = '';
		}
		
		$return .= '<input type="text" name="name['.$forum['fid'].']" value="'.dhtmlspecialchars($forum['name']).'" class="txt" />'.
			($type == '' ? '<a href="###" onclick="addrowdirect = 1;addrow(this, 2, '.$forum['fid'].')" class="addchildboard">'.cplang('forums_admin_add_subactivity').'</a>' : '').
			'</div>'.$boardattr.
			'</td><td align="right" class="td23 lightfont">('.($type == 'group' ? 'gid:' : 'fid:').$forum['fid'].')</td>'.
			'</td><td class="td23">'.showforum_moderators($forum).'</td>
			<td width="160">'.$this_activity_op.'</td></tr>';
		if($type == 'group') $return .= '<tbody id="group_'.$forum['fid'].'"'.($toggle ? ' style="display:none;"' : '').'>';
	} else {
		if($last == 'lastboard') {
			$return = '</tbody><tr><td></td><td colspan="4"><div class="lastboard"><a href="###" onclick="addrow(this, 1, '.$forum['fid'].')" class="addtr">'.cplang('forums_admin_add_forum').'</a></div></td><td>&nbsp;</td></tr>';
		} elseif($last == 'lastchildboard' && $type) {
			$return = '<script type="text/JavaScript">$(\'cb_'.$type.'\').className = \'lastchildboard\';</script>';
		} elseif($last == 'last') {
			$return = '</tbody>';
		}
	}
	
	echo  $return = isset($return) ? $return : '';
	
	return $forum['fid'];
}

function showforum_moderators($forum) {
	global $_G;
	if($forum['moderators']) {
		$moderators = explode("\t", $forum['moderators']);
		$count = count($moderators);
		$max = $count > 2 ? 2 : $count;
		$mods = array();
		for($i = 0;$i < $max;$i++) {
			$mods[] = $forum['inheritedmod'] ? '<b>'.$moderators[$i].'</b>' : $moderators[$i];
		}
		$r = implode(', ', $mods);
		if($count > 2) {
			$r = '<span onmouseover="showMenu({\'ctrlid\':this.id})" id="mods_'.$forum['fid'].'">'.$r.'</span>';
			$mods = array();
			foreach($moderators as $moderator) {
				$mods[] = $forum['inheritedmod'] ? '<b>'.$moderator.'</b>' : $moderator;
			}
			$r = '<a href="'.ADMINSCRIPT.'?action=forums&operation=moderators&fid='.$forum['fid'].'" title="'.cplang('forums_moderators_comment').'">'.$r.' &raquo;</a>';
			$r .= '<div class="dropmenu1" id="mods_'.$forum['fid'].'_menu" style="display: none">'.implode('<br />', $mods).'</div>';
		} else {
			$r = '<a href="'.ADMINSCRIPT.'?action=forums&operation=moderators&fid='.$forum['fid'].'" title="'.cplang('forums_moderators_comment').'">'.$r.' &raquo;</a>';
		}


	} else {
		$r = '<a href="'.ADMINSCRIPT.'?action=forums&operation=moderators&fid='.$forum['fid'].'" title="'.cplang('forums_moderators_comment').'">'.cplang('forums_admin_no_moderator').'</a>';
	}
	return $r;
}

function copy_threadclasses($threadtypes, $fid) {
	global $_G;
	if($threadtypes) {
		$threadtypes = dunserialize($threadtypes);
		$i = 0;
		$data = array();
		foreach($threadtypes['types'] as $key => $val) {
			$data = array('fid' => $fid, 'name' => $val, 'displayorder' => $i++, 'icon' => $threadtypes['icons'][$key], 'moderators' => $threadtypes['moderators'][$key]);
			$newtypeid = C::t('forum_threadclass')->insert($data, true);
			$newtypes[$newtypeid] = $val;
			$newicons[$newtypeid] = $threadtypes['icons'][$key];
			$newmoderators[$newtypeid] = $threadtypes['moderators'][$key];
		}
		$threadtypes['types'] = $newtypes;
		$threadtypes['icons'] = $newicons;
		$threadtypes['moderators'] = $newmoderators;
		return serialize($threadtypes);
	}
	return '';
}

// 生成设置奖励项
function get_reward_tr($reward = array()){
	$default_reward = array(
		'name'=>'',
		'credittype'=>0,
		'credit'=>0,
		'totalnum'=>0
	);
	foreach($default_reward as $k=>$v){
		if(!isset($reward[$k])){
			$reward[$k] = $v;
		}
	}
	
	global $_G;
	$credittypeselect = '';
	foreach($_G['setting']['extcredits'] as $k=>$extcredit){
		if($reward['credittype'] == $k){
			$credittypeselect .= '<option value="'.$k.'" selected>'.$extcredit['title'].'</option>';
		}else{
			$credittypeselect .= '<option value="'.$k.'">'.$extcredit['title'].'</option>';
		}
	}
	
	return array(
		'<a href="###" class="addtr" onclick="addRewardTr();">新增一个奖项</a>&nbsp;&nbsp;&nbsp;'.cplang('forums_activity_set_rewardname').'：<input class="txt" type="text" name="newrewardname[]" value="'.$reward['name'].'">',
		cplang('forums_activity_set_rewardcredittype').'：<select name="newrewardcredittype[]">'.$credittypeselect.'</select>&nbsp;&nbsp;'.
		cplang('forums_activity_set_rewardcredit').'：<input class="txt" type="text" name="newrewardcredit[]" value="'.$reward['credit'].'" style="width:40px;">&nbsp;&nbsp;'.
		cplang('forums_activity_set_rewardtotalnum').'：<input class="txt" type="text" name="newrewardtotalnum[]" value="'.$reward['totalnum'].'" style="width:40px;">'.
		'<a href="###" onclick="removeRewardTr(this);">'.cplang('delete').'</a>'
	);
}

// 生成颁奖项
function get_reward_input($params = array()){
	$default_arr = array(
		'carrid'=>0,
		'key'=>0,
		'nickname'=>'',
		'uid'=>''
	);
	$input_arr = array();
	foreach($default_arr as $k=>$v){
		if(!isset($params[$k])){
			$input_arr[$k] = $v;
		}else{
			$input_arr[$k] = $params[$k];
		}
	}
	if($input_arr['uid'] > 0){
		$str = '<tr class="noborder">
					<td class="vtop rowform"><a href="home.php?mod=space&uid='.$input_arr['uid'].'" target="_blank">'.$input_arr['nickname'].'(uid:'.$input_arr['uid'].')</a></td>
					<td class="vtop">&nbsp;</td>
				</tr>';
	}else{
		$str = '<tr class="noborder">
					<td class="vtop rowform"><input class="txt rewardusername" type="text" name="nickname_'.$input_arr['carrid'].'_'.$input_arr['key'].'" id="nickname_'.$input_arr['carrid'].'_'.$input_arr['key'].'" value="'.$input_arr['nickname'].'"></td>
					<td class="vtop"><input class="uid" type="hidden" name="uid_'.$input_arr['carrid'].'_'.$input_arr['key'].'" value="'.$input_arr['uid'].'"></td>
				</tr>';
	}

	return $str;
}
