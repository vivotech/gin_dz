<?php

/**
 * 精简模板引擎
 */

class simple_template
{
    /*
     * The name of the directory where templates are located.
     * @var string
     */
    var $templatedir = 'source/admincp/template/';
     
    /*`
     * The directory where compiled templates are located.
     * @var string
     */
    var $compiledir = 'data/template/admincp/';
    
	/**
	 * 模板后序名
	 */
	var $template_ext = 'htm';
	
	var $template_name_pre = '';
	
	var $template_name = '';
	
	var $tpl = '';
	
	var $dir = '';
	
    /*
     * where assigned template vars are kept
     * @var array
     */
    var $vars = array();
    
	//当直接使用$this->设置assign时
	function __set($key, $var){
        return $this->assign($key, $var);  
    }  
	
    /*
     * compile a resource
     * sets PHP tag to the compiled source
     * @param string $tpl (template file)
     */
    function parse($tpl,$cpl)
    {
        // load template file //
        $fp   = @fopen($tpl, 'r');
        $text = fread($fp, filesize($tpl));
		//防止直读模板
		$text = "<?php if(!defined('IN_DISCUZ') || !defined('IN_ADMINCP')) {exit('Access Denied');} ?>\n".$text;
        fclose($fp);
        // repalce template tag to PHP tag //
        $text        = str_replace('{/if}', '<?php } ?>', $text);
        $text        = str_replace('{/loop}', '<?php } ?>', $text);
        $text        = str_replace('{foreachelse}', '<?php } else {?>', $text);
        $text        = str_replace('{/foreach}', '<?php } ?>', $text);
        $text        = str_replace('{else}', '<?php } else {?>', $text);
        $text        = str_replace('{loopelse}', '<?php } else {?>', $text);
		$text        = str_replace('{eval}', '<?php ', $text);
		$text        = str_replace('{/eval}', '?> ', $text);
		
        // template pattern tags //
        $pattern     = array(
            '/\{\$(.*?)\}/',
            '/\{\:(.*?)\}/',
            "/\{include\s+file=[\"\']{1}(\S*)[\"\']{1}\s+dir=[\"\']{1}(\S*)[\"\']{1}\}/i",
            '/\{include file=(\"|\')(\w*[a-zA-Z0-9_\.][a-zA-Z]\w*)(\"|\')\}/',
            '/\{if (.*?)\}/',
            '/\{elseif (.*?)\}/',
            '/\{loop\s+\$(\S*)\s+\$?(\w*[a-zA-Z0-9_])\s*\}/i',
            '/\{foreach\s+\$(\S*)\s+\$?(\w*[a-zA-Z0-9_])\s*\}/i',
            '/\{loop\s+\$(.*)\s+\$?(\w*[a-zA-Z0-9_])\s+\$?(\w*[a-zA-Z0-9_])\s*\}/i',
            '/\{foreach\s+\$(.*)\s+\$?(\w*[a-zA-Z0-9_])\s+\$?(\w*[a-zA-Z0-9_])\s*\}/i'
        );
        
        $replacement = array(
            '<?php echo \$\1?>',
            '<?php echo \1?>',
            '<?php $this->display(\'\1\',\'\',\'\2\')?>',
            '<?php $this->display(\'\2\')?>',
            '<?php if(\1) {?>',
            '<?php } elseif(\1) {?>',
            '<?php if (count((array)\$\1)) foreach((array)\$\1 as \$\2) {?>',
            '<?php if (count((array)\$\1)) foreach((array)\$\1 as \$\2) {?>',
            '<?php if (count((array)\$\1)) foreach((array)\$\1 as \$\2 => \$\3) {?>',
            '<?php if (count((array)\$\1)) foreach((array)\$\1 as \$\2 => \$\3) {?>'
        );
        // repalce template tags to PHP tags //
        $text = preg_replace($pattern, $replacement, $text);
       	
        if ($fp = @fopen($cpl, 'w')) {
            fputs($fp, $text);
            fclose($fp);
        }
    }
     
    /*
     * assigns values to template variables
     * @param array|string $k the template variable name(s)
     * @param mixed $v the value to assign
     */
    function assign($k, $v = null)
    {
        $this->vars[$k] = $v;
    }
    
	/**
	 * 设置模板文件前序
	 */
	function setTemplateNamePre($pre){
		$this->template_name_pre = $pre;
	}
	
	/**
	 * 设置模板默认名称
	 */
	function setTemplateName($name){
		$this->template_name = strtolower($name);
	}
	
	/**
	 * 设置模板文件扩展名
	 */
	function setTemplateExt($ext){
		$this->template_ext = strtolower($ext);
	}
	
    /*
     * ste directory where templates are located
     * @param string $str (path)
     */
    function templateDir($path)
    {
        $this->templatedir = $this->pathCheck($path);
    }
     
    /*
     * set where compiled templates are located
     * @param string $str (path)
     */
    function compileDir($path)
    {
        $this->compiledir = $this->pathCheck($path);
    }
     
    /*
     * check the path last character
     * @param string $str (path)
     * @return string
     */
    function pathCheck($str)
    {
        return (preg_match('/\/$/', $str)) ? $str : $str . '/';
    }
     
    /**
     * @param string $tpl (template file)
     */
    function display($tpl = '',$dir = '', $templatedir = '')
    {
        echo $this->fetch($tpl,$dir, $templatedir);
    }
	
	/* 返回模板 */
	function fetch($tpl = '',$dir = '', $templatedir = '')
    {
    	global $_G;
    	$tpl = $tpl=='' ? $this->template_name_pre.$this->template_name : strtolower($tpl);
		$tpldir = DISCUZ_ROOT. ($templatedir==''?$this->templatedir : $templatedir).($dir ? strtolower($dir). DS :'');
		dmkdir($tpldir);
		$tplfile = $tpldir . $tpl . '.' .$this->template_ext;
        if (!file_exists($tplfile)) {
            exit('can not load template file : ' . $tplfile);
        }
		$compliedir = DISCUZ_ROOT.$this->compiledir;
		dmkdir($compliedir);
        $compliefile =  $compliedir .($dir ? strtolower($dir). '_' :''). $tpl . '.php';
        if (!file_exists($compliefile) || filemtime($tplfile) > filemtime($compliefile)) {
            $this->parse($tplfile,$compliefile);
        }
		//解析函数
		extract($this->vars);
		//开启缓冲，编译
		ob_start();
		include_once($compliefile);
		$result = ob_get_contents();
		ob_end_clean();
        return $result;
    }
	
	/* 特殊功能 */
	
	function shownav($header = '', $menu = '', $nav = ''){
		global $action, $operation;
		$title = 'cplog_'.$action.($operation ? '_'.$operation : '');
		if(in_array($action, array('home', 'custommenu'))) {
			$customtitle = '';
		} elseif(cplang($title, false)) {
			$customtitle = $title;
		} elseif(cplang('nav_'.($header ? $header : 'index'), false)) {
			$customtitle = 'nav_'.$header;
		} else {
			$customtitle = rawurlencode($nav ? $nav : ($menu ? $menu : ''));
		}
		$title = cplang('header_'.($header ? $header : 'index')).($menu ? '&nbsp;&raquo;&nbsp;'.cplang($menu) : '').($nav ? '&nbsp;&raquo;&nbsp;'.cplang($nav) : '');
		$ctitle = cplang('header_'.($header ? $header : 'index'));
		if($menu) {
			$ctitle = cplang($menu);
		}
		if($nav) {
			$ctitle = cplang($nav);
		}
		$addtomenu = "&nbsp;&nbsp;<a target=\"main\" title=\"".cplang('custommenu_addto')."\" href=\"".ADMINSCRIPT."?action=misc&operation=custommenu&do=add&title=".rawurlencode($ctitle)."&url=".rawurlencode(cpurl())."\">[+]</a>";
		$dtitle = str_replace("'", "\'", cplang('admincp_title').' - '.str_replace('&nbsp;&raquo;&nbsp;', ' - ', $title));
		$this->assign('shownav','<script type="text/JavaScript">parent.document.title = \''.$dtitle.'\';if(parent.$(\'admincpnav\')) parent.$(\'admincpnav\').innerHTML=\''.$title.$addtomenu.'\';</script>');
	}

	function showsubmenu($title, $menus = array(), $right = '', $replace = array()){
		if(empty($menus)) {
			$s = '<div class="itemtitle">'.$right.'<h3>'.cplang($title, $replace).'</h3></div>';
		} elseif(is_array($menus)) {
			$s = '<div class="itemtitle">'.$right.'<h3>'.cplang($title, $replace).'</h3><ul class="tab1">';
			foreach($menus as $k => $menu) {
				if(is_array($menu[0])) {
					$s .= '<li id="addjs'.$k.'" class="showdropmenu '.($menu[1] ? 'current' : 'hasdropmenu').'" ><a href="#"><span>'.cplang($menu[0]['menu']).'<em>&nbsp;&nbsp;</em></span></a><div id="addjs'.$k.'child" class="dropmenu" style="display:none;">';
					if(is_array($menu[0]['submenu'])) {
						foreach($menu[0]['submenu'] as $submenu) {
							$s .= $submenu[1] ? '<a href="'.ADMINSCRIPT.'?action='.$submenu[1].'" class="'.($submenu[2] ? 'current' : '').'" onclick="'.$submenu[3].'">'.cplang($submenu[0]).'</a>' : '<a><b>'.cplang($submenu[0]).'</b></a>';
						}
					}
					$s .= '</div></li>';
				} else {
					$s .= '<li'.($menu[2] ? ' class="current"' : '').'><a href="'.(!$menu[4] ? ADMINSCRIPT.'?action='.$menu[1] : $menu[1]).'"'.(!empty($menu[3]) ? ' target="_blank"' : '').'><span>'.cplang($menu[0]).'</span></a></li>';
				}
			}
			$s .= '</ul></div>';
		}

		$ext_jquery = <<<EOS
			//二级菜单
			$('.showdropmenu').hover(function(){
				$(this).find('div.dropmenu').stop().show(100);
			},function(){
				$(this).find('div.dropmenu').stop().hide(100);
			});
EOS;
		$this->addJquery($ext_jquery);
		//二级菜单JS
		$this->assign('showsubmenu',!empty($menus) ? '<div class="floattop">'.$s.'</div><div class="floattopempty"></div>' : $s);
	}
	
	//添加Jquery到页面底部
	function addJquery($jquery){
		$this->vars['jquery'] .= $jquery;
	}
	
	public static function addBootstrap2(){
		echo '<link rel="stylesheet" type="text/css" href="/static/bootstrap2/css/bootstrap.min.css"/>';
		echo '<script type="text/javascript" src="/static/bootstrap2/js/bootstrap.min.js"></script>';
	}
	
	public static function addjQueryUI(){
		echo '<script type="text/javascript" src="/static/jquery/jquery-ui-1.10.3.min.js"></script>';
	}
	
}
 
?>