<?php

/**
 * AJAX操作
 */

if(!defined('IN_DISCUZ') || !defined('IN_ADMINCP')) {
	exit('Access Denied');
}

//转换forum_thread表中coverimg字段
if($operation == 'change_coverimg'){
	$tid = I('tid',0,'intval');
	$coverimg = I('coverimg');
	$data = array('coverimg'=>$coverimg);
	if(C::t('forum_thread')->update($tid,$data)){
		ajax_return(1);
	}
	ajax_return();
}
//为商品列表添加规格
elseif($operation == 'spec'){
	$tpl->psid = $psid = I('psid',0);
	$tpl->spec_id = $spec_id = I('spec_id',0,'intval');
	$tpl->do = $do = $do ? $do : 'list';
	$tpl->types = get_product_spec_types();
	$model = M('common_product_spec');
	$series_model = M('common_product_series');
	//多级循环，下级包含上级规格，但不能编辑
	if($do == 'list'){
		$where=array();
		$series = $series_model->find($psid);
		if($series['type']=='group'){
			$where['psid'] = $psid;
		}elseif($series['type']=='series'){
			$where['psid'] = array('IN',array($psid,$series['psup']));
		}elseif($series['type']=='sub'){
			$up_series = $series_model->find($series['psup']);
			$where['psid'] = array('IN',array($psid,$series['psup'],$up_series['psup']));
		}
		$pagesize = I('pagesize',10,'intval');
		$listcount = $model->where($where)->count();
		$tpl->list = $list = $model->where($where)->limit((($_G['page']-1)*$pagesize).','.$pagesize)->order('displayorder DESC')->select();
		$tpl->multipage = multi($listcount, $pagesize, $_G['page'], adminurl('ajax/spec/list'));
	}
	elseif($do == 'add'){
		//提交
		if(IS_POST){
			$date = array(
				'psid'=>$psid,
				'name'=>I('name',''),
				'type'=>I('type','text','',array('text','select','date')),
				'extvalue' => htmlspecialchars(I('extvalue','')),
				'displayorder'=>I('displayorder',0,'intval'),
				'addtime'=>TIMESTAMP,
			);
			if($psid<1){
				cpmsg_error('参数错误！！');
			}
			if(empty($date['name'])){
				cpmsg_error('名称不能为空！');
			}
			$dbaction = 'add';
			if($spec_id){
				$date['id'] = $spec_id;
				$dbaction = 'save';
			}
			if($model->$dbaction($date)){
				dheader("location:".adminurl('ajax/spec/list','psid='.$psid));
			}
		}
		$tpl->spec = $spec = $model->find($spec_id);
	}
	elseif($do == 'delete'){
		if($model->delete($spec_id)){
			dheader("location:".adminurl('ajax/spec/list','psid='.$psid));
		}
	}
}
//加载商品规则商品
elseif($operation == 'productspec'){
	$psid = I('psid',0);
	if(!$psid){
		echo '请选选择分类';
		exit();
	}
	$default = $_POST['spec'];
	if(!empty($default)){
		$default = json_decode($default,TRUE);
	}
	$spec_model = M('common_product_spec');
	$series_model = M('common_product_series');
	$where=array();
	$series = $series_model->find($psid);
	if($series['type']=='group'){
		$where['psid'] = $psid;
	}elseif($series['type']=='series'){
		$where['psid'] = array('IN',array($psid,$series['psup']));
	}elseif($series['type']=='sub'){
		$up_series = $series_model->find($series['psup']);
		$where['psid'] = array('IN',array($psid,$series['psup'],$up_series['psup']));
	}
	$specs = $spec_model->where($where)->order('displayorder DESC')->select();
	if(!$specs){
		echo '<a href="'.adminurl('productseries/admin').'">添加规格</a>';
		exit();
	}
	
	$return = '<table>';
	foreach($specs as $sk=>$sv){
		$return .= "<tr><th>{$sv['name']}：</th><td>";
		if($sv['type']=='text'){
			$return .= "<input type='text' name='spec[{$sv[id]}]' value='{$default[$sv[id]]}'  />";
		}elseif($sv['type']=='select'){
			$return .= "<select name='spec[{$sv[id]}]'>";
			$ops = explode('<br />', nl2br($sv['extvalue']));
			foreach($ops as $ov){
				//除去空字符
				$ov = preg_replace("/\s*/i", '', $ov);
				$return .= '<option '.($default[$sv['id']]==$ov?'selected':'').' value="'.$ov.'">'.$ov.'</option>';
			}
			$return .= "</select>";
		}elseif($sv['type']=='date'){
			$return .= '<input name="spec['.$sv['id'].']" type="text" onclick="showcalendar(event, this)" value="'.$default[$sv['id']].'" >';
		}
		$return .= "</td></tr>";
	}
	$return .= '</table>';
	echo $return;
	exit();
}
//设置默认发帖板块
elseif($operation == 'setdefaultforum'){
	
	if(IS_POST){
		$configdata = array(
						'app' => I('appfid',0,'intval'),
						'shop'=> I('shopfid',0,'intval'),
					);
		$settings = array('defaultforum' => $configdata);
		C::t('common_setting')->update_batch($settings);
		updatecache('setting');
		echo 1;exit();
	}
	
	$query = C::t('forum_forum')->fetch_all_forum_for_sub_order();
	$groups = $forums = $subs = $fids = $showed = array();
	foreach($query as $forum) {
		if($forum['type'] == 'group') {
			$groups[$forum['fid']] = $forum;
		} elseif($forum['type'] == 'sub') {
			$subs[$forum['fup']][] = $forum;
		} else {
			$forums[$forum['fup']][] = $forum;
		}
		$fids[] = $forum['fid'];
	}
	$tpl->forums  = $forums;
	$tpl->subs = $subs;
	$tpl->defaultforum = $defaultforum =  unserialize($_G['setting']['defaultforum']);
}
//返回积分商品列表
elseif($operation == 'getcreditproduct'){
	$list = M('common_product_onsell')->where("type=1")->select();
	return_json($list);
	exit();
}
//商城配置
elseif($operation == 'shopsetting'){
	
	$tpl->do = $do = I('do','slider');
	$tpl->shownav('topic', '微信账号管理');
	$topmenu = array(
        array('商城轮播图', 'ajax&operation=shopsetting&do=slider' , $operation=='shopsetting'&&$do=='slider'),
		array('支付方式', 'ajax&operation=shopsetting&do=payment' , $operation=='shopsetting'&&$do=='payment'),
	);

	
	if($do=='payment'){
		$model = M('common_payment');
		if(IS_POST){
			foreach($_POST['pid'] as $key=>$val){
				$savedata = array('status'=>isset($_POST['status'][$key]) ? 1 : 0 ,'is_mobile'=>isset($_POST['is_mobile'][$key]) ? 1 : 0);
				$model->where("pid = '{$key}'")->save($savedata);
			}
			updatecache('payment');
			cpmsg('修改成功',adminurl('ajax/shopsetting/payment'),'success');
		}
		
		$tpl->list = $list = $model->select();
	}
	elseif($do=='paysetting'){
		require_once libfile('function/order');
		
		$paytype = $tpl->paytype = I('paytype');
		$payments = get_payment();
		$payment = array();
		foreach($payments as $k=>$v){
			if($v['paytype']==$paytype){
				$payment=$v;
			}
		}
		if(empty($payment)){
			cpmsg_error('错误参数');
		}
		array_push($topmenu,array('支付配置-'.$payment['payname'], 'ajax&operation=shopsetting&do=paysetting&paytype='.$paytype , TRUE));
		
		if(IS_POST){
			
			$saveData = array();
			foreach($_POST as $key=>$val){
				if(strstr($key,$paytype.'-')!==FALSE){
					$saveData[str_replace($paytype.'-', '', $key)] = $val;
				}
			}
			
			$targetFolder = 'data/cacert';
			$targetPath = DISCUZ_ROOT . $targetFolder;
			if (!empty($_FILES)) {
	//			print_r($_FILES);exit();
				$fileTypes = array('pem','pfx');
				foreach($_FILES as $fkey=>$fval){
					//上传成功
					if($_FILES[$fkey]['error']==0){
						$tempFile = $_FILES[$fkey]['tmp_name'];
						$targetFile = rtrim($targetPath,'/') . '/' . $_FILES[$fkey]['name'];
						$fileParts = pathinfo($_FILES[$fkey]['name']);
						if (in_array($fileParts['extension'],$fileTypes)) {
							move_uploaded_file($tempFile,$targetFile);
						}else{
							cpmsg_error('上传文件格式错误，只能为'.implode('或', $fileTypes),adminurl('ajax/shopsetting/paysetting',array('paytype'=>$paytype)));
						}
						$saveData[str_replace($paytype.'-', '', $fkey)] = $targetFolder.'/' . $_FILES[$fkey]['name'];
					}
				}
			}
			
			M('common_payment')->where("pid='{$payment[pid]}'")->save(array('payconfig'=>serialize($saveData)));
			updatecache('payment');
			cpmsg('修改成功',adminurl('ajax/shopsetting/paysetting',array('paytype'=>$paytype)),'success');
		}
		
		$tpl->config = $config = unserialize($payment['payconfig']);
	}
    //轮播图
    elseif($do=='slider'){
        $tpl->list = $list = unserialize($_G['setting']['shopslider']);
    }

	$tpl->showsubmenu('商城配置管理', $topmenu);
}
//设置轮播图
elseif($operation == 'setshopslider'){

    $list = unserialize($_G['setting']['shopslider']);
    $list = $list ? $list : array();
    $tpl->id = $id = I('id',0,'intval');

    if(IS_POST){

        $shopslider = array(
            'spid'  => I('spid',0,'intval'),
            'url'   => I('url'),
            'displayorder'  => I('displayorder')
        );
        //新增操作
        if($id<=0){
            $new_id = count($list)+1;
            while(in_array($new_id,array_keys($list))){
                $new_id++;
            }
            $list[$new_id] = $shopslider;
        }else{
            //更新
            foreach($list as $key=>$val) {
                if($key==$id){
                    $list[$key] = $shopslider;
                }
            }
        }
        $settings = array('shopslider' => $list);
        C::t('common_setting')->update_batch($settings);
        updatecache('setting');
        ajax_return(1, $id ? '修改成功' : '添加成功');
        exit();
    }

    $tpl->product_list = $product_list = C::t('common_product_onsell')->fetch_all_page(1,100);
    $tpl->banner = $banner =  isset($list[$id]) ? $list[$id] : array();

}
//清空商品轮播图
elseif($operation == 'clearshopslider') {
    $id = I('id',0,'intval');
    if($id<=0){
        $list = '';
    }else{
        $list = unserialize($_G['setting']['shopslider']);
        foreach($list as $key=>$val){
            if($key==$id){
                unset($list[$id]);
            }
        }
    }
    $settings = array('shopslider' => $list);
    C::t('common_setting')->update_batch($settings);
    updatecache('setting');
    dheader('location:'.adminurl('ajax/shopsetting/slider'));
    exit();
}

$tpl->display($operation,$action);

?>