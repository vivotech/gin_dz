<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

/**
 * @name 检查用户是否完成指定的新手教学
 * @param Int  $uid
 * @param String  $page
 * @return Bool
 */
function checkBeginnerGuideIsFinished($uid, $page)
{
	$record = C::t('member_finish_beginner_guide')->fetch_by_uid($uid);
	
	if (!$record)
	{
		return false;
	}
	
	if (isset($record[$page]))
	{
		return (bool)$record[$page];
	}
	
	return false;
}

/**
 * @name 完成指定的新手教程
 * @param Int  $uid
 * @param String  $page
 * @return Bool
 */
function finishBeginnerGuide($uid, $page)
{
	if ( !table_member_finish_beginner_guide::guide_page_is_available($page) )
	{
		return false;
	}
	
	$record = C::t('member_finish_beginner_guide')->fetch_by_uid($uid);
	
	if ($record) {			// 更新记录
		$result = C::t('member_finish_beginner_guide')->update($record['mfbgid'], array($page => 1));
	} else {				// 插入新记录
		$result = C::t('member_finish_beginner_guide')->insert(array(
			'uid'	=> $uid,
			$page	=> 1
		));
	}
	
	return $result;
}


