<?php

/*
 * APP方法
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

/**
 * 判断手机是IOS还是Android
 */
function get_device_type(){
	 $agent = strtolower($_SERVER['HTTP_USER_AGENT']);
	 $type = 'other';
	 if(strpos($agent, 'iphone') || strpos($agent, 'ipad')){
	  $type = 'ios';
	 }
	 if(strpos($agent, 'android')){
	  $type = 'android';
	 }
	 return $type;
}

/**
 * app获取返回积分
 * @param array $credict_obj updatecreditbyaction()函数返回的结果
 */

function app_get_return_credict($credict_obj , $return_all = false){
	global $_G;
	//获取积分设置
	$extcredits = $_G['setting']['extcredits'];
	//数据为空
	if(empty($credict_obj)){
		$res = array('result'=>false,'rulename'=>'','credicts' => array());
		return $res;
	}
	$credict_obj = $return_all ? $credict_obj : array($credict_obj);
	$res = array();
	$i = 0;
	foreach($credict_obj as $k=>$v){
		$res[$i] = array('result'=>$v['updatecredit'],'rulename'=>$v['rulename'],'credicts' => array());
		foreach($extcredits as $key=>$val){
			$res[$i]['credicts']['extcredits'.$key] = $v['extcredits'.$key];
		}
		$i++;
	}
	return count($res)==1?$res[0]:$res;
}


/**
 * 返回数据
 * 
 * @param int $res 结果
 * @param string $msg 结果，自动读取message语言包
 * @param mix $data 数组等
 * @param string $returntype 返回数据类型 ，默认JSON
 * @param string $l 字体包文件名，默认为：message
 * 
 */

function sendAppMessage($res,$msg,$data,$returntype = 'JSON',$l='message'){
	global $_G;
	//如果是数组就解析
	$res_msg = '';
	if(is_array($msg)){
		if($msg[0]===FALSE){
			$res_msg = $msg[1];
		}else{
			$res_msg = lang($l,$msg[0]);
			foreach($msg[1] as $k=>$v){
				$res_msg = str_replace('{'.$k.'}', $v, $res_msg);
			}
		}
	}else{
		$res_msg = lang($l,$msg);
	}
	
	$res = array(
		'result'	=> $res,
		'msg'		=> $res_msg,
		'data'		=> $data
	);
	
	//判断是否登陆，如果登陆，则全局返回_sign作登陆处理
//	if($_G['uid']){
//		$res['_sign'] = rawurlencode($_G['cookie']['auth']);
//		$res['_salt'] = rawurlencode($_G['cookie']['saltkey']);
//	}
	
	return appReturn($res,$returntype);
}

/**
 * app返回数据类型函数
 * 
 */
function appReturn($arr , $type = 'JSON'){
	$arr = is_array($arr) ? $arr : array($arr);
	switch(strtoupper($type)){
		case 'JSON':
			echo json_encode($arr,JSON_UNESCAPED_UNICODE);
			exit();
			break;
		case 'XML':
			echo xml_encode($arr);
			exit();
			break;
		case 'RETURN':
			return $arr;
			break;
		default:
			return FALSE;
			exit();
			break;
	}
	return FALSE;
}

/**
 * 解析列表页附件（单个）
 * @return array 返回原输入数组
 */

function app_parselistattach($attachpids, $attachtags, $postlist, $skipaids = array(),$tid = 0) {
	global $_G;
	if(!$attachpids) {
		return;
	}
	$tid = $tid ? $tid : $_G['tid'] ;
	$attachpids = is_array($attachpids) ? $attachpids : array($attachpids);
	$attachexists = FALSE;
	$skipattachcode = $aids = $payaids = $findattach = array();
	foreach(C::t('forum_attachment_n')->fetch_all_by_id('tid:'.$tid, 'pid', $attachpids) as $attach) {
		$attachexists = TRUE;
		if($skipaids && in_array($attach['aid'], $skipaids)) {
			$skipattachcode[$attach[pid]][] = "/\[attach\]$attach[aid]\[\/attach\]/i";
			continue;
		}
		$attached = 0;
		$extension = strtolower(fileext($attach['filename']));
		$attach['ext'] = $extension;
		$attach['imgalt'] = $attach['isimage'] ? strip_tags(str_replace('"', '\"', $attach['description'] ? $attach['description'] : $attach['filename'])) : '';
		$attach['attachicon'] = attachtype($extension."\t".$attach['filetype']);
		$attach['attachsize'] = sizecount($attach['filesize']);
		if($attach['isimage'] && !$_G['setting']['attachimgpost']) {
			$attach['isimage'] = 0;
		}
		$attach['attachimg'] = $attach['isimage'] && (!$attach['readperm'] || $_G['group']['readaccess'] >= $attach['readperm']) ? 1 : 0;
		if($attach['attachimg']) {
			$GLOBALS['aimgs'][$attach['pid']][] = $attach['aid'];
		}
		
		if($attach['price']) {
			if($_G['setting']['maxchargespan'] && TIMESTAMP - $attach['dateline'] >= $_G['setting']['maxchargespan'] * 3600) {
				C::t('forum_attachment_n')->update('tid:'.$_G['tid'], $attach['aid'], array('price' => 0));
				$attach['price'] = 0;
			} elseif(!$_G['forum_attachmentdown'] && $_G['uid'] != $attach['uid']) {
				$payaids[$attach['aid']] = $attach['pid'];
			}
		}
		$attach['payed'] = $_G['forum_attachmentdown'] || $_G['uid'] == $attach['uid'] ? 1 : 0;
		$attach['url'] = ($attach['remote'] ? $_G['setting']['ftp']['attachurl'] : $_G['setting']['attachurl']).'forum/';
		$attach['dbdateline'] = $attach['dateline'];
		$attach['dateline'] = dgmdate($attach['dateline'], 'u');
		$postlist['attachments'][$attach['aid']] = $attach;
		if(!defined('IN_MOBILE_API') && !empty($attachtags[$attach['pid']]) && is_array($attachtags[$attach['pid']]) && in_array($attach['aid'], $attachtags[$attach['pid']])) {
			$findattach[$attach['pid']][$attach['aid']] = "/\[attach\]$attach[aid]\[\/attach\]/i";
			$attached = 1;
		}
		if(!$attached) {
			if($attach['isimage']) {
				$postlist['imagelist'][] = $attach['aid'];
				$postlist['imagelistcount']++;
				if($postlist['first']) {
					$GLOBALS['firstimgs'][] = $attach['aid'];
				}
			} else {
				if(!$_G['forum_skipaidlist'] || !in_array($attach['aid'], $_G['forum_skipaidlist'])) {
					$postlist['attachlist'][] = $attach['aid'];
				}
			}
		}
		$aids[] = $attach['aid'];
	}
	if($aids) {
		$attachs = C::t('forum_attachment')->fetch_all($aids);
		foreach($attachs as $aid => $attach) {
			if($postlist) {
				$postlist['attachments'][$attach['aid']]['downloads'] = $attach['downloads'];
			}
		}
	}
	if($payaids) {
		foreach(C::t('common_credit_log')->fetch_all_by_uid_operation_relatedid($_G['uid'], 'BAC', array_keys($payaids)) as $creditlog) {
			$postlist[$payaids[$creditlog['relatedid']]]['attachments'][$creditlog['relatedid']]['payed'] = 1;
		}
	}
	if(!empty($skipattachcode)) {
		foreach($skipattachcode as $pid => $findskipattach) {
			foreach($findskipattach as $findskip) {
				$postlist['message'] = preg_replace($findskip, '', $postlist['message']);
			}
		}
	}
	if($attachexists) {
		foreach($attachtags as $pid => $aids) {
			if($findattach[$pid]) {
				$attach_index = 1;
				foreach($findattach[$pid] as $aid => $find) {
					//
					$postlist['images'] .= app_attachinlist($postlist['attachments'][$aid], $postlist,$attach_index-1);
					if($attach_index!=count($findattach[$pid])){
						$postlist['images'] .= ',';
					}
					$postlist['message'] = preg_replace($find, '', $postlist['message']);
					$attach_index++;
				}
			}
		}
		
	} else {
		loadcache('posttableids');
		$posttableids = $_G['cache']['posttableids'] ? $_G['cache']['posttableids'] : array('0');
		foreach($posttableids as $id) {
			C::t('forum_post')->update($id, $attachpids, array('attachment' => '0'), true);
		}
	}
	
	return $postlist;
//	print_r($postlist);
}

//APP帖子列表中展示图片
function app_attachinlist($attach, $post,$img_id=0) {
	global $_G;
	$firstpost = $post['first'];
	$attach['refcheck'] = (!$attach['remote'] && $_G['setting']['attachrefcheck']) || ($attach['remote'] && ($_G['setting']['ftp']['hideurl'] || ($attach['isimage'] && $_G['setting']['attachimgpost'] && strtolower(substr($_G['setting']['ftp']['attachurl'], 0, 3)) == 'ftp')));
	$aidencode = packaids($attach);
	$widthcode = attachwidth($attach['width']);
	$is_archive = $_G['forum_thread']['is_archived'] ? '&fid='.$_G['fid'].'&archiveid='.$_G[forum_thread][archiveid] : '';
	$attachthumb = getimgthumbname($attach['attachment']);
	$musiccode = getstatus($post[status], 7) && fileext($attach['attachment']) == 'mp3' ? (browserversion('ie') > 8 || browserversion('safari') ? '<audio controls="controls"><source src="'.$attach['url'].$attach['attachment'].'"></audio>' : parseaudio($attach['url'].$attach['attachment'], 400)) : '';
	
	$return = 'http://'.$_SERVER['HTTP_HOST'].'/'.($attach['refcheck']?'forum.php?mod=attachment'.$is_archive.'&aid=$aidencode&noupdate=yes':($attach['url'].$attach['attachment']));
	return $return;
}

function app_checkemail($email) {
	global $_G;
	
	$email = strtolower(trim($email));
	if(strlen($email) > 32) {
		//Email 地址无效
		sendAppMessage(RES_NO,'profile_email_illegal', array('error_email'=>$email));
	}
	if($_G['setting']['regmaildomain']) {
		$maildomainexp = '/('.str_replace("\r\n", '|', preg_quote(trim($_G['setting']['maildomainlist']), '/')).')$/i';
		if($_G['setting']['regmaildomain'] == 1 && !preg_match($maildomainexp, $email)) {
			//抱歉，Email 包含不可使用的邮箱域名
			sendAppMessage(RES_NO,'profile_email_domain_illegal', array('error_email'=>$email));
		} elseif($_G['setting']['regmaildomain'] == 2 && preg_match($maildomainexp, $email)) {
			//抱歉，Email 包含不可使用的邮箱域名
			sendAppMessage(RES_NO,'profile_email_domain_illegal', array('error_email'=>$email));
		}
	}

	loaducenter();
	$ucresult = uc_user_checkemail($email);

	if($ucresult == -4) {
		//Email 地址无效
		sendAppMessage(RES_NO,'profile_email_illegal', array('error_email'=>$email));
	} elseif($ucresult == -5) {
		//抱歉，Email 包含不可使用的邮箱域名
		sendAppMessage(RES_NO,'profile_email_domain_illegal', array('error_email'=>$email));
	} elseif($ucresult == -6) {
		//该 Email 地址已被注册
		sendAppMessage(RES_NO,'profile_email_duplicate', array('error_email'=>$email));
	}
}

/* app_member uploadavatar 头像处理函数 */

function get_home($uid) {
	$uid = sprintf("%09d", $uid);
	$dir1 = substr($uid, 0, 3);
	$dir2 = substr($uid, 3, 2);
	$dir3 = substr($uid, 5, 2);
	return $dir1.'/'.$dir2.'/'.$dir3;
}

function set_home($uid, $dir = '.') {
	$uid = sprintf("%09d", $uid);
	$dir1 = substr($uid, 0, 3);
	$dir2 = substr($uid, 3, 2);
	$dir3 = substr($uid, 5, 2);
	!is_dir($dir.'/'.$dir1) && mkdir($dir.'/'.$dir1, 0777);
	!is_dir($dir.'/'.$dir1.'/'.$dir2) && mkdir($dir.'/'.$dir1.'/'.$dir2, 0777);
	!is_dir($dir.'/'.$dir1.'/'.$dir2.'/'.$dir3) && mkdir($dir.'/'.$dir1.'/'.$dir2.'/'.$dir3, 0777);
}

function get_avatar($uid, $size = 'big', $type = '', $from = 'qiniu') {
	$size = in_array($size, array('big', 'middle', 'small')) ? $size : 'big';
	$uid = abs(intval($uid));
	$uid = sprintf("%09d", $uid);
	$dir1 = substr($uid, 0, 3);
	$dir2 = substr($uid, 3, 2);
	$dir3 = substr($uid, 5, 2);
	$typeadd = $type == 'real' ? '_real' : '';
	if($from == 'qiniu'){
		return  $dir1.'/'.$dir2.'/'.$dir3.'/'.substr($uid, -2).$typeadd."_avatar.jpg";
	}else{
		return  $dir1.'/'.$dir2.'/'.$dir3.'/'.substr($uid, -2).$typeadd."_avatar_$size.jpg";
	}
}

function flashdata_decode($s) {
	$r = '';
	$l = strlen($s);
	for($i=0; $i<$l; $i=$i+2) {
		$k1 = ord($s[$i]) - 48;
		$k1 -= $k1 > 9 ? 7 : 0;
		$k2 = ord($s[$i+1]) - 48;
		$k2 -= $k2 > 9 ? 7 : 0;
		$r .= chr($k1 << 4 | $k2);
	}
	return $r;
}

function moveAvatar($uid,$avatar_temp_url,$image_type='jpg'){
	if(!defined('UC_DATADIR')){
		define('UC_DATADIR', DISCUZ_ROOT . 'uc_server/data/');
	}
	if(!defined('UC_DATAURL')){
		define('UC_DATAURL', DISCUZ_ROOT . 'uc_server/data');
	}
	$home = get_home($uid);
	if(!is_dir(UC_DATADIR.'./avatar/'.$home)) {
		set_home($uid, UC_DATADIR.'./avatar/');
	}
	
	if($image_type == 'jpg'){
		$image_src = imagecreatefromjpeg($avatar_temp_url);
	}elseif($image_type == 'png'){
		$image_src = imagecreatefrompng($avatar_temp_url);
	}else{
		$image_src = imagecreatefromgif($avatar_temp_url);
	}
	
	$width = imagesx($image_src);
	$height = imagesy($image_src);
	
	//大头像
	$bigavatar_width = 200;
	$bigavatar_height= 200;
	$bigavatar = imagecreatetruecolor($bigavatar_width,$bigavatar_height);
	
	//中头像
	$middleavatar_width = 120;
	$middleavatar_height= 120;
	$middleavatar = imagecreatetruecolor($middleavatar_width,$middleavatar_height);
	//小头像
	$smallavatar_width = 48;
	$smallavatar_height= 48;
	$smallavatar = imagecreatetruecolor($smallavatar_width,$smallavatar_height);
	
	 //关键函数，参数（目标资源，源，目标资源的开始坐标x,y, 源资源的开始坐标x,y,目标资源的宽高w,h,源资源的宽高w,h）
	imagecopyresampled($bigavatar, $image_src, 0, 0, 0, 0, $bigavatar_width, $bigavatar_height, $width, $height);
    imagecopyresampled($middleavatar, $image_src, 0, 0, 0, 0, $middleavatar_width, $middleavatar_height, $width, $height);
	imagecopyresampled($smallavatar, $image_src, 0, 0, 0, 0, $smallavatar_width, $smallavatar_height, $width, $height);
	
	$bigavatarfile = UC_DATADIR.'./avatar/'.get_avatar($uid, 'big');
	$middleavatarfile = UC_DATADIR.'./avatar/'.get_avatar($uid, 'middle');
	$smallavatarfile = UC_DATADIR.'./avatar/'.get_avatar($uid, 'small');
	
	if(file_exists($bigavatarfile)){
		@unlink($bigavatarfile);
	}
	if(file_exists($middleavatarfile)){
		@unlink($middleavatarfile);
	}
	if(file_exists($smallavatarfile)){
		@unlink($smallavatarfile);
	}
	
	//保存头像
	switch($image_type){
		case 'jpg' : imagejpeg($bigavatar,$bigavatarfile);imagejpeg($middleavatar,$middleavatarfile);imagejpeg($smallavatar,$smallavatarfile);break;
		case 'png' : imagepng($bigavatar,$bigavatarfile);imagepng($middleavatar,$middleavatarfile);imagepng($smallavatar,$smallavatarfile);break;
		case 'gif' : imagegif($bigavatar,$bigavatarfile);imagegif($middleavatar,$middleavatarfile);imagegif($smallavatar,$smallavatarfile);break;
	}
	
	@unlink(UC_DATADIR.'./tmp/upload'.$uid.'.'.$image_type);
	
	//上传到头像到7牛
	$qiniuavatarfile = 'avatar/'.get_avatar($uid, 'big');
		
	$CURL = new curl();
	// 获取上传token
	$token_url = 'http://'.$_SERVER['HTTP_HOST'].'/forum.php?mod=ajax&action=getQiNiuToken&key='.urlencode($qiniuavatarfile);
	$CURL->request_init($token_url);
	$token_return = $CURL->request_action();
	$token_return = json_decode($token_return, 1);
	// 上传
	$post_array = array(
		'token'=>$token_return['uptoken'],
		'file'=>'@'.$bigavatarfile,
		'key'=>$qiniuavatarfile
	);
	$upload_url = 'http://upload.qiniu.com/';
	$CURL->request_init($upload_url, 'post', $post_array);
	$upload_return = $CURL->request_action();
	
}

//检查数据库起始位置
function app_ckstart($start, $perpage) {
	global $_G;
	$_G['setting']['maxpage'] = $_G['setting']['maxpage'] ? $_G['setting']['maxpage'] : 100;
	$maxstart = $perpage*intval($_G['setting']['maxpage']);
	if($start < 0 || ($maxstart > 0 && $start >= $maxstart)) {
		sendAppMessage(RES_NO,'length_is_not_within_the_scope_of');
	}
}

function makeurl($id, $idtype, $spaceuid=0) {
	$url = '';
	switch($idtype) {
		case 'tid':
			$url = 'forum.php?mod=viewthread&tid='.$id;
			break;
		case 'fid':
			$url = 'forum.php?mod=forumdisplay&fid='.$id;
			break;
		case 'blogid':
			$url = 'home.php?mod=space&uid='.$spaceuid.'&do=blog&id='.$id;
			break;
		case 'gid':
			$url = 'forum.php?mod=group&fid='.$id;
			break;
		case 'uid':
			$url = 'home.php?mod=space&uid='.$id;
			break;
		case 'albumid':
			$url = 'home.php?mod=space&uid='.$spaceuid.'&do=album&id='.$id;
			break;
		case 'aid':
			$url = 'portal.php?mod=view&aid='.$id;
			break;
	}
	return $url;
}

?>