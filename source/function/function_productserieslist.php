<?php
/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: function_admincp.php 34645 2014-06-16 09:08:07Z hypowang $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

function productseriesselect($groupselectable = FALSE, $arrayformat = 0, $selectedpsid = 0, $showhide = FALSE, $evalue = FALSE, $special = 0){
	global $_G;
	
	if(!isset($_G['cache']['productseries'])){
		loadcache('productseries');
		if(!isset($_G['cache']['productseries'])){
			updatecache('productseries');
		}
	}

	$seriescache = $_G['cache']['productseries'];
	$serieslist = $arrayformat ? array() : '<optgroup label="&nbsp;">';
	foreach($seriescache as $series) {
		if(!$series['status'] && !$showhide) {
			continue;
		}
		if($selectedpsid) {
			if(!is_array($selectedpsid)) {
				$selected = $selectedpsid == $series['psid'] ? ' selected' : '';
			} else {
				$selected = in_array($series['psid'], $selectedpsid) ? ' selected' : '';
			}
		}
		if($series['type'] == 'group') {
			if($arrayformat) {
				$serieslist[$series['psid']]['series'] = $series['series'];
			} else {
				$serieslist .= $groupselectable ? '<option value="'.($evalue ? 'gid_' : '').$series['psid'].'" class="bold">--'.$series['series'].'</option>' : '</optgroup><optgroup label="--'.$series['series'].'">';
			}
			$visible[$series['psid']] = true;
		} elseif($series['type'] == 'series' && isset($visible[$series['psup']]) && (!$series['viewperm'] || ($series['viewperm'] && seriesperm($series['viewperm'])) || strstr($series['users'], "\t$_G[uid]\t")) && (!$special || (substr($series['allowpostspecial'], -$special, 1)))) {
			if($arrayformat) {
				$serieslist[$series['psup']]['sub'][$series['psid']] = $series['series'];
			} else {
				$serieslist .= '<option value="'.($evalue ? 'psid_' : '').$series['psid'].'"'.$selected.'>'.$series['series'].'</option>';
			}
			$visible[$series['psid']] = true;
		} elseif($series['type'] == 'sub' && isset($visible[$series['psup']]) && (!$series['viewperm'] || ($series['viewperm'] && seriesperm($series['viewperm'])) || strstr($series['users'], "\t$_G[uid]\t")) && (!$special || substr($series['allowpostspecial'], -$special, 1))) {
			if($arrayformat) {
				$serieslist[$seriescache[$series['psup']]['psup']]['child'][$series['psup']][$series['psid']] = $series['series'];
			} else {
				$serieslist .= '<option value="'.($evalue ? 'psid_' : '').$series['psid'].'"'.$selected.'>&nbsp; &nbsp; &nbsp; '.$series['series'].'</option>';
			}
		}
	}
	if(!$arrayformat) {
		$serieslist .= '</optgroup>';
		$serieslist = str_replace('<optgroup label="&nbsp;"></optgroup>', '', $serieslist);
	}
	return $serieslist;
}

/**
 * @name 选中产品类型,更换产品
 */
function select_series_change_product_script(){
	$adminscript = ADMINSCRIPT;
	echo <<<EOT
<script type="text/javascript">
jQuery(function(){
	jQuery("#newseriesid").change(function(){
		var _this = jQuery(this);
		jQuery.ajax({
			"url":"{$adminscript}?action=productonsell&operation=ajaxgetproductonsell",
			"data":{
				"psid":_this.val()
			},
			"dataType":"json",
			success:function(data){
				var l = data,
					select = jQuery("#newspid");
				select.html("");
				for(var i = 0; i < l.length; i++){
					var option = '<option value="' + l[i]['spid'] + '">(' + l[i]['subject'] + ')' + l[i]['name'] + '</option>';
					jQuery(option).appendTo(select);
				}
			}
		});
	});
});
</script>
EOT;
}
