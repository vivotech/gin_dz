<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

require_once libfile('function/cache');
require_once DISCUZ_ROOT."/source/plugin/gsignin/table/table_gsignin_member.php";

/**
 * @name 获取所有签到会员记录
 * @return Array
 */
function getAllGsigninMemberData()
{
	// 获取缓存
	$gsignin_member_data = getCache('gsignin_member_data');
	$gsignin_member_data_time = getCache('gsignin_member_data_time');
	
	$cache_available = false;
	
	if ( $gsignin_member_data ) {		// 有缓存
		
		// 检查缓存
		$table_info = DB::fetch_first("SELECT TABLE_NAME,UPDATE_TIME FROM information_schema.tables where TABLE_NAME='vivo_gsignin_member'");

		if ( strtotime($table_info['UPDATE_TIME']) > $checkin_member_data_time) {		// 缓存已过期
			$gsignin_member_data = null;
		} else {
			$cache_available = true;
			return $gsignin_member_data;	// 缓存有效时直接返回
		}
	}
	
	// 没有缓存或缓存已过期
	
	// 获取数据
	$gsignin_member_data = C::t('gsignin_member')->fetch_all_my();

	// 保存缓存
	savecache('gsignin_member_data', $gsignin_member_data);
	savecache('gsignin_member_data_time', time());
	
	//
	return $gsignin_member_data;
}

function getUserGsigninMemberData($uid) {
	
	$gsignin_member_data = getAllGsigninMemberData();
	
	foreach ($gsignin_member_data as $v)
	{
		if ($v['uid'] == $uid) {
			return $v;
		}
	}
	
	return null;
}

function getGsigninRank($uid)
{
	// 获取用户的签到记录
	$user_gsignin = getUserGsigninMemberData($uid);
	
	if (!$user_gsignin) {	// 没有任何签到记录
		return 0;
	}
	
	$user_gsignin_total = $user_gsignin['total'];
	
	$gsignin_member_data = getAllGsigninMemberData();
	
	$gsignin_member_count = count($gsignin_member_data);
	
	$rank = 0;
	
	foreach ($gsignin_member_data as $v) {
		if ( $v['total'] < $user_gsignin_total && $v['uid'] != $uid ) {
			$rank++;
		}
	}
	
	return $rank / ($gsignin_member_count - 1);
	
}

function getTodayGsignTime($uid) {
	
	$last_gsign = C::t('table_gsignin_signin')->fetch_last_by_uid($uid);
	if (!$last_gsign) {
		return null;
	}
	
	return $last_gsign['currenttime'];
}



