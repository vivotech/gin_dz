<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

/* 经销商操作函数库
 * 依赖经销商缓存函数库  function/dealercache
 */
require_once libfile('function/dealercache');

/**
 * @name 获取商店
 * @param $duid 
 * @param $onlyone 是否只获取一个 
 * @return Array
 */
function getShop($duid, $onlyone = true){
	if($onlyone){
		return C::t('dealer_member_shop')->fetch_by_duid($duid);
	}else{
		return C::t('dealer_member_shop')->fetch_all_by_duid($duid);
	}
}

/**
 * @name 获取dealer下的所有商铺的id
 * @param Int $duid  店铺ID
 * @return Array
 */
function getDealerShopIds($duid) {
	$shop_record = getShop($duid, false);
	$ids = array();
	foreach ($shop_record as $shop) {
		$ids[] = $shop['sid'];
	}
	return $ids;
}

/**
 * 获取店铺日统计缓存
 * @param Int $sid  店铺ID
 * @param String|Array $date  日期，格式是 "yyyy-mm-dd"字符串 或 数组 ("year","month","day")
 * @param Bool $refresh  是否刷新数据
 * @return Array  dealer_shop_rebate_day 记录
 */
function getShopRebateDay($sid, $date, $refresh = false) {
	$year = 0;
	$month = 0;
	$day = 0;
	if ( is_array($date) ) {
		$year = $date['year'];
		$month = $date['month'];
		$day = $date['day'];
	} else {
		$time = strtotime($date);
		$year = date('Y', $time);
		$month = date('n', $time);
		$day = date('j', $time);
	}
	
	$datestr = $year.'-'.$month.'-'.$day;
	$timeline = strtotime($datestr);
	
	if ($refresh) {
		$record = update_record_day_cache($sid, $datestr, true);
	} else {
		$record = C::t('dealer_shop_rebate_day')->fetch_by_sid_year_month_day($sid, $year, $month, $day);
		if ( !$record ) {
			$record = update_record_day_cache($sid, $datestr, true);
		}
	}
	//var_dump($record);
	
	return $record;
}

/**
 * @name 获取店铺月统计缓存
 * @param Int $sid  店铺ID
 * @param Int $year  年份
 * @param Int $month  月份
 * @param Bool $refresh  是否刷新数据
 * @return Array
 */
function getShopRebateMonth($sid, $year, $month, $refresh = false) {
	
	if ( $refresh ) {
		$record = update_record_month_cache($sid, $year, $month, true);
	} else {
		$record = C::t('dealer_shop_rebate_month')->fetch_by_sid_year_month($sid, $year, $month);
	
		if ( !$record ) {
			$record = update_record_month_cache($sid, $year, $month, true);
		}
	}
	
	return $record;
}

/**
 * @name 获取店铺日浏览统计
 * @param Int $sid  店铺ID
 * @param String|Array $date  日期，格式是 "yyyy-mm-dd"字符串 或 数组 ("year","month","day")
 * @return Array
 */
function getShopBrowsingDay($sid, $date, $refresh = false) {
	
	$year = 0;
	$month = 0;
	$day = 0;
	if ( is_array($date) ) {
		$year = $date['year'];
		$month = $date['month'];
		$day = $date['day'];
	} else {
		$time = strtotime($date);
		$year = date('Y', $time);
		$month = date('n', $time);
		$day = date('j', $time);
	}
	
	$datestr = $year.'-'.$month.'-'.$day;
	$timeline = strtotime($datestr);
	
	// 从商品浏览日统计缓存表中获取记录
	if ($refresh) {
		$record = update_shop_browsing_day_cache($sid, $datestr, true);
	} else {
		$record = C::t('dealer_browsing_day')->fetch_by_sid_timeline($sid, $timeline);
		// 如果没有记录则更新日缓存
		if (!$record) {
			$record = update_shop_browsing_day_cache($sid, $datestr, true);
		}
	}
	
	return $record;
}

/**
 * @name 获取店铺月浏览统计
 * @param Int $sid  店铺ID
 * @param Int $year  年份
 * @param Int $month  月份
 * @param Bool $refresh  是否刷新数据
 * @return Int
 */
function getShopBrowsingMonth($sid, $year, $month, $refresh = false) {
	
	$datestr = $year.'-'.$month;
	$timeline = strtotime($datestr);
	$day_count = date('t');
	
	// 更新缓存
	if ($refresh) {
		$lastday = $day_count;
		if ( $year == date('Y') && $month == date('n') ) {
			$lastday = date('j');
		}
		rebuild_record_month_cache($sid, $year, $month, $lastday);
	}
	
	$count_browsing = 0;
	for ($i = 1; $i <= $day_count; $i++) {
		
		$shopBrowsingDay = C::t('dealer_product_browsing_day')->fetch_by_sid_timeline($sid, strtotime($datestr.'-'.$i));
		
		if ( !$shopBrowsingDay ) {
			$shopBrowsingDay = update_shop_browsing_day_cache($sid, $datestr.'-'.$i, true);
		}
		
		$count_browsing += $shopBrowsingDay['amount'];
	}
	
	return $count_browsing;
}

/**
 * @name 获取经销商日统计缓存
 * @param Int $duid  经销商ID
 * @param String|Array $date  日期，格式是 "yyyy-mm-dd"字符串 或 数组 ("year","month","day")
 * @param Bool $refresh  是否刷新数据
 * @return Array  dealer_rebate_day 记录
 */
function getDealerRebateDay($duid, $date, $refresh = false) {
	
	$year = 0;
	$month = 0;
	$day = 0;
	if ( is_array($date) ) {
		$year = $date['year'];
		$month = $date['month'];
		$day = $date['day'];
	} else {
		$time = strtotime($date);
		$year = date('Y', $time);
		$month = date('n', $time);
		$day = date('j', $time);
	}
	$datestr = $year . '-' . $month . '-' . $day;
	
	// 获取缓存记录
	if ($refresh) {
		$delaer_rebate_data = update_dealer_rebate_day_cache($duid, $datestr, true);
	} else {
		$delaer_rebate_data = C::t('dealer_rebate_day')->fetch_by_duid_day($duid, $year, $month, $day);
		// 没记录时更新缓存并获取数据
		if ( !$delaer_rebate_data ) {
			$delaer_rebate_data = update_dealer_rebate_day_cache($duid, $datestr, true);
		}
	}
	
	return $delaer_rebate_data;
}

/**
 * @name 获取经销商月统计缓存
 * @param Int $duid  经销商ID
 * @param Int $year  年份
 * @param Int $month  月份
 * @param Bool $refresh  是否刷新数据
 * @return Array  dealer_rebate_day 记录
 */
function getDealerRebateMonth($duid, $year, $month, $refresh = false) {
	
	if ($refresh) {
		$dealerRebateMonth = update_dealer_rebate_month_cache($duid, $year, $month, true);
	} else {
		$dealerRebateMonth = C::t('dealer_rebate_month')->fetch_by_duid_month($sid, $year, $month);
		if ( !$dealerRebateMonth ) {
			$dealerRebateMonth = update_dealer_rebate_month_cache($duid, $year, $month, true);
		}
	}
	
	return $dealerRebateMonth;
	
}



