<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

define('DATE_DATA_FORMAT_ERROR', '请给出正确的时间格式');
define('SHOP_PRODUCT_BROWSING_NOBODY', 'NoBody');

/**
 * @name 更新店铺日统计缓存
 * @param $sid
 * @param $datestr 格式:xxxx-xx-xx
 */
function update_record_day_cache($sid, $datestr, $returndata = false){
	
	$datetimestamp = strtotime($datestr);
	if($datetimestamp === FALSE){
		showmessage(DATE_DATA_FORMAT_ERROR);
	}
	$year = date('Y', $datetimestamp);
	$month = date('n', $datetimestamp);
	$day = date('j', $datetimestamp);
	
	$starttimestamp = $datetimestamp;
	$endtimestamp = $starttimestamp + ONE_DAY_SECONDS - 1;
	
	// 获取指定日的分成记录
	$order_list = C::t('dealer_shop_rebate')->fetch_all_by_sid_btime_etime($sid, $starttimestamp, $endtimestamp);
	$count_result = count_record($order_list);
	
	// 删除旧有统计
	C::t('dealer_shop_rebate_day')->delete_by_sid_year_month_day($sid, $year, $month, $day);
	$insert_data = array(
		'sid'=>$sid,
		'year'=>$year,
		'month'=>$month,
		'day'=>$day
	);
	$insert_data = array_merge($insert_data, $count_result);
	$opt = C::t('dealer_shop_rebate_day')->insert($insert_data);
	if ($returndata) {
		return $insert_data;
	} else {
		return $opt;
	}
}

/**
 * @name 更新购买记录月缓存
 * @param $sid
 * @param $year 年份
 * @param $month 月份,没有前导零
 */
function update_record_month_cache($sid, $year, $month, $returndata = false){
	$year = intval($year);
	$month = intval($month);
	if($month > 12 || $month < 1 || $year < 1970){
		showmessage(DATE_DATA_FORMAT_ERROR);
	}
	$month = ($month < 10) ? '0'.$month : $month;
	
	$datestr = $year.'-'.$month.'-'.'01 00:00:00';
	$datetimestamp = strtotime($datestr);
	$starttimestamp = $datetimestamp;
	$endtimestamp = $starttimestamp + date('t', $starttimestamp) * ONE_DAY_SECONDS;
	
	$order_list = C::t('dealer_shop_rebate')->fetch_all_by_sid_btime_etime($sid, $starttimestamp, $endtimestamp);
	$count_result = count_record($order_list);
	
	// 删除旧有统计
	C::t('dealer_shop_rebate_month')->delete_by_sid_year_month($sid, $year, $month);
	$insert_data = array(
		'sid'=>$sid,
		'year'=>$year,
		'month'=>$month
	);
	$insert_data = array_merge($insert_data, $count_result);
	$opt = C::t('dealer_shop_rebate_month')->insert($insert_data);
	
	if ($returndata)
	{
		return $insert_data;
	}
	
	return $opt;
}

/**
 * @name 统计购买记录并返回统计结果
 * @return Array
 */
function count_record(&$records){
	$count_result = array(
		'count'=>0,
		'total'=>0.0,
		'profit'=>0.0,
		'rebate'=>0.0
	);
	if(count($records) > 0){
		foreach($records as $k=>$o){
			if($o['total'] <= 0){
				continue;
			}
			$count_result['count'] += $o['amount'];
			$count_result['total'] += $o['total'];
			$count_result['profit'] += $o['profit'];
			$count_result['rebate'] += $o['rebate'];
		}
	}
	return $count_result;
}

/**
 * @name 重构购买记录当月每日记录缓存
 */
function rebuild_record_month_cache($sid, $year, $month, $lastday){
	$month = ($month < 10) ? '0'.$month : $month;
	for($i = 1 ; $i <= $lastday; $i++){
		$this_date = $year.'-'.$month.'-'.$i;
		update_record_day_cache($sid, $this_date);
	}
}

/**
 * @name 更新扫码记录日缓存
 * @param $sid
 * @param $datestr 格式:xxxx-xx-xx
 */
function update_shop_browsing_day_cache($sid, $datestr, $returndata = false){
	$datetimestamp = strtotime($datestr.' 00:00:00');
	if($datetimestamp === FALSE){
		showmessage(DATE_DATA_FORMAT_ERROR);
	}
	
	$starttimestamp = $datetimestamp;
	$endtimestamp = $starttimestamp + ONE_DAY_SECONDS;
	
	$insert_arr = array(
		'sid'=>$sid,
		'dateline'=>$starttimestamp
	);
	
	$insert_arr['amount'] = C::t('common_member_browsing')->count_by_sid_btime_etime($sid, $starttimestamp, $endtimestamp);
	C::t('dealer_browsing_day')->delete_by_sid_timeline($sid, $starttimestamp);
	$opt = C::t('dealer_browsing_day')->insert($insert_arr);
	
	if ($returndata) {
		return $insert_arr;
	}
	
	return $opt;

}

/**
 * @name 重构扫码记录月记录每日缓存
 */
function rebuild_shop_browsing_month_cache($sid, $year, $month, $lastday){
	$month = ($month < 10) ? '0'.$month : $month;
	for($i = 1 ; $i <= $lastday; $i++){
		$this_date = $year.'-'.$month.'-'.$i;
		update_shop_browsing_day_cache($sid, $this_date);
	}
}

/**
 * @name 更新产品扫码分布记录日缓存
 * @param $sid
 * @param $datestr 格式:xxxx.xx.xx
 * @param $num 生成前几个产品的缓存
 * @param Bool $returndate  是则返回缓存数据，否则返回缓存是否保存成功。
 * @return Array|Null
 */
function update_shop_product_browsing_day_cache($sid, $datestr, $num = null, $returndate = false){
	$num = ($num) ? intval($num) : 0;
	//die();
	$datetimestamp = strtotime($datestr.' 00:00:00');
	if($datetimestamp === FALSE){
		showmessage(DATE_DATA_FORMAT_ERROR);
	}
	
	$starttimestamp = $datetimestamp;
	$endtimestamp = $starttimestamp + ONE_DAY_SECONDS;
	
	$totalcount = C::t('common_member_browsing')->count_by_sid_btime_etime($sid, $starttimestamp, $endtimestamp);
	$pdlist = C::t('common_member_browsing')->count_pd_by_sid_btime_etime($sid, $starttimestamp, $endtimestamp, 0, $num);
	
	C::t('dealer_product_browsing_day')->delete_by_sid_timeline($sid, $datetimestamp);
	$insert_data = array(
		'sid'=>$sid,
		'dateline'=>$datetimestamp
	);
	if(count($pdlist) > 0){
		foreach($pdlist as $pd){
			format_shop_product_browsing_data($insert_data, $pd, $totalcount);
			C::t('dealer_product_browsing_day')->insert($insert_data);
		}
	}else{
		$insert_data['pdid'] = 0;
		$insert_data['pdname'] = SHOP_PRODUCT_BROWSING_NOBODY;
		$insert_data['rate'] = 1000;
		$opt = C::t('dealer_product_browsing_day')->insert($insert_data);
	}
	
	if ($returndate) {
		return $insert_data;
	}
}

/**
 * @name 更新产品扫码分布记录月记录
 * @param Int $sid  店铺ID
 * @param Int $year  年份
 * @param Int $month  月份
 * @param $num 生成前几个产品的缓存
 */
function update_shop_product_browsing_month_cache($sid, $year, $month, $num = null){
	
	$num = ($num) ? intval($num) : 0;
	$this_month = ($month < 10) ? '0'.$month : $month;
	
	// 指定月的第一天
	$this_date = $year.'-'.$this_month.'-01 00:00:00';
	$starttimestamp = strtotime($this_date);
	
	// 指定月的总天数
	$days = date('t', $starttimestamp);
	
	// 指定月的最后一秒
	$endtimestamp = $starttimestamp + $days * ONE_DAY_SECONDS - 1;
	
	
	$totalcount = C::t('common_member_browsing')->count_by_sid_btime_etime($sid, $starttimestamp, $endtimestamp);
	$pdlist = C::t('common_member_browsing')->count_pd_by_sid_btime_etime($sid, $starttimestamp, $endtimestamp, 0, $num);
	
	C::t('dealer_product_browsing_month')->delete_by_sid_year_month($sid, $year, $month);
	$insert_data = array(
		'sid'=>$sid,
		'year'=>$year,
		'month'=>$month
	);
	if(count($pdlist) > 0){
		foreach($pdlist as $pd){
			format_shop_product_browsing_data($insert_data, $pd, $totalcount);
			C::t('dealer_product_browsing_month')->insert($insert_data);
		}
	}else{
		$insert_data['pdid'] = 0;
		$insert_data['pdname'] = SHOP_PRODUCT_BROWSING_NOBODY;
		$insert_data['rate'] = 1000;
		C::t('dealer_product_browsing_month')->insert($insert_data);
	}
}

/**
 * @name 更新经销商的日销售统计记录
 * @param Int $duid  经销商ID
 * @param String $datestr 格式:yyyy-mm-dd
 * @param Bool $returndate  是则返回缓存数据，否则返回缓存是否保存成功，默认 false。
 * @return Null|Array
 */
function update_dealer_rebate_day_cache($duid, $datestr, $returndata = false) {
	
	// 删除旧记录
	$time = strtotime($datestr);
	$year = date('Y', $time);
	$month = date('n', $time);
	$day = date('j', $time);
	C::t('dealer_rebate_day')->delete_by_duid_day($duid, $year, $month, $day);
	
	// 获取所有店铺的ID
	$shopIds = getDealerShopIds($duid);
	
	$count = 0;
	$total = 0;
	$profit = 0;
	$rebate = 0;
	$browsing_amount = 0;
	
	// 开始计算
	foreach ($shopIds as $sid) {
		
		// 先更新每一间店铺的日销售统计记录
		$shop_data = update_record_day_cache($sid, $datestr, true);
		
		// 叠加数据
		$count += $shop_data['count'];
		$total += $shop_data['total'];
		$profit += $shop_data['profit'];
		$rebate += $shop_data['rebate'];
		
		$shopProductBrowsingDay = getShopBrowsingDay($sid, $datestr, true);
		$browsing_amount += $shopProductBrowsingDay['amount'];
	}
	
	// 保存数据
	$dealer_rebate_date = array(
		'duid'	=> $duid,
		'year'	=> $year,
		'month'	=> $month,
		'day'	=> $day,
		'count'	=> $count,
		'total'	=> $total,
		'profit'	=> $profit,
		'rebate'	=> $rebate,
		'browsing_amount'	=> $browsing_amount
	);
	
	C::t('dealer_rebate_day')->insert($dealer_rebate_date);
	
	if ($returndata) {
		return $dealer_rebate_date;
	}
}

/**
 * @name 重构经销商的当月销售统计每日缓存
 */
function rebuild_dealer_rebate_month_cache($duid, $year, $month, $lastday){
	$month = ($month < 10) ? '0'.$month : $month;
	for($i = 1 ; $i <= $lastday; $i++){
		$this_date = $year.'-'.$month.'-'.$i;
		update_dealer_rebate_day_cache($duid, $this_date);
	}
}

/**
 * @name 更新经销商的月销售统计记录
 * @param Int $duid  经销商ID
 * @param Int $year 年份
 * @param Int $month 月份
 * @param Bool $returndate  是则返回缓存数据，否则返回缓存是否保存成功，默认 false。
 * @return Null|Array
 */
function update_dealer_rebate_month_cache($duid, $year, $month, $returndata = false) {
	
	$datestr = $year.'-'.$month.'-1 00:00:00';
	$time = strtotime($datestr);
	
	// 重构当月每日的缓存
	rebuild_dealer_rebate_month_cache($duid, $year, $month, date('t', $time));
	// 删除旧记录
	C::t('dealer_rebate_month')->delete_by_duid_month($duid, $year, $month);
	
	// 获取所有店铺的ID
	$shopIds = getDealerShopIds($duid);
	
	$count = 0;
	$total = 0;
	$profit = 0;
	$rebate = 0;
	$browsing_amount = 0;
	
	foreach ($shopIds as $sid) {
		
		// 先更新每一间店铺的月扫码统计记录
		$records = C::t('dealer_rebate_day')->fetch_all_by_duid_year_month($duid, $year, $month);
		
		foreach($records as $r){
			$count += $r['count'];
			$total += $r['total'];
			$profit += $r['profit'];
			$rebate += $r['rebate'];
			$browsing_amount += $r['browsing_amount'];
		}
	}
	
	$insert_data = array(
		'duid'	=> $duid,
		'year'	=> $year,
		'month'	=> $month,
		'count'	=> $count,
		'total'	=> $total,
		'profit'	=> $profit,
		'rebate'	=> $rebate,
		'browsing_amount'	=> $browsing_amount
	);
	
	$res = C::t('dealer_rebate_month')->insert($insert_data);
	
	if($returndata){
		return $insert_data;
	}else{
		return $res;
	}

}

/**
 * @name 格式化商品浏览数据
 */
function format_shop_product_browsing_data(&$insert_data, $pd, $totalcount){
	$this_product = C::t('common_product')->fetch($pd['pdid']);
	$insert_data['pdid'] = $pd['pdid'];
	$insert_data['pdname'] = $this_product['name'];
	$insert_data['rate'] = floor(($pd['c'] / $totalcount) * 1000);
}
