<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: function_admincp.php 34645 2014-06-16 09:08:07Z hypowang $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

function froumactivitytypeselect($selectid = 0){
	$arr = array('悬赏活动', '试用活动');
	$select_str = '';
	foreach($arr as $k => $v){
		if($selectid == $k){
			$select_str .= '<option value="'.$k.'" selected>'.$v.'</option>';	
		}else{
			$select_str .= '<option value="'.$k.'">'.$v.'</option>';
		}
	}
	return $select_str;
}

/**
 * 获取活动排名
 */
function get_rank($fid){
	if(empty($fid)) return FALSE;
	$fid = intval($fid);
	$activity = get_activity($fid);
	$rank_formula = $activity['rank'] ? $activity['rank'] : '[L]*1+[R]*1+[G]*1+[F]*1';
	//计算公式
	$rank_formula = count_formula($rank_formula);
	//拼凑sql
	$sql_rank_formula = str_replace(array('[L]','[R]','[G]','[F]'), array('views','replies','saygood','favtimes'), $rank_formula);
	
	$sql = "SELECT *,{$sql_rank_formula} as rank FROM ".DB::table('forum_thread')." WHERE fid='{$fid}' ORDER BY rank DESC LIMIT 0,10";
	$res=DB::fetch_all($sql);
	return $res;
}

function get_activity($fid){
	return C::t('forum_custom_activity')->fetch_by_fid($fid);
}

function count_formula($formula){
	$_temp_formula = explode('+', $formula);
	$_all_count = 0;
	foreach($_temp_formula as $k=>$v){
		$_sigle = explode('*', $v);
		$_all_count += intval($_sigle[1]);
	}
	$res = '';
	foreach($_temp_formula as $key=>$val){
		$_one = explode('*', $val);
		$res .= ($res==''?'':'+').$_one[0].'*('.$_one[1]."/".$_all_count.')';
	}
	return $res;
}
