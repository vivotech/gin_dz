<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

/**
 * @name 创建x轴为时间的数组
 * @author Jerry
 * @param $timebegin 开始时间戳
 * @param $timeend 结束时间戳
 * @param $num 分开多少段
 * @param $dateformat X坐标时间格式
 * @param $isfollow X轴数据点是只标注开始时间,还是标注开始时间与结束时间
 */
function x_axis_times($timebegin, $timeend, $num, $dateformat = 'Y-m-n H:i:s', $isfollow = true){
	
	// 总时间距离
	$timedis = $timeend - $timebegin;
	if($timedis <= 0){
		return FALSE;
	}
	
	// 时间段距离
	$timepiece = $timedis / $num;
	
	$i = 1;
	while($i <= $num){
		if($isfollow){
			$return[date($dateformat,$timebegin).'~'.date($dateformat,$timebegin + $timepiece)] = 0;
		}else{
			$return[date($dateformat,$timebegin)] = 0;
		}
		
		$timebegin += $timepiece;
		$i++;
	}
	return $return;
}