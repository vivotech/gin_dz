<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: function_admincp.php 34645 2014-06-16 09:08:07Z hypowang $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

@set_time_limit(0);

