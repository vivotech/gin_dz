<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

/**
 * 快速简历wechatClient对象
 */
function buildWechatClientObj($wechat){
	return new wechat_client($wechat['key'],$wechat['secret']);
}

function setWXPayState($oid,$weid,$return_data){
	$model = M('payment_wechat_state');
	$item = $model->where("oid='{$oid}'")->find();
	//如果没有，就插入状态
	$insertData = array(
			'return_code'	=> $return_data["return_code"],
			'result_code'	=> $return_data["result_code"],
			'trade_state'	=> $return_data["trade_state"] ? $return_data["trade_state"] : '0',
			'weid'		 	=> $weid,
			'oid'		 	=> $oid,
			'out_trade_no'	=> $return_data["out_trade_no"],
			'openid'		=> $return_data['openid'],
			'is_subscribe'	=> $return_data['is_subscribe'],
			'trade_type'	=> $return_data['trade_type'],
			'bank_type'		=> $return_data['bank_type'],
			'total_fee'		=> $return_data['total_fee'],
			'coupon_fee'	=> $return_data['coupon_fee'],
			'fee_type'		=> $return_data['fee_type'],
			'transaction_id'=> $return_data['transaction_id'],
			'time_end'		=> $return_data['time_end'],
		);

//    file_put_contents(DISCUZ_ROOT.DS."pws.log", var_export($insertData,true)."\r\n".var_export($item,true),FILE_APPEND);

	if(empty($item) || $item == null || !$item){
		$model->add($insertData);
	}else{
		$model->where("pws_id='{$item[pws_id]}'")->save($insertData);
	}
	//更新订单表支付状态
	M('common_member_order')->where("oid='{$oid}'")->save(array('paystate'=>1));
	return $item ? 'update' : 'insert'; 
}

//微信自动关联账号
function wechat_auto_bind_accout($data,$wechatdata){
	if(empty($data) || !is_array($data) || empty($data['input']) || empty($wechatdata) || !is_array($wechatdata)){return FALSE;}
	$user = C::t('common_member')->fetch_by_username($data['input']);
	if($user){
		//更新绑定即可
		$res = updataWechatMemberByID($wechatdata['id'],array('uid'=>$user['uid']));
		//更新用户昵称
		C::t('common_member_profile')->update_by_uid($user['uid'],array('nickname'=>$wechatdata['nickname']));
		
		//实现自动登录
		require_once libfile('function/member');
		return setWechatLoginStatus($user,2592000);
	}else{
		return wechat_auto_register($data,$wechatdata);
	}
}

//微信自动登录，含自动注册账号
function wechat_auto_register($data,$wechatdata){
	if(empty($data) || !is_array($data) || empty($data['input']) || empty($wechatdata) || !is_array($wechatdata)){return FALSE;}
	global $_G;
	include_once libfile('wechat/member','class');
	require_once libfile('function/app');
	if(!defined('RES_NO')){define('RES_NO', 0);}
	if(!defined('RES_YES')){define('RES_YES', 1);}
	
	$regdatas = array(
				'type'	=> $data['type'] ? $data['type'] : 'mobile',
				'input'	=>	$data['input'],
				'password' => $data['password'] ? $data['password'] : '123456',
			);
	
	$register = new wechat_register_ctl();
	$register->setting = $_G['setting'];
	//设置返回数据类型
	$register->returntype = "RETURN";
	$register->after_register_array = array('updataWechatMember'=>$wechatdata);
	return $register->on_register($regdatas);
}

/**
 * 获取公众号的wechat_client对象,默认取回默认公众号的
 */
function getDefalutWechatClient($weid=0){
	global $_G;
	if($weid){
		$wechat = getWechatByWeid($weid);
		return new wechat_client($wechat['key'],$wechat['secret']);
	}else{
		return new wechat_client($_G['wechat']['key'],$_G['wechat']['secret']);
	}
}

function getWechatByWeid($weid){
	return M('wechats')->find($weid);
}

function getWechatByHash($hash){
	global $_G;
	//直接读缓存
	if($_G['wechat']['hash']==$hash){
		return $_G['wechat'];
	}
	$wechat = M('wechats')->where("hash='$hash'")->find();
	if($wechat){
		$_G['wechat'] = $wechat;
	}
	return $wechat;
}

function getLevelName($level=-1){
	$levels = array('普通订阅号','认证订阅好/普通服务号','认证服务号');
	if($level>-1 && $level<count($levels)){
		return $levels[$level];
	}elseif($level==-1){
		return $levels;
	}
	return FALSE;
}

function getReplyType($type=-1,$lang = 'zh'){
	$types =  array('文字回复','图文回复','语音回复');
	$types_en = array('text','image','music');
	$types = $lang == 'zh' ? $types : $types_en;
	if($type>-1 && $type<count($types)){
		return $types[$type];
	}elseif($type==-1){
		return $types;
	}
	return FALSE;
}

function getReplyKeywordType($type=-1){
	$types =  array('匹配','包含','正则');
	if($type>-1 && $type<count($types)){
		return $types[$type];
	}elseif($type==-1){
		return $types;
	}
	return FALSE;
}

//设置默认公众账号，可全局使用$_G['wechat']调用
function setDefaultWechat($weid){
	$weid = intval($weid);
	$item = C::t('wechats')->fetch($weid);
	if(!$item){
		return FALSE;
	}
	$settings = array('wechat' => $item);
	$res = C::t('common_setting')->update_batch($settings);
	updatecache('setting');
	return $res;
}

function getWechatMemberByOpenid($openid){
	return C::t('common_member_wechat')->fetch_by_openid($openid);
}

function getWechatMemberByUnionid($unionid){
	return C::t('common_member_wechat')->fetch_by_unionid($unionid);
}

function updataWechatMemberByID($id,$values=array()){
	if(empty($values)){return FALSE;}
	if(!is_array($values)){return FALSE;}
	return C::t('common_member_wechat')->update($id,$values);
}

//获取微信性别
function getSexName($sex){
	switch($sex){
		case 1:return '男';break;
		case 2:return '女';break;
		default:return '未知';break;
	}
}

function smartUrlEncode($url){
	if (strpos($url, '=') === false) {
		return $url;
	} else {
		$urls = parse_url($url);
		parse_str($urls['query'], $queries);
		if (!empty($queries)) {
			foreach ($queries as $variable => $value) {
				$params[$variable] = urlencode($value);
			}
		}
		$queryString = http_build_query($params, '', '&');
		return $urls['scheme'] . '://' . $urls['host'] . $urls['path'] . '?' . $queryString . '#' . $urls['fragment'];
	}
}

/**
 * @param $rid 回复ID
 * @param $type 回复类型
 */
function fieldsFormDisplay($rid,$type){
	global $tpl,$_G;
	$type_en = getReplyType($type,'en');
	$tpl->reply = $reply = M('wechat_reply_'.$type_en)->where("rid='{$rid}'")->select();
	echo $tpl->fetch($type_en.'_list','msgitem');
}

?>
