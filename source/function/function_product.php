<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: function_admincp.php 34645 2014-06-16 09:08:07Z hypowang $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

/**
 * 判断能否购买此商品
 * @param $spid 
 * @param $num 购买的数量
 * @return int 0,可以购买;1,超过时限;2,超过限量;3,超过库存;4,用户组权限不足;5,积分不足
 */
function isBuyable($spid, $num = 0){
	global $_G;
	$spid = intval($spid);
	if($spid <= 0 || !$_G['uid']){
		return false;
	}
	
	$onsellproduct = C::t('common_product_onsell')->fetch($spid);
	
	if(TIMESTAMP > $onsellproduct['expiration'] && $onsellproduct['expiration'] > 0){
		return 1;
	}
	if($onsellproduct['groupid']){
		if(!$_G['cache']['usergroups']){
			loadcache('usergroups');
		}
		if($_G['member']['credits'] < $_G['cache']['usergroups'][$onsellproduct['groupid']]['creditshigher']){
			return 4;
		}
	}
	$buyablenum = buyableNum($spid);
	if($buyablenum != -1 && $buyablenum == 0){
		return 2;
	}
	if($buyablenum > 0 && $buyablenum < $num){
		return 3;
	}

	if($onsellproduct['type'] == 1){
		$member = C::t('common_member_count')->fetch($_G['uid']);
		$price = getDiscountPrice($onsellproduct['discount'] ? $spid : 0, $onsellproduct['price'], $onsellproduct['discounttype']);
		if($member['extcredits'.$_G['setting']['creditstrans']] < $price * $num){
			return 5;
		}
	}

	return 0;
}

/**
 * @name 返回商品的可购买数,仅考虑商品限制规则,库存,不考虑用户积分
 * @return int 可购买数量
 */
function buyableNum($spid){
	global $_G;
	$spid = intval($spid);
	if($_G['uid'] > 0 && $spid > 0){
		$onsellproduct = C::t('common_product_onsell')->fetch($spid);
		$stock_nums = $onsellproduct['totalitems'] - $onsellproduct['amount'];
		$stock_nums = ($stock_nums > 0) ? $stock_nums : 0;
		if($onsellproduct['limit'] <= 0){
			return $stock_nums;
		}else{
			$buy_nums = C::t('common_member_order_detail')->fetch_num_by_spid($spid, $_G['uid']);
			$buyable_num = $onsellproduct['limit'] - $buy_nums;
			if($buyable_num > 0){
				return ($stock_nums > $buyable_num) ? $buyable_num : $stock_nums;
			}else{
				return 0;
			}
		}
	}else{
		return 0;
	}
}

/**
 * @name 返回商品的可购买数,考虑用户积分
 * @return int 可购买数量
 */
function buyableNumCredits($spid){
	global $_G;
	$buyable_nums = buyableNum($spid);
	if($buyable_nums > 0 || $buyable_nums == -1){
		$onsellproduct = C::t('common_product_onsell')->fetch($spid);
		$member = C::t('common_member_count')->fetch($_G['uid']);
		$price = getDiscountPrice($onsellproduct['discount'] ? $spid : 0, $onsellproduct['price'], $onsellproduct['discounttype']);
		$num = floor($member['extcredits'.$_G['setting']['creditstrans']] / $price);
		if($buyable_nums == -1){
			return $num;
		}else{
			return ($buyable_nums > $num) ? $num : $buyable_nums;
		}
	}
	return $buyable_nums;
}

/**
 * @name 获取单个在售商品信息
 * @param $id pdid
 */
function getProduct($id = 0){
	global $_G;
	if(!$id) return FALSE;
	$id = intval($id);
	$products = getProductList($id);
	return $products[0];
}

/**
 * @name 获取在售商品列表(含商品折扣)
 * @param $ids pdid
 */
function getProductList($ids){
	global $_G;
	if(empty($ids)) return FALSE;
	$ids = is_array($ids) ? $ids : explode(',' , $ids);
	$products = C::t('common_product')->fetch_onsell_product_by_pdid($ids);

	foreach($products as $pk=>$pv){
		//当用户登录的时候才显示折扣信息
		if($_G['uid'] && $pv['price'] > 0){
			$products[$pk]['discount_price'] = getDiscountPriceByProductOnsell($pv);
		}else{
			$products[$pk]['discount_price'] = $pv['costprice'];
		}
	}
	return $products;
}

/**
 * @name 计算折扣价格
 * @param $productonsell common_product_onsell的数据行
 */
function getDiscountPriceByProductOnsell($productonsell){
	$discount_price = $productonsell['costprice'];
	//根据discount判断结构类型
	//discount : 0为自定义模板，1为全局折扣模板，查找规则库
	if($productonsell['discount'] == 1){
		//全局折扣模板， 默认模板只有 用户组设置
		$discount_price = getDiscountPrice(0, $productonsell['price'], 0);
	}else{
		$discount_price = getDiscountPrice($productonsell['spid'], $productonsell['price'], $productonsell['discounttype']);
	}
	return $discount_price;
}

/**
 * 判断自定义折扣
 * @param int $spid 上架商品ID
 * @param float $old_price 折扣前价格，原价
 * @param int $type 折扣规则：0用户组规则,1积分规则
 * @return 折扣价格
 */
function getDiscountPrice($spid,$old_price,$type = 1){
	global $_G;
	//积分规则
	if($type == 1){
		$sp = C::t('common_product_onsell_discount')->fetch_all_by_spid($spid,' type = 1 ' , ' credit DESC ');
	}else{
		$sp = C::t('common_product_onsell_discount')->fetch_all_by_spid($spid,' type = 0 ' );
	}
	$new_price = $old_price;
	foreach($sp as $k=>$v){
		//判断规则类型: 0用户组规则,1积分规则
		if($type == 1){
			//大于多少积分就有折扣
			if($_G['member']['credits']>=$v['credit']){
				//判断折扣 
				$new_price = checkOperationtype($v,$new_price);
			}
		}else{
			//用户组规则
			if($_G['member']['groupid'] == $v['groupid']){
				//判断折扣 
				$new_price = checkOperationtype($v,$new_price);
			}
		}
	}
	return $new_price;
}
 
/**
 * 判断折扣类型，0折扣,1减扣
 */

function checkOperationtype($sp,$price){
	$new_price = 0;
	//判断折扣 ： 0折扣,1减扣
	if($sp['operationtype'] == 1){
		$new_price = number_format($price - $sp['num'] , 2, '.','');
	}else{
		$new_price = number_format($price * ($sp['num'] / 100) , 2,'.','');
	}
	return $new_price;
}

/**
 * @name 更新上架产品状态
 */
function updateOnsellProductState($spid){
	$product_onsell = C::t('common_product_onsell')->fetch($spid);
}

/**
 * get product onsell record  获取上架商品记录
 * @param $spid Int  product onsell ID
 * @return Array|false
 */
function getProductOnsellBySpid($spid)
{
  return C::t('common_product_onsell')->fetch_by_spid($spid);
}

/**
 * get spid by product ID  根据商品ID获取一个上架商品ID
 * @param $pdid Int  product ID
 * @return Int|false
 */
function getSpidByProductId($pdid)
{
  $table = new table_common_product_onsell;
  return $table->fetch_spid_by_pdid($pdid);
}

/**
 * get spids by product ID  根据商品ID获取所有上架商品ID
 * @param $pdid Int  product ID
 * @return Array
 */
function getSpidsByProductId($pdid)
{
  $table = new table_common_product_onsell;
  return $table->fetch_spids_by_pdid($pdid);
}

/**
 * @name 获取媒体列表
 * @param Array $product
 * @return Array
 */
function getMediaList($product)
{
	$media_list = $product['media_list'];
	if (is_string($media_list)) {
		return json_decode(htmlspecialchars_decode($media_list), true);
	}
	return $media_list;
}

/**
 * 解析商品详情
 * @param string $desc 未解码商品详情
 * @return string 解码后商品详情
 */
function decodeDesc($desc){
	return preg_replace('/\[img\]([a-zA-Z0-9\/\.]*)\[\/img\]/', '<img src="$1" />', htmlspecialchars_decode($desc));	
}

/**
 * 计算剩余时间
 * @param int $dateline 上架日期时间戳
 * @param int $expiration 下架日期时间戳
 * @return int 返回剩余多少秒
 */
function get_left_time($dateline,$expiration,$format = 0){
	if(time() > $expiration){
		return 0;
	}
	if($dateline > time()){
		return time()-$dateline;
	}
	return $expiration - time();
	
}

/**
 * @name 获取计量单位
 * @param float $price 价格
 * @param int $type 类型 : 0：人民币 ; 1:积分
 * @return string 价格（含单位）
 */
function getBuyPriceUnit($price,$type){
	if($type == 0){
		return '￥&nbsp;'.$price;
	}else{
		return $price.'&nbsp;'.$_G['setting']['extcredits'][$_G['setting']['creditstrans']]['title'];
	}
}

