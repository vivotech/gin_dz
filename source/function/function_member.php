<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: function_member.php 35030 2014-10-23 07:43:23Z laoguozhang $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

function userlogin($username, $password, $questionid, $answer, $loginfield = 'username', $ip = '') {
	$return = array();

	if($loginfield == 'uid' && getglobal('setting/uidlogin')) {
		$isuid = 1;
	} elseif($loginfield == 'email') {
		$isuid = 2;
	} elseif($loginfield == 'auto') {
		$isuid = 3;
	} else {
		$isuid = 0;
	}

	if(!function_exists('uc_user_login')) {
		loaducenter();
	}
	//判断是哪种登陆方式，如果客户没有指定的话
	//需要手动去判断
	if($isuid == 3) {
		if(!strcmp(dintval($username), $username) && getglobal('setting/uidlogin')) {
		//使用UID登陆
			$return['ucresult'] = uc_user_login($username, $password, 1, 1, $questionid, $answer, $ip);
		} elseif(isemail($username)) {
		//使用邮箱登陆
			$return['ucresult'] = uc_user_login($username, $password, 2, 1, $questionid, $answer, $ip);
		} elseif(check_mobile_number($username)){
		//使用手机登陆
			$return['ucresult'] = uc_user_login($username, $password, 3, 1, $questionid, $answer, $ip);
		}
		if($return['ucresult'][0] <= 0 && $return['ucresult'][0] != -3) {
			$return['ucresult'] = uc_user_login(addslashes($username), $password, 0, 1, $questionid, $answer, $ip);
		}
	} else {
		$return['ucresult'] = uc_user_login(addslashes($username), $password, $isuid, 1, $questionid, $answer, $ip);
	}
	$tmp = array();
	$duplicate = '';
	list($tmp['uid'], $tmp['username'], $tmp['password'], $tmp['email'], $duplicate) = $return['ucresult'];
	$return['ucresult'] = $tmp;
	if($duplicate && $return['ucresult']['uid'] > 0 || $return['ucresult']['uid'] <= 0) {
		$return['status'] = 0;
		return $return;
	}

	$member = getuserbyuid($return['ucresult']['uid'], 1);
	if(!$member || empty($member['uid'])) {
		$return['status'] = -1;
		return $return;
	}
	$return['member'] = $member;
	$return['status'] = 1;
	if($member['_inarchive']) {
		C::t('common_member_archive')->move_to_master($member['uid']);
	}
	if($member['email'] != $return['ucresult']['email']) {
		C::t('common_member')->update($return['ucresult']['uid'], array('email' => $return['ucresult']['email']));
	}

	return $return;
}

function setWechatLoginStatus($member, $cookietime){
	global $_G;
	$credits = setloginstatus($member, $cookietime);
	require_once libfile('function/app');
	$param = array(
		'username' => $_G['member']['username'],
		'usergroup' => $_G['group']['grouptitle'], 
		'uid' => $_G['member']['uid'], 
		'groupid' => $_G['groupid'], 
		'avatar'=> avatar($_G['member']['uid'],'middle',TRUE),
		'credits' => $_G['member']['credits'],
		'addcredits' => app_get_return_credict($credits),
		'syn' => $ucsynlogin ? 1 : 0,
	);
	$param = array_merge($param,$_G['member']);
	return sendAppMessage(1, '登录成功！' , $param, 'RETURN');
}

function setloginstatus($member, $cookietime) {
	global $_G;
	$_G['uid'] = intval($member['uid']);
	$_G['username'] = $member['username'];
	$_G['adminid'] = $member['adminid'];
	$_G['groupid'] = $member['groupid'];
	$_G['formhash'] = formhash();
	$_G['session']['invisible'] = getuserprofile('invisible');
	//加入profile表中数据 by goodspb 20150513
	space_merge($member, 'profile');
	$_G['member'] = $member;
	
	loadcache('usergroup_'.$_G['groupid']);
	C::app()->session->isnew = true;
	C::app()->session->updatesession();

	dsetcookie('auth', authcode("{$member['password']}\t{$member['uid']}", 'ENCODE'), $cookietime, 1, true);
	dsetcookie('loginuser');
	dsetcookie('activationauth');
	dsetcookie('pmnum');

	include_once libfile('function/stat');
	updatestat('login', 1);
	if(defined('IN_MOBILE')) {
		updatestat('mobilelogin', 1);
	}
	if($_G['setting']['connect']['allow'] && $_G['member']['conisbind']) {
		updatestat('connectlogin', 1);
	}
	//检查积分
	$rule = updatecreditbyaction('daylogin', $_G['uid']);
	if(!$rule['updatecredit']) {
		checkusergroup($_G['uid']);
	}
	return $rule;
}

function logincheck($username) {
	global $_G;

	$return = 0;
	$username = trim($username);
	loaducenter();
	if(function_exists('uc_user_logincheck')) {
		$return = uc_user_logincheck(addslashes($username), $_G['clientip']);
	} else {
		$login = C::t('common_failedlogin')->fetch_ip($_G['clientip']);
		$return = (!$login || (TIMESTAMP - $login['lastupdate'] > 900)) ? 5 : max(0, 5 - $login['count']);

		if(!$login) {
			C::t('common_failedlogin')->insert(array(
				'ip' => $_G['clientip'],
				'count' => 0,
				'lastupdate' => TIMESTAMP
			), false, true);
		} elseif(TIMESTAMP - $login['lastupdate'] > 900) {
			C::t('common_failedlogin')->insert(array(
				'ip' => $_G['clientip'],
				'count' => 0,
				'lastupdate' => TIMESTAMP
			), false, true);
			C::t('common_failedlogin')->delete_old(901);
		}
	}
	return $return;
}

function loginfailed($username) {
	global $_G;

	loaducenter();
	if(function_exists('uc_user_logincheck')) {
		return;
	}
	C::t('common_failedlogin')->update_failed($_G['clientip']);
}

function failedipcheck($numiptry, $timeiptry) {
	global $_G;
	if(!$numiptry) {
		return false;
	}
	list($ip1, $ip2) = explode('.', $_G['clientip']);
	$ip = $ip1.'.'.$ip2;
	return $numiptry <= C::t('common_failedip')->get_ip_count($ip, TIMESTAMP - $timeiptry);
}

function failedip() {
	global $_G;
	list($ip1, $ip2) = explode('.', $_G['clientip']);
	$ip = $ip1.'.'.$ip2;
	C::t('common_failedip')->insert_ip($ip);
}

function getinvite() {
	global $_G;

	if($_G['setting']['regstatus'] == 1) return array();
	$result = array();
	$cookies = empty($_G['cookie']['invite_auth']) ? array() : explode(',', $_G['cookie']['invite_auth']);
	$cookiecount = count($cookies);
	$_GET['invitecode'] = trim($_GET['invitecode']);
	if($cookiecount == 2 || $_GET['invitecode']) {
		$id = intval($cookies[0]);
		$code = trim($cookies[1]);
		if($_GET['invitecode']) {
			$invite = C::t('common_invite')->fetch_by_code($_GET['invitecode']);
			$code = trim($_GET['invitecode']);
		} else {
			$invite = C::t('common_invite')->fetch($id);
		}
		if(!empty($invite)) {
			if($invite['code'] == $code && empty($invite['fuid']) && (empty($invite['endtime']) || $_G['timestamp'] < $invite['endtime'])) {
				$result['uid'] = $invite['uid'];
				$result['id'] = $invite['id'];
				$result['appid'] = $invite['appid'];
			}
		}
	} elseif($cookiecount == 3) {
		$uid = intval($cookies[0]);
		$code = trim($cookies[1]);
		$appid = intval($cookies[2]);

		$invite_code = space_key($uid, $appid);
		if($code == $invite_code) {
			$inviteprice = 0;
			$member = getuserbyuid($uid);
			if($member) {
				$usergroup = C::t('common_usergroup')->fetch($member['groupid']);
				$inviteprice = $usergroup['inviteprice'];
			}
			if($inviteprice > 0) return array();
			$result['uid'] = $uid;
			$result['appid'] = $appid;
		}
	}

	if($result['uid']) {
		$member = getuserbyuid($result['uid']);
		$result['username'] = $member['username'];
	} else {
		dsetcookie('invite_auth', '');
	}

	return $result;
}

function replacesitevar($string, $replaces = array()) {
	global $_G;
	$sitevars = array(
		'{sitename}' => $_G['setting']['sitename'],
		'{bbname}' => $_G['setting']['bbname'],
		'{time}' => dgmdate(TIMESTAMP, 'Y-n-j H:i'),
		'{adminemail}' => $_G['setting']['adminemail'],
		'{username}' => $_G['member']['username'],
		'{myname}' => $_G['member']['username']
	);
	$replaces = array_merge($sitevars, $replaces);
	return str_replace(array_keys($replaces), array_values($replaces), $string);
}

function clearcookies() {
	global $_G;
	foreach($_G['cookie'] as $k => $v) {
		if($k != 'widthauto') {
			dsetcookie($k);
		}
	}
	$_G['uid'] = $_G['adminid'] = 0;
	$_G['username'] = $_G['member']['password'] = '';
}

function crime($fun) {
	if(!$fun) {
		return false;
	}
	include_once libfile('class/member');
	$crimerecord = & crime_action_ctl::instance();
	$arg_list = func_get_args();
	if($fun == 'recordaction') {
		list(, $uid, $action, $reason) = $arg_list;
		return $crimerecord->$fun($uid, $action, $reason);
	} elseif($fun == 'getactionlist') {
		list(, $uid) = $arg_list;
		return $crimerecord->$fun($uid);
	} elseif($fun == 'getcount') {
		list(, $uid, $action) = $arg_list;
		return $crimerecord->$fun($uid, $action);
	} elseif($fun == 'search') {
		list(, $action, $username, $operator, $startime, $endtime, $reason, $start, $limit) = $arg_list;
		return $crimerecord->$fun($action, $username, $operator, $startime, $endtime, $reason, $start, $limit);
	} elseif($fun == 'actions') {
		return crime_action_ctl::$actions;
	}
	return false;
}
function checkfollowfeed() {
	global $_G;

	if($_G['uid']) {
		$lastcheckfeed = 0;
		if(!empty($_G['cookie']['lastcheckfeed'])) {
			$time = explode('|', $_G['cookie']['lastcheckfeed']);
			if($time[0] == $_G['uid']) {
				$lastcheckfeed = $time[1];
			}
		}
		if(!$lastcheckfeed) {
			$lastcheckfeed = getuserprofile('lastactivity');
		}
		dsetcookie('lastcheckfeed', $_G['uid'].'|'.TIMESTAMP, 31536000);
		$followuser = C::t('home_follow')->fetch_all_following_by_uid($_G['uid']);
		$uids = array_keys($followuser);
		if(!empty($uids)) {
			$count = C::t('home_follow_feed')->count_by_uid_dateline($uids, $lastcheckfeed);
			if($count) {
				notification_add($_G['uid'], 'follow', 'member_follow', array('count' => $count, 'from_id'=>$_G['uid'], 'from_idtype' => 'follow'), 1);
			}
		}
	}
	dsetcookie('checkfollow', 1, 30);
}

function checkemail($email) {
	global $_G;

	$email = strtolower(trim($email));
	if(strlen($email) > 32) {
		//Email 地址无效
		showmessage('profile_email_illegal', '', array(), array('handle' => false));
	}
	if($_G['setting']['regmaildomain']) {
		$maildomainexp = '/('.str_replace("\r\n", '|', preg_quote(trim($_G['setting']['maildomainlist']), '/')).')$/i';
		if($_G['setting']['regmaildomain'] == 1 && !preg_match($maildomainexp, $email)) {
			//抱歉，Email 包含不可使用的邮箱域名
			showmessage('profile_email_domain_illegal', '', array(), array('handle' => false));
		} elseif($_G['setting']['regmaildomain'] == 2 && preg_match($maildomainexp, $email)) {
			//抱歉，Email 包含不可使用的邮箱域名
			showmessage('profile_email_domain_illegal', '', array(), array('handle' => false));
		}
	}

	loaducenter();
	$ucresult = uc_user_checkemail($email);

	if($ucresult == -4) {
		//Email 地址无效
		showmessage('profile_email_illegal', '', array(), array('handle' => false));
	} elseif($ucresult == -5) {
		//抱歉，Email 包含不可使用的邮箱域名
		showmessage('profile_email_domain_illegal', '', array(), array('handle' => false));
	} elseif($ucresult == -6) {
		//该 Email 地址已被注册
		showmessage('profile_email_duplicate', '', array(), array('handle' => false));
	}
}

function make_getpws_sign($uid, $idstring) {
	global $_G;
	$link = "{$_G['siteurl']}member.php?mod=getpasswd&uid={$uid}&id={$idstring}";
	return dsign($link);
}


/**
 * @author goodspb
 * @desc 使用远程图片设置头像
 */
function setAvatarFromRemoteUrl($url,$uid,$type=1){
	if($url==''){return FALSE;}
	if(!$uid){return FALSE;}
	global $_G;
	//获取头像的方式
	if($type){
		$ch=curl_init();
		$timeout=10;
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
		$img=curl_exec($ch);
		curl_close($ch);
	}else{
		ob_start();
		readfile($url);
		$img=ob_get_contents();
		ob_end_clean();
	}
	
	define('UC_DATADIR', DISCUZ_ROOT . 'uc_server/data/');
	define('UC_DATAURL', DISCUZ_ROOT . 'uc_server/data');
	//保存临时头像
	$avatar_temp_url = UC_DATAURL.'/tmp/upload'.$uid.'.jpg';
	if(file_exists($avatar_temp_url)){
		@unlink($avatar_temp_url);
	}
	$fp2=@fopen($avatar_temp_url,'a');
	fwrite($fp2,$img);
	fclose($fp2);
	
	require_once libfile('function/app');
	moveAvatar($uid,$avatar_temp_url);
	
	return TRUE;
}

function getProfiles($uid = null) {

    global $_G;
    $uid = (int)$uid;

    // 如果是自己
    if ( isset($_G['uid']) && $_G['uid'] === $uid ) {
        return $_G['member'];
    }

    //
    $profile = C::t('common_member_profile')->fetch_by_pk($uid);
    $profile = array_merge($profile, C::t('common_member') -> fetch($uid));
    return $profile;
}

/**
 * 获取用户组
 * @return Array
 */
function getUserGroups() {

    global $_G;

    if ( isset($_G['cache']['usergroups']) ) {
        return $_G['cache']['usergroups'];
    }

    loadcache('usergroups');

    if ( isset($_G['cache']['usergroups']) ) {
        return $_G['cache']['usergroups'];
    }

    return array();
}

/**
 * @name 获取点赞总数
 * @param Int       $uid 用户ID
 * @param string    $type 点赞类型
 *   点赞类型的可选值：
 *     forum    论坛
 *     thread   主题
 *     post     评论
 */
function getSaygoodTotal($uid, $type = 'all') {

    $forum_saygood_total = 0;

    $thread_saygood_total = 0;
    if ($type = 'all' || $type = 'thread' ) {
        $thread_saygood_total = C::t('home_saygood')->count_by_uid_idtype($uid, 'tid');
        if ($type = 'thread') {
            return $thread_saygood_total;
        }
    }

    $post_saygood_total = 0;
    if ($type = 'all' || $type = 'post' ) {
        $post_saygood_total = C::t('forum_hotreply_member')->count_by_uid($uid);
        if ($type = 'post') {
            return $post_saygood_total;
        }
    }

    return $forum_saygood_total + $thread_saygood_total + $post_saygood_total;

}

include_once libfile('function/product');

function getBuyedProductSerieCount($uid, $series) {

    // 查询用户下过的订单的编号
    $sql = sprintf("SELECT oid FROM %s WHERE uid=%d AND state=6",
        DB::table(table_common_member_order::$table_name), $uid
    );
    $oids_record = DB::fetch_all($sql);

    // 没有任何订单
    if (!$oids_record) {
        return 0;
    }

    $oids = array();
    foreach ($oids_record as $v) {
        $oids[] = $v['oid'];
    }

    // 获取系列记录
    $product_series = C::t('common_product_series')->fetch_by_series($series);

    // 没有找到商品系列记录
    if (!$product_series) {
        return 0;
    }

    // 查询订单的详情记录
    $sql = sprintf("SELECT * FROM %s WHERE oid in (%s)",
        DB::table('common_member_order_detail'), implode(',', $oids)
    );
    $orders_detail_record = DB::fetch_all($sql);

    $count = 0;
    foreach ($orders_detail_record as $v) {

        // 获取在售商品记录
        $spid = $v['spid'];
        $product_onsell = getProductOnsellBySpid($spid);
        if (!$product_onsell) {
            continue;
        }

        // 获取商品
        $product = getProduct($product_onsell['pdid']);
        if (!$product) {
            continue;
        }

        // 获取商品的系列
        $series = C::t('common_product_series')->fetch_by_pk($product['psid']);
        if (!$series) {
            continue;
        }

        // 检查是否在指定的系列里面
        if ( $product_series['psid'] == $series['psid'] || table_common_product_series::isParents($product_series, $series) ) {
            $count += $v['amount'];
        }
    }

    return $count;

}

?>

