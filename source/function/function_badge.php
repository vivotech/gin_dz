<?php

require_once libfile('function/cache');

/**
 * 获取所有徽章记录
 * @return Array
 */
function getBadges() {
	global $_G;
	
	if (!isset($_G['cache']['badgeslist'])) {
		loadcache('badgeslist');
		if ( !isset($_G['cache']['badgeslist'] )) {
			updatecache('badgeslist');
		}
	}
	
	//var_dump($_G['cache']['badgeslist']);
	return $_G['cache']['badgeslist'];
}

/**
 * 通过ID获取徽章
 * @param Int  $id
 * @return Array|null
 */
function getBadgeById($id)
{
	$badges = getBadges();
	
	foreach ($badges as $k => $v)
	{
		if ($k==$id)
		{
			return $v;
		}
	}
	
	return null;
}

/**
 * @name 检查可以获取的徽章
 * @return Array
 */
function getCanObtainBadge($uid) {
	
	$badges = getBadges();
	
	$result = array();
	foreach ($badges as $badge) {
		$badge_level = checkBadgeCanObtain($badge, $uid);
		if ( $badge_level ) {
			$result[] = array(
				'badge'	=> $badge,
				'level'	=> $badge_level
			);
		}
	}
	
	return $result;
}

/**
 * @name 检查徽章是否可以获取
 * @return false|Int  不能获取返回 false，能获取则返回级别
 */
function checkBadgeCanObtain($badge, $uid) {
	
	switch ($badge['badge']) {
		// 签到达人
		case 'checkin_badge' :
			
			require_once DISCUZ_ROOT."/source/plugin/gsignin/table/table_gsignin_member.php";
			require_once DISCUZ_ROOT."/source/plugin/gsignin/function/function.php";
			
			// 获取签到总数
			$gsignin_member = C::t('gsignin_member')->fetch_by_uid($uid);
			if (!$gsignin_member) {
				return false;
			}
			$checkin_total = $gsignin_member['total'];
			
			// 检查用户可以获得的徽章级别
			$checkin_level = 0;
			for ($i=1; $i<=7; $i++) {
				$lv_limit = $badge['lv'.$i.'_limit'];
				if ( $lv_limit && $checkin_total >= $lv_limit ) {
					$checkin_level = $i;
				}
			}
			
			if (!$checkin_level) {		// 还没有到达最低的条件
				return false;
			} else {
				// 获取用户获得徽章记录
				$badge_member = C::t('badge_member')->fetch_by_badge_uid($badge['badge'], $uid);
				
				if ( $badge_member && $checkin_level <= $badge_member['lv'] ) {		// 还是原来那一个级别
					return false;
				}

				return $checkin_level;		// 可以获得
				
			}
			
			break;
		// 起泡酒达人
		case 'bubble_wine_badge' :
			return false;
			break;
		// 分享达人
		case 'share_badge' :
		
			$share_count = C::t("forum_thread")->count_by_authorid($uid);
			
			// 检查用户可以获得的徽章级别
			$share_level = 0;
			for ($i=1; $i<=7; $i++) {
				$lv_limit = $badge['lv'.$i.'_limit'];
				if ( $lv_limit && $share_count >= $lv_limit ) {
					$share_level = $i;
				}
			}
			
			if (!$share_level) {		// 还没有到达最低的条件
				return false;
			} else {
				// 获取用户获得徽章记录
				$badge_member = C::t('badge_member')->fetch_by_badge_uid($badge['badge'], $uid);
				
				if ( $badge_member && $share_level <= $badge_member['lv'] ) {		// 还是原来那一个级别
					return false;
				}

				return $share_level;		// 可以获得
			}
			
			return false;
			break;
	}
	
	return false;
	
}

/**
 * @name 获取会员获得的徽章记录
 * @param Int $uid  会员ID
 * @return Array
 */
function getMemberBadges($uid) {

    global $_G;

    if (isset($_G['member']['badges'])) {
        $badges = $_G['member']['badges'];
    } else {
        $badges = C::t('badge_member')->fetch_by_uid($uid);
        if ($badges) {
            $_G['member']['badges'] = $badges;
        } else {
            $_G['member']['badges'] = $badges = array();
        }
    }

    return $badges;
}

/**
 * @name 检查指定会员是否拥有某一个徽章
 * @param Int $uid  会员ID
 * @param Array|String  $badge 徽章记录数组或徽章ID
 * @return Bool
 */
function checkMemberHasBadge($uid, $badge) {

    // 获取徽章的ID
    $badge_id = is_array($badge) ? $badge['badge'] : $badge;

    // 获取会员徽章记录
    $member_badges = getMemberBadges($uid);

    foreach ($member_badges as $v) {

        if ( $v['badge'] == $badge_id ) {
            return true;
        }
    }

    return false;
}

