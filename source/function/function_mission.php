<?php


if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

//定义一下常量
define('RULE_SHARE',1);
define('RULE_SHARE_BUY',2);

function load_base_class(){
	$base_path = DISCUZ_ROOT.'./source/module/task/task_base.php';
	if(!file_exists($base_path)){
		throw new Exception('Class lost : task_base');
		exit();
	}
	require_once $base_path;
}

function get_task_types($id=null){
	$arr = array('product' => '商品分享任务');
	return $id===null ? $arr : $arr[$id];
}

function get_prize_types($id=null){
	$arr = array('积分礼品','优惠券礼品','实物礼品');
	return $id===null ? $arr : $arr[$id];
}

//加经验值
function task_add_progress($uid,$ruleArr,$taskUser){
	global $_G;
	if(!is_array($ruleArr)) return FALSE;
	$tid = $taskUser['tid'];
	$where = array('tid'=>$tid,'uid'=>$uid,'trid'=>$ruleArr['trid'],'task_uid'=>$taskUser['uid']);
	$item = C::t('task_rule_user')->fetch_condition($where);
	if($item){
		return FALSE;
	}
	$insertData = array(
		'tid' => $tid,
		'uid' => $uid,
		'trid'=> $ruleArr['trid'],
		'task_uid'=> $taskUser['uid'],
		'dateline'=>TIMESTAMP
	);
	$trid = C::t('task_rule_user')->insert($insertData,TRUE);
	if($trid){
		$task = C::t('task')->fetch($tid);
		$progress = intval($taskUser['progress']);
		$aval = intval($ruleArr['aval']);
		$left = intval($task['finish']) - $progress;
		$add = $left >= $aval ? $aval : $left;
		$now_progress = array('progress'=> $progress + $add );
		C::t('task_user')->update_by_uid_tid($taskUser['uid'], $tid , $now_progress);
	}
	return true;
}

function add_product_progress($uid){
	$trids = array();
	$list = get_rule_user_list($uid);
	foreach($list as $k=>$v){
		$task = get_task($v['tid']);
		if(isset($task['rules'][RULE_SHARE_BUY])){
			$had = FALSE;
			foreach( $list as $kk=>$vv ){
				if($vv['tid']==$v['tid'] && $vv['trid'] == $task['rules'][RULE_SHARE_BUY]['trid']){
					$had = TRUE;
				}
			}
			if(!$had){
				$trids[] = array(
						'tid' => $v['tid'],
						'trid'=> $task['rules'][RULE_SHARE_BUY]['trid'],
						'task_uid'=> $v['task_uid'],
					);
			}
		}
	}
	
	foreach($trids as $key=>$val){
		$rule = get_rule($val['trid']);
		$taskuser = get_task_user($val['tid'],$val['task_uid']);
		task_add_progress($uid,$rule,$taskuser);
	}
	
}

function get_rule_user_list($uid){
	return C::t('task_rule_user')->fetch_condition(array('uid'=>$uid));
}

function get_task_list(){
	return C::t('task')->fetch_all_list();
}

function get_task($tid){
	return C::t('task')->fetch($tid);
}

function get_task_user($tid,$uid){
	return C::t('task_user')->fetch_by_uid_tid($uid,$tid);
}

function get_rule($trid){
	return C::t('task_rule')->fetch($trid);
}

function get_rule_desc_by_actionid($actionid){
    switch($actionid){
        case 1: return '好友浏览'; break;
        case 2: return '好友购买了该商品'; break;
    }
    return false;
}

?>