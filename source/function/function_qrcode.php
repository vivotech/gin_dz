<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

function create_qrcode($datas = array(), $zipfilename = '', $tempdir = '', $extra = array()){
	if(!is_array($datas)){
		$datas = array($datas);
	}
	
	global $_G;
	$qrcode_url = $_G['siteurl'].'qrcode.php?data=';
	if(empty($zipfilename)){
		$zipfilename = time().rand(1000, 9999).'.zip';
	}
	if(!is_dir($tempdir)){
		$tempdir = DISCUZ_ROOT.'data/qrcode/';
	}
	$CURL = new curl();
	$filenames = array();
	
	foreach($datas as $k=>$url){
		// 生成缓存文件
		$action_url = $qrcode_url.urlencode($url);
		$CURL->request_init($action_url);
		$return = $CURL->request_action();
		$filenames[$k] = md5($return.rand(1000, 9999)).'.png';
		file_put_contents($tempdir.$filenames[$k], $return);
	}
	
	// 打包成ZIP
	$ZIP = new ZipArchive();
	$zip_path = $tempdir.$zipfilename;
	if(file_exists($zip_path)){
		@unlink($zip_path);
	}
	if($ZIP->open($zip_path, ZipArchive::CREATE) !== TRUE){
		return $filenames;
	}
	
	$description = '';
	foreach($filenames as $k=>$filename){
		$thisfile = $tempdir.$filename;
		$ZIP->addFile($thisfile, $filename);
		$description .= 'Filename:'.$filename;
		if(!empty($extra[$k])){
			$description .= '=>'.$extra[$k];
		}
		$description .= "\n";
	}
	$ZIP->addFromString('Qrcode-Description.txt', $description);
	$ZIP->close();
	return $zip_path;
}
