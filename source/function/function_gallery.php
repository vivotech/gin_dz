<?php

function hasGallery($uid){
	global $_G;
	$uid = ($uid) ? $_G['uid'] : $uid;
	if(!$uid) return;
	
	$gallery = C::t('common_member_gallery')->fetch_by_uid($uid);
	if($gallery){
		return $gallery;
	}else{
		return FALSE;
	}
}

function createDefaultGallery($uid){
	global $_G;
	$uid = ($uid) ? $_G['uid'] : $uid;
	if(!$uid) return;
	
	$name = ($_G['config']['gallery']['defaultname']) ? $_G['config']['gallery']['defaultname'] : '我的图库';
	$insert_data = array(
		'uid'=>$uid,
		'name'=>$name,
		'gup'=>0,
		'dateline'=>TIMESTAMP,
		'isdefault'=>1
	);
	$gid = C::t('common_member_gallery')->insert($insert_data, 1);
	return ($gid > 0) ? $gid : FALSE;
}

