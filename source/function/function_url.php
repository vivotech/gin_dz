<?php

function createUrl($text, $params = array())
{
  $url = $text;
  if ( strpos('?', $url) < 0)
  {
    $url .= "?";
  }
  
  $param_string = "";
  foreach ($params as $k => $v)
  {
    if (strlen($param_string))
    {
      $param_string .= "&";
    }
    $param_string .= $k .'='.$v;
  }
  
  return $url .= $param_string;
}

/**
 * @name 重定向到一个地址
 * @param String $url  网址
 */
function redirect($url) {
	header('location: '.$url);
}

