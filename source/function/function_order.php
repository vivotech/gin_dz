<?php

/**
 *  订单方法
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

function sendOrderMessage($type='ordersure',$orderObj){
	global $_G;
	//微信模板消息
	if($_G['openid']){
		$openid = $_G['openid'];
		$wxt = new wechat_template();
		$wxt ->setOrder($orderObj);
		switch($type){
			case 'ordersure':
				$res = $wxt->sendOrderSure($openid);
				break;
			case 'orderpaysure':
				//$paystate = M('payment_wechat_state')->where("oid='$orderObj[oid]'")->find();
				$res = $wxt->sendOrderPaySure($openid,/*strtotime($paystate['time_end'])*/TIMESTAMP);
				break;
			case 'returnordersure':
				//退货申请
				$wxt->sendReturnOrderSure($openid);
				break;
			default:
				return FALSE;
		}
	}
	
	//手机短信
	
	return $res;
}

function get_paystate_name($paystate){
	switch(intval($paystate)){
		case 0: return '<font color="red">【未支付】</font>';break;
		case 1: return '<font color="green">【已支付】</font>';break;
		case 2: return '<font color="blue">【已退款】</font>';break;
	}
}

function get_payment($pid = 0){
	global $_G;
	if(!$_G['cache']['payment']){
		loadcache('payment');
	}
	$payments = $_G['cache']['payment'];
	return (isset($payments[$pid]) && $pid != 0) ? $payments[$pid] : $payments;
}

function get_payment_name($payment){
	$payment = get_payment($payment);
	return $payment['payname'];
}

/**
 * @name 获取订单状态名称
 */
function get_state_name($state){
	require_once libfile('function/admincp');
	$state = intval($state);
	if($state > -4 && $state < 7){
		if($state == 6){
			$return = '<span style="color:'.cplang('colornormal').'">'.cplang('product_order_state_'.$state).'</span>';
		}else{
			$return = cplang('product_order_state_'.$state);
		}
	}else{
		$return = cplang('product_order_state_default');
	}
	return $return;
}

/**
 * @name 获取订单状态对应的信息
 */
function get_state_message($state, $ecid = 0, $express_num = '',$oid = 0){
  $text = '';
  switch ($state)
  {
    case 0 :
      $text .= '下单成功';
      break;
    case 1 :
      $text .= '已审核，正在进行商品调度';
      break;
    case 2 :
      $text .= '商品已打好包，正在安排出库';
      break;
    case 3 :
      $text .= '商品已出库，正在安排投递';
      break;
    case 4 :
	  global $_G;
	  if(!$_G['cache']['express_company'])
	  {
	  	loadcache('express_company');
	  }
	  if(!empty($_G['cache']['express_company'][$ecid]))
	  {
	  	$text .= '快件已发出，请耐心等候，承运快递：'.$_G['cache']['express_company'][$ecid]['ecname'].'，快递单号：<a href="'.U('forum/order/express',array('oid'=>$oid)).'" style="color: #0033CC;text-decoration: underline;">'.$express_num.'</a>';
	  }
	  else
	  {
	  	$text .= '快件已发出，请耐心等候';
	  }
      break;
    case 5 :
      $text .= '快件开始派送，请准备收货';
      break;
    case 6 :
      $text .= '已完成配送，欢迎您再次光临！';
      break;
    case -1 :
      $text .= '退货申请中,请等待专员为您服务';
      break;
    case -2 :
      $text .= '退货完成';
      break;
    case -3 :
      $text .= '退货失败';
      break;
  }
  return $text;
}

/**
 * @name 获取取消订单类型名称
 */
function get_cancel_type_name($type){
	$type = intval($type);
	$return = '';
	switch($type){
		case -1 :
			$return = '其他原因';
			break;
		case 0 :
			$return = '重复下单';
			break;
		case 1 :
			$return = '现在不想购买';
			break;
		case 2 :
			$return = '收货人信息有误';
			break;
		case 3 :
			$return = '无法支付';
			break;
		default:
			$return = '其他原因';
	}
	return $return;
}

/**
 * @name 更新订单状态
 */
function change_state($oid , $state){
	$tostate = intval($state);
	$oid = intval($oid);
	
	$order = C::t('common_member_order')->fetch($oid);
	$oldstate = intval($order['state']);
	if($oldstate >= $tostate && $tostate > 0){
		return false;
	}
	
	for($i = $oldstate + 1; $i <= $tostate; $i++){
		insert_state_record($oid, $i);
	}
	
	if($tostate < 0){
		insert_state_record($oid, $tostate);
	}
	
	$update_data = array('state'=>$tostate);
	$affect_rows = C::t('common_member_order')->update($oid, $update_data);
	return ($affect_rows > 0) ? true : false;
}

/**
 * @name 插入状态变换记录
 */
function insert_state_record($oid, $state){
	global $_G;
	if($state == 5 || $state == 6){
		$operator = 0;
	}else{
		$operator = $_G['uid'];
	}
	$insert_data = array(
		'oid'=>$oid,
		'uid'=>$operator,
		'dateline'=>time(),
		'state'=>$state,
		'message'=>get_state_message($state)
	);
	$sid = C::t('common_member_order_state')->insert($insert_data, 1);
	return $sid;
}

/**
 * @name 获取订单所有详细信息
 */
function getOrderAllInfo($oid, $uid)
{
	global $_G;
	//直接使用oid进行查找
	$order = C::t('common_member_order')->fetch($oid);
	if (!$order || $order['uid']!=$uid){
		return false;
	}
	return getOrderOtherInfo($order);
}

/**
 * @name 获取订单其他信息
 */
function getOrderOtherInfo($order){
	global $_G;
	
	if(!$_G['cache']['express_company']){
  		loadcache('express_company');
	}
	
	$res = array();
	
	$res['express_company'] = $_G['cache']['express_company'][$order['express']];
  
	// get order state record
	$res['stateTable'] = C::t('common_member_order_state')->fetch_all_by_oid($order['oid']);
  
	// get product
	$res['productTable'] = C::t('common_product')->fetch_all_by_oid($order['oid']);
  	foreach($res['productTable'] as $k=>$v){
  		$res['productTable'][$k]['media_list'] = json_decode(htmlspecialchars_decode($v['media_list']),TRUE);
  	}
	// get user address
	$res['address'] = C::t('common_member_address')->fetch_by_uaid($order['uaid']);
	
	return array_merge($order,$res);
}

/**
 * @name 创建订单号
 */
function createOrderOpenId($oid){
	$oid = 1888888888 + intval($oid);
	return $oid;
}

/**
 * @name 快速获取积分单位
 */
function getCreditstransTitle(){
	global $_G;
	return $_G['setting']['extcredits'][$_G['setting']['creditstrans']]['title'];
}

/**
 * @name 快速获取价格
 * @param float $rmb 人民币价格
 * @param float $credit 积分价格
 * @param string $seperater 拼接字符（当2种价格同时存在的时候）
 * @return string 拼接
 */
function getPriceInOrderDetail($rmb = 0.00 ,$credit = 0.00,$seperater='<br>&<br>',$resident = FALSE){
	$price = '';
	if((!empty($rmb) && $rmb != 0) || $resident == TRUE){
		$price .= '￥&nbsp'.$rmb;
	}
	if((!empty($credit) && $credit != 0 && !empty($rmb) && $rmb != 0) || $resident == TRUE){
		$price .= $seperater;
	}
	if((!empty($credit) && $credit != 0) || $resident == TRUE){
		$price .= $credit.'&nbsp'.getCreditstransTitle();
	}
	
	return $price;
}

/**
 * @name 检查订单快递情况
 */
function check_order_express($oid, $ecid, $express_num){
	global $_G;
  	if(!$_G['cache']['express_company']){
  		loadcache('express_company');
  	}
	$express_com = $_G['cache']['express_company'][$ecid]['apiname'];
  	$express_state = intval(Express::query_express($express_com, $express_num));
	if($express_state == 2){
		change_state($oid , 5);
	}else if($express_state == 3){
		change_state($oid , 6);
	}
	return $express_state;
}

/**
 * @name 插入订单函数
 * @param int $address 收货地址id
 * @param int $payment 付款方式id
 * @param array $newspid 商品上架ID
 * @param array $newnum 购买商品数量
 * @param string $newnote 备注
 * @param int $deliver_time 送货时间
 * @return bool
 */
function order_add($address, $payment , $newspid, $newnum, $newnote ,$deliver_time = 2){
	global $_G;
	require_once libfile('function/product');
	// 合并订单还是新增订单
	$product_onsell = array();
	$isBuyable = array();
	$isBuyableCount = 0;
	$goodcost = 0;
	$expresscost = 0;
	$totalcredit = 0;
	$totalprice = 0;
	$totalprofit = 0;
	
	// 判断限时限量情况,积分情况
	if(count($newspid) > 0){
		foreach($newspid as $k=>$spid){
			if($newnum[$k] > 0){
				if((isBuyable($spid, $newnum[$k]) === 0)){
					$product_onsell[$k] = C::t('common_product_onsell')->fetch($spid);
					// 计算售价
					if($product_onsell[$k]['discount'] == 1){
						$product_onsell[$k]['discount_price'] = getDiscountPrice(0, $product_onsell[$k]['price'], 0);
					}else if($product_onsell[$k]['discount'] == 0){
						$product_onsell[$k]['discount_price'] = getDiscountPrice($spid, $product_onsell[$k]['price'], $product_onsell[$k]['discounttype']);
					}else{
						$product_onsell[$k]['discount_price'] = $product_onsell[$k]['price'];
					}
					
					if($product_onsell[$k]['type'] == 1){
						$product_onsell[$k]['total'] = 0;
						$product_onsell[$k]['credit'] = $product_onsell[$k]['discount_price'];
						$product_onsell[$k]['discount_price'] = 0;
						$product_onsell[$k]['totalcredit'] = $product_onsell[$k]['credit'] * $newnum[$k];
						$product_onsell[$k]['profit'] = 0;
						$totalcredit += $product_onsell[$k]['totalcredit'];
					}else{
						$product_onsell[$k]['credit'] = 0;
						$product_onsell[$k]['totalcredit'] = 0;
						$product_onsell[$k]['total'] = $product_onsell[$k]['discount_price'] * $newnum[$k];
						// 计算利润 
						$product_onsell[$k]['profit'] = ($product_onsell[$k]['discount_price'] - $product_onsell[$k]['cost']) * $newnum[$k];
						$goodcost += $product_onsell[$k]['total'];
					}
					
					$totalprofit += $product_onsell[$k]['profit'];
					
					$isBuyable[$k] = true;
					$isBuyableCount++;
				}else{
					$isBuyable[$k] = false;
				}
			}else{
				continue;
			}
		}
	}
	$totalprice = $goodcost + $expresscost;
	// 判断用户是否够分买全部的商品
	if($totalcredit > 0){
		$member = C::t('common_member_count')->fetch($_G['uid']);
		if($member['extcredits'.$_G['setting']['creditstrans']] < $totalcredit){
			$isBuyableCount = 0;
		}
	}

	// 插入订单
	if($isBuyableCount > 0){
		//订单事务开始
		DB::begin();
		
		// 新增订单
		$oid = insert_order($address,$newnote,$payment,$goodcost,$expresscost,$totalcredit,$totalprice,$totalprofit ,$deliver_time);
		
		if(!$oid){
			DB::rollback();
			return FALSE;
		}
		
		if($oid > 0){
			// 返点统计
			define('DEALER_SHOP_CODE_REBATE_RATE', 0.5);  // 返点率定义
			define('DEALER_SHOP_AREA_REBATE_RATE', 0.2);  // 返点率定义
			$total_rebate = 0;
			$sids = array();
			// 获取地址经销商归属
			$this_address = C::t('common_member_address')->fetch($address);
			$this_area_shop = C::t('dealer_shop_address')->fetch_by_provinceid_cityid($this_address['provinceid'], $this_address['cityid']);
			
			// 插入订单明细
			if(count($newspid) > 0){
				
				foreach($newspid as $k=>$spid){
					if($isBuyable[$k]){
						
						$this_odid = insert_order_detail_row($oid,$spid,$newnum[$k],$product_onsell[$k]);
						
						if(!$this_odid){
							DB::rollback();
							return FALSE;
						}
						// 返点给经销商
						// 查询经销商归属
						$this_relation = C::t('dealer_sale_relation')->fetch_by_uid_pdid($_G['uid'], $product_onsell[$k]['pdid']);
						$insert_data = array(
							'oid'=>$oid,
							'o_openid'=>$o_openid,
							'nickname'=>$_G['member']['nickname'],
							'pdid'=>$product_onsell[$k]['pdid'],
							'pdname'=>$product_onsell[$k]['subject'],
							'odid'=>$this_odid,
							'amount'=>$newnum[$k],
							'total'=>$product_onsell[$k]['total'],
							'profit'=>$product_onsell[$k]['profit'],
							'dateline'=>TIMESTAMP
						);
						if(!empty($this_relation['sid'])){
							$insert_data['sid'] = $this_relation['sid'];
							$insert_data['type'] = 0;
							$insert_data['rebate'] = $product_onsell[$k]['profit'] * DEALER_SHOP_CODE_REBATE_RATE;
						}else{
							$insert_data['sid'] = $this_area_shop['sid'];
							$insert_data['type'] = 1;
							$insert_data['rebate'] = $product_onsell[$k]['profit'] * DEALER_SHOP_AREA_REBATE_RATE;
						}
						// 插入经销商成交记录
						if(!C::t('dealer_shop_rebate')->insert($insert_data)){
							DB::rollback();
							return FALSE;
						}
					}
				}
			}
		
			
			// 插入状态记录
			$insert_data = array(
				'oid'=>$oid,
				'uid'=>$_G['uid'],
				'dateline'=>TIMESTAMP,
				'state'=>0
			);
			
			if(!C::t('common_member_order_state')->insert($insert_data)){
				DB::rollback();
				return FALSE;
			}
			// 设置默认地址
			$update_data = array(
				'lastusetime'=>TIMESTAMP
			);
			C::t('common_member_address')->update($address, $update_data);

			DB::commit();
			return $oid;
		}else{
			DB::rollback();
			return FALSE;
		}
	}else{
		return FALSE;
	}
}

/**
 * @param $address  地址ID
 * @param $newnote  留言
 * @param $payment  支付方式
 * @param $goodcost 商品价格
 * @param $expresscost  快递价格
 * @param $totalcredit 需要的总积分
 * @param $totalprice  需要的总价
 * @param $totalprofit 总利润？
 * @param $deliver_time 配送时间
 * @return bool 是否插入成功
 */

function insert_order($address,$newnote,$payment,$goodcost,$expresscost,$totalcredit,$totalprice,$totalprofit,$deliver_time){
	global $_G;
	
	$insert_data = array(
		'uid'=>$_G['uid'],
		'dateline'=>TIMESTAMP,
		'uaid'=>$address,
		'note'=>$newnote,
		'payment' => check_payment($payment),
		'paystate'=> 0,	//支付状态，当payment>0时有效
		'goodcost'=> $goodcost,
		'expresscost'=>$expresscost,
		'totalcredit'=>$totalcredit,
		'totalprice'=>$totalprice,
		'totalprofit'=>$totalprofit,
        'deliver_time'=> $deliver_time
	);
	$oid = C::t('common_member_order')->insert($insert_data, 1);
	
	if($oid > 0){
		// 生成订单号
		$o_openid = createOrderOpenId($oid);
		$update_data = array(
			'o_openid'=>$o_openid
		);
		C::t('common_member_order')->update($oid, $update_data);
	}else{
		//回滚操作
		DB::rollback();
		return FALSE;
	}
	return $oid;
}

/**
 * 插入一个订单商品详情
 */
function insert_order_detail_row($oid,$spid,$amount,$product_onsell){
	global $_G;
	$insert_data = array(
		'oid'=>$oid,
		'spid'=>$spid,
		'amount'=>$amount,
		'price'=>$product_onsell['price'],
		'discount'=>$product_onsell['discount_price'],
		'credit'=>$product_onsell['credit'],
		'totalcredit'=>$product_onsell['totalcredit'],
		'total'=>$product_onsell['total']
	);
	
	$this_odid = C::t('common_member_order_detail')->insert($insert_data, 1);
	if(!$this_odid){
		DB::rollback();
		return FALSE;
	}
	// 减扣可售卖数量
	$update_data = array(
		'amount'=>$product_onsell['amount'] + $amount
	);
	
	if(!C::t('common_product_onsell')->update($product_onsell['spid'], $update_data)){
		DB::rollback();
		return FALSE;
	}
	
	// 减扣用户分数
	if($product_onsell['type'] == 1){
		updatemembercount($_G['uid'], array($_G['setting']['creditstrans'] => '-'.$product_onsell['totalcredit']), false, 'PCC', $spid);
	}
	
	return $this_odid;
}

function check_payment($payment){
	$payments = get_payment();
	$keys = array_keys($payments);
	return in_array($payment,$keys) ? $payment : 1;
}

function getFullAddress($address,$level=5,$seperater=''){
	$result = $address['province'];
	if($level>1) $result .= $seperater.$address['city'];
	if($level>2) $result .= $seperater.$address['dist'];
	if($level>3) $result .= $seperater.$address['community'];
	if($level>4) $result .= $seperater.$address['address'];
	return $result;
}

/**
 * @name 手机我的订单显示模板
 */
function mobile_order_list_item_template(&$orderItem, $uid, $isreturn = false){
	$return = '';
	$orderAllInfo = getOrderAllInfo($orderItem['oid'], $uid);

	$return = '
	<div class="touch-orderlist-box" data-oid="'.$orderItem['oid'].'">
		<div class="touch-order-info">
			<p class="order-state">'.get_state_name($orderAllInfo['state']).'</p>
			<p class="order-num">订单号：'.$orderAllInfo['o_openid'].'</p>
		</div>
		<ul class="touch-order-pdlist">';
	foreach($orderAllInfo['productTable'] as $p){
		$return .= mobile_order_list_product_template($p);
	}

    $return .= '
		</ul>
		<div class="touch-order-detail">
			<!--<p class="touch-order-date">'.custom_date($orderAllInfo['dateline']).'</p>-->
			<p class="touch-order-total">实付金额:<span class="data">￥<span class="bigprize">'.$orderAllInfo['totalprice'].'</span></span></p>
			<div class="touch-order-btn-list">';
    //当订单状态为未完成时，且为货到付款或在线付款的已付款状态时
    if($orderAllInfo['state']<6 && ( ( $orderAllInfo['payment'] > 1 && $orderAllInfo['paystate'] > 0 ) || $orderAllInfo['payment'] == 1  )  ){
        $return .= '<button class="order-btn" type="button" onclick="location.href=\''.U('forum/order/comment',array('oid'=>$orderItem['oid'])).'\';">晒订单</button>';
        if($orderAllInfo['expresscom'] > 0 && !empty($orderAllInfo['expresscode'])){
            $return .= '<button class="order-btn getexpress" type="button" data-oid="'.$orderAllInfo['oid'].'" >查看物流</button>';
        }
    }else{
        //当未支付的之后
        $return .=  '<button class="order-btn order-btn-pay" data-oid="'.$orderItem['oid'].'">立刻支付</button> &nbsp;&nbsp; <button class="order-btn order-btn-cancle" onclick="location.href=\''.U('forum/order/cancelorder',array('oid'=>$orderAllInfo['oid'])).'\'">取消订单</button>';
    }

    $return .= '<!--<button class="order-btn" type="button" >联系我们</button>-->
			</div>
		</div>
	</div>';


	if($isreturn){
		return $return;
	}else{
		echo $return;
	}
}

/**
 * @name 手机我的订单,产品显示模板
 */
function mobile_order_list_product_template($p){
	$return = '
		<li class="touch-order-pd">
			<div class="touch-order-pd-img">
				<img class="pd-img" src="'.$p['media_list'][0][media_url].'?imageView2/1/w/78/h/78">
			</div>
			<div class="touch-order-pd-detail">
				<p class="touch-order-pd-name">'.$p[name].'</p>
				<p class="touch-order-pd-num">￥'.$p[discount].'元 x'.$p[amount].$p[unit].'</p>
			</div>
			<div style="clear:both;"></div>
		</li>';
	return $return;
}

/**
 * @name 返回取消订单原因
 */
function get_order_cancel_reason(){
	return '<option value="0">重复下单</option>
		  	<option value="1">现在不想购买</option>
		  	<option value="2">收货人信息有误</option>
		  	<option value="3">无法支付</option>
		  	<option value="-1">其他原因</option>';
}

/**
 * 清空购物车
 */
function clear_cart($uid=0){
	global $_G;
	$uid = $uid==0 ? $_G['uid'] : $uid;
	C::t('common_member_shopcart')->delete_by_uid($uid);
}

function get_deliver_time($id=null){
    $arr = array('仅在工作日配送','仅在双休日、假日配送','工作日、双休日与假日均可配送');
    return isset($arr[$id]) && $id!=null ? $arr[$id] : $arr;
}

?>