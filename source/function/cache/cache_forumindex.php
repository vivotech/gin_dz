<?php

/**
 * 首页版面缓存
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

function build_cache_forumindex(){
	$list = C::t("forum_index")->fetch_list();
	savecache('forumindex', $list);
}