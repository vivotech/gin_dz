<?php

/**
 * 支付方式
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

function build_cache_payment() {
	$data = array();
	$sql  = "SELECT * FROM ".DB::table('common_payment');
	$list = DB::fetch_all($sql);
	foreach($list as $key=>$val){
		$data[$val['pid']] = $val;
	}
	savecache('payment', $data);
}

?>