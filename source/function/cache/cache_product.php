<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: cache_seriesstick.php 24152 2011-08-26 10:04:08Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

function build_cache_product() {
	$data = array();
	$productlist = C::t('common_product')->fetch_all_data();

	$data = array();
	if(!empty($productlist)) {
		foreach($productlist as $k => $product){
			$data[$product['pdid']] = $product;
		}
	}

	savecache('product', $data);
}
?>