<?php

/**
 * 保存banner图
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

function build_cache_forum_banner_ad(){
	$list = C::t('forum_banner_ad')->fetch_all();
	savecache('forum_banner_ad', $list);
}

?>