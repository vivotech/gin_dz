<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: cache_seriesstick.php 24152 2011-08-26 10:04:08Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

function build_cache_productseries() {
	$data = array();
	$productseries = C::t('common_product_series')->fetch_all();
	$serieslist = array();

	$seriesnoperms = array();
	foreach($productseries as $val) {
		$series = array(
			'psid' => $val['psid'],
			'type' => $val['type'],
			'series' => $val['series'],
			'psup' => $val['psup'],
			'status' => $val['status'],
			'displayorder' => $val['displayorder']
		);
		$series['orderby'] = bindec((($series['simple'] & 128) ? 1 : 0).(($series['simple'] & 64) ? 1 : 0));
		$series['ascdesc'] = ($series['simple'] & 32) ? 'ASC' : 'DESC';
		$series['extra'] = unserialize($series['extra']);
		if(!is_array($series['extra'])) {
			$series['extra'] = array();
		}

		if(!isset($serieslist[$series['psid']])) {
			if($series['psup']) {
				$serieslist[$series['psup']]['count']++;
			}
			$serieslist[$series['psid']] = $series;
		}
	}

	$data = array();
	if(!empty($serieslist)) {
		foreach($serieslist as $psid1 => $series1) {
			if(($series1['type'] == 'group' && $series1['count'])) {
				$data[$psid1] = $series1;
				foreach($serieslist as $psid2 => $series2) {
					if($series2['psup'] == $psid1 && $series2['type'] == 'series') {
						$data[$psid2] = $series2;
						foreach($serieslist as $psid3 => $series3) {
							if($series3['psup'] == $psid2 && $series3['type'] == 'sub') {
								$data[$psid3] = $series3;
							}
						}
					}
				}
			}
		}
	}
	
	savecache('productseries', $data);
}
?>