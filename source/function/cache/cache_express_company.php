<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: cache_domainwhitelist.php 24152 2011-08-26 10:04:08Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

function build_cache_express_company() {

	$data = C::t('common_express_company')->fetch_all_table();
	$companys = array();
	foreach($data as $d){
		$companys[$d['ecid']] = $d;
	}
	savecache('express_company', $companys);
}

?>