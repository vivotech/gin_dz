<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: cache_advs.php 30323 2012-05-22 09:33:36Z monkey $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

function build_cache_forumactivity() {
	$activities = C::t('forum_custom_activity')->fetch_all_activity(' WHERE closed = 0');
	$data = array();
	if(count($activities) > 0){
		foreach($activities as $k=>$v){
			if($v['spid'] > 0){
				$this_onsell_product = C::t('common_product_onsell')->fetch($v['spid']);
				$v['peoplelimit'] = floor($this_onsell_product['totalitems'] / $this_onsell_product['limit']);
				$v['credits'] = $this_onsell_product['price'];
			}else{
				$v['peoplelimit'] = 0;
				$v['credits'] = 0;
			}
			
			$data[$v['fid']] = $v;
		}
	}
	savecache('forumactivity', $data);
}