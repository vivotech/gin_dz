<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: cache_seriesstick.php 24152 2011-08-26 10:04:08Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

function build_cache_productonsell() {
	$data = array();
	$sql = 'SELECT sp.* FROM '.DB::table('common_product_onsell').' sp WHERE sp.closed = 0';
	$query = DB::query($sql);
	while($sp = DB::fetch($query)){
		$data[$sp['spid']] = $sp;
	}
	
	savecache('productonsell', $data);
}