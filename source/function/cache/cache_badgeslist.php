<?php


function build_cache_badgeslist() {

	$badges_list = C::t('badge_rule')->fetch_all_data();

	$data = array();
	if(!empty($badges_list)) {
		foreach($badges_list as $badge){
			$data[$badge['badge']] = $badge;
		}
	}

	savecache('badgeslist', $data);
}

