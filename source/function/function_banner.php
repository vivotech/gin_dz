<?php

function getBanners($type = 'index',$limit = 0, $orderType = '', $getDetail = false)
{
  $threadList = C::t('forum_banner_ad')->fetch_all($type,$limit, $orderType);
  if($getDetail){
  	$tids = array();
  	foreach($threadList as $k=>$thread){
  		$tids[] = $thread['tid'];
  	}
	$threads = C::t('forum_thread')->fetch_all($tids);
	foreach($threadList as $k=>$thread){
		$threads[$thread['tid']]['dateline'] = custom_date($threads[$thread['tid']]['dateline']);
		$threads[$thread['tid']]['avatar'] = avatar($threads[$thread['tid']]['authorid']);
		$threadList[$k] = array_merge($thread, $threads[$thread['tid']]);
	}
  }
  return $threadList;
}

function getBannerByTid($tid)
{
  $table = new table_forum_banner_ad();
  return $table->fetch_row_by_tid($tid);
}

function getBannerByid($id){
	$table = new table_forum_banner_ad();
	return $table->fetch($id);
}

function updateBanner($data, $condition)
{
  $table = new table_forum_banner_ad();
  return $table->update($data, $condition);
}

function insertBanner($data)
{
  $table = new table_forum_banner_ad();
  return $table->insert($data);
}

function deleteBannerByid($id){
	$table = new table_forum_banner_ad();
	return $table->delete(intval($id));
}

