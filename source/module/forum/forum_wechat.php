<?php

//微信图文文章

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

$act_array = array(
	'article' , //文章展示
);

$act = isset($_GET['action']) ? ( in_array(trim($_GET['action']),$act_array) ? trim($_GET['action']) : 'article' )  : 'article';

if($act == 'article'){
	$newsid = I('newsid',0,'intval');
	$item = M('wechat_reply_image')->find($newsid);
	if(!$item){
		showmessage('没有该文章！');
	}
	$item['content'] = htmlspecialchars_decode($item['content']);
	include template('forum/wechat_article');
}