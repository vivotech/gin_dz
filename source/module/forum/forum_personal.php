<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: forum_guide.php 34066 2013-09-27 08:36:09Z nemohou $
 */

if (!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

//可以通过nickname来查找用户资料
$nickname = I('nickname', '');
if ($nickname != '' && !empty($nickname)) {
	if ($_uid = getuidbynickname($nickname)) {
		$_GET['uid'] = $_uid;
	} else {
		showmessage('没有该用户');
	}
}

// 载入库
require_once libfile('function/url');

$uid = I('uid', $_G['uid'], 'intval');
$pagesize = 15;
$start = (empty($_G['page'])) ? 0 : ($_G['page'] - 1) * $pagesize;
$action = I('action', 'personal');

if (!$uid) {
	dheader('location:/member.php?mod=logging&action=login');
}
// 个人信息
if ($_G['uid'] == $uid) {
	//如果是自己，就不用查数据库了
	$profile = $_G['member'];
	$_G['personal_bg'] = $_G['member']['personal_bg'];

	//检查是否已经绑定手机
	$binded_mobile = empty($profile['mobile'])||$profile['mobile']=='' ? FALSE : TRUE;
} else {

    // 获取个人资料
	$profile = C::t('common_member_profile')->fetch_by_pk($uid);
    if (!$profile)
    {
        // 没有该用户时转到首页
        $url = '/forum.php';
        redirect($url);
    }

	$_G['personal_bg'] = $profile['personal_bg'];

    // 获取帐号资料
	$profile = array_merge($profile, C::t('common_member') -> fetch($uid));
}



// 获取等级
$profile['group'] = $_G['group'];
// 获取对应的积分
//$profile['extcredit'] = C::t('common_member_count') -> fetch($uid);


$filter = I('filter', 'all', '', array('all', 'digest', 'top'));

//个人帖子
$threads = C::t('forum_thread') -> fetch_all_by_authorid_displayorder($uid, $filter == 'top' ? 0 : null, '>', null, '', $start, $pagesize, null, null, '', $filter == 'digest' ? 1 : null);
foreach ($threads as $k => $thread) {
	$threads[$k]['media_list'] = json_decode(htmlspecialchars_decode($threads[$k]['media_list']), 1);
	$threads[$k]['first_img'] = $threads[$k]['media_list'][0]['media_url'] ? $threads[$k]['media_list'][0]['media_url'] : '';
	unset($threads[$k]['media_list']);
}
// 分页条
$threads_all_count = C::t('forum_thread') -> count_by_fid_authorid_displayorder($uid, $filter == 'top' ? 0 : null, '>', null, '', null, null, '', $filter == 'digest' ? 1 : null);
$theurl = 'forum.php?mod=personal&uid=' . $uid;
$pagecount = @ceil($threads_all_count/$pagesize);
$multipage = multi($threads_all_count, $pagesize, $_G['page'], $theurl, $_G['setting']['threadmaxpages']);

// 订单信息，只在手机和是自己页面的时候显示
//if(IN_MOBILE && $_G['uid']==$uid){
//	$myorder_count_waitpay = C::t('common_member_order')->count_where(' WHERE uid = '.$uid.' AND '.table_common_member_order::get_state_where_condition('waitpay')); // 待付款
//	$myorder_count_waitdelivery = C::t('common_member_order')->count_where(' WHERE uid = '.$uid.' AND '.table_common_member_order::get_state_where_condition('waitdelivery')); // 待发货
//	$myorder_count_delivering = C::t('common_member_order')->count_where(' WHERE uid = '.$uid.' AND '.table_common_member_order::get_state_where_condition('delivering')); // 已发货
//	$myorder_count_finished = C::t('common_member_order')->count_where(' WHERE uid = '.$uid.' AND '.table_common_member_order::get_state_where_condition('finished')); // 已完成
//}

//经验槽操作，当不为管理员才出现
if($_G['member']['adminid']==0){
    $next_group = array();
    $usergroups = get_groups();
    foreach($usergroups as $key=>$val){
        if($key==$_G['member']['groupid']){
            $next_group = current($usergroups);
            break;
        }
        next($usergroups);
    }
}

// AJAX循环帖子
if ($_GET['ajax'] == 1) {
	$result = array();
	$result['result'] = empty($threads) ? 0 : 1;
	if($result['result'] == 0){
		$result['msg'] = '翻到最底了哦!';
	}else{
		$result['list'] = array();
		foreach($threads as $v){
			$result['list'][] = $v;
 		}
	}
	echo json_encode($result, JSON_UNESCAPED_UNICODE);
	exit();
}

// 获取今日签到记录
require_once DISCUZ_ROOT."/source/plugin/gsignin/table/table_gsignin_member.php";
$member_gsignin_today = C::t('gsignin_member')->fetch_first_by_uid_today($_G['uid']);

include template('forum/personal');

