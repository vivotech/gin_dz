<?php
/**
 *  交易模块
 */
if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

//加载库文件
require_once libfile('function/product');
require_once libfile('function/order');

$act_array = array(
  'showmycart',
  'ajaxmycartlist',
  'ajaxmycartadd',
  'ajaxmycartupdate',
  'ajaxmycartdelete',
  'ajaxclearcar',
  'buy',			//下单动作
  'confirm',		//确认订单
  'done',			//下单完成
  'detail',   		// 订单详情,
  'myorder',		// 我的订单
  'cart',			// 购物车
  'cancelorder',	// 取消订单界面
  'state',			//订单支付状态查询
  'pay',			//发起支付
  'comment',		//晒订单，发测评文章
  'express',        //查询快递
);
$act = isset($_GET['action']) ? ( in_array(trim($_GET['action']),$act_array) ? trim($_GET['action']) : 'buy' )  : 'buy';

// 我的购物车
if($act == 'showmycart'){
	if($_G['uid'] > 0){
        $cart_count = C::t('common_member_shopcart')->count_all_by_uid($_G['uid']);
		include template('forum/mycart');
	}else{
		header('Location: forum.php');
	}
	
}
// 列出购物车物品
elseif($act == 'ajaxmycartlist'){
	$uid = ($_G['uid']) ? $_G['uid'] : 0;
	if($uid > 0){
		$cart_product_list = C::t('common_member_shopcart')->fetch_all_by_uid($uid);
		ajax_return(1,'', $cart_product_list);
	}else{
		ajax_return(0, '未登录');
	}
	exit;
}
// 向购物车添加商品
elseif($act == 'ajaxmycartadd'){
	$spid = $_GET['spid'] > 0 ? intval($_GET['spid']) : 0;
	$num = $_GET['num'] > 0 ? intval($_GET['num']) : 0;
	$uid = ($_G['uid']) ? $_G['uid'] : 0;
	$isok = true;
	
	if($uid > 0 && $spid > 0 && $num > 0){
		require_once libfile('function/product');
		// 检测自己的购物车是否有这个东西
		$productonsell_incart = C::t('common_member_shopcart')->fetch_by_uid_spid($uid, $spid);
		if($productonsell_incart['amount'] > 0){
			$newnum = $productonsell_incart['amount'] + $num;
			$code = isBuyable($spid, $newnum);
		}else{
			$code = isBuyable($spid, $num);
		}
		
		if($code === 0){
			if($productonsell_incart['amount'] > 0){
				$update_data = array(
					'amount'=>$newnum,
					'total'=>$productonsell_incart['discount'] * $newnum
				);
				$affectrows = C::t('common_member_shopcart')->update_by_uid_spid($uid, $spid, $update_data);
				if($affectrows > 0){
					ajax_return(1);
				}else{
					ajax_return(0, '修改没有影响到数据库');
				}
			}else{
				$productonsells = C::t('common_product_onsell')->fetch_detail($spid);
				if($productonsells[0]){
					$this_product = $productonsells[0];
					$this_product['media_list'] = getMediaList($this_product);
					$this_product['user_discount_price'] = getDiscountPriceByProductOnsell($this_product);
					
					$insert_data = array(
						'uid'=>$uid,
						'spid'=>$this_product['spid'],
						'type'=>$this_product['type'],
						'pic'=>$this_product['media_list'][0]['media_url'],
						'name'=>$this_product['name'],
						'amount'=>$num,
						'price'=>$this_product['costprice'],
						'discount'=>$this_product['user_discount_price'],
						'total'=>$this_product['user_discount_price'] * $num,
						'dateline'=>TIMESTAMP
					);
					$res = C::t('common_member_shopcart')->insert($insert_data, 1);
					ajax_return(1);
				}else{
					ajax_return(0, '没有此商品');
				}
			}
		}else{
			ajax_return(0 , '无法购买，返回码:'.$code);
		}
	}else{
		ajax_return(0 , '非法参数');
	}
	exit;
}
// 从购物车更新商品
elseif($act == 'ajaxmycartupdate'){
	$spid = $_GET['spid'] > 0 ? intval($_GET['spid']) : 0;
	$uid = ($_G['uid']) ? $_G['uid'] : 0;
	$num = $_GET['num'] > 0 ? intval($_GET['num']) : 0;
	
	if($spid > 0 && $uid > 0 && $num > 0){
		$productonsell_incart = C::t('common_member_shopcart')->fetch_by_uid_spid($uid, $spid);
		$update_data = array(
			'amount'=>$num,
			'total'=>$productonsell_incart['discount'] * $num
		);
		$affectrows = C::t('common_member_shopcart')->update_by_uid_spid($uid, $spid, $update_data);
		if($affectrows > 0){
			ajax_return(1);
		}else{
			ajax_return(0, '对数据无产生影响');
		}
	}else{
		ajax_return(0 , '非法参数');	
	}
	exit;
}
// 从购物车删除商品
elseif($act == 'ajaxmycartdelete'){
	$spid = $_GET['spid'] > 0 ? intval($_GET['spid']) : 0;
	$uid = ($_G['uid']) ? $_G['uid'] : 0;
	
	if($spid > 0 && $uid > 0){
		$res = C::t('common_member_shopcart')->delete_by_uid_spid($uid, $spid);
		if($res){
			ajax_return(1);
		}else{
			ajax_return(0, '删除失败');
		}
	}else{
		ajax_return(0 , '非法参数');
	}
	exit;
}
//清空购物车
elseif($act == 'ajaxclearcar'){
    $uid = ($_G['uid']) ? $_G['uid'] : 0;
    $res = C::t('common_member_shopcart')->delete_by_uid($uid);
    if($res){
        ajax_return(1);
    }else{
        ajax_return(0, '删除失败');
    }
}
// 下单
elseif($act == 'buy'){
	$spid = $_GET['spid'];
	$num = (intval($_GET['num']) > 0) ? intval($_GET['num']) : 1;
	$from = !empty($_GET['from']) ? $_GET['from'] : '';
	$allow_from = array('cart');
	if((empty($spid) && !in_array($from, $allow_from)) || empty($_G['uid'])){
		showmessage('参数错误');
	}
	
	if($from == 'cart'){
		$products_incart = C::t('common_member_shopcart')->fetch_all_by_uid($_G['uid']);
		if(count($products_incart) > 0){
			$spids = array();
			$nums = array();
			foreach($products_incart as $k=>$p){
				$spids[$k] = intval($p['spid']);
				$nums[$k] = intval($p['amount']);
			}
		}else{
			showmessage('你的购物车没有商品');
		}
	}else{
		$spids = array($spid);
		$nums = array($num);
	}
	
	// 获取用户信息
	$address_list = C::t('common_member_address')->fetch_all_by_uid($_G['uid']);
	//设置默认address
	if($address_list && !isset($_G['cookie']['default_address_id'])){
		dsetcookie('default_address_id',$address_list[0]['uaid']);
		dsetcookie('default_address_name',$address_list[0]['name']);
		dsetcookie('default_address_phone',$address_list[0]['phone']);
		dsetcookie('default_address_address',getFullAddress($address_list[0]));
	}
	require_once libfile('function/profile');
	//获取系统地址
	$values = array(0, 0, 0, 0);
	$elems = array('provinceid', 'cityid', 'distid', 'communityid');
	$provice_list = showdistrict($values, $elems, 'selectdirectbox', 1, 'reside', true);
	
	$order_total = 0.0;
	$order_credit_total = 0.0;
	$product_count = 0;
	$product_onsells = array();
	foreach($spids as $k=>$spid){
		// 判断限时限量的方法
		$product_onsells[$k]['buyAbleNum'] = buyableNum($spid);
		
		// 获取产品信息
		$product_onsell = C::t('common_product_onsell')->fetch($spid);
		$product = C::t('common_product')->fetch($product_onsell['pdid']);
		if($product_onsell['discount'] == 0){
			$product_onsell['newprice'] = getDiscountPrice($product_onsell['spid'], $product_onsell['price'], $product_onsell['discounttype']);
		}elseif($product_onsell['discount'] == 1){
			$product_onsell['newprice'] = getDiscountPrice(0, $product_onsell['price'], 0);
		}else{
			$product_onsell['newprice'] = $product_onsell['price'];
		}
		
		$product_onsell['totalprice'] = $product_onsell['newprice'] * $nums[$k];
		$product['media_list'] = json_decode(htmlspecialchars_decode($product['media_list']), 1);
		$product_onsell['main_pic'] =  $product['media_list'][0]['media_url'];
		
		// 重新组合到数组
		$product_onsells[$k]['spid'] = $spid;
		$product_onsells[$k]['type'] = $product_onsell['type'];
		$product_onsells[$k]['name'] = $product['name'];
		$product_onsells[$k]['unit'] = $product['unit'];
		$product_onsells[$k]['newprice'] = $product_onsell['newprice'];
		$product_onsells[$k]['totalprice'] = $product_onsell['totalprice'];
		$product_onsells[$k]['media_list'] = $product['media_list'];
		$product_onsells[$k]['main_pic'] = $product_onsell['main_pic'];
		if($product_onsell['type'] == 0){
			$order_total += $product_onsells[$k]['totalprice'];
		}else if($product_onsell['type'] == 1){
			$order_credit_total += $product_onsells[$k]['totalprice'];
		}
		
		$product_count += $nums[$k];
	}

	$order_total = number_format($order_total, 2);

	include template('forum/order_buy');
}
// 确认订单
elseif($act == 'confirm'){
	
	
	if(submitcheck('ordersubmit')){
		$address = $_GET['address'];
		$newspid = $_GET['newspid'];
		$newnum = $_GET['newnum'];
		$newnote = $_GET['newnote'];
		$payment = I('payment',-1);
        $deliver_time = I('deliver_time',0); //送货时间

		$from = empty($_GET['from']) ? '' : $_GET['from'];
		
		if(!$_G['uid'] || !is_array($newspid) || !is_array($newnum)){
			showmessage('参数错误!');
		}
		
		if(!$address){
			showmessage('没有设置地址!');
		}
		
		if($payment==-1){
			showmessage('请先设置支付方式！');
		}
		
		//添加订单
		if($oid = order_add($address, $payment, $newspid, $newnum, $newnote, $deliver_time)){
			// 清除购物车
			clear_cart();
			//任务奖励
			require_once libfile('function/mission');
			add_product_progress($_G['uid']);
			
			//如果为微信支付（$payment=2），加入&showwxpaytitle=1,限制标题为"微信安全支付"
			$url = "/forum.php?mod=order&action=detail&order_id={$oid}".($payment==2?'&showwxpaytitle=1':'');
			dheader('Location:'.$url);
		}else{
			showmessage('您没有购买此商品的权限!');
		}
		exit();
	}else{
		showmessage('请按照正确方式提交表单');
	}
}
// 订单详细
elseif($act == 'detail'){
	//加载库文件
	require_once  libfile('function/product');
	require_once  libfile('function/order');
	// get user ID
	$user_id = $_G['member']['uid'];
	// 未登录时转到首页
	if (!$user_id) {
		dheader("Location: /");
	}
	// get order id param  获取订单ID参数
	$order_id = I('order_id',0,'intval');
	if (!$order_id || $order_id < 1) {
		dheader("Location: /");
	}
	// get order info  获取订单信息
	$orderInfo = C::t('common_member_order') -> fetch_by_oid($order_id, $user_id);
	// check order state
	if ($orderInfo['state'] == 4) {
		// check order deliver
		$express_state = check_order_express($orderInfo['oid'], $orderInfo['expresscom'], $orderInfo['expresscode']);
		if ($express_state == 2) {
			$orderInfo['state'] = 5;
		} else if ($express_state == 3) {
			$orderInfo['state'] = 6;
		}
	}
	$orderAllInfo = getOrderAllInfo($order_id, $user_id);
	$orderInfo = &$orderAllInfo;
	$orderState = $orderAllInfo['stateTable'];
	$orderDetail = $orderAllInfo['productTable'];
	$orderAddress = $orderAllInfo['address'];
	// order not found  找不到订单
	if (!$orderInfo || $orderInfo['uid'] != $_G['member'][uid]) {
		dheader("Location: /");
	}
	// details not found  找不到订单详情
	if (!$orderAllInfo) {
		showmessage('找不到订单详情');
		exit();
	}
	$orderStateTime = array("submit" => "", "sended" => "", "start_deliver" => "", "success" => "", );
	foreach ($orderState as $v) {
		switch ($v['state']) {
			case 0 :
				$orderStateTime["submit"] = date("Y-m-d", $v['dateline']) . '<br />' . date("H:i:s", $v['dateline']);
			case 2 :
				$orderStateTime["sended"] = date("Y-m-d", $v['dateline']) . '<br />' . date("H:i:s", $v['dateline']);
			case 4 :
				$orderStateTime["start_deliver"] = date("Y-m-d", $v['dateline']) . '<br />' . date("H:i:s", $v['dateline']);
			case 6 :
				$orderStateTime["success"] = date("Y-m-d", $v['dateline']) . '<br />' . date("H:i:s", $v['dateline']);
				break;
		}
	}
	
	//发送通知消息
	sendOrderMessage('ordersure',$orderInfo);
	
	// display
	include  template('forum/order_detail');

}
//发起支付
elseif($act == 'pay'){
	
	$order_id = I('order_id',0,'intval');
	$user_id = $_G['uid'];

	if (!$order_id || $order_id < 1 || !$user_id) {
		showmessage('订单ID出错！');
	}

	$order = C::t('common_member_order')->fetch_by_oid($order_id, $user_id);
	if(!$order || $order['paystate']!= 0){
		showmessage('没有该订单！');
	}
	
	if(!$_G['cache']['payment']){
		loadcache('payment');
	}

	$payment = $_G['cache']['payment'];
	$payment = 'payment_'.$payment[$order['payment']]['paytype'];

	$dopay = new $payment($order);
	$pay_content = $dopay->pay();
    include  template('forum/order_pay');
//	exit();
}
//订单状态展示（在线支付时告知支付结果）
elseif($act == 'state'){
	//是否AJAX
	$ajax = I('ajax',0);
	
	if(!$_G['uid']){
		if(!$ajax){
			showmessage('未登录，请先登录！');
		}else{
			ajax_return(0,'未登录，请先登录！');
		}
	}
	
	//支付查询微信接口
	$remote = I('remote',0,'intval');
	
	$uid = $_G['uid'];
	$oid = I('oid',0,'intval');
	$order = C::t('common_member_order')->fetch($oid,true);
	if($order['uid']!=$uid){
		if(!$ajax){
			showmessage('没有操作订单权限！');
		}else{
			ajax_return(0,'没有操作订单权限！');
		}
	}

    if(!$_G['cache']['payment']){
        loadcache('payment');
    }

    $payment = $_G['cache']['payment'];
    $payment = 'payment_'.$payment[$order['payment']]['paytype'];

    $payObj = new $payment($order);
    $order = $payObj->checkStatus($remote);

	//页面显示状态
	$page_show = $order['paystate'] == 0 ? 'no_pay' : 'payed' ;

    //已支付时，发送通知消息
    if($order['paystate']==1){
        sendOrderMessage('orderpaysure',$order);
    }

	if(!$ajax){
		include  template('forum/order_state');
	}else{
		ajax_return(1,'查询成功！',$order);
	}
}
// 我的订单列表
elseif($act == 'myorder'){
	//加载库文件
	require_once  libfile('function/url');
	$user_id = $_G['member']['uid'];
	// 未登录时转到首页
	if (!$user_id) {
		dheader("Location: ".U('member/logging/login',array('mobile'=>2)));
	}
	
	$page = I('page', 1, 'intval');
	$keywords = I("post.keywords",'');
	if ($page < 1){
		$page = 1;
	}
	$limit = 10;
	
	// get order type
	$orderType = isset($_GET['order_type']) ? $_GET['order_type'] : 'unfinished';
	
	// set filters
	$filters = array('uid=' . $user_id);
	if (!empty($orderType)){
		$filters[] = table_common_member_order::get_state_where_condition($orderType);
	}
	
	if ($keywords) {
		$orderList = table_common_member_order::fetch_rows_by_keywords(mysql_real_escape_string($keywords), array("uid" => (int)$user_id));
		$orderCount = count($orderList);
		$pageCount = 1;
	} else {
		$orderList = table_common_member_order::fetch_rows(array('start' => ($page - 1) * $limit, 'order' => ' oid DESC', 'limit' => $limit, 'filters' => $filters));
		$orderCount = table_common_member_order::count_rows($filters);
		$pageCount = ceil($orderCount / $limit);
	}
	//如果是手机版，则使用AJAX进行加载页面
	if (defined('IN_MOBILE') && $_GET['ajax'] == 1) {
		if($_GET['html'] == 1){
			$html = '';
			if($orderList){
				foreach ($orderList as $k => $orderItem) {
					$html .= mobile_order_list_item_template($orderItem, $_G['member']['uid'], true);
				} 
			}
			$res = array('result' => empty($orderList) ? 0 : 1, 'data' => $html ? htmlspecialchars($html) : null);
		}else{
			if($orderList){
				foreach ($orderList as $k => $orderItem) {
					$orderAllInfo = getOrderAllInfo($orderItem['oid'], $_G['member']['uid']);
					$orderAllInfo['state_name'] = get_state_name($orderAllInfo['state']);
					$orderList[$k] = array_merge($orderAllInfo, $orderList[$k]);
					$orderList[$k]['media_list'] = getMediaList($orderAllInfo['productTable'][0]);
				}
			}
			$res = array('result' => empty($orderList) ? 0 : 1, 'list' => $orderList ? $orderList : null);
		}

		echo json_encode($res, JSON_UNESCAPED_UNICODE);
		exit();
	}
	include  template('forum/order_list');
}
//晒订单
elseif($act == 'comment'){

	//有数据提交的时候
	if(IS_POST){
		$_tid = I('tid',0,'intval');
		$thread = C::t('forum_thread')->fetch($_tid);
		if($thread['authorid']==$_G['uid']){
			$_spid = I('spid',0,'intval');
			$_odid = I('odid',0,'intval');
			C::t('forum_thread')->update($_tid,array('spid'=>$_spid));
			C::t('common_member_order_detail')->update($_odid,array('tid'=>$_tid));
			ajax_return(1,'发表成功！！');
		}else{
			ajax_return(0,'权限不足！'.$_tid);
		}
		exit();
	}
	
	$oid = I('oid',0,'intval');
	$orderInfo = C::t('common_member_order')->fetch($oid);
	if(!$orderInfo){
		showmessage('参数错误！');
	}
	
	$profile = $_G['member'];
	if($orderInfo['uid']!=$_G['uid']){
		$profile = getuserprofilebyuid($orderInfo['uid'],'profile');
	}
	
	$orderAllInfo = getOrderOtherInfo($orderInfo);
	foreach($orderAllInfo['productTable'] as $k => $v){
		$_threads = C::t('forum_thread')->fetch($v['tid']);
		if($_threads){
			$_threads['media_list'] = json_decode(htmlspecialchars_decode($_threads['media_list']),TRUE);
		}
		$orderAllInfo['productTable'][$k]['threads'] = !empty($_threads) ? $_threads : null;
	}
	
	$defaultforum = unserialize($_G['setting']['defaultforum']);
	include  template('forum/order_comment');
}
// 申请退货
elseif ($act == 'cancelorder'){
	
	$oid = $_GET['oid'] > 0 ? intval($_GET['oid']) : 0;
	$uid = $_G['member']['uid'];
	
	if(!$oid || !$uid){
		header('Location: /');
		exit;
	}
	$orderAllInfo = getOrderAllInfo($oid, $uid);
	if(!$orderAllInfo){
		showmessage('找不到此订单');
	}
	include template('forum/order_cancel');
	exit;
}
else if ( $act == 'cart' ) {
	include template('forum/order_cart');
}
//查快递,用 oid 来查询，避免被人刷
elseif( $act == 'express'){

    $oid = I('oid',0);
    $order = C::t('common_member_order')->fetch($oid);
    if(!$order || $order['uid'] != $_G['uid']){
        showmessage('订单号出错！');
    }

    if($order['expresscom'] == 0 || empty($order['expresscode'])){
        showmessage('此订单未发货！');
    }

    //快递公司列表
    if(!$_G['cache']['express_company']){
        loadcache('express_company');
    }
    $express_company_list = $_G['cache']['express_company'];

    $express = Express::query_express($express_company_list[$order['expresscom']]['apiname'],$order['expresscode']);
    $error = Express::get_error();
    $errmsg = Express::get_errmsg();

    include template('forum/order_express');
}

