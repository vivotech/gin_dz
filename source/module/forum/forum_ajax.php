<?php
/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: forum_ajax.php 34303 2014-01-15 04:32:19Z hypowang $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}
define('NOROBOT', TRUE);

if($_GET['action'] == 'checkusername') {

	$username = trim($_GET['username']);
	$usernamelen = dstrlen($username);
	if($usernamelen < 3) {
		showmessage('profile_username_tooshort', '', array(), array('handle' => false));
	} elseif($usernamelen > 15) {
		showmessage('profile_username_toolong', '', array(), array('handle' => false));
	}

	loaducenter();
	$ucresult = uc_user_checkname($username);

	if($ucresult == -1) {
		showmessage('profile_username_illegal', '', array(), array('handle' => false));
	} elseif($ucresult == -2) {
		showmessage('profile_username_protect', '', array(), array('handle' => false));
	} elseif($ucresult == -3) {
		if(C::t('common_member')->fetch_by_username($username) || C::t('common_member_archive')->fetch_by_username($username)) {
			showmessage('register_check_found', '', array(), array('handle' => false));
		} else {
			showmessage('register_activation', '', array(), array('handle' => false));
		}
	}

	$censorexp = '/^('.str_replace(array('\\*', "\r\n", ' '), array('.*', '|', ''), preg_quote(($_G['setting']['censoruser'] = trim($_G['setting']['censoruser'])), '/')).')$/i';
	if($_G['setting']['censoruser'] && @preg_match($censorexp, $username)) {
		showmessage('profile_username_protect', '', array(), array('handle' => false));
	}

} elseif($_GET['action'] == 'checkemail') {

	require_once libfile('function/member');
	checkemail($_GET['email']);

} elseif($_GET['action'] == 'checkinvitecode') {

	$invitecode = trim($_GET['invitecode']);
	if(!$invitecode) {
		showmessage('no_invitation_code', '', array(), array('handle' => false));
	}
	$result = array();
	if($invite = C::t('common_invite')->fetch_by_code($invitecode)) {
		if(empty($invite['fuid']) && (empty($invite['endtime']) || $_G['timestamp'] < $invite['endtime'])) {
			$result['uid'] = $invite['uid'];
			$result['id'] = $invite['id'];
			$result['appid'] = $invite['appid'];
		}
	}
	if(empty($result)) {
		showmessage('wrong_invitation_code', '', array(), array('handle' => false));
	}

} elseif($_GET['action'] == 'checkuserexists') {

	if(C::t('common_member')->fetch_by_username(trim($_GET['username'])) || C::t('common_member_archive')->fetch_by_username(trim($_GET['username']))) {
		showmessage('<img src="'.$_G['style']['imgdir'].'/check_right.gif" width="13" height="13">', '', array(), array('msgtype' => 3));
	} else {
		showmessage('username_nonexistence', '', array(), array('msgtype' => 3));
	}

} elseif($_GET['action'] == 'attachlist') {

	require_once libfile('function/post');
	loadcache('groupreadaccess');
	$attachlist = getattach($_GET['pid'], intval($_GET['posttime']), $_GET['aids']);
	$attachlist = $attachlist['attachs']['unused'];
	$_G['group']['maxprice'] = isset($_G['setting']['extcredits'][$_G['setting']['creditstrans']]) ? $_G['group']['maxprice'] : 0;

	include template('common/header_ajax');
	include template('forum/ajax_attachlist');
	include template('common/footer_ajax');
	dexit();
} elseif($_GET['action'] == 'imagelist') {

	require_once libfile('function/post');
	$attachlist = getattach($_GET['pid'], intval($_GET['posttime']), $_GET['aids']);
	$imagelist = $attachlist['imgattachs']['unused'];

	include template('common/header_ajax');
	include template('forum/ajax_imagelist');
	include template('common/footer_ajax');
	dexit();

} elseif($_GET['action'] == 'get_rushreply_membernum') {
	$tid = intval($_GET['tid']);
	if($tid) {
		$membernum = C::t('forum_post')->count_author_by_tid($tid);
		showmessage('thread_reshreply_membernum', '', array('membernum' => intval($membernum - 1)), array('alert' => 'info'));
	}
	dexit();
} elseif($_GET['action'] == 'deleteattach') {

	$count = 0;
	if($_GET['aids']) {
		foreach($_GET['aids'] as $aid) {
			$attach = C::t('forum_attachment_n')->fetch('aid:'.$aid, $aid);
			if($attach && ($attach['pid'] && $attach['pid'] == $_GET['pid'] && $_G['uid'] == $attach['uid'])) {
				updatecreditbyaction('postattach', $attach['uid'], array(), '', -1, 1, $_G['fid']);
			}
			if($attach && ($attach['pid'] && $attach['pid'] == $_GET['pid'] && $_G['uid'] == $attach['uid'] || $_G['forum']['ismoderator'] || !$attach['pid'] && $_G['uid'] == $attach['uid'])) {
				C::t('forum_attachment_n')->delete('aid:'.$aid, $aid);
				C::t('forum_attachment')->delete($aid);
				dunlink($attach);
				$count++;
			}
		}
	}
	include template('common/header_ajax');
	echo $count;
	include template('common/footer_ajax');
	dexit();

} elseif($_GET['action'] == 'secondgroup') {

	require_once libfile('function/group');
	$groupselect = get_groupselect($_GET['fupid'], $_GET['groupid']);
	include template('common/header_ajax');
	include template('forum/ajax_secondgroup');
	include template('common/footer_ajax');
	dexit();

} elseif($_GET['action'] == 'displaysearch_adv') {
	$display = $_GET['display'] == 1 ? 1 : '';
	dsetcookie('displaysearch_adv', $display);
} elseif($_GET['action'] == 'checkgroupname') {
	$groupname = trim($_GET['groupname']);
	if(empty($groupname)) {
		showmessage('group_name_empty', '', array(), array('msgtype' => 3));
	}
	$tmpname = cutstr($groupname, 20, '');
	if($tmpname != $groupname) {
		showmessage('group_name_oversize', '', array(), array('msgtype' => 3));
	}
	if(C::t('forum_forum')->fetch_fid_by_name($groupname)) {
		showmessage('group_name_exist', '', array(), array('msgtype' => 3));
	}
	showmessage('', '', array(), array('msgtype' => 3));
	include template('common/header_ajax');
	include template('common/footer_ajax');
	dexit();
} elseif($_GET['action'] == 'getthreadtypes') {
	include template('common/header_ajax');
	if(empty($_GET['selectname'])) $_GET['selectname'] = 'threadtypeid';
	echo '<select name="'.$_GET['selectname'].'">';
	if(!empty($_G['forum']['threadtypes']['types'])) {
		if(!$_G['forum']['threadtypes']['required']) {
			echo '<option value="0"></option>';
		}
		foreach($_G['forum']['threadtypes']['types'] as $typeid => $typename) {
			if($_G['forum']['threadtypes']['moderators'][$typeid] && $_G['forum'] && !$_G['forum']['ismoderator']) {
				continue;
			}
			echo '<option value="'.$typeid.'">'.$typename.'</option>';
		}
	} else {
		echo '<option value="0" /></option>';
	}
	echo '</select>';
	include template('common/footer_ajax');
} elseif($_GET['action'] == 'getimage') {
	$_GET['aid'] = intval($_GET['aid']);
	$image = C::t('forum_attachment_n')->fetch('aid:'.$_GET['aid'], $_GET['aid'], 1);
	include template('common/header_ajax');
	if($image['aid']) {
		echo '<img src="'.getforumimg($image['aid'], 1, 300, 300, 'fixnone').'" id="image_'.$image['aid'].'" onclick="insertAttachimgTag(\''.$image['aid'].'\')" width="'.($image['width'] < 110 ? $image['width'] : 110).'" cwidth="'.($image['width'] < 300 ? $image['width'] : 300).'" />';
	}
	include template('common/footer_ajax');
	dexit();
} elseif($_GET['action'] == 'setthreadcover') {
	$aid = intval($_GET['aid']);
	$imgurl = $_GET['imgurl'];
	require_once libfile('function/post');
	if($_G['forum'] && ($aid || $imgurl)) {
		if($imgurl) {
			$tid = intval($_GET['tid']);
			$pid = intval($_GET['pid']);
		} else {
			$threadimage = C::t('forum_attachment_n')->fetch('aid:'.$aid, $aid);
			$tid = $threadimage['tid'];
			$pid = $threadimage['pid'];
		}

		if($tid && $pid) {
			$thread =get_thread_by_tid($tid);
		} else {
			$thread = array();
		}
		if(empty($thread) || (!$_G['forum']['ismoderator'] && $_G['uid'] != $thread['authorid'])) {
			if($_GET['newthread']) {
				showmessage('set_cover_faild', '', array(), array('msgtype' => 3));
			} else {
				showmessage('set_cover_faild', '', array(), array('closetime' => 3));
			}
		}
		if(setthreadcover($pid, $tid, $aid, 0, $imgurl)) {
			if(empty($imgurl)) {
				C::t('forum_threadimage')->delete_by_tid($threadimage['tid']);
				C::t('forum_threadimage')->insert(array(
					'tid' => $threadimage['tid'],
					'attachment' => $threadimage['attachment'],
					'remote' => $threadimage['remote'],
				));
			}
			if($_GET['newthread']) {
				showmessage('set_cover_succeed', '', array(), array('msgtype' => 3));
			} else {
				showmessage('set_cover_succeed', '', array(), array('alert' => 'right', 'closetime' => 1));
			}
		}
	}
	if($_GET['newthread']) {
		showmessage('set_cover_faild', '', array(), array('msgtype' => 3));
	} else {
		showmessage('set_cover_faild', '', array(), array('closetime' => 3));
	}

} elseif($_GET['action'] == 'updateattachlimit') {

	$_G['forum']['allowpostattach'] = isset($_G['forum']['allowpostattach']) ? $_G['forum']['allowpostattach'] : '';
	$_G['group']['allowpostattach'] = $_G['forum']['allowpostattach'] != -1 && ($_G['forum']['allowpostattach'] == 1 || (!$_G['forum']['postattachperm'] && $_G['group']['allowpostattach']) || ($_G['forum']['postattachperm'] && forumperm($_G['forum']['postattachperm'])));
	$_G['forum']['allowpostimage'] = isset($_G['forum']['allowpostimage']) ? $_G['forum']['allowpostimage'] : '';
	$_G['group']['allowpostimage'] = $_G['forum']['allowpostimage'] != -1 && ($_G['forum']['allowpostimage'] == 1 || (!$_G['forum']['postimageperm'] && $_G['group']['allowpostimage']) || ($_G['forum']['postimageperm'] && forumperm($_G['forum']['postimageperm'])));

	$allowuploadnum = $allowuploadtoday = TRUE;
	if($_G['group']['allowpostattach'] || $_G['group']['allowpostimage']) {
		if($_G['group']['maxattachnum']) {
			$allowuploadnum = $_G['group']['maxattachnum'] - getuserprofile('todayattachs');
			$allowuploadnum = $allowuploadnum < 0 ? 0 : $allowuploadnum;
			if(!$allowuploadnum) {
				$allowuploadtoday = false;
			}
		}
		if($_G['group']['maxsizeperday']) {
			$allowuploadsize = $_G['group']['maxsizeperday'] - getuserprofile('todayattachsize');
			$allowuploadsize = $allowuploadsize < 0 ? 0 : $allowuploadsize;
			if(!$allowuploadsize) {
				$allowuploadtoday = false;
			}
			$allowuploadsize = $allowuploadsize / 1048576 >= 1 ? round(($allowuploadsize / 1048576), 1).'MB' : round(($allowuploadsize / 1024)).'KB';
		}
	}
	include template('common/header_ajax');
	include template('forum/post_attachlimit');
	include template('common/footer_ajax');
	exit;

} elseif($_GET['action'] == 'forumchecknew' && !empty($_GET['fid']) && !empty($_GET['time'])) {
	$fid = intval($_GET['fid']);
	$time = intval($_GET['time']);

	if(!$_GET['uncheck']) {
		$foruminfo = C::t('forum_forum')->fetch($fid);
		$lastpost_str = $foruminfo['lastpost'];
		if($lastpost_str) {
			$lastpost = explode("\t", $lastpost_str);
			unset($lastpost_str);
		}
		include template('common/header_ajax');
		echo $lastpost['2'] > $time ? 1 : 0 ;
		include template('common/footer_ajax');
		exit;
	} else {
		$_G['forum_colorarray'] = array('', '#EE1B2E', '#EE5023', '#996600', '#3C9D40', '#2897C5', '#2B65B7', '#8F2A90', '#EC1282');
		$query = C::t('forum_forumfield')->fetch($fid);
		$forum_field['threadtypes'] = dunserialize($query['threadtypes']);
		$forum_field['threadsorts'] = dunserialize($query['threadsorts']);
		unset($query);
		$forum_field = daddslashes($forum_field);
		$todaytime = strtotime(dgmdate(TIMESTAMP, 'Ymd'));
		foreach(C::t('forum_thread')->fetch_all_by_fid_lastpost($fid, $time, TIMESTAMP) as $thread) {
			$thread['icontid'] = $thread['forumstick'] || !$thread['moved'] && $thread['isgroup'] != 1 ? $thread['tid'] : $thread['closed'];
			if(!$thread['forumstick'] && ($thread['isgroup'] == 1 || $thread['fid'] != $_G['fid'])) {
				$thread['icontid'] = $thread['closed'] > 1 ? $thread['closed'] : $thread['tid'];
			}
			list($thread['subject'], $thread['author'], $thread['lastposter']) = daddslashes(array($thread['subject'], $thread['author'], $thread['lastposter']));
			$thread['dateline'] = $thread['dateline'] > $todaytime ? "<span class=\"xi1\">".dgmdate($thread['dateline'], 'd')."</span>" : "<span>".dgmdate($thread['dateline'], 'd')."</span>";
			$thread['lastpost'] = dgmdate($thread['lastpost']);
			if($forum_field['threadtypes']['prefix']) {
				if($forum_field['threadtypes']['prefix'] == 1) {
					$thread['threadtype'] = $forum_field['threadtypes']['types'][$thread['typeid']] ? '<em>[<a href="forum.php?mod=forumdisplay&fid='.$fid.'&filter=typeid&typeid='.$thread['typeid'].'">'.$forum_field['threadtypes']['types'][$thread['typeid']].'</a>]</em> ' : '' ;
				} elseif($forum_field['threadtypes']['prefix'] == 2) {
					$thread['threadtype'] = $forum_field['threadtypes']['icons'][$thread['typeid']] ? '<em><a href="forum.php?mod=forumdisplay&fid='.$fid.'&filter=typeid&typeid='.$thread['typeid'].'"><img src="'.$forum_field['threadtypes']['icons'][$thread['typeid']].'"/></a></em> ' : '' ;
				}
			}
			if($forum_field['threadsorts']['prefix']) {
				$thread['threadsort'] = $forum_field['threadsorts']['types'][$thread['sortid']] ? '<em>[<a href="forum.php?mod=forumdisplay&fid='.$fid.'&filter=sortid&typeid='.$thread['sortid'].'">'.$forum_field['threadsorts']['types'][$thread['sortid']].'</a>]</em>' : '' ;
			}
			if($thread['highlight']) {
				$string = sprintf('%02d', $thread['highlight']);
				$stylestr = sprintf('%03b', $string[0]);

				$thread['highlight'] = ' style="';
				$thread['highlight'] .= $stylestr[0] ? 'font-weight: bold;' : '';
				$thread['highlight'] .= $stylestr[1] ? 'font-style: italic;' : '';
				$thread['highlight'] .= $stylestr[2] ? 'text-decoration: underline;' : '';
				$thread['highlight'] .= $string[1] ? 'color: '.$_G['forum_colorarray'][$string[1]].';' : '';
				if($thread['bgcolor']) {
					$thread['highlight'] .= "background-color: $thread[bgcolor];";
				}
				$thread['highlight'] .= '"';
			} else {
				$thread['highlight'] = '';
			}
			$target = $thread['isgroup'] == 1 || $thread['forumstick'] ? ' target="_blank"' : ' onclick="atarget(this)"';
			if(in_array('forum_viewthread', $_G['setting']['rewritestatus'])) {
				$thread['threadurl'] = '<a href="'.rewriteoutput('forum_viewthread', 1, '', $thread['tid'], 1, '', '').'"'.$thread['highlight'].$target.'class="s xst">'.$thread['subject'].'</a>';
			} else {
				$thread['threadurl'] = '<a href="forum.php?mod=viewthread&amp;tid='.$thread['tid'].'"'.$thread['highlight'].$target.'class="s xst">'.$thread['subject'].'</a>';
			}
			if(in_array($thread['displayorder'], array(1, 2, 3, 4))) {
				$thread['id'] = 'stickthread_'.$thread['tid'];
			} else {
				$thread['id'] = 'normalthread_'.$thread['tid'];
			}
			$thread['threadurl'] = $thread['threadtype'].$thread['threadsort'].$thread['threadurl'];
			if(in_array('home_space', $_G['setting']['rewritestatus'])) {
				$thread['authorurl'] = '<a href="'.rewriteoutput('home_space', 1, '', $thread['authorid'], '', '').'">'.$thread['author'].'</a>';
				$thread['lastposterurl'] = '<a href="'.rewriteoutput('home_space', 1, '', '', rawurlencode($thread['lastposter']), '').'">'.$thread['lastposter'].'</a>';
			} else {
				$thread['authorurl'] = '<a href="home.php?mod=space&uid='.$thread['authorid'].'">'.$thread['author'].'</a>';
				$thread['lastposterurl'] = '<a href="home.php?mod=space&username='.rawurlencode($thread['lastposter']).'">'.$thread['lastposter'].'</a>';
			}
			$threadlist[] = $thread;
		}
		if($threadlist) {
			krsort($threadlist);
		}
		include template('forum/ajax_threadlist');

	}
} elseif($_GET['action'] == 'downremoteimg') {
	$_GET['message'] = str_replace(array("\r", "\n"), array($_GET['wysiwyg'] ? '<br />' : '', "\\n"), $_GET['message']);
	preg_match_all("/\[img\]\s*([^\[\<\r\n]+?)\s*\[\/img\]|\[img=\d{1,4}[x|\,]\d{1,4}\]\s*([^\[\<\r\n]+?)\s*\[\/img\]/is", $_GET['message'], $image1, PREG_SET_ORDER);
	preg_match_all("/\<img.+src=('|\"|)?(.*)(\\1)([\s].*)?\>/ismUe", $_GET['message'], $image2, PREG_SET_ORDER);
	$temp = $aids = $existentimg = array();
	if(is_array($image1) && !empty($image1)) {
		foreach($image1 as $value) {
			$temp[] = array(
				'0' => $value[0],
				'1' => trim(!empty($value[1]) ? $value[1] : $value[2])
			);
		}
	}
	if(is_array($image2) && !empty($image2)) {
		foreach($image2 as $value) {
			$temp[] = array(
				'0' => $value[0],
				'1' => trim($value[2])
			);
		}
	}
	require_once libfile('class/image');
	if(is_array($temp) && !empty($temp)) {
		$upload = new discuz_upload();
		$attachaids = array();

		foreach($temp as $value) {
			$imageurl = $value[1];
			$hash = md5($imageurl);
			if(strlen($imageurl)) {
				$imagereplace['oldimageurl'][] = $value[0];
				if(!isset($existentimg[$hash])) {
					$existentimg[$hash] = $imageurl;
					$attach['ext'] = $upload->fileext($imageurl);
					if(!$upload->is_image_ext($attach['ext'])) {
						continue;
					}
					$content = '';
					if(preg_match('/^(http:\/\/|\.)/i', $imageurl)) {
						$content = dfsockopen($imageurl);
					} elseif(preg_match('/^('.preg_quote(getglobal('setting/attachurl'), '/').')/i', $imageurl)) {
						$imagereplace['newimageurl'][] = $value[0];
					}
					if(empty($content)) continue;
					$patharr = explode('/', $imageurl);
					$attach['name'] =  trim($patharr[count($patharr)-1]);
					$attach['thumb'] = '';

					$attach['isimage'] = $upload -> is_image_ext($attach['ext']);
					$attach['extension'] = $upload -> get_target_extension($attach['ext']);
					$attach['attachdir'] = $upload -> get_target_dir('forum');
					$attach['attachment'] = $attach['attachdir'] . $upload->get_target_filename('forum').'.'.$attach['extension'];
					$attach['target'] = getglobal('setting/attachdir').'./forum/'.$attach['attachment'];

					if(!@$fp = fopen($attach['target'], 'wb')) {
						continue;
					} else {
						flock($fp, 2);
						fwrite($fp, $content);
						fclose($fp);
					}
					if(!$upload->get_image_info($attach['target'])) {
						@unlink($attach['target']);
						continue;
					}
					$attach['size'] = filesize($attach['target']);
					$upload->attach = $attach;
					$thumb = $width = 0;
					if($upload->attach['isimage']) {
						if($_G['setting']['thumbsource'] && $_G['setting']['sourcewidth'] && $_G['setting']['sourceheight']) {
							$image = new image();
							$thumb = $image->Thumb($upload->attach['target'], '', $_G['setting']['sourcewidth'], $_G['setting']['sourceheight'], 1, 1) ? 1 : 0;
							$width = $image->imginfo['width'];
							$upload->attach['size'] = $image->imginfo['size'];
						}
						if($_G['setting']['thumbstatus']) {
							$image = new image();
							$thumb = $image->Thumb($upload->attach['target'], '', $_G['setting']['thumbwidth'], $_G['setting']['thumbheight'], $_G['setting']['thumbstatus'], 0) ? 1 : 0;
							$width = $image->imginfo['width'];
						}
						if($_G['setting']['thumbsource'] || !$_G['setting']['thumbstatus']) {
							list($width) = @getimagesize($upload->attach['target']);
						}
						if($_G['setting']['watermarkstatus'] && empty($_G['forum']['disablewatermark'])) {
							$image = new image();
							$image->Watermark($attach['target'], '', 'forum');
							$upload->attach['size'] = $image->imginfo['size'];
						}
					}
					$aids[] = $aid = getattachnewaid();
					$setarr = array(
						'aid' => $aid,
						'dateline' => $_G['timestamp'],
						'filename' => $upload->attach['name'],
						'filesize' => $upload->attach['size'],
						'attachment' => $upload->attach['attachment'],
						'isimage' => $upload->attach['isimage'],
						'uid' => $_G['uid'],
						'thumb' => $thumb,
						'remote' => '0',
						'width' => $width
					);
					C::t("forum_attachment_unused")->insert($setarr);
					$attachaids[$hash] = $imagereplace['newimageurl'][] = '[attachimg]'.$aid.'[/attachimg]';

				} else {
					$imagereplace['newimageurl'][] = $attachaids[$hash];
				}
			}
		}
		if(!empty($aids)) {
			require_once libfile('function/post');
		}
		$_GET['message'] = str_replace($imagereplace['oldimageurl'], $imagereplace['newimageurl'], $_GET['message']);
	}
	$_GET['message'] = addcslashes($_GET['message'], '/"\'');
	print <<<EOF
		<script type="text/javascript">
			parent.ATTACHORIMAGE = 1;
			parent.updateDownImageList('$_GET[message]');
		</script>
EOF;
	dexit();
} elseif($_GET['action'] == 'exif') {
	$exif = C::t('forum_attachment_exif')->fetch($_GET['aid']);
	$s = $exif['exif'];
	if(!$s) {
		require_once libfile('function/attachment');
		$s = getattachexif($_GET['aid']);
		C::t('forum_attachment_exif')->insert($_GET['aid'], $s);
	}
	include template('common/header_ajax');
	echo $s;
	include template('common/footer_ajax');
	exit;
} elseif($_GET['action'] == 'getthreadclass') {
	$fid = intval($_GET['fid']);
	$threadclass = '';
	if($fid) {
		$option = array();
		$forumfield = C::t('forum_forumfield')->fetch($fid);
		if(!empty($forumfield['threadtypes'])) {
			foreach(C::t('forum_threadclass')->fetch_all_by_fid($fid) as $tc) {
				$option[] = '<option value="'.$tc['typeid'].'">'.$tc['name'].'</option>';
			}
			if(!empty($option)) {
				$threadclass .= '<option value="">'.lang('forum/template', 'modcp_select_threadclass').'</option>';
				$threadclass .= implode('', $option);
			}
		}
	}

	if(!empty($threadclass)) {
		$threadclass = '<select name="typeid" id="typeid" width="168" class="ps">'.$threadclass.'</select>';
	}
	include template('common/header_ajax');
	echo $threadclass;
	include template('common/footer_ajax');
	exit;

} elseif($_GET['action'] == 'forumjump') {
	require_once libfile('function/forumlist');
	$favforums = C::t('home_favorite')->fetch_all_by_uid_idtype($_G['uid'], 'fid');
	$visitedforums = array();
	if($_G['cookie']['visitedfid']) {
		loadcache('forums');
		foreach(explode('D', $_G['cookie']['visitedfid']) as $fid) {
			$fid = intval($fid);
			$visitedforums[$fid] = $_G['cache']['forums'][$fid]['name'];
		}
	}
	$forumlist = forumselect(FALSE, 1);
	include template('forum/ajax_forumlist');
} elseif($_GET['action'] == 'quickreply') {
	$tid = intval($_GET['tid']);
	$fid = intval($_GET['fid']);
	if($tid) {
		$thread = C::t('forum_thread')->fetch($tid);
		if($thread && !getstatus($thread['status'], 2)) {
			$list = C::t('forum_post')->fetch_all_by_tid('tid:'.$tid, $tid, true, 'DESC', 0, 10, null, 0);
			loadcache('smilies');
			foreach($list as $pid => $post) {
				if($post['first']) {
					unset($list[$pid]);
				} else {
					$post['message'] = preg_replace($_G['cache']['smilies']['searcharray'], '', $post['message']);
					$post['message'] = preg_replace("/\{\:soso_((e\d+)|(_\d+_\d))\:\}/e", '', $post['message']);
					$list[$pid]['message'] = cutstr(preg_replace("/\[.+?\]/ies", '', dhtmlspecialchars($post['message'])), 300) ;
				}
			}
			krsort($list);
		}
	}
	list($seccodecheck, $secqaacheck) = seccheck('post', 'reply');
	include template('forum/ajax_quickreply');
} elseif($_GET['action'] == 'getpost') {
	$tid = intval($_GET['tid']);
	$fid = intval($_GET['fid']);
	$pid = intval($_GET['pid']);
	$thread = C::t('forum_thread')->fetch($tid);
	$post = C::t('forum_post')->fetch($thread['posttableid'], $pid);
	include template('forum/ajax_followpost');
} elseif($_GET['action'] == 'quickclear') {
	$uid = intval($_GET['uid']);
	if($_G['adminid'] != 1) {
		showmessage('quickclear_noperm');
	}
	include_once libfile('function/misc');
	include_once libfile('function/member');

	if(!submitcheck('qclearsubmit')) {
		$crimenum_avatar = crime('getcount', $uid, 'crime_avatar');
		$crimenum_sightml = crime('getcount', $uid, 'crime_sightml');
		$crimenum_customstatus = crime('getcount', $uid, 'crime_customstatus');
		$crimeauthor = getuserbyuid($uid);
		$crimeauthor = $crimeauthor['username'];

		include template('forum/ajax');
	} else {
		if(empty($_GET['operations'])) {
			showmessage('quickclear_need_operation');
		}
		$reason = checkreasonpm();
		$allowop = array('avatar', 'sightml', 'customstatus');
		$cleartype = array();
		if(in_array('avatar', $_GET['operations'])) {
			C::t('common_member')->update($uid, array('avatarstatus'=>0));
			loaducenter();
			uc_user_deleteavatar($uid);
			$cleartype[] = lang('forum/misc', 'avatar');
			crime('recordaction', $uid, 'crime_avatar', lang('forum/misc', 'crime_reason', array('reason' => $reason)));
		}
		if(in_array('sightml', $_GET['operations'])) {
			C::t('common_member_field_forum')->update($uid, array('sightml' => ''), 'UNBUFFERED');
			$cleartype[] = lang('forum/misc', 'signature');
			crime('recordaction', $uid, 'crime_sightml', lang('forum/misc', 'crime_reason', array('reason' => $reason)));
		}
		if(in_array('customstatus', $_GET['operations'])) {
			C::t('common_member_field_forum')->update($uid, array('customstatus' => ''), 'UNBUFFERED');
			$cleartype[] = lang('forum/misc', 'custom_title');
			crime('recordaction', $uid, 'crime_customstatus', lang('forum/misc', 'crime_reason', array('reason' => $reason)));
		}
		if(($_G['group']['reasonpm'] == 2 || $_G['group']['reasonpm'] == 3) || !empty($_GET['sendreasonpm'])) {
			sendreasonpm(array('authorid' => $uid), 'reason_quickclear', array(
				'cleartype' => implode(',', $cleartype),
				'reason' => $reason,
				'from_id' => 0,
				'from_idtype' => 'quickclear'
			));
		}
		showmessage('quickclear_success', $_POST['redirect'], array(), array('showdialog'=>1, 'closetime' => true, 'msgtype' => 2, 'locationtime' => 1));
	}
} elseif($_GET['action'] == 'getpostfeed') {
	$tid = intval($_GET['tid']);
	$pid = intval($_GET['pid']);
	$flag = intval($_GET['flag']);
	$feed = $thread = array();
	if($tid) {
		$thread = C::t('forum_thread')->fetch($tid);
		if($flag) {
			$post = C::t('forum_post')->fetch($thread['posttableid'], $pid);
			require_once libfile('function/discuzcode');
			require_once libfile('function/followcode');
			$post['message'] = followcode($post['message'], $tid, $pid);
		} else {
			if(!isset($_G['cache']['forums'])) {
				loadcache('forums');
			}
			$feedid = intval($_GET['feedid']);
			$feed = C::t('forum_threadpreview')->fetch($tid);
			if($feedid) {
				$feed = array_merge($feed, C::t('home_follow_feed')->fetch_by_feedid($feedid));
			}
			$post['message'] = $feed['content'];
		}
	}
	include template('forum/ajax_followpost');

} elseif($_GET['action'] == 'setnav') {
	if($_G['adminid'] != 1) {
		showmessage('quickclear_noperm');
	}
	$allowfuntype = array('portal', 'group', 'follow', 'collection', 'guide', 'feed', 'blog', 'doing', 'album', 'share', 'wall', 'homepage', 'ranklist');
	$type = in_array($_GET['type'], $allowfuntype) ? trim($_GET['type']) : '';
	$do = in_array($_GET['do'], array('open', 'close')) ? $_GET['do'] : 'close';
	if(!submitcheck('funcsubmit')) {
		$navtitle = lang('spacecp', $do == 'open' ? 'select_the_navigation_position' : 'close_module', array('type' => lang('spacecp', $type)));
		$closeprompt = lang('spacecp', 'close_module', array('type' => lang('spacecp', $type)));
		include template('forum/ajax');
	} else {
		if(!empty($type)) {
			$funkey = $type.'status';
			$funstatus = $do == 'open' ? 1 : 0;
			if($type != 'homepage') {
				$identifier = array('portal' => 1, 'group' => 3, 'feed' => 4, 'ranklist' => 8, 'follow' => 9, 'guide' => 10, 'collection' => 11, 'blog' => 12, 'album' => 13, 'share' => 14, 'doing' => 15);
				$navdata = array('available' => -1);
				$navtype = $do == 'open' ? array() : array(0, 3);
				if(in_array($type, array('blog', 'album', 'share', 'doing', 'follow'))) {
					$navtype[] = 2;
				}
				if($do == 'open') {
					if($_GET['location']['header']) {
						$navtype[] = 0;
						$navdata['available'] = 1;
					}
					if($_GET['location']['quick']) {
						$navtype[] = 3;
						$navdata['available'] = 1;
					}
					$navdata['available'] = $navdata['available'] == 1 ? 1 : 0;
					if(empty($_GET['location']['header']) || empty($_GET['location']['quick'])) {
						C::t('common_nav')->update_by_navtype_type_identifier(array(0, 2, 3), 0, array("$type", "$identifier[$type]"), array('available' => 0));
					}
				}
				if($navtype) {
					C::t('common_nav')->update_by_navtype_type_identifier($navtype, 0, array("$type", "$identifier[$type]"), $navdata);
					if(in_array($type, array('blog', 'album', 'share', 'doing', 'follow')) && !$navdata['available']) {
						C::t('common_nav')->update_by_navtype_type_identifier(array(2), 0, array("$type"), array('available' => 1));
					}
				}
			}
			C::t('common_setting')->update($funkey, $funstatus);

			$setting[$funkey] = $funstatus;
			include libfile('function/cache');
			updatecache('setting');
		}
		showmessage('do_success', dreferer(), array(), array('header'=>true));
	}
	exit;
} elseif($_GET['action'] == 'checkpostrule') {
	require_once libfile('function/post');
	include template('common/header_ajax');
	$_POST = array('action' => $_GET['ac']);
	list($seccodecheck, $secqaacheck) = seccheck('post', $_GET['ac']);
	if($seccodecheck || $secqaacheck) {
		include template('forum/seccheck_post');
	}
	include template('common/footer_ajax');
	exit;
}
//循环帖子出来
elseif($_GET['action'] == 'loadallforum'){
	$size = isset($_REQUEST['pagesize']) ? intval($_REQUEST['pagesize']) : $_G['tpp'];
	$count = @ceil((C::t('forum_thread')->count()) / $_G['tpp'] ) ;//共多少页
	$page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;
	$page = $page > $count ? 1 : $page;
	$result = C::t('forum_thread')->fetch_all_limit(true , ($page-1)*$_G['tpp'] , $_G['tpp']);
	$return['result'] = $result ? ( $page == 1 ? 0 : 1 ) : 0 ;
	if($return['result'] == 0){
		$return['msg'] = '翻到最底了哦!';
	}else{
		$return['data']['share_list'] = $result;
		$return['data']['page'] = $page;
	}
	echo json_encode($return,JSON_UNESCAPED_UNICODE);
	exit();
}

// get user info when mouse enter user's avatar  指向用户头像时获取用户信息
else if ($_GET['action'] === "test")
{
  $user_id = isset($_REQUEST['userId']) ? intval($_REQUEST['userId']) : 0;
  if (!$user_id)
  {
    return;
  }
  
  $data = get_user_info_by_uid($user_id);
  $return = array(
  		"result"  => 1,
  		'data' => $data,
  );
  
  echo json_encode($return,JSON_UNESCAPED_UNICODE);
  exit();
}

// 添加收货地址
else if ($_GET['action'] === "addaddress"){
	require_once libfile('function/order');
	
	$newname = $_GET['newname'];
	$newprovince = $_GET['province'];
	$newcity = $_GET['city'];
	$newdist = $_GET['dist'];
	$newcommunity = $_GET['community'];
	$newprovinceid = $_GET['provinceid'];
	$newcityid = $_GET['cityid'];
	$newdistid = $_GET['distid'];
	$newcommunityid = $_GET['communityid'];
	$newaddress = $_GET['newaddress'];
	$newphone = $_GET['newphone'];
	if(!$_G['uid'] || !$newname || !$newaddress || !$newphone){
		exit('参数缺失！');
	}
	$insert_data = array(
		'uid'=>$_G['uid'],
		'name'=>$newname,
		'address'=>$newaddress,
		'phone'=>$newphone,
		'province'=>$newprovince,
		'city'=>$newcity,
		'dist'=>$newdist,
		'community'=>$newcommunity,
		'provinceid'=>$newprovinceid,
		'cityid'=>$newcityid,
		'distid'=>$newdistid,
		'communityid'=>$newcommunityid
	);
	$uaid = C::t('common_member_address')->insert($insert_data, 1);
	$uaid = intval($uaid);
	$return = array('uaid'=>$uaid);
	$return = array_merge($return,$insert_data);
	$return['address'] = getFullAddress($return);
	
	//修改cookie默认地址
	dsetcookie('default_address_id', $uaid);
	dsetcookie('default_address_name', $newname);
	dsetcookie('default_address_phone', $newphone);
	dsetcookie('default_address_address', getFullAddress($insert_data));
	
    echo json_encode($return,JSON_UNESCAPED_UNICODE);
    exit();
}
// 删除收货地址
else if ($_GET['action'] === "deladdress"){
	$uaid = $_GET['uaid'];
	if(!$_G['uid'] || !$uaid){
		exit;
	}
	$res = C::t('common_member_address')->delete_by_uid($uaid, $_G['uid']);
	$return = array('result'=>$res);
	
	//清楚默认收货地址
	dsetcookie('default_address_id',null);
	dsetcookie('default_address_name',null);
	dsetcookie('default_address_phone',null);
	dsetcookie('default_address_address',null);
	
    echo json_encode($return, JSON_UNESCAPED_UNICODE);
    exit();
}
//设置默认收货地址(APP在用)
elseif($_GET['action'] === "setdefaultaddress"){
	require_once libfile('function/order');
	$uaid = I('uaid',0,'intval');
	if($res = C::t('common_member_address')->fetch($uaid)){
		dsetcookie('default_address_id',$uaid);
		dsetcookie('default_address_name',$res['name']);
		dsetcookie('default_address_phone',$res['phone']);
		dsetcookie('default_address_address',getFullAddress($res));
		echo 1;
	}else{
		echo 0;
	}
	
    exit();
}
// get post list
else if ( $_GET['action'] == "postlist" ) {
  require_once libfile('function/post');
  
  $tid = isset($_GET['tid']) ? (int)$_GET['tid'] : 0;
  $pid = isset($_GET['pid']) ? (int)$_GET['pid'] : 0;
  $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
  $type = isset($_GET['type']) ? $_GET['type'] : "old";    // old 更旧 new 更新
  $timeline = isset($_GET['timeline']) ? $_GET['timeline'] : time();  // 以该时间线为基础获取
  $pidline = isset($_GET['pidline']) ? $_GET['pidline'] : 0;          // 不包含该帖子ID
  
  $postperpage = $_G['setting']['mobile']['mobilepostperpage'];
  
  // get cache
  $cacheName = 'post/'.$tid."/".$page;
  
  // set filters
  $filters = array("tid=".$tid, "first=0");   // 指定主题ID和不是第一张帖
  if ($pid) { $filters[] = "pid=".$pid; }     // 如果指定特定帖子ID
  if ($type === "old")
  {
    $filters[] = "dateline<=".$timeline;
  }
  else
  {
    $filters[] = "dateline>=".$timeline;
  }
  if ($pidline)
  {
    $filters[] = "pid!=".$pidline;
  }
  
  // get post list
  $post_options = array(
    "start" => $postperpage * ($page - 1),
    "limit" => $pid ? "1" : $postperpage,
    "filters" => $filters,
    "order" => "dateline",
    "desc"  => true
  );
  $postlist = table_forum_post::fetch_rows($post_options);
  
  require_once libfile('function/discuzcode');
  require_once libfile('function/attachment');
  
  $bbcode = new bbcode();
  for ($i=0; $i<count($postlist); $i++)
  {
    $post = $postlist[$i];
    $postlist[$i]['avatar'] = avatar($post['authorid'], 'middle', true);
    $postlist[$i]['timeline'] = $post['dateline'];
    $postlist[$i]['dateline'] = dgmdate($post['dateline'], 'u');
    $postlist[$i]['message'] = discuzcode($postlist[$i]['message']);
    
    
    $postlist[$i]['message'] = str_replace("\r\n", "<br />", $postlist[$i]['message']);
    $postlist[$i]['message'] = str_replace("\n", "<br />", $postlist[$i]['message']);
    
    $imageTags = get_img_from_html($postlist[$i]['message']);
    foreach ($imageTags as $v) {
    	$postlist[$i]['images'][] = get_attr_from_tag($v, 'src');
		$postlist[$i]['message'] = str_replace($v, '', $postlist[$i]['message']);
    }
	
	//$postlist[$i]['message'] = str_replace('<br /><br />', '<br />', $postlist[$i]['message']);
    $postlist[$i]['message'] = preg_replace('/(<br \/>)+/', '<br />', $postlist[$i]['message']);
    
    $postlist[$i]['tags'] = str_replace('\t', '  ', $postlist[$i]['tags']);
    $postlist[$i]['is_saygood'] = isSayGood($_G['uid'], $post['pid']);
    $postlist[$i]['saygood_number'] = getSayGoodNumber($post['pid']);
    if ($postlist[$i]['reppid'] && ($reppost = getPostByPid($postlist[$i]['reppid'])))
    {
      $postlist[$i]['rep_author'] = $reppost['author'];
	  $postlist[$i]['rep_authorid'] = $reppost['authorid'];
      //$postlist[$i]['rep_text'] = discuzcode($bbcode->bbcode2html($reppost['message']));
      
	  /*
	  $repImageTags = get_img_from_html($postlist[$i]['rep_text']);
	  foreach ($repImageTags as $v) {
	      $postlist[$i]['images'][] = get_attr_from_tag($v, 'src');
		  $postlist[$i]['message'] = str_replace($v, '', $postlist[$i]['message']);
	  }*/
	  
	  
	}
    else {
      $postlist[$i]['rep_author'] = '';
      $postlist[$i]['rep_text'] = '';
    }

  }
  // print result
  $json_result = json_encode($postlist, JSON_UNESCAPED_UNICODE);
  echo $json_result;
  
  exit;
}
// 点赞
else if ( $_GET['action'] == "saygood" )
{
  $result = array('state'=>'');
  
  // 检查用户登录
  if (!$_G['uid'])
  {
    $result['state'] = 'error';
    $result['error'] = 'not login';
    $result['message'] = '请先进行登录';
    echo json_encode($result, JSON_UNESCAPED_UNICODE);
    exit();
  }
  
  require_once libfile('function/post');
  
  $pid = isset($_GET['pid']) ? (int)$_GET['pid'] : 0;
  $tid = isset($_GET['tid']) ? (int)$_GET['tid'] : 0;
  
  // 给帖子点赞
  if ($pid)
  {
    // 获取要执行的帖子
    $post = C::t('forum_post')->fetch('tid:'.$_GET['tid'], $_GET['pid'], false);

    if (!$post)
    {
      $result['state'] = 'error';
      $result['error'] = 'post error';
      $result['message'] = '找不到帖子';
      echo json_encode($result, JSON_UNESCAPED_UNICODE);
      exit();
    }

    $result['pid'] = $post['pid'];

    // 获取回帖投票记录
    $hotreply = C::t('forum_hotreply_number')->fetch_by_pid($post['pid']);

    // 如果不存在回帖投票记录则插入一个新的记录
    if(empty($hotreply)) {
      $hotreply['pid'] = C::t('forum_hotreply_number')->insert(array(
        'pid' => $post['pid'],
        'tid' => $post['tid'],
        'support' => 0,
        'against' => 0,
        'total' => 0,
      ), true);
    }
    else
    {
      // 如果已投票则返回错误提示
      if(C::t('forum_hotreply_member')->fetch($post['pid'], $_G['uid'])) {
        $result['state'] = 'info';
        $result['message'] = '该帖子已赞';
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
        exit();
      }
    }

    C::t('forum_hotreply_number')->update_num($post['pid'], 1);

    // 保存会员赞的记录
    C::t('forum_hotreply_member')->insert(array(
      'tid' => $post['tid'],
      'pid' => $post['pid'],
      'uid' => $_G['uid'],
      'attitude' => 1,
    ));

      // 奖励积分
      $credit_rule = C::t('common_credit_rule')->fetch_one_by_action('saygood');

      if ($credit_rule) {

          $credit = & credit::instance();
          $credit->updatemembercount($credit_rule, $uid);

          //$return['credit'] = $credit_rule['extcredits2'];
          $member_count = C::t('common_member_count')->fetch_by_uid($uid);
          //var_dump($member_count);
          if ($member_count) {
              //$return['credit_own'] = $member_count['extcredits2'];
          }

      }

    $result['state'] = 'success';
    $result['message'] = '成功';
    echo json_encode($result, JSON_UNESCAPED_UNICODE);
    exit();
  }
  // 给主题点赞
  else if ($tid)
  {
    // get thread
    $threadTable = new table_forum_thread();
    $thread = $threadTable->fetch($tid);
    if (!$thread)
    {
      $result['state'] = 'error';
      $result['message'] = '找不到主题';
      echo json_encode($result, JSON_UNESCAPED_UNICODE);
      exit();
    }
    
    $homeSayGoodTable = new table_home_saygood();
    $saygood_record = $homeSayGoodTable->fetch_by_uid_id($_G['uid'], $tid, 'tid');
    if ($saygood_record)
    {
      $result['state'] = 'error';
      $result['message'] = '你已为该主题点赞了。';
      echo json_encode($result, JSON_UNESCAPED_UNICODE);
      exit();
    }
    
    // update saygood data
    $threadTable->update($tid, array(
      "saygood" => $thread['saygood'] + 1,
    ));
    
    // insert a saygood record
    DB::insert("home_saygood", array(
      "uid"   => $_G['uid'],
      "id"    => $tid,
      "idtype"  => "tid",
      "dateline"  => time(),
    ));

      // 奖励积分
      $credit_rule = C::t('common_credit_rule')->fetch_one_by_action('saygood');

      if ($credit_rule) {

          $credit = & credit::instance();
          $credit->updatemembercount($credit_rule, $uid);

          //$return['credit'] = $credit_rule['extcredits2'];
          $member_count = C::t('common_member_count')->fetch_by_uid($uid);
          //var_dump($member_count);
          if ($member_count) {
              //$return['credit_own'] = $member_count['extcredits2'];
          }

      }
    
    $result['state'] = 'success';
    $result['message'] = '成功';
    echo json_encode($result, JSON_UNESCAPED_UNICODE);
    exit();
  }
}
// 我的图库获取页数
elseif($_GET['action'] == 'ajaxLoadMyGallery'){
	$pagesize = $_GET['pagesize'];
	$pagestart = $_GET['pagestart'];
	$uid = $_G['uid'];
	if(!$uid || !$pagestart || !$pagesize){
		$return = '';
	}else{
		/*
		require_once libfile('function/editor');
		$attachlist_count = C::t('forum_attachment')->count_by_uid($_G['uid']);
		$attachlist = C::t('forum_attachment')->fetch_all_by_uid($_G['uid'], ($pagestart - 1) * $pagesize, $pagesize);
		$return = '';
		foreach($attachlist as $aid=>$attach){
			$temp_attach = C::t('forum_attachment_n')->fetch_by_aid_uid($attach['tableid'], $attach['aid'], $_G['uid']);
			$temp_attach['url'] = ($temp_attach['remote'] ? $_G['setting']['ftp']['attachurl'] : $_G['setting']['attachurl']).'forum/';
			$attachlist[$aid] = array_merge($temp_attach, $attachlist[$aid]);
			$return .= gallery_template($attachlist[$aid], true);
		}
		 * 
		 */
		require_once libfile('function/gallery');
		$gallery = hasGallery($uid);
		if($gallery['gid']){
			$image_list = C::t('common_member_gallery_image')->fetch_all_by_uid_gid($uid, $gallery['gid'], ($pagestart -1) * $pagesize, $pagesize);
			foreach($image_list as $image){
				$return .= gallery_template($image, true);
			}
		}
	}
	echo $return;
	exit;
}
// 循环默认背景文件夹，返回背景图文件json
elseif($_GET['action'] == 'ajaxLoadDefaultPersonBg'){
	//背景图文件夹
	$res = array();
	
	$bg_dir = DISCUZ_ROOT.'static'.DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'vivo'.DIRECTORY_SEPARATOR.'person_background'.DIRECTORY_SEPARATOR;
	if(is_dir($bg_dir)){
		 if($dir = opendir($bg_dir)){
		 	//循环文件夹
		 	while(($file = readdir($dir))!= false){
			    $_temp = explode('.', $file);
			    //判断文件扩展名是否为图片
			    $img_array = array('jpg','jpeg','gif','png');
			    if(in_array($_temp[count($_temp)-1],$img_array)){
			    	$res[] = $file;
			    }
		    }
		    closedir($dir);
		 }
	}else{
		echo $bg_dir.'没有找到文件夹';
		exit();
	}

	echo json_encode($res);
	exit;
}
// 获取七牛Token
elseif($_GET['action'] == 'getQiNiuToken'){
	$bucket = $_G['config']['extend']['storage']['qiniu']['bucket'];
	$scope = !empty($_GET['key']) ? $bucket.':'.$_GET['key'] : $bucket;
	C::import('storage/qiniu', 'vendor', true, true);
	$GLOBALS['QINIU_UP_HOST'] = 'http://up.qiniu.com';
	$GLOBALS['QINIU_RS_HOST'] = 'http://rs.qbox.me';
	$GLOBALS['QINIU_RSF_HOST'] = 'http://rsf.qbox.me';
	Qiniu_setKeys($_G['config']['extend']['storage']['qiniu']['accesskey'], $_G['config']['extend']['storage']['qiniu']['secretkey']);
	$putPolicy = new Qiniu_RS_PutPolicy($scope);
	$upToken = $putPolicy->Token(null);
	$return = '{"uptoken":"'.$upToken.'"}';
	echo $return;
	exit;
}
// 获取七牛文件名
elseif($_GET['action'] == 'getQiNiuRandomKey'){
	echo get_randstr();
	exit;
}
// 更新个人主页
elseif($_GET['action'] == 'updateUserPersonalBg'){
	$res = false;
	$url = $_GET['url'];
	if(!empty($_G['uid']) && !empty($url)){
		$update_data = array(
			'personal_bg'=>$url
		);
		$affect_rows = C::t('common_member_profile')->update($_G['uid'], $update_data);
		$res = true;
	}
	
	if($res){
		echo '{"result":"1"}';
	}else{
		echo '{"result":"0"}';
	}
	exit;
}
//参加活动
elseif($_GET['action'] == "activity" ){
	
	if(!$_G['uid']){
		ajax_return(0,'请先登录！');
	}
	
	$opparticipate_arr = array('participate');
	$op = I('op','participate','',$op_arr);
	
	//参加活动
	if($op == 'participate'){
		$caid = I('caid',0,'intval');
		$fid = I('fid',0,'intval');
		//检查是否有这个活动
		$check_activity = C::t('forum_custom_activity')->fetch($caid);
		if($check_activity['fid'] != $fid){
			ajax_return(0,'没有这个活动');
		}
		$check_participate = C::t('forum_custom_activity_join')->fetch_by_caid_uid($caid,$_G['uid']);
		if($check_participate){
			ajax_return(0,'已经参加过这个活动！');
		}
		$insert_data=array(
				'caid' => $caid,
				'uid'  => $_G['uid'],
				'nickname'=> $_G['member']['nickname'],
				'dateline'=> time()
		);
		if(C::t('forum_custom_activity_join')->insert($insert_data)){
			ajax_return(1,'参加活动成功！！');
		}else{
			ajax_return(0,'参加活动失败，请联系管理员！！');
		}
	}
	
}
//获取活动帖子排名
elseif($_GET['action'] == 'get_rank'){
	$fid = I('fid',0,'intval');
	$model = C::t('common_syscache');
	$rank = $model->fetch_row('rank_'.$fid);
	//当缓存不存在或者缓存时间大于10分钟的时候，重新设置读取一次排名
	if(!$rank || TIMESTAMP - $rank['dateline'] > 60*10){
		require_once libfile('function/forumactivity');
		$data = get_rank($fid);
		savecache('rank_'.$fid, $data);
	}else{
		if(!$_G['cache']['rank_'.$fid]){
			loadcache('rank_'.$fid);
		}
		$data = $_G['cache']['rank_'.$fid];
	}
	if(!$data){
		$data = array(array('authorid'=>0,'author'=>'暂无排名'));
	}
	
	ajax_return(1,'排名成功！',$data);
}
//获取
elseif($_GET['action'] == 'insert_gallery_image'){
	$uid = ($_G['uid']) ? $_G['uid'] : 0;
	$url = I('url', '');
	$desc = I('desc', '');
	$fastpost = I('fastpost',0,'intval',array(0,1));
	
	if(!$uid){
		ajax_return(0,'未登陆!');
	}
	
	require_once libfile('function/gallery');
	$gallery = hasGallery($uid);
	if(!$gallery){
		$gid = createDefaultGallery($uid);
	}else{
		$gid = $gallery['gid'];
	}
	
	$insert_data = array(
		'uid'=>$uid,
		'gid'=>$gid,
		'url'=>$url,
		'desc'=>$desc,
		'dateline'=>TIMESTAMP
	);
	$giid = C::t('common_member_gallery_image')->insert($insert_data, 1);
		
	if($giid > 0){
		require_once libfile('function/editor');
		$insert_data['giid'] = $giid;
		$html_content = htmlspecialchars(gallery_template($insert_data, 1,$fastpost));
		ajax_return(1, '', array('giid'=>$giid, 'html'=>$html_content));
	}else{
		ajax_return(0, '插入图库失败', '');
	}
}
//删除图库图片
elseif($_GET['action'] == 'delete_gallery_image'){
	$uid = ($_G['uid']) ? $_G['uid'] : 0;
	$id = I('id',0,'intval');
	if(!$uid){
		ajax_return(0,'未登陆!');
	}
	if(!$id){
		ajax_return(0,'参数错误!');
	}
	$item = C::t('common_member_gallery_image')->fetch($id);
	if($item['uid']!=$uid){
		ajax_return(0,'没有权限删除!');
	}
	if(C::t('common_member_gallery_image')->delete($id)){
		ajax_return(1);
	}else{
		ajax_return(0,'删除错误!');
	}
}
elseif ($_GET['action'] == 'order_cancel'){
	
	$order_openid = getgpc('order_openid');
	$uid = ($_G['uid']) ? $_G['uid'] : 0;
	if(!$uid){
		ajax_return(0,'未登陆!');
	}
	if(!$order_openid){
		ajax_return(2,'参数错误!');
	}
	
	$cancel_reason_type = getgpc('reason_type');
	$cancel_note = getgpc('note');
	
	require_once libfile('function/order');
	
	// 获取订单
	$order = C::t('common_member_order')->fetch_by_openid($order_openid, $uid);
	
	// 找不到该用户的订单呀
	if (!$order || $order['uid'] != $uid) {
		ajax_return(3,'查询订单记录失败!');
	}
	
	// 已在退货流程中，拒绝重复申请。
	if ( $order['state'] < 0 ) {
		ajax_return(4,'该订单已在退货流程中，不要重复提交申请。');
	}
	
	// 时间太久了
	if ( $order['dateline'] + 365*24*3600 < TIMESTAMP ) {
		ajax_return(5,'时间太久了，已超出退货期限。');
	}
	
	// 更新订单信息
	change_state($order['oid'], -1);
	
	// 添加订单取消记录
	$opt = C::t('common_member_order_cancel')->insert(array(
		'oid'	=> $order['oid'],
		'uid'	=> $uid,
		'type'	=> $cancel_reason_type ? $cancel_reason_type : -1,
		'note'	=> $cancel_note,
		'state'	=> 0,
		'dateline'	=> time(),
		'reason'	=> '',
	));
	
	if ($opt) {
		sendOrderMessage('returnordersure',$order);
		ajax_return(1,'成功提交退货申请。');
	}
	else {
		ajax_return(6,'处理出错，请稍后再试。');
	}
	
}
// 将微信头像本地化
elseif($_GET['action'] == 'updataAvatar'){
	if($_G['uid']){
		$wx = $_G['wechatuser'];
		if($wx['isheadimglocal']==0){
			//远程获取头像
			require_once libfile('function/member');
			$url = substr($wx['headimgurl'],0,strlen($wx['headimgurl'])-1).'132';
			if($wx['headimgurl'] && setAvatarFromRemoteUrl($url,$_G['uid'])){
				C::t('common_member_wechat')->update($wx['id'],array('isheadimglocal'=>1));
				ajax_return(1,'处理完毕',$url);
			}
		}
	}
	ajax_return(0);
}
// 获取商品帖子
elseif($_GET['action'] == 'getThreadByspid'){
	
	require_once libfile('function/discuzcode');
	
	$spids = $_GET['spids'];
	$isall = isset($_GET['isall']) ? true : false;
	$start = isset($_GET['start']) ? intval($_GET['start']) : 0;
	$pagesize = isset($_GET['pagesize']) ? intval($_GET['pagesize']) : 0;
	$tid = isset($_GET['tid']) ? intval($_GET['tid']) : 0;
	
	$spids = explode(',', $spids);
	
	// 获取商品相关联的帖子
	if($isall){
		$threads = C::t('forum_thread')->fetch_all_by_spids($spids, $tid, $start);
	}else{
		$threads = C::t('forum_thread')->fetch_all_by_spids($spids, $tid, $start, $pagesize);
	}
	
	foreach($threads as $k=>$thread){
		$threads[$k]['media_list'] = json_decode(htmlspecialchars_decode($thread['media_list']), 1);
		$threads[$k]['avatar'] = avatar($thread['authorid']);
		$threads[$k]['summary']	= discuzcode($threads[$k]['summary']);
	}
	ajax_return(1,'', $threads);
}
// 注册
else if ( $_GET['action'] == 'register' ) {
	
	$param_input	= isset($_POST['input']) ? $_POST['input'] : '';
	$param_type		= isset($_POST['type']) ? $_POST['type'] : '';
	$param_password	= isset($_POST['password']) ? $_POST['password'] : '';
	
	$result = array(
		'res'	=> 0,
		'msg'	=> '',
		'data'	=> array()
	);
	
	if ( empty($param_input) || empty($param_type) || empty($param_password) ) {
		$result['msg'] = '缺少参数';
		ajax_return($result['res'], $result['msg'], $result['data']);
		exit();
	}
	
	$domain = $_SERVER['HTTP_HOST'];
	$url = 'http://'.$domain.'/app.php?mod=member&act=register&input=' .$param_input. '&type=' .$param_type. '&password=' .$param_password;
	
	$curl = new curl();
	$curl->request_init($url);
	$result = $curl->request_action();
	echo $result;
	exit();
}
// 完成新手指导
else if ( $_GET['action'] == 'finish_beginner_guide' ) {
	
	// 检查用户ID
	if ( !$_G['uid'] ) {
		echo '0';
		exit();
	}
	
	$uid = $_G['uid'];
	
	require_once libfile('function/product');
	require_once libfile('function/beginner_guide');

    // 完成的页面
	$guide_page = getgpc('guide_page');

    // 完成新手引导
	$result = finishBeginnerGuide($_G['uid'] , $guide_page);
	
	$return = array();
	if ($result) {
		
		$return['result'] = 1;
		
		// 奖励积分
		$credit_rule = C::t('common_credit_rule')->fetch_one_by_action('mobile_home_bg');
		if ($credit_rule) {
			
			$credit = & credit::instance();
			$credit->updatemembercount($credit_rule, $uid);
			
			$return['credit'] = $credit_rule['extcredits2'];
			//$member_count = C::t('common_member_count')->fetch_by_uid($uid);
            $member_count = $_G['member']['credits'];
			//var_dump($member_count);
			if ($member_count) {
				$return['credit_own'] = $member_count['extcredits2'];
			}
			
		}
		
		// 获取一个可换购的商品
		$one_integral_product = C::t('common_product_onsell')->fetch_one_integral_product();
		if ($one_integral_product) {
			$return['product_name'] = $one_integral_product['subject'];
			$return['product_price'] = $one_integral_product['price'];
			$product = getProduct($one_integral_product['pdid']);
			if ($product) {
				$media_list = getMediaList($product);
				if ( count($media_list) ) {
					$return['product_image'] = $media_list[0]['media_url'];
				}
			}
		}
	} else {
		$return['result'] = 0;
	}
	
	echo json_encode($return);
	exit();
	
} else if ( $_GET['action'] == 'debug' ) {
	
	var_dump($_G['member']);

	exit();
}

// dump user
/*
elseif($_GET['action'] == 'dump_user'){
	$sql = 'SELECT username,uid FROM '.DB::table('common_member').' ORDER BY uid';
	$query = DB::query($sql);
	$count = 0;
	while($user = DB::fetch($query)){
		$count++;
		$str = $user['username'].','.getnicknamebyuid($user['uid'])."\n";
		file_put_contents('d:/user', $str, FILE_APPEND);
	}
	echo '共导出'.$count.'个';
}
*/

showmessage('succeed', '', array(), array('handle' => false));

?>