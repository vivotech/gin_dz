<?php

/**
 *  任务模块
 */
if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

require_once libfile('function/mission');
require_once libfile('function/checkin');

$act_array = array(
	'list',	// 任务列表
	'item',	// 进行中的任务
);
$act = isset($_GET['action']) ? ( in_array(trim($_GET['action']),$act_array) ? trim($_GET['action']) : 'list' )  : 'list';

$uid = I('uid',$_G['uid'],'intval');
// 必须有UID
if (!$uid){
    showmessage('参数错误！');
}

// 任务列表
if ($act == 'list') {

    $isreward = isset($_GET['isreward']) ? ( in_array($_GET['isreward'],array(0,1)) ? $_GET['isreward'] : 0 ) : 0;

    $task_lists = get_task_list();
    $lists = array();
    foreach($task_lists as $key => $item){
        $taskuser = get_task_user($item['tid'], $_G['uid']);
        if($taskuser['isreward']==$isreward || ($isreward==0 && !$taskuser)){
            $item['user'] = $taskuser;
            $lists[] = $item;
        }
	}

//	// 获取会员签到记录
//	$gsignin_member = getUserGsigninMemberData($_G['uid']);
//
//	// 获取会员积分
//	$member_count = C::t('common_member_count')->fetch_by_uid($_G['uid']);

	include template('forum/task_list');
}
//任务详细
elseif ($act == 'item') {
	$tid = getgpc('tid');
	$task = get_task($tid);
	include template('forum/task_item_'.$task['type']);
}

