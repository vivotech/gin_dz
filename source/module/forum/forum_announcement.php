<?php

/**
 *  公告页面
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

require_once libfile('function/discuzcode');

$time = I('m',$_G['timestamp']);

$pagesize = I('pagesize',10,'intval');
$page = $_G['page'];

$count = C::t('forum_announcement')->count_by_date($time,1);
$pagecount = @ceil($count/$pagesize);
$announcedata = C::t('forum_announcement')->fetch_all_by_date($time,1,($page-1)*$pagesize,$pagesize);
//分页函数
$page_url = '/forum.php?mod=announcement'.($time!=$_G['timestamp']?'&m='.$time:'');
$multipage = multi($count,$pagesize,$page,$page_url,$pagecount);

if(!count($announcedata)) {
	showmessage('announcement_nonexistence');
}

$announcelist = array();
foreach ($announcedata as $announce) {
	$announce['authorenc'] = rawurlencode($announce['author']);
	$tmp = strtotime(dgmdate($announce['starttime'], 'Ym'));
	$months[$tmp] = $tmp;
//	if(!empty($_GET['m']) && $_GET['m'] != dgmdate($announce['starttime'], 'Ym')) {
//		continue;
//	}
	$announce['starttime'] = dgmdate($announce['starttime'], 'd');
	$announce['endtime'] = $announce['endtime'] ? dgmdate($announce['endtime'], 'd') : '';
	$announce['message'] = $announce['type'] == 1 ? "[url]{$announce[message]}[/url]" : $announce['message'];
	$announce['message'] = nl2br(discuzcode($announce['message'], 0, 0, 1, 1, 1, 1, 1));
	$announcelist[] = $announce;
}
//print_r($months);
$annid = isset($_GET['id']) ? intval($_GET['id']) : 0;

include template('forum/announcement');

?>