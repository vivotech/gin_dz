<?php

/**
 *  徽章模块
 */
if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

require_once libfile('function/url');
require_once libfile('function/badge');
require_once libfile('function/member');

$act_array = array(
	'index',	// 徽章列表
	'detail',	// 徽章详情
	'obtain',	// 获得徽章
    'debug'
);

$act = isset($_GET['action']) ? ( in_array(trim($_GET['action']),$act_array) ? trim($_GET['action']) : 'index' )  : 'index';

// 必须先登录
if (!$_G['uid']){
	header('Location: forum.php');
}

$uid = $_G['uid'];

// 获取个人资料
$profiles = getProfiles($uid);

// 获取等级
if (!$_G['cache']['usergroups']) {
    loadcache('usergroups');
}
$profiles['group']['stars'] = $_G['cache']['usergroups'][$profiles['groupid']]['stars'];

// 徽章列表
if ($act == 'index') {
	
	// 所需数据：徽章名，已点亮的徽章级别图标或未点亮的一级图标，是否已点亮，徽章级别
	
	$badges_list = getBadges();

	$badge_data = array();
	foreach ($badges_list as $badge_item) {
		$badge_data_item = array(
			'name'	=> $badge_item['name'],
			'badge_id'	=> $badge_item['badge']
		);
		$badge_member = C::t('badge_member')->fetch_by_badge_uid($badge_item['badge'], $uid);
		
		if (!$badge_member) {	// 没有任何获得记录
			$badge_data_item['badge_image'] = $badge_item['lv1_image'];		// 徽章级别图标
			$badge_data_item['high_light']	= false;						// 未点亮
			$badge_data_item['badge_lv']	= 0;
		}
		else {
			// 获取已点亮的徽章级别
			$badge_data_item['badge_lv']	= $badge_item['lv'];
			$badge_data_item['badge_image'] = $badge_item['lv'.$badge_member['lv'].'_image'];	// 徽章级别图标

			if ($badge_data_item['badge_lv']) {
				$badge_data_item['high_light']	= true;		// 已点亮
			} else {
				$badge_data_item['high_light']	= false;	// 未点亮
			}
		}
		
		$badge_data[] = $badge_data_item;
		
	}

	// 检查可以获取的徽章
	$badge_can_obtain = array();
	
	foreach ($badges_list as $badge_item) {
		$badge_level = checkBadgeCanObtain($badge_item, $uid);
		if ( $badge_level ) {
			$badge_can_obtain[] = array(
				'badge'	=> $badge_item,
				'lv'	=> $badge_level
			);
		}
	}

	include template('forum/badge_index');
}
// 徽章详情
else if ($act == 'detail') {
	$badge_id = getgpc('badge_id');
	
	$badge = getBadgeById($badge_id);
	if (!$badge) {
		$url = '/forum.php';
		redirect($url);
	}
	
	// 检查是否能获得
	$can_obtain = checkBadgeCanObtain($badge, $uid);
	
	// 检查可获得的徽章
	$badges_can_obtain = getCanObtainBadge($uid);
	
	switch ($badge['badge']) {

		case 'checkin' :
			require_once libfile('function/checkin');
			require_once DISCUZ_ROOT."/source/plugin/gsignin/table/table_gsignin_member.php";
			require_once DISCUZ_ROOT."/source/plugin/gsignin/function/function.php";
			$gsignin_member = C::t('gsignin_member')->fetch_by_uid($uid);
			if (!$gsignin_member) {
				$url = '/forum.php';
				redirect($url);
			}
			$checkin_total = $gsignin_member['total'];
			$user_rank = getGsigninRank($uid);
			include template('forum/badge_detail_checkin_badge');
			break;

		case 'share' :
			//$badge_member = C::t('badge_member')->fetch_by_badge_uid($uid);
			include template('forum/badge_detail_share_badge');
			break;

		case 'bubble_wine' :

            $bubble_wine_buy_amount = getBuyedProductSerieCount($uid, '气泡酒');

			include template('forum/badge_detail_bubble_wine_badge');
			break;

        case 'comment' :

            $comment_count = table_forum_post::count_rows(array(
                sprintf('authorid=%d', $uid),
                'first=0',
            ));
            include template('forum/badge_detail_comment');
            break;

        case 'saygood' :

            $saygood_count = C::t('home_saygood')->count_by_uid($uid);
            include template('forum/badge_detail_saygood');
            break;
	}
	
}
// 获得徽章
else if ( $act == 'obtain' ) {
	
	$badge_id = getgpc('badge_id');
	
	$badge_rule_model = badge_rule::getInstance($badge_id);
	
	if (!$badge_rule_model) {
		$url = '/forum.php';
		redirect($url);
	}
	
	if (!$badge_rule_model->canObtain($uid)) {
		echo '不够资格获取徽章';
	}
	else {
		$opt = $badge_rule_model->obtain($uid);
		
		if ($opt) {
			echo '成功获取徽章';
		} else {
			echo '获取徽章失败';
		}
	}

} else if ($act=='debug') {

    include_once libfile('function/member');
    $bubble_wine_buy_count = getBuyedProductSerieCount($_G['uid'], '气泡酒');
    var_dump($bubble_wine_buy_count);
}




