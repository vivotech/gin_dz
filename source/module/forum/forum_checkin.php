<?php
/**
 * Created by PhpStorm.
 * User: wen
 * Date: 2015/7/2
 * Time: 15:31
 */

/**
 *  徽章模块
 */
if(!defined('IN_DISCUZ')) {
    exit('Access Denied');
}

$act_array = array(
    'index',	// 签到主页
);

$act = isset($_GET['action']) ? ( in_array(trim($_GET['action']),$act_array) ? trim($_GET['action']) : 'index' )  : 'index';

// 必须先登录
if (!$_G['uid']){
    header('Location: forum.php');
}

$uid = $_G['uid'];

require_once libfile('function/url');
require_once libfile('function/member');

// 获取个人资料
$profiles = getProfiles($uid);

// 获取等级
if (!$_G['cache']['usergroups']) {
    loadcache('usergroups');
}

$profiles['group']['stars'] = $_G['cache']['usergroups'][$profiles['groupid']]['stars'];

require_once DISCUZ_ROOT."/source/plugin/gsignin/table/table_gsignin_member.php";

if ($act == 'index') {

    // 当前用户的签到情况
    $member_gsignin = C::t('gsignin_member')->fetch_first_by_uid($uid);
    $member_gsignin_today = C::t('gsignin_member')->fetch_first_by_uid_today($_G['uid']);

    // 签到排名前3名
    $memberTops = C::t('gsignin_member')->fetch_all_top_by_total($today);

    // 签到排名
    $memberBottoms = C::t('gsignin_member')->fetch_all_bottom_by_total(0 ,10);

    $top10_has_user = false;

    $member_top_data = array();
    foreach ($memberBottoms as $v)
    {
        $uid = (int)$v['uid'];
        if ( $_G['uid'] == $uid )
        {
            $top10_has_user = true;     // top 10 has current user
        }

        $user = getuserbyuid($uid);

        $user_name = '';

        $member_top_data[] = array(
            'checkin_total' => $v['total'],
            'checkin_continuous'    => $v['continuous'],
            'name'          => $user ? $user['username'] : '未知用户',
            'nickname'      => getnicknamebyuid($uid)
        );

    }

    include template('forum/checkin_index');
}


