<?php

if (!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

//require_once libfile('function/forumlist');
require_once  libfile('function/product');

$act_array = array(
	'productseries', 	// 商品分类页
	'list', 			// 商品列表页
	'view', 			// 商品详情页
	'creditlist',		// 积分换购商品列表页
);
$act = isset($_REQUEST['action']) ? (in_array(trim($_REQUEST['action']), $act_array) ? trim($_REQUEST['action']) : 'view') : 'view';

if($act == 'productseries'){
	$inputPsid = $_GET['psid'] > 0 ? intval($_GET['psid']) : 0;
	// 获取商品列表
	require_once libfile('function/productserieslist');
	$productseries = productseriesselect(1, 1);
	
	
	include template('forum/productseries');
}
elseif ($act == 'list') {

	$pagesize = I('pagesize', 10, 'intval');
	$page = I('page', 1, 'intval');
	$model = C::t('common_product_onsell');
	$psid = $_GET['psid'] > 0 ? intval($_GET['psid']) : 0;
	$psup = $_GET['psup'] > 0 ? intval($_GET['psup']) : 0;

	$where = " sp.type = 0 AND sp.closed = 0 AND (sp.expiration > " . TIMESTAMP. " OR sp.expiration <= 0)";
	if(!empty($psid)){
		$where .= ' AND p.`psid` = '.$psid;
	}
	
	$count = $model -> count_onsell($psid);
	$pagecount = @ceil($count / $pagesize);
	
	//	if ($page > $pagecount) {
	//		sendAppMessage(RES_YES, 'app_page_bottom');
	//	}

	$list = $model -> fetch_all_page($page, $pagesize, $where);
	//剩余时间
	$left_time = array();
	foreach ($list as $lk => $lv) {
		$list[$lk]['media_list'] = json_decode(htmlspecialchars_decode($lv['media_list']), 1);
		$list[$lk]['main_pic'] = $list[$lk]['media_list'] ? $list[$lk]['media_list'][0]['media_url'] : '';
		$list[$lk]['costprice'] = number_format($lv['costprice'], 2);
		//处理折扣
		$list[$lk]['left_count'] = buyableNum($lv['spid']);
		$list[$lk]['left_time'] = get_left_time($lv['dateline'], $lv['expiration']);
		$left_time[$lv['spid']] = $list[$lk]['left_time'];
		$list[$lk]['discount_price'] = getDiscountPrice($lv['discount'] == 1 ? 0 : $lv['spid'], $lv['price'], $pv['discount']);
	}

	if(defined('IN_MOBILE') && $page > 1){
		$res = array('result'=>empty($list)?0:1,'list'=>$list);
		echo json_encode($res, JSON_UNESCAPED_UNICODE);
		exit();
	}else{
		include template('forum/product_list');
	}
}
//商品详情
elseif ($act == 'view') {
	$spid = isset($_GET["spid"]) ? (int)$_GET["spid"] : 0;
	$product_id = isset($_GET["pdid"]) ? (int)$_GET["pdid"] : 0;
	if (empty($product_id) && empty($spid)) {
		exit('参数错误');
	}
	// get product onsell record
	if($spid){
		$productonsell = C::t('common_product_onsell')->fetch($spid);
		$product_list = getProductList($productonsell['pdid']);
		foreach($product_list as $p){
			if($p['spid'] == $spid){
				$product = $p;
				break;
			}
		}
	}else if($product_id){
		$product = getProduct($product_id);
	}
	
	// 记录浏览关系
	$relation_sid = ($_GET['sid']) ? intval($_GET['sid']) : 0;
	$relation_pdid = $product['pdid'];
	$relation_uid = ($_G['uid']) ? $_G['uid'] : 0;
	$relation_ip = ($_G['clientip']) ? $_G['clientip'] : '';
	$insert_data = array(
		'uid'=>$_G['uid'],
		'pdid'=>$relation_pdid,
		'sid'=>$relation_sid,
		'dateline'=>TIMESTAMP,
		'ip'=>$relation_ip
	);
	C::t('common_member_browsing')->insert($insert_data);
	
	// 记录商铺关系
	if($_GET['from'] == 'qrcode'){
		if(!empty($relation_sid) && !empty($relation_pdid) && !empty($relation_uid)){
			// 查看有没有
			$this_relation = C::t('dealer_sale_relation')->fetch_by_uid_pdid($relation_uid, $relation_pdid);
			if(!$this_relation){
				$insert_data = array(
					'uid'=>$relation_uid,
					'pdid'=>$relation_pdid,
					'sid'=>$relation_sid,
					'dateline'=>TIMESTAMP,
					'refreshdate'=>TIMESTAMP
				);
				C::t('dealer_sale_relation')->insert($insert_data);
			}else{
				$update_data = array(
					'sid'=>$relation_sid,
					'refreshdate'=>TIMESTAMP
				);
				C::t('dealer_sale_relation')->update($this_relation['srid'], $update_data);
			}
		}
	}
	
	if(!$product) {
		exit('没有该商品！');
	}
	
	// get buy able number  获取可以购买的数量
	if($product['type'] == 1){
		$buyAbleNum = buyableNumCredits($product['spid']);
	}else{
		$buyAbleNum = buyableNum($product['spid']);
	}

	$product['dateline'] = $product['dateline'] ? $product['dateline'] : 0;
	$product['expiration'] = $product['expiration'] ? $product['expiration'] : 0;
	$product['discount_price'] = empty($product['discount_price']) ? $product['costprice'] : $product['discount_price'];
	
	$product['desc'] = htmlspecialchars_decode($product['desc']);
	$bbcodeModel = new bbcode();
	$product['desc'] = $bbcodeModel->bbcode2html($product['desc'], 2);

	// get posts
	// $threadTable = new table_forum_thread();
	// $threadList = $threadTable -> fetch_rows(array('filters' => array('spid=' . $product_id)));

	$login_condition = ($_G['uid'] > 0) ? true : false;
	$buycode = isBuyable($spid, $buyAbleNum);
	$buy_condition = ($buycode != 0);
	// display
	include template('forum/product');
}
// 积分换购商品
else if($act == 'creditlist'){
	$uid = $_G['uid'];
	$user_credits = getuserprofile('extcredits'.$_G['setting']['creditstrans']);
	
	if(!$uid){
		showmessage('');
	}
	$profile = $_G['member'];
	
	if(!$_G['cache']['productonsell']){
		loadcache('productonsell');
	}
	if(!$_G['cache']['product']){
		loadcache('product');
	}
	
	$products = array();
	$productonsells = &$_G['cache']['productonsell'];
	foreach($productonsells as $p){
		if($p['type'] == 1){
			$isbuyable = isBuyable($p['spid'], 1);
			if($isbuyable === 0){
				$products[] = array_merge($p, $_G['cache']['product'][$p['pdid']]);
			}
		}
	}
	include template('forum/product_credit_list');
}
