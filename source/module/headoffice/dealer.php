<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

$action = $_GET['action'] ? $_GET['action'] : 'viewdealer';

$act_arr = array(
	'viewdealer',	// 查看经销商列表
	'edit',			// 编辑
);

if ( !in_array($action, $act_arr) ){
	showmessage('非法操作');
}

require_once libfile('function/dealer');

$message = '';
$error = '';

require_once libfile('function/url');

/**
 * 保存编辑的动作（新建或修改经销商）
 */
function actionSave() {
	
	$duid = getgpc('duid');
	$name = getgpc('name');
	$phone = getgpc('phone');
	$password = getgpc('password');
	$password2 = getgpc('password2');
	$deleted = (int)getgpc('deleted');
	if ( $deleted<0 || $deleted>1 ) {
		$deleted = 0;
	}
	
	global $action;
	
	global $message;
	global $error;
	
	// 如果有指定经销商则获取它
	$dealer = null;
	if ($duid) {
		$dealer = C::t('dealer_member')->fetch_by_duid($duid);
		if (!$dealer) {
			$duid = '';
		}
	}
	
	// 修改经销商
	if ($dealer) {
		
		// 检查输入数据
		if ( !strlen($name) ) {
			$error = '请输入经销商名称';
			return;
		}
		
		// 设置新的数据
		$new_data = array(
			'company'	=> $name,
			'deleted'	=> $deleted
		);
		$opt = C::t('dealer_member')->update($dealer['duid'], $new_data);
		
		if ( $opt ) {
			$message = '修改成功。';
		} else {
			$message = '修改失败。';
		}
		
	}
	// 新建经销商
	else {
		
		// 检查输入数据
		if ( !strlen($name) ) {
			$error = '请输入经销商名称';
			return;
		}
		
		if ( !strlen($phone) ) {
			$error = '请输入手机号码';
			return;
		}
		
		if ( !strlen($password) ) {
			$error = '请输入密码';
			return;
		}
		
		if ( $password != $password2 ) {
			$error = '两次输入的密码不匹配';
			return;
		}
		
		// 设置 API 的注册网址
		global $_G;
		$host = $_G['config']['db']['1']['dbhost'];  // $_G['config']['wechat_url']
		$url_register = 'http://' .$host. '/app.php?mod=member&act=register&input=' .$phone. '&type=mobile&password='.$password;
		
		// 调用API注册新帐号
		$oCurl = new curl();
		$oCurl->request_init($url_register);
		$result = json_decode($oCurl->request_action(), true);
		if ( !$result['result'] ) {
			$error = $result['msg'];
			return;
		}
		
		// 获取用户帐号信息并创建一个经销商帐号
		$user_data = $result['data'];
		$insert_id = C::t('dealer_member')->insert(array(
			'uid'	=> $user_data['uid'],
			'company'	=> $name,
			'deleted'	=> $deleted
		), true);
		
		if ($insert_id) {
			// 重定向到新建的经销商编辑页
			redirect('/' .CURSCRIPT. '.php?mod=dealer&action='.$action.'&duid=' . $insert_id);
		} else {
			$error = '创建经销商用户失败。';
		}
		
	}
	
}

// 经销商列表页
if ($action == 'viewdealer'){
	
	$delaers_list = C::t('dealer_member')->fetch_child_dealers();
	
	include template('headoffice/viewdealer');
}
// 编辑页
else if ( $action == 'edit' ) {
	
	$duid = isset($_GET['duid']) ? (int)$_GET['duid'] : 0;
	$name = getgpc('name');
	
	if ($name) {
		actionSave();
	}
	
	if ($duid) {
		$dealer = C::t('dealer_member')->fetch_by_duid($duid);
		if ($dealer){
			$dealer_mobile_record = C::t('common_member_identifying')->fetch_verified_mobile_by_uid($dealer['uid']);
			if ($dealer_mobile_record) {
				$dealer['phone'] = $dealer_mobile_record['input_value'];
			} else {
				$dealer['phone'] = '';
			}
		} else {
			$dealer = array(
				'duid'	=> 0,
				'phone'	=> ''
			);
		}
	} else {
		$dealer = array('duid'=>0);
	}
	
	include template('headoffice/editdealer');
	
}


