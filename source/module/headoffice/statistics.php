<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

// 可用的统计类型
$type_array = array(
	'deal',		// 交易销售统计
);

// 统计类型
$statistics_type = getgpc('type');
$statistics_type ? null : $statistics_type = 'guest';
in_array($statistics_type, $type_array) ? null : $statistics_type = 'guest';

// 日期范围
$range_type = getgpc('range_type');

// 日期类型
$date_type = getgpc('date_type');
in_array($date_type, array(
	'day',	// 按日
	'month'	// 按月
)) ? null : $date_type = 'day';

// 获取要查询的年月日
$day	= $_GET['day'] ? $_GET['day'] : date('Y-m-d');
$month	= $_GET['month'] ? intval($_GET['month']) : date('n');
$year	= $_GET['year'] ? intval($_GET['year']) : date('Y');

// 检查输入数据
if ( $date_type == 'day' ) {
	
	// 如果查询的日期超过今天，则设置为今天
	if ( strtotime($day) > time() ) {
		$day = date('Y-m-d');
	}
}
else if ( $date_type == 'month' ) {
	
	if ( strtotime($year.'-'.$month) > time() )
	{
		$year	= date('Y');
		$month	= date('m');
	}
}

// 载入数据
require_once libfile('function/dealer');
require_once libfile('function/dealercache');
require_once libfile('function/chart');

$this_shop = getShop($_G['dealer']['duid']);

// 获取经销商列表
$dealers = C::t('dealer_member')->fetch_child_dealers();

// 销售统计
if ( $statistics_type === 'deal' ){
	
	// 所有经销商的分成统计数据
	$dealers_rebate_date = array();
	
	// 按日统计
	if($date_type == 'day'){
		
		$date = $day;
		
		// 遍历每一个经销商
		foreach ($dealers as $dealer) {
			
			// 获取经销商的日统计数据
			$dealer_rebate_date = getDealerRebateDay($dealer['duid'], $date, true);
			
			// 设置该经销商的统计数据
			$dealers_rebate_date[]	= array(
				'duid'	=> $dealer['duid'],						// 经销商ID
				'dealer_company'	=> $dealer['company'],		// 经销商名称
				'count'	=> $dealer_rebate_date['count'],		// 总交易量
				'total'	=> $dealer_rebate_date['total'],		// 总交易额
				//'profit'	=> $dealer_rebate_date['profit'],	// 总利润额(暂时不用)
				'rebate'	=> $dealer_rebate_date['rebate'],	// 总分成额
				'browsing_amount'	=> $dealer_rebate_date['browsing_amount']	// 总扫码量
			);
		}
		
		$headoffice_rebate_date = getDealerRebateDay(1, $date, true);
		
	}else if ($date_type == 'month'){
		
		// 遍历每一个经销商
		foreach ($dealers as $dealer) {
			
			// 获取经销商的月统计数据
			$dealer_rebate_date = getDealerRebateMonth($dealer['duid'], $year, $month);
			
			// 设置该经销商的统计数据
			$dealers_rebate_date[]	= array(
				'duid'	=> $dealer['duid'],						// 经销商ID
				'dealer_company'	=> $dealer['company'],		// 经销商名称
				'count'	=> $dealer_rebate_date['count'],		// 总交易量
				'total'	=> $dealer_rebate_date['total'],		// 总交易额
				//'profit'	=> $dealer_rebate_date['profit'],	// 总利润额(暂时不用)
				'rebate'	=> $dealer_rebate_date['rebate'],	// 总分成额
				'browsing_amount'	=> $dealer_rebate_date['browsing_amount']	// 总扫码量
			);
			
		}
		
		$headoffice_rebate_date = getDealerRebateMonth(1, $date, true);
		
	}
	
	// 载入模板
	include template(CURSCRIPT.'/statistics_deal');
}

