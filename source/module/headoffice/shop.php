<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

$action = $_GET['action'] ? $_GET['action'] : 'index';
$type = getgpc('type');

$act_arr = array(
	'index',		// 查看经销商列表
	'edit',			// 编辑
);

if ( !in_array($action, $act_arr) ){
	showmessage('非法操作');
}

$message = '';
$error = '';

require_once libfile('function/dealer');
require_once libfile('function/url');

/**
 * 保存编辑的动作（新建或修改店铺）
 */
function actionSave() {
	
	$sid = getgpc('sid');
	$duid = (int)getgpc('duid');
	$name = getgpc('name');
	$startdate = getgpc('startdate');
	$deleted = (int)getgpc('deleted');
	
	global $action;
	
	global $message;
	global $error;
	
	if (!$duid) {
		redirect('/'.CURSCRIPT.'.php');
		return;
	}
	
	if ($sid) {
		$shop = C::t('dealer_member_shop')->fetch_by_sid($sid);
		if (!$shop) {
			redirect('/'.CURSCRIPT.'.php?mod=shop');
			exit();
		}
	} else {
		$shop = array('sid'=>0);
	}
	
	if (!$name) {
		$error = '请输入店铺名称。';
		return;
	}
	
	// 检查开店日期
	if (!$startdate) {
		redirect('/'.CURSCRIPT.'.php?mod=shop&action=edit');
		exit();
	}
	$startdate =strtotime($startdate);
	if (!$startdate) {
		redirect('/'.CURSCRIPT.'.php?mod=shop&action=edit');
		exit();
	}
	

	if ($shop['sid']) {
		
		if ( $deleted < 0 || $deleted > 1) {
			$deleted = 0;
		}
		
		$new_data = array(
			'name'	=> $name,
			'startdate'	=> $startdate,
			'deleted'	=> $deleted
		);
		
		$opt = C::t('dealer_member_shop')->update($shop['sid'], $new_data);
		
		if ($opt) {
			$message = '保存成功。';
		}
		else {
			$error = '保存失败。';
		}
	}
	// 新增店铺
	else {
		
		$new_data = array(
			'name'	=> $name,
			'duid'	=> $duid,
			'startdate'	=> $startdate,
			'dateline'	=> time()
		);
		
		$opt = C::t('dealer_member_shop')->insert($new_data, true);
		
		if ($opt) {
			$message = '保存成功。';
			redirect('/'.CURSCRIPT.'.php?mod=shop&action=edit&duid='.$duid.'&sid='.$opt);
			return;
		}
		else {
			$error = '保存失败。';
		}
	}
	
}

// 店铺列表
if ( $action == 'index' ) {
	
	// 获取经销商ID
	$duid = getgpc('duid');
	if (!$duid) {
		redirect('/'.CURSCRIPT.'.php');
		exit();
	}
	
	// 获取经销商
	$dealer = C::t('dealer_member')->fetch_by_duid($duid);
	if (!$dealer) {
		redirect('/'.CURSCRIPT.'.php');
		exit();
	}
	
	// 获取经销商的店铺
	$shops = getShop($duid, false);
	
	include template('headoffice/shop/index');
	
}
// 店铺编辑
else if ( $action == 'edit' ) {
	
	// 获取经销商ID
	$duid = getgpc('duid');
	if (!$duid) {
		redirect('/'.CURSCRIPT.'.php');
		exit();
	}
	
	// 获取经销商
	$dealer = C::t('dealer_member')->fetch_by_duid($duid);
	if (!$dealer) {
		redirect('/'.CURSCRIPT.'.php');
		exit();
	}
	
	if ( count($_POST) ) {
		actionSave();
	}
	
	$sid = getgpc('sid');
	
	if ($sid) {
		$shop = C::t('dealer_member_shop')->fetch_by_sid($sid);
		if (!$shop) {
			redirect('/'.CURSCRIPT.'.php?mod=shop&action=edit');
			exit();
		}
	} else {
		$shop = array(
			'sid'	=> 0
		);
	}
	
	include template('headoffice/shop/edit');
	
}



