<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}
define('NOROBOT', TRUE);

// send identifying code action
$action = isset($_GET['action']) ? $_GET['action'] : '';

if( $action === 'send_identifying_code') {
  
  $return = array(
    "result"  => 0,
  );
  
  // get mobile param
  $mobile = isset($_GET['mobile']) ? $_GET['mobile'] : "";
  $identifying = new identifying('moblie',$mobile,6,$_G['uid']);
  
  if (!$identifying->checkFormat())
  {
    $return["error"] = "手机号码格式不正确。";
    echo json_encode($return, JSON_UNESCAPED_UNICODE);
    return;
  }

  // check if the mobile is used
  if ($identifying->checkHaveOne()){
    $return["result"] = 10004;
    $return["error"] = "该手机号已被注册";
    echo json_encode($return, JSON_UNESCAPED_UNICODE);
    return;
  }

  
  // check send time
  if (!$identifying->checkTimesInSecound(60))
  {
    $return["result"] = 20003;
    $return["error"] = "请过60秒后再操作。";
    echo json_encode($return, JSON_UNESCAPED_UNICODE);
    return;
  }
  
  //1个小时只能发3条
  if (!$identifying->checkTimesInArea(3))
  {
    $return["result"] = 20003;
    $return["error"] = "1个小时内只能发3条验证码，请稍后再试！";
    echo json_encode($return, JSON_UNESCAPED_UNICODE);
    return;
  }
  
  if ($identifying->send())
  {
     $return["result"] = 1;
  }
  else
  {
      $return["error"] = "更新数据库失败";
  }
  echo json_encode($return, JSON_UNESCAPED_UNICODE);
}
// submit identifying code action
else if( $action === 'submit_identifying_code') {
  
  $return = array(
    "result"  => 0,
  );
  
  // get mobile param
  $mobile = isset($_GET['mobile']) ? $_GET['mobile'] : "";
  $identifying = new identifying('moblie',$mobile,6,$_G['uid']);
  
  if (!$identifying->checkFormat()){
    $return["error"] = "手机号码格式不正确。";
    echo json_encode($return, JSON_UNESCAPED_UNICODE);
    return;
  }
  
  // check if the mobile is used
  if ($identifying->checkHaveOne())
  {
    $return["result"] = 10004;
    $return["error"] = "该手机号已被注册";
    echo json_encode($return, JSON_UNESCAPED_UNICODE);
    return;
  }
  
  // check time out
  if (!$identifying->checkTimeOut())
  {
    $return["error"] = "验证码已过期";
    echo json_encode($return, JSON_UNESCAPED_UNICODE);
    return;
  }
  
  // check identifying code
  $identifying_code = isset($_GET['identifying_code']) ? $_GET['identifying_code'] : "";
  if (!$identifying->checkMatch($identifying_code))
  {
    $return["error"] = "验证码不正确";
    echo json_encode($return, JSON_UNESCAPED_UNICODE);
    return;
  }
  
  if ($identifying->setSureVerify())
  {
  	  //更新discuz原有的手机字段
      $identifying -> updateDZmobile($_G['uid']);
      $return['result'] = 1;
  }
  else
  {
    $return['error'] = "数据库更新失败";
  }
  
  echo json_encode($return, JSON_UNESCAPED_UNICODE);
  
}
// 获取url
else if($action === 'get_operator_url'){
	require_once libfile('function/forum');
	$type = (empty($_GET['type'])) ? '' : $_GET['type'];
	$op = (empty($_GET['op'])) ? '' : $_GET['op'];
	$id = (empty($_GET['id'])) ? 0 : $_GET['id'];
	$result = 1;
	$return_url = '';
	switch($type){
		case 'favorite':
			if($op == 'add'){
				//判断该用户是否已经收藏
				$is_favorite = check_favorite_by_id($id);
				if(!empty($is_favorite['favid'])){
					$return_url = 'home.php?mod=spacecp&ac=favorite&type=thread&favid='.$is_favorite['favid'].'&op=delete&formhash='.FORMHASH;
				}else{
					$result = 0;
				}
			}else if($op == 'cancle'){
				$return_url = 'home.php?mod=spacecp&ac=favorite&type=thread&id='.$id.'&formhash='.FORMHASH;
			}
			break;
		case 'saygood':
			if($op == 'add'){
				$is_saygood = check_saygood_by_id($id);
				if(!empty($is_saygood['saygood_id'])){
					$return_url = 'home.php?mod=spacecp&ac=saygood&type=thread&op=delete&saygood_id='.$is_saygood['saygood_id'].'&formhash='.FORMHASH;
				}else{
					$result = 0;
				}
			}else if($op == 'cancle'){
				$return_url = 'home.php?mod=spacecp&ac=saygood&type=thread&id='.$id.'&formhash='.FORMHASH;
			}
			break;
		default:
			$return_url = '';
	}
	
	echo json_encode(array('result'=>$result,'data'=>$return_url));
	exit;
}
