<?php
/**
 * Created by PhpStorm.
 * User: wen
 * Date: 2015/7/17
 * Time: 14:45
 */

if(!defined('IN_DISCUZ')) {
    exit('Access Denied');
}
define('NOROBOT', TRUE);

$action = isset($_GET['action']) ? $_GET['action'] : '';

if (!$action)
{
    $action = 'index';
}

// 个人信息
$profile = $_G['member'];
$_G['personal_bg'] = $_G['member']['personal_bg'];

//检查是否已经绑定手机
$binded_mobile = empty($profile['mobile']) ? FALSE : TRUE;

// 获取等级
if (!$_G['cache']['usergroups']) {
    loadcache('usergroups');
}
$profile['group']['stars'] = $_G['cache']['usergroups'][$profile['groupid']]['stars'];
// 获取对应的积分
$profile['extcredit'] = C::t('common_member_count') -> fetch($uid);

if ($action==='index') {
    include template('home/config_index');
}


