<?php

/**
 * 微信绑定界面
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

if(!in_array($_GET['action'], array('bind','verifycode','dobind'))) {
	showmessage('undefined_action');
}

if(!defined('IN_MOBILE') || !defined('IN_WECHAT')){
	showmessage('请在微信中使用！');
}

if(!$_G['uid']){
	showmessage('请先登录！');
}

$action = $_GET['action'];

//微信绑定页面
if($action=='bind'){
	include template('member/wechat');
}
////验证码
elseif($action == 'verifycode'){
	
	$phone = I('cellphone',0);
	if($phone==0){
		ajax_return(0,'手机号码为空');
	}
	
	$identy = new identifying('mobile',$phone,5,$_G['uid']);
	
	if(!$identy->checkFormat()){
		ajax_return(0,'手机号码格式不正确，请检查！'.($identy->checkFormat()));
	}
	
	//不检查是否存在 checkHaveOne();
	//直接检查一个小时内有没有超过3条
//	if(!$identy->checkTimesInArea()){
//		ajax_return(0,'超过发送时限');
//	}
	
	if($identy->send()){
		ajax_return(1,'发送成功！');
	}
	
}
//绑定
elseif($action == 'dobind'){
	
	$binddata = array(
				'phone'		=> I('phone',''),
				'snscode'	=> I('snscode',''),
			);
	
	$identy = new identifying('mobile',$binddata['phone'],5,$_G['uid']);
	
	if(!$identy->checkTimeOut()){
		ajax_return(0,'验证码已超时！');
	}
	
	if(!$identy->checkMatch($binddata['snscode'])){
		ajax_return(0,'验证码错误！');
	}

	if(!check_mobile_number($binddata['phone'])){
		ajax_return(0,'手机格式错误！！' );
	}
	
	
	if ($identy->setSureVerify()){
		//更新discuz原有的手机字段
		$identy -> updateDZmobile($_G['uid']);

		ajax_return(1,'绑定成功' );
	}
	else{
		ajax_return(0,'数据库更新失败！！' );
	}

}
	