<?php

/**
 * APP 接口会员操作
 */
 
if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}
 
error_reporting(E_ERROR);

include_once libfile('app/member','class');
include_once libfile('function/member');

$allow_action_array = array(
	'gethash',					//获取hash
	'login','logout',			//登陆
	'register','verifycode',	//注册
	'profile',	//获取个人资料，更新个人资料
	'userinfo', 'uploadavatar',//获取其他用户基础资料
    'updateavatar',             // 更新头像
	'signin',					//签到
	'setpushuid',				//设置百度云推送UID
);

$app_action = in_array(APPACT , $allow_action_array) ? APPACT : 'login' ;

////验证权限
//$_sign = I('_sign','');
////不需要_sign就能操作行为
//$no_sign_action_list = array('login','logout','register','verifycode');
////验证_sign
//if(!in_array($app_action, $no_sign_action_list)){
//	$app_sign = new app_sign();
//	if(!$app_sign->check_sign($_sign)){
//		sendAppMessage(RES_NO, 'app_sign_error', array('time'=>time()));
//	}
//}

//检查登陆状态
$no_login_action_array = array('login','logout','register','verifycode','userinfo');
if(!in_array($app_action,$no_login_action_array)){
	// 判断用户是否登录
	if(empty($_G['uid'])){
		sendAppMessage(RES_NO, array(FALSE,'请先登陆！') , array('is_signin'=>0));
		exit;
	}
}

if($app_action == 'gethash'){
	if($_G['uid']){
		$formhash =  formhash();
		sendAppMessage(RES_YES, array(FALSE,'获取FORMHASH成功！') , $formhash);
	}
	sendAppMessage(RES_NO, array(FALSE,'获取FORMHASH失败!'));
}

//登陆
elseif($app_action == 'login' || $app_action == 'logout'){
	
	$logging = new app_logging_ctl();
	$logging->setting = $_G['setting'];
	$method = 'on_'.$app_action;

	//设置返回数据类型
	$logging->returntype = "JSON";
	$logging->$method();
}

//注册
elseif($app_action == 'register' || $app_action == 'verifycode'){
	
	$register = new app_register_ctl();
	$register->setting = $_G['setting'];
	$method = 'on_'.$app_action;

	//设置返回数据类型
	$register->returntype = "JSON";
	$register->$method();
	
}
//获取个人资料
elseif($app_action == 'profile'){

	//引入资料页面
	include libfile('app/profile','include');

}
//获取单个用户资料
elseif($app_action == 'userinfo'){

	$uid = I('uid',0,'intval');
	require_once libfile('function/forum');

	$return = get_user_info_by_uid($uid,TRUE);

	if(!$return){
		sendAppMessage(RES_NO, 'app_get_userinfo_error', array('uid'=>$uid));
	}

	sendAppMessage(RES_YES, 'app_get_userinfo_success', $return);
}
// 更新头像
elseif($app_action == 'updateavatar'){

    $uid = $_G['uid'];
    if (!$uid) { return; }
    $avatar_url = getgpc('avatar');
    $opt = setAvatarFromRemoteUrl($avatar_url,$uid);

    if ($opt)
    {
        echo '1';
    }
    else
    {
        echo '0';
    }

    return;
}
//上传头像
elseif($app_action == 'uploadavatar' ){
	
	define('UC_DATADIR', DISCUZ_ROOT . 'uc_server/data/');
	define('UC_DATAURL', DISCUZ_ROOT . 'uc_server/data');
	
	$uid = $_G['uid'];
	
	/////////////////////////////////临时使用
	
//	$authorid =  I('authorid');
//	$user = getuserbyusername($authorid);
//	$uid = $user['uid'] ? $user['uid'] : $_G['uid'];
//	file_put_contents("e:/user.txt", $authorid.'('.var_export($user,true).')'."\n",FILE_APPEND);
	
	/////////////////////////////////
	
	$image_type = I('type','jpg');
	$imgtypearr = array('jpg','png','gif');
	$uploadtype = I('uploadtype','form');
	
	if(!in_array($image_type, $imgtypearr)){
		sendAppMessage(RES_NO,array(FALSE,'暂不支持'.$image_type.'格式！！请选用'.implode(',', $imgtypearr).'格式！'));
	}
	
	$avatar_temp_url = UC_DATAURL.'/tmp/upload'.$uid.'.'.$image_type;
	if(file_exists($avatar_temp_url)){
		@unlink($avatar_temp_url);
	}
	
	//如果不等于file，则使用二进制数据保存图片
	if($uploadtype != 'form'){
		
		//	file_put_contents('E:/te.txt', $_REQUEST['Filedata']);
	
		if(!isset($_REQUEST['Filedata'])){
			sendAppMessage(RES_NO,array(FALSE,'没有数据！！！,请检查！'));
		}
		
		//使用GD库中imagecreatefromstring函数来判断图片
		if(function_exists('imagecreatefromstring') && is_callable('imagecreatefromstring')){
			$is_img = imagecreatefromstring($_REQUEST['Filedata']);
			if(!$is_img){
				sendAppMessage(RES_NO,array(FALSE,'数据不为图片，请检查数据来源！'));
			}
		}
		
		//保存图片
		file_put_contents($avatar_temp_url, $_REQUEST['Filedata']);
	
	}else{
		//采用form格式上传图片
		
		if(empty($_FILES['Filedata'])) {
			sendAppMessage(RES_NO,array(FALSE,'没有数据！！！,请检查！'));
		}
		
		list($width, $height, $type, $attr) = getimagesize($_FILES['Filedata']['tmp_name']);
		if(!in_array($type, array(1,2,3,6))) {
			@unlink($_FILES['Filedata']['tmp_name']);
			sendAppMessage(RES_NO,array(FALSE,'文件格式不正确，请检查！'));
		}
		
		$imgtype = array(1 => '.gif', 2 => '.jpg', 3 => '.png');
		$filetype = $imgtype[$type];
		if(!$filetype) $filetype = '.jpg';
		$tmpavatar = UC_DATADIR.'./tmp/upload'.$uid.$filetype;
		file_exists($tmpavatar) && @unlink($tmpavatar);
		if(@copy($_FILES['Filedata']['tmp_name'], $tmpavatar) || @move_uploaded_file($_FILES['Filedata']['tmp_name'], $tmpavatar)) {
			@unlink($_FILES['Filedata']['tmp_name']);
			list($width, $height, $type, $attr) = getimagesize($tmpavatar);
			if($width < 10 || $height < 10 || $type == 4) {
				@unlink($tmpavatar);
				sendAppMessage(RES_NO,array(FALSE,'没有数据！！！,请检查！2'));
			}
		} else {
			@unlink($_FILES['Filedata']['tmp_name']);
			sendAppMessage(RES_NO,array(FALSE,'文件格式不正确，请检查！'));
		}
		//保存以下附件名
		$image_type = str_replace('.', '', $filetype);
	}
	
	//移动头像到正确的位置
	moveAvatar($uid,$avatar_temp_url,$image_type);
	
	//上传头像送积分 操作
	if(empty($_G['member']['avatarstatus']) && uc_check_avatar($_G['uid'], 'middle')) {
		C::t('common_member')->update($_G['uid'], array('avatarstatus'=>'1'));
		$credit = updatecreditbyaction('setavatar');
		manyoulog('user', $_G['uid'], 'update');
	}
	
	sendAppMessage(RES_YES,array(FALSE,'上传头像成功！'),array('avatar' => avatar($uid,'middle'),'addcredits'=>app_get_return_credict($credit)));
}
//签到
elseif($app_action == 'signin'){
	
	//用于判断是否签到
	$do = I('do','sign');
	
	require_once DISCUZ_ROOT."/source/plugin/gsignin/function/function.php";
	$set = $_G['cache']['plugin']['gsignin'];
	// 获取今天日期
	$today = strtotime(date('Y-m-d',TIMESTAMP));
	
	// 判断用户组是否有权限
    /*
	if(!in_array($_G['groupid'], unserialize($set['useGroups']))) {
		sendAppMessage(RES_NO, array(FALSE,'用户组权限不足！！'));
		exit;
	}*/
		
	$currentTime = TIMESTAMP;
	$member = C::t('#gsignin#gsignin_member')->fetch_first_by_uid($_G['uid']);
	if(empty($member)){
		$member = array(
			'uid'=>$_G['uid'],
			'continuous'=>0,
			'total'=>0,
			'difference'=>0,
			'lasttime'=>0,
			'isshield'=>0
		);
		C::t('#gsignin#gsignin_member')->insert($member);
	}
	//TODO 判断用户是否被屏蔽
	if($member['isshield']==1){
		sendAppMessage(RES_NO, array(FALSE,'用户已被屏蔽~'));
		exit;
	}

	// 获取今日签到排名
	$ranking = C::t('#gsignin#gsignin_signin')->fetch_ranking($currentTime,$today);
	$ranking = $ranking+1;

	// 获取前一次签到记录
	$prevSignin = C::t('#gsignin#gsignin_signin')->fetch_last_by_uid($_G['uid']);
	// 判断是否第一次签到
	if(empty($prevSignin)) {
		$prevSigninTime = 0;
		$difference = 0;
	} else {
		$prevSigninTime = $prevSignin['currenttime'];
		// TODO 因为修改字段原因添加了判断，发布的时候可以去掉
		if($prevSignin['ranking'] !=0 ){
			$difference = $ranking - $prevSignin['ranking'];
		}else {
			$difference = 0;
		}
	}
	
	//检查是否已经签到
	if($do == 'checksign'){
		sendAppMessage(RES_YES, array(FALSE,'签到判断'),$prevSigninTime>=$today?1:0);
		exit();
	}
	
	// 判断今日是否签到
	if( $prevSigninTime>=$today) {
		sendAppMessage(RES_NO, array(FALSE,'亲，您今日已经签到了哦！'));
		exit;
	}
	
	// 获得最后一次断签记录
	$breakSignin = C::t('#gsignin#gsignin_signin')->fetch_last_break_by_uid($_G['uid']);
	//debug(date('Y-m-d',$breakSignin['currenttime']));
	// 获得连续签到天数
	$prevContinuous = C::t('#gsignin#gsignin_signin')->fetch_continuous_by_uid($_G['uid'],$breakSignin['currenttime']);
	//debug($prevContinuous);
	// 判断本次签到是否连续签到
	if(strtotime(date('Y-m-d',$currentTime))-strtotime(date('Y-m-d',$prevSigninTime))>(60*60*24)) {
		$member['continuous'] = 1;
	}else {
		$member['continuous'] = $prevContinuous+1;
	}

	// 获取奖励设置
	$award =  C::t("#gsignin#gsignin_award")->fetch_first_by_continuous($member['continuous']);
	// 获取积分设置
	$extcredits = $_G['setting']['extcredits'];
	//积分记录
	$record_extcredits=array('result'=>true,'rulename'=>'每日签到','credicts'=>array());
	foreach($extcredits as $key => $val){
		if(!empty($award)){
			$record_extcredits['credicts']['extcredits'.$key] = $award['extcredits'.$key];
			$awardExtcredits['extcredits'.$key] = $award['extcredits'.$key];
		} else{
			$record_extcredits['credicts']['extcredits'.$key] = 0;
			$awardExtcredits['extcredits'.$key] = 0;
		}
		updatemembercount($_G['uid'], array('extcredits'.$key=>$award['extcredits'.$key]));
	}
	
	$pluginName = 'gsignin';
	$componentTable = '#gsignin#gsignin_component';
	$model = 'extend';

	if (file_exists('source/plugin/'.$pluginName.'/com/'.$model.'/'.$model.'.php')){
		$component= C::t($componentTable)->fetch_first_by_model($model);
		if (!empty($component) && $component['flag'] == 1){
			$extend = C::t('#gsignin#gsignin_extend')->fetch_first_by_id($ranking);
			if(!empty($extend)) {
				foreach($extcredits as $key => $val){
					$awardExtcredits['extcredits'.$key] += $extend['extcredits'.$key];
					updatemembercount($_G['uid'], array('extcredits'.$key=>$extend['extcredits'.$key]));
				}
			}
		}
	}

	$currentSignin = array(
		'uid'=>$_G['uid'],
		'prevtime'=>$prevSigninTime,
		'currenttime'=>$currentTime,
		'ranking'=>$ranking,
		'extcredits1'=>$awardExtcredits['extcredits1'],
		'extcredits2'=>$awardExtcredits['extcredits2'],
		'extcredits3'=>$awardExtcredits['extcredits3'],
		'extcredits4'=>$awardExtcredits['extcredits4'],
		'extcredits5'=>$awardExtcredits['extcredits5'],
		'extcredits6'=>$awardExtcredits['extcredits6'],
		'extcredits7'=>$awardExtcredits['extcredits7'],
		'extcredits8'=>$awardExtcredits['extcredits8'],
		'issupplement'=>0
	);

	// 插入本次签到数据
	C::t('#gsignin#gsignin_signin')->insert($currentSignin);

	$member['total'] = $member['total']+1;
	$member['difference'] = $difference;
	$member['lasttime'] = $currentTime;

	// 更新用户数据
	C::t('#gsignin#gsignin_member')->update($member['uid'],$member);
	
	// 判断是否开启签到发帖
	// APP中无需签到发帖
//		if(!empty($set['isPost'])) {
//			if(!empty($set['fid'])) {
//				$post = C::t('#gsignin#gsignin_post')->fetch_first_by_id(1);
//				if(empty($post) || $today > $post['time']) {
//					$tid = generatethread(lang('plugin/gsignin','s00058').date('Y'.lang('plugin/gsignin','year').'m'.lang('plugin/gsignin','month').'d'.lang('plugin/gsignin','fuck'),$today).lang('plugin/gsignin','s00059'),lang('plugin/gsignin','s00060').$ranking.lang('plugin/gsignin','s00061'),$_G['clientip'],$_G['uid'],'',$set['fid']);
//					if(empty($post)) {
//						$post = array('tid'=>$tid,'time'=>$today);
//						C::t('#gsignin#gsignin_post')->insert($post);
//					} else {
//						$post['tid'] = $tid;
//						$post['time'] = $today;
//						C::t('#gsignin#gsignin_post')->update($post['id'],$post);
//					}
//				} else {
//					generatepost(lang('plugin/gsignin','s00060').$ranking.lang('plugin/gsignin','s00061'), $_G['uid'], $post['tid'], '', '', '');
//				}
//			}
//		}

	$pluginName = 'gsignin';
	$componentTable = '#gsignin#gsignin_component';
	$model = 'auto';

	if (file_exists('source/plugin/'.$pluginName.'/com/'.$model.'/'.$model.'.php')){
		$component= C::t($componentTable)->fetch_first_by_model($model);
		if (!empty($component) && $component['flag'] == 1){
			// 判断自定签到是否有配置
			if(!empty($set['uids']) && !empty($set['prob'])) {
				if($set['prob'] > 0){
					$ranking += 1;
					$tempUids = explode('/',$set['uids']);
					$resultUids = array();
					$resultMember = array();
					foreach($tempUids as $key => $val) {
						$discuzMember = getuserbyuid(intval($val));
						if(!empty($discuzMember)) {

							$member = C::t('#gsignin#gsignin_member')->fetch_first_by_uid(intval($val));

							if(empty($member)){
								$member = array(
							'uid'=>intval($val),
							'continuous'=>0,
							'total'=>0,
							'difference'=>0,
							'lasttime'=>0,
							'isshield'=>0
								);
								C::t('#gsignin#gsignin_member')->insert($member);
							}


							// 获取前一次签到记录
							$prevSignin = C::t('#gsignin#gsignin_signin')->fetch_last_by_uid(intval($val));
							// 判断是否第一次签到
							if(empty($prevSignin)) {
								$prevSigninTime = 0;
								$difference = 0;
							} else {
								$prevSigninTime = $prevSignin['currenttime'];
								// TODO 因为修改字段原因添加了判断，发布的时候可以去掉
								if($prevSignin['ranking'] !=0 ){
									$difference = $ranking - $prevSignin['ranking'];
								}else {
									$difference = 0;
								}
							}
								
							// 判断今日是否签到
							if( $prevSigninTime<$today) {
								$resultUids[] = array(
									'uid'=>intval($val),
									'prevtime'=>$prevSigninTime,
									'currenttime'=>$currentTime+1,
									'ranking'=>$ranking,
									'extcredits1'=>0,
									'extcredits2'=>0,
									'extcredits3'=>0,
									'extcredits4'=>0,
									'extcredits5'=>0,
									'extcredits6'=>0,
									'extcredits7'=>0,
									'extcredits8'=>0,
									'issupplement'=>1
								);

								$resultMember[] = array(
							'continuous' => $member['continuous']+1,
							'total' => $member['total']+1,
							'difference' => $difference,
							'lasttime' => $currentTime+1
								);
							}
						}
					}
					if(!empty($resultUids)) {
						$tempRandom = rand(0,count($resultUids)-1);
						$resultRandom = rand(1,100);
						if($resultRandom <= $set['prob']) {
							C::t('#gsignin#gsignin_signin')->insert($resultUids[$tempRandom]);
							C::t('#gsignin#gsignin_member')->update($resultUids[$tempRandom]['uid'],$resultMember[$tempRandom]);
						}
					}
				}
			}
		}
	}
	sendAppMessage(RES_YES, array(FALSE,'签到成功！'),$record_extcredits);
}
//设置百度云推送UID
elseif($app_action == 'setpushuid'){
	$baidupush_uid = I('baidupush_uid','');
	//0为IOS，1为安卓
	$baidupush_platform = I('baidupush_platform',0,'intval',array(0,1));
	if(C::t('common_member_profile')->update_by_uid($_G['uid'],array('baidupush_uid'=>$baidupush_uid , 'baidupush_platform'=>$baidupush_platform))){
		sendAppMessage(RES_YES, array(FALSE,'修改成功'));
	}
	sendAppMessage(RES_NO, array(FALSE,'修改错误'));
}

?>
