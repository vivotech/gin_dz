<?php

/**
 * APP 微信接口
 */

if (!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

$allow_action_array = array(
		'bind',		//微信绑定
);

$app_action = in_array(APPACT, $allow_action_array) ? APPACT : 'bind';


if($app_action == 'bind'){
	
	$accesstoken = I('accesstoken','');
	$openid = I('openid','');
	$unionid = I('unionid','');
	if(empty($accesstoken) || empty($openid) || empty($unionid)){
		sendAppMessage(0, '参数为空');
	}
	$wx_appclient = new wechat_appclient($accesstoken);
	$user = $wx_appclient->getUserInfoById($openid);
	if($user == null){
		sendAppMessage(0, '参数无效');
	}
	if($user['unionid'] != $unionid){
		sendAppMessage(0, 'unionid匹配错误');
	}
	
	//删除openid，以免和和公众平台openid冲突
	unset($user['openid']);
	//根据unionid获取微信用户表
	$wechatMember = wechat_base::updateWechatMember($user);
	if($wechatMember['uid']){
		$user = getuserbyuid($wechatMember['uid'], 1);
		if(!empty($user)) {
			require_once libfile('function/member');
			$res = setWechatLoginStatus($user,2592000);
		}
	}else{
		//自动注册账号
		require_once libfile('function/wechat');
		$res = wechat_auto_bind_accout(array('input'=>md5($user['unionid'])),$userInfo);
	}
	
	echo json_encode($res,JSON_UNESCAPED_UNICODE);
	exit();
}
