<?php

/**
 * APP 商城接口
 */

if (!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

$allow_action_array = array(
			'catlist', //分类列表
			'goodlist', //商品列表
			'buy', //下单API
			'orderlist', //订单列表
		);

$app_action = in_array(APPACT, $allow_action_array) ? APPACT : 'goodlist';

include_once  libfile('function/product');

//需要登录的操作
$login_action_arr = array(
		'orderlist',
 	);
if(in_array($app_action,$login_action_arr)){
	if(!$_G['uid']){
		sendAppMessage(RES_NO, array(FALSE,'请先登陆！'));
	}
}


//分类列表
if ($app_action == 'catlist') {

	$pagesize = I('pagesize', 20, 'intval');
	$page = I('page', 1, 'intval');

	$model = C::t('common_product_series');
	$count = $model -> count();
	$pagecount = @ceil($count / $pagesize);
	if ($page > $pagecount) {
		sendAppMessage(RES_YES, 'app_page_bottom');
	}
	$list = $model -> fetch_all_page($page, $pagesize);
	if ($list) {
		sendAppMessage(RES_YES, array(FALSE, '列表查询成功！'), array('count' => $count, 'pagecount' => $pagecount, 'list' => $list));
	} else {
		sendAppMessage(RES_YES, array(FALSE, '暂时没有数据哦！'));
	}

}
//商品列表
elseif ($app_action == 'goodlist') {

	$pagesize = I('pagesize', 10, 'intval');
	$page = I('page', 1, 'intval');
	$model = C::t('common_product_onsell');

	$count = $model -> count();
	$pagecount = @ceil($count / $pagesize);
	if ($page > $pagecount) {
		sendAppMessage(RES_YES, 'app_page_bottom');
	}

	$list = $model -> fetch_all_page($page, $pagesize);
	foreach ($list as $lk => $lv) {
		$list[$lk]['media_list'] = json_decode(htmlspecialchars_decode($lv['media_list']), 1);
		$list[$lk]['main_pic'] = $list[$lk]['media_list'] ? $list[$lk]['media_list'][0]['media_url'] : '';
		$list[$lk]['costprice'] = number_format($lv['costprice'], 2);
		//处理折扣
		$list[$lk]['left_count'] = buyableNum($lv['spid']);
		$list[$lk]['left_time'] = get_left_time($lv['dateline'], $lv['expiration']);
		$list[$lk]['discount_price'] = getDiscountPrice($lv['discount'] == 1 ? 0 : $lv['spid'], $lv['price'], $pv['discount']);
		$list[$lk]['dateline'] = date('Y-m-d,H:i', $lv['dateline']);
		$list[$lk]['expiration'] = $lv['expiration'] ? date('Y-m-d,H:i', $lv['expiration']) : 0;

	}

	if ($list) {
		$member_res = array('uid' => $_G['uid'], 'groupid' => $_G['member']['groupid'], 'credits' => $_G['member']['credits']);
		sendAppMessage(RES_YES, 'app_list_success', array('member' => $member_res, 'count' => $count, 'pagecount' => $pagecount, 'list' => $list));
	} else {
		sendAppMessage(RES_YES, 'app_page_bottom');
	}

}
//下单
elseif ($app_action == 'buy') {
	
	
	
}
//订单列表
elseif ($app_action == 'orderlist') {
	//加载库文件
	require_once  libfile('function/url');
	$uid = $_G['uid'];
	$page = I("page",0,'intval');
	$pagesize = I('pagesize',20,'intval');
	$model = C::t('common_member_order');
	$limit = 10;
	$count = $model->count(" uid = $uid ");
	$pagecount = @ceil($count/$pagesize);
	$list = $model->fetch_all_page($page,$pagesize," WHERE o.uid = '$uid'");
	if($list){
		sendAppMessage(RES_YES, 'app_list_success', array('count'=>$count,'pagecount'=>$pagecount,'list'=>$list));
	}else{
		sendAppMessage(RES_YES, 'app_page_bottom');
	}
}
?>
