<?php

/**
 * APP 信息空间接口
 */

if (!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

require_once libfile('function/home');

$allow_action_array = array(
		'favorite', //收藏列表
		'message',	//消息列表
);

$app_action = in_array(APPACT, $allow_action_array) ? APPACT : 'favorite';

if(!$_G['uid']){
	sendAppMessage(RES_NO, array(FALSE,'请先登陆~'),'10001');
}

//收藏列表
if ($app_action == 'favorite') {
	
	$space = getuserbyuid($_G['uid']);

	$page = empty($_GET['page']) ? 1 : intval($_GET['page']);
	if ($page < 1) $page = 1;
	$id = empty($_GET['id']) ? 0 : intval($_GET['id']);
	
	//每页大小
	$perpage = I('pagesize',20,'intval');

	$_G['disabledwidthauto'] = 0;
	
	$start = ($page - 1) * $perpage;
	app_ckstart($start, $perpage);

	$idtypes = array('thread' => 'tid', 'forum' => 'fid', 'blog' => 'blogid', 'group' => 'gid', 'album' => 'albumid', 'space' => 'uid', 'article' => 'aid');
	$_GET['type'] = isset($idtypes[$_GET['type']]) ? $_GET['type'] : 'all';
	$actives[$_GET['type']] = ' class="a"';

	$gets = array('mod' => 'space', 'uid' => $space['uid'], 'do' => 'favorite', 'type' => $_GET['type'], 'from' => $_GET['from']);
	$theurl = 'home.php?' . url_implode($gets);

	$wherearr = $list = array();
	$favid = empty($_GET['favid']) ? 0 : intval($_GET['favid']);
	$idtype = isset($idtypes[$_GET['type']]) ? $idtypes[$_GET['type']] : '';

	$count = C::t('home_favorite') -> count_by_uid_idtype($_G['uid'], $idtype, $favid);
    //var_dump(C::t('home_favorite')->fetch_all_by_uid_idtype($_G['uid'], $idtype, $favid, $start,$perpage));
	if ($count) {
//		$icons = array('tid' => '<img src="static/image/feed/thread.gif" alt="thread" class="t" /> ', 'fid' => '<img src="static/image/feed/discuz.gif" alt="forum" class="t" /> ', 'blogid' => '<img src="static/image/feed/blog.gif" alt="blog" class="t" /> ', 'gid' => '<img src="static/image/feed/group.gif" alt="group" class="t" /> ', 'uid' => '<img src="static/image/feed/profile.gif" alt="space" class="t" /> ', 'albumid' => '<img src="static/image/feed/album.gif" alt="album" class="t" /> ', 'aid' => '<img src="static/image/feed/article.gif" alt="article" class="t" /> ', );
		$articles = array();
		foreach (C::t('home_favorite')->fetch_all_by_uid_idtype($_G['uid'], $idtype, $favid, $start,$perpage) as $value) {
//			$value['icon'] = isset($icons[$value['idtype']]) ? $icons[$value['idtype']] : '';
			$value['url'] = makeurl($value['id'], $value['idtype'], $value['spaceuid']);
			$value['description'] = !empty($value['description']) ? nl2br($value['description']) : '';

            include_once libfile('function/forum');

            if ( $value['idtype'] == "tid" ) {
                $thread = get_thread_by_tid($value['id']);
                if ($thread) {
                    $value['fid'] = $thread['fid'];
                    $value['tid'] = $thread['tid'];
                    $forum = C::t('forum_forum')->get_forum_by_fid($thread['fid']);
                    if ($forum) {
                        $value['forumname'] = $forum['name'];
                    }
                    $value['subject'] = $thread['subject'];
                    $value['replies'] = $thread['replies'];
                    $value['avatar'] = get_avatar($thread['authorid'], 'small');
                    $value['saygood'] = $thread['saygood'];
                    $value['author'] = $thread['author'];
                    $value['dateline'] = $thread['dateline'];

                }
            }

            $list[$value['favid']] = $value;

			if ($value['idtype'] == 'aid') {
				$articles[$value['favid']] = $value['id'];
			}


		}
		if (!empty($articles)) {
			include_once  libfile('function/portal');
			$_urls = array();
			foreach (C::t('portal_article_title')->fetch_all($articles) as $aid => $article) {
				$_urls[$aid] = fetch_article_url($article);
			}
			foreach ($articles as $favid => $aid) {
				$list[$favid]['url'] = $_urls[$aid];
			}
		}
	}

	$multi = multi($count, $perpage, $page, $theurl);

	dsetcookie('home_diymode', $diymode);

	if (!$_GET['type']) {
		$_GET['type'] = 'all';
	}
	if ($_GET['type'] == 'group') {
		$navtitle = lang('core', 'title_group_favorite', array('gorup' => $_G['setting']['navs'][3]['navname']));
	} else {
		$navtitle = lang('core', 'title_' . $_GET['type'] . '_favorite');
	}
	
	if($value){
		sendAppMessage(RES_YES, array(FALSE,'获取收藏列表成功') , array('count'=>$count,'pagecount'=>@ceil($count/$perpage),'list'=>$list));
	}else{
		sendAppMessage(RES_NO, array(FALSE,'列表为空！！'));
	}
	
}
//消息操作
elseif('message' == $app_action){
	
	//消息分类
	$do_arr = array(
			'sns',		//站内信
			'notice',	//提醒
		);
	$do = I('do','sns');
	$do = in_array($do,$do_arr) ? $do : 'sns';
	//引入之后的文件
	include_once libfile('app/message_'.$do,'include');
}

