<?php

/**
 * APP 社区操作
 */
if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}
 
include_once  libfile('app/member', 'class');

$allow_action_array = array(
						'gethash', //APP返回fromhash
						'list', //帖子列表
						'detail', //帖子详情
						'post', //发表帖子
						'reply', //回复
						'catlist', //分类列表
						'saygood', //点赞
						'favorite', //收藏
						'banner', //banner图
						'post2', //临时推送
				);

$app_action = in_array(APPACT, $allow_action_array) ? APPACT : 'list';

//加载
require_once  libfile('function/forumlist');
require_once  libfile('function/forum');


if($app_action == 'gethash'){
	if($_G['uid']){
		$formhash =  formhash();
		sendAppMessage(RES_YES, array(FALSE,'获取FORMHASH成功！') , $formhash);
	}
	sendAppMessage(RES_NO, array(FALSE,'获取FORMHASH失败!'));
}
//帖子列表
elseif($app_action == 'list') {
	
	//接受参数
	$fid = $_GET['fid'];
	//用户ID查找
	$uid = I('uid',0,'intval');
	//是否只显示置顶帖
	$top = I('top','false');
	//分页大小
	$pagesize = I('pagesize',$_G['tpp'],'intval');
	
	//排序
	$order  = 'dateline';
	
	//查询条件
	$where = ' 1 ';
//	$where .= (!isset($_GET['fid'])) ? '' : " AND fid = {$fid} ";
	if(isset($_GET['fid']) && !empty($_GET['fid'])){
		$fids = explode('|', $fid);
		if(count($fids)==1){
			$fid = intval($fid);
			$where .= " AND fid = {$fid} ";
		}elseif(count($fids)>1){
			foreach($fids as $fk => $fv){
				$fids[$fk] = intval($fv);
			}
			$where .= " AND fid IN ( ".implode(',', $fids)." ) ";
		}
	}
	$where .= ($uid == 0) ? '' : " AND authorid = {$uid} ";
	$where .= (strtolower($top) == 'true') ? " AND displayorder > 0 " : ' AND displayorder >= 0 ';
	
	//共多少页
	$count = C::t('forum_thread')->count($where);
	$pagecount = @ceil( $count / $pagesize );
	$page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;
//	$page = $page > $count ? 1 : $page;
	if($page>$pagecount){
		sendAppMessage(RES_YES, 'app_forum_list_noresult',array('count'=>$pagecount,'pagecount'=>$pagecount,'list'=>array()));
	}
	
	$all_threads = C::t('forum_thread') -> fetch_all_limit(TRUE, ($page-1)*$pagesize , $pagesize , $order , $where);
	
	foreach ($all_threads as $ak => $av) {
		// 头像
		$all_threads[$ak]['avatar'] = avatar($av['authorid']);
		//判断该用户是否已经收藏
		$all_threads[$ak]['is_favorite'] = check_favorite_by_id($av['tid']) ? TRUE : FALSE;
		//判断该用户是否点赞
		$all_threads[$ak]['is_saygood'] = check_saygood_by_id($av['tid']) ? TRUE : FALSE ;
		
		//删除字段
		$block_data = array('attachments','imagelist','attachlist');
		foreach($block_data as $v){
			unset($all_threads[$ak][$v]);
		}
		
	}
	
//	print_r($all_threads);
	if($all_threads){
		if(!$_G['cache']['forums']){
			loadcache('forums');
		}
		$forums = $_G['cache']['forums'];
		//处理返回数据
		$res = array();
		foreach($all_threads as $v){
			$v['forumname'] = $forums[$v['fid']]['name'];
			$res[] = $v;
		}
		//print_r($res);
		sendAppMessage(RES_YES, '', array('count'=>$count,'pagecount'=>$pagecount,'list'=>$res?$res:array()));
	}else{
		sendAppMessage(RES_YES, 'app_forum_list_noresult');
	}
	
}
//帖子详情
elseif ($app_action == 'detail') {
	$tid = I('tid',0,'intval');
	$detail = I('detail','no');
	
	$thread_post = C::t('forum_thread')->fetch($tid);
	if(!$thread_post){
		sendAppMessage(RES_NO, 'app_forum_nothread', $thread_post);
	}
	
	//解析media_list
	$_temp_media_list = json_decode(htmlspecialchars_decode($thread_post['media_list']),TRUE);
	$thread_post['media_list'] =  $_temp_media_list ? $_temp_media_list : array() ;
	$thread_post['dateline'] = custom_date($thread_post['dateline']);
	//判断该用户是否已经收藏
	$thread_post['is_favorite'] = check_favorite_by_id($thread_post['tid']) ? TRUE : FALSE;
	//判断该用户是否点赞
	$thread_post['is_saygood'] = check_saygood_by_id($thread_post['tid']) ? TRUE : FALSE ;
	
	if($detail == 'yes'){
		$_temp_thread = C::t('forum_post')->fetch_by_tid($thread_post['tid']);
		$thread_post = array_merge($_temp_thread,$thread_post);
	}
	
	sendAppMessage(RES_YES, array(FALSE,'获取成功!'), $thread_post);
	
}
//分类列表
elseif ($app_action == 'catlist') {
	
	if(!$_G['cache']['forums']){
		loadcache('forums');
	}
	$res = array();
	foreach ($_G['cache']['forums'] as $k => $v) {
		if($v['status'] != 0){
			$res[] = array(
				'id'	=>	$v['fid'],
				'name'	=>	$v['name'],
				'type'  =>  $v['type'],
				'enname' =>  $v['enname'],
			);
		}
	}
	sendAppMessage(RES_YES, '', $res);
}
//发帖操作
elseif($app_action == 'post'){
	if(!$_G['uid']){
		sendAppMessage(RES_NO,array(FALSE,'请先登陆！'));
	}
	//引入页面
	include_once libfile('app/post','include');
}
//点赞操作
elseif ($app_action == 'saygood') {
	if(!$_G['uid']){
		sendAppMessage(RES_NO,array(FALSE,'请先登陆！'));
	}
	//引入页面
	include libfile('app/saygood','include');
}
//收藏
elseif($app_action == 'favorite'){
	if(!$_G['uid']){
		sendAppMessage(RES_NO,array(FALSE,'请先登陆！'));
	}
	//引入页面
	include libfile('app/favorite','include');
}
//回复
elseif($app_action == 'reply'){
	//引入页面
	include libfile('app/reply','include');
}
//banner图
elseif($app_action == 'banner'){
	$limit = I('limit',0,'intval');
	if(!$_G['cache']['forum_banner_ad']){
		loadcache('forum_banner_ad');
	}
	$res = array();
	$i = 0;
	foreach($_G['cache']['forum_banner_ad'] as $v){
		if($v['type']=='index'){
			if($limit==0 || $i<=$limit){
				$res[] = $v;
				$i++;
			}
		}
	}
	sendAppMessage(RES_YES, '', $res);
}
//临时导入文章
elseif($app_action == 'post2'){
	if(!$_G['uid']){
		sendAppMessage(RES_NO,array(FALSE,'请先登陆！'));
	}
	//引入页面
	include_once libfile('app/post2','include');
}

?>