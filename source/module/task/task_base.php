<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

//抽象基类
abstract class task_base{
	
	public $task;
	public $rules;
    public $prizes;

	protected $task_user_model;
	protected $task_rule_model;
	protected $taskUser;
	
	function __construct($task){
		$this->task = $task;
        $this->rules = $task['rules'];
        $this->prizes = $task['prizes'];
        unset($task['rules']);
        unset($task['prizes']);

		$this->task_user_model = C::t('task_user');
		$this->task_rule_model = C::t('task_rule');

	}
	
	//获取用户列表
	public function get_task_user($uid = 0){
		global $_G;
		$uid = $uid==0 ? $_G['uid'] : $uid;
		$taskUser = $this->task_user_model->fetch_by_uid_tid($uid,$this->task['tid']);
		return $this->taskUser = $taskUser;
	}
	
	public function add_task_user($uid=0){
		global $_G;
		$uid = $uid==0 ? $_G['uid'] : $uid;
		if(!$this->get_task_user($uid)){
			$insertData = array(
				'tid' => $this->task['tid'],
				'uid' => $uid,
				'progress' => 0,
				'dateline'	=> TIMESTAMP,
				'isreward'	=> 0,
				'rewardtime'=> ''
			);
			$insertData['tuid'] = $this->task_user_model->insert($insertData,TRUE);
		}
		return $insertData ? $insertData : false;
	}

    public function get_rule($tid){
        return C::t('task_rule')->fetch($rid);
    }

	public function set_task_user($data=array(),$uid=0){
		global $_G;
		if(empty($data)) return false;
		$uid = $uid==0 ? $_G['uid'] : $uid;
		return $this->task_user_model->update_by_uid_tid($uid,$this->task['tid'],$data);
	}

    //获取用户列表
    public function on_getusers(){
        global $_G;
        $uid = I('uid',$_G['uid'],'intval');
        $pagesize = I('pagesize',10,'intval');
        $page = I('page',1,'intval');
        $model = C::t('task_rule_user');
        $task = $this->task;
        $count = $model->count_all_by_uid_tid($uid,$task['tid']);
        $list = $model->fetch_all_by_uid_tid($uid,$task['tid'],$page,$pagesize);

        if($list){

            $user_ids = array();
            foreach($list as $key=>$val){
                $user_ids[] = $val['uid'];
            }

            $rules = array();
            foreach($this->rules as $k=>$v){
                $rules[$v['trid']] = $v;
            }

            $users = C::t('common_member_profile')->fetch_all($user_ids);
            foreach($list as $key=>$val) {
                $list[$key]['avatar'] = urlencode(avatar($val['uid'],'small',true));
                $list[$key]['nickname'] = $users[$val['uid']]['nickname'];
                $rule = $rules[$val['trid']];
                $list[$key]['progress'] = get_rule_desc_by_actionid($rule['actionid'])." +".$rule['aval'];
            }

        }

        return $list ?  ajax_return(1,'',array('count'=>$count,'page'=>$page,'pagesize'=>$pagesize,'list'=>$list)) : ajax_return(0);
    }

	//游戏开始中
	abstract public function on_play();
	
	//当游戏结束
	abstract public function on_finish();
	
}