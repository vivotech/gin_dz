<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class task_product extends task_base{
	
	//任务展示
	function on_play(){
        global $_G;

        $uid = I('uid',$_G['uid'],'intval');
		$taskUser = $this->get_task_user($uid);
		if(!$taskUser && $uid!=0 && $uid==$_G['uid']){
			$this->add_task_user();
		}
        $task = $this->task;
		ajax_return(1,'ok',array('is_award'=> $taskUser['progeress']== $task['finish'] ? 1 : 0  ,'task'=>$task,'task_user'=>$taskUser));
	}
	
	//任务分享
	function on_share(){
		global $_G;
		
		$shareid = I('shareid',0,'intval');
		$taskUser = $this->get_task_user($shareid);
		if(!$taskUser){
			ajax_return(0,'参数错误！');
		}
		
		$uid = $_G['uid'];
		//如果是本人的话
		if($shareid == $uid){
			ajax_return(0,'自己不能帮自己加分哦！');
		}
		
		//任务
		$task = $this->task;
		//获取积分规则
		$rules = $task['rules'];
		//如果有分享获取积分
		if(!isset($rules[RULE_SHARE])){
			ajax_return(0,'没有分享获取积分的功能哦！');
		}
		
		$progress_result = task_add_progress($uid,$rules[RULE_SHARE],$taskUser);
		if(!$progress_result){
			ajax_return(0,'您已经帮助过了哦！');
		}
		
		ajax_return( 1 , $progress_result ? 1 : 0 , $rules[RULE_SHARE]['aval'] );
	}
	
	function on_sharepage(){

		$shareid = I('shareid',0,'intval');
		$taskUser = $this->get_task_user($shareid);
		if(!$taskUser){
			ajax_return(0,'参数错误！');
		}
		
		include template('task/product_share');
	}
	
	//任务领奖操作
	function on_finish(){
		global $_G;
		
		$taskUser = $this->get_task_user();
		if(!$taskUser){
			ajax_return(0,'参数错误！');
		}
		
		$task = $this->task;
		
		if($taskUser['progeress']!=$task['finish']){
			ajax_return(0,'还没有完成哦~',$task['finish']-$taskUser['progeress']);
		}
		
		//已经领取
		if($taskUser['isreward']==1){
			ajax_return(0,'已经领取过了,'.date('Y-m-d H:i',$taskUser['rewardtime']),$taskUser['rewardtime']);
		}
		
		//获取用户资料
		$address = I('address',0,'intval');
		
		//看看是什么奖励
		$prizes = $task['prizes'];
		foreach($prizes as $key=>$prize){
			$prizes[$key]['state'] = 1;
			//积分礼品
			if($prize['ptype']==1){
				updatemembercount($taskUser['uid'], array($_G['setting']['creditstrans'] => '+'.$prize['num']), false, 'TAK', $task['tid'] );
			}elseif($prize['ptype']==2){
				//优惠券
				
			}elseif($prize['ptype']==3){
				
				$spid = $prize['pid'];
				$product_onsell = C::t('common_product_onsell')->fetch($spid);
				if($product_onsell['amount'] >= $product_onsell['totalitems']){
					$prizes[$key]['state'] = 0;
					continue;
				}
				
				DB::begin();
				require_once libfile('function/order');
				$oid = insert_order($address,'', 0 , 0 , 0 , 0 , 0 ,0);
				if(!$oid){
					DB::rollback();
					showmessage('领取奖品失败！');
				}
				$po['amount'] = $product_onsell['amount'];
				if(insert_order_detail_row($oid,$spid,1,$po)){
					DB::commit();
				}
				
			}
			
		}
		
		ajax_return(1,'奖品处理完毕',$prizes);
		
	}
	
}
