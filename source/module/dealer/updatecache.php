<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

// 可用的缓存类型
$type_array = array(
	'order_day',	// 按日生成销售缓存
	'order_month',	// 按月生成销售缓存
	'browsing_month' // 按月生成扫码缓存
);

// 缓存类型
$cache_type = getgpc('type');
$cache_type ? null : $cache_type = 'order_day';
if(!in_array($cache_type, $type_array)){
	showmessage('没有此缓存类型');
}

require_once libfile('function/dealer');

$sid = ($_GET['sid']) ? intval($_GET['sid']) : 0;
if(!$sid){
	$this_shop = getShop($_G['dealer']['duid']);
	$sid = $this_shop['sid'];
}

require_once libfile('function/dealercache');

if($cache_type == 'order_day'){
	
	$date = $_GET['date'] ? trim($_GET['date']) : date('Y-m-d');
	$res = update_record_day_cache($sid, $date);
	result_handler($res);
	
}else if($cache_type == 'order_month'){
	$year = $_GET['year'] ? $_GET['year'] : 0;
	$month = $_GET['month'] ? $_GET['month'] : 0;
	$days_count = date('t', strtotime($year.'-'.$month.'-1 00:00:00'));
	
	rebuild_record_month_cache($sid, $year, $month, $days_count);

	result_handler(true);
}else if($cache_type == 'browsing_month'){
	$year = $_GET['year'] ? $_GET['year'] : 0;
	$month = $_GET['month'] ? $_GET['month'] : 0;
	$days_count = date('t', strtotime($year.'-'.$month.'-1 00:00:00'));
	
	rebuild_shop_browsing_month_cache($sid, $year, $month, $days_count);
	update_shop_product_browsing_month_cache($sid, $year, $month, $days_count);
	result_handler(true);
}


function result_handler($res){
	if($res){
		echo '<script>alert("更新缓存成功");history.back();</script>';
	}else{
		echo '<script>alert("更新缓存失败");history.back();</script>';
	}
	exit;
}
