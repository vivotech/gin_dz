<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

$action = $_GET['action'] ? $_GET['action'] : 'index';

$act_arr = array(
	'index',	// 店铺列表
	'detail',	// 店铺详情
);

if ( !in_array($action, $act_arr)){
	showmessage('非法操作');
}

require_once libfile('function/product');
require_once libfile('function/dealer');

// 查看店铺列表
if ($action == 'index'){
	
	$shops = C::t('dealer_member_shop')->fetch_all_filters(array(
		'duid'	=> $_G['dealer']['duid'],
		'deleted'	=> 0
	));
	include template('dealer/shop/index');
}
// 店铺详情
else if ( $action == 'detail' ) {
	
	$sid = getgpc('sid');
	if ( !$sid ) {
		redirect('/'.CURSCRIPT.'.php?mod=shop');
		exit;
	}
	
	$shop = C::t('dealer_member_shop')->fetch_by_sid($sid);
	if (!$shop) {
		redirect('/'.CURSCRIPT.'.php?mod=shop');
		exit;
	}
	
	// 今天日期
	$today = date('Y-m-d');
	$today_time = strtotime($today);
	
	// 店铺今天的统计记录
	$shopRebateDay	= getShopRebateDay($sid, $today, true);
	$shopBrowsingDay	= getShopBrowsingDay($sid, $today, true);
	$today_deals	= $shopRebateDay['count'];		// 订单数
	$today_rebate	= $shopRebateDay['rebate'];		// 分成额
	$today_browsing	= $shopBrowsingDay['amount'];	// 扫码量
	
	// 本周排行
	$week_day	= date('w')+1;
	$week_start	= $today_time - $week_day * ONE_DAY_SECONDS;
	$week_end	= $today_time + (7-$week_day) * ONE_DAY_SECONDS;
	$product_group_count	= C::t('common_member_browsing')->count_group_btime_etime_filters(
		$week_start, $week_end, array(
			'sid'	=> $sid
		), 'pdid', 5
	);
	
	$week_product_browsing_sort = array();
	$sort = 1;
	foreach ($product_group_count as $count_data) {
		
		$pd_data = array();
		$pd_data['sort']	= $sort;
		$pd_data['pdid']	= $count_data['pdid'];					// 商品ID
		$product			= getProduct($count_data['pdid']);
		$pd_data['name']	= $product ? $product['name'] : '';	// 商品名称
		$pd_data['count']	= $count_data['count'];				// 商品扫码总数
		
		$week_product_browsing_sort[] = $pd_data;
		$sort++;
	}
	
	include template('dealer/shop/detail');
	
}

