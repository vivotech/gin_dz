<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

// date range  日期范围
$today_date = date('Y-m-d', TIMESTAMP);
$date_start = $_GET['date_start'] ? $_GET['date_start'] : $today_date;
$date_end = $_GET['date_end'] ? $_GET['date_end'] : $today_date;
$pagestart = ($_G['page']) ? (int)$_G['page'] : 1;
$pagesize = ($_GET['pagesize']) ? intval($_GET['pagesize']) : 20;
$itemstart = ($pagestart - 1) * $pagesize;

$starttimestamp = strtotime($date_start.' 00:00:00');
$endtimestamp = strtotime($date_end.' 23:59:59');

if(!$starttimestamp || !$endtimestamp){
	showmessage('时间格式错误');
}

require_once libfile('function/dealer');
//$this_shop = getShop($_G['dealer']['duid']);
//$sid = $this_shop['sid'];

// 获取选择的店铺
$sid	= getgpc('sid');
if ($sid) {
	$selected_shop = C::t('dealer_member_shop')->fetch_by_sid($sid);
	$selected_shop_sid = $selected_shop['sid'];
} else {
	$selected_shop = null;
	$selected_shop_sid = null;
}
$this_shop = getShop($_G['dealer']['duid']);

// 生成点测试分成记录数据
$time_start = strtotime('2015-04-01');
$time_end = time();

if ( getgpc('renderdata') && $selected_shop_sid ) {
	
	for ($i=0; $i<10; $i++) {
	
		$random_time = ceil(rand($time_start, $time_end));
		
		$model_data = array(
			'sid'		=> $selected_shop_sid,	// 当前店铺
			'type'		=> 0,
			'oid'		=> 9999,
			'o_openid'	=> 999999999,
			'nickname'	=> 'order tester',
			'pdid'		=> 9999,
			'pdname'	=> '测试商品',
			'odid'		=> 9999,
			'amount'	=> 1,
			'total'		=> 100,
			'profit'	=> 70,
			'rebate'	=> 35,
			'dateline'	=> $random_time,	// 随机时间
		);
		$opt = C::t('dealer_shop_rebate')->insert($model_data);
	}
}

$orders_num = 0;
$orders = array();
if ($selected_shop_sid) {
	$orders_num = (int)C::t('dealer_shop_rebate')->count_by_sid_btime_etime($selected_shop_sid, $starttimestamp, $endtimestamp);
	if ( $itemstart > $orders_num ) {		// 开始索引超出总数
		$itemstart = $pagestart = 1;
	}
	$orders = C::t('dealer_shop_rebate')->fetch_all_by_sid_btime_etime($selected_shop_sid, $starttimestamp, $endtimestamp, $itemstart, $pagesize, 'dateline DESC');

}

$pagecount = ceil($orders_num / $pagesize);

//$multipage = multi($orders_num, $pagesize, $pagestart, "dealer.php?mod=order&date_start=".$date_start."&date_end=".$date_end.'&pagesize='.$pagesize);
include template('dealer/order');

