<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

// GET 参数 type 代表可用的统计类型
$type_array = array(
	'guest',	// 客流统计
	'product',	// 商品流量统计
	'deal',		// 交易统计
	'scan_code',// 扫码统计
);

// 统计类型
$statistics_type = getgpc('type');
$statistics_type ? null : $statistics_type = 'guest';
in_array($statistics_type, $type_array) ? null : $statistics_type = 'guest';

// 加载函数
require_once libfile('function/dealer');

// 日期范围
$range_type = getgpc('range_type');

// 日期类型
$date_type = getgpc('date_type');
in_array($date_type, array(
	'day',	// 按日
	'month'	// 按月
)) ? null : $date_type = 'day';

// 获取要查询的年月日
$day	= $_GET['day'] ? $_GET['day'] : date('Y-m-d');
$month	= $_GET['month'] ? intval($_GET['month']) : date('n');
$year	= $_GET['year'] ? intval($_GET['year']) : date('Y');
//$selectedMonth = 
$refresh = !!getgpc('refresh');

// 获取经销商所有店铺 (顶栏菜单已经获取过了)
isset($dealer_shops) ? null : $dealer_shops = C::t('dealer_member_shop')->fetch_all_filters(array(
	'duid'	=> $_G['dealer']['duid'],
	'deleted'	=> 0
));

// 获取选择的店铺
$sid	= getgpc('sid');
if ($sid) {
	$selected_shop = C::t('dealer_member_shop')->fetch_by_sid($sid);
	$selected_shop_sid = $selected_shop['sid'];
} else {
	$selected_shop = null;
	$selected_shop_sid = null;
}
$this_shop = getShop($_G['dealer']['duid']);

// 载入数据
require_once libfile('function/dealer');
require_once libfile('function/dealercache');
require_once libfile('function/chart');

/* 交易统计
 * 按日统计时，直接查询分成记录表
 * 按月统计时，通过 rebuild_record_month_cache 更新缓存，通过 getShopRebateDay 获取日分成记录（查询日记录缓存表）
 */
if ( $statistics_type === 'deal' ){
	
	// 按日统计
	if($date_type == 'day'){
		
		// 统计数据数
		$countsum = 0;
		$rebatesum = 0;
		$totalsum = 0;
		
		// 每小时的秒数
		$timedis = 3600;
		
		// 每小时的开始时间和结束时间
		$t_start = strtotime($day);			// *-*-* *:00:00
		$t_end = $t_start + $timedis - 1;	// *-*-* *:59:59
		
		// 每时段的数据记录
		$data_deal_count = array();
		$data_deal_total = array();
		$data_rebate = array();
		
		// 循环24小时，获取数据
		for ($i=1; $i<=24; $i++) {
			
			$data_deal_count[$i] = 0;
			$data_deal_total[$i] = 0;
			$data_rebate[$i] = 0;
			
			// 统计每一个时段的数据
			if ( is_array($selected_shop) ) {	// 指定的店铺
				
				$this_list = C::t('dealer_shop_rebate')->fetch_all_by_sid_btime_etime($selected_shop['sid'], $t_start, $t_end);
				
				$this_count_result = count_record($this_list);
				$data_deal_count[$i] = $this_count_result['count'];		// 订单量
				$data_deal_total[$i] = $this_count_result['total'];		// 订单额
				$data_rebate[$i] = $this_count_result['rebate'];		// 分成额
			}
			else {		// 所有店铺
				foreach ($dealer_shops as $shop) {
					
					// 查询该时段的分成记录列表
					$this_list = C::t('dealer_shop_rebate')->fetch_all_by_sid_btime_etime($shop['sid'], $t_start, $t_end);
				
					// 调用汇总函数
					$this_count_result = count_record($this_list);
					
					$data_deal_count[$i] += $this_count_result['count'];		// 订单量
					$data_deal_total[$i] += $this_count_result['total'];		// 订单额
					$data_rebate[$i] += $this_count_result['rebate'];			// 分成额
				}
			}
			
			// 合计所有时段的数据
			$countsum += $data_deal_count[$i];	// 订单量
			$totalsum += $data_deal_total[$i];	// 订单额
			$rebatesum += $data_rebate[$i];		// 分成额
			
			// 时段的开始时间和结束时间向后移一个单位
			$t_start += $timedis;
			$t_end += $timedis;
		}
		
	}else if($date_type == 'month'){
		
		// 统计数据数
		$countsum = 0;
		$rebatesum = 0;
		$totalsum = 0;
		
		// 每个月的开始时间和结束时间
		$t_start = 0;
		$t_end = 0;
		
		// 每时段的统计数据
		$data_deal_count = array();
		$data_deal_total = array();
		$data_rebate = array();
		
		$this_year = $year;
		$this_month = $month;
		$this_month_day_count = date('t', strtotime($this_year.'-'.$this_month.'-01'));
		
		// 循环指定月的所有天，获取数据
		for ($i=1; $i<=$this_month_day_count; $i++) {

			$data_deal_count[$i] = 0;
			$data_deal_total[$i] = 0;
			$data_rebate[$i] = 0;
			
			$t_day = ($i < 10) ? '0'.$i : $i;
			$datestr = $year.'-'.$month.'-'.$t_day;
			
			// 指定的店铺
			if ( is_array($selected_shop) ) {
				
				$data_count = $this_month_day_count;
				
				if ($year == date('Y') && $month == date('m') ) {
					$data_count = date('j') - 1;
				}
				
				// 获取店铺日分成
				$shopRebateDay = getShopRebateDay($selected_shop['sid'], $datestr, $refresh);

				$data_deal_count[$i] += $shopRebateDay['count'];
				$data_deal_total[$i] += $shopRebateDay['total'];
				$data_rebate[$i] += $shopRebateDay['rebate'];
			}
			// 所有店铺
			else {
				foreach ($dealer_shops as $shop) {
					
					// 获取店铺日分成
					$shopRebateDay = getShopRebateDay($shop['sid'], $datestr, $refresh);

					$data_deal_count[$i] += $shopRebateDay['count'];
					$data_deal_total[$i] += $shopRebateDay['total'];
					$data_rebate[$i] += $shopRebateDay['rebate'];
				}
			}
			
			// 合计
			$countsum += $data_deal_count[$i];
			$totalsum += $data_deal_total[$i];
			$rebatesum += $data_rebate[$i];
		}
	}
	
	// 载入模板
	include template('dealer/statistics_deal');
}

/* 扫码统计
 * 按日统计直接查询扫码记录表
 * 按月统计通过 getShopBrowsingDay() 函数获取每日的记录缓存
 */
else if ( $statistics_type === 'scan_code' ){
	
	// 接收参数
	$pagesize = 20;
	$pagestart = $_GET['page'] ? intval($_GET['page']) : 1;
	
	// 按日的扫码统计
	if($date_type == 'day'){
		
		// 统计数据
		$scancountsum = 0;
		
		// 每小时的秒数
		$timedis = 3600;
		
		// 每小时的开始时间和结束时间
		$t_start = strtotime($day);			// *-*-* *:00:00
		$t_end = $t_start + $timedis - 1;	// *-*-* *:59:59
		
		// 每时段的扫描统计数据
		$data_scan_code = array();
		
		// 循环24小时，获取数据
		for ($i=1; $i<=24; $i++) {
			
			$data_scan_code[$i] = 0;
			
			// 指定的店铺
			if ( is_array($selected_shop) ) {
				$data_scan_code[$i] = C::t('common_member_browsing')->count_by_sid_btime_etime($selected_shop['sid'], $t_start, $t_end);
			}
			// 所有店铺
			else {
				foreach ($dealer_shops as $shop) {
					$data_scan_code[$i] += C::t('common_member_browsing')->count_by_sid_btime_etime($shop['sid'], $t_start, $t_end);
				}
			}
			
			$scancountsum += $data_scan_code[$i];
			$t_start += $timedis;
			$t_end += $timedis;
		}
		
		// 商品流量分布图
		// 饼图分块
		$data_count = 8;
		$starttimestamp = strtotime($day);
		$endtimestamp = $starttimestamp + 3600 * 24 - 1;
		update_shop_product_browsing_day_cache($this_shop['sid'], $day, $data_count);
		$pie_datas = C::t('dealer_product_browsing_day')->fetch_all_by_sid_timeline($this_shop['sid'], $starttimestamp);
		format_piedata($pie_datas, $data_product_ratio);
		
		// 扫码名单明细
		$shop_browsing_list = C::t('common_member_browsing')->fetch_all_by_sid_btime_etime($this_shop['sid'], $starttimestamp, $endtimestamp, ($pagestart - 1) * $pagesize, $pagesize);
		$shop_browsing_count = C::t('common_member_browsing')->count_by_sid_btime_etime($this_shop['sid'], $starttimestamp, $endtimestamp);
		$multipage = multi($shop_browsing_count, $pagesize, $pagestart, 'dealer.php?mod=statistics&type=scan_code&day='.$day);
		
		
	}
	// 按月的扫码统计
	else if($date_type == 'month'){
		
		// 统计数据
		$scancountsum = 0;
		
		// 每个月的开始时间和结束时间
		$t_start = 0;
		$t_end = 0;
		
		// 每时段的扫描统计数据
		$data_scan_code = array();
		
		$this_year = $year;
		$this_month = $month;
		$this_month_day_count = date('t', strtotime($this_year.'-'.$this_month));
		
		// 循环1个月所有天，获取数据
		for ($i=1; $i<=$this_month_day_count; $i++) {

			$data_scan_code[$i] = 0;
			
			$t_day = ($i < 10) ? '0'.$i : $i;
			$datestr = $year.'-'.$month.'-'.$t_day;
			
			// 指定的店铺
			if ( is_array($selected_shop) ) {

				$shopBrowsingDay = getShopBrowsingDay($selected_shop['sid'], $datestr, $refresh);
				$data_scan_code[$i] += $shopBrowsingDay['amount'];
			}
			// 所有店铺
			else {
				foreach ($dealer_shops as $shop) {
					
					$shopBrowsingDay = getShopBrowsingDay($shop['sid'], $datestr, $refresh);
					$data_scan_code[$i] += $shopBrowsingDay['amount'];
				}
			}
			
			$scancountsum += $data_scan_code[$i];
		}
		
		// 商品流量分布图
		// 饼图分块
		$data_count = 10;
		$pie_datas = C::t('dealer_product_browsing_month')->fetch_all_by_sid_year_month($this_shop['sid'], $year, $month);
		if(count($pie_datas) < $data_count){
			update_shop_product_browsing_month_cache($this_shop['sid'], $year, $month, $data_count);
			$pie_datas = C::t('dealer_product_browsing_month')->fetch_all_by_sid_year_month($this_shop['sid'], $year, $month);
		}
		format_piedata($pie_datas, $data_product_ratio);
	}
	// 载入模板
	include template('dealer/statistics_scan_code');
}

function format_piedata(&$piedata, &$tararr){
	foreach($piedata as $d){
		$tararr[$d['pdname']] = $d['rate'] / 10;
	}
}
