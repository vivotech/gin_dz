<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

set_time_limit(0);

$request_type = getgpc('type');
$request_type ? null : $request_type = 'my';

// 加载函数
require_once libfile('function/dealer');

$page = $_GET['page'] ? intval($_GET['page']) : 1;
$pagesize = 20;

// 载入商品系列
if(!$_G['cache']['productseries']){
	loadcache('productseries');
}
$productseries = $_G['cache']['productseries'];

// 获取选择的店铺
$sid	= getgpc('sid');
if ($sid) {
	$selected_shop = C::t('dealer_member_shop')->fetch_by_sid($sid);
	$selected_shop_sid = $selected_shop['sid'];
} else {
	$selected_shop = null;
	$selected_shop_sid = null;
}
$this_shop = getShop($_G['dealer']['duid']);

// 申请产品代理
if(submitcheck('applysubmit')){

}

else if(submitcheck('qrcodesubmit') ){
// 打印二维码
	echo 'qrcodesubmit';
	exit;
}
else if(submitcheck('qrcodeallsubmit')){
	
	require_once libfile('function/qrcode');
	
	$sid = $_GET['sid'] ? intval($_GET['sid']) : 0;
	if(empty($sid)){
		$this_shop = C::t('dealer_member_shop')->fetch_by_duid($_G['dealer']['duid']);
		$sid = $this_shop['sid'];
	}
	
	if(!$_G['cache']['product']){
		loadcache('product');
	}
	$products = $_G['cache']['product'];
	$urls = array();
	$extras = array();
	
	$baseurl = $_G['config']['wechat_url'].'forum.php?mod=product&action=view&from=qrcode&sid='.$sid.'&pdid=';
	foreach($products as $pdid=>$product){
		$thisurl = $baseurl.$product['pdid'];
		$urls[$product['pdid']] = $thisurl;
		$extras[$product['pdid']] = 'Id:'.$product['pdid'].';Name:'.$product['name'];
	}
	$zip_path = create_qrcode($urls, $sid.'.zip', '', $extras);
	
	download_file($zip_path, 'application/zip',$_G['dealer']['company'].'-qrcode'.'.zip');
	exit;
}else{
// view

	$product_list = array();
	
	if($request_type == 'all'){
		$productnum = C::t('common_product')->count();
		$multipage = multi($productnum, $pagesize, $page, "dealer.php?mod=product&type=admin");
		$product_list = C::t('common_product')->fetch_all_page($page, $pagesize);
	}
	
	// 生成点扫码记录 网址添加 renderdata 参数即可随机生成扫码数据
	$time_start = strtotime('2015-05-22');
	$time_end = time();
	$product_count = count($product_list);
	if ( getgpc('renderdata') && is_array($selected_shop) ) {
		
		for ($i=0; $i<100; $i++) {
			
			$random_index = ceil(rand($time_start, $time_end));
			$product_index = $random_index%$product_count;
			
			$model_data = array(
				'uid'	=> $_G['uid'],								// 当前用户
				'sid'	=> $selected_shop['sid'],					// 选中的店铺
				'pdid'	=> $product_list[$product_index]['pdid'],	// 随机商品
				'dateline'	=> $random_index,						// 随机时间
				'ip'	=> $_SERVER['REMOTE_ADDR']
			);
			$opt = C::t('common_member_browsing')->insert($model_data);
			
		}
		
	}
	
}


include template('dealer/product');

