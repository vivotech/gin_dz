<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

require_once libfile('function/dealer');
require_once libfile('function/dealercache');
require_once libfile('function/chart');

// 今天的开始和结束时间
$starttimestamp = strtotime(date('Y-m-d'));
$endtimestamp = strtotime(date('Y-m-d').' 23:59:59');

// 今天日期
$date = date('Y-m-d');

// 实时获取经销商日统计数据
$dealerRebateDay = getDealerRebateDay($_G['dealer']['duid'], $date, ture);

// 获取今日的实时数据
$today_deals	= $dealerRebateDay['count'];	// 今日下单数
$today_rebate	= $dealerRebateDay['rebate'];	// 今日分成总额
$today_browsing	= $dealerRebateDay['browsing_amount'];	// 今日扫码次数

// 近七日的数据
$day_count = 7;

// 时间点数组(每一项为一天的 00:00:00 )
$x_axis = array();

// 每一天的秒数
$daily_seconds = 24 * 3600;

// 今天的 00:00:00 向前推7天的时间戳
$starttimestamp = strtotime(date('Y-m-d', TIMESTAMP)) - ($day_count - 1) * $daily_seconds;

// 最近7天的日期
for ($i=1; $i<=$day_count; $i++) {
	$x_axis[date('Y-m-d', $starttimestamp + $daily_seconds * ($i - 1) )] = 0;
}

// 循环7天日期获取统计数据
foreach($x_axis as $k=>$v){
	
	$this_date = $k.' 00:00:00';
	$this_timestamp = strtotime($this_date);
	$this_year = date('Y', $this_timestamp);
	$this_month = date('n', $this_timestamp);
	$this_day = date('j', $this_timestamp);
	
	$dealerRebateDay = getDealerRebateDay($_G['dealer']['duid'], $k, true);	// 获取实时统计数据
	$data_rebate_7_day[$k] = $dealerRebateDay['rebate'];
	$data_scan_qr_7_day[$k] = $dealerRebateDay['browsing_amount'];
	
}

// 获取各店铺噶情况
$shops_data = array();
foreach ($dealer_shops as $shop) {
	
	$datastr = date('Y-m-d', TIMESTAMP);
	$shopRebateDay		= getShopRebateDay($shop['sid'], $datastr);
	$shopBrowsingDay	= getShopBrowsingDay($shop['sid'], $datastr);
	
	$shop_data = array();
	$shop_data['sid'] = $shop['sid'];
	$shop_data['name'] = $shop['name'];
	$shop_data['today_order_num'] = $shopRebateDay['count'];
	$shop_data['today_rebate'] = $shopRebateDay['rebate'];
	$shop_data['today_browsing'] = $shopBrowsingDay['amount'];
	
	$shops_data[] = $shop_data;
}

include template('dealer/index');

