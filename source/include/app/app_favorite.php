<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: spacecp_favorite.php 34278 2013-12-03 09:46:45Z nemohou $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

require_once libfile('function/home');

$_GET['type'] = in_array($_GET['type'], array("thread", "forum", "group", "blog", "album", "article", "all")) ? $_GET['type'] : 'all';
if($_GET['op'] == 'delete') {
		
		$tid = I('id',0,'intval');
		$favid = I('favid',0,'intval');
		if($favid){
			$thevalue = C::t('home_favorite')->fetch($favid);
		}else{
			if($tid){
				$thevalue = C::t('home_favorite')->fetch_by_id_idtype($tid,'tid',$_G['uid']);
				$favid = $thevalue['favid'];
			}
		}
		
		if(empty($thevalue) || $thevalue['uid'] != $_G['uid']) {
			sendAppMessage(RES_NO,'favorite_does_not_exist');
		}
		switch($thevalue['idtype']) {
			case 'fid':
				C::t('forum_forum')->update_forum_counter($thevalue['id'], 0, 0, 0, 0, -1);
				break;
			case 'tid':
				C::t('forum_thread')->discrease($thevalue['id'], array('favtimes'=>1));
				break;
			default:
				break;
		}
		
		C::t('home_favorite')->delete($favid);
		if($_G['setting']['cloud_status']) {
			$favoriteService = Cloud::loadClass('Service_Client_Favorite');
			$favoriteService->remove($_G['uid'], $favid);
		}
		
		sendAppMessage(RES_YES,'do_success');

} else {

//	cknewuser();

	$type = empty($_GET['type']) || isset($_GET['type']) ? 'thread' : $_GET['type'];
	$id = empty($_GET['id']) ? 0 : intval($_GET['id']);
	$spaceuid = empty($_GET['spaceuid']) ? 0 : intval($_GET['spaceuid']);
	$idtype = $title = $icon = '';
	switch($type) {
		case 'thread':
			$idtype = 'tid';
			$thread = C::t('forum_thread')->fetch($id);
			$title = $thread['subject'];
			break;
		case 'forum':
			$idtype = 'fid';
			$foruminfo = C::t('forum_forum')->fetch($id);
			loadcache('forums');
			$forum = $_G['cache']['forums'][$id];
			break;
		case 'blog':
			$idtype = 'blogid';
			$bloginfo = C::t('home_blog')->fetch($id);
			$title = ($bloginfo['uid'] == $spaceuid) ? $bloginfo['subject'] : '';
			break;
		case 'group':
			$idtype = 'gid';
			$foruminfo = C::t('forum_forum')->fetch($id);
			$title = $foruminfo['status'] == 3 ? $foruminfo['name'] : '';
			break;
		case 'album':
			$idtype = 'albumid';
			$result = C::t('home_album')->fetch($id, $spaceuid);
			$title = $result['albumname'];
			break;
		case 'space':
			$idtype = 'uid';
			$_member = getuserbyuid($id);
			$title = $_member['username'];
			$unset($_member);
			break;
		case 'article':
			$idtype = 'aid';
			$article = C::t('portal_article_title')->fetch($id);
			$title = $article['title'];
			break;
	}

	if(empty($idtype) || empty($title)) {
		sendAppMessage(RES_NO,'favorite_cannot_favorite');
	}

	$fav = C::t('home_favorite')->fetch_by_id_idtype($id, $idtype, $_G['uid']);
	if($fav) {
		sendAppMessage(RES_NO,'favorite_repeat');
	}
	$description = $extrajs = '';
	$description_show = nl2br($description);

	$fav_count = C::t('home_favorite')->count_by_id_idtype($id, $idtype);
	
	$arr = array(
		'uid' => intval($_G['uid']),
		'idtype' => $idtype,
		'id' => $id,
		'spaceuid' => $spaceuid,
		'title' => getstr($title, 255),
		'description' => getstr($_POST['description'], '', 0, 0, 1),
		'dateline' => TIMESTAMP
	);
	
	$favid = C::t('home_favorite')->insert($arr, true);
	if($_G['setting']['cloud_status']) {
		$favoriteService = Cloud::loadClass('Service_Client_Favorite');
		$favoriteService->add($arr['uid'], $favid, $arr['id'], $arr['idtype'], $arr['title'], $arr['description'], TIMESTAMP);
	}
	
	switch($type) {
		case 'thread':
			C::t('forum_thread')->increase($id, array('favtimes'=>1));
			require_once libfile('function/forum');
			update_threadpartake($id);
			break;
		case 'forum':
			C::t('forum_forum')->update_forum_counter($id, 0, 0, 0, 0, 1);
			$extrajs = '<script type="text/javascript">$("number_favorite_num").innerHTML = parseInt($("number_favorite_num").innerHTML)+1;$("number_favorite").style.display="";</script>';
			dsetcookie('nofavfid', '', -1);
			break;
		case 'blog':
			C::t('home_blog')->increase($id, $spaceuid, array('favtimes' => 1));
			break;
		case 'group':
			C::t('forum_forum')->update_forum_counter($id, 0, 0, 0, 0, 1);
			break;
		case 'album':
			C::t('home_album')->update_num_by_albumid($id, 1, 'favtimes', $spaceuid);
			break;
		case 'space':
			C::t('common_member_status')->increase($id, array('favtimes' => 1));
			break;
		case 'article':
			C::t('portal_article_count')->increase($id, array('favtimes' => 1));
			break;
	}

	sendAppMessage(RES_YES,'favorite_do_success', array('id' => $id, 'favid' => $favid));
}


?>