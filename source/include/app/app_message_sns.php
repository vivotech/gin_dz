<?php

//消息 —— 站内短消息

if (!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

loaducenter();

$list = array();

$plid = empty($_GET['plid']) ? 0 : intval($_GET['plid']);
$daterange = empty($_GET['daterange']) ? 0 : intval($_GET['daterange']);
$touid = empty($_GET['touid']) ? 0 : intval($_GET['touid']);
$opactives['pm'] = 'class="a"';

if (empty($_G['member']['category_num']['manage']) && !in_array($_G['adminid'], array(1, 2, 3))) {
	unset($_G['notice_structure']['manage']);
}

//私人消息详情列表
if ($_GET['subop'] == 'view') {
	
	$type = $_GET['type'];
	$page = empty($_GET['page']) ? 0 : intval($_GET['page']);
	
	$chatpmmember = intval($_GET['chatpmmember']);
	$chatpmmemberlist = array();
	
	//查看单独某一个人的消息详情列表
	if ($chatpmmember) {
		$chatpmmember = uc_pm_chatpmmemberlist($_G['uid'], $plid);
		if (!empty($chatpmmember)) {
			$authorid = $founderuid = $chatpmmember['author'];
			$chatpmmemberlist = C::t('common_member') -> fetch_all($chatpmmember['member']);
			foreach (C::t('common_member_field_home')->fetch_all($chatpmmember['member']) as $uid => $member) {
				$chatpmmemberlist[$uid] = array_merge($member, $chatpmmemberlist[$uid]);
			}
		}
		require_once  libfile('function/friend');
		$friendgrouplist = friend_group_list();
		$actives = array('chatpmmember' => ' class="a"');
		
	} else {
	
		if ($touid) {
			$ols = array();
			
			$perpage = I('pagesize',5,'intval');

			if (!$daterange) {
				
				$member = getuserbyuid($touid);
				$tousername = $member['username'];
				unset($member);
				$count = uc_pm_view_num($_G['uid'], $touid, 0);
				if (!$page) {
					$page = ceil($count / $perpage);
				}
				$list = uc_pm_view($_G['uid'], 0, $touid, 5, ceil($count / $perpage) - $page + 1, $perpage, 0, 0);
				foreach($list as $k=>$v){
					$v['message'] = preg_replace("/<a[^>]*>(.*)<\/a>/is","\\1",$v['message']);
					$list[$k]['message'] = nl2br($v['message']);
				}
				
			} else {
				sendAppMessage(RES_NO, 'parameters_error');
			}

		} else {
			
			$perpage = I('pagesize',10,'intval');
			
			$count = uc_pm_view_num($_G['uid'], $plid, 1);
			if (!$daterange) {
				if (!$page) {
					$page = ceil($count / $perpage);
				}
				$list = uc_pm_view($_G['uid'], 0, $plid, 5, ceil($count / $perpage) - $page + 1, $perpage, $type, 1);
			} else {
				$list = uc_pm_view($_G['uid'], 0, $plid, 5, ceil($count / $perpage) - $page + 1, $perpage, $type, 1);
				$chatpmmember = uc_pm_chatpmmemberlist($_G['uid'], $plid);
				if (!empty($chatpmmember)) {
					$authorid = $founderuid = $chatpmmember['author'];
					$chatpmmemberlist = C::t('common_member') -> fetch_all($chatpmmember['member']);
					foreach (C::t('common_member_field_home')->fetch_all($chatpmmember['member']) as $uid => $member) {
						$chatpmmemberlist[$uid] = array_merge($member, $chatpmmemberlist[$uid]);
					}
					foreach (C::app()->session->fetch_all_by_uid($chatpmmember['member']) as $value) {
						if (!$value['invisible']) {
							$ols[$value['uid']] = $value['lastactivity'];
						}
					}
				}
				$membernum = count($chatpmmemberlist);
				$subject = $list[0]['subject'];
				$refreshtime = $_G['setting']['chatpmrefreshtime'];

			}
		}
		$founderuid = empty($list) ? 0 : $list[0]['founderuid'];
		$pmid = empty($list) ? 0 : $list[0]['pmid'];
	}
	$actives['privatepm'] = ' class="a"';

}
//群发信息详细列表
elseif ($_GET['subop'] == 'viewg') {
	
	$grouppm = C::t('common_grouppm') -> fetch($_GET['pmid']);
	if (!$grouppm) {
		$grouppm = array_merge((array)C::t('common_member_grouppm') -> fetch($_G['uid'], $_GET['pmid']), $grouppm);
	}
	if ($grouppm) {
		$grouppm['numbers'] = $grouppm['numbers'] - 1;
	}
	if (!$grouppm['status']) {
		C::t('common_member_grouppm') -> update($_G['uid'], $_GET['pmid'], array('status' => 1, 'dateline' => TIMESTAMP));
	}
	
//	$actives['announcepm'] = ' class="a"';
	
	if($grouppm){
		sendAppMessage(RES_YES, array('获取公共消息成功！'), $grouppm);
	}else{
		sendAppMessage(RES_NO, array(FALSE,'暂没有信息哦！'));
	}
}
//忽略某消息
elseif ($_GET['subop'] == 'ignore') {

	$ignorelist = uc_pm_blackls_get($_G['uid']);
	$actives = array('ignore' => ' class="a"');

}
//消息提醒设定
elseif ($_GET['subop'] == 'setting') {

	$actives = array('setting' => ' class="a"');
	$acceptfriendpmstatus = $_G['member']['onlyacceptfriendpm'] ? $_G['member']['onlyacceptfriendpm'] : ($_G['setting']['onlyacceptfriendpm'] ? 1 : 2);
	$ignorelist = uc_pm_blackls_get($_G['uid']);
	
}
//消息列表
else {
	//分类
	$filter = in_array($_GET['filter'], array(
			'private',	//私人消息
			'announce'	//公共消息
		)) ? $_GET['filter'] : 'private';
	
	//是否显示新消息
	$isnew = I('isnew',0,'intval');
	
	$perpage = I('pagesize',10,'intval');

	$page = empty($_GET['page']) ? 0 : intval($_GET['page']);
	if ($page < 1) $page = 1;
	
	$grouppms = $gpmids = $gpmstatus = array();
	$newpm = $newpmcount = 0;
	
	if ($filter == 'announce') {
		$announcepm = 0; //标记未读数量
		$count = C::t('common_member_grouppm')->count_all_by_uid($_G['uid'], $filter == 'announce' ? 1 : 0 );
		$start = ($page-1)*$perpage;
		//群发短消息用户记录表
		$_temp_announcepm = C::t('common_member_grouppm')->fetch_all_by_uid($_G['uid'], $filter == 'announce' ? 1 : 0 , ' dateline ASC ' , $start , $perpage );
		foreach ($_temp_announcepm as $gpmid => $gpuser) {
			$gpmstatus[$gpmid] = $gpuser['status'];
			if ($gpuser['status'] == 0) {
				$announcepm++;
			}
		}
		$gpmids = array_keys($gpmstatus);
		if ($gpmids) {
			foreach (C::t('common_grouppm')->fetch_all_by_id_authorid($gpmids) as $grouppm) {
				$grouppm['message'] = cutstr(strip_tags($grouppm['message']), 100, '');
				$grouppm['dateline'] = date('Y-m-d H:i',$grouppm['dateline']);
				$grouppm['isnew'] = $gpmstatus[$grouppm['id']] ? 0 : 1;
				$grouppms[] = $grouppm;
			}
		}
	}

	if ($filter == 'private') {
		//查询用户消息
		$result = uc_pm_list($_G['uid'], $page, $perpage, 'inbox', $filter.'pm', 200);
		$count = $result['count'];
		$list = $result['data'];
		
		$newpmarr = uc_pm_checknew($_G['uid'], 1);
		$newpm = $newpmarr['newpm'];
	}
	
	//检查有多少条新信息
	$newpmcount = $newpm + $announcepm;
	if($_G['member']['newpm']) {
		if($newpm && $_G['setting']['cloud_status']) {
			$msgService = Cloud::loadClass('Cloud_Service_Client_Message');
			$msgService->setMsgFlag($_G['uid'], $_G['timestamp']);
		}
		C::t('common_member')->update($_G['uid'], array('newpm' => 0));
		uc_pm_ignore($_G['uid']);
	}
	
	//公共消息返回
	if($filter == 'announce'){
		if($grouppms){
			sendAppMessage(RES_YES, array(FALSE,'成功获取“公共消息列表”！'), array('count'=>$count ,'pagecount'=>@ceil($count/$perpage), 'newscount'=> $newpmcount , 'list'=>$grouppms));
		}else{
			sendAppMessage(RES_NO, array(FALSE,'列表为空哦！'));
		}
	}
	
}

//list普通列表
if (!empty($list)) {
	$today = $_G['timestamp'] - ($_G['timestamp'] + $_G['setting']['timeoffset'] * 3600) % 86400;
	foreach ($list as $key => $value) {
		$value['lastsummary'] = str_replace('&amp;', '&', $value['lastsummary']);
		$value['lastsummary'] = preg_replace("/&[a-z]+\;/i", '', $value['lastsummary']);
		$value['daterange'] = 5;
		if ($value['lastdateline'] >= $today) {
			$value['daterange'] = 1;
		} elseif ($value['lastdateline'] >= $today - 86400) {
			$value['daterange'] = 2;
		} elseif ($value['lastdateline'] >= $today - 172800) {
			$value['daterange'] = 3;
		} elseif ($value['lastdateline'] >= $today - 604800) {
			$value['daterange'] = 4;
		}
		$value['lastmessage'] = unserialize($value['lastmessage']);
		$list[$key] = $value;
	}
	sendAppMessage(RES_YES, array(FALSE,'获取列表成功！'), array('count'=>$count ,'pagecount'=>@ceil($count/$perpage), 'newscount'=> $newpmcount , 'list'=>$list) );
}else{
	sendAppMessage(RES_NO, array(FALSE,'暂没有信息哦！'));
}

//include_once  template("diy:home/space_pm");

?>