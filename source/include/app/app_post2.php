<?php
	/**
	 * 批量转帖操作
	 * 使用discuz中的post_newthread.php
	 * 
	 * http://192.168.1.58/app.php?mod=forum&act=post2&fromhash=xxxx&author=哈哈&authorid=2223333&dateline=1406654653&subject=12312321312312&message=23423432432423
	 */
	
	if (!defined('IN_DISCUZ')) {
		exit('Access Denied');
	}
	
//	error_reporting(E_ALL);
	
	require_once libfile('function/editor');
	require_once libfile('function/post');
	require_once  libfile('function/emoji');
	
	//$s = "1231<img src='http://pic1.ooopic.com/uploadfilepic/sheji/2009-08-05/OOOPIC_SHIJUNHONG_20090805326b1a80e6c6aa6a.jpg' >";;
	//echo html2bbcode($s);
	//exit();
	
	//捕获数据
	
	$pid = I('pid',0,'intval');
	$sortid = I('sortid',0,'intval');
	$typeid = I('typeid',0,'intval');
	$special = I('special',0,'intval');
	$fid = 50;
	$author = I('author','');
	$authorid = I('authorid',substr(md5(time()),6),'intval');
	//查找UID
	$profile = C::t('common_member_profile')->fetch_where(array('nickname'=>$author));
	if($profile){
		//已经有该昵称
		$_G['uid']=$uid=$profile['uid'];
	}else{
		
		//自动注册
		loaducenter();//加载ucenter类
		$password = '123456';
		$email = $authorid.'@qq.com';
		$_G['uid'] = $uid = uc_user_register(addslashes($authorid), $password, $email, $questionid, $answer, $_G['clientip']);
		$profile['nickname'] = $author;
		$init_arr = array('credits' => explode(',', $_G['setting']['initcredits']), 'profile' => $profile, 'emailstatus' => $emailstatus);
		C::t('common_member') -> insert($uid, $authorid, $password, $email, $_G['clientip'], $groupinfo['groupid'], $init_arr);
	}

	//如果没有fid，则随机选择一个
//	if (!isset($_G['cache']['forums'])) {
//		loadcache('forums');
//		if (!isset($_G['cache']['forums'])) {
//			updatecache('forums');
//		}
//	}

//	//找出APP发帖隐藏板块
//	$fids = array();
//	foreach($_G['cache']['forums'] as $k=>$v){
//		//如果是隐藏板块，找出来
//		if($v['status'] == 0){
//			$fids[] = $v['fid'];
//		}
//	}
//	if($fid){
//		if(!in_array($fid,$fids)){
//			$fid = $fids[0];
//		}
//	}else{
//		$fid = $fids[0];
//	}
	
	$subject = isset($_REQUEST['subject']) ? dhtmlspecialchars(censor(trim($_REQUEST['subject']))) : '';
	$subject = !empty($subject) ? str_replace("\t", ' ', $subject) : $subject;
	$message = isset($_REQUEST['message']) ? $_REQUEST['message'] : '';
	$polloptions = isset($polloptions) ? censor(trim($polloptions)) : '';
	$readperm = isset($_REQUEST['readperm']) ? intval($_REQUEST['readperm']) : 0;
	$price = isset($_REQUEST['price']) ? intval($_REQUEST['price']) : 0;
	
	
	// 将message保存为仅保留空格的150长概要
	$messagewithhtml = $message;
	$media_list_imgs = get_img_from_html($messagewithhtml);
//	$messagewithhtml = strip_tags($messagewithhtml, '<br>');
//	$messagewithhtml = preg_replace('/<br[^\f\n\r\t\v\>]*>/i', "\n", $messagewithhtml);
//	$messagewithhtml = mb_substr($messagewithhtml, 0, 200);
//	$messagewithhtml = htmlspecialchars(nl2br($messagewithhtml));
	
	//转换unicode表情
	$UAtype = get_device_type();
	if($UAtype=='ios'){
		$message = emoji_softbank_to_unified($message); //IOS
	}elseif($UAtype == 'android'){
		$message = emoji_google_to_unified($message); //android
	}
	$message = emoji_unified_to_html($message);

	//将$meesage转变成BBCODE
	$message = app_html2bbcode(nl2br($message));
	$messagewithhtml = messagecutstr($message,200);
	
	// 将附件保存为media_list
	$media_list_arr = array();
	foreach($media_list_imgs as $tempimg){
		$tempimgurl = get_attr_from_tag($tempimg, 'src');
		if($tempimgurl[0]){
			$media_list_arr[] = array(
				'media_type'=>0,
				'media_url'=>$tempimgurl[0],
				'media_desc'=>'',
				'media_duration'=>''
			);
		}
	}
	
	$media_list = htmlspecialchars(json_encode($media_list_arr, JSON_UNESCAPED_UNICODE));
	
	//注册操作类
	$modthread = C::m('forum_thread_app',$fid,$uid);
	$bfmethods = $afmethods = array();
	
	$params = array(
		'subject' => $subject, 
		'message' => $message, 
		'typeid' => $typeid, 
		'sortid' => $sortid, 
		'special' => $special, 
		'summary' => $messagewithhtml, 
		'media_list' => $media_list
	);
	
	$_REQUEST['save'] = $_G['uid'] ? $_REQUEST['save'] : 0;
	
	if ($_G['group']['allowsetpublishdate'] && $_REQUEST['cronpublish'] && $_REQUEST['cronpublishdate']) {
		$publishdate = strtotime($_REQUEST['cronpublishdate']);
		if ($publishdate > $_G['timestamp']) {
			$_REQUEST['save'] = 1;
		} else {
			$publishdate = $_G['timestamp'];
		}
	} else {
		$publishdate = $_G['timestamp'];
	}
	
	//控制发布时间
	$params['publishdate'] = I('dateline',$publishdate);
	
	$params['save'] = $_REQUEST['save'];
	
	$params['sticktopic'] = $_REQUEST['sticktopic'];
	
	$params['digest'] = $_REQUEST['addtodigest'];
	$params['readperm'] = $readperm;
	$params['isanonymous'] = $_REQUEST['isanonymous'];
	$params['price'] = $_REQUEST['price'];
	
	if (in_array($special, array(1, 2, 3, 4, 5))) {
		$specials = array(1 => 'extend_thread_poll', 2 => 'extend_thread_trade', 3 => 'extend_thread_reward', 4 => 'extend_thread_activity', 5 => 'extend_thread_debate');
		$bfmethods[] = array('class' => $specials[$special], 'method' => 'before_newthread');
		$afmethods[] = array('class' => $specials[$special], 'method' => 'after_newthread');
	
		if (!empty($_REQUEST['addfeed'])) {
			$modthread -> attach_before_method('feed', array('class' => $specials[$special], 'method' => 'before_feed'));
			if ($special == 2) {
				$modthread -> attach_before_method('feed', array('class' => $specials[$special], 'method' => 'before_replyfeed'));
			}
		}
	}
	
	if ($special == 1) {
	
	} elseif ($special == 3) {
	
	} elseif ($special == 4) {
	} elseif ($special == 5) {
	
	} elseif ($specialextra) {
	
		@include_once DISCUZ_ROOT . './source/plugin/' . $_G['setting']['threadplugins'][$specialextra]['module'] . '.class.php';
		$classname = 'threadplugin_' . $specialextra;
		if (class_exists($classname) && method_exists($threadpluginclass = new $classname, 'newthread_submit')) {
			$threadpluginclass -> newthread_submit($_G['fid']);
		}
		$special = 127;
		$params['special'] = 127;
		$params['message'] .= chr(0) . chr(0) . chr(0) . $specialextra;
	
	}
	
	$params['typeexpiration'] = $_REQUEST['typeexpiration'];
	
	$params['ordertype'] = $_REQUEST['ordertype'];
	
	$params['hiddenreplies'] = $_REQUEST['hiddenreplies'];
	//允许通知作者
	$params['allownoticeauthor'] = $_REQUEST['allownoticeauthor'] ? $_REQUEST['allownoticeauthor'] : 1;
	$params['tags'] = $_REQUEST['tags'];
	$params['bbcodeoff'] = $_REQUEST['bbcodeoff'];
	$params['smileyoff'] = $_REQUEST['smileyoff'];
	$params['parseurloff'] = $_REQUEST['parseurloff'];
	$params['usesig'] = $_REQUEST['usesig'];
	$params['htmlon'] = $_REQUEST['htmlon'];
	if ($_G['group']['allowimgcontent']) {
		$params['imgcontent'] = $_REQUEST['imgcontent'];
		$params['imgcontentwidth'] = $_G['setting']['imgcontentwidth'] ? intval($_G['setting']['imgcontentwidth']) : 100;
	}
	
	$params['geoloc'] = diconv($_REQUEST['geoloc'], 'UTF-8');
	
	//if ($_REQUEST['rushreply']) {
	//	$bfmethods[] = array('class' => 'extend_thread_rushreply', 'method' => 'before_newthread');
	//	$afmethods[] = array('class' => 'extend_thread_rushreply', 'method' => 'after_newthread');
	//}
	
//	$afmethods[] = array('class' => 'extend_thread_image', 'method' => 'after_newthread');
	
	$modthread -> attach_before_methods('newthread', $bfmethods);
	$modthread -> attach_after_methods('newthread', $afmethods);
	
	$return = $modthread -> newthread($params);
	$tid = $modthread -> tid;
	$pid = $modthread -> pid;
	//记录积分情况
	$credits = $modthread -> credits;
	
	dsetcookie('clearUserdata', 'forum');
	
	if ($specialextra) {
		$classname = 'threadplugin_' . $specialextra;
		if (class_exists($classname) && method_exists($threadpluginclass = new $classname, 'newthread_submit_end')) {
			$threadpluginclass -> newthread_submit_end($_G['fid'], $modthread -> tid);
		}
	}
	
	if (!$modthread -> param('modnewthreads') && !empty($_REQUEST['addfeed'])) {
		$modthread -> feed();
	}
	
	if (!empty($_G['setting']['rewriterule']['forum_viewthread']) && in_array('forum_viewthread', $_G['setting']['rewritestatus'])) {
		$returnurl = rewriteoutput('forum_viewthread', 1, '', $modthread -> tid, 1, '', $extra);
	} else {
		$returnurl = "forum.php?mod=viewthread&tid={$modthread->tid}&extra=$extra";
	}
	$values = array('fid' => $modthread -> forum('fid'), 'tid' => $modthread -> tid, 'pid' => $modthread -> pid, 'addcredits' =>app_get_return_credict($credits ,true) , 'coverimg' => '', 'sechash' => !empty($_REQUEST['sechash']) ? $_REQUEST['sechash'] : '');
	
	sendAppMessage(RES_YES, 'app_'.$return, $authorid);
//	showmessage($return, $returnurl, array_merge($values, (array)$modthread -> param('values')), $modthread -> param('param'));
	
?>