<?php

//消息  —— 提醒

if (!defined('IN_DISCUZ')) {
	exit('Access Denied');
}


$perpage = I('pagesize',20,'intval');
$perpage = $perpage < 1 ? 1 : $perpage;
$page = empty($_GET['page'])?0:intval($_GET['page']);

if($page<1) $page = 1;

$start = ($page-1)*$perpage;
app_ckstart($start, $perpage);

$list = array();
$mynotice = $count = 0;
$multi = '';

if(empty($_G['member']['category_num']['manage']) && !in_array($_G['adminid'], array(1,2,3))) {
	unset($_G['notice_structure']['manage']);
}

$view = (!empty($_GET['view']) && (isset($_G['notice_structure'][$_GET['view']]) || in_array($_GET['view'], array('userapp'))))?$_GET['view']:'mypost';
$actives = array($view=>' class="a"');
$opactives[$view] = 'class="a"';
$categorynum = $newprompt = array();

//操作
if($view == 'userapp') {

	space_merge($space, 'status');

	if($_GET['op'] == 'del') {
		$appid = intval($_GET['appid']);
		C::t('common_myinvite')->delete_by_appid_touid($appid, $_G['uid']);
		sendAppMessage(RES_YES, 'do_success', "home.php?mod=space&do=notice&view=userapp&quickforward=1");
	}

	$filtrate = 0;
	$count = 0;
	$apparr = array();
	$type = intval($_GET['type']);
	
	foreach(C::t('common_myinvite')->fetch_all_by_touid($_G['uid']) as $value) {
		$count++;
		$key = md5($value['typename'].$value['type']);
		$apparr[$key][] = $value;
		if($filtrate) {
			$filtrate--;
		} else {
			if($count < $perpage) {
				if($type && $value['appid'] == $type) {
					$list[$key][] = $value;
				} elseif(!$type) {
					$list[$key][] = $value;
				}
			}
		}
	}
	$mynotice = $count;

} else {

	if(!empty($_GET['ignore'])) {
		C::t('home_notification')->ignore($_G['uid']);
	}

	foreach (array('wall', 'piccomment', 'blogcomment', 'clickblog', 'clickpic', 'sharecomment', 'doing', 'friend', 'credit', 'bbs', 'system', 'thread', 'task', 'group') as $key) {
		$noticetypes[$key] = lang('notification', "type_$key");
	}

	$isread = in_array($_GET['isread'], array(0, 1)) ? intval($_GET['isread']) : 0;
	$category = $type = '';
	if(isset($_G['notice_structure'][$view])) {
		if(!in_array($view, array('mypost', 'interactive'))) {
			$category = $view;
		} else {
			$deftype = $_G['notice_structure'][$view][0];
			if($_G['member']['newprompt_num']) {
				foreach($_G['notice_structure'][$view] as $subtype) {
					if($_G['member']['newprompt_num'][$subtype]) {
						$deftype = $subtype;
						break;
					}
				}
			}
			$type = in_array($_GET['type'], $_G['notice_structure'][$view]) ? trim($_GET['type']) : $deftype;
		}
	}
	$wherearr = array();
	$new = -1;
	if(!empty($type)) {
		$wherearr[] = "`type`='$type'";
	}

	$sql = ' AND '.implode(' AND ', $wherearr);

	$newnotify = false;
	$count = C::t('home_notification')->count_by_uid($_G['uid'], $new, $type, $category);
	if($count) {
		if($new == 1 && $perpage == 30) {
			$perpage = 200;
		}
		$res = C::t('home_notification')->fetch_all_by_uid($_G['uid'], $new, $type, $start, $perpage, $category);
//		print_r($res);
		foreach($res as $value) {
			if($value['from_num'] > 0) $value['from_num'] = $value['from_num'] - 1;
			
			//匹配URL
//			$pat = '/<a(.*?)href="(.*?)"(.*?)>(.*?)<\/a>/i';     
//			preg_match_all($pat, $value['note'], $matchs);
//			$value['url'] = $_G['siteurl'].(count($matchs[2])==1?$matchs[2][0]:$matchs[2][1])."&platform=app";
			if($view!='system'){
				//个人消息的类型
				if($value['from_idtype'] == 'post'){
					$value['tid'] = $value['from_id'];
				}else{
					$value['tid'] = get_tid_by_pid($value['from_id']);
				}
			}else{
				$pat = '/<a(.*?)href="(.*?)"(.*?)>(.*?)<\/a>/i';
				preg_match_all($pat, $value['note'], $matchs);
				if($matchs[2]){
					$value['url'] = $_G['siteurl'].(count($matchs[2])==1?$matchs[2][0]:$matchs[2][1])."&platform=app";
				}
				$value['note'] = str_replace("看看我能做什么","",$value['note']);
			}
			//除去a标签
			$value['note'] = preg_replace("/<a[^>]*>查看<\/a>/is","$1",$value['note']);
			$value['note'] = preg_replace("#<a[^>]*>(.*?)</a>#is", "$1", $value['note']);
			$value['note'] = preg_replace("/<br[^>]*>/i", '', str_replace("\r\n", "<br/>", htmlspecialchars_decode(html_entity_decode(nl2br($value['note'])))));
			$value['dateline'] = custom_date($value['dateline']);
			$value['avatar'] = avatar($value['authorid'],'middle',TRUE);
			$value['isread'] = $value['new'] ? 0 : 1;
			
			unset($value['new']);
			$list[] = $value;
		}
		
	}

	if($newnotify) {
		C::t('home_notification')->ignore($_G['uid'], $type, $category, true, true);
		if($_G['setting']['cloud_status']) {
			$noticeService = Cloud::loadClass('Service_Client_Notification');
			$noticeService->setNoticeFlag($_G['uid'], TIMESTAMP);
		}
	}
	
	//更新是否已读
	helper_notification::update_newprompt($_G['uid'], ($type ? $type : $category));
	
	if($_G['setting']['my_app_status']) {
		$mynotice = C::t('common_myinvite')->count_by_touid($_G['uid']);
	}
	
	if($_G['member']['newprompt']) {
		$recountprompt = 0;
		foreach($_G['member']['category_num'] as $promptnum) {
			$recountprompt += $promptnum;
		}
		$recountprompt += $mynotice;
		if($recountprompt == 0) {
			C::t('common_member')->update($_G['uid'], array('newprompt' => 0));
		}
	}

	$readtag = array($type => ' class="a"');

}

dsetcookie('promptstate_'.$_G['uid'], $newprompt, 31536000);
sendAppMessage(RES_YES, array(FALSE,'列表返回成功！') , array('count'=>intval($count),'pagecount'=>@ceil($count/$perpage),'newscount'=> $_G['member']['category_num'][$view] ? $_G['member']['category_num'][$view] : 0 ,'list'=>$list));

//include_once template("diy:home/space_notice");


?>