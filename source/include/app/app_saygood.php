<?php

/**
 * 点赞行为
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}
$_GET['type'] = in_array($_GET['type'], array("thread", "forum")) ? $_GET['type'] : 'thread';

//取消点赞
if($_GET['op'] == 'delete') {

    $tid = I('id',0,'intval');
    $sid = I('saygood_id',0,'intval');
    if($sid){
        $thevalue = C::t('home_saygood')->fetch($sid);
    }else{
        if($tid){
            $thevalue = C::t('home_saygood')->fetch_by_id_idtype($tid,'tid',$_G['uid']);
            $sid = $thevalue['saygood_id'];
        }
    }

    if(empty($thevalue) || $thevalue['uid'] != $_G['uid']) {
        sendAppMessage(RES_NO, 'saygood_does_not_exist');
    }

    //C::t('home_saygood')->delete($sid);
    C::t('home_saygood')->update($sid, array('state' => '0'));
    C::t('forum_thread')->discrease($thevalue['id'], array('saygood'=>1));

    sendAppMessage(RES_YES,'do_success', array('saygood_id' => $sid, 'id' => $thevalue['id']));

} else {
	 //点赞
    //	cknewuser();
	
	$type = empty($_GET['type']) ? 'thread' : $_GET['type'];
	$id = empty($_GET['id']) ? 0 : intval($_GET['id']);
	$spaceuid = empty($_GET['spaceuid']) ? 0 : intval($_GET['spaceuid']);
	$idtype = $title = $icon = '';
	switch($type) {
		case 'thread':
			$idtype = 'tid';
			$thread = C::t('forum_thread')->fetch($id);
			$title = $thread['subject'];
			$icon = '<img src="static/image/feed/thread.gif" alt="thread" class="vm" /> ';
			break;
	}
	if(empty($idtype) || empty($title)) {
		//抱歉，您指定的信息无法点赞
		sendAppMessage(RES_NO,'saygood_cannot');
	}

    // 获取点赞记录
	$fav = C::t('home_saygood')->fetch_by_id_idtype($id, $idtype, $_G['uid']);
	$description = $extrajs = '';

    if ($fav)    // 曾经点赞过
    {
        if ($fav['state']) {                 // 不能重复点赞
            sendAppMessage(RES_NO,'saygood_repeat');
            exit();
        }
        else {                  // 取消了点赞，又再点赞，仅更新记录，不再奖励积分。
            C::t('home_saygood')->update($fav['saygood_id'], array('state'=>'1'));
            // return message
            sendAppMessage(RES_YES,'saygood_do_success', array('id' => $id, 'saygood_id' => $favid));
            exit();
        }
    }
    else {                  // 从未点赞过

        // 插入新记录
        $arr = array(
            'uid' => intval($_G['uid']),
            'idtype' => $idtype,
            'id' => $id,
            'spaceuid' => $spaceuid,
            'dateline' => TIMESTAMP
        );
        $favid = C::t('home_saygood')->insert($arr, true);
        C::t('forum_thread')->increase($id, array('saygood'=>1));

        // 奖励积分
        $credit_rule = C::t('common_credit_rule')->fetch_one_by_action('saygood');
        if ($credit_rule) {
            $credit = & credit::instance();
            $credit->updatemembercount($credit_rule, $_G['uid']);
        }

        // return message
        sendAppMessage(RES_YES,'saygood_do_success', array('id' => $id, 'saygood_id' => $favid));
        exit();
    }


}


?>