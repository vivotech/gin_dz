<?php

/**
 * 点赞行为
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}
$_GET['type'] = in_array($_GET['type'], array("thread", "forum")) ? $_GET['type'] : 'thread';

//取消点赞
if($_GET['op'] == 'delete') {
	
		$sid = intval($_GET['saygood_id']);
		$thevalue = C::t('home_saygood')->fetch($sid);
		if(empty($thevalue) || $thevalue['uid'] != $_G['uid']) {
			showmessage('saygood_does_not_exist');
		}
		if(/*submitcheck('deletesubmit')*/!submitcheck('deletesubmit')) { // 跳过确定键
			C::t('home_saygood')->delete($sid);
			C::t('forum_thread')->discrease($thevalue['id'], array('saygood'=>1));
			showmessage('do_success', dreferer(), array('saygood_id' => $sid, 'id' => $thevalue['id']), array('showdialog'=>1, 'showmsg' => true, 'closetime' => true, 'locationtime' => 3));
		}

} else {
//点赞
	cknewuser();
	
	$type = empty($_GET['type']) ? '' : $_GET['type'];
	$id = empty($_GET['id']) ? 0 : intval($_GET['id']);
	$spaceuid = empty($_GET['spaceuid']) ? 0 : intval($_GET['spaceuid']);
	$idtype = $title = $icon = '';
	switch($type) {
		case 'thread':
			$idtype = 'tid';
			$thread = C::t('forum_thread')->fetch($id);
			$title = $thread['subject'];
			$icon = '<img src="static/image/feed/thread.gif" alt="thread" class="vm" /> ';
			break;
	}
	if(empty($idtype) || empty($title)) {
		//抱歉，您指定的信息无法点赞
		showmessage('saygood_cannot');
	}

	$fav = C::t('home_saygood')->fetch_by_id_idtype($id, $idtype, $_G['uid']);
	if($fav) {
		showmessage('saygood_repeat');
	}
	$description = $extrajs = '';

	$fav_count = C::t('home_saygood')->count_by_id_idtype($id, $idtype);
	if(submitcheck('favoritesubmit') || ($type == 'forum' || $type == 'group' || $type == 'thread') && $_GET['formhash'] == FORMHASH) {
		$arr = array(
			'uid' => intval($_G['uid']),
			'idtype' => $idtype,
			'id' => $id,
			'spaceuid' => $spaceuid,
			'dateline' => TIMESTAMP
		);
		$favid = C::t('home_saygood')->insert($arr, true);
		C::t('forum_thread')->increase($id, array('saygood'=>1));
		showmessage('saygood_do_success', dreferer(), array('id' => $id, 'saygood_id' => $favid), array('showdialog' => true, 'closetime' => true, 'extrajs' => $extrajs));
	}
}

include template('home/spacecp_saygood');


?>