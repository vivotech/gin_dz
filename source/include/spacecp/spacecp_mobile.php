<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

if ( !$_G['member']['uid'] )
{
  return;
}

// get mobile identifying record
$user_id = $_G['member']['uid'];
$mobileIdentifying = C::t('common_member_profile')->fetch($user_id);
if(!empty($mobileIdentifying['mobile'])){
	$identifying_message = C::t('common_member_identifying')->fetch_by_mobile($mobileIdentifying['mobile']);
}else{
	$identifying_message = array(
		'is_verify'=>0
	);
}


include template("home/spacecp_mobile");