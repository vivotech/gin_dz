<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}


require_once DISCUZ_ROOT."/source/plugin/gsignin/function/function.php";

$set = $_G['cache']['plugin']['gsignin'];
$navigation[] = lang('plugin/gsignin','s00010');

//获取插件名称和版本号
$plugin_info = DB::fetch_first('select * from %t where identifier=%s', array('common_plugin','gsignin'));

// 判断用户是否登录
if(empty($_G['uid'])){
	showmessage(lang('plugin/gshop','Please login first!'), '', array(), array('login' => true));
	exit;
}
// 判断用户是否有权限
$adminUids=explode('/',$set['adminUids']);
if(!in_array($_G['uid'],$adminUids)){
	showmessage(lang('plugin/gsignin','s00006'));
}

$members = C::t('#gsignin#gsignin_member')->fetch_all_my();

if($_GET['action'] == "delete") {//屏蔽用户和取消屏蔽操作
	
	if ($_GET['formhash']==formhash()){
		$member = C::t('#gsignin#gsignin_member')->fetch_first_by_uid($_GET['uid']);
		if($_GET['do']=='shield'){//屏蔽用户
			$member['isshield']=1;
		}else if($_GET['do']=='shielded'){//取消屏蔽
			$member['isshield']=0;
		}
		C::t('#gsignin#gsignin_member')->update($member['uid'],$member);
		showRight(lang('plugin/gsignin','s00025'),"plugin.php?id=gsignin:member");
	}
}

else if($_GET['action'] == "search") {//检索功能
	$where = ' where 1=1';
	$url = 'plugin.php?id=gsignin:member&action=search';
	
	if(!empty($_GET['uid']) && $_GET['uid'] != -1) {
		$where = $where.' and uid='.$_GET['uid'];
		$url = $url.'&uid='.$_GET['uid'];
	}
	
	if(!empty($_GET['begindays'])) {
		$where = $where.' and continuous>='.$_GET['begindays'];
		$url=$url.'&begindays='.$_GET['begindays'];
	}
	
	if(!empty($_GET['enddays'])) {
		$where = $where.' and continuous<='.$_GET['enddays'];
		$url=$url.'&enddays='.$_GET['enddays'];
	}
	
	$currpage=$_GET['page']?$_GET['page']:1;
	$perpage=20;
	$num = C::t("#gsignin#gsignin_member")->fetch_count_by_where($where);
	$members = C::t("#gsignin#gsignin_member")->fetch_limit_by_where($where,($currpage-1)*$perpage,$perpage);
	$paging = helper_page :: multi($num, $perpage, $currpage, $url, 0, 11, false, false);
	$totalPage = ceil($num/$perpage);
	include template('gsignin:cp/member_list');
}

else if($_GET['action'] == "update_input"){//编辑弹窗
	$member=C::t("#gsignin#gsignin_member")->fetch_first_by_uid($_GET['uid']);
	include template('gsignin:member/update');
}

else if($_GET['action'] == "update"){//编辑操作
	if($_GET['formhash']==formhash()){
		$tt='/^\\d+$/';
		preg_match($tt,$_GET['card'],$result);
		if(empty($result)){
			showError(lang('plugin/gsignin', 's00056'));
			exit;
		}
		$updatemember=array(
			'card'=>$_GET['card'],
		); 
		C::t('#gsignin#gsignin_member')->update($_GET['uid'],$updatemember);
		showRight(lang('plugin/gsignin', 's00057'),'plugin.php?id=gsignin:member');
	}
}

else if($_GET['action'] == "extend") {

}
else {
	$currpage=$_GET['page']?$_GET['page']:1;
	$perpage=20;
	$num = C::t("#gsignin#gsignin_member")->fetch_count();
	$members = C::t("#gsignin#gsignin_member")->fetch_limit(($currpage-1)*$perpage,$perpage);
	$url = 'plugin.php?id=gsignin:member';
	$paging = helper_page :: multi($num, $perpage, $currpage, $url, 0, 11, false, false);
	$totalPage = ceil($num/$perpage);

	include template('gsignin:cp/member_list');
	
}