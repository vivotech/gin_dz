<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

require_once DISCUZ_ROOT."/source/plugin/gsignin/function/function.php";

$set = $_G['cache']['plugin']['gsignin'];
$navigation[] = lang('plugin/gsignin','s00005');

// 判断用户是否登录
if(empty($_G['uid'])){
	showmessage(lang('plugin/gshop','Please login first!'), '', array(), array('login' => true));
	exit;
}

//获取插件名称和版本号
$plugin_info = DB::fetch_first('select * from %t where identifier=%s', array('common_plugin','gsignin'));
// 判断用户是否有权限
$adminUids=explode('/',$set['adminUids']);
if(!in_array($_G['uid'],$adminUids)){
	showmessage(lang('plugin/gsignin','s00006'));
}

if($_GET['action'] == "award") {
	
	if ($_GET['formhash']==formhash()){
		//debug($_GET);
		for($i=0; $i<count($_GET['continuous']); $i++) {
			if($_GET['continuous'][$i] !='') {
				if(!empty($_GET['delete'][$i])) {
					C::t("#gsignin#gsignin_award")->delete($_GET['awardid'][$i]);
				} else {
					if(!empty($_GET['awardid'][$i])) {
						$award = array(
							'id'=>$_GET['awardid'][$i],
							'continuous'=>$_GET['continuous'][$i],
							'extcredits1'=>$_GET['extcredits1'][$i],
							'extcredits2'=>$_GET['extcredits2'][$i],
							'extcredits3'=>$_GET['extcredits3'][$i],
							'extcredits4'=>$_GET['extcredits4'][$i],
							'extcredits5'=>$_GET['extcredits5'][$i],
							'extcredits6'=>$_GET['extcredits6'][$i],
							'extcredits7'=>$_GET['extcredits7'][$i],
							'extcredits8'=>$_GET['extcredits8'][$i],
							'color'=>$_GET['color'][$i],
							'weight'=>$_GET['weight'][$i]
						);
						C::t("#gsignin#gsignin_award")->update($award['id'],$award);
					}else {
						$award = array(
							'continuous'=>$_GET['continuous'][$i],
							'extcredits1'=>$_GET['extcredits1'][$i],
							'extcredits2'=>$_GET['extcredits2'][$i],
							'extcredits3'=>$_GET['extcredits3'][$i],
							'extcredits4'=>$_GET['extcredits4'][$i],
							'extcredits5'=>$_GET['extcredits5'][$i],
							'extcredits6'=>$_GET['extcredits6'][$i],
							'extcredits7'=>$_GET['extcredits7'][$i],
							'extcredits8'=>$_GET['extcredits8'][$i],
							'color'=>$_GET['color'][$i],
							'weight'=>$_GET['weight'][$i]
						);
						C::t("#gsignin#gsignin_award")->insert($award);
					}
				}
			}
		}
		showRight(lang('plugin/gsignin','s00007'),"plugin.php?id=gsignin:award");
	}
}
else if($_GET['action'] == "extend") {

}
else {
	$awards = C::t("#gsignin#gsignin_award")->fetch_all_cp();
	include template('gsignin:cp/award');
}