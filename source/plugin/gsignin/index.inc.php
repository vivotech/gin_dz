<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}
//TODO 设置用在脚本中所有日期/时间函数的默认时区，其中PRC为“中华人民共和国”
date_default_timezone_set('PRC');

require_once DISCUZ_ROOT."/source/plugin/gsignin/function/function.php";

$set = $_G['cache']['plugin']['gsignin'];

$navtitle = $set['title'];
$metakeywords = $set['keywords'];
$metadescription = $set['description'];

//获取全局配置信息
$pages=getConfig('#gsignin#gsignin_config', 'global', 'perpage');
$perpage=$pages['value'];
$perpage = 10;

// 获取今天日期
$today = strtotime(date('Y-m-d',TIMESTAMP));

// 默认查看累计签到排行
$request_action = isset($_GET['action']) ? $_GET['action'] : 'total';

// 签到动作
if($_GET['action'] == "signin") {

	if ($_GET['formhash']==formhash()){

		// 判断用户是否登录
		if(empty($_G['uid'])){
			showError(lang('plugin/gsignin','s00001'));
			exit;
		}
		// 判断用户组是否有权限
		if(!in_array($_G['groupid'], unserialize($set['useGroups']))) {
			showError(lang('plugin/gsignin','s00002'));
			exit;
        }

		$currentTime = TIMESTAMP;
		$member = C::t('#gsignin#gsignin_member')->fetch_first_by_uid($_G['uid']);
		if(empty($member)){
			$member = array(
				'uid'=>$_G['uid'],
				'continuous'=>0,
				'total'=>0,
				'difference'=>0,
				'lasttime'=>0,
				'isshield'=>0
			);
			C::t('#gsignin#gsignin_member')->insert($member);
		}
		//TODO 判断用户是否被屏蔽
		if($member['isshield']==1){
			showError(lang('plugin/gsignin','s00024'));
			exit;
		}

		// 获取今日签到排名
		$ranking = C::t('#gsignin#gsignin_signin')->fetch_ranking($currentTime,$today);
		$ranking = $ranking+1;

		// 获取前一次签到记录
		$prevSignin = C::t('#gsignin#gsignin_signin')->fetch_last_by_uid($_G['uid']);
		
		// 判断是否第一次签到
		if(empty($prevSignin)) {
			$prevSigninTime = 0;
			$difference = 0;
		} else {
			$prevSigninTime = $prevSignin['currenttime'];
			// TODO 因为修改字段原因添加了判断，发布的时候可以去掉
			if($prevSignin['ranking'] !=0 ){
				$difference = $ranking - $prevSignin['ranking'];
			}else {
				$difference = 0;
			}
		}

		// 判断今日是否签到
		if( $prevSigninTime>=$today) {
			showError(lang('plugin/gsignin','s00003'));
			exit;
		}
		
		// 获得最后一次断签记录
		$breakSignin = C::t('#gsignin#gsignin_signin')->fetch_last_break_by_uid($_G['uid']);
		
		// 获得连续签到天数
		$prevContinuous = C::t('#gsignin#gsignin_signin')->fetch_continuous_by_uid($_G['uid'],$breakSignin['currenttime']);

		// 判断本次签到是否连续签到
		if(strtotime(date('Y-m-d',$currentTime))-strtotime(date('Y-m-d',$prevSigninTime))>(60*60*24)) {
			$member['continuous'] = 1;
		}else {
			$member['continuous'] = $prevContinuous+1;
		}

		// 获取奖励设置
		$award =  C::t("#gsignin#gsignin_award")->fetch_first_by_continuous($member['continuous']);
		// 获取积分设置
		$extcredits = $_G['setting']['extcredits'];


		foreach($extcredits as $key => $val){
			if(!empty($award)){
				$awardExtcredits['extcredits'.$key] = $award['extcredits'.$key];
			} else{
				$awardExtcredits['extcredits'.$key] = 0;
			}
			updatemembercount($_G['uid'], array('extcredits'.$key=>$award['extcredits'.$key]));
		}


		$pluginName = 'gsignin';
		$componentTable = '#gsignin#gsignin_component';
		$model = 'extend';

		if (file_exists('source/plugin/'.$pluginName.'/com/'.$model.'/'.$model.'.php')){
			$component= C::t($componentTable)->fetch_first_by_model($model);
			if (!empty($component) && $component['flag'] == 1){
				$extend = C::t('#gsignin#gsignin_extend')->fetch_first_by_id($ranking);
				if(!empty($extend)) {
					foreach($extcredits as $key => $val){
						$awardExtcredits['extcredits'.$key] += $extend['extcredits'.$key];
						updatemembercount($_G['uid'], array('extcredits'.$key=>$extend['extcredits'.$key]));
					}
				}
			}
		}

        // 插入本次签到数据
		$currentSignin = array(
			'uid'=>$_G['uid'],
			'prevtime'=>$prevSigninTime,
			'currenttime'=>$currentTime,
			'ranking'=>$ranking,
			'extcredits1'=>$awardExtcredits['extcredits1'],
			'extcredits2'=>$awardExtcredits['extcredits2'],
			'extcredits3'=>$awardExtcredits['extcredits3'],
			'extcredits4'=>$awardExtcredits['extcredits4'],
			'extcredits5'=>$awardExtcredits['extcredits5'],
			'extcredits6'=>$awardExtcredits['extcredits6'],
			'extcredits7'=>$awardExtcredits['extcredits7'],
			'extcredits8'=>$awardExtcredits['extcredits8'],
			'issupplement'=>0
		);
		C::t('#gsignin#gsignin_signin')->insert($currentSignin);

		$member['total'] = $member['total']+1;
		$member['difference'] = $difference;
		$member['lasttime'] = $currentTime;
		// 更新用户数据
		C::t('#gsignin#gsignin_member')->update($member['uid'],$member);
		// 判断是否开启签到发帖
		if(!empty($set['isPost'])) {
			if(!empty($set['fid'])) {
				$post = C::t('#gsignin#gsignin_post')->fetch_first_by_id(1);
				if(empty($post) || $today > $post['time']) {
					$tid = generatethread(lang('plugin/gsignin','s00058').date('Y'.lang('plugin/gsignin','year').'m'.lang('plugin/gsignin','month').'d'.lang('plugin/gsignin','fuck'),$today).lang('plugin/gsignin','s00059'),lang('plugin/gsignin','s00060').$ranking.lang('plugin/gsignin','s00061'),$_G['clientip'],$_G['uid'],'',$set['fid']);
					if(empty($post)) {
						$post = array('tid'=>$tid,'time'=>$today);
						C::t('#gsignin#gsignin_post')->insert($post);
					} else {
						$post['tid'] = $tid;
						$post['time'] = $today;
						C::t('#gsignin#gsignin_post')->update($post['id'],$post);
					}
				} else {
					generatepost(lang('plugin/gsignin','s00060').$ranking.lang('plugin/gsignin','s00061'), $_G['uid'], $post['tid'], '', '', '');
				}
			}
		}

		$pluginName = 'gsignin';
		$componentTable = '#gsignin#gsignin_component';
		$model = 'auto';

		if (file_exists('source/plugin/'.$pluginName.'/com/'.$model.'/'.$model.'.php')){
			$component= C::t($componentTable)->fetch_first_by_model($model);
			if (!empty($component) && $component['flag'] == 1){
				// 判断自定签到是否有配置
				if(!empty($set['uids']) && !empty($set['prob'])) {
					if($set['prob'] > 0){
						$ranking += 1;
						$tempUids = explode('/',$set['uids']);
						$resultUids = array();
						$resultMember = array();
						foreach($tempUids as $key => $val) {
							$discuzMember = getuserbyuid(intval($val));
							if(!empty($discuzMember)) {

								$member = C::t('#gsignin#gsignin_member')->fetch_first_by_uid(intval($val));

								if(empty($member)){
									$member = array(
								'uid'=>intval($val),
								'continuous'=>0,
								'total'=>0,
								'difference'=>0,
								'lasttime'=>0,
								'isshield'=>0
									);
									C::t('#gsignin#gsignin_member')->insert($member);
								}


								// 获取前一次签到记录
								$prevSignin = C::t('#gsignin#gsignin_signin')->fetch_last_by_uid(intval($val));
								// 判断是否第一次签到
								if(empty($prevSignin)) {
									$prevSigninTime = 0;
									$difference = 0;
								} else {
									$prevSigninTime = $prevSignin['currenttime'];
									// TODO 因为修改字段原因添加了判断，发布的时候可以去掉
									if($prevSignin['ranking'] !=0 ){
										$difference = $ranking - $prevSignin['ranking'];
									}else {
										$difference = 0;
									}
								}
									
								// 判断今日是否签到
								if( $prevSigninTime<$today) {
									$resultUids[] = array(
							'uid'=>intval($val),
							'prevtime'=>$prevSigninTime,
							'currenttime'=>$currentTime+1,
							'ranking'=>$ranking,
							'extcredits1'=>0,
							'extcredits2'=>0,
							'extcredits3'=>0,
							'extcredits4'=>0,
							'extcredits5'=>0,
							'extcredits6'=>0,
							'extcredits7'=>0,
							'extcredits8'=>0,
							'issupplement'=>1
									);

									$resultMember[] = array(
								'continuous' => $member['continuous']+1,
								'total' => $member['total']+1,
								'difference' => $difference,
								'lasttime' => $currentTime+1
									);
								}
							}
						}
						if(!empty($resultUids)) {
							$tempRandom = rand(0,count($resultUids)-1);
							$resultRandom = rand(1,100);
							if($resultRandom <= $set['prob']) {
								C::t('#gsignin#gsignin_signin')->insert($resultUids[$tempRandom]);
								C::t('#gsignin#gsignin_member')->update($resultUids[$tempRandom]['uid'],$resultMember[$tempRandom]);
							}
						}
					}
				}
			}
		}
		showRight(lang('plugin/gsignin','s00004'),"plugin.php?id=gsignin:index");
	}
}

else if($_GET['action'] == "calendar") {

	$pluginName = 'gsignin';
	$componentTable = '#gsignin#gsignin_component';
	$model = 'calendar';

	if (file_exists('source/plugin/'.$pluginName.'/com/'.$model.'/'.$model.'.php')){
		$component= C::t($componentTable)->fetch_first_by_model($model);
		if (!empty($component) && $component['flag'] == 1){
			require_once DISCUZ_ROOT.'source/plugin/'.$pluginName.'/com/'.$model.'/'.$model.'.php';
		}
	}

	include template('gsignin:calendar');
}

// 查看连续签到排行
else if($_GET['action'] == "continuous") {

	$pluginName = 'gsignin';
	$componentTable = '#gsignin#gsignin_component';
	$model = 'continuous';

	if (file_exists('source/plugin/'.$pluginName.'/com/'.$model.'/'.$model.'.php')){
		$component= C::t($componentTable)->fetch_first_by_model($model);
		if (!empty($component) && $component['flag'] == 1){
			require_once DISCUZ_ROOT.'source/plugin/'.$pluginName.'/com/'.$model.'/'.$model.'.php';
		}
	}

	//配置管理
	$model2 = 'copyright';
	if (file_exists('source/plugin/'.$pluginName.'/com/'.$model2.'/'.$model2.'.php')){
		$component2= C::t($componentTable)->fetch_first_by_model($model2);
		if (!empty($component2) && $component2['flag'] == 1){
			require_once DISCUZ_ROOT.'source/plugin/'.$pluginName.'/com/'.$model2.'/'.$model2.'.php';
		}
	}

	$rightCheck = "continuous";
	include template('gsignin:index/index');
}
// 查看累计签到排行
else if($_GET['action'] == "total" || $request_action === 'total') {

	$pluginName = 'gsignin';
	$componentTable = '#gsignin#gsignin_component';
	$model = 'total';

	if (file_exists('source/plugin/'.$pluginName.'/com/'.$model.'/'.$model.'.php')){
		$component= C::t($componentTable)->fetch_first_by_model($model);
		if (!empty($component) && $component['flag'] == 1){
			require_once DISCUZ_ROOT.'source/plugin/'.$pluginName.'/com/'.$model.'/'.$model.'.php';
		}
	}

	//配置管理
	$model2 = 'copyright';
	if (file_exists('source/plugin/'.$pluginName.'/com/'.$model2.'/'.$model2.'.php')){
		$component2= C::t($componentTable)->fetch_first_by_model($model2);
		if (!empty($component2) && $component2['flag'] == 1){
			require_once DISCUZ_ROOT.'source/plugin/'.$pluginName.'/com/'.$model2.'/'.$model2.'.php';
		}
	}

	$rightCheck = "total";
	include template('gsignin:index/index');
}

else if($_GET['action'] == "buycard"){//购买补签卡弹窗与操作

	$pluginName = 'gsignin';
	$componentTable = '#gsignin#gsignin_component';
	$model = 'card';

	if (file_exists('source/plugin/'.$pluginName.'/com/'.$model.'/'.$model.'.php')){
		$component= C::t($componentTable)->fetch_first_by_model($model);
		if (!empty($component) && $component['flag'] == 1){
			require_once DISCUZ_ROOT.'source/plugin/'.$pluginName.'/com/'.$model.'/'.$model.'.php';
		}else{
			$message=lang('plugin/gsignin', 's00048');
			include template('gsignin:showajaxerror');
			exit;
		}
	}else{
		$message=lang('plugin/gsignin', 's00047');
		include template('gsignin:showajaxerror');
		exit;
	}
}

else if($_GET['action'] == "add")
{
	$pluginName = 'gsignin';
	$componentTable = '#gsignin#gsignin_component';
	$model = 'card';

	if (file_exists('source/plugin/'.$pluginName.'/com/'.$model.'/'.$model.'.php')){
		$component= C::t($componentTable)->fetch_first_by_model($model);
		if (!empty($component) && $component['flag'] == 1){
			require_once DISCUZ_ROOT.'source/plugin/'.$pluginName.'/com/'.$model.'/'.$model.'.php';
		}else{
			showError(lang('plugin/gsignin', 's00048'));
			exit;
		}
	}else{
		showError(lang('plugin/gsignin', 's00047'));
		exit;
	}
}

else if($_GET['action'] == "extend") {

}

//显示页面 index
else {
	
	$pluginName = 'gsignin';
	$componentTable = '#gsignin#gsignin_component';
	$model = 'change';

	if (file_exists('source/plugin/'.$pluginName.'/com/'.$model.'/'.$model.'.php')){
		$component= C::t($componentTable)->fetch_first_by_model($model);
		if (!empty($component) && $component['flag'] == 1){
			require_once DISCUZ_ROOT.'source/plugin/'.$pluginName.'/com/'.$model.'/'.$model.'.php';
		}
	}

	//配置管理
	$model2 = 'copyright';
	if (file_exists('source/plugin/'.$pluginName.'/com/'.$model2.'/'.$model2.'.php')){
		$component2= C::t($componentTable)->fetch_first_by_model($model2);
		if (!empty($component2) && $component2['flag'] == 1){
			require_once DISCUZ_ROOT.'source/plugin/'.$pluginName.'/com/'.$model2.'/'.$model2.'.php';
		}
	}

	$rightCheck = "";
	if(!empty($_G['uid'])) {

		$member = C::t('#gsignin#gsignin_member')->fetch_first_by_uid($_G['uid']);
		if(empty($member)){
			$member = array(
				'uid'=>$_G['uid'],
				'continuous'=>0,
				'total'=>0,
				'difference'=>0,
				'lasttime'=>0,
				'isshield'=>0
			);
			C::t('#gsignin#gsignin_member')->insert($member);
		}
		// 获取本月第一天日期的时间戳
		$currentMouthFirstDay = strtotime(date('Y-m-01',TIMESTAMP));
		// 获取本月最后一天日期的时间戳
		$currentMouthLastDay = strtotime(date('Y-m-d',$currentMouthFirstDay)."+1 month -1 day");
		// 获取今天的天数
		$currentMouthTotalDay = date("d",$currentMouthLastDay);

		$nextMouthFirstDay = strtotime(date('Y-m-d',$currentMouthFirstDay)."+1 month");
		// 获得本月当前用户签到记录
		$monthSignins =  C::t('#gsignin#gsignin_signin')->fetch_all_by_uid_month($_G['uid'],$currentMouthFirstDay,$nextMouthFirstDay);

		$forget = date("d",$today) - count($monthSignins);
		if($member['lasttime']>$today) {
			$ranking = C::t('#gsignin#gsignin_signin')->fetch_ranking($member['lasttime'],$today);
			$ranking = $ranking+1;
		}
	}
	$memberTops = C::t('#gsignin#gsignin_member')->fetch_all_top_by_today($today);
	$currpage=$_GET['page']?$_GET['page']:1;
	if ( $perpage>0 )
    {
		$offset=($currpage-1)*$perpage+3;
		
		//查询今天所有的签到用户数量
		$num = C::t('#gsignin#gsignin_member')->fetch_count_by_today($today);
		$bottomnum=$num-count($memberTops);
		
		$memberBottoms = C::t('#gsignin#gsignin_member')->fetch_all_bottom_by_today($today,$offset,$perpage);
		
		
		$url='plugin.php?id=gsignin:index';
		$paging=helper_page::multi($bottomnum,$perpage,$currpage,$url,0,11,false,false);
		if($bottomnum>0){
			$isPage=ceil($bottomnum/$perpage);
		}else{
			$isPage=1;
		}
	}else{
		$offset=3;
		$num = C::t('#gsignin#gsignin_member')->fetch_count_by_today($today);
		$bottomnum=$num-count($memberTops);
		$memberBottoms = C::t('#gsignin#gsignin_member')->fetch_all_bottom_by_today($today,$offset,$num);
		$isPage=1;
	}
	// 控制用户名颜色变化，这里的处理方法也不是灰常好

	$awards = C::t("#gsignin#gsignin_award")->fetch_all_front();

	for($i=0;$i<count($memberTops);$i++) {
		for($j=0;$j<count($awards);$j++) {
			if($memberTops[$i]['continuous']>=$awards[$j]['continuous']) {
				$memberTops[$i]['color'] = $awards[$j]['color'];
				if($awards[$j]['weight'] > 0){
					$memberTops[$i]['weight'] = "bold";
				}
				break;
			}
		}
	}

	for($i=0;$i<count($memberBottoms);$i++) {
		for($j=0;$j<count($awards);$j++) {
			if($memberBottoms[$i]['continuous']>=$awards[$j]['continuous']) {
				$memberBottoms[$i]['color'] = $awards[$j]['color'];
				if($awards[$j]['weight'] > 0){
					$memberBottoms[$i]['weight'] = "bold";
				}
				break;
			}
		}
	}
	//debug($memberBottoms);
	include template('gsignin:index/index');
}