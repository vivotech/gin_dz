<?php
if(!defined('IN_DISCUZ')){
	exit('Access Denied');
}

class table_gsignin_signin extends discuz_table
{
	public function __construct(){
		$this->_table	= 'gsignin_signin';
		$this->_pk		= 'id';
		parent::__construct();
	}
	
	public function fetch_all_my(){
    	return DB::fetch_all('select * from %t',array($this->_table));
    }
    // 获取指定用户一个月的签到记录
	public function fetch_all_by_uid_month($uid,$currentMonth,$nextMouth){
    	return DB::fetch_all('select * from %t where uid=%d and currenttime>=%d and currenttime<%d order by currenttime',array($this->_table,$uid,$currentMonth,$nextMouth));
    }
    // 根据id获取记录
	public function fetch_first_by_id($id){
    	return DB::fetch_first('select * from %t where id=%d',array($this->_table,$id));
    }
    // 获取指定用户最后一次签到记录
	public function fetch_last_by_uid($uid){
    	return DB::fetch_first('select * from %t where uid=%d order by currenttime desc',array($this->_table,$uid));
    }
    // 获取指定用户最后一次断签记录
	public function fetch_last_break_by_uid($uid){
    	return DB::fetch_first('select * from %t where  floor((currenttime+8*60*60)/(60*60*24))*60*60*24-floor((prevtime+8*60*60)/(60*60*24))*60*60*24>60*60*24 and uid=%d order by currenttime desc',array($this->_table,$uid));
    }
    // 获取指定用户指定时间的前一次签到记录
	public function fetch_prev_by_uid($uid,$time){
    	return DB::fetch_first('select * from %t where uid=%d and currenttime<%d order by currenttime desc',array($this->_table,$uid,$time));
    }
    // 获取指定用户指定时间的后一次签到记录
	public function fetch_next_by_uid($uid,$time){
    	return DB::fetch_first('select * from %t where uid=%d and currenttime>%d order by currenttime',array($this->_table,$uid,$time));
    }
    
    // 获取指定用户指定签到记录的前一次签到记录
	public function fetch_prev_by_uid_($uid,$id){
    	return DB::fetch_first('select * from %t where uid=%d and id<%d order by id desc',array($this->_table,$uid,$id));
    }
    // 获取指定用户指定签到记录的后一次签到记录
	public function fetch_next_by_uid_($uid,$id){
    	return DB::fetch_first('select * from %t where uid=%d and id>%d order by id',array($this->_table,$uid,$id));
    }
	
    public function fetch_limit($offset,$limit){
    	return DB::fetch_all('select * from %t order by currenttime desc limit %d,%d',array($this->_table,$offset,$limit));
    }
    
 	public function fetch_limit_by_where($where,$offset,$limit){
    	return DB::fetch_all('select * from %t %i order by currenttime desc limit %d,%d',array($this->_table,$where,$offset,$limit));
    }
    
  	public function fetch_count(){
    	return DB::result_first('select count(*) from %t',array($this->_table));
    }
  	
    public function fetch_count_by_where($where){
    	return DB::result_first('select count(*) from %t %i',array($this->_table,$where));
    }
	
    // 获取指定用户大于指定时间的签到记录（主要用于获取连续签到记录）
	public function fetch_continuous_by_uid($uid,$begintime){
    	return DB::result_first('select count(*) from %t where uid=%d and currenttime>=%d',array($this->_table,$uid,$begintime));
    }
    
	public function fetch_ranking($time,$today){
    	return DB::result_first('select count(*) from %t where currenttime<%d and currenttime>=%d',array($this->_table,$time,$today));
    }
    
	public function fetch_ranking_member($time,$today){
    	return DB::fetch_all('select * from %t where currenttime<%d and currenttime>=%d order by currenttime desc',array($this->_table,$time,$today));
    }
}
?>