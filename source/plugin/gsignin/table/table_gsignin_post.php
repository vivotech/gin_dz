<?php
if(!defined('IN_DISCUZ')){
	exit('Access Denied');
}

class table_gsignin_post extends discuz_table
{
	public function __construct(){
		$this->_table	= 'gsignin_post';
		$this->_pk		= 'id';
		parent::__construct();
	}
	
	public function fetch_all_my(){
    	return DB::fetch_all('select * from %t',array($this->_table));
    }
    
	public function fetch_first_by_id($id){
    	return DB::fetch_first('select * from %t where id=%d',array($this->_table,$id));
    }
    
}
?>