<?php
if(!defined('IN_DISCUZ')){
	exit('Access Denied');
}

class table_gsignin_configtype extends discuz_table
{
	public function __construct(){
		$this->_table	= 'gsignin_configtype';
		$this->_pk		= 'id';
		parent::__construct();
	}
	
	public function fetch_all_my(){
    	return DB::fetch_all('select * from %t order by displayorder',array($this->_table));
    }
    
	public function fetch_first_by_id($id){
    	return DB::fetch_first('select * from %t where id=%d',array($this->_table,$id));
    }

    public function fetch_all_limit_by_where($offset,$limit,$where=''){
    	return DB::fetch_all('select * from %t where 1=1 %i order by displayorder limit %d,%d',array($this->_table,$where,$offset,$limit));
    }
    
  	public function fetch_all_count_by_where($where=''){
    	return DB::result_first('select count(*) from %t where 1=1 %i order by displayorder',array($this->_table,$where));
    }
    
}
?>