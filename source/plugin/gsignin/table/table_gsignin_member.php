<?php
if(!defined('IN_DISCUZ')){
	exit('Access Denied');
}

class table_gsignin_member extends discuz_table
{
	public function __construct(){
		$this->_table	= 'gsignin_member';
		$this->_pk		= 'uid';
		parent::__construct();
	}
	
	public function fetch_all_my(){
    	return DB::fetch_all('select * from %t',array($this->_table));
    }
  
  /**
   * @param $today int  timestamp
   * @return array
   */
	public function fetch_all_top_by_today($today = '')
  {
    
    return DB::fetch_all('select * from %t where lasttime>=%d order by lasttime,uid limit 0,3',array($this->_table,$today));
  }
  
  public function fetch_first_by_uid_today($uid, $today = '')
  {
    if ( empty($today) )
    {
      $today = strtotime(date("Y-m-d"));
    }
    return DB::fetch_all('select * from %t where uid=%d and lasttime>=%d order by lasttime desc limit 0,1',array($this->_table, $uid, $today));
  }
	
	public function fetch_all_bottom_by_today($today,$offset,$limit){
    	return DB::fetch_all('select * from %t where lasttime>=%d order by lasttime,uid limit %d,%d',array($this->_table,$today,$offset,$limit));
    }
	
	public function fetch_by_uid($uid) {
		return DB::fetch_first('select * from %t where uid=%d',array($this->_table,$uid));
	}
	
	public function fetch_count_by_today($today){
    	return DB::result_first('select count(*) from %t where lasttime>=%d order by lasttime,uid',array($this->_table,$today));
    }
    
	public function fetch_all_top_by_continuous(){
    	return DB::fetch_all('select * from %t where total>0 order by continuous desc,uid limit 0,3',array($this->_table));
    }
	
	public function fetch_all_bottom_by_continuous($offset,$limit){
    	return DB::fetch_all('select * from %t where total>0 order by continuous desc,uid limit %d,%d',array($this->_table,$offset,$limit));
    }
    
	public function fetch_count_by_continuous(){
    	return DB::result_first('select count(*) from %t where total>0 order by continuous desc,uid',array($this->_table));
    }
  
	public function fetch_all_top_by_total(){
    	return DB::fetch_all('select * from %t where total>0 order by total desc,uid limit 0,3',array($this->_table));
    }
	
	public function fetch_all_bottom_by_total($offset = 0,$limit = 0){
    	$sql = sprintf('select * from %s where total>0 order by total desc,uid', DB::table($this->_table));
		if ($limit)
		{
			$sql .= sprintf(' limit %d,%d', $offset, $limit);
		}
    	return DB::fetch_all($sql);
    }
    
	public function fetch_count_by_total(){
    	return DB::result_first('select count(*) from %t where total>0 order by total desc,uid',array($this->_table));
    }
     
	public function fetch_all_my_by_today($today){
    	return DB::fetch_all('select * from %t where lasttime>=%d order by lasttime',array($this->_table,$today));
    }
    
	public function fetch_first_by_uid($uid){
    	return DB::fetch_first('select * from %t where uid=%d',array($this->_table,$uid));
    }

    public function fetch_limit($offset,$limit){
    	return DB::fetch_all('select * from %t limit %d,%d',array($this->_table,$offset,$limit));
    }
    
  	public function fetch_count(){
    	return DB::result_first('select count(*) from %t',array($this->_table));
    }
	
    public function fetch_limit_by_where($where,$offset,$limit){
    	return DB::fetch_all('select * from %t %i limit %d,%d',array($this->_table,$where,$offset,$limit));
    }
    
  	public function fetch_count_by_where($where){
    	return DB::result_first('select count(*) from %t %i',array($this->_table,$where));
    }

    public function fetch_rows($fields = '*', $where = '', $order = '')
    {
        $sql = sprintf("SELECT %s FROM %s", $fields, DB::table($this->_table) );
        if ($where)
        {
            $sql .= ' WHERE ' . $where;
        }
        if ($order)
        {
            $sql .= ' ORDER BY ' . $order;
        }
        return DB::fetch_all($sql);
    }

}

?>

