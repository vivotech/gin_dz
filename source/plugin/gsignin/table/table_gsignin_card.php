<?php
if(!defined('IN_DISCUZ')){
	exit('Access Denied');
}

class table_gsignin_card extends discuz_table
{
	public function __construct(){
		$this->_table	= 'gsignin_card';
		$this->_pk		= 'id';
		parent::__construct();
	}
	//查询设置信息
	public function fetch_card_setInfo()
 	{
 		return DB::fetch_first('select * from %t', array($this->_table));
 	}
 	//用于判断是否是第一次设置
	public function count_card_setInfo()
 	{
 		return DB::result_first('select count(*) from %t', array($this->_table));
 	}
}
?>