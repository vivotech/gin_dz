<?php
 
 class table_gsignin_component extends discuz_table{
 	
 	public function __construct(){
 		$this->_table='gsignin_component';
 		$this->_pk='id';
 		parent::__construct();
 	}
 	
	public function fetch_all_my(){
    	return DB::fetch_all('select * from %t',array($this->_table));
    }
    
	public function fetch_first_by_id($id){
    	return DB::fetch_first('select * from %t where id=%d',array($this->_table,$id));
    }
    
 	public function fetch_first_by_model($model){
    	return DB::fetch_first('select * from %t where model=%s',array($this->_table,$model));
    }

    public function fetch_all_limit($offset,$limit){
    	return DB::fetch_all('select * from %t limit %d,%d',array($this->_table,$offset,$limit));
    }
    
  	public function fetch_all_count(){
    	return DB::result_first('select count(*) from %t',array($this->_table));
    }
 }
?>
