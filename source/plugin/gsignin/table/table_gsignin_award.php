<?php
if(!defined('IN_DISCUZ')){
	exit('Access Denied');
}

class table_gsignin_award extends discuz_table
{
	public function __construct(){
		$this->_table	= 'gsignin_award';
		$this->_pk		= 'id';
		parent::__construct();
	}
	
	public function fetch_all_front(){
    	return DB::fetch_all('select * from %t order by continuous desc',array($this->_table));
    }
    
	public function fetch_all_cp(){
    	return DB::fetch_all('select * from %t order by continuous asc',array($this->_table));
    }
    
	public function fetch_first_by_id($id){
    	return DB::fetch_first('select * from %t where id=%d',array($this->_table,$id));
    }
    
	public function fetch_first_by_continuous($continuous){
    	return DB::fetch_first('select * from %t where continuous<=%d order by continuous desc',array($this->_table,$continuous));
    }

    public function fetch_limit($offset,$limit){
    	return DB::fetch_all('select * from %t limit %d,%d',array($this->_table,$offset,$limit));
    }
    
  	public function fetch_count(){
    	return DB::result_first('select count(*) from %t',array($this->_table));
    }
    
}
?>