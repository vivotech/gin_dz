<?php
if(!defined('IN_DISCUZ')){
	exit('Access Denied');
}

class table_gsignin_config extends discuz_table {
	
	var $type_table;
	
	public function __construct(){
		$this->_table	= 'gsignin_config';
		$this->_pk		= 'id';
		$this->type_table = 'gsignin_configtype';
		parent::__construct();
	}

	/**
	 * 获得所有数据
	 * 
	 * @return array
	 */
	public function fetch_all_my(){
    	return DB::fetch_all('select * from %t  order by displayorder',array($this->_table));
    }
    
	/**
	 * 根据ID获得单条记录
	 * 
	 * @param int $id
	 * @return 
	 */
	public function fetch_first_by_id($id){
    	return DB::fetch_first('select * from %t where id=%d',array($this->_table,$id));
    }
	
    public function fetch_all_limit_by_where($offset=0,$limit=65536,$where=''){
    	$_fields = 'config.id,config.name,config.desc,variable,value,regex,prompt,module,isdisplay,config.displayorder';
    	$type_fields = 'type.name typename,identifier,content';
    	$fields = $_fields.','.$type_fields;
    	$sql = 'select '.$fields.' from %t config,%t type where config.typeid = type.id %i order by displayorder limit %d,%d';   	    	
    	return DB::fetch_all($sql,array($this->_table,$this->type_table,$where,$offset,$limit));
    }
    
  	public function fetch_all_count_by_where($where=''){
  		$sql = 'select count(*) from %t config,%t type where config.typeid = type.id %i';  
    	return DB::result_first($sql,array($this->_table,$this->type_table,$where));
    }
    
}
?>