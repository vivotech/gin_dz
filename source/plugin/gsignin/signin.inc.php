<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

//TODO 设置用在脚本中所有日期/时间函数的默认时区，其中PRC为“中华人民共和国”
date_default_timezone_set('PRC'); 

$today = strtotime(date('Y-m-d',TIMESTAMP));

require_once DISCUZ_ROOT."/source/plugin/gsignin/function/function.php";

$set = $_G['cache']['plugin']['gsignin'];
$navigation[] = lang('plugin/gsignin','s00011');

//获取插件名称和版本号
$plugin_info = DB::fetch_first('select * from %t where identifier=%s', array('common_plugin','gsignin'));

// 判断用户是否登录
if(empty($_G['uid'])){
	showmessage(lang('plugin/gshop','Please login first!'), '', array(), array('login' => true));
	exit;
}
// 判断用户是否有权限
$adminUids=explode('/',$set['adminUids']);
if(!in_array($_G['uid'],$adminUids)){
	showmessage(lang('plugin/gsignin','s00006'));
}

if($_GET['action'] == "delete") {
	
	if ($_GET['formhash']==formhash()){
		// 获取要删除的签到记录
		$signin = C::t("#gsignin#gsignin_signin")->fetch_first_by_id($_GET['signinid']);
		
		$member = C::t('#gsignin#gsignin_member')->fetch_first_by_uid($signin['uid']);
		
		$prevSignin = C::t("#gsignin#gsignin_signin")->fetch_prev_by_uid_($signin['uid'],$signin['id']);
		$nextSignin = C::t("#gsignin#gsignin_signin")->fetch_next_by_uid_($signin['uid'],$signin['id']);
		// 判断删除的是否为最后一次签到
		if(!empty($nextSignin)) {
			$nextSignin['prevtime'] = $prevSignin['currenttime'];
			C::t("#gsignin#gsignin_signin")->update($nextSignin['id'],$nextSignin);
			
			// 刷新用户连签数据
			$breakSignin = C::t('#gsignin#gsignin_signin')->fetch_last_break_by_uid($signin['uid']);
			$prevContinuous = C::t('#gsignin#gsignin_signin')->fetch_continuous_by_uid($signin['uid'],$breakSignin['currenttime']);
			
			// 连续签到数据
			$member['continuous'] = $prevContinuous;
			// 累计签到数据
			$member['total'] = $member['total']-1;
			// 更新用户数据
			C::t('#gsignin#gsignin_member')->update($member['uid'],$member);
		} else {
			if(empty($prevSignin)) {
				$prevSigninTime = 0;
			} else {
				$prevSigninTime = $prevSignin['currenttime'];
			}
			$member['continuous'] = $member['continuous']-1;
			$member['total'] = $member['total']-1;
			$member['lasttime'] = $prevSigninTime;
			// 更新用户数据
			C::t('#gsignin#gsignin_member')->update($member['uid'],$member);
			
		}
		
		C::t("#gsignin#gsignin_signin")->delete($_GET['signinid']);	
		showRight(lang('plugin/gsignin','s00012'),"plugin.php?id=gsignin:signin&page=".$_GET['page']);
	}
}

else if($_GET['action'] == "search") {
	
	$where = ' where 1=1';
	$url = 'plugin.php?id=gsignin:signin&action=search';
	
	if(!empty($_GET['uid']) && $_GET['uid'] != -1) {
		$where = $where.' and uid='.$_GET['uid'];
		$url = $url.'&uid='.$_GET['uid'];
	}
	
	if(!empty($_GET['begindate']) && $_GET['begindate'] != '') {
		$where = $where.' and currenttime>='.strtotime($_GET['begindate']);
		$url = $url.'&begindate='.$_GET['begindate'];
	}
	
	if(!empty($_GET['enddate']) && $_GET['enddate'] != '') {
		$where = $where.' and currenttime<='.strtotime($_GET['enddate']."23:59:59");
		$url = $url.'&enddate='.$_GET['enddate'];
	}
	
	$currentpage=$_GET['page']?$_GET['page']:1;
	$pageSize=20;
	$num = C::t("#gsignin#gsignin_signin")->fetch_count_by_where($where);;
	$signins = C::t("#gsignin#gsignin_signin")->fetch_limit_by_where($where,($currentpage-1)*$pageSize,$pageSize);
	$paging = helper_page :: multi($num, $pageSize, $currentpage, $url, 0, 11, false, false);
	$totalPage = ceil($num/$pageSize);
	include template('gsignin:cp/signin_list');
}

else if($_GET['action'] == "add_input") {
	include template('gsignin:signin/add');
}

else if($_GET['action'] == "add") {
	
	if($_GET['do']=="ajax"){
		$user=getuserbyuid($_GET['uid']);
		include template('gsignin:signin/getuser');
		exit;
	}
	
	if ($_GET['formhash']==formhash()){
		$uid = $_GET['uid'];
		$member = getuserbyuid($uid);
		// 判断用户是否存在
		if(empty($member)){
			showError(lang('plugin/gsignin','s00041').$uid.lang('plugin/gsignin','s00044'));
			exit;
		}
		$member = C::t('#gsignin#gsignin_member')->fetch_first_by_uid($uid);
		
		if(empty($member)){
			$member = array(
				'uid'=>$_G['uid'],
				'continuous'=>0,
				'total'=>0,
				'difference'=>0,
				'lasttime'=>0,
				'isshield'=>0
			);
			C::t('#gsignin#gsignin_member')->insert($member);
		}
		
		// 判断所添加签到的日期，是否已经签到过
		$currenttime = $_GET['currenttime'];
		if(empty($currenttime)){
			showError(lang('plugin/gsignin','s00051'));
			exit;
		}
		$begintime = strtotime($currenttime);
		$endtime = strtotime($currenttime." 23:59:59");
		
		if($begintime >= $today){
			showError(lang('plugin/gsignin','s00039').date('Y-m-d',$today).lang('plugin/gsignin','s00040'));
			exit;
		}
		
		
		$where = 'where currenttime>='.$begintime.' and currenttime<='.$endtime.' and uid='.$uid;
		$count = C::t("#gsignin#gsignin_signin")->fetch_count_by_where($where);
		
		if(!empty($count)) {
			showError(lang('plugin/gsignin','s00041').$uid.lang('plugin/gsignin','s00042').$currenttime.lang('plugin/gsignin','s00043'));
			exit;
		}
		
		$prevSignin = C::t("#gsignin#gsignin_signin")->fetch_prev_by_uid($uid,strtotime($currenttime));
		$nextSignin = C::t("#gsignin#gsignin_signin")->fetch_next_by_uid($uid,strtotime($currenttime));
		
		// 判断后一次签到是否为空
		if(empty($nextSignin)) {
			// 判断前一次签到是否为空
			if(empty($prevSignin)){
				$currentSignin = array(
					'uid'=>$uid,
					'prevtime'=>0,
					'currenttime'=>$endtime,
					'ranking'=>0,
					'extcredits1'=>0,
					'extcredits2'=>0,
					'extcredits3'=>0,
					'extcredits4'=>0,
					'extcredits5'=>0,
					'extcredits6'=>0,
					'extcredits7'=>0,
					'extcredits8'=>0,
					'issupplement'=>1
				);
				// 插入补签记录
				C::t('#gsignin#gsignin_signin')->insert($currentSignin);				
			}else{
				$currentSignin = array(
					'uid'=>$uid,
					'prevtime'=>$prevSignin['currenttime'],
					'currenttime'=>$endtime,
					'ranking'=>0,
					'extcredits1'=>0,
					'extcredits2'=>0,
					'extcredits3'=>0,
					'extcredits4'=>0,
					'extcredits5'=>0,
					'extcredits6'=>0,
					'extcredits7'=>0,
					'extcredits8'=>0,
					'issupplement'=>1
				);
				// 插入补签记录
				C::t('#gsignin#gsignin_signin')->insert($currentSignin);
			}
		} else{
			// 判断前一次签到是否为空
			if(empty($prevSignin)){
				$nextSignin['prevtime'] = $endtime;
				// 更新后一次签到记录
				C::t("#gsignin#gsignin_signin")->update($nextSignin['id'],$nextSignin);
				$currentSignin = array(
					'uid'=>$uid,
					'prevtime'=>0,
					'currenttime'=>$endtime,
					'ranking'=>0,
					'extcredits1'=>0,
					'extcredits2'=>0,
					'extcredits3'=>0,
					'extcredits4'=>0,
					'extcredits5'=>0,
					'extcredits6'=>0,
					'extcredits7'=>0,
					'extcredits8'=>0,
					'issupplement'=>1
				);
				// 插入补签记录
				C::t('#gsignin#gsignin_signin')->insert($currentSignin);
				
			}else{
				$nextSignin['prevtime'] = $endtime;
				// 更新后一次签到记录
				C::t("#gsignin#gsignin_signin")->update($nextSignin['id'],$nextSignin);
				$currentSignin = array(
					'uid'=>$uid,
					'prevtime'=>$prevSignin['currenttime'],
					'currenttime'=>$endtime,
					'ranking'=>0,
					'extcredits1'=>0,
					'extcredits2'=>0,
					'extcredits3'=>0,
					'extcredits4'=>0,
					'extcredits5'=>0,
					'extcredits6'=>0,
					'extcredits7'=>0,
					'extcredits8'=>0,
					'issupplement'=>1
				);
				// 插入补签记录
				C::t('#gsignin#gsignin_signin')->insert($currentSignin);
			}			
		}
		
		// 刷新用户连签数据
		$breakSignin = C::t('#gsignin#gsignin_signin')->fetch_last_break_by_uid($uid);
		$prevContinuous = C::t('#gsignin#gsignin_signin')->fetch_continuous_by_uid($uid,$breakSignin['currenttime']);
		
		// 连续签到数据
		$member['continuous'] = $prevContinuous;
		// 累计签到数据
		$member['total'] = C::t("#gsignin#gsignin_signin")->fetch_count_by_where('where uid='.$uid);;
		// 更新用户数据
		C::t('#gsignin#gsignin_member')->update($member['uid'],$member);
		
		showRight(lang('plugin/gsignin','s00045'),"plugin.php?id=gsignin:signin&page=".$_GET['page']);
	}
}

else if($_GET['action'] == "extend") {

}
else {
	$currpage=$_GET['page']?$_GET['page']:1;
	$perpage=20;
	$num = C::t("#gsignin#gsignin_signin")->fetch_count();
	$signins = C::t("#gsignin#gsignin_signin")->fetch_limit(($currpage-1)*$perpage,$perpage);
	$url = 'plugin.php?id=gsignin:signin';
	$paging = helper_page :: multi($num, $perpage, $currpage, $url, 0, 11, false, false);
	$totalPage = ceil($num/$perpage);

	include template('gsignin:cp/signin_list');
}