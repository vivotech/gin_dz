<?php
if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}


require_once DISCUZ_ROOT."/source/plugin/gsignin/function/function.php";

//获取插件名称和版本号
$plugin_info = DB::fetch_first('select * from %t where identifier=%s', array('common_plugin','gsignin'));

$set = $_G['cache']['plugin']['gsignin'];
$navigation[] = lang('plugin/gsignin','s00026');

// 判断用户是否登录
if(empty($_G['uid'])){
	showmessage(lang('plugin/gshop','Please login first!'), '', array(), array('login' => true));
	exit;
}
// 判断用户是否有权限
$adminUids=explode('/',$set['adminUids']);
if(!in_array($_G['uid'],$adminUids)){
	showmessage(lang('plugin/gsignin','s00006'));
}
// 初始化插件唯一标识符
$pluginIdentifier = 'gsignin';
// 初始化表名
$tableName = '#'.$pluginIdentifier.'#'.$pluginIdentifier.'_config';
// 初始化模块名
$mymodule = $_GET['mod'];

if($_GET['action'] == "update") {
	// 验证formhash	
	if ($_GET['formhash']==formhash()) {
		$variables = $_GET['variables'];
		// 循环接收配置数据
		foreach($variables as $key => $val) {
			$config = getConfig($tableName,$_GET['cmod'],$val);
			// 判断配置项是否存在
			if(!empty($config)) {
				if($config['identifier'] == 'file'){
					$value = array(
						'value'=>uploadFile($_FILES[$val]['name'],$_FILES[$val]['tmp_name'])
					);
				} else {
					$value = array(
						'value'=>$_GET[$val]
					);
				}
		//debug($config);
				// 插入数据
				C::t($tableName)->update($config['id'],$value);
			}
		}
		// 弹出修改成功提示框
		echo "<script>";
		echo "parent.window.showright('修改成功！')";
		echo "</script>";
	}	
// 留作扩展
}else if($_GET['action'] == "extend") {

}else{
	$configs = getConfigs($tableName,$_GET['cmod']);
//debug(''.$pluginIdentifier.':config/'.$mymodule.'');
	include template($pluginIdentifier.':cp/'.$mymodule);

}