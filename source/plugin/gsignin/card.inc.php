<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

require_once DISCUZ_ROOT."/source/plugin/gsignin/function/function.php";

$set = $_G['cache']['plugin']['gsignin'];
$navigation[] = lang('plugin/gsignin','s00026');

// 判断用户是否登录
if(empty($_G['uid'])){
	showmessage(lang('plugin/gshop','Please login first!'), '', array(), array('login' => true));
	exit;
}

//获取插件名称和版本号
$plugin_info = DB::fetch_first('select * from %t where identifier=%s', array('common_plugin','gsignin'));
// 判断用户是否有权限
$adminUids=explode('/',$set['adminUids']);
if(!in_array($_G['uid'],$adminUids)){
	showmessage(lang('plugin/gsignin','s00006'));
}
$setInfo=C::t('#gsignin#gsignin_card')->fetch_card_setInfo();
if($_GET['action']=='set'){
	if($_GET['formhash']==formhash()){
		$tt='/^\\d+$/';
		$tt1='/^([0-9]*|-[1])$/';
		preg_match($tt,$_GET['price'],$result);    
		preg_match($tt1,$_GET['inventory'],$resultA);
		if(empty($result)){
			showError(lang('plugin/gsignin','s00027'));
			exit;
		}
		if($_GET['extcredits']=='0'){
			showError(lang('plugin/gsignin','s00029'));
			exit;
		}
		if(empty($resultA)){
			showError(lang('plugin/gsignin','s00028'));
			exit;
		}
		
		if($_FILES['picture']['tmp_name']) {
			$picname = $_FILES['picture']['name'];
			if ($picname != "") {
				if($setInfo['picture']!=''){
					unlink($setInfo['picture']);
				}
				$type = strstr($picname, '.');
				if ($type != ".gif" && $type != ".jpg"&& $type != ".png"&& $type != ".jpeg") {
					showerror(lang('plugin/gsignin','s00030'));
					exit;
				}
				$pics = date("YmdHis"). $type;
				$img_dir="source/plugin/gsignin/attachment/";
				if(!is_dir($img_dir)){
					mkdir($img_dir);
				}
				$picture = "source/plugin/gsignin/attachment/". $pics;
				$result=move_uploaded_file($_FILES['picture']['tmp_name'],$picture); 
			}
		}else{
			$picture=$setInfo['picture'];
		}
		
		$cardset=array(
			'picture'=>$picture,
			'price'=>$_GET['price'],
			'extcredits'=>$_GET['extcredits'],
			'salevolume'=>$_GET['salevolume'],
			'inventory'=>$_GET['inventory'],
		);
		if(C::t('#gsignin#gsignin_card')->count_card_setInfo()!=0){
			C::t('#gsignin#gsignin_card')->update($setInfo['id'],$cardset);
		}else{
			C::t('#gsignin#gsignin_card')->insert($cardset);
		}
		showRight(lang('plugin/gsignin','s00031'),'plugin.php?id=gsignin:card');
		exit;
	}
}else{
	$setInfo=C::t('#gsignin#gsignin_card')->fetch_card_setInfo();
	include template('gsignin:cp/cardset');
}
?>