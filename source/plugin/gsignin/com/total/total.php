<?php
	
	if(!empty($_G['uid'])) {
		
		$member = C::t('#gsignin#gsignin_member')->fetch_first_by_uid($_G['uid']);
		if(empty($member)){
			$member = array(
				'uid'=>$_G['uid'],
				'continuous'=>0,
				'total'=>0,
				'difference'=>0,
				'lasttime'=>0,
				'isshield'=>0
			);
			C::t('#gsignin#gsignin_member')->insert($member);
		}
		// 获取本月第一天日期的时间戳
		$currentMouthFirstDay = strtotime(date('Y-m-01',TIMESTAMP));
		// 获取本月最后一天日期的时间戳
		$currentMouthLastDay = strtotime(date('Y-m-d',$currentMouthFirstDay)."+1 month -1 day");
		// 获取今天的天数
		$currentMouthTotalDay = date("d",$currentMouthLastDay);
		
		$nextMouthFirstDay = strtotime(date('Y-m-d',$currentMouthFirstDay)."+1 month");
		// 获得本月当前用户签到记录
		$monthSignins =  C::t('#gsignin#gsignin_signin')->fetch_all_by_uid_month($_G['uid'],$currentMouthFirstDay,$nextMouthFirstDay);
		
		$forget = date("d",$today) - count($monthSignins);
		if($member['lasttime']>$today) {
			$ranking = C::t('#gsignin#gsignin_signin')->fetch_ranking($member['lasttime'],$today);
			$ranking = $ranking+1;
		}
	}

	$memberTops = C::t('#gsignin#gsignin_member')->fetch_all_top_by_total();
	$currpage=$_GET['page']?$_GET['page']:1;
	
	if($perpage>0){
		//$offset=($currpage-1)*$perpage+3;
		$offset=($currpage-1)*$perpage;
		//查询所有的签到用户数量
		$num = C::t('#gsignin#gsignin_member')->fetch_count_by_total();
		$bottomnum=$num-count($memberTops);
		$memberBottoms = C::t('#gsignin#gsignin_member')->fetch_all_bottom_by_total($offset,$perpage);
		$url='plugin.php?id=gsignin:index&action=total';
		$paging=helper_page::multi($bottomnum,$perpage,$currpage,$url,0,11,false,false);
		if($bottomnum>0){
			$isPage=ceil($bottomnum/$perpage);
		}else{
			$isPage=1;
		}
	}else{
		//$offset=3;
		$offset = 0;
		//查询所有的签到用户数量
		$num = C::t('#gsignin#gsignin_member')->fetch_count_by_total();
		$bottomnum=$num-count($memberTops);
		$memberBottoms = C::t('#gsignin#gsignin_member')->fetch_all_bottom_by_total($offset,$num);
		$isPage=1;
	}
	
	// 控制用户名颜色变化，这里的处理方法也不是灰常好
	
	$awards = C::t("#gsignin#gsignin_award")->fetch_all_front();
	
	for($i=0;$i<count($memberTops);$i++) {
		for($j=0;$j<count($awards);$j++) {
			if($memberTops[$i]['continuous']>=$awards[$j]['continuous']) {
				$memberTops[$i]['color'] = $awards[$j]['color'];
				if($awards[$j]['weight'] > 0){
					$memberTops[$i]['weight'] = "bold";
				}
				break;
			}
		}	
	}
	
	for($i=0;$i<count($memberBottoms);$i++) {
		for($j=0;$j<count($awards);$j++) {
			if($memberBottoms[$i]['continuous']>=$awards[$j]['continuous']) {
				$memberBottoms[$i]['color'] = $awards[$j]['color'];
				if($awards[$j]['weight'] > 0){
					$memberBottoms[$i]['weight'] = "bold";
				}
				break;
			}
		}		
	}
	