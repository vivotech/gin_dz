<?php
if(!defined('IN_DISCUZ') || !defined('IN_ADMINCP')) {
	exit('Access Denied');
}

$pluginName = 'gsignin';
$componentTable = '#gsignin#gsignin_component';

$names[] = lang('plugin/gsignin','s00014');
$models[] = 'continuous';

$names[] = lang('plugin/gsignin','s00015');
$models[] = 'total';

$names[] = lang('plugin/gsignin','s00016');
$models[] = 'change';

$names[] = lang('plugin/gsignin','s00017');
$models[] = 'calendar';

// TODO 添加补签卡判断
$names[] = lang('plugin/gsignin','s00046');
$models[] = 'card';

// TODO 添加版权管理组件
$names[] = lang('plugin/gsignin','s00052');
$models[] = 'copyright';

// TODO 奖励扩展管理组件
$names[] = lang('plugin/gsignin','s00064');
$models[] = 'extend';

// TODO 自动签到理组件
$names[] = lang('plugin/gsignin','s00065');
$models[] = 'auto';


for($i=0;$i<count($names);$i++){
	if (file_exists('source/plugin/'.$pluginName.'/com/'.$models[$i].'/'.$models[$i].'.php')){
		$component= C::t($componentTable)->fetch_first_by_model($models[$i]);
		if (empty($component)){
			$component = array(
				'name'=>$names[$i],
				'model'=>$models[$i],
				'flag'=>'0',
				'createtime'=>TIMESTAMP
			);
			C::t($componentTable)->insert($component);
		}
	}
}

