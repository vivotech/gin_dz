<?php
/*
	Install Uninstall Upgrade AutoStat System Code
*/
if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}
//start to put your own code 
$sql = <<<EOF
DROP TABLE IF EXISTS `pre_gsignin_award`;
DROP TABLE IF EXISTS `pre_gsignin_member`;
DROP TABLE IF EXISTS `pre_gsignin_signin`;
DROP TABLE IF EXISTS `pre_gsignin_component`;
DROP TABLE IF EXISTS `pre_gsignin_card`;
DROP TABLE IF EXISTS `pre_gsignin_configtype`;
DROP TABLE IF EXISTS `pre_gsignin_config`;
DROP TABLE IF EXISTS `pre_gsignin_post`;
DROP TABLE IF EXISTS `pre_gsignin_extend`;
EOF;

runquery($sql);
//finish to put your own code
$finish = TRUE;
?>