<?php
if(!defined('IN_DISCUZ')) {
	exit("Access Denied");
}

//TODO 设置用在脚本中所有日期/时间函数的默认时区，其中PRC为“中华人民共和国”
date_default_timezone_set('PRC'); 

$set = $_G['cache']['plugin']['gsignin'];

// 判断用户是否登录
if(empty($_G['uid'])){
	showmessage(lang('plugin/gshop','Please login first!'), '', array(), array('login' => true));
	exit;
}
// 判断用户是否有权限
$adminUids=explode('/',$set['adminUids']);
if(!in_array($_G['uid'],$adminUids)){
	showmessage(lang('plugin/gsignin','s00006'));
}

//获取插件名称和版本号
$plugin_info = DB::fetch_first('select * from %t where identifier=%s', array('common_plugin','gsignin'));

include template('gsignin:cp/index');

?>