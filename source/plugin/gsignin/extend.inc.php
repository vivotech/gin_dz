<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

require_once DISCUZ_ROOT."/source/plugin/gsignin/function/function.php";

$set = $_G['cache']['plugin']['gsignin'];
$navigation[] = lang('plugin/gsignin','s00062');

// 判断用户是否登录
if(empty($_G['uid'])){
	showmessage(lang('plugin/gshop','Please login first!'), '', array(), array('login' => true));
	exit;
}

//获取插件名称和版本号
$plugin_info = DB::fetch_first('select * from %t where identifier=%s', array('common_plugin','gsignin'));
// 判断用户是否有权限
$adminUids=explode('/',$set['adminUids']);
if(!in_array($_G['uid'],$adminUids)){
	showmessage(lang('plugin/gsignin','s00006'));
}

if($_GET['action'] == "extend") {

	if ($_GET['formhash']==formhash()){

		for($i=0; $i<count($_GET['extcredits1']); $i++) {
			$extend = array(
				'id'=>$i+1,
				'extcredits1'=>$_GET['extcredits1'][$i],
				'extcredits2'=>$_GET['extcredits2'][$i],
				'extcredits3'=>$_GET['extcredits3'][$i],
				'extcredits4'=>$_GET['extcredits4'][$i],
				'extcredits5'=>$_GET['extcredits5'][$i],
				'extcredits6'=>$_GET['extcredits6'][$i],
				'extcredits7'=>$_GET['extcredits7'][$i],
				'extcredits8'=>$_GET['extcredits8'][$i],
			);
			C::t('#gsignin#gsignin_extend')->update($extend['id'],$extend);
		}
		showRight(lang('plugin/gsignin','s00063'),"plugin.php?id=gsignin:extend");
	}
}
else {
	$extend1 = C::t('#gsignin#gsignin_extend')->fetch_first_by_id(1);
	if(empty($extend1)) {
		$extend1 = array(
			'extcredits1'=>0,
			'extcredits2'=>0,
			'extcredits3'=>0,
			'extcredits4'=>0,
			'extcredits5'=>0,
			'extcredits6'=>0,
			'extcredits7'=>0,
			'extcredits8'=>0,
		);
		C::t('#gsignin#gsignin_extend')->insert($extend1);
	}
	$extend2 = C::t('#gsignin#gsignin_extend')->fetch_first_by_id(2);
	if(empty($extend2)) {
		$extend2 = array(
			'extcredits1'=>0,
			'extcredits2'=>0,
			'extcredits3'=>0,
			'extcredits4'=>0,
			'extcredits5'=>0,
			'extcredits6'=>0,
			'extcredits7'=>0,
			'extcredits8'=>0,
		);
		C::t('#gsignin#gsignin_extend')->insert($extend2);
	}
	$extend3 = C::t('#gsignin#gsignin_extend')->fetch_first_by_id(3);
	if(empty($extend3)) {
		$extend3 = array(
			'extcredits1'=>0,
			'extcredits2'=>0,
			'extcredits3'=>0,
			'extcredits4'=>0,
			'extcredits5'=>0,
			'extcredits6'=>0,
			'extcredits7'=>0,
			'extcredits8'=>0,
		);
		C::t('#gsignin#gsignin_extend')->insert($extend3);
	}
	
	include template('gsignin:cp/extend');
}