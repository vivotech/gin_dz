<?php

if(!defined('IN_DISCUZ') || !defined('IN_ADMINCP')) {
	exit('Access Denied');
}

$pluginName = 'gsignin';
$componentTable = '#gsignin#gsignin_component';

require_once DISCUZ_ROOT.'./source/plugin/'.$pluginName.'/com/component.php';


if($_GET['do'] == 'install'){//安装
	if($_GET['formhash']==formhash()){
		$component=C::t($componentTable)->fetch_first_by_id($_GET['cid']);
		C::t($componentTable)->update($component['id'],array('flag'=>1));
		cpmsg(lang('plugin/gsignin','s00008'), 'action=plugins&identifier='.$pluginName.'&pmod=component&page='.$_GET['page'], 'succeed');
	}
}
else if($_GET['do'] == 'uninstall'){//卸载
	if($_GET['formhash']==formhash()){
		$component=C::t($componentTable)->fetch_first_by_id($_GET['cid']);
		C::t($componentTable)->update($component['id'],array('flag'=>0));
		cpmsg(lang('plugin/gsignin','s00009'), 'action=plugins&identifier='.$pluginName.'&pmod=component&page='.$_GET['page'], 'succeed');
	}
}

else {
	$currpage=$_GET['page']?$_GET['page']:1;
	$perpage=30;
	$offset=($currpage-1)*$perpage;
	$num=C::t($componentTable)->fetch_all_count();
	$components=C::t($componentTable)->fetch_all_limit($offset,$perpage);
	$paging=helper_page::multi($num,$perpage,$currpage,'admin.php?action=plugins&identifier='.$pluginName.'&pmod=component',0,8,false,false);
	include template($pluginName.':component');
}

?>

















