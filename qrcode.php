<?php
/*
 * 二维码
 */

if(function_exists('date_default_timezone_set')) {
	@date_default_timezone_set('PRC');
}

define('ROOT_PATH' , strtr(dirname(__FILE__) , '\\','/').'/');

//print_r($_SERVER);
$lastmodtime = !empty($_SERVER["HTTP_IF_MODIFIED_SINCE"]) ? strtotime($_SERVER["HTTP_IF_MODIFIED_SINCE"]) : '';//客户端发送的头信息
$filemtime = time();

if ($lastmodtime < $filemtime)
{
	$maxage = 3600;
	
	$expire = strftime("%a, %d %b %G %H:%M:%S GMT+8",$filemtime+$maxage);
	header("Expires: ".$expire);
	header("Cache-Control:max-age=$maxage");
	$lastmodify = strftime("%a, %d %b %G %H:%M:%S GMT+8", $filemtime);
	header('Last-Modified: ' .$lastmodify);
	header("Content-type: image/png");
	
	//set it to writable location, a place for temp generated PNG files
	$PNG_TEMP_DIR = ROOT_PATH.'data'.DIRECTORY_SEPARATOR.'qrcode'.DIRECTORY_SEPARATOR;
	$PNG_WEB_DIR = 'data/qrcode/';
	
	include ROOT_PATH . "source".DIRECTORY_SEPARATOR."class".DIRECTORY_SEPARATOR."class_qrtools.php";
	
	if (!file_exists($PNG_TEMP_DIR))
		mkdir($PNG_TEMP_DIR);
	$errorCorrectionLevel = 'H';
	$matrixPointSize = 10;
	$margin = 2;
	if(!empty($_REQUEST['data'])){
		
		if (isset($_REQUEST['level']) && in_array($_REQUEST['level'], array('L','M','Q','H')))
		$errorCorrectionLevel = $_REQUEST['level'];
		
		
		if (isset($_REQUEST['size']))$matrixPointSize = min(max((int)$_REQUEST['size'], 1), 20);
		
		if(isset($_REQUEST['margin']))$margin = min(max((int)$_REQUEST['size'], 2), 10);
		
		//$margin = empty($_REQUEST['margin']) ? 4 : intval($_REQUEST['margin']);
		
		$_REQUEST['data'] = trim(urldecode($_REQUEST['data']));
		
		$filename = $PNG_TEMP_DIR.'test'.md5($_REQUEST['data'].'|'.$errorCorrectionLevel.'|'.$matrixPointSize .'|'.$margin).'.png';

	}else{
		$filename = $PNG_TEMP_DIR . 'test.png';
		$_REQUEST['data'] = 'http://www.ginye.net/';
	}
	
	QRcode::png($_REQUEST['data'], $filename, $errorCorrectionLevel, $matrixPointSize, $margin);
	
	//echo ROOT_PATH.$PNG_WEB_DIR.basename($filename);
	echo file_get_contents($filename);
}
else
{
	$lastmodify = strftime("%a, %d %b %G %H:%M:%S GMT+8", $filemtime);
	header('Last-Modified: ' .$filemtime);
	header('HTTP/1.1 304 Not Modified');
}


//QRtools::timeBenchmark();

    