<?php


define('APPTYPEID', 2);
define('CURSCRIPT', 'forum');

require './source/class/class_core.php';

$modarray = array('start','finish','action');

$mod = !in_array(C::app()->var['mod'], $modarray) ? 'start' : C::app()->var['mod'];

C::app()->init();

require DISCUZ_ROOT.'./source/module/game/game_'.$mod.'.php';


?>