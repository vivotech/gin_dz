<?php

define('CURSCRIPT', 'headoffice');

require './source/class/class_core.php';

$discuz = C::app();
$discuz->init();

// mod 请求参数
$modarray = array(
	'index',	// 首页概况
	'signin',	// 登录界面
	'statistics',	// 统计
	'dealer',	// 经销商管理
	'shop'		// 店铺管理
);
$mod = !in_array(C::app()->var['mod'], $modarray) ? 'index' : C::app()->var['mod'];

// 初始化 App
$app = C::app();
$app->init();

if ( $mod !== 'signin' ) {
	if ( empty($_G['uid']) )
	{
		header('location: /headoffice.php?mod=signin');
	}
	
	// 获取经销商记录
	$this_dealer = C::t('dealer_member')->fetch_by_uid($_G['uid']);
	
	// 不是经销商的话
	if ( empty($this_dealer) ){
		showmessage('出错了，正为你转到首页。', 'forum.php');
	}
	
	// 不是总部的话
	else if ( $this_dealer['duid'] != 1 ) {
		showmessage('出错了，正为你转到经销商后台。', 'dealer.php');
	}
	$_G['dealer'] = $this_dealer;
	
}

// 定义常量
define('ONE_DAY_SECONDS', 86400);

// 载入视图
require DISCUZ_ROOT.'./source/module/' .CURSCRIPT. '/'.$mod.'.php';

