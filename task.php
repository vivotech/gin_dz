<?php


define('APPTYPEID', 2);
define('CURSCRIPT', 'forum');

require './source/class/class_core.php';

$mod = empty(C::app()->var['mod']) ? 'play' : C::app()->var['mod'];

C::app()->init();

require_once libfile('function/mission');

$tid = I('tid',0,'intval');
$uid = $_G['uid'];
$task = get_task($tid);
if(!$task){
	ajax_return(0,'任务不存在！');
}


//若为非长期任务
if($task['timetype']!=0){
	if(TIMESTAMP < $task['starttime'] ){
		ajax_return(0,'任务未开始！');
	}
	if(TIMESTAMP > $task['endtime'] ){
		ajax_return(0,'任务已结束');
	}
}

$types = array_keys(get_task_types());
if(!in_array($task['type'], $types)){
	ajax_return(0,'任务类型错误！');
}

load_base_class();

$class_name = 'task_'.$task['type'];
$class_path = DISCUZ_ROOT.'./source/module/task/'.$class_name.'.php';

if(file_exists($class_path)){
	require_once $class_path;
}

if(!class_exists($class_name)){
	ajax_return(0,'任务类型错误！');
}

$taskObject = new $class_name($task);
$on_action = 'on_'.$mod;
if(!method_exists($taskObject, $on_action)){
	ajax_return(0,'操作失败！');
}
$taskObject->$on_action();

?>