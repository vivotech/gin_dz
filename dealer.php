<?php

define('CURSCRIPT', 'dealer');

require './source/class/class_core.php';

$discuz = C::app();
$discuz->init();

// mod 请求参数
$modarray = array(
	'index',	// 首页概况
	'signin',	// 登录界面
	'product',	// 商品管理
	'order',	// 订单管理
	'statistics',	// 统计
	'shop',		// 店铺管理
	'updatecache',// 更新缓存
);
$mod = !in_array(C::app()->var['mod'], $modarray) ? 'index' : C::app()->var['mod'];

// 初始化 App
$app = C::app();
$app->init();

if ( $mod !== 'signin' ) {
	if ( empty($_G['uid']) )
	{
		header('location: /dealer.php?mod=signin');
	}
	
	// 获取经销商记录
	$this_dealer = C::t('dealer_member')->fetch_by_uid($_G['uid']);
	
	// 不是经销商的话
	if ( empty($this_dealer) || $this_dealer['deteled'] ){
		showmessage('您不是经销商，无法进入经销商模块', 'forum.php');
	}
	else if ( $this_dealer['duid'] < 2 ) {
		showmessage('出错了，正在为你转到经销商模块。', 'headoffice.php');
	}
	$_G['dealer'] = $this_dealer;	// type Array
	
	// 获取有效的店铺
	$dealer_shops = C::t('dealer_member_shop')->fetch_all_filters(array(
		'duid'	=> $_G['dealer']['duid'],
		'deleted'	=> 0
	));
	
}

// 定义常量
define('ONE_DAY_SECONDS', 86400);

// 载入视图
require DISCUZ_ROOT.'./source/module/' .CURSCRIPT. '/'.$mod.'.php';

