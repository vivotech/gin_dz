<?php
	
	/**
	 * 短链接生成文件
	 * 百度、新浪短链接
	 * 
	 * 新浪的短连接效率高~~推荐
	 * 
	 */
	
	$types = array('b','s'); //百度、新浪
	//默认为新浪短链接
	$type = isset($_GET['type']) ? ( in_array(trim(strtolower($_GET['type'])),$types) ? trim(strtolower($_GET['type'])) : 's' )  : 's';
	
	if(!isset($_REQUEST['url']) || empty($_REQUEST['url'])){
		ret('URL参数为空');
	}
	
	$url = trim($_REQUEST['url']);
	$uncode_url = urldecode($url);
	
	//生成百度短连接
	if($type == 'b'){
		
		$ch=curl_init();
		curl_setopt($ch,CURLOPT_URL,"http://dwz.cn/create.php");
		curl_setopt($ch,CURLOPT_POST,true);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		$data=array('url'=>$uncode_url);
		curl_setopt($ch,CURLOPT_POSTFIELDS,$data);
		$strRes=curl_exec($ch);
		curl_close($ch);
		$arrResponse=json_decode($strRes,true);
		if($arrResponse['status']==0 && !empty($arrResponse['err_msg']) && empty($arrResponse['tinyurl']))
		{
			//错误处理
			ret($arrResponse['err_msg']);
		}
		//返回成功
		ret($arrResponse['tinyurl'],1);
	
	}
	//新浪短连接
	elseif($type == 's'){
		
		//未链接上 “意大利原生活” 的应用 , 此处用 我的
		$apiKey='2453450409';
    	$apiUrl='http://api.t.sina.com.cn/short_url/shorten.json?source='.$apiKey.'&url_long='.$url;
    	$response = file_get_contents($apiUrl);
    	$json = json_decode($response);
		ret($json[0]->url_short,1);
		
	}
	
	
	function ret($msg='生成失败',$res=0){
		$result = array('result'=>$res,'msg'=>$msg);
		echo json_encode($result,JSON_UNESCAPED_UNICODE);
		exit();
	}
	
?>