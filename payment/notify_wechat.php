<?php

/**
 * 微支付回调
 */

require '../source/class/class_minicore.php';

require_once libfile('function/payment');
require_once libfile('function/wechat');

$notice_debug = false;
require_once libfile('wechat/pay','class');

//使用通用通知接口
$notify = new Notify_pub();

$_G['wechatpay'] = getPaymentConfigByName('wechat');

$_log = new wechat_log($notice_debug);
$_log->wxlog("【页面调用成功 start】\n");

//存储微信的回调
$xml = $GLOBALS['HTTP_RAW_POST_DATA']; //此方式php5.6被废弃
//$xml = file_get_contents('php://input');

$notify->saveData($xml);
$sign = $notify->checkSign();
//验证签名，并回应微信。
//对后台通知交互时，如果微信收到商户的应答不是成功或超时，微信认为通知失败，
//微信会通过一定的策略（如30分钟共8次）定期重新发起通知，
//尽可能提高通知的成功率，但微信不保证通知最终能成功。

$_log->wxlog("【支付配置】:\r\n".var_export($_G['wechatpay'],TRUE));

if($sign == FALSE){
    $notify->setReturnParameter("return_code","FAIL");//返回状态码
    $notify->setReturnParameter("return_msg","签名失败");//返回信息
    $_log->wxlog("【签名失败】:\r\n".var_export($notify->data,TRUE));
}else{
    $notify->setReturnParameter("return_code","SUCCESS");//设置返回码
}
$returnXml = $notify->returnXml();
echo $returnXml;

$_log->wxlog("【接收到的notify通知】:\r\n".$xml."\r\n");

//订单处理
if($sign == TRUE){

    if ($notify->data["return_code"] == "FAIL") {
        //此处应该更新一下订单状态，商户自行增删操作
        $_log->wxlog("【通信出错】:\r\n".$xml."\r\n");
    }
    elseif($notify->data["result_code"] == "FAIL"){
        //此处应该更新一下订单状态，商户自行增删操作
        $_log->wxlog("【业务出错】:\r\n".$xml."\r\n");
    }
    else{
        //return_code == 'SUCCESS' && result_code == 'SUCCESS'
        $_log->wxlog("【支付成功】:\r\n".$xml."\r\n");

        $return_data = $notify->data;
        $attach = json_decode($return_data['attach'],TRUE);

        $_log->wxlog("【附件信息】:\r\n".var_export($attach,true)."\r\n");

        $order = M('common_member_order')->find($attach['oid']);

        //当为微信支付 且 为未支付
        $_log->wxlog("【订单信息】:\r\n".var_export($order,true)."\r\n");

        if($order['payment']==2 && $order['paystate']==0){

            $_log->wxlog("【返回信息】:\r\n".var_export($return_data,true)."\r\n");

            setWXPayState($attach['oid'],$attach['weid'],$return_data);
        }
    }

}

$_log->wxlog("【页面调用结束 end】\r\n");

?>