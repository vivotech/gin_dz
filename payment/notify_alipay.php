<?php

/**
 * 支付宝回调
 */

require '../source/class/class_minicore.php';

require libfile('function/payment');

//计算得出通知验证结果
$alipayNotify = new alipay_notify();
$verify_result = $alipayNotify->verifyNotify();

if($verify_result) {//验证成功

    //商户订单号
    $out_trade_no = $_POST['out_trade_no'];

    //交易状态
    $trade_status = $_POST['trade_status'];

    $alipayconfig = $alipayNotify->getConfig();

    $order = getOrderByOutTradeNo($out_trade_no);
    if(!$order || $order['payment'] != $alipayconfig['pid']){
        echo "fail";
        exit();
    }

    if($_POST['trade_status'] == 'TRADE_FINISHED') {
        //退款日期超过可退款期限后（如三个月可退款），支付宝系统发送该交易状态通知
        M('payment_alipay_state')->where("oid='{$order['oid']}'")->save(array('trade_status'=>'TRADE_FINISHED'));
    }
    else if ($_POST['trade_status'] == 'TRADE_SUCCESS') {
        //付款完成后，支付宝系统发送该交易状态通知
        setAlipayState($order['oid'],$_POST);
    }


    echo "success";		//请不要修改或删除

}
else {
    //验证失败
    echo "fail";

    //调试用，写文本函数记录程序运行情况是否正常
    //logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
}