#format
##Date:20150420
##Author:Jerry
##SQL Chinese Description:like APP推送记录表
#DROP TABLE IF EXISTS `vivo_home_notification_applog`;
#CREATE TABLE IF NOT EXISTS `vivo_home_notification_applog` (
#  `id` int(11) NOT NULL AUTO_INCREMENT,
#  `uid` int(11) NOT NULL,
#  `action` char(20) NOT NULL COMMENT '动作',
#  `msg` varchar(255) NOT NULL COMMENT '发送的消息',
#  `time` char(12) NOT NULL COMMENT '时间戳',
#  PRIMARY KEY (`id`),
#  KEY `uid` (`uid`)
#) ENGINE=MyISAM DEFAULT CHARSET=utf32 AUTO_INCREMENT=1 ;

############################################################################################

##Date:20150420
##Author:goodspb
##SQL Chinese Description : banner图数据库加上type字段，识别各个页面
	ALTER TABLE  `vivo_forum_banner_ad` ADD  `type` CHAR( 15 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT  '' COMMENT  '分类，在不同的页面上显示' AFTER  `baid`
#end


##Date:20150423
##Author:goodspb
##SQL Chinese Description : banner图数据库加上message字段，描述
ALTER TABLE  `vivo_forum_banner_ad` ADD  `message` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT  '' COMMENT  '描述' AFTER  `title`
#end


##Date:20150424
##Author:Jerry
##SQL Chinese Description : 所有文章图片外链直接连七牛
#用户图片库
DROP TABLE IF EXISTS `vivo_common_member_gallery`;
CREATE TABLE `vivo_common_member_gallery`(
  `gid` INT PRIMARY KEY AUTO_INCREMENT,
  `uid` INT NOT NULL DEFAULT 0 COMMENT '对应`user`.`user_id`',
  `name` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '用户图片库名',
  `gup` INT NOT NULL DEFAULT 0 COMMENT '对应自己primary_key',
  `dateline` INT NOT NULL DEFAULT 0 COMMENT '图片库创建日期',
  `isdefault` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '是否是默认图库',
  KEY `uid`(`uid`),
  KEY `gup`(`gup`)
);
#用户图片库图片表
DROP TABLE IF EXISTS `vivo_common_member_gallery_image`;
CREATE TABLE `vivo_common_member_gallery_image`(
  `giid` INT PRIMARY KEY AUTO_INCREMENT,
  `uid` INT NOT NULL DEFAULT 0 COMMENT '对应`user`.`user_id`',
  `gid` INT NOT NULL DEFAULT 0 COMMENT '对应`user_gallery`.`user_gallery_id`',
  `url` VARCHAR(1024) NOT NULL DEFAULT '' COMMENT '图片URL',
  `desc` VARCHAR(512) NOT NULL DEFAULT '' COMMENT '图片描述',
  `dateline` INT NOT NULL DEFAULT 0 COMMENT '上传日期',
  KEY `uid`(`uid`),
  KEY `gid`(`gid`)
);
#end

##Date:20150424
##Author:Jerry
##SQL Chinese Description : 购物系统增加购物车
#购物车
DROP TABLE IF EXISTS `vivo_common_member_shopcart`;
CREATE TABLE `vivo_common_member_shopcart`(
  `uid` INT NOT NULL DEFAULT 0 COMMENT '用户id',
  `spid` INT NOT NULL DEFAULT 0 COMMENT '商品',
  `pic` VARCHAR(1024) NOT NULL DEFAULT '' COMMENT '首页图',
  `name` VARCHAR(256) NOT NULL DEFAULT '' COMMENT '商品名称',
  `amount` INT NOT NULL DEFAULT 0 COMMENT '商品数量',
  `price` DECIMAL(8,2) NOT NULL DEFAULT 0.00 COMMENT '折前价格',
  `discount` DECIMAL(8,2) NOT NULL DEFAULT 0.00 COMMENT '折后价格',
  `total` DECIMAL(8,2) NOT NULL DEFAULT 0.0 COMMENT '总价',
  `dateline` INT NOT NULL DEFAULT 0 COMMENT '添加日期',
  KEY `uid`(`uid`),
  KEY `spid`(`spid`)
);
#浏览行为
DROP TABLE IF EXISTS `vivo_common_member_browsing`;
CREATE TABLE `vivo_common_member_browsing`(
  `uid` INT NOT NULL DEFAULT 0 COMMENT '用户id',
  `sid` INT NOT NULL DEFAULT 0 COMMENT '商铺id',
  `pdid` INT NOT NULL DEFAULT 0 COMMENT '产品id',
  `dateline` INT NOT NULL DEFAULT 0 COMMENT '浏览时间',
  `ip` CHAR(15) NOT NULL DEFAULT 0 COMMENT '客户ip',
  KEY `uid`(`uid`)
);
##SQL Chinese Description : 地址表增加联动识别
ALTER TABLE `vivo_common_member_address` ADD `province` CHAR(16) NOT NULL DEFAULT '' COMMENT '省份';
ALTER TABLE `vivo_common_member_address` ADD `city` CHAR(16) NOT NULL DEFAULT '' COMMENT '城市';
ALTER TABLE `vivo_common_member_address` ADD `dist` CHAR(16) NOT NULL DEFAULT '' COMMENT '镇区';
ALTER TABLE `vivo_common_member_address` ADD `community` CHAR(16) NOT NULL DEFAULT '' COMMENT '街道';
ALTER TABLE `vivo_common_member_address` ADD `provinceid` INT NOT NULL DEFAULT 0 COMMENT '省份id';
ALTER TABLE `vivo_common_member_address` ADD `cityid` INT NOT NULL DEFAULT 0 COMMENT '城市id';
ALTER TABLE `vivo_common_member_address` ADD `distid` INT NOT NULL DEFAULT 0 COMMENT '镇区id';
ALTER TABLE `vivo_common_member_address` ADD `communityid` INT NOT NULL DEFAULT 0 COMMENT '街道id';

##SQL Chinese Description : 订单表增加利润字段
ALTER TABLE `vivo_common_member_order`
ADD COLUMN `totalprofit`  decimal(10,2) NOT NULL DEFAULT 0.0 COMMENT '产品总利润' AFTER `totalprice`;
##SQL Chinese Description : 订单明细表增加主键,利润字段
ALTER TABLE `vivo_common_member_order_detail`
ADD COLUMN `odid`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST ,
ADD PRIMARY KEY (`odid`);
ALTER TABLE `vivo_common_member_order_detail`
ADD COLUMN `profit` decimal(10,1) NOT NULL DEFAULT 0.00 COMMENT '利润' AFTER `totalcredit`;
##SQL Chinese Description : 商品上架表增加成本字段
ALTER TABLE `vivo_common_product_onsell`
ADD COLUMN `cost` decimal(8,2) NOT NULL DEFAULT 0.0 COMMENT '成本价' AFTER `groupid`;

#######################经销商系统扩展
#经销商账户系统
DROP TABLE IF EXISTS `vivo_dealer_member`;
CREATE TABLE `vivo_dealer_member`(
  `duid` INT PRIMARY KEY AUTO_INCREMENT,
  `uid` INT NOT NULL DEFAULT 0 COMMENT '用户id',
  `company` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '经销商名',
  `today_deals` INT NOT NULL DEFAULT 0 COMMENT '今日下单数',
  `today_rebate` DECIMAL(8,1) NOT NULL DEFAULT 0.0 COMMENT '今日分成额',
  `today_browsing` INT NOT NULL DEFAULT 0 COMMENT '今日扫码次数',
  KEY `uid`(`uid`)
);
#经销商--扩展资料
DROP TABLE IF EXISTS `vivo_dealer_member_profile`;
CREATE TABLE `vivo_dealer_member_profile`(
  `duid` INT PRIMARY KEY,
  `owner` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '拥有者',
  `mobile` CHAR(16) NOT NULL DEFAULT '' COMMENT '手机',
  `phone` CHAR(16) NOT NULL DEFAULT '' COMMENT '座机'
);

#经销商名下店铺
DROP TABLE IF EXISTS `vivo_dealer_member_shop`;
CREATE TABLE `vivo_dealer_member_shop`(
  `sid` INT PRIMARY KEY AUTO_INCREMENT,
  `duid` INT NOT NULL DEFAULT 0 COMMENT '所属经销商id',
  `name` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '商铺名称',
  `dateline` INT NOT NULL DEFAULT 0 COMMENT '创建日期',
  `startdate` INT NOT NULL DEFAULT 0 COMMENT '开店日期',
  `today_deals` INT NOT NULL DEFAULT 0 COMMENT '今日下单数',
  `today_rebate` DECIMAL(8,1) NOT NULL DEFAULT 0.0 COMMENT '今日分成额',
  `today_browsing` INT NOT NULL DEFAULT 0 COMMENT '今日扫码次数',
  KEY `duid`(`duid`)
);

#经销商名下店铺--扩展资料
DROP TABLE IF EXISTS `vivo_dealer_member_shop_profile`;
CREATE TABLE `vivo_dealer_member_shop_profile`(
  `sid` INT PRIMARY KEY,
  `address` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '商铺地址',
  `area` INT NOT NULL DEFAULT 0 COMMENT '店铺大小:单位平方米'
);

#代理商品申请表
DROP TABLE IF EXISTS `vivo_dealer_member_product_apply`;
CREATE TABLE `vivo_dealer_member_product_apply`(
  `mpaid` INT PRIMARY KEY AUTO_INCREMENT,
  `duid` INT NOT NULL DEFAULT 0 COMMENT '经销商id',
  `pdid` INT NOT NULL DEFAULT 0 COMMENT '商品id',
  `dateline` INT NOT NULL DEFAULT 0 COMMENT '插入日期',
  `bdate` INT NOT NULL DEFAULT 0 COMMENT '代理开始日期',
  `edate` INT NOT NULL DEFAULT 0 COMMENT '代理结束日期',
  `rebate` SMALLINT NOT NULL DEFAULT 0 COMMENT '返点',
  `status` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '状态:0待审核,1已通过,2已驳回,3已过期'
);

#代理商品表
DROP TABLE IF EXISTS `vivo_dealer_member_product`;
CREATE TABLE `vivo_dealer_member_product`(
  `mpid` INT PRIMARY KEY AUTO_INCREMENT,
  `duid` INT NOT NULL DEFAULT 0 COMMENT '经销商id',
  `pdid` INT NOT NULL DEFAULT 0 COMMENT '商品id',
  `dateline` INT NOT NULL DEFAULT 0 COMMENT '插入日期',
  `rebate` SMALLINT NOT NULL DEFAULT 0 COMMENT '返点',
  `totalsales` DECIMAL(10,1) NOT NULL DEFAULT 0.0 COMMENT '总销售额',
  `traffic` INT NOT NULL DEFAULT 0 COMMENT '总流量'
);

#商品-商铺-用户关系绑定
DROP TABLE IF EXISTS `vivo_dealer_sale_relation`;
CREATE TABLE `vivo_dealer_sale_relation`(
  `srid` INT PRIMARY KEY AUTO_INCREMENT,
  `uid` INT NOT NULL DEFAULT 0 COMMENT '用户id',
  `pdid` INT NOT NULL DEFAULT 0 COMMENT '产品id',
  `sid` INT NOT NULL DEFAULT 0 COMMENT '店铺id',
  `dateline` INT NOT NULL DEFAULT 0 COMMENT '第一次时间',
  `refreshdate` INT NOT NULL DEFAULT 0 COMMENT '刷新时间',
  KEY `uid`(`uid`),
  KEY `pdid`(`pdid`),
  KEY `sid`(`sid`)
);

#财务返点规则表
DROP TABLE IF EXISTS `vivo_dealer_rebate_rule`;
CREATE TABLE `vivo_dealer_rebate_rule`(
  `duid` INT PRIMARY KEY,
  `rebate` SMALLINT NOT NULL DEFAULT 0 COMMENT '财务返点'
);

#地址-商铺映射
DROP TABLE IF EXISTS `vivo_dealer_shop_address`;
CREATE TABLE `vivo_dealer_shop_address`(
  `sid` INT NOT NULL DEFAULT 0 COMMENT '商铺id',
  `name` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '商铺名',
  `provinceid` INT NOT NULL DEFAULT 0 COMMENT '省份id',
  `cityid` INT NOT NULL DEFAULT 0 COMMENT '市id',
  KEY `sid`(`sid`)
);

#订单成交记录表(原始数据)
DROP TABLE IF EXISTS `vivo_dealer_shop_rebate`;
CREATE TABLE `vivo_dealer_shop_rebate`(
  `rid` INT PRIMARY KEY AUTO_INCREMENT,
  `sid` INT NOT NULL DEFAULT 0 COMMENT '商铺id',
  `type` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '扫码购买0,地区归属购买1',
  `oid` INT NOT NULL DEFAULT 0 COMMENT '订单id',
  `o_openid` INT NOT NULL DEFAULT 0 COMMENT '订单号',
  `nickname` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '下单用户昵称',
  `pdid` INT NOT NULL DEFAULT 0 COMMENT  '商品id',
  `pdname` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '商品名称',
  `odid` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '明细id',
  `amount` SMALLINT NOT NULL DEFAULT 0 COMMENT '下单数量', 
  `total` DECIMAL(8,2) NOT NULL DEFAULT 0.00 COMMENT '总价',
  `profit` DECIMAL(8,2) NOT NULL DEFAULT 0.00 COMMENT '利润',
  `rebate` DECIMAL(8,2) NOT NULL DEFAULT 0.00 COMMENT '分成',
  `dateline` INT NOT NULL DEFAULT 0 COMMENT '下单日期',
  KEY `sid`(`sid`),
  KEY `dateline`(`dateline`)
);

##数据统计
#店铺购买记录日缓存
DROP TABLE IF EXISTS `vivo_dealer_shop_rebate_day`;
CREATE TABLE `vivo_dealer_shop_rebate_day`(
  `sid` INT NOT NULL DEFAULT 0 COMMENT '商铺id',
  `year` SMALLINT NOT NULL DEFAULT 0 COMMENT '年份',
  `month` TINYINT(2) NOT NULL DEFAULT 0 COMMENT '月份',
  `day` TINYINT(2) NOT NULL DEFAULT 0 COMMENT '日',
  `count` INT NOT NULL DEFAULT 0 COMMENT '日下单量',
  `total` DECIMAL(8,1) NOT NULL DEFAULT 0.0 COMMENT '日营业额',
  `profit` DECIMAL(8,1) NOT NULL DEFAULT 0.0 COMMENT '日利润额',
  `rebate` DECIMAL(8,1) NOT NULL DEFAULT 0.0 COMMENT '日分成额',
  `year_on_year` SMALLINT NOT NULL DEFAULT 0 COMMENT '同比',
  `chain` SMALLINT NOT NULL DEFAULT 0 COMMENT '环比',
  KEY `sid`(`sid`),
  KEY `year`(`year`),
  KEY `month`(`month`),
  KEY `day`(`day`)
);
#店铺购买记录月缓存
DROP TABLE IF EXISTS `vivo_dealer_shop_rebate_month`;
CREATE TABLE `vivo_dealer_shop_rebate_month`(
  `sid` INT NOT NULL DEFAULT 0 COMMENT '商铺id',
  `year` SMALLINT NOT NULL DEFAULT 0 COMMENT '年份',
  `month` TINYINT(2) NOT NULL DEFAULT 0 COMMENT '月份',
  `count` INT NOT NULL DEFAULT 0 COMMENT '月下单量',
  `total` DECIMAL(10,1) NOT NULL DEFAULT 0.00 COMMENT '月营业额',
  `profit` DECIMAL(10,1) NOT NULL DEFAULT 0.0 COMMENT '月利润额',
  `rebate` DECIMAL(10,1) NOT NULL DEFAULT 0.00 COMMENT '月分成额',
  `year_on_year` SMALLINT NOT NULL DEFAULT 0 COMMENT '同比',
  `chain` SMALLINT NOT NULL DEFAULT 0 COMMENT '环比',
  KEY `sid`(`sid`),
  KEY `year`(`year`),
  KEY `month`(`month`)
);

#扫码记录日缓存
DROP TABLE IF EXISTS `vivo_dealer_browsing_day`;
CREATE TABLE `vivo_dealer_browsing_day`(
  `sid` INT NOT NULL DEFAULT 0 COMMENT '商铺id',
  `amount` INT NOT NULL DEFAULT 0 COMMENT '数量',
  `dateline` INT NOT NULL DEFAULT 0 COMMENT '日期',
  KEY `sid`(`sid`),
  KEY `dateline`(`dateline`)
);
#扫码产品日缓存表
DROP TABLE IF EXISTS `vivo_dealer_product_browsing_day`;
CREATE TABLE `vivo_dealer_product_browsing_day`(
  `sid` INT NOT NULL DEFAULT 0 COMMENT '商铺id',
  `pdid` INT NOT NULL DEFAULT 0 COMMENT '产品id',
  `pdname` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '产品名',
  `rate` SMALLINT(4) NOT NULL DEFAULT 0 COMMENT '所占比率',
  `dateline` INT NOT NULL DEFAULT 0 COMMENT '日期',
  KEY `sid`(`sid`),
  KEY `dateline`(`dateline`)
);
#扫码产品月缓存表
DROP TABLE IF EXISTS `vivo_dealer_product_browsing_month`;
CREATE TABLE `vivo_dealer_product_browsing_month`(
  `sid` INT NOT NULL DEFAULT 0 COMMENT '商铺id',
  `pdid` INT NOT NULL DEFAULT 0 COMMENT '产品id',
  `pdname` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '产品名',
  `rate` SMALLINT(4) NOT NULL DEFAULT 0 COMMENT '所占比率',
  `year` SMALLINT(4) NOT NULL DEFAULT 0 COMMENT '年份',
  `month` TINYINT(2) NOT NULL DEFAULT 0 COMMENT '月份',
  KEY `sid`(`sid`),
  KEY `year`(`year`),
  KEY `month`(`month`)
);

#经销商统计日缓存
DROP TABLE IF EXISTS `vivo_dealer_rebate_day`;
CREATE TABLE `vivo_dealer_rebate_day`(
  `duid` INT NOT NULL DEFAULT 0 COMMENT '经销商id',
  `year` SMALLINT NOT NULL DEFAULT 0 COMMENT '年份',
  `month` TINYINT(2) NOT NULL DEFAULT 0 COMMENT '月份',
  `day` TINYINT(2) NOT NULL DEFAULT 0 COMMENT '日',
  `count` INT NOT NULL DEFAULT 0 COMMENT '日下单量',
  `total` DECIMAL(8,1) NOT NULL DEFAULT 0.0 COMMENT '日营业额',
  `profit` DECIMAL(8,1) NOT NULL DEFAULT 0.0 COMMENT '日利润额',
  `rebate` DECIMAL(8,1) NOT NULL DEFAULT 0.0 COMMENT '日分成额',
  `browsing_amount` INT NOT NULL DEFAULT 0 COMMENT '扫码数量',
  KEY `duid`(`duid`),
  KEY `year`(`year`),
  KEY `month`(`month`),
  KEY `day`(`day`)
);
#经销商统计月缓存
DROP TABLE IF EXISTS `vivo_dealer_rebate_month`;
CREATE TABLE `vivo_dealer_rebate_month`(
  `duid` INT NOT NULL DEFAULT 0 COMMENT '经销商id',
  `year` SMALLINT NOT NULL DEFAULT 0 COMMENT '年份',
  `month` TINYINT(2) NOT NULL DEFAULT 0 COMMENT '月份',
  `count` INT NOT NULL DEFAULT 0 COMMENT '月下单量',
  `total` DECIMAL(10,1) NOT NULL DEFAULT 0.00 COMMENT '月营业额',
  `profit` DECIMAL(10,1) NOT NULL DEFAULT 0.0 COMMENT '月利润额',
  `rebate` DECIMAL(10,1) NOT NULL DEFAULT 0.00 COMMENT '月分成额',
  `browsing_amount` INT NOT NULL DEFAULT 0 COMMENT '扫码数量',
  KEY `duid`(`duid`),
  KEY `year`(`year`),
  KEY `month`(`month`)
);

#总统计日缓存
DROP TABLE IF EXISTS `vivo_dealer_all_rebate_day`;
CREATE TABLE `vivo_dealer_all_rebate_day`(
  `year` SMALLINT NOT NULL DEFAULT 0 COMMENT '年份',
  `month` TINYINT(2) NOT NULL DEFAULT 0 COMMENT '月份',
  `day` TINYINT(2) NOT NULL DEFAULT 0 COMMENT '日',
  `count` INT NOT NULL DEFAULT 0 COMMENT '日下单量',
  `total` DECIMAL(8,1) NOT NULL DEFAULT 0.0 COMMENT '日营业额',
  `profit` DECIMAL(8,1) NOT NULL DEFAULT 0.0 COMMENT '日利润额',
  `rebate` DECIMAL(8,1) NOT NULL DEFAULT 0.0 COMMENT '日分成额',
  `browsing_amount` INT NOT NULL DEFAULT 0 COMMENT '扫码数量',
  KEY `year`(`year`),
  KEY `month`(`month`),
  KEY `day`(`day`)
);
#总统计月缓存
DROP TABLE IF EXISTS `vivo_dealer_all_rebate_month`;
CREATE TABLE `vivo_dealer_all_rebate_month`(
  `year` SMALLINT NOT NULL DEFAULT 0 COMMENT '年份',
  `month` TINYINT(2) NOT NULL DEFAULT 0 COMMENT '月份',
  `count` INT NOT NULL DEFAULT 0 COMMENT '月下单量',
  `total` DECIMAL(10,1) NOT NULL DEFAULT 0.00 COMMENT '月营业额',
  `profit` DECIMAL(10,1) NOT NULL DEFAULT 0.0 COMMENT '月利润额',
  `rebate` DECIMAL(10,1) NOT NULL DEFAULT 0.00 COMMENT '月分成额',
  `browsing_amount` INT NOT NULL DEFAULT 0 COMMENT '扫码数量',
  KEY `year`(`year`),
  KEY `month`(`month`)
);

#end



############### 微信模块 ####################
#date: 20150504

#公众账号信息表
DROP TABLE IF EXISTS `vivo_wechats`;
CREATE TABLE  `vivo_wechats` (
  `weid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hash` char(5) NOT NULL COMMENT '用户标识. 随机生成保持不重复',
  `uid` int(10) unsigned NOT NULL COMMENT '关联的用户',
  `token` varchar(32) NOT NULL COMMENT '随机生成密钥',
  `EncodingAESKey` varchar(43) NOT NULL,
  `access_token` varchar(1000) NOT NULL DEFAULT '' COMMENT '存取凭证结构',
  `level` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '接口权限级别, 0 普通订阅号, 1 认证订阅号|普通服务号, 2认证服务号',
  `name` varchar(30) NOT NULL COMMENT '公众号名称',
  `account` varchar(30) NOT NULL COMMENT '微信帐号',
  `original` varchar(50) NOT NULL,
  `signature` varchar(100) NOT NULL COMMENT '功能介绍',
  `country` varchar(10) NOT NULL,
  `province` varchar(3) NOT NULL,
  `city` varchar(15) NOT NULL,
  `welcome` varchar(1000) NOT NULL COMMENT '欢迎信息',
  `default` varchar(1000) NOT NULL COMMENT '默认回复',
  `default_message` varchar(500) NOT NULL DEFAULT '' COMMENT '其他消息类型默认处理器',
  `default_period` tinyint(3) unsigned NOT NULL COMMENT '回复周期时间',
  `lastupdate` int(10) unsigned NOT NULL DEFAULT '0',
  `key` varchar(50) NOT NULL,
  `secret` varchar(50) NOT NULL,
  `payment` varchar(5000) NOT NULL DEFAULT '',
  `shortcuts` varchar(2000) NOT NULL DEFAULT '',
  `quickmenu` varchar(2000) NOT NULL DEFAULT '',
  `parentid` int(10) unsigned NOT NULL DEFAULT '0',
  `subwechats` varchar(1000) NOT NULL DEFAULT '',
  `siteinfo` varchar(1000) NOT NULL DEFAULT '',
  `menuset` text NOT NULL,
  `groups` varchar(2000) NOT NULL COMMENT '粉丝分组',
  `accountlink` varchar(500) DEFAULT NULL COMMENT '公众号引导关注',
  `jsapi_ticket` varchar(1000) NOT NULL,
  PRIMARY KEY (`weid`),
  UNIQUE KEY `hash` (`hash`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

#信息规则表
CREATE TABLE IF NOT EXISTS `vivo_wechat_rule` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `weid` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '回复类型：0：文字回复，1：图文回复，2：语音回复',
  `reply` varchar(1024) NOT NULL DEFAULT '' COMMENT '回复，如果是文字回复，则直接保存，如果是语音或者图文，则保存ID',
  `displayorder` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '规则排序',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '规则状态，0禁用，1启用，2置顶',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

#规则内容
DROP TABLE IF EXISTS `vivo_wechat_rule_keyword`;
CREATE TABLE `vivo_wechat_rule_keyword` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '规则ID',
  `keyword` varchar(255) NOT NULL COMMENT '内容',
  `keytype` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '类型1匹配，2包含，3正则',
  `displayorder` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '规则排序，255为置顶，其它为普通排序',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '规则状态，0禁用，1启用',
  PRIMARY KEY (`id`),
  KEY `idx_content` (`content`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

#语音信息回复内容表
DROP TABLE IF EXISTS `vivo_wechat_reply_music`;
CREATE TABLE `vivo_wechat_reply_music` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '标题',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '介绍',
  `url` varchar(300) NOT NULL DEFAULT '' COMMENT '音乐地址',
  `hqurl` varchar(300) NOT NULL DEFAULT '' COMMENT '高清地址',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

#图文信息回复内容表
DROP TABLE IF EXISTS `vivo_wechat_reply_image`;
CREATE TABLE `vivo_wechat_reply_image` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parentid` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `thumb` varchar(256) NOT NULL,
  `content` text NOT NULL,
  `url` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

#微信用户记录
DROP TABLE IF EXISTS `vivo_common_member_wechat`;
CREATE TABLE IF NOT EXISTS `vivo_common_member_wechat` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `weid` smallint(5) NOT NULL,
  `uid` int(11) unsigned NOT NULL,
  `openid` char(32) NOT NULL DEFAULT '',
  `subscribe` tinyint(1) NOT NULL COMMENT '是否关注公众账号',
  `nickname` char(50) NOT NULL COMMENT '微信昵称',
  `sex` tinyint(1) NOT NULL COMMENT '用户的性别，值为1时是男性，值为2时是女性，值为0时是未知',
  `city` char(20) NOT NULL COMMENT '用户所在城市',
  `country` char(20) NOT NULL COMMENT '用户所在国家',
  `province` char(20) NOT NULL COMMENT '用户所在省份',
  `language` char(10) NOT NULL COMMENT '用户的语言，简体中文为zh_CN',
  `headimgurl` varchar(255) NOT NULL COMMENT '用户头像',
  `isheadimglocal` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否将头像本地化为dz头像',
  `subscribe_time` char(15) NOT NULL COMMENT '用户关注时间，为时间戳。',
  `unionid` char(32) NOT NULL,
  `groupid` INT( 10 ) NOT NULL COMMENT  '用户所在的分组ID',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `addtime` CHAR( 15 ) NOT NULL COMMENT  '获取资料的时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `openid` (`openid`),
  KEY `uid` (`uid`),
  KEY `weid` (`weid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

##Date:20150504
##Author:Jerry
##SQL Chinese Description : 增加商品在售列表隐藏字段
ALTER TABLE `vivo_common_product_onsell`
ADD COLUMN `ishidden` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '是否在商品在售列表隐藏' AFTER `discounttype`;

##SQL Chinese Description : 取消订单申请表
DROP TABLE IF EXISTS `vivo_common_member_order_cancel`;
CREATE TABLE `vivo_common_member_order_cancel` (
  `ocid` INT PRIMARY KEY AUTO_INCREMENT,
  `oid` INT NOT NULL DEFAULT 0 COMMENT '订单id',
  `uid` INT NOT NULL DEFAULT 0 COMMENT '用户id',
  `type` TINYINT NOT NULL DEFAULT 0 COMMENT '取消订单原因:-1其他原因,0重复下单,1现在不想购买,2收货人信息有误,3无法支付',
  `note` VARCHAR(1024) NOT NULL DEFAULT '' COMMENT '备注',
  `state` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '取消申请状态:0刚申请,1已批准,2已反驳',
  `dateline` INT NOT NULL DEFAULT 0 COMMENT '申请时间',
  `reason` TEXT NOT NULL DEFAULT '' COMMENT '处理原因',
  KEY `oid`(`oid`),
  KEY `uid`(`uid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

##Date:20150507
##Author:goodspb
##SQL Chinese Description : 订单表中加入支付方式&支付状态
ALTER TABLE  `vivo_common_member_order` ADD  `payment` TINYINT( 1 ) NOT NULL DEFAULT  '0' COMMENT  '支付方式，0：货到付款；1:微信支付' AFTER  `note` ,
ADD  `paystate` TINYINT( 1 ) NOT NULL COMMENT  '支付状态，0：未支付；1：已支付；2：已退款' AFTER  `payment`

##SQL Chinese Description : 微信支付状态记录
DROP TABLE IF EXISTS `vivo_payment_wechat_state`;
CREATE TABLE IF NOT EXISTS `vivo_payment_wechat_state` (
  `pws_id` int(11) NOT NULL AUTO_INCREMENT,
  `return_code` char(10) NOT NULL DEFAULT '' COMMENT '通信标识',
  `result_code` char(10) NOT NULL DEFAULT '' COMMENT '交易 标识',
  `trade_state` CHAR( 32 ) NOT NULL DEFAULT '' COMMENT '当使用订单查询接口主动查询时返回本字段：SUCCESS—支付成功  REFUND—转入退款  NOTPAY—未支付  CLOSED—已关闭  REVOKED—已撤销  USERPAYING--用户支付中  NOPAY--未支付(输入密码或 确认支付超时)  PAYERROR--支付失败(其他 原因，如银行返回失败)',
  `weid` smallint(6) NOT NULL DEFAULT '0' COMMENT '公众号ID',
  `oid` int(11) NOT NULL COMMENT '订单ID',
  `out_trade_no` int(10) NOT NULL COMMENT '订单号',
  `openid` varchar(128) NOT NULL COMMENT '用户openid',
  `is_subscribe` char(1) NOT NULL COMMENT '用户是否关注公众账号，Y- 关注，N-未关注，仅在公众 账号类型支付有效',
  `trade_type` char(16) NOT NULL COMMENT '交易类型',
  `bank_type` char(16) NOT NULL COMMENT '银行类型，采用字符串类型 的银行标识',
  `total_fee` int(10) NOT NULL COMMENT '总金额',
  `coupon_fee` int(10) NOT NULL COMMENT '现金券支付金额<=订单总金 额，订单总金额-现金券金额 为现金支付金额',
  `fee_type` char(8) NOT NULL COMMENT '货币类型，符合 ISO 4217标 准的三位字母代码，默认人 民币：CNY',
  `transaction_id` varchar(32) NOT NULL COMMENT '微信支付订单号',
  `time_end` char(14) NOT NULL COMMENT '支付 完成时间， 格式为 yyyyMMddhhmmss，如2009年 12月27日9点10分10秒表 示为 20091227091010。时区 为GMT+8 beijing。该时间取 自微信支付服务器',
  PRIMARY KEY (`pws_id`),
  KEY `oid` (`oid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


-- --------------------------------------------------------

##Date:20150508
##Author:Jerry
##SQL Chinese Description : 增加商品手机列表图
ALTER TABLE `vivo_common_product`
ADD COLUMN `mobile_banner` VARCHAR(1024) NOT NULL DEFAULT '' COMMENT '商品手机列表图' AFTER `media_list`;

-- --------------------------------------------------------

#Date: 20150513
#AUTHOR: goodspb
#主题表增加封面图片
ALTER TABLE  `vivo_forum_thread` ADD  `coverimg` VARCHAR( 1000 ) NOT NULL DEFAULT  '' COMMENT  '封面图片' AFTER  `media_list`

-- --------------------------------------------------------

#Date: 20150514
#author: goodspb
#商品增加规格显示
CREATE TABLE IF NOT EXISTS `vivo_common_product_spec` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `psid` smallint(6) NOT NULL COMMENT '分类ID',
  `name` char(30) NOT NULL COMMENT '名称',
  `type` CHAR( 10 ) NOT NULL COMMENT  '分类：text;select;date;',
  `extvalue` TEXT NOT NULL COMMENT  '扩展选项；例如select中的选项',
  `displayorder` smallint(6) NOT NULL COMMENT '排序',
  `addtime` char(14) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `psid` (`psid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


#商品表保存规格
ALTER TABLE  `vivo_common_product` ADD  `spec` TEXT NOT NULL COMMENT  '规格值' AFTER  `unit`
-- --------------------------------------------------------

#Date: 20150519
#author: goodspb
#修改用户表 username字段长度为32位
ALTER TABLE  `vivo_common_member` CHANGE  `username`  `username` CHAR( 32 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT  ''

-- --------------------------------------------------------


#Date: 20150521
#author: wen
##SQL Chinese Description : 增加字段标记经销商及旗下店铺的删除状态
ALTER TABLE `vivo_dealer_member` ADD `deleted` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '经销商是否已经删除';
ALTER TABLE `vivo_dealer_member_shop` ADD `deleted` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '经销商是否已经删除';

-- --------------------------------------------------------

#Date: 20150521
#author: goodspb
## 增加模板消息判断表
CREATE TABLE IF NOT EXISTS `vivo_common_member_wechat_templatemsg` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` char(11) NOT NULL COMMENT '目标的类型，如果是订单通知，则为order',
  `target_id` int(11) NOT NULL COMMENT '目标的ID，如果是订单通知，则target_id为oid',
  `msgid` int(11) NOT NULL COMMENT '消息模板的ID',
  `state` tinyint(1) NOT NULL COMMENT '0：未知；1：成功；2：用户拒绝接收；3：发送失败',
  `timeline` char(15) NOT NULL COMMENT '时间戳',
  PRIMARY KEY (`id`),
  KEY `target_id` (`target_id`,`msgid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------


#Date: 20150528
#author: Jerru
##SQL Chinese Description : 购物车增加字段标识人民币商品和积分商品
ALTER TABLE `vivo_common_member_shopcart`
ADD COLUMN `type`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '0人民币商品,1积分商品' AFTER `spid`;

-- --------------------------------------------------------

#Date: 20150528
#author: goodspb
#标记订单商品详细中绑定TID
ALTER TABLE  `vivo_common_member_order_detail` ADD  `tid` INT( 10 ) NOT NULL DEFAULT  '0' COMMENT  '绑定的文章ID' AFTER  `spid` ,
ADD INDEX (  `tid` );


#Date: 20150529
#author: goodspb
#微信带参数的二维码
CREATE TABLE IF NOT EXISTS `vivo_wechat_qrcode` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(1000) NOT NULL COMMENT '二维码URL',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '二维码类型: 0:临时二维码；1：永久二维码',
  `deadline` char(15) NOT NULL COMMENT '到期时间，时间戳',
  `func` tinyint(1) NOT NULL DEFAULT '0' COMMENT '功能：暂时只有0：发送关键词',
  `param` varchar(1000) NOT NULL COMMENT '参数',
  `scene_id` char(32) NOT NULL COMMENT '场景值ID，临时二维码时为32位非0整型，永久二维码时最大值为100000（目前参数只支持1--100000）',
  `ticket` int(11) NOT NULL COMMENT '获取的二维码ticket，凭借此ticket可以在有效时间内换取二维码。',
  PRIMARY KEY (`id`),
  KEY `scene_id` (`scene_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------


#Date: 20150601
#author: goodspb
#保存微信上报的地理位置
CREATE TABLE IF NOT EXISTS `vivo_wechat_member_location` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `weid` smallint(3) NOT NULL,
  `openid` varchar(50) NOT NULL,
  `latitude` char(15) NOT NULL COMMENT '地理位置纬度',
  `longitude` char(15) NOT NULL COMMENT '地理位置经度',
  `precision` char(15) NOT NULL COMMENT '地理位置精度',
  `addtime` char(15) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `openid` (`openid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

----------------------------------------------------------------------------

# Date: 20150619
# author: zhiwen
# 徽章规则表

CREATE TABLE IF NOT EXISTS `vivo_badge_rule` (
  `badge` varchar(64) NOT NULL DEFAULT '',
  `name` varchar(64) NOT NULL DEFAULT '' COMMENT '徽章名',
  `displayorder` smallint(6) DEFAULT '0' COMMENT '排序',
  `desc` varchar(1024) DEFAULT '' COMMENT '描述',
  `lv1` char(16) DEFAULT '' COMMENT '等级1名字',
  `lv1_limit` int(11) DEFAULT '0' COMMENT '等级1升级限制',
  `lv1_image` varchar(1024) DEFAULT '' COMMENT '等级1图片',
  `lv2` char(16) DEFAULT '' COMMENT '等级2名字',
  `lv2_limit` int(11) DEFAULT '0' COMMENT '等级2升级限制',
  `lv2_image` varchar(1024) DEFAULT '' COMMENT '等级2图片',
  `lv3` char(16) DEFAULT '' COMMENT '等级3名字',
  `lv3_limit` int(11) DEFAULT '0' COMMENT '等级3升级限制',
  `lv3_image` varchar(1024) DEFAULT '' COMMENT '等级3图片',
  `lv4` char(16) DEFAULT '' COMMENT '等级4名字',
  `lv4_limit` int(11) DEFAULT '0' COMMENT '等级4升级限制',
  `lv4_image` varchar(1024) DEFAULT '' COMMENT '等级4图片',
  `lv5` char(16) DEFAULT '' COMMENT '等级5名字',
  `lv5_limit` int(11) DEFAULT '0' COMMENT '等级5升级限制',
  `lv5_image` varchar(1024) DEFAULT '' COMMENT '等级5图片',
  `lv6` char(16) DEFAULT '' COMMENT '等级6名字',
  `lv6_limit` int(11) DEFAULT '0' COMMENT '等级6升级限制',
  `lv6_image` varchar(1024) DEFAULT '' COMMENT '等级6图片',
  `lv7` char(16) DEFAULT '' COMMENT '等级7名字',
  `lv7_limit` int(11) DEFAULT '0' COMMENT '等级7升级限制',
  `lv7_image` varchar(1024) DEFAULT '' COMMENT '等级7图片',
  PRIMARY KEY (`badge`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-------------------------------------------------------------------------------

# Date: 20150619
# author: zhiwen
# 用户拥有徽章表

CREATE TABLE IF NOT EXISTS `vivo_badge_member` (
  `bmid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `badge` enum('checkin','bubble_wine','share','comment','favorite','finishtask') NOT NULL DEFAULT 'checkin' COMMENT '对应vivo_badge_rule.badge',
  `num` int(11) NOT NULL DEFAULT '0',
  `lv1_dateline` int(11) NOT NULL DEFAULT '0' COMMENT '等级1获得时间',
  `lv2_dateline` int(11) NOT NULL DEFAULT '0' COMMENT '等级2获得时间',
  `lv3_dateline` int(11) NOT NULL DEFAULT '0' COMMENT '等级3获得时间',
  `lv4_dateline` int(11) NOT NULL DEFAULT '0' COMMENT '等级4获得时间',
  `lv5_dateline` int(11) NOT NULL DEFAULT '0' COMMENT '等级5获得时间',
  `lv6_dateline` int(11) NOT NULL DEFAULT '0' COMMENT '等级6获得时间',
  `lv7_dateline` int(11) NOT NULL DEFAULT '0' COMMENT '等级7获得时间',
  `lv` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '已获得的等级',
  PRIMARY KEY (`bmid`),
  KEY `uid` (`uid`),
  KEY `badge` (`badge`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-------------------------------------------------------------------------------

# Date: 20150619
# author: zhiwen
# 用户完成新手指导表

CREATE TABLE IF NOT EXISTS `vivo_member_finish_beginner_guide` (
  `mfbgid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `home` int(11) unsigned DEFAULT '0' COMMENT '首页完成时间',
  `post_page` int(11) unsigned DEFAULT '0' COMMENT '帖子页完成时间',
  `mobile_viewthread` int(2) DEFAULT NULL,
  PRIMARY KEY (`mfbgid`),
  KEY `uid` (`uid`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

# Date : 20150623
# author : goodspb
# 增加订单事务，所以将member_order所有表换成 INNODB
ALTER TABLE  `vivo_common_member_order_cancel` ENGINE = INNODB

# 修改订单表：
ALTER TABLE  `vivo_common_member_order` CHANGE  `payment`  `payment` TINYINT( 1 ) NOT NULL DEFAULT  '1' COMMENT  '支付方式，对应common_payment表';
UPDATE `vivo_common_member_order` SET `payment`=2 WHERE `payment`=1;
UPDATE `vivo_common_member_order` SET `payment`=1 WHERE `payment`=0;

#增加支付选项表
CREATE TABLE IF NOT EXISTS `vivo_common_payment` (
  `pid` tinyint(3) unsigned NOT NULL AUTO_INCREMENT COMMENT '支付方式ID',
  `paytype` char(32) NOT NULL COMMENT '支付方式种类，如：微信支付为wechat',
  `payname` char(32) NOT NULL COMMENT '支付方式名称',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否开启，0:关闭，1开启',
  `is_mobile` tinyint(1) NOT NULL DEFAULT '0' COMMENT '手机是否关闭，默认PC版是全部开启的',
  PRIMARY KEY (`pid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

#转存表中的数据 `vivo_common_payment`
INSERT INTO `vivo_common_payment` (`pid`, `paytype`, `payname`, `status`, `is_mobile`) VALUES
(1, 'cash', '货到付款', 1, 1),
(2, 'wechat', '微信支付', 1, 1),
(3, 'alipay', '支付宝', 1, 0);

#添加任务数据库
#主任务列表
CREATE TABLE IF NOT EXISTS `vivo_task` (
  `tid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` char(20) NOT NULL DEFAULT '0' COMMENT '任务分类，0：商品分享任务；',
  `name` varchar(256) NOT NULL COMMENT '任务名称',
  `pic` varchar(1024) NOT NULL COMMENT '图标',
  `timetype` tinyint(1) NOT NULL DEFAULT '0' COMMENT '时间类型：0为长期有效，1为时间区间',
  `starttime` int(11) unsigned NOT NULL COMMENT '开始时间',
  `endtime` int(11) unsigned NOT NULL COMMENT '结束时间',
  `finish` int(10) NOT NULL DEFAULT '100' COMMENT '达成的完成度',
  PRIMARY KEY (`tid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

#任务奖励表
CREATE TABLE IF NOT EXISTS `vivo_task_prize` (
  `tpid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL,
  `ptype` tinyint(1) NOT NULL COMMENT '奖品类型：0:积分;1:优惠券;2:实物',
  `pid` smallint(6) NOT NULL COMMENT '奖品id',
  `num` int(10) NOT NULL COMMENT '奖励数量',
  PRIMARY KEY (`tpid`),
  KEY `tid` (`tid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

-- --------------------------------------------------------


#任务规则表
CREATE TABLE IF NOT EXISTS `vivo_task_rule` (
  `trid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL,
  `desc` varchar(1000) NOT NULL COMMENT '描述',
  `icon` varchar(1000) NOT NULL COMMENT 'ICON',
  `actionid` tinyint(1) NOT NULL COMMENT '操作ID',
  `aval` smallint(3) NOT NULL COMMENT '增加多少完成度',
  PRIMARY KEY (`trid`),
  KEY `tid` (`tid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------


#任务用户表-分表1
CREATE TABLE IF NOT EXISTS `vivo_task_rule_user_0` (
  `truid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL,
  `trid` int(11) NOT NULL,
  `task_uid` int(11) NOT NULL COMMENT '任务主UID',
  `uid` int(11) NOT NULL,
  `dateline` int(11) NOT NULL,
  PRIMARY KEY (`truid`),
  KEY `trid` (`trid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

#任务用户表-分表2
CREATE TABLE IF NOT EXISTS `vivo_task_rule_user_1` (
  `truid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL,
  `trid` int(11) NOT NULL,
  `task_uid` int(11) NOT NULL COMMENT '任务主UID',
  `uid` int(11) NOT NULL,
  `dateline` int(11) NOT NULL,
  PRIMARY KEY (`truid`),
  KEY `trid` (`trid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

#任务用户表-分表3
CREATE TABLE IF NOT EXISTS `vivo_task_rule_user_2` (
  `truid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL,
  `trid` int(11) NOT NULL,
  `task_uid` int(11) NOT NULL COMMENT '任务主UID',
  `uid` int(11) NOT NULL,
  `dateline` int(11) NOT NULL,
  PRIMARY KEY (`truid`),
  KEY `trid` (`trid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

#任务用户表-分表4
CREATE TABLE IF NOT EXISTS `vivo_task_rule_user_3` (
  `truid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL,
  `trid` int(11) NOT NULL,
  `task_uid` int(11) NOT NULL COMMENT '任务主UID',
  `uid` int(11) NOT NULL,
  `dateline` int(11) NOT NULL,
  PRIMARY KEY (`truid`),
  KEY `trid` (`trid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

#任务用户表-分表5
CREATE TABLE IF NOT EXISTS `vivo_task_rule_user_4` (
  `truid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL,
  `trid` int(11) NOT NULL,
  `task_uid` int(11) NOT NULL COMMENT '任务主UID',
  `uid` int(11) NOT NULL,
  `dateline` int(11) NOT NULL,
  PRIMARY KEY (`truid`),
  KEY `trid` (`trid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

#任务进度用户表-分表1
CREATE TABLE IF NOT EXISTS `vivo_task_user_0` (
  `tuid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL COMMENT '任务ID',
  `uid` int(11) NOT NULL COMMENT '用户ID',
  `progress` tinyint(3) NOT NULL COMMENT '进度，百分比',
  `dateline` int(11) NOT NULL COMMENT '时间戳',
  `isreward` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否领奖，0：未；1：已领',
  `rewardtime` int(11) NOT NULL COMMENT '领奖时间',
  PRIMARY KEY (`tuid`),
  KEY `tid` (`tid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

#任务进度用户表-分表2
CREATE TABLE IF NOT EXISTS `vivo_task_user_1` (
  `tuid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL COMMENT '任务ID',
  `uid` int(11) NOT NULL COMMENT '用户ID',
  `progress` tinyint(3) NOT NULL COMMENT '进度，百分比',
  `dateline` int(11) NOT NULL COMMENT '时间戳',
  `isreward` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否领奖，0：未；1：已领',
  `rewardtime` int(11) NOT NULL COMMENT '领奖时间',
  PRIMARY KEY (`tuid`),
  KEY `tid` (`tid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

#任务进度用户表-分表3
CREATE TABLE IF NOT EXISTS `vivo_task_user_2` (
  `tuid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL COMMENT '任务ID',
  `uid` int(11) NOT NULL COMMENT '用户ID',
  `progress` tinyint(3) NOT NULL COMMENT '进度，百分比',
  `dateline` int(11) NOT NULL COMMENT '时间戳',
  `isreward` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否领奖，0：未；1：已领',
  `rewardtime` int(11) NOT NULL COMMENT '领奖时间',
  PRIMARY KEY (`tuid`),
  KEY `tid` (`tid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

#任务进度用户表-分表4
CREATE TABLE IF NOT EXISTS `vivo_task_user_3` (
  `tuid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL COMMENT '任务ID',
  `uid` int(11) NOT NULL COMMENT '用户ID',
  `progress` tinyint(3) NOT NULL COMMENT '进度，百分比',
  `dateline` int(11) NOT NULL COMMENT '时间戳',
  `isreward` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否领奖，0：未；1：已领',
  `rewardtime` int(11) NOT NULL COMMENT '领奖时间',
  PRIMARY KEY (`tuid`),
  KEY `tid` (`tid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

#任务进度用户表-分表5
CREATE TABLE IF NOT EXISTS `vivo_task_user_4` (
  `tuid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL COMMENT '任务ID',
  `uid` int(11) NOT NULL COMMENT '用户ID',
  `progress` tinyint(3) NOT NULL COMMENT '进度，百分比',
  `dateline` int(11) NOT NULL COMMENT '时间戳',
  `isreward` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否领奖，0：未；1：已领',
  `rewardtime` int(11) NOT NULL COMMENT '领奖时间',
  PRIMARY KEY (`tuid`),
  KEY `tid` (`tid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
