<?php

/**
 * APP接口
 */


define('APPTYPEID', 0);
define('CURSCRIPT', 'app');

error_reporting(E_ERROR);

require './source/class/class_core.php';

$modarray = array(
				'member',			//登陆
				'forum',			//列表操作
				'space',			//信息操作
				'shop',				//商城接口
				'wechat',			//微信登录接口
			);

$mod = !in_array(C::app()->var['mod'], $modarray) ? 'forum' : C::app()->var['mod'];
$act = trim($_REQUEST['act'] ? $_REQUEST['act'] : $_REQUEST['action']);

define('CURMODULE', $mod);
define('APPACT', $act);

//数据返回标识
define('RES_NO', 0);
define('RES_YES', 1);

C::app()->init();

//加载库文件
require libfile('function/app');
//require libfile('app/sign','class');

//runhooks();

require DISCUZ_ROOT.'./source/module/app/app_'.$mod.'.php';

?>