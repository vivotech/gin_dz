

(function($){
	$.fn.ginyeSlider = (function(options){
		
		var items = this.find("li");	// 获取项目
		var count = items.length;		// 项目总数
		var index_position = index_center = count_level = Math.ceil(count / 2) -1;	// 中间的位置
		var wrapper_width = this[0].offsetWidth;	// 容器宽度
		var item_width = items[0].offsetWidth;		// 项目宽度
		var item_height = items[0].offsetHeight;	// 项目高度
		var step = (wrapper_width - item_width) / (count - 1);	// 获取间距
		
		items.data("source_width", item_width);
		items.data("source_height", item_height);
		
		// 初始化第1个到中间的位置
		for (var i=0; i< count; i++)
		{
			if ( index_position>=count )
			{
				index_position = 0;
			}
			items.eq(i).data("index", index_position);
			index_position++;
		}
		
		items.removeClass("active").eq(0).addClass("active");
		
		// each item
		for (var i=0; i<count; i++)
		{
			var index = items.eq(i).data("index");
			var css_left = (step * index) + "px";
			var level = Math.abs(index - index_center) + 1;
			var css_zindex = count - level;
			var zoom = 1 / (count_level + 1) * level;
			var css_width = zoom * item_width;
			var css_height = zoom * item_height;
			items.eq(i).css({
				"left": css_left,
				"z-index": css_zindex,
				"width": css_width + "px",
				"height": css_height + "px",
			});
		}
	});
})(jQuery);

