﻿$(document).ready(
	function()
	{
		$('.carousel').carousel(
			{
				carouselWidth:640,
				carouselHeight:230,
				slidesPerScroll:3,
				directionNav:true,
				frontWidth: 332,
				frontHeight: 220,
				description: true,
				descriptionContainer: $(".carousel .desc"),
				mouse: true
			}
		);
	}
);