<?php
$data = json_decode($_GET['data'], 1);
?>
<!DOCTYPE html>
<html>
<head>
    <title>意大利原生活</title>
    <meta charset="utf-8" />
    <link rel="Stylesheet" type="text/css" href="content.css" />
    <link rel="Stylesheet" type="text/css" href="carousel.css" />
    <script type="text/javascript" src="scripts/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="scripts/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="scripts/jquery.carousel-1.1.min.js"></script>
    <script type="text/javascript" src="scripts/sample01.js"></script>
<script type="text/javascript">
(function($){
	$.fn.countSec = function(inputOptions){
		var self = this,
			options = {};
		
		this.Init = function(){
			options.btime = inputOptions.begin;
			options.etime = inputOptions.end;
			options.timedis = options.etime - options.btime;
			
			self.action();
		}
		
		this.action = function(){
			if(options.timedis > 0){
				self.html(self._formatData(options.timedis));
				options.timedis--;
				setTimeout(function(){
					self.action();
				}, 1000);
			}else{
				self.html("已过期");
			}
		}
		
		this._formatData = function(timedis){
			var re = "",
				rhour = parseInt(timedis / 3600),
				hour = (rhour > 99 )? 99 : rhour,
				min = parseInt((timedis - rhour * 3600) / 60),
				sec = parseInt(timedis - rhour * 3600 - min * 60);
			hour = (hour < 10) ? "0" + hour : hour;
			min = (min < 10) ? "0" + min : min;
			sec = (sec < 10) ? "0" + sec : sec;
			return hour + ":" + min + ":" + sec;
		}
		
		this.Init();
		return self;
	}
})(jQuery)
</script>
</head>
<body>
<div class="main_container">
<div class="">
<!-- carousel -->
<div class="carousel"> <!-- BEGIN CONTAINER -->
    <div class="slides"> <!-- BEGIN CAROUSEL -->
    	<?php
    	if(count($data) > 0){
    		foreach($data as $k=>$v){
    		?>
	        <div <?php echo($k == 0) ? 'class="active"' : '';?>>
	            <a href="<?php echo $v['url'];?>" target="_blank">
	            	<div class="box_image">
						<img src="<?php echo $v['image'];?>" alt="" />
					</div>
					<div class="box_content">
						<div class="number">
							<span><?php
							echo empty($v['peoplelimit']) ? '不限' : $v['peoplelimit'] . '人';
							?></span><br />
							<span>限定人数</span>
						</div>
						<div class="time">
							<span <?php echo !empty($v['time']) ? 'class="count-sec" data-b="'.time().'" data-e="'.$v['time'].'"' : ''; ?>><?php
							echo empty($v['time']) ? '不限' : $v['time'];
							?></span><br />
							<span>剩余时间</span>
						</div>
						<div class="score">
							<span><?php
							echo empty($v['credits']) ? '不限' : intval($v['credits']) . '分';
							?></span><br />
							<span>所需积分</span>
						</div>
					</div>
	            </a>
	        </div>
    		<?php
			}
		}
    	?>
    </div>
    <div class="desc">
    	<?php
		foreach ($data as $item) {
			echo '<div>' . $item['subject'] . '</div>';
		}
    	?>
    </div>
</div> <!-- END CAROUSEL -->
</div>
</div>
<script type="text/javascript">
jQuery(function(){
	jQuery(".count-sec").each(function(index){
		var _this = jQuery(this);
		_this.countSec({
			"begin":_this.attr("data-b"),
			"end":_this.attr("data-e")
		});
	});
});
</script>
</body>
</html>