
jQuery(function(){
    //JS调整高度
    fix_float_height('.touch-address-box');
    fix_float_height('.touch-order-good');
    //付款方式
    jQuery('.paymethod-selector>.selector-item').click(function(){
        var _this = jQuery(this),
            payment = _this.attr("data-content");
        if(_this.hasClass("unactive")){
            _this.siblings(".selector-item").each(function(){
                var _ = jQuery(this);
                _.removeClass("active");
                _.addClass("unactive");
            })
            _this.removeClass("unactive");
            _this.addClass("active");
            jQuery("#payment").val(payment);
        }
    });
    jQuery('#address-add-btn').click(function(){
        jQuery('#address-input').show().siblings().hide();
    });
    jQuery('#cancle-add-address-btn').click(function(){
        clearAddressInput();
        jQuery('#address-input').hide().siblings().show();
    });
    jQuery("#show_address_selecter").click(function(){
        jQuery('.addresss_check').hide();
        jQuery('#address-select-sign-' + jQuery('#address_input_id').val()).show();
        show_hide();
    });

    //滚动
    jQuery('#touch-address-selecter').perfectScrollbar();
    jQuery('#address-select-sign-' + jQuery('#address_input_id').val()).show();
    //选择地址
    jQuery(document).on('click','.touch-address-selecter-ul li:not("#address-input") div.address-detail',function(){
        var _this = jQuery(this).parent();
        var _id = _this.attr('data-id');
        setDefalut(_id,_this.attr('data-name'),_this.attr('data-phone'),_this.attr('data-address'),true);
        show_hide();
    });

    //配送时间选择
    jQuery("#select-deliver-time").click(function(){
        show_hide_obj(['touch-deliver-selecter','touch-deliver-selecter-mask']);
    });

    jQuery(".touch-deliver-selecter-ul li").click(function(){
        var va = jQuery(this).attr('data-value');
        jQuery('#deliver_time').val(va);
        jQuery("#select-deliver-time span").html(jQuery(this).html());
        show_hide_obj(['touch-deliver-selecter','touch-deliver-selecter-mask']);
    });

    //点击购买
    jQuery("#buyAction").click(function(){
        if(checkSubmit()){
            jQuery('#order-submit-form').submit();
        }
    });

});



function checkDefalut(defalt_id){
    jQuery('.address-delete-btn').show();
    document.getElementById('address-delete-btn-'+defalt_id).style.display="none";
}
function show_hide(){
    show_hide_obj(['touch-address-selecter','touch-address-selecter-mask']);
}
function show_hide_obj(_id){
    var no_click = arguments[1] ? arguments[1] : true;
    for(var i= 0;i<_id.length;i++){
        jQuery('#'+_id[i]).fadeToggle(100);
        //如果mask，添加返回方法
        if(_id[i].indexOf('mask')>0){
            jQuery('#'+_id[i]).click(function(){
                for(var j= 0;j<_id.length;j++) {
                    jQuery('#' + _id[j]).fadeOut(100);
                }
            });
        }
    }
}
function setDefalut(_id,_name,_phone,_address,change_cookie){
    change_cookie = change_cookie || false;
    jQuery('#address_input_id').val(_id);
    cheageDefalutDOM(_name,_phone,_address);
    checkDefalut(_id);
    if(change_cookie){
        jQuery.post('/forum.php?mod=ajax&action=setdefaultaddress',{'uaid':_id},function(){});
    }
}
function cheageDefalutDOM(_name,_phone,_address){
    var h = '<p class="namephone"><span class="name" >'+_name+'</span><span class="phone" >'+_phone+'</span></p><p class="address" >'+_address+'</p>';
    jQuery('div.touch-address-body').empty().css('height','auto').html(h);
    fix_float_height('.touch-address-box');
}
function changePrice(num){
    num = parseInt(num);
    var oneprice = parseFloat('{$product_onsell[newprice]}');
    var totalprice = oneprice*num;
    jQuery('#totalprice-box').html('实付款：{$rmb_product}' + totalprice + '{$credit_product}');
    jQuery('#product-total-price').html('{$rmb_product}' + totalprice + '{$credit_product}');
}

/* 检查提交 */
function checkSubmit(){
    var _addressID = jQuery('#address_input_id').val();
    if(_addressID=='' || _addressID <=0 || _addressID == 'undefined'){
        //如果没有收获地址
        alert('请选择或添加收货地址！');
        return false;
    }
    var _deliver_time = jQuery('#deliver_time').val();
    if(_deliver_time == '' || _deliver_time == 'undefined'){
        alert("请选择配送时间！");
        return false;
    }

    if(confirm('确认订单吗？')){
        return true;
    }
    return false;
}

// 清除输入框
function clearAddressInput(){
    var newname = jQuery("#newname"),
        newaddress = jQuery("#newaddress"),
        newphone = jQuery("#newphone");

    newname.val("");
    newaddress.val("");
    newphone.val("");
    jQuery("#provinceid").val("");
    jQuery("#cityid").remove();
    jQuery("#distid").remove();
    jQuery("#communityid").remove();
}

//删除地址
function delAddress(obj){
    var o = jQuery(obj),
        uaid = o.attr("data-content");
    if(uaid > 0){
        jQuery.ajax({
            "type":"POST",
            "url":"forum.php",
            "data":{
                "mod":"ajax",
                "action":"deladdress",
                "uaid":uaid
            },
            "dataType":"json",
            "success":function(data){
                var r = data.result;
                if(r > 0){
                    o.parent().parent().hide("500",function(){ jQuery(this).remove()});
                }else{
                    alert("删除地址失败", 2000);
                }
            }
        });
    }
}


// 显示新增地址窗口
function addAddressContainer(){
    clearAddressInput();
    jQuery("#address-input").removeClass("disable");
}

// 新增地址
function addAddress(obj){
    var self = this,
        newphone = jQuery("#newphone"),
        _form = jQuery('#newaddressform'),
        p = newphone.val(),
        province = _form.find('select[name="provinceid"] option:selected').text(),
        city = _form.find('select[name="cityid"] option:selected').text(),
        dist = _form.find('select[name="distid"] option:selected').text(),
        community = _form.find('select[name="communityid"] option:selected').text();

    if(!checkMobileFormat(p)){
        alert('手机号码不正确，请重填！');
        newphone.focus();
        return;
    }

    if(jQuery("#newname").val().length > 0 && jQuery("#newaddress").val().length > 0 && p.length > 0){
        jQuery.ajax({
            "type":"POST",
            "url":"forum.php",
            "data":jQuery('#newaddressform').serialize()+'&province='+province+'&city='+city+'&dist='+dist+'&community='+community,
            "dataType":"json",
            "success":function(data){
                var id = data.uaid;
                if(id > 0){
                    var btn = jQuery('#address-add-btn');
                    var li = jQuery("<li></li>")
                        .attr({"data-id":id,'data-name':data.name,'data-address':data.address,'data-phone':data.phone})
                        .html('<div class="radio"><span id="address-select-sign-'+id+'" class="glyphicon glyphicon-ok-sign addresss_check"></span></div><div class="address-detail"><p class="namephone"><span class="name">'+data.name+'</span><span class="phone">'+data.phone+'</span></p><p class="address">'+data.address+'</p></div><div class="address-edit"><a style="display:none;" id="address-delete-btn-'+id+'" class="btn address-delete-btn" href="###" onclick="delAddress(this);return false;" data-content="'+id+'"><span class="glyphicon glyphicon-remove-circle"><span></a></div>');
                    li.insertBefore(btn);
                    clearAddressInput();
                    setDefalut(id,data.name,data.phone,data.address);
                    jQuery('#address-input').hide().siblings().show();
                    show_hide();
                }else{
                    alert("添加地址失败", 2000);
                }
            }
        });
    }else{
        alert("请填写完整的地址信息", 2000);
    }
}