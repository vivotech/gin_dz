//AJAX上传头像插件

(function($){
	
	$.fn.ajaxUploadAavatar = function(op){
		var self = this,
			options = {},
			//插件地址
			load_path = '/Public/js/community/pc/ajaxUploadAvatar/',
			contenter = $('<div></div>').attr('class','ajaxuploadAvatar');
		
		$('body').append(contenter);
		contenter.load(load_path+'ajaxUploadAvatar.html');
		
		//初始化
		this.init = function(){
			//加载css
			self.initOptions();
			self.setEvents();
		}
		
		//初始化参数
		this.initOptions = function(){
			
			
		}
		
		
		//绑定事件
		this.setEvents = function(){
			
			//插件自身弹出事件
			self.click(function(){
				$('#umask').fadeIn();
			});
			//关闭按钮
			contenter.on('click','#cancle-btn,#btn-userheadimg-cancle',function(){
				$('#umask').fadeOut();
				location.reload();
			});
			//保存图片
			contenter.on('click','#btn-userheadimg-sure',function(){
				var $save_form  = $('#save-avator-form');
				if($('#sourceLink').val() == ''){
					alert('请选上传图片');
					return;
				}
 				$.ajax({
			        "type":"POST",
			        "url":"/community/ajax/ajaxUpdateUserAvatar",
			        "data":$save_form.serialize(),
			        "dataType":"json",
			        "success":function(data){
			        	if(data.result == 1){
			      			//直接刷新页面
			      			location.reload();
			        	}else{
			        		vivoAlert("上传头像失败");
			        	}
			        }
			   });
			});
		}
		
		//启动
		self.init();
	}
	
})(jQuery);
