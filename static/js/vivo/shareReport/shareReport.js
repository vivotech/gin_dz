/**
 * script for report dialog  举报对话框
 * @author: xiaowen
 */

/**
 * jQuery reportDialog
 * @param $ jQuery
 */
(function ($)
{
  $.fn.reportDialog = function (inputOptions)
  {
    var self = this;

    if (!inputOptions.shareId)
    {
      return false;
    }

    // create a new dialog
    var dialog = $("<div>");
    dialog.attr("id", "reportDialog");
    dialog.attr("class", "dialog");
    dialog.appendTo(document.body);

    // init options
    var options = {
      shareId: 0,
      shareTitle: "",
      success: false
    };

    // set control path
    var url_reportDialog = "/Public/js/community/pc/shareReport";

    // load template
    var date = new Date();
    var url = url_reportDialog + "/shareReport.html" + "?time=" + date.getTime().toString();

    dialog.load(url, function ()
    {
      var share_id = Number(inputOptions.shareId);

      // get share info
      var url = VivoOptions.getOption("loadShareUrl");
      if (!url)
      {
        return;
      }

      // get share detail
      $.ajax(url, {
        "data": {
          "share_id": share_id,
          "pagesize": 1
        },
        "dataType": "json",
        "success": (function (result) {
          if (result.result == "1")
          {
            var share_data = result.data.share_list[0];

            // get dialog
            var dialog = $("#reportDialog");

            // output dialog content
            dialog.find(".share-item > .user-img-container > .user-img").attr("src", share_data.avatar);
            dialog.find(".share-item > .user-name-container > .user-name").html(share_data.user_name);
            dialog.find(".share-item > .share-title-container > .share-title").html(share_data.share_title);

            var list_image = dialog.find(".share-item > .img-list-container > .img-list");
            var media_list = $.parseJSON($("<div/>").html(share_data.media_list).text());

            var count_pic = 0;
            for (var i = 0; i < media_list.length; i++)
            {
              if (media_list[i].media_type == "0")
              {
                var item_image = $("<li/>").addClass("img-list-item");
                var image = $("<img/>").addClass("img").attr("src", media_list[i].media_url);
                item_image.append(image);
                list_image.append(item_image);
              }
            }

            dialog.find(".share-item > .share-info > .share-time").html(share_data.share_time);
            dialog.find(".share-item > .share-info > .from-source").html(share_data.from_source);

          }
        })
      });

      // set submit form event
      $("#reportDialogForm").submit(function () {
        var reportReason = $("#report_reason").val();
        if (reportReason === "")
        {
          $("#report_reason").focus();
          return;
        }

        $.ajax({
          "type": "POST",
          "url": VivoOptions.getOption("shareReportActionUrl"),
          "data": {
            "shareReportContent": reportReason,
            "shareId": share_id
          },
          "async": false,
          "success": function (data) {
            if (data > 0) {
              vivoAlert("举报成功，谢谢您的参与", 2000, "success");
            } else {
              vivoAlert("举报失败", 500, "fail");
            }
            self.destroyMyself();
          }
        });

        return false;
      });

      // set close button
      dialog.find(".dialog_buttons .closeButton").click(function () {
        $("#reportDialog").remove();
        $("#reportMask").remove();
      });

    });

    // get mask
    var mask = $("#reportMask");
    if (mask.length < 1)
    {
      mask = $("<div>").attr("id", "reportMask");
      mask.appendTo(document.body);
      mask.click(function () {
        self.destroyMyself();
      });
    }

    self.destroyMyself = function () {
      $("#reportDialog").remove();
      $("#reportMask").remove();
    };
    mask.show();

    // return
    return this;
  };
})(jQuery);

