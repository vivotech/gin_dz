$(function(){
	// 收藏动作
	$("body").on("click",".share-list>.share-list-item>.btn-list>.btn-list-item",function(){
		var isLogin = checkUserLogin();
		if(isLogin){
			var self = $(this),
				shareId = self.attr("data-content"),
				type = 2;

			$.ajax({
				"type":"POST",
				"url":"/community/ajax/changeCollection",
				"data":{
					"type":type,
					"shareId":shareId
				},
				"success":function(data){
					if(data == 2){
						self.parent().parent().fadeOut("300",function(){
							$(this).remove();
						});
					}else{
						vivoAlert("操作失败!返回:" + data);
					}
				}
			});
		}
	});
});