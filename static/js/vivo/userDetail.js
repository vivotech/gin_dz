$(function(){
	
	$('#submit-save-userdata').click(function(){
		
		var _url = '/community/ajax/ajaxUpdateUserPersonalInfo';
		var go_submit = true;
		//检查必填项目
		$('input').each(function(){
			var name = $(this).attr('name');
			var value= $(this).val();
			//用户名必填
			if(name == 'userName'){
				if(value == ''){
					vivoAlert('用户名不能为空！',1000);
					$(this).focus();
					go_submit = false;
				}
			}
		});
		
		if(go_submit == true){
			$.post(_url,$('#person-data').serialize(),function(data){
				var obj = jQuery.parseJSON(data);
				if(obj.result == 1){
					vivoAlert("修改用户个人信息成功",2000);
				}else{
					vivoAlert(obj.msg,2000);
				}
			});
		}
	});
	
	//标签切换
	$("#body-container>.main-container>.user-setting-container>.user-setting-navi>.user-setting-navi-item").click(function(){
		var _this = $(this);
		if(!_this.hasClass("active")){
			_this.siblings(".user-setting-navi-item").removeClass("active");
			_this.addClass("active");
			
			var containerName = _this.attr("data-content"),
				settingContainer = $("#body-container>.main-container>.user-setting-container>.user-setting");
			for(var i = 0; i < settingContainer.length; i++){
				var o = $(settingContainer[i]);
				if(o.hasClass(containerName)){
					o.addClass("active");
				}else{
					o.removeClass("active");
				}
			}
		}
	});
	
	//个人信息获取
	$("#body-container>.main-container>.user-setting-container>.user-setting>.edit-action.personal-info").click(function(){
		var inputContainer = $(this).siblings(".user-setting-table"),
			userName = inputContainer.find(".user-setting-input.user-name").val(),
			sex = 0,
			email = inputContainer.find(".user-setting-input.email").val(),
			mobile = inputContainer.find(".user-setting-input.mobile").val();
		
		inputContainer.find(".content>.sex").each(function(index){
			var _this = $(this);
			if(_this.prop("checked") == true){
				sex = _this.val();
			}
		});
		
		//检查输入
		
		//执行更新
		$.ajax({
			"type":"POST",
			"url":"/community/ajax/ajaxUpdateUserPersonalInfo",
			"data":{
				"userName":userName,
				"sex":sex,
				"email":email,
				"mobile":mobile
			},
			"dataType":"json",
			"success":function(data){
				if(data.result == 1){
					vivoAlert("修改用户个人信息成功",2000);
				}
			}
		})
	});
});