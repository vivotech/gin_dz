/**
 * actions for order page
 */

jQuery(document).ready(function(){
	jQuery('.orderList .returnRequest').click(function(){
		order_cancel_openid = jQuery(this).data('order-openid');
		dialog = jQuery( "#returnRequestConfirmDialog" ).dialog({
	      autoOpen: true,
	      height: 300,
	      width: 350,
	      buttons: [{
	      	'text': '确认',
	      	'click': function() {
	      		dialog.dialog( "close" );
	      		jQuery.ajax('/forum.php?mod=ajax&action=order_cancel', {
	      			'data' : {
	      				'order_openid': order_cancel_openid,
	      				'reason_type': jQuery('#returnReasonType').val(),
	      				'reason': jQuery('#returnReason').val()
	      			},
	      			'dataType': 'json',
	      			'success': function(data){
	      				if (data.result=='1') {
	      					showMask('returnRequestResultMask');
	      					vivoAlert(data.msg, 3000, '', function(){
		      					jQuery('#returnRequestResultMask').remove();
		      				});
	      					location.reload();
	      				}
	      				else {
	      					showMask('returnRequestResultMask');
	      					vivoAlert(data.msg, 3000, '', function(){
		      					jQuery('#returnRequestResultMask').remove();
		      				});
	      				}
	      			},
	      			'fail': function() {
	      				showMask('returnRequestResultMask');
	      				vivoAlert('连接超时，请稍后再试。', 3000, '', function(){
	      					jQuery('#returnRequestResultMask').remove();
	      				});
	      			}
	      		});
	      		jQuery('#returnRequestConfirmMask').remove();
	      	}
	      },{
	      	'text': '取消',
	      	'click': function() {
	      		dialog.dialog( "close" );
	      		jQuery('#returnRequestConfirmMask').remove();
	      	}
	      }],
	      close: function() {
	      	jQuery('#returnRequestConfirmMask').remove();
	      }
	    });
	    
	    showMask('returnRequestConfirmMask', function(){
	    	dialog.dialog( "close" );
	      	jQuery('#returnRequestConfirmMask').remove();
	    });
	});
	
});

