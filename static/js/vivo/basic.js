/************************************************ 以下为基础类  ******************************************************/
// 检查用户有否登陆,没有则默认弹出登陆,或者可以自己设定回调函数
function checkUserLogin(cb) {
	var self = $jq(this);

	self.Init = function() {
		return self.checkAction();
	}

	self.checkAction = function() {
		var res = true;
		$jq.ajax({
			"url": "/community/ajax/ajaxCheckLoginState",
			"dataType": "json",
			"async": false,
			"success": function(data) {
				var r = data.result;
				if (r != 1) {
					if (typeof(cb) === "function") {
						cb();
					} else {
						$jq(document).loginDialog({
							isAjax: true,
							success: function() {

							}
						});
					}
					res = false;
				}
			}
		});
		return res;
	}

	return self.Init();
}

/**
 * 自定义Alert窗口
 * @param msg  String  alert message  警告框内容
 * @param isAutoClose  Number  auto close delay time  自动关闭延时
 * @param type  String  window type value: success, fail  窗口类型
 */
function vivoAlert(msg, isAutoClose, type, cb) {

	var self = this;

	this.Init = (function() {
		self.alertWindowClassName = "alert-window";
		self.alertWindowHeaderClassName = "alert-window-header";
		self.alertWindowBodyClassName = "alert-window-body";
		self.closeBtnClassName = "alert-window-closebtn";

		self.InitData();
		self.InitContainer();
		self.InitPosition();
		self.InitEvent();
	});

	this.InitContainer = (function() {
		jQuery("div." + self.alertWindowClassName).remove();
		var alertWindow = $jq("<div></div>").addClass(self.alertWindowClassName).appendTo("body");
		self.alertWindow = alertWindow;

		//头部
		var window_header = $jq("<div></div>").attr({
			"class": self.alertWindowHeaderClassName
		}).appendTo(self.alertWindow);

		if (type) {
			alertWindow.addClass("alert-window-" + type); // special class
		}

		//内容区
		var window_body = $jq("<div></div>").attr({
			"class": self.alertWindowBodyClassName
		}).appendTo(self.alertWindow);
		self.alertWindowBody = self.alertWindow.find("." + self.alertWindowBodyClassName);
		if (type) // special icon
		{
			var window_icon = $jq("<div></div>").addClass("alert-window-icon").appendTo(window_body);
		}

		//文字内容
		$jq("<p></p>").addClass("alert-window-content").html(self.alertMsg).appendTo(self.alertWindowBody);

		// 关闭按钮
		var close_button = $jq("<button></button>").attr({
			"class": "alert-window-close",
			"type": "button"
		}).html("x").appendTo(window_header);
		close_button.click(function() {
			//var alert_window = $jq(this).parents(".alert-window");
			//alert_window.remove();
			self.destroyMyself();
		});
	});

	this.InitData = (function() {
		self.alertMsg = msg;
	});

	this.InitPosition = (function() {
		var w_w = $jq(window).width(),
			w_h = $jq(window).height(),
			t_w = self.alertWindow.width(),
			t_h = self.alertWindow.height(),
			t_x = (w_w - t_w) / 2,
			t_y = (w_h - t_h) / 2;
		t_x = (t_x > 0) ? t_x : 0;
		t_y = (t_y > 0) ? t_y : 0;
		self.alertWindow.css({
			"top": t_y + "px",
			"left": t_x + "px"
		});
	});

	this.InitEvent = function() {
		if (!isAutoClose) {
			//点击确定关闭
			self.alertWindow.find("." + self.closeBtnClassName).click(function() {
				self.destroyMyself();
			});
		} else {
			setTimeout(function() {
				self.destroyMyself();
			}, isAutoClose);
		}
	}

	this.destroyMyself = function() {
		self.alertWindow.fadeOut("500", function() {
			$jq(this).remove();
			if (typeof(cb) == 'function') {
				cb(this);
			}
		});
	}

	this.Init();
}

/**
 * 自定义confirm窗口
 * @param msg  String  alert message  选择框内容
 */
function vivoConfirm(options) {
	return confirm(options.message);
}

//通知浮窗
function noticeWindow(inputOptions) {
	var self = $jq(this),
		options = {},
		mainContainer = $jq("<div></div>");

	self.Init = function() {
		self.InitOptions();
		self.InitData();
		self.InitContainer();
	}

	self.InitOptions = function() {
		options.noticeList = (inputOptions.noticeList) ? inputOptions.noticeList : [{
			"content": "错误通知",
			"type": "0"
		}];
		options.behindObj = (inputOptions.behindObj) ? inputOptions.behindObj : "#header-container>.header>.user-info";
		options.shiftX = (inputOptions.shiftX) ? inputOptions.shiftX : 0;
		options.shiftY = (inputOptions.shiftY) ? inputOptions.shiftY : 19;
	}

	self.InitData = function() {
		self.exist = false;
		self.mainContainerId = "vivoNoticeWindow";
		self.noticeListClassName = "vivo-notice-list";
		self.noticeClassName = "vivo-notice-list-item";
	}

	self.InitContainer = function() {
		var existMainContainer = $jq("#" + self.mainContainerId);
		if (existMainContainer.length > 0) {
			mainContainer = existMainContainer;
			mainContainer.find("." + self.noticeListClassName).html("");
			self.exist = true;
		} else {
			mainContainer.attr({
				"id": self.mainContainerId
			}).css({
				"position": "fixed",
				"top": "0px",
				"left": "0px",
				"opacity": "0.97",
				"font-size": "12px"
			}).appendTo("body");

			$jq("<ul></ul>").attr({
				"class": self.noticeListClassName
			}).css({
				"padding": "10px 10px 10px 20px",
				"background-color": "#F6F4DF",
				"border": "1px solid #ffeebb",
				"border-radius": " 0 0 3px 3px",
				"box-shadow": "0px 2px 2px 0px rgba(0,0,0,0.1)"
			}).appendTo(mainContainer);
		}

		self.noticeList = mainContainer.find("." + self.noticeListClassName);

		var noticeList = options.noticeList;
		for (var i = 0; i < noticeList.length; i++) {
			var span = '<span class="remove"><i class="fa fa-remove"></i></span>',
				a = '';

			if (noticeList[i].type == 1) {
				a = '<a class="action" href="/community/User/userComment" target="_blank">查看评论</a>';
			} else if (noticeList[i].type == 2) {
				a = '<a class="action" href="#" target="_blank">查看好友</a>';
			}
			$jq("<li></li>").attr({
				"class": self.noticeClassName
			}).css({
				"display": "block",
				"color": "#383838",
				"padding": "4px 0px"
			}).html(span + noticeList[i].content + " ， " + a).appendTo(self.noticeList);
		}
		self.noticeList.find("." + self.noticeClassName + ">.action").css({
			"text-decoration": "none",
			"line-height": "20px",
			"color": "#eb7350",
			"margin": "0px 0px 0px 6px",
			"cursor": "pointer"
		});
		self.noticeList.find("." + self.noticeClassName + ">.remove").css({
			"float": "right",
			"padding": "0 5px 0 40px",
			"text-decoration": "none",
			"line-height": "20px",
			"font-size": "12px",
			"color": "#999",
			"cursor": "pointer"
		});

		self.InitPosition();
		self.InitEvent();
	}

	self.InitPosition = function() {
		var behindObj = $jq(options.behindObj);

		if (behindObj.length > 0) {
			o_x = behindObj.offset().left,
				o_y = behindObj.offset().top - $jq(window).scrollTop(),
				o_w = behindObj.width(),
				o_h = behindObj.height(),
				p_x = o_x + (o_w - mainContainer.width()) / 2 + options.shiftX,
				p_y = o_y + o_h + options.shiftY;

			mainContainer.css({
				"top": p_y + "px",
				"left": p_x + "px"
			});
		} else {
			vivoAlert("未找到可追加窗口的元素");
		}

	}

	self.InitEvent = function() {

		var lis = self.noticeList.find("." + self.noticeClassName);
		lis.each(function(index) {
			var _this = $jq(this);
			//点击关闭消失
			_this.find(".remove").unbind("click").click(function() {
				self._removeLi(_this);
			});

			//点击查看消失
			_this.find(".action").unbind("click").click(function() {
				self._removeLi(_this);
			});
		})
	}

	self._removeLi = function(obj) {
		$jq(obj).fadeOut("300", function() {
			$jq(this).remove();

			var lis = self.noticeList.find("." + self.noticeClassName);
			if (lis.length <= 0) {
				self.destroyMyself();
			}
		});
	}

	self.hideMyself = function(obj) {
		obj.hide("300");
	}

	self.showMyself = function(obj) {
		obj.show("300");
	}

	self.destroyMyself = function() {
		mainContainer.remove();
	}

	self.Init();
}

/**
 * @name 如果隐藏就显示，如果显示就隐藏
 * @param {Jquery Object} obj
 */
function triggerHideOrShow(obj) {
	if (obj.css("display") == "none") {
		obj.show();
	} else {
		obj.css("display", "none");
	}
}

/**
 * remind you have a new message  提醒你有新的消息
 */
function remindNewMessage(options) {
	var remind_window = $jq("#remindWindow");
	if (!remind_window.length) {
		remind_window = $jq("<div></div>");
		remind_window.attr({
			"id": "remindWindow",
			"class": "remind-window"
		});

		remind_window.append($jq('<div class="rhombus"><i class="rhombus_border"></i><i class="rhombus_background"></i></div>'));

		remind_window.append($jq('<ul class="list_message"></ul>'));

		/*
		var message = $jq("<p></p>");
		message.html('<a href="javascript:;">你有新的消息！</a>');
		remind_window.append(message);
		*/

		remind_window.appendTo($jq("#bellIconItem"));
	}

	var clean = options.clean ? options.clean : false;
	if (clean) {
		remind_window.find(".list_message").html("");
	}

	var noticeList = options.noticeList ? options.noticeList : new Array();

	for (var i = 0; i < noticeList.length; i++) {
		var message = $jq('<li class="item_message"></li>');
		if (noticeList[i].type == 1) {
			var a = '<a class="action" href="/community/User/userComment" target="_blank">查看评论</a>';
		} else if (noticeList[i].type == 2) {
			var a = '<a class="action" href="#" target="_blank">查看好友</a>';
		}

		message.html(noticeList[i].content + " ， " + a);

		remind_window.find(".list_message").append(message);
	}

	$jq("#bellIconItem").addClass("bell-active");

}


//自动获取通知
function autoLoadUserNotice(user_id, timeout) {
	var self = $jq(this),
		state = false;

	self.Init = function() {
		self.userId = (user_id) ? user_id : 0;
		self.timeout = (timeout) ? timeout : 30000;

		self.loadData();
	}

	self.loadData = function() {
		setInterval(function() {
			if (!state) {
				state = true;
				$jq.ajax({
					"type": "POST",
					"url": "/community/ajax/getUserNotice",
					"data": {
						"userId": self.userId
					},
					"dataType": "json",
					"success": function(data) {
						state = false;
						var noticeList = new Array();
						if (data.result == 1) {
							var r = data.data;
							if (r['comment_count'] > 0) {
								noticeList.push({
									"content": "你有" + r['comment_count'] + "条未读评论",
									"type": "1"
								});
								jQuery("#bellIconItem").addClass("bell-active");
							}
							if (r['message_count'] > 0) {
								noticeList.push({
									"content": "你有" + r['message_count'] + "条未读消息",
									"type": "2"
								});
								jQuery("#bellIconItem").addClass("bell-active");
							}

							if (noticeList.length > 0) {
								remindNewMessage({
									"noticeList": noticeList,
									"clean": true
								});
							}
						}
					}
				});
			}
		}, self.timeout);
	}

	self.Init();
}

// 页面元素重定位
function page_element_reposition() {
	// 右边固定快速按钮--定位
	var bodyContainer = $jq("#body-container"),
		w_h = $jq(window).height(),
		w_w = $jq(window).width(),
		minHeight = w_h;

	if (!bodyContainer.length) {
		return;
	}
	bodyContainer.css("min-height", minHeight + "px");

	var quickList = $jq("#page-quick-btn-list"),
		quickList_w = parseInt(quickList.width());
	quickList.css({
		"left": (w_w - quickList_w - 20) + "px",
		"bottom": 78 + "px",
		"opacity": 1
	});
}

//数秒器
(function($jq) {
	$jq.fn.countSec = function(inputOptions) {
		var self = this,
			options = {},
			sec = 0;

		this.Init = function() {
			options.sec = (inputOptions.sec) ? inputOptions.sec : 60;
			options.cb = (inputOptions.cb) ? inputOptions.cb : 60;
			sec = options.sec;
			self.content = self.html();

			self.start();
		}

		this.start = function() {
			self.count();
		}

		this.count = function() {
			if (sec > 0) {
				self.html(self.content + "(" + sec + ")");
				sec = sec - 1;
				setTimeout(function() {
					self.count();
				}, 1000);
			} else {
				self.html(self.content);
				if (typeof(options.cb) == "function") {
					options.cb(self);
				}
			}
		}

		this.Init();
	}
})(jQuery);

//倒计时器
(function($jq) {
	jQuery.fn.countTimer = function(inputOptions) {
		var self = this,
			options = {},
			sec = 0;

		self.Init = function() {
			options.beginTime = inputOptions.beginTime;
			options.endTime = inputOptions.endTime;
			options.ismobile = inputOptions.ismobile ? inputOptions.ismobile : 'web';
			self.InitEvent();
		}

		self.InitEvent = function() {
			sec = self.refreshTime();
			if (sec > 0) {
				setTimeout(function() {
					self.InitEvent();
				}, 1000);
			}
		}

		self.refreshTime = function() {
			var timeDis = options.endTime - options.beginTime;
			if (timeDis > 0) {
				if (options.ismobile != 'web') {
					self.html(self._getTimeDisString(timeDis));
				} else {
					self.css({
						"color": "blue"
					}).html(self._getTimeDisString(timeDis));
				}
			} else {
				if (options.endTime <= 0) {
					self.html("无限期!");
				} else {
					if (options.ismobile != 'web') {
						self.html("已结束!").css({
							'background-color': '#EDEDED',
							'color': '#999'
						});
					} else {
						self.css({
							"color": "red"
						}).html("已结束!");
					}
				}
			}
			options.beginTime++;
			return timeDis;
		}

		self._getTimeDisString = function(timestamp) {
			if (timestamp > 0) {
				if (options.ismobile == 'web') {
					var str = '还剩',
						leftTime = 0,
						days = 0,
						hours = 0,
						mins = 0,
						secs = 0;
					days = Math.floor(timestamp / 86400);
					leftTime = timestamp - (days * 86400);
					hours = Math.floor(leftTime / 3600);
					leftTime -= hours * 3600;
					mins = Math.floor(leftTime / 60);
					secs = leftTime - mins * 60;

					if (days > 0) {
						str += days + "天";
					}
					str += hours + "时";
					str += mins + "分";
					str += secs + "秒";
					return str;
				} else {
					var str = (options.ismobile == 'nodetail') ? '' : '剩余时间：',
						leftTime = 0,
						days = 0,
						hours = 0,
						mins = 0,
						secs = 0;
					days = Math.floor(timestamp / 86400);
					leftTime = timestamp - (days * 86400);
					hours = Math.floor(leftTime / 3600);
					leftTime -= hours * 3600;
					mins = Math.floor(leftTime / 60);
					secs = leftTime - mins * 60;

					if (days > 0) {
						str += days + "天 ";
					}
					str += hours + ":";
					str += mins + ":";
					str += secs + "";
					return str;
				}
			} else {
				return '';
			}

		}
		this.Init();
	}
})(jQuery);

//扩展函数，自动加载CSS/JS
$jq.extend({
	includePath: '',
	include: function(file) {
		var files = typeof file == "string" ? [file] : file;
		for (var i = 0; i < files.length; i++) {
			var name = files[i].replace(/^s|s$jq/g, "");
			var att = name.split('.');
			var ext = att[att.length - 1].toLowerCase();
			var isCSS = ext == "css";
			var tag = isCSS ? "link" : "script";
			var attr = isCSS ? " type='text/css' rel='stylesheet' " : " language='javascript' type='text/javascript' ";
			var link = (isCSS ? "href" : "src") + "='" + $jq.includePath + name + "'";
			if ($jq(tag + "[" + link + "]").length == 0) document.write("<" + tag + attr + link + "></" + tag + ">");
		}
	}
});


/**
 * show mask layer
 * @param id string  mask layer wrapper ID
 * @param eventClick function  mask click event
 */
function showMask(id, eventClick) {
	if (id) {
		var mask = jQuery("#" + id);
	} else {
		var mask = jQuery(".mask");
	}
	if (mask.length < 1) {
		mask = jQuery("<div>").attr("class", "mask");
		if (id) {
			mask.attr("id", id);
		}
		mask.appendTo(document.body);
		mask.click(eventClick);
	}
	mask.show();
}

/**
 * 在float的情况下根据最大块的高度来设置其他块居中
 * @param string/jQquery Selector  fatherOjb  需要调整的ID 或者 class 或者对象
 */
function fix_float_height(fatherOjb) {
	var $childrenOjb = jQuery(fatherOjb).children();
	var realChildren = new Array();
	var maxHeight = 0,
		maxObj = null;
	$childrenOjb.each(function(i, e) {
		var f = jQuery(e).css('float');
		//还调整有浮动的元素
		if (f == 'left' || f == 'right') {
			realChildren.push(e);
		}
		var h = parseInt(jQuery(e).height()) + parseInt(jQuery(e).css('padding-top')) + parseInt(jQuery(e).css('padding-bottom'));
		if (h > maxHeight) {
			maxHeight = h;
			maxObj = e;
		}
	});
	for (i = 0; i < realChildren.length; i++) {
		var $fixOjb = jQuery(realChildren[i]);
		$fixOjb.css({
			'height': maxHeight,
			'padding-top': (maxHeight - parseInt($fixOjb.height())) / 2
		});
	}
}

/**
 * console log text
 * @param text  String  log content
 */
function log(text) {
	if (window.console) {
		window.console.log(text);
	}
}

/**
 * 关注我们（微信）
 */
function followWeixin() {
	jQuery('#followWeixinDialog').fadeIn();
}

/************************************************ 以下为自动执行事件  ******************************************************/

$jq(document).ready(function() {


	$jq(window).resize(function() {

		// 页面元素重定位
		page_element_reposition();

		// 设置左侧导航高度
		$jq('#header-container .header').css('height', jQuery(window).height());

	});
	$jq(window).trigger('resize');

	// 左边导航条滚动动作
	$jq('#header-container .header').on('mousewheel', function(){
		scroll_top = $jq(this).scrollTop();
		scroll_top += event.deltaY;
		this.scrollTop = scroll_top;
	});

	// 左边导航栏点击收缩
	jQuery("#header-container>.header>.navi-title.forums").click(function() {
		var _this = jQuery(this),
			list = _this.next(".navi.forums");
		triggerHideOrShow(list);
	});
	jQuery("#header-container>.header>.user-info>.username").click(function() {
		var _this = jQuery(this),
			list = _this.siblings(".user-quick-list");
		triggerHideOrShow(list);
	});

	// 表单单选项
	$jq("body").on("click", ".ulcheckbox-form-control>li", function() {
		var self = $jq(this),
			isactive = self.hasClass("active"),
			ul = self.parent(),
			data_target = $jq("#" + ul.attr("data-target"));

		if (isactive) {
			self.removeClass("active");
		} else {
			self.addClass("active");
		}

		//刷新隐藏的input值
		data_target.val("");
		var lis = ul.find("li");

		for (var i = 0; i < lis.length; i++) {
			var li = $jq(lis[i]);
			if (li.hasClass("active")) {
				data_target.val(data_target.val() + li.attr("data-value") + "|");
			}
		}

		if (data_target.val().substr(-1, 1) == '|') {
			var str = data_target.val();
			data_target.val(str.substr(0, str.length - 1));
		}
	});

	// 右边固定快速按钮--回到顶部
	$jq("#page-quick-btn-list>.btn-item.goto-top").click(function() {
		var _this = $jq(this),
			top = $jq(window).scrollTop(),
			timeout = 200,
			times = 20,
			pre_width = top / times,
			pre_time = timeout / times;

		_this.scrollToTop = function() {
			if (top != 0) {
				top = $jq(window).scrollTop();
				$jq(window).scrollTop(top - pre_width);
				setTimeout(function() {
					_this.scrollToTop();
				}, pre_time);
			}
		}

		_this.start = function() {
			_this.scrollToTop();
		}

		_this.start();
	});

	// 右边消息按钮
	$jq(".page-quick-btn-list .bell .icon").click(function() {
		var item = $jq(this).parent();
		if (item.hasClass('bell-active')) {
			item.removeClass('bell-active');
			$jq("#remindWindow").hide();
		} else {
			item.addClass('bell-active');
			$jq("#remindWindow").show();
		}
	});

	// login action 登录对话框
	jQuery("#loginOperation").click(function() {
		var referer = encodeURIComponent(window.location.pathname + window.location.search);
		showWindow('login', 'member.php?mod=logging&action=login&referer=' + referer, 'get', 1, '', function() {

			var body_width = jQuery(document.body).width();
			var window_height = jQuery(window).height();
			var body_scroll_top = jQuery(window).scrollTop();

			var dialog_width = jQuery('#fwin_login').width();
			var dialog_height = jQuery('#fwin_login').height();

			/*
			jQuery('#fwin_login').css({
				'left': (body_width - dialog_width) / 2 + 'px',
				'top': ((window_height - dialog_height) / 2 + body_scroll_top) + 'px'
			});*/

			showMask("loginMask", function() {
				jQuery("#fwin_login").remove();
				jQuery(".mask").hide();
				jQuery(".waterfall-container>.box>.thread-container>.mask-image>.thread-image").removeClass("filter");
			});

			jQuery(".waterfall-container>.box>.thread-container>.mask-image>.thread-image").addClass("filter");

			jQuery("#fwin_login .regLink").click(function() {
				jQuery('#registerDialog').fadeIn();
				showMask('registerDialogMask', function() {
					jQuery('#registerDialog').hide();
					jQuery('#registerDialogMask').hide();
				});
				return false;
			});

			jQuery('.loginForm .column2 label').click(function() {
				jQuery(this).parent().find('input').focus();
				jQuery(this).hide();
			});

			jQuery('.loginForm input').focus(function() {
				jQuery(this).parent().find('label').hide();
			});

		});
		return false;
	});

	// 对话框关闭按钮
	jQuery('.dialog_close').click(function() {
		var dialog = jQuery(this).parents('.dialog');
		var dialog_id = dialog.attr('id');
		if (dialog_id) {
			jQuery('#' + dialog_id + 'Mask').hide(); // 隐藏遮罩层
		}
		dialog.hide();
	});

	// 注册表单提交
	jQuery('#registerform').submit(function() {

		//var url_register = '/forum.php?mod=ajax&action=register';
		var url_register = '/app.php?mod=member&act=register';

		// 检查手机号码
		var input_mobile = jQuery(this).find('input[name=mobile]');
		var mobile_value = input_mobile.val();
		if (mobile_value.length != 11) {
			input_mobile.focus();
			return false;
		}

		// 检查密码
		var input_password = jQuery(this).find('input[name=password]');
		var password_value = input_password.val();

		jQuery.ajax(url_register, {
			'type': 'post',
			'dataType': 'json',
			'data': {
				'input': mobile_value,
				'type': 'mobile',
				'password': password_value
			},
			'success': function(result) {
				if (result.result == '1') {
					location.reload();
				} else {
					alert(result.msg);
				}
			}
		})
		return false;
	});

	// 分享按钮(微博)
	jQuery('.share-btn.weibo').click(function() {
		shareWeibo();
	});

	// 分享按钮(QQ空间)
	jQuery('.share-btn.qqzone').click(function() {
		shareQQzone();
	});

	// 分享按钮(微信)
	jQuery('.share-btn.wechat').click(function() {
		shareWechat();
	});

	// 草稿箱按钮在发帖页
	jQuery("#draftlist").click(function() {
		var parent = jQuery(this).parent();
		var menu = parent.children(".menu");
		menu.toggle();
	});

	// check keyboard input
	document.body.onkeydown = (function(event) {
		// get key code
		var keyCode = 0;
		try {
			keyCode = event.keyCode; // standard
		} catch (e) {
			try {
				keyCode = event.which; // IE
			} catch (e) {}
		}

		// not allow press Backspace key  不允许按退格键
		if (keyCode === 8) {
			var element_name = event.target.nodeName;

			// allow input & textarea element
			if (element_name === "INPUT" || element_name === "TEXTAREA") {
				return true;
			}

			return false;
		}

		return true;
	});

	/* 编辑按钮 */
	$jq('.share-list-item').mouseover(function() {
		$jq(this).find('.author-edit-btn a').stop().fadeIn();
	});
	$jq('.share-list-item').mouseout(function() {
		$jq(this).find('.author-edit-btn a').stop().fadeOut();
	});



});