// 移除写帖按钮
var userInfoContainer = $("#header-container>.header>.user-info");
userInfoContainer.find(".user-info-btn[title=\"发帖\"]").css("display","none");
userInfoContainer.find(".username").css("margin","0px 0px 0px 65px");
$("#body-container>.page-quick-btn-list>.goto-shareadd").css("display","none");

var ue = UE.getEditor('share_content',{
	  toolbars:[[
            'insertimage','|','undo', 'redo', '|',
            'fontsize', 'indent', '|',
            'horizontal', 'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'blockquote', 'pasteplain', '|', 
		    'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', '|',
            'justifyleft', 'justifycenter', 'justifyright', '|', 
            'preview', 'drafts'
        ]]
		,saveInterval: 30000
		,elementPathEnabled : false
});

function getKeyword(editor){
	var article = editor.getContentTxt();
	$.ajax({
		type	:	"POST",
		url		:	"/community/share/getkeyword",
		data	:	{
			"article"	:	article
		},
		dataType:	"json",
		success	:	function(data){
			var wordlist = data,
				list = $("#key_word_list");

			$("#key_word").val("");
			list.html("");

			for(var i = 0; i < wordlist.length ; i++){
				var li = "<li data-value=\"" + wordlist[i] + "\">" + wordlist[i] + "</li>";
				$(li).appendTo(list);
			}
		}
	})
}

$(function(){
	$("body").on("click","#gettags",function(){
		getKeyword(ue);
	});
	$("#preview-action").click(function(){
		var editContent = ue.getContent(),
			editTitle = $("#share_title").val();
		
		sharePreview({
			"title":editTitle,
			"content":editContent
		})
	});
	$("#share_title").keypress(function(e){
		if(e.keyCode == 13){
			return false;
		}
	});
	$("#shareAddForm").submit(function(){
		// 验证标题
		var shareTitle = $("#share_title").val();
		if(shareTitle.length <= 0){
			vivoAlert("请填写标题");
			return false;
		}
		
	})
});