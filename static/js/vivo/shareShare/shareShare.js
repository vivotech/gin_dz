(function($jq){
	
	$jq.fn.shareShare = function(inputOptions){
		var self = this,
			mainContainer = $jq("<div></div>").attr("class","share-share-container").html("载入中.."),
			options = {};
		
		this.Init = function(){
			options.display = (inputOptions.display)?inputOptions.display : "block";
			//分享设置
			options.sharetitle = (inputOptions.sharetitle)? '【意大利原生活】@意大利原生活  ' +inputOptions.sharetitle : '@意大利原生活';
			options.share_url = (inputOptions.share_url)? inputOptions.share_url : window.SITEURL;
			options.share_picurl = (inputOptions.share_picurl)?inputOptions.share_picurl : '';
			//检查图片链接
			if(options.share_picurl.indexOf('http')<0){
				options.share_picurl = window.SITEURL + options.share_picurl;
			}
			//新浪微博APPID
			options.weibo_appkey = '2453450409';
			self.InitContainer();
		}
		
		this.InitContainer = function(){
			mainContainer.css({
				"position":"absolute",
				"display":options.display,
				"top":"0px",
				"left":"0px",
			}).appendTo("body");
			
			mainContainer.load('/static/js/vivo/shareShare/shareShare.html');
			
			self.InitEvent();
			self.shareWeibo();
			self.shareQQzone();
		}
		
		this.InitPosition = function(){
			var t_x = self.offset().left,
				t_y = self.offset().top,
				t_w = self.width() + parseInt(self.css("padding-left")) + parseInt(self.css("padding-right")),
				t_h = self.height() + parseInt(self.css("padding-top")) + parseInt(self.css("padding-bottom")),
				p_w = mainContainer.width() + parseInt(mainContainer.css("padding-left")) + parseInt(mainContainer.css("padding-right")),
				p_h = mainContainer.height() + parseInt(mainContainer.css("padding-top")) + parseInt(mainContainer.css("padding-bottom")),
				p_x = t_x  + t_w / 2 - p_w / 2,
				p_y = t_y + t_h;
			
			mainContainer.css({
				"top":p_y + "px",
				"left":p_x + "px",
				'z-index': '1'
			});
		}
		
		this.InitEvent = function(){
			self.click(function(){
				mainContainer.fadeToggle();
				self.InitPosition();
			});
			mainContainer.on('click','#cancle-btn',function(){
				$jq('.weixin_qcode_mask').fadeOut();
			});
			var qrcode_php = window.SITEURL + 'qrcode.php';
			mainContainer.on('click','#button_weixin',function(){
				$jq('.weixin_qcode_mask').find('.weixin_qcode_box img.qrcode').attr({'src':qrcode_php+'?data='+encodeURIComponent(options.share_url),'title':options.share_url});
				$jq('.weixin_qcode_mask').fadeIn();
			});
		}
		
		//微博分享按钮
		this.shareWeibo = function(){
			mainContainer.on('click','#button_tsina',function(){
				window.open('http://service.weibo.com/share/share.php?appkey='+options.weibo_appkey+'&title='+encodeURIComponent(options.sharetitle)+'&url='+encodeURIComponent(options.share_url)+'&pic='+encodeURIComponent(options.share_picurl)+'&searchPic=false&style=simple');
			});
		}
		
		//分享到QQ空间
		this.shareQQzone = function(){
			mainContainer.on('click','#button_qzone',function(){
				//生成短链接
		 		$jq.getJSON(
		             '/shorturl.php?type=s&url='+encodeURIComponent(options.share_url),
		             function(data){
//		             	alert(data.result);
		                 var shareqqzonestring='http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?summary='+encodeURIComponent(options.sharetitle)+'&url='+encodeURIComponent(data.msg)+'&pics='+encodeURIComponent(options.share_picurl);
						 window.open(shareqqzonestring,'_blank');
		             }
		        );
			});
		}
		
		self.Init();
	}
	
})(jQuery);

function shareWeibo() {
	
	var appkey	= '2453450409';
	var share_title	= '@意大利原生活 ' + document.title;
	var share_url = window.SITEURL;
	var share_pic = ''
	
	var images = jQuery('.main-container .share-content img');
	if (images.length && images[0].src) {
		share_pic = images[0].src;
	}
	
	//检查图片链接
	if(share_pic.indexOf('http')<0){
		share_pic = location.origin + share_pic;
	}
	
	window.open('http://service.weibo.com/share/share.php?appkey='+appkey+'&title='+encodeURIComponent(share_title)+'&url='+encodeURIComponent(share_url)+'&pic='+encodeURIComponent(share_pic)+'&searchPic=false&style=simple');
}

function shareQQzone() {
	
	var appkey	= '';
	var share_title	= '@意大利原生活 ' + document.title;
	var share_url = window.SITEURL;
	var share_pic = ''
	
	var images = jQuery('.main-container .share-content img');
	if (images.length && images[0].src) {
		share_pic = images[0].src;
	}
	
	//检查图片链接
	if(share_pic.indexOf('http')<0){
		share_pic = location.origin + share_pic;
	}
	
	$jq.getJSON('/shorturl.php?type=s&url='+encodeURIComponent(share_url),
		function(data){
			var shareqqzonestring='http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?summary='+encodeURIComponent(share_title)+'&url='+encodeURIComponent(data.msg)+'&pics='+encodeURIComponent(share_pic);
			window.open(shareqqzonestring,'_blank');
		}
	);
}

function shareWechat() {
	
	var qrcodeBox = jQuery('#qrcodeDialog');
	
	// 如果没有二维码窗口则创建一个
	if ( !qrcodeBox.length ) {
		
		qrcodeBox = jQuery('<div></div>').attr('id', 'qrcodeDialog').addClass('dialog').appendTo(document.body);
		
		var share_url = window.SITEURL;
		qrcodeBox.append(jQuery('<img />').attr('src', '/qrcode.php?data=' + share_url));
		qrcodeBox.click(function(){
			jQuery(this).fadeOut();
		});
	}
	
	// 显示二维码
	qrcodeBox.fadeIn();

    showMask('qrcode_mask', function(){
        qrcodeBox.fadeOut();
        jQuery('#qrcode_mask').hide();
    });
	
}

