//区域选择 4级联动JS

	var region = new Object();
	region.loadRegions = function(couid, proid, cityid, couval, proval , cityval)
	{
		couval = couval||-1;
		proval = proval||-1;
		cityval = cityval||-1;
		var $country = $('#'+couid);
		var $province = $('#'+proid);
		var $city = $('#'+cityid);
	  	var jsondata;
	  	
	  	region.setDefaults($country,'省份');
	  	region.setDefaults($province,'城市');
	  	region.setDefaults($city,'区域');
	  	
	  	region.init(couid, proid, cityid,couval, proval ,cityval);
	  	
	  	$country.change(function(){
	  		var selValue = $(this).val();
	  		$province.empty();
	  		region.setDefaults($province,'城市');
	  		region.getProvince(selValue,0,proid);
	  		//当重新选择省份的时候，默认将区域清除
	  		$city.empty();
	  		region.setDefaults($city,'区域');
	  	});
	  	
	  	$province.change(function(){
	  		var selValue = $(this).val();
	  		$city.empty();
	  		region.setDefaults($city,'区域');
	  		region.getCity(selValue,0,cityid);
	  	});
	}
	
	//设置默认
	region.setDefaults = function(obj,name){
	  	var optionDefault = "<option value='0'>请选择"+name+"</option>";
	  	obj.append(optionDefault);
	}
	
	//初始化
	region.init = function(couid, proid, cityid, couval, proval , cityval){
		$.getJSON(region.getFileName(), function(data){
	  		$.each(data,function(i,v){
	  			//设置默认
	  			if(couval!=-1 && v['id'] == couval){
	  				$('#'+couid).append("<option selected='selected' value='"+v['id']+"'>"+ v['text'] +"</option>");
	  			}else{
	  				$('#'+couid).append("<option value='"+v['id']+"'>"+ v['text'] +"</option>");
	  			}
	  		});
	  	});
	  	
	  	//如果已经选择过省份
	  	if(proval!=-1){
	  		region.getProvince(couval,proval,proid);
	  	}
	  	if(cityval!=-1){
	  		region.getCity(proval,cityval,cityid);
	  	}
	}
	
	//获取省份
	region.getProvince = function(t,d,id){
		t = t || -1;
		d = d || -1;
		$.getJSON(region.getFileName(), function(data){
		  	$.each(data,function(i,v){
//		  		console.log(v['id']);
		  		if(v['id'] == t || t == -1){
		  			$.each(v['children'], function(ii,vv) {
		  				if(d!=-1 && vv['id'] == d){
		  					$('#'+id).append("<option selected='selected' value='"+vv['id']+"'>"+ vv['text'] +"</option>");
		  				}else{
		  					$('#'+id).append("<option value='"+vv['id']+"'>"+ vv['text'] +"</option>");
		  				}
              
		  			});
		  		}
		  	});
	    });
	}
	
	//获取城市
	region.getCity = function(t,d,id){
		t = t || -1;
		d = d || -1;
		$.getJSON(region.getFileName(), function(data){
		  	$.each(data,function(i,v){
		  		$.each(v['children'], function(ii,vv) {
		  			if(vv['id'] == t || t == -1){
			  			$.each(vv['children'], function(iii,vvv) {
			  				if(d!=-1 && vvv['id'] == d){
		  						$('#'+id).append("<option selected='selected' value='"+vvv['id']+"'>"+ vvv['text'] +"</option>");
			  				}else{
		  						$('#'+id).append("<option value='"+vvv['id']+"'>"+ vvv['text'] +"</option>");
		  					}
		  				});
		  			}
		  		});
		  	});
	    });
	}
	
	region.getFileName = function()
	{
	    return "/Public/ProvinceCityArea.json";
	}
