/**
 * 进一步完善你的信息 对话框
 */

/**
 * jQuery completeDataDialog
 * @param $ jQuery
 */
(function($)
{
  $.fn.completeDataDialog = (function(inputOptions)
  {
    var dialog = $("#completeDataDialog");
    
    // create dialog if not exist
    if (dialog.length < 1)
    {
      dialog = $("<div>");
      dialog.attr("id", "completeDataDialog");
      dialog.attr("class", "dialog");
      dialog.appendTo(document.body);
    }
    
    // path
    var url_completeDataDialog = "/Public/js/community/pc/completeDataDialog";
    
    // data
    var data = new Date();
    
    // load
    var url = url_completeDataDialog + "/completeDataDialog.html" + "?time=" + data.getTime().toString();
    dialog.load(url, function()
    {
      // set close button
      dialog.find(".dialog_buttons .closeButton").click(function(){
        $("#completeDataDialog").remove();
        $("#completeDataMask").remove();
      });
      
      // get user personal info
      $.ajax("/community/ajax/ajaxGetUserPersonalInfo", {
        "dataType": "json",
        "success": (function(result){
          if (result.result == "1")
          {
            $('#completeDataDialog input[name="userName"]').val(result.data.user_name);
          }
        })
      });
      
      // set form action
      dialog.find("form").submit(function(){
        var url = "/community/ajax/ajaxUpdateUserPersonalInfo";
        $.ajax(url, {
          "type": "post",
          "dataType": "json",
          "data": $(this).serialize(),
          "success": (function(result){
            if (result.result == 1)
            {
              // remove complete data dialog
              $("#completeDataDialog").remove();
              $("#completeDataMask").remove();
              
              vivoAlert("更新成功", 2000, "success");
              window.setInterval(function(){
                location.reload();
              }, 1000);
            }
            else
            {
              var error_message = VivoOptions.getErrMsg(result.result);
              vivoAlert(error_message, 3000, "fail");
            }
          })
        });
        
        return false;
      });
      
    });
    
    // get mask
    var mask = $("#completeDataMask");
    if (mask.length < 1)
    {
      mask = $("<div>").attr("id", "completeDataMask");
      mask.appendTo(document.body);
      mask.click(function(){
    	  $("#completeDataDialog").remove();
        $("#completeDataMask").remove();
      });
    }
    
    self.destroyMyself = function(){
      $("#completeDataDialog").remove();
      $("#completeDataMask").remove();
    };
    mask.show();
    
    // return
    return this;
    
  });

})(jQuery);

