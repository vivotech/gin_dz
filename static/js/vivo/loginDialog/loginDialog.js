/**
 * script for login dialog  登录对话框
 * @author: xiaowen
 */

/**
 * jQuery loginDialog
 * @param $ jQuery
 */
(function ($)
{
  $.fn.loginDialog = function (inputOptions)
  {
    var self = this;
    // get dialog
    var dialog = $("#loginDialog");

    // init options
    var options = {
      isAjax: false,
      success: false
    };
    if (inputOptions) {
      options.isAjax = (inputOptions.isAjax) ? inputOptions.isAjax : false;
      options.success = (inputOptions.success) ? inputOptions.success : false;
    }

    // create dialog if not exist
    if (dialog.length < 1)
    {
      dialog = $("<div>");
      dialog.attr("id", "loginDialog");
      dialog.attr("class", "dialog");
      dialog.appendTo(document.body);
    }

    // path
    var url_loginDialog = "/Public/js/community/pc/loginDialog";

    // data
    var data = new Date();

    // load
    var url = url_loginDialog + "/loginDialog.html" + "?time=" + data.getTime().toString();
    dialog.load(url, function ()
    {
      // get dialog
      var dialog = $("#loginDialog");

      // check placeholder
      dialog.find('input.form_control').each(function () {
        if (!('placeholder' in document.createElement('input')))
        {
          if (this.type === "text")
          {
            $(this).val($(this).attr("placeholder")).focus(function () {
              if (this.value === $(this).attr("placeholder"))
              {
                this.value = "";
              }
            });
          }
        }
      });

      if (!options.isAjax)
      {
        // set action
        dialog.find("form").attr("action", VivoOptions.getOption("actionLogin"));

        // set submit
        dialog.find("form").submit(function ()
        {
          var loginname = $("#loginDialogLoginName").val(),
                  pwd = $("#loginDialogPwd").val();

          // check data
          if (loginname === "")
          {
            $("#loginDialogLoginName").focus();
            return false;
          }
          if (pwd === "")
          {
            $("#loginDialogPwd").focus();
            return false;
          }
        });
      }
      else
      {
        // set submit form event
        $("#loginDialogForm").submit(function () {
          var loginname = $("#loginDialogLoginName").val(),
                  pwd = $("#loginDialogPwd").val();

          // check data
          if (loginname === "")
          {
            $("#loginDialogLoginName").focus();
            return false;
          }
          if (pwd === "")
          {
            $("#loginDialogPwd").focus();
            return false;
          }

          $.ajax({
            "type": "POST",
            "url": VivoOptions.getOption("actionLogin"),
            "data": {
              "loginname": loginname,
              "pwd": pwd,
              "is_ajax": 1
            },
            "async": false,
            "dataType": "json",
            "success": function (data) {
              var r = data.result,
                      u = data.data;
              if (r == 20003)
              {
                return false;
              }

              if (r == 1) {
                $("#header-container>.header>.user-info").html('<button class="user-info-btn" title="发帖" onclick="location.href=\'/community/share/shareadd\'"><i class="fa fa-edit"></i></button><div class="user-img-container"><img class="user-img" src="' + u['avatar'] + '"></div><p class="username" id="active_myMenu">' + u['user_name'] + '<i class="fa fa-caret-down icon-down"></i></p>');
                vivoAlert("登陆成功", 500, "success");
                $("#loginDialog").remove();
                $("#loginMask").remove();
              }
              else
              {
                var error_message = VivoOptions.getErrMsg(r);
                vivoAlert(error_message, false, "fail");
                $("#vivo-alert-window").css("z-index", "902");
              }
            }
          });
          return false;
        });
      }

      // set close button
      dialog.find(".dialog_buttons .closeButton").click(function () {
        $("#loginDialog").remove();
        $("#loginMask").remove();
      });

      dialog.find(".link_register").click(function ()
      {
        $(document).registerDialog();
        self.destroyMyself();
      });
    });

    // get mask
    var mask = $("#loginMask");
    if (mask.length < 1)
    {
      mask = $("<div>").attr("id", "loginMask");
      mask.appendTo(document.body);
      mask.click(function () {
        self.destroyMyself();
      });
    }

    self.destroyMyself = function () {
      $("#loginDialog").remove();
      $("#loginMask").remove();
    };
    mask.show();

    // return
    return this;
  };
})(jQuery);
