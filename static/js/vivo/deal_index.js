$(document).ready(function() {
	
		
	// 最近7天分成额的柱形统计图
	function renderChart() {
		
		var bodyWidth = $(document.body).width();
		
		// iPad 以上使用柱形+折线图
		if (bodyWidth >= 768) {
			
			// 设置展示7天分成的柱形图数据
			var chart_column_2d_data = new Array();
			var chart_column_2d_labels = new Array();
			var max_rebate = 0;
			for (var k in chart_rebate_7_day_data) {
				
				// 设置柱数据
				chart_column_2d_data.push({
					name: k.substr(-5, 5),
					value: chart_rebate_7_day_data[k],
					color: '#f39c12'
				});
				
				// 设置标记文本
				chart_column_2d_labels.push(k.substr(-5, 5));
				
				// 检查最大值
				if ( chart_rebate_7_day_data[k] > max_rebate ) {
					max_rebate = chart_rebate_7_day_data[k];
				}
			}
			
			// 设置分成额的基数
			var unit_rebate = '元';
			var ratio_rebate = 1;
			
			// 最大值大于1万时使用万元作单位
			if ( max_rebate - 10000 > 0 ) {
				unit_rebate = '万元';
				alert(unit_rebate);
				ratio_rebate = 0.0001;
			}
			
			// 设置展示7天扫码量的折线图
			var chart_line_2d_data = new Array();
			var max_scan_qr = 0;
			for (var k in chart_scan_qr_7_day_data) {
				
				chart_line_2d_data.push(chart_scan_qr_7_day_data[k]);
				
				// 设置最大值
				if ( chart_scan_qr_7_day_data[k] > max_scan_qr ) {
					max_scan_qr = chart_scan_qr_7_day_data[k];
				}
			}
			
			var chart = new iChart.Column2D({
				render : 'sevenDayRebateChart',
				data : chart_column_2d_data,
				
				width : $("#sevenDayRebateChart").width(),
				height : $("#sevenDayRebateChart").height(),
		
				border: false,
		
				column_width : 62,
				sub_option : {
					label : {
						fontsize: 11,
						fontweight: 600,
						color : '#f39c12',
						offsety: -30
					}
				},
				coordinate : {
					//background_color : null,
					//grid_color : '#c0c0c0',
					width : $("#sevenDayRebateChart").width() -100,
					height: $("#sevenDayRebateChart").height() -100,
					axis : {
						color : '#c0d0e0',
						width : [0, 0, 1, 0]
					},
					scale : [{
						position : 'left',
						scale_share: 10,
						label : {
							fontsize:11,
							fontweight:600,
							color : '#666'
						}
					},{
						position: 'right',
						start_scale:0,
						end_scale: max_scan_qr < 100 ? 100 : Math.ceil(max_scan_qr / (max_scan_qr.toString().length * 10) ) * max_scan_qr.toString().length * 10 * 2 ,
						scale_share: 10,
						scale_enable : false,
					}]
				}
			});
			
			//构造折线图（扫码量）
			var line = new iChart.LineBasic2D({
				z_index:1000,
				data: [{
		    		name : '',
		    		value: chart_line_2d_data,
		    		color:'#605ca8',
		    		line_width:5
			    }],
				label:{
					color:'#605ca8'
				},
				point_space: chart.get('column_width')+chart.get('column_space'),
				scaleAlign : 'right',
				sub_option : {
					label : {
						fontsize:11,
						fontweight:600,
						color : '#605ca8'
					},
					point_size:22,
				},
				coordinate:chart.coo,//共用坐标系
			});
			
			chart.plugin(line);
			
			//利用自定义组件构造左侧说明文本
			chart.plugin(new iChart.Custom({
				drawFn:function(){
					//计算位置
					var coo = chart.getCoordinate(),
						x = coo.get('originx'),
						y = coo.get('originy');
						
					//在左上侧的位置，渲染一个单位的文字
					chart.target.textAlign('start')
					.textBaseline('bottom')
					.textFont('15px')
					.fillText('最近7天的分成总额（单位：'+unit_rebate+'）',x-30,y-20,false,'#f39c12');
					//.textFont('600 11px Verdana')
					//.fillText('in millions',x-20,y-10,false,'#c52120');
					
					//在右上侧的位置，渲染一个单位的文字
					chart.target.textAlign('end')
					.textBaseline('bottom')
					.textFont('16px')
					.fillText('最近7天的扫码量',x+20+coo.width,y-20,false,'#605ca8');
					//.textFont('600 11px Verdana')
					//.fillText('in thousands',x+20+coo.width,y-10,false,'#34a1d9');
					
				}
			}));
			chart.draw();
			chart.eventOff();
		}
		// 超小屏幕使用条形图
		else {
			
			var chart_labels = new Array();
			var chart_data_scan = new Array();
			var chart_data_rebate = new Array();
			
			for (var k in chart_scan_qr_7_day_data) {
				chart_labels[chart_labels.length] = k.substr(-5, 5);
				chart_data_scan[chart_data_scan.length] = chart_scan_qr_7_day_data[k];
			}
			
			for (var k in chart_rebate_7_day_data) {
				chart_data_rebate[chart_data_rebate.length] = chart_rebate_7_day_data[k];
			}
			
			// 创建条形统计图
			var chart = new iChart.BarMulti2D({
				render : 'sevenDayRebateChart',
				data: [{
		         		name : '分成额',
		         		value: chart_data_rebate,
		         		color:'#f39c12'
		        },{
		         		name : '扫码量',
		         		value: chart_data_scan,
		         		color:'#605ca8'
		        }],
		        border: null,	// 不要边框
				labels: chart_labels,
				width : $("#sevenDayRebateChart").width(),
				height : $("#sevenDayRebateChart").height(),
				background_color : '#fff',
				legend:{
					enable:true,
					background_color : null,
					border : {
						enable : false
					},
					valign: 'top',
					align: 'right',
					offsetx: 20,
					offsety: -20
					
				},
				coordinate:{
					scale:[{
						 position:'bottom',	
						 start_scale:0
					}],
					background_color : null,
					axis : {
						width : 0
					},
					// 外边据各30px
					width: $("#sevenDayRebateChart").width() - 100,
					height: $("#sevenDayRebateChart").height() - 60,
					offsety: 10
				}
			});
			chart.draw();
			
			// 禁用事件响应
			chart.eventOff();
		}
	}
	
	$(window).on('resize', function(){
		renderChart();
	});
	renderChart();
	
});
