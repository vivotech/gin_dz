/**
 * script for reset password dialog  重设密码对话框
 * @author: xiaowen
 */

/**
 * jQuery loginDialog
 * @param $ jQuery
 */
(function($)
{
  $.fn.resetPasswordDialog = (function(inputOptions)
  {
	  var self = this;
    // get dialog
    var dialog = $("#resetPasswordDialog");
    
    // create dialog if not exist
    if (dialog.length < 1)
    {
      dialog = $("<div>");
      dialog.attr("id", "resetPasswordDialog");
      dialog.attr("class", "dialog");
      dialog.appendTo(document.body);
    }
    
    // path
    var url_resetPasswordDialog = "/Public/js/community/pc/resetPasswordDialog";
    
    // data
    var data = new Date();
    
    // load
    var url = url_resetPasswordDialog + "/resetPasswordDialog.html" + "?time=" + data.getTime().toString();
    
    dialog.load(url, function()
    {
      // get dialog
      var dialog = $("#resetPasswordDialog");
      
      dialog.find("form").submit(function()
      {
        // get input
        var passwordInputs = $(this).find('input[type="password"]');
        
        // check required
        if ( passwordInputs[0].value === "" )
        {
          passwordInputs[0].focus();
          return false;
        }
        if (passwordInputs[1].value === "")
        {
          passwordInputs[1].focus();
          return false;
        }
        
        // check matching
        if ( passwordInputs[0].value !== passwordInputs[1].value )
        {
          passwordInputs[0].value = passwordInputs[1].value = "";
          passwordInputs[0].focus();
          return false;
        }
        
        // send request
        
        
        // return
        return false;
        
      });
      
      // set close button
      dialog.find(".dialog_buttons .closeButton").click(function()
      {
        $("#resetPasswordDialog").remove();
        $("#resetPasswordMask").remove();
      });
      
    });
    
    // get mask
    var mask = $("#resetPasswordMask");
    if (mask.length < 1)
    {
      mask = $("<div>").attr("id", "resetPasswordMask");
      mask.appendTo(document.body);
      mask.click(function()
      {
    	  $("#resetPasswordDialog").remove();
        $("#resetPasswordMask").remove();
      });
    }
    
    // show mask
    mask.show();
    
    // return
    return this;
    
  });
})(jQuery);

