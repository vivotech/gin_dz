// 格式化文章数据
function formatThreadData(d){
	var pic = "";
	//global forum_icons;
	if(d["coverimg"] == ''){
		if(d["media_list"].length > 0){
			pic = d["media_list"][0]["media_url"];
		}
	}else{
		pic = d["coverimg"] + "?imageView2/2/h/350";
	}
	var html = '\
		<a class="thread-container" target="_blank" href="/forum.php?mod=viewthread&tid=' + d.tid +'">\
		    <div class="image_box">\
                <img class="thread-image" src="' + pic + '">\
            </div>\
            <div class="thread-forum">\
                <i class="icon icon_fid_' + d.fid + '"></i>' + d.forumname + '\
            </div>\
            <p class="thread-title">' + d.subject + '</p>\
            <div class="thread-state">\
                <span class="thread-date">' + d.dateline + '</span>\
                <span class="thread-icon thread-saygood"></span>' + d.saygood + '\
                <span class="thread-icon thread-replies"></span>' + d.replies + '\
            </div>\
		</a>\
	';
	return html;
}

// 读取文章内容
var loadThread = function(page, fid, cb){
	pagesize = 11;
	jQuery.ajax({
		url:"/app.php",
		data:{
			"mod":"forum",
			"act":"list",
			"page":page,
			"pagesize":pagesize,
			"fid":fid
		},
		dataType:"json",
		success:cb
	});
}
$jq(function(){
	// 配置参数
	var colsCustom = 4,									// 每行元素数目
		waterfallContainerId = "waterfall-container",	// 瀑布流容器ID
		waterfallIHeight = 345,							// 瀑布流单元素高度
		waterfallSpacing = 10;							// 瀑布流元素间空白
	// 内部使用的变量
	var _threadLoadLock = false,
		_page = 1,
		_pagecount = false,
		_reloadTimes = 0,
		_maxReloadTimes = 10;
	// 瀑布流初始化
	var indexWaterFall = jQuery("#" + waterfallContainerId).vivoWaterFall({
		itemClassName:"box",		// 子元素类名
		cols:colsCustom,			// 列数
		iHeight:waterfallIHeight,	// 行高
		spacing:waterfallSpacing	// 空白
	});

	jQuery("body").on("mouseenter", ".waterfall-container>.box>.thread-container", function(){
		var _this = jQuery(this);
		_this.find("div.content").css("display","none");
		_this.find(".hidden-content").css("display","block");
	});
	jQuery("body").on("mouseleave", ".waterfall-container>.box>.thread-container", function(){
		var _this = jQuery(this);
		_this.find("div.content").css("display","block");
		_this.find(".hidden-content").css("display","none");
	});
	
	// 读取文章内容回调
	var loadThreadCallBack = function(d){
		if(d.result == 1){
			_threadLoadLock = false;
			_page++;
			_pagecount = d.data.pagecount;
			_reloadTimes = 0;
			indexWaterFall.showLoading(false);
			
			var list = d.data.list,
				htmls = new Array(),
				size = 1;
			for(var i = 0; i < list.length; i++){
				var html = formatThreadData(list[i]);
				htmls.push(html);
			}
			indexWaterFall.Append(htmls, size , {
				animate:{
					duration: 300,
					delay: 150,
					top: 80,
					left: 0
				}
			});
		}else{
			_reloadTimes++;
			if(_reloadTimes <= _maxReloadTimes){
				loadThread(_page, fidRange, loadThreadCallBack);
			}else{
				console.log('读取文章列表失败');
			}
		}

        // 设置图片等比例放大填充容器
        jQuery('.waterfall-container .box .thread-container .image_box img').each(function(){

            setImageFill(this);
        });
	}
	
	// 向下滚动加载
	jQuery(window).scroll(function(){
		
		var st = jQuery(window).scrollTop(),
			wh = jQuery(window).height(),
			waterfallContainer = jQuery("#" + waterfallContainerId),
			cH = waterfallContainer.height(),
			cY = waterfallContainer.offset().top;
			
		
		if(st + wh >= cY + cH){
			if(!_threadLoadLock && (_pagecount == false || _page < _pagecount)){
				_threadLoadLock = true;
				indexWaterFall.showLoading(true);
				loadThread(_page, fidRange, loadThreadCallBack);
			}
		}
	});

	loadThread(_page, fidRange, loadThreadCallBack);

	// banner
	if ( $jq('.vivo-banner-container').length )
	{
		$jq('.vivo-banner-container').vivoBanner({
			speed: 500,
			delay: 3000,
			fluid: true,
			dots: true,
			items: ">.banner",
			item: ">.banner-item",
			complete: function(el, target){
				var index = $jq('.vivo-banner-container .banner-item').index(target);
				$jq('.vivo-banner-container .thumbnail li')
				  .removeClass("active").eq(index).addClass("active");
			}
		});
	}

	var lock_vivo_banner = false;

    /*
	$jq('.vivo-banner-container .thumbnail li').mouseenter(function(){
		if (lock_vivo_banner) { return; }
		lock_vivo_banner = true;
		var index = $jq('.vivo-banner-container .thumbnail li').removeClass("active").index(this);
		var UnsliderData = $jq('.vivo-banner-container .banner').data('unslider');
		$jq('.vivo-banner-container .thumbnail li').eq(index).addClass("active");
		UnsliderData.to(index);
		window.setTimeout(function(){ lock_vivo_banner = false; }, 500);
	}).each(function(){
		var index = $jq('.vivo-banner-container .thumbnail li').index(this);
	    if (index===0)
	    {
	    	$jq(this).addClass("active");
	    }
	});*/

    // recommend information
    new Swiper('.recommend_information .swiper-container', {
        pagination: '.recommend_information .swiper-pagination',
        paginationClickable: true,
        'autoplay': 3000,
        'speed': 2000
    });

});

