(function($){
	$.fn.imageViewer = function(picList,picIndex,options){
		var self = this;
		
		if(!options){
			var options = {};
		}
		
		this.Init = function(){
			self.mainContainerId = "img-viewer";										//主容器ID
			self.bgClassName = "img-viewer-bg";											//背景类名
			self.picContainerClassName = "img-container";								//图片容器类名
			self.picShowerId = "img-viewer-shower";										//图片ID名
			self.btnCloseClassName = "img-viewer-btnClose";								//关闭按钮类名
			self.bgInnerClassName = "img-viewer-inner";									//左右按键容器名
			self.InnerBtnClassName = "img-viewer-inner-btn";							//左右按键公共类名
			self.InnerBtnLeftClassName = "left";										//左按键类名
			self.InnerBtnRightClassName = "right";										//右按键类名
			self.imgControlBarContainerClassName = "img-viewer-control-bar-container";	//图片控制台容器类名
			self.imgControlBarClassName = "img-viewer-control-bar";						//图片控制台类名
			self.imgControlBarBtnClassName = "img-viewer-control-bar-btn";				//图片控制台按钮类名
			self.controlBtn = [
					              {"className":"zoomOut","btnName":"放大","picPosition":"0 -258px"},
					              {"className":"zoomIn","btnName":"缩小","picPosition":"0 -292px"},
					              {"className":"rotate","btnName":"旋转","picPosition":"-96px -256px"},
					              {"className":"download","btnName":"下载","picPosition":"6px -326px"}
					          ];
			
			self.InitOptions();
			self.InitContainer();
			self.InitData(picList,picIndex);
			self.InitEvent();
		}
		
		this.InitOptions = function (){
			var w = $(window).width()*0.66,
				h = $(window).height()*0.66,
				min_w = (w < 800)?'800':w,
				min_h = (w < 400)?'400':h;
			
			options.url = (options.url) ? options.url : '/static/js/vivo/imageViewer/images';	
			options.width = (options.width) ? options.width:min_w + 'px';								//图片容器宽度
			options.height = (options.height) ? options.height:min_h + '600px';							//图片容器高度
			options.innerBtnWidth = (options.innerBtnWidth) ? options.innerBtnWidth:15;					//左右按键宽度
			options.innerBtnHeight = (options.innerBtnHeight) ? options.innerBtnHeight:28;				//左右按键高度
			options.innerBtnPadding = (options.innerBtnPadding) ? options.innerBtnPadding:40;			//左右按键离图片容器距离
			options.controlBarPadding = (options.controlBarPadding) ? options.controlBarPadding:60;		//图片控制台离图片容器高度
		}
		
		this.InitContainer = function(){
			var mainContainerId = self.mainContainerId;
			
			//移除已存在的主容器
			$("#" + mainContainerId).remove();
			
			//建立主容器
			$('<div></div>').attr({
				"id":mainContainerId
			}).css({
				"position":"absolute",
				"z-index":"999"
			}).appendTo(self);
			
			var mainContainer = $("#"+mainContainerId);
			//建立背景
			var bgClassName = self.bgClassName;
			
			$("<div></div>").attr({
				"class":bgClassName
			}).css({
				"position":"fixed",
				"top":"0px",
				"left":"0px",
				"background-color":"rgba(0,0,0,0.8)",
				"z-index":"999"
			}).appendTo(mainContainer);
			
			var bg = mainContainer.find("." + bgClassName);
			
			//建立背景关闭键
			var btnClose = self.btnCloseClassName;
			jQuery("<a></a>").attr({
				"class":btnClose
			}).css({
				"position":"fixed",
				"top":"0px",
				"right":"0px",
				"display":"block",
				"background-color":"#393939",
				"height":"58px",
				"width":"58px",
				"border-radius":"0px 0px 0px 138px"
			}).appendTo(bg);
			jQuery("<span></span>").css({
				"display":"block",
				"cursor":"pointer",
				"height":"34px",
				"width":"34px",
				"margin":"10px 5px 0px 18px",
				"background":"url('" + options.url + "/buttonFromMicroMsg.png') no-repeat -38px -37px",
			}).appendTo(bg.find("." + btnClose));
			
			//背景左右键
			var bgInnerClassName = self.bgInnerClassName;
			$("<div></div>").attr({
				"class":bgInnerClassName
			}).css({
				"position":"fixed",
				"width":parseInt(options.width) + options.innerBtnPadding*2 + options.innerBtnWidth*2 + "px",
				"height":options.innerBtnHeight
			}).appendTo(bg);
			
			var bgInner = bg.find("." + bgInnerClassName),
				InnerBtnClassName = self.InnerBtnClassName,
				InnerBtnLeftClassName = self.InnerBtnLeftClassName,
				InnerBtnRightClassName = self.InnerBtnRightClassName;
			//左键
			$("<a></a>").attr({
				"class":InnerBtnClassName + " " + InnerBtnLeftClassName
			}).css({
				"float":"left",
			}).appendTo(bgInner);
			$("<span></span>").css({
				"background":"url('" + options.url + "/buttonFromMicroMsg-btn.png') no-repeat -72px -290px",
			}).appendTo(bgInner.find("." + InnerBtnLeftClassName));
			//右键
			$("<a></a>").attr({
				"class":InnerBtnClassName + " " + InnerBtnRightClassName
			}).css({
				"float":"right",
			}).appendTo(bgInner);
			$("<span></span>").css({
				"background":"url('" + options.url + "/buttonFromMicroMsg-btn.png') no-repeat -106px -290px",
			}).appendTo(bgInner.find("." + InnerBtnRightClassName));
			//为按键添加样式
			bgInner.find("." + InnerBtnClassName).css({
				"display":"block",
				"width":"15px",
				"height":options.innerBtnHeight
			});
			bgInner.find("." + InnerBtnClassName + " span").css({
				"display":"block",
				"width":"15px",
				"height": options.innerBtnHeight + "px",
				"cursor":"pointer"
			});
			
			//图片控制台
			var imgControlBarContainerClassName = self.imgControlBarContainerClassName;
			$("<div></div>").attr({
				"class":imgControlBarContainerClassName
			}).css({
				"position":"fixed",
				"display":"block",
				"width":"100%",
				"z-index":"1001"
			}).appendTo(mainContainer);
			
			var imgControlBarContainer = mainContainer.find("." + imgControlBarContainerClassName),
				imgControlBarClassName = self.imgControlBarClassName;
			
			$("<ul></ul>").attr({
				"class":imgControlBarClassName
			}).css({
				"display":"block",
				"width":"220px",
				"margin":"0px auto",
				"background": "#333",
				"border-radius": "40px",
				"box-shadow": "0 1px 2px #000",
				"padding": "0px 14px 0px 20px"
			}).appendTo(imgControlBarContainer);
			
			var imgControlBar = imgControlBarContainer.find("." + imgControlBarClassName),
				imgControlBarBtnClassName = self.imgControlBarBtnClassName,
				controlBtn = self.controlBtn;
			
			for(var i = 0 ; i < controlBtn.length ; i++){
				$("<li></li>").attr({
					"class":imgControlBarBtnClassName + " " + controlBtn[i].className,
					"title":controlBtn[i].btnName
				}).css({
					"display":"inline-block",
					"margin":"8px 0px",
					"padding":"4px 10px 0px 10px",
					"border-right":"1px solid #2c2c2c"
				}).appendTo(imgControlBar);
				$("<span></span>").css({
					"display":"block",
					"background":"url('" + options.url + "/buttonFromMicroMsg-btn.png') no-repeat " + controlBtn[i].picPosition,
					"width":"34px",
					"height":"28px",
					"cursor":"pointer"
				}).appendTo(imgControlBar.find("." + imgControlBarBtnClassName + "." + controlBtn[i].className));
				
				if(i == controlBtn.length - 1){
					imgControlBar.find("." + imgControlBarBtnClassName + "." + controlBtn[i].className).css({"border-right":"none"});
				}
			}
			
			//建立图片容器
			var picContainer = self.picContainerClassName
 			$("<div></div>").attr({
				"class":picContainer
			}).css({
				"position":"fixed",
				"width":options.width,
				"height":options.height,
				"z-index":"1000"
			}).appendTo(mainContainer);
			
			//建立图片显示
			$("<img>").attr({
				"id":self.picShowerId
			}).css({
				"box-shadow": "0px 4px 30px #333",
				"position": "relative",
				"top":"0px",
				"left":"0px"
			}).appendTo(mainContainer.find("." + picContainer));
			
			self.InitContainerPosition();
		}
		
		this.InitContainerPosition = function(){
			var mainContainer = $("#" + self.mainContainerId);
			
			//调整背景大小
			var w_h = $(window).height(),
				w_w = $(window).width(),
				bg = mainContainer.find("." + self.bgClassName);
			
			bg.css({
				"width":w_w + "px",
				"height":w_h + "px"
			});
			
			var btn_x = (w_w - parseInt(options.width) - options.innerBtnPadding*2 - options.innerBtnWidth*2)/2,
				c_y = (w_h - parseInt(options.height))/2;	
				btn_x = (btn_x < 0) ? 0 : btn_x; 
				c_y = (c_y < 0) ? 0 : c_y;

			//调整背景左右键位置
			var btn_y = c_y + (parseInt(options.height)/2);
			bg.find("." + self.bgInnerClassName).css({
				"top":btn_y + "px",
				"left":btn_x + "px"
			});
			//调整中间框位置
			var bg_x = btn_x + options.innerBtnPadding + options.innerBtnWidth;
			
			mainContainer.find("." + self.picContainerClassName).css({
				"top":c_y + "px",
				"left":bg_x + "px"
			});
			//调整背景下操作栏位置
			var controlBar_y = options.controlBarPadding + c_y + parseInt(options.height);
			mainContainer.find("." + self.imgControlBarContainerClassName).css({
				"top":controlBar_y + "px"
			});
			
		}
		
		this.InitData = function(picArr , picIndex){
			self.picList = picArr;
			self.picCount = picArr.length;
			self.innerBtnLeft = $("#" + self.mainContainerId + " ." + self.bgInnerClassName + " ." + self.InnerBtnClassName + "." + self.InnerBtnLeftClassName);
			self.innerBtnRight = $("#" + self.mainContainerId + " ." + self.bgInnerClassName + " ." + self.InnerBtnClassName + "." + self.InnerBtnRightClassName);
			self.picShower = $("#" + self.picShowerId);
			self.changePic(picIndex);
		}
		
		this.InitEvent = function(){
			//窗体大小变换事件
			$(window).resize(function(e){
				self.InitContainerPosition();
			});
			//按下ESC关闭窗口
			$(document).unbind("keyup");
			$(document).keyup(function(e) {
				if (e.keyCode == 27) {
					self.destroySelf();
				}else if(e.keyCode == 37){
					// 前一张
					self.changePic(parseInt(self.currentPicIndex)-1);
				}else if(e.keyCode == 39){
					self.changePic(parseInt(self.currentPicIndex)+1);
				}
			});
			//关闭按钮事件
			$("#" + self.mainContainerId + " ." + self.btnCloseClassName).click(function(){
				self.destroySelf();
			});
			
			//上一张下一张
			self.innerBtnLeft.click(function(){
				self.changePic(parseInt(self.currentPicIndex)-1);
			});
			self.innerBtnRight.click(function(){
				self.changePic(parseInt(self.currentPicIndex)+1);
			});
			
			//放大
			$("#" + self.mainContainerId + " ." + self.imgControlBarClassName + " ." + self.imgControlBarBtnClassName + "." + self.controlBtn[0].className).click(function(){
				self.resetPicContainerOverflow("visible");
				self.zoomPic(1);
			});
			//缩小
			$("#" + self.mainContainerId + " ." + self.imgControlBarClassName + " ." + self.imgControlBarBtnClassName + "." + self.controlBtn[1].className).click(function(){
				self.resetPicContainerOverflow("visible");
				self.zoomPic(-1);
			});
			//旋转
			$("#" + self.mainContainerId + " ." + self.imgControlBarClassName + " ." + self.imgControlBarBtnClassName + "." + self.controlBtn[1].className).click(function(){
				
			});
			//下载按钮
			$("#" + self.mainContainerId + " ." + self.imgControlBarClassName + " ." + self.imgControlBarBtnClassName + "." + self.controlBtn[3].className).click(function(){
				window.open(picList[self.currentPicIndex],"_blank");
			});
			
			//点击背景销毁自己
			$("#" + self.mainContainerId + " ." + self.bgClassName).click(function(e){
				if($(e.target).hasClass(self.bgClassName)){
					self.destroySelf();
				}
			});
			$("#" + self.mainContainerId + " ." + self.picContainerClassName).click(function(e){
				self.destroySelf();
			});
		}
		
		//重设
		this.resetInnerBtnState = function(){
			if(parseInt(self.currentPicIndex) == 0){
				//隐藏左键
				self.innerBtnLeft.css("display","none");
			}else{
				self.innerBtnLeft.css("display","block");
			}
			
			if(parseInt(self.currentPicIndex) + 1 == self.picCount){
				//隐藏右键
				self.innerBtnRight.css("display","none");
			}else{
				self.innerBtnRight.css("display","block");
			}
		}
		
		//变换图片
		this.changePic = function(index){
			index = (index) ? index : 0;
			if(index < 0){
				index = 0;
			}else if(index > (self.picCount - 1)){
				index = self.picCount - 1;
			}else{
				self.currentPicIndex = index;
				self.picShower.attr({
					"src" : self.picList[index]
				}).css({
					"display":"none"
				}).load(function(){
					var _this = $(this);
					$("<img/>")
					.attr("src",self.picList[index])
					.load(function(){
						self.picShowerRealWidth = this.width;
						self.picShowerRealHeight = this.height;
						
						var parentContainer = self.picShower.parent(),
							parentContainer_w = parentContainer.width(),
							parentContainer_h = parentContainer.height();
						
						self.resetPicContainerOverflow("hidden");
						
						if(self.picShowerRealWidth > parentContainer_w ){
							self.picShowerRealHeight = self.picShowerRealHeight/self.picShowerRealWidth*parentContainer_w;
							self.picShowerRealWidth = parentContainer_w;
						}
						if(self.picShowerRealHeight > parentContainer_h){
							self.picShowerRealWidth = self.picShowerRealWidth/self.picShowerRealHeight*parentContainer_h;
							self.picShowerRealHeight = parentContainer_h;
						}
						
						_this.css({
							"width" : self.picShowerRealWidth + "px",
							"height": self.picShowerRealHeight + "px",
							"display":"block"
						});
						
						self.resetPicPosition();
					})
				});
			}
			
			self.resetInnerBtnState();
		}
		
		//重设图片位置
		this.resetPicPosition = function(){
			var p_w = self.picShower.width(),
				p_h = self.picShower.height(),
				parentContainer = self.picShower.parent(),
				parentContainer_w = parentContainer.width(),
				parentContainer_h = parentContainer.height(),
				p_x = (parentContainer_w-p_w)/2,
				p_y = (parentContainer_h-p_h)/3;
			
			if(parentContainer_w > p_w){
				
			}
			self.picShower.css({
				"top":p_y + "px",
				"left":p_x + "px"
			});
		}
		
		this.resetPicContainerOverflow = function(overflow){
			overflow = (overflow) ? overflow : "hidden";
			var parentContainer = self.picShower.parent();
			parentContainer.css({
				"overflow":overflow
			});
		}
		
		//缩放图片
		this.zoomPic = function(level){
			level = (level) ? level : 1;
			var zoomRate = 0.3;
			var p_w = self.picShower.width(),
			p_h = self.picShower.height();
		
			self.picShower.css({
				"width":p_w + self.picShowerRealWidth*zoomRate*level + "px",
				"height":p_h + self.picShowerRealHeight*zoomRate*level + "px",
			});
			self.resetPicPosition();
		}
		
		//销毁自己
		this.destroySelf = function(){
			$("#" + self.mainContainerId).fadeOut("500",function(){
				$(this).remove();
			});
		}
		
		this.Init();
	}
})(jQuery)