(function($){
	$.fn.shareComments = function(shareId,appendFunction,destoryObj){
		var self = this,
			commentsContainer = $("<div>");
		
		this.Init = function(){
			self.InitData();
			self.InitContainer();
		}
		
		this.InitData = function(){
			self.commentsContainerClassName = "comments-container";
			
			// sender
			self.senderContainerClassName = "user-comment-sender-container";
			self.userImgContainerClassName = "user-img-container";
			self.userImgClassName = "user-img";
			self.sendBtnClassName = "send-btn";
			self.sendInputClassName = "user-comment-input";
			
			// filter
			self.filterContainerClassName = "comments-filter-container";
			self.filterItemClassName = "comments-filter-item";
			
			// commentsList
			self.commentsListClassName = "comments-list";
			self.commentClassName = "comment";
			
			// comments-page-navi
			self.commentsPageNaviClassName = "comments-page-navi";
			
			// 自身变量
			self.templetUrl = "/Public/js/community/pc/shareComments/";
			self.shareId = shareId;
		}
		
		this.InitContainer = function(){
			//主容器
			commentsContainer.load(self.templetUrl + "/shareComments.html", function(){
				// 查找容器
				self.senderContainer = commentsContainer.find("." + self.senderContainerClassName);
				self.filterContainer = commentsContainer.find("." + self.filterContainerClassName);
				self.commentsList = commentsContainer.find("." + self.commentsListClassName);
				self.commentsPageNavi = commentsContainer.find("." + self.commentsPageNaviClassName);
				
				self.InitEvent();
				self.InitAppend();
				
				// 加载用户头像
				var userImg = "/Public/images/user-nopic.jpg";
				$.ajax({
					"url":"/community/ajax/ajaxGetUserPersonalInfo",
					"dataType":"json",
					"success":function(data){
						var r = data.result,
							u = data.data;
						if(r == 1){
							if(u['avatar']){
								userImg = u['avatar'];
							}
						}
						self.senderContainer.find("." + self.userImgContainerClassName + ">." + self.userImgClassName).attr("src",userImg);
					}
				})
				
				
				// 载入评论
				self.refreshComment({
					"pagestart":"1"
				});
			});
			
		}
		
		this.InitEvent = function(){
			//销毁事件
			$(destoryObj).click(function(){
				self.destroySelf();
			});
			
			// emoji表情
			self.senderContainer.find(".control-bar>.control-btn>.control-btn-item.emotion-add").emoji({
				"target":self.senderContainer.find("." + self.sendInputClassName)
			});
			
			//发送评论
			self.senderContainer.find("." + self.sendBtnClassName).click(function(){
				// 检查有否登陆
				var isLogin = checkUserLogin();
				if(isLogin){
					var _this = $(this),
						sendInput = self.senderContainer.find("."  + self.sendInputClassName),
						commentContent = sendInput.val();
					commentContent = $.trim(commentContent);
					_this.prop("disabled",true).addClass("disabled");
					
					if(commentContent.length){
						$.ajax({
							type:"POST",
							url:"/community/ajax/ajaxAddcomment",
							data:{
								"share_id":self.shareId,
								"content":commentContent,
								"from_source":"PC端网页版"
							},
							dataType:"json",
							success:function(data){
								if(data.id > 0){
									//插入dom
									var commentJson = data;
									commentJson['content'] = $("<div/>").html(commentJson['content']).text();
									var commentHtml = "",
										commentObj = {};
										
									commentHtml = self._formatComment(commentJson);
									commentObj = $(commentHtml);
									self._showReplyEvent(commentObj.find(".comment-state>.comment-state-btn.reply"));
									self._showUserInfo(commentObj.find(".user-img-container"));
									commentObj.find(".comment-reply>.control-bar>.control-btn>.control-btn-item.emotion-add").emoji({
										"target":commentObj.find(".comment-reply>.comment-reply-input-container>.comment-reply-input")
									});
									commentObj.prependTo(self.commentsList);
									
									//清除输入框
									sendInput.val("");
									//新增评论数
									var comments_data = $(destoryObj).find(".data");
									comments_data.html(parseInt(comments_data.html()) + 1);
								}else{
									vivoAlert("发送评论失败!");
								}
								_this.prop("disabled",false).removeClass("disabled");
							}
						});
					}else{
						_this.prop("disabled",false).removeClass("disabled");
						vivoAlert("不能发送空评论哦~");
					}
				}
			});
			
			//发送回复评论
			self.commentsList.on("click","." + self.commentClassName + ">.comment-reply>.control-bar>.btn-list>.btn-list-item.send-reply-btn",function(){
				// 检查有否登陆
				var isLogin = checkUserLogin();
				if(isLogin){
					var _this = $(this),
						input = $(this).parent().parent().siblings(".comment-reply-input-container").find(".comment-reply-input"),
						id = input.attr("data-content"),
						username = input.attr("data-from"),
						userId = input.attr("data-from-id"),
						commentContent = input.val();
					
					commentContent = $.trim(commentContent);
					_this.prop("disabled",true).addClass("disabled");
					
					if(commentContent.length){
						var inputContent = commentContent;
						$.ajax({
							type:"POST",
							url:"/community/ajax/ajaxAddcomment",
							data:{
								"share_id":self.shareId,
								"content":inputContent,
								"share_comment_id":id,
								"from_source":"PC端网页版"
							},
							dataType:"json",
							success:function(data){
								if(data.id > 0){
									//插入dom
									var commentJson = data;
									commentJson['content'] = $("<div/>").html(commentJson['content']).text(),
									commentJson['parent_id'] = id,
									commentJson['comment_object'] = {"user_id":userId,"user_name":username};
									var commentHtml = "",
										commentObj = {};
										
									commentHtml = self._formatComment(commentJson);
									commentObj = $(commentHtml);
									self._showReplyEvent(commentObj.find(".comment-state>.comment-state-btn.reply"));
									self._showUserInfo(commentObj.find(".user-img-container"));
									commentObj.find(".comment-reply>.control-bar>.control-btn>.control-btn-item.emotion-add").emoji({
										"target":commentObj.find(".comment-reply>.comment-reply-input-container>.comment-reply-input")
									});
									commentObj.prependTo(self.commentsList);
									
									//清除输入框
									input.val("");
									//新增评论数
									var comments_data = $(destoryObj).find(".data");
									comments_data.html(parseInt(comments_data.html()) + 1);
									// 隐藏输入框
									_this.parent().parent().parent().css("display","none");
								}else{
									vivoAlert("发送评论失败!");
								}
								_this.prop("disabled",false).removeClass("disabled");
							}
						});
					}else{
						_this.prop("disabled",false).removeClass("disabled");
						vivoAlert("不能发送空回复哦~",1000);
					}
				}
			});
			
			//评论@人
			
			//filter标签事件
			self.filterContainer.find("." + self.filterItemClassName).click(function(){
				var _this = $(this);
				if(!_this.hasClass("active")){
					var lis = _this.parent().find("." + self.filterItemClassName),
						type = _this.attr("data-content");
					
					lis.each(function(index){
						$(this).removeClass("active");
					});
					
					_this.addClass("active");
					
					self.commentsList.find("li").remove();
					self.refreshComment({
						"pagestart":"1",
						"type":type
					});
				}
			});
			
			//翻页
			
		}
		
		this.destroySelf = function(){
			commentsContainer.slideUp("300",function(){
				$(this).remove();
			});
		}
		
		this.InitAppend = function(){
			if(typeof(appendFunction) == 'function'){
				appendFunction(commentsContainer);
			}
		}
		
		this.refreshComment = function(options){
			self._loadComment(options,function(){
				self.commentsList.find(".loading").remove();
			});
		}
		
		// 加载评论
		this._loadComment = function(options,cb){
			var _options = {};
			_options.pagestart = (options.pagestart) ? options.pagestart : 1;
			_options.type = (options.type) ? options.type : 1;
			
			$.ajax({
				"url":"/community/ajax/commentList",
				"data":{
					"shareId":self.shareId,
					"type":_options.type,
					"pagestart":_options.pagestart
				},
				"dataType":"json",
				"success":function(data){
					var res = data.result;
					if(res == 1){
						var commentList = data['share_comment_list'];
						for(var i = 0; i < commentList.length ; i++){
							var commentJson = commentList[i];
							commentJson['content'] = $("<div/>").html(commentJson['content']).text();
							var commentHtml = self._formatComment(commentJson),
								commentObj = $(commentHtml);
							self._showReplyEvent(commentObj.find(".comment-state>.comment-state-btn.reply"));
							self._showUserInfo(commentObj.find(".user-img-container"));
							commentObj.find(".comment-reply>.control-bar>.control-btn>.control-btn-item.emotion-add").emoji({
								"target":commentObj.find(".comment-reply>.comment-reply-input-container>.comment-reply-input")
							});
							commentObj.appendTo(self.commentsList);
						}
					}
					
					if(typeof(cb) == "function"){
						cb();
					}
				}
			});
		}
		
		//格式化评论
		this._formatComment = function(json){
			var avatar = (json.avatar) ? json.avatar : "/Public/images/user-nopic.jpg",
				content = (json['parent_id'] > 0)? "回复<a class=\"" + VivoOptions.getOption("userPersonalIndexClassName") + "\" href=\"" + VivoOptions.getOption("userPersonalIndexUrl") + "?user_id=" + json['comment_object']['user_id'] + "\">@"+ json['comment_object']['user_name'] +"</a>：" + json['content'] : json['content'];
			var li = "  <li class=\"comment\">\
							<div class=\"user-img-container\" data-content=\"" + json['user_id'] + "\"><img class=\"user-img\" src=\"" + avatar + "\"></div>\
							<div class=\"comment-content\">\
								<a class=\"user-name\" target=\"_blank\" href=\"" + VivoOptions.getOption("userPersonalIndexUrl") + "?user_id=" + json['user_id'] + "\">" + json['user_name'] + "</a>：" + content + "\
							</div>\
							<div class=\"comment-state\">\
								<a class=\"comment-state-btn reply\">回复</a>\
								<p class=\"comment-date\">" + json['reply_time'] + "</p>\
							</div>\
							<div class=\"comment-reply\">\
								<div class=\"comment-reply-input-container\"><input class=\"comment-reply-input\" type=\"text\" data-content=\"" + json['id'] + "\" data-from=\"" + json['user_name'] + "\" data-from-id=\"" + json['user_id'] + "\"></div>\
								<div class=\"control-bar\">\
									<div class=\"btn-list\">\
										<button class=\"btn-list-item send-reply-btn\" type=\"button\">回复</button>\
									</div>\
									<div class=\"control-btn\">\
										<a class=\"control-btn-item emotion-add\" title=\"表情\"><i class=\"fa fa-smile-o\"></i></a>\
									</div>\
								</div>\
							</div>\
						</li>";
			
			return li;
		}
		
		// 挂载显示回复事件
		this._showReplyEvent = function(obj){
			obj.click(function(){
				var _thisBtn = $(this),
					state = _thisBtn.hasClass("active"),
					replyContainer = $(this).parent().siblings(".comment-reply"),
					replyInput = replyContainer.find(".comment-reply-input-container>.comment-reply-input");
				
				if(state){
					_thisBtn.removeClass("active");
					replyInput.val("");
					replyContainer.hide("300");
				}else{
					_thisBtn.addClass("active");
					replyInput.attr("placeholder","回复@" + replyInput.attr("data-from") + ":");
					replyContainer.show("300",function(){
						replyInput.focus();
					});
				}
			})
		}
		
		// 挂载用户弹窗
		this._showUserInfo = function(obj){
			var userId = obj.attr("data-content");
			obj.mouseenter(function(){
				obj.userInfo({
					"userId":userId
				});
			})
		}
		
		this.Init();
		
		return self;
	}
})(jQuery);