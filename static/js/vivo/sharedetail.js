$(function(){
	
	//评论动作
	var btn = $(".share-list>.share-list-item .btn-list>.btn-list-item.commentAction"),
		shareId = btn.attr("data-content"),
		liParent = btn.parent().parent();
	
	btn.shareComments(shareId , function(commentsContainer){
		commentsContainer.appendTo(liParent);
	});
	
});