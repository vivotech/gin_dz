function sharePreview(inputOptions){
	var self = $(this),
		options = {},
		mainContainer = $("<div></div>").attr({"class":"share-preview-container"});
	
	self.Init = function(){
		self.InitOptions();
		self.InitData();
		self.InitContainer();
	}
	
	self.InitOptions = function(){
		options.title = inputOptions.title;
		options.content = inputOptions.content;
		options.template = [
		                    	{
		                    		"width":"640",
		                    		"height":"320",
		                    		"url":"/public/js/community/pc/sharePreview/template/webpc.html",
		                    		"titleId":"share-title",
		                    		"contentId":"share-content"
		                    	}
		                    ];
		options.htmlTemplate = "/public/js/community/pc/sharePreview/sharePreview.html";
		options.iframeId = "preview-iframe";
	}
	
	self.InitData = function(){
		
	}
	
	self.InitContainer = function(){
		mainContainer.load(options.htmlTemplate,function(){
			self.window = mainContainer.find(".share-preview");
			self.InitIframe(options.template[0]);
			self.InitEvent();
		}).appendTo("body");
	}
	
	self.InitIframe = function(template){
		self.iframe = $("#" + options.iframeId);
		self.iframe.attr({
			"height":template.height + "px",
			"width":template.width + "px",
			"src":template.url
		});
		self.iframe.load(function(){
			self.iframe.contents().find("#" + template.titleId).html(options.title);
			self.iframe.contents().find("#" + template.contentId).html(options.content);
			self.InitPosition();
		});
	}
	
	self.InitPosition = function(){
		mainContainer.css({
			"display":"block"
		});
		
		var t_w = self.window.width(),
			t_h = self.window.height(),
			w_w = $(window).width(),
			w_h = $(window).height(),
			p_x = (w_w - t_w) / 2,
			p_y = (w_h - t_h) / 2;
		
		mainContainer.find(".share-preview-bg").css({
			"width":w_w + "px",
			"height":w_h + "px"
		});
		
		self.window.css({
			"top":p_y + "px",
			"left":p_x + "px"
		})
	}
	
	self.InitEvent = function(){
		$(window).resize(function(){
			self.InitPosition();
		});
		mainContainer.find(".top-header>.top-btn-list>.btn-item.close-window").click(function(){
			self.destroyMyself();
		});
	}
	
	self.destroyMyself = function(){
		mainContainer.fadeOut("300",function(){
			mainContainer.remove();
		})
	}
	
	self.Init();
	
	return self;
}