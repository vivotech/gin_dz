(function($){
	$.fn.vivoWaterFall = function(inputOptions){
		var self = this,
			options = {};
		
		self.Init = function(){
			options.itemClassName = inputOptions.itemClassName ? inputOptions.itemClassName : "box";
			options.cols = inputOptions.cols ? inputOptions.cols : 4;
			options.spacing = inputOptions.spacing ? inputOptions.spacing : 10;
			
			options.rowsNum = 1;
			options.colsNum = 0;
			options.cWidth = self.width();
			options.iWidth = (options.cWidth - (options.cols - 1) * options.spacing )/ options.cols ;
			options.iHeight = inputOptions.iHeight ? inputOptions.iHeight : options.iWidth * 0.6;
			options.cHeight = self.height();
			self.InitContainer();
		}
		
		self.InitContainer = function(){
			self.css({
				"position":"relative"
			});
			self.find("." + options.itemClassName).each(function(){
				self._resizeItem($(this));
			});
		}
		
		/**
		 * @name 追加一个框
		 * @param {String} html html字符串或者html字符串数组
		 * @param {Number} size 框的大小或者框的大小数组
		 * @param {Object} io 可选选项
		 * {
		 * 	className: "Your want to add classname",
		 *  animate:{
		 * 	  duration: 1000,
		 * 	  delay: 100,
		 *    top: 200,
		 *    left: 0
		 *  }
		 * }
		 */
		self.Append = function(html, size , io){
			
			var o = {},
				htmls = new Array(),
				sizes = new Array(),
				t_animate = {};
			
			if(typeof(html) == 'string'){
				htmls.push(html);
			}else{
				htmls = html;
			}
			if(typeof(size) == 'number'){
				for(var i = 0; i < htmls.length; i++){
					sizes.push(size);
				}
			}else{
				sizes = size;
			}
			
			t_animate.duration = io.animate.duration ?  io.animate.duration : 400;
			t_animate.top = io.animate.top ?  io.animate.top : 100;
			t_animate.left = io.animate.left ?  io.animate.left : 0;
			
			for(var i = 0; i < htmls.length; i++){
				var item = jQuery("<div>"),
					size = self._sizeFilter(sizes[i]);
				o.className = options.itemClassName + " " + options.itemClassName + "-size-" + size;
				o.className = io.className ? o.className + " " + io.className : o.className;
				
				t_animate.delay = io.animate.delay ?  i * io.animate.delay : 1;
				o.animate = io.animate ? t_animate : '';
				
				item.attr({
					"class":o.className,
					"data-size":size
				}).css({
					"opacity":0
				}).html(htmls[i]);
				item.appendTo(self);
				self._resizeItem(item, o.animate);
			}
		}
		
		/**
		 * @name 展示loading框
		 * @param {Boolean} state
		 */
		self.showLoading = function(state){
			if(state == true){
				self.siblings(".sk-spinner-three-bounce").css("display","block");
			}else{
				self.siblings(".sk-spinner-three-bounce").css("display","none");
			}
		}
		
		/**
		 * @name 
		 * @param {jQuery Object} obj
		 * @param {Object} animate
		 *      { 
		 *			duration: 1000,
		 * 			delay: 100,
		 *			top: 200,
		 *			left: 0
		 *		}
		 */
		self._resizeItem = function(obj, animate){
			var size = self._sizeFilter(obj.attr("data-size")),
				isChangeLine;
			var colsBegin = 0;
			if(options.colsNum + size > options.cols){
				// 换行
				isChangeLine = true;
				options.rowsNum++;
				options.colsNum = size;
				colsBegin = 1;
			}else{
				// 不换行
				isChangeLine = false;
				colsBegin = options.colsNum + 1;
				options.colsNum += size;
			}
			
			var width = 0,
				top = 0,
				left = 0;
			width = (size - 1) * options.spacing + size * options.iWidth;
			top = (options.rowsNum - 1) * (options.iHeight + options.spacing);
			left = (colsBegin - 1) * (options.iWidth + options.spacing);
			
			obj.css({
				"position": "absolute",
				"display": "block",
				"width": width + "px",
				"height": options.iHeight + "px",
				"opacity":0
			});
			if(typeof(animate) == 'object'){
				var t_top = top + animate.top,
					t_left = left + animate.left,
					t_animate = {
						opacity : 1,
						top: top,
						left: left
					};
				obj.css({
					"top":t_top + "px",
					"left":t_left + "px"
				});
				setTimeout(function(){
					obj.animate(t_animate,animate.duration);
				}, animate.delay);
			}else{
				obj.css({
					"opacity":1,
					"top":top + "px",
					"left":left + "px"
				});
			}

			var containerHeight = parseInt(top) + parseInt(options.iHeight);
			// 重置框体高度
			self.css({
				"height":containerHeight + "px"
			});
		}
		
		self._sizeFilter = function(size){
			size = parseInt(size);
			if(size > options.cols){
				size = options.cols;
			}else if(size <= 0){
				size = 1;
			}
			return size;
		}
		
		self.Init();
		return self;
	}
})(jQuery);