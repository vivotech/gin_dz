jQuery(".mobileForm").ready(function()
{
  // set send verify code button
  jQuery(".sendVerifyCodeButton").click(function()
  {
    // check lock
    if ( typeof(lock_send_verify_code) !== "undefined" && lock_send_verify_code === true )
    {
      return false;
    }

    var url = "home.php";
    jQuery.ajax(url, {
      "dataType": "json",
      "data": {
        "mod": "ajax",
        "action": "send_identifying_code",
        "mobile": jQuery(".phoneInput").val()
      },
      "success": (function(result){
        if (result.result == "1")
        {
          jQuery(".mobileForm .sendVerifyCodeButton").countSec({"sec":60});

          // lock
          lock_send_verify_code = true;
          window.setInterval(function(){
            lock_send_verify_code = false;
          }, 60*1000);
        }
        else if (result.result == "20003")
        {
          vivoAlert("请过60秒后再操作。", 3000, "fail");
          // lock
          lock_send_verify_code = true;
          window.setInterval(function()
          {
            lock_send_verify_code = false;
          }, 60*1000);
        }
        else if (result.result == "10004")
        {
          vivoAlert("该手机号码已注册。", 3000, "fail");
        }
        else
        {
          var error_message = VivoOptions.getErrMsg(result.result);
          vivoAlert(result.error, 3000, "fail");
        }
      })
    });
  });

  jQuery(".mobileForm").submit(function(){

    jQuery.ajax("home.php", {
      "dataType": "json",
      "data": {
        "mod": "ajax",
        "action": "submit_identifying_code",
        "mobile": jQuery(this).find(".phoneInput").val(),
        "identifying_code": jQuery(this).find("input[name='identifying_code']").val()
      },
      success: (function(result){
        if (result.result == 1)
        {
          vivoAlert("成功绑定手机！。", 2000, "success");
          window.setTimeout(function(){
            location.reload();
          }, 2000);
        }
        else
        {
          vivoAlert(result.error, 3000, "error");
        }
      })
    });

    return false;
  });

});