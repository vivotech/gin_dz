(function($){
	$.fn.userMsg = function(inputOptions){
		var self = this,
			options = {},
			userMsgContainer = $("<div></div>");
		
		this.Init = function(){
			self.InitOptions();
			self.InitData();
			self.InitContainer();
			self.InitEvent();
		}
		
		this.InitOptions = function(){
			
		}
		
		this.InitData = function(){
			self.templateUrl = "/public/js/community/pc/userMsg/userMsg.html";
		}
		
		this.InitContainer = function(){
			userMsgContainer.load(self.templateUrl,function(){
				$(this).appendTo("body");
			});
		}
		
		this.InitEvent = function(){
			
		}
		
		this.Init();
	}
})(jQuery);