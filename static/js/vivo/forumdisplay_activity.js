jQuery(function(){
	jQuery('#activityBanner').vivoBanner({
		speed: 500,
		delay: 3000,
		fluid: true,
		dots: true,
		items: ">.bannerList",
		item: ">.bannerItem"
	});
});