/*
 * 设置封面插件
 */
(function($){
	$.fn.setBannerImg = function(inputOptions){
		var self = this,
		    options = {},
			container = $('<div></div>').attr('class','setBannerImg-container'),
			bg_selected_img = $('<img/>').attr({'src':'/static/js/vivo/setBannerImg/bgselected.png' , 'id':'BGSelected-img'}),
			now_banner_box;
		//初始化
		this.init = function(){
			self.setOptions();
			
			//获得原banner的 对象
			now_banner_box = self.parent();
			
			//加载模板
			container.load('/static/js/vivo/setBannerImg/setBannerImg.html');
			container.appendTo('body');
			
			container.css({
				'display':'none',
				'position':'fixed',
				//'top':now_banner_box.offset().top + now_banner_box.height() + parseInt(now_banner_box.css("padding-top")) + parseInt(now_banner_box.css("padding-bottom")),
				'bottom':0,
				'left':0,
				'width': $(document).width(),
				'z-index': 100,
			});
			
			
			//循环默认图片
			self.loadDefaultImg();
			//绑定
			self.setEvents();
		}
		
		//设置参数
		this.setOptions = function(){
			options.defaultBG = ( inputOptions.defaultBG ) ? inputOptions.defaultBG : 'default.jpg';
		}
		
		this.loadDefaultImg = function(){
			//图片路径
			var path = '/static/image/vivo/person_background/';
			//使用PHP循环默认图片文件夹
			$.get('/forum.php?mod=ajax&action=ajaxLoadDefaultPersonBg',function(data){
				var obj = eval(data);
           		$(obj).each(function(index){
					var value = obj[index];
					if(options.defaultBG == path + value){
						container.find('.setbannerimg-options>ul').append('<li class="select"><img class="bg" src='+ path + value +'  /><img src="/Public/js/community/pc/setBannerImg/bgselected.png" id="BGSelected-img" /></li>');
					}else{
						container.find('.setbannerimg-options>ul').append('<li><img class="bg" src='+ path + value +'  /></li>');
					}
				});
			});
			container.find('.setbannerimg-options>ul').perfectScrollbar('update');
		}
		
		//绑定方法
		this.setEvents = function(){
			self.click(function(){
				container.slideToggle();
			});
			
			//关闭按钮
			container.on('click','#cancle-setbannerimg-btn',function(){
				container.slideToggle();
			});
			
			//当鼠标到各个图片的上面
			container.on('mouseenter','.setbannerimg-options li:not("#uploadPersonalBg")',function(){
				//将图片添加上去
				$(this).append(bg_selected_img);
				$(this).siblings('.select').append(bg_selected_img);
			});
			
			//当鼠标移走
			container.on('mouseleave','.setbannerimg-options li:not("#uploadPersonalBg,.select")',function(){
				//将图片添加上去
				$(this).children('#BGSelected-img').remove();
			});
			
			//当选择图片的时候
			container.on('click','.setbannerimg-options li:not("#uploadPersonalBg")',function(){
				var bg = $(this).children('img.bg').attr('src');
				var t = $(this);
				self.changeBG(bg,t);
			});
		}
		
		this.changeBG = function(sourceLink,obj){
			$.ajax({
	        	"type":"POST",
	        	"url":"/forum.php?mod=ajax&action=updateUserPersonalBg",
	        	"data":{
	        		"url":sourceLink
	        	},
	        	"dataType":"json",
	        	"success":function(data){
	        		if(data.result == 1){
	        			$("#uploadAvatarContainer").css("background-image","url('" + sourceLink + "')");
	        			obj.append(bg_selected_img).addClass('select');
	        			obj.siblings().removeClass('select').find('#BGSelected-img').remove();
	        		}else{
	        			vivoAlert("上传头像失败");
	        		}
	        	}
	        });
		}
		
		self.init();
	}
})(jQuery)
