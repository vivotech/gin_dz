$(document).ready(function(){
	
	function renderChart() {
		
	    var countChart = $("#countChart");
		var totalChart = $("#totalChart");
		var profitChart = $("#profitChart");
		
		// get document width
		var bodyWidth = $(document.body).width();
		
		// 窗口宽度在 iPad 以上使用折线图
		if (bodyWidth >= 768) {
			// 设置统计图插件数据
			var countChart_data = [{
		 		name : '交易量',
		 		value: chart_count_data,
		 		color:'#00c0ef',
		 		line_width:2
		    }];
			
			// 渲染交易量统计图
			var chartCount = new iChart.LineBasic2D({
				render : 'countChart',
				data: countChart_data,
				align:'center',
				width : $("#countChart").width(),
				height : $("#countChart").height(),
				border: null,
				
				animation : true,//开启过渡动画
				animation_duration:800,//600ms完成动画
				
				sub_option : {
					smooth : true,
					hollow: false,
					hollow_inside:false,
					point_size:8
				},
				
				tip:{
					enable:true,
					shadow:true,
					listeners:{
						 //tip:提示框对象、name:数据名称、value:数据值、text:当前文本、i:数据点的索引
						parseText:function(tip,name,value,text,i){
							
							if (dateType=='day') {
								text = (chart_count_labels[i]-1) + ':00-' + chart_count_labels[i] + ':00 交易量:<br/>';
							} else {
								text = monthSelected + '-' + chart_count_labels[i] + ' 交易量:<br/>';
							}
							
							return "<span style='color:#005268;font-size:12px;'>"+text+
								"</span><span style='color:#005268;font-size:16px;'>"+value+"</span>";
							
						}
					}
				},
				crosshair:{
					enable:true,
					line_color:'#62bce9'
				},
				
				coordinate:{
					width: $("#countChart").width() - 90,
					height: $("#countChart").height() - 60,
					axis:{
						color:'#9f9f9f',
						width:[0,0,2,2]
					},
					scale:[{
						 position:'left',
						 start_scale: 0,
						 end_scale: chart_count_max >= 100 ? null : 100,
						 scale_enable : false,
						 scale_color:'#9f9f9f'
					},{
						 position:'bottom',
						 //scale_enable : false,
						 labels: chart_count_labels
					}]
				}
			});
			
			//开始画图
			chartCount.draw();
			
			// 设置统计图插件数据
			var chartTotal_data = [{
		 		name : '交易额',
		 		value: chart_total_data,
		 		color:'#dd4b39',
		 		line_width:2
		    }];
		    
		    // 渲染交易额统计图
			var chartTotal = new iChart.LineBasic2D({
				render : 'totalChart',
				data: chartTotal_data,
				align:'center',
				width : $("#totalChart").width(),
				height : $("#totalChart").height(),
				border: null,
				
				animation : true,//开启过渡动画
				animation_duration:800,//600ms完成动画
				tip:{
					enable:true,
					shadow:true,
					listeners:{
						 //tip:提示框对象、name:数据名称、value:数据值、text:当前文本、i:数据点的索引
						parseText:function(tip,name,value,text,i){
							
							if (dateType=='day') {
								text = (chart_total_labels[i]-1) + ':00-' + chart_total_labels[i] + ':00 交易额:<br/>';
							} else {
								text = monthSelected + '-' + chart_total_labels[i] + ' 交易额:<br/>';
							}
							
							return "<span style='color:#005268;font-size:12px;'>"+text+
								"</span><span style='color:#005268;font-size:20px;'>"+value+"</span>";
						}
					}
				},
				crosshair:{
					enable:true,
					line_color:'#ec4646'
				},
				sub_option : {
					smooth : true,
					hollow:false,
					hollow_inside:false,
					point_size:8
				},
				coordinate:{
					width: $("#totalChart").width() - 90,
					height: $("#totalChart").height() - 60,
					striped_factor : 0.18,
					axis:{
						color:'#9f9f9f',
						width:[0,0,2,2]
					},
					scale:[{
						 position:'left',
						 start_scale: 0,
						 end_scale: chart_total_max >= 100 ? null : 100,
						 scale_size:2,
						 scale_enable : false,
						 scale_color:'#9f9f9f'
					},{
						 position:'bottom',	
						 scale_enable : false,
						 labels: chart_total_labels
					}]
				}
			});
			
			//开始画图
			chartTotal.draw();
			
			// 设置统计图插件数据
			var rebateChart_data = [{
		 		name : '分成额',
		 		value: chart_rebate_data,
		 		color:'#ff851b',
		 		line_width:2
		    }];
			
			// 渲染分成额统计图
			var chartRebate = new iChart.LineBasic2D({
				render : 'rebateChart',
				data: rebateChart_data,
				align:'center',
				width : $("#rebateChart").width(),
				height : $("#rebateChart").height(),
				border: null,
				
				animation : true,//开启过渡动画
				animation_duration:800,//600ms完成动画
				tip:{
					enable:true,
					shadow:true,
					listeners:{
						 //tip:提示框对象、name:数据名称、value:数据值、text:当前文本、i:数据点的索引
						parseText:function(tip,name,value,text,i){
							
							if (dateType=='day') {
								text = (chart_rebate_labels[i]-1) + ':00-' + chart_rebate_labels[i] + ':00 分成额:<br/>';
							} else {
								text = monthSelected + '-' + chart_rebate_labels[i] + ' 交易额:<br/>';
							}
							
							return "<span style='color:#005268;font-size:12px;'>"+text+
								"</span><span style='color:#005268;font-size:20px;'>"+value+"</span>";
						}
					}
				},
				crosshair:{
					enable:true,
					line_color:'#ff851b'
				},
				sub_option : {
					smooth : true,
					point_size:8
				},
				coordinate:{
					width: $("#rebateChart").width() - 90,
					height: $("#rebateChart").height() - 60,
					axis:{
						color:'#9f9f9f',
						width:[0,0,2,2]
					},
					scale:[{
						 position:'left',
						 start_scale: 0,
						 end_scale: chart_rebate_max >= 100 ? null : 100,
						 scale_enable : false,
						 scale_color:'#9f9f9f'
					},{
						 position:'bottom',	
						 //scale_enable : false,
						 labels: chart_rebate_labels
					}]
				}
			});
			
			//开始画图
			chartRebate.draw();
			
		}
		// 超小屏幕使用条形图
		else {
			
			// 创建交易量条形统计图
			var chartCount = new iChart.BarMulti2D({
				render : 'countChart',
				data: [{
	         		name : '交易量',
	         		value: chart_count_data,
	         		color:'#00c0ef'
		        }],
				labels: chart_count_labels,
				width : $("#countChart").width(),
				height : $("#countChart").height(),
				background_color : '#ffffff',
				border: null,
				legend:{
					enable:true,
					background_color : null,
					border : {
						enable : false
					}
				},
				coordinate:{
					scale:[{
						 position:'bottom',	
						 start_scale:0
					}],
					axis:{
						width:[0,0,1,1]
					},
					background_color : null,
					axis : {
						width : 0
					},
					// 外边据各30px
					width: $("#countChart").width() - 100,
					height: $("#countChart").height() - 60
				},
				offsetx: -20
			});
			
			//利用自定义组件构造左侧说明文本
			chartCount.plugin(new iChart.Custom({
				drawFn:function(){
					//计算位置
					var coo = chartCount.getCoordinate(),
						x = coo.get('originx'),
						y = coo.get('originy'),
						w = coo.width,
						h = coo.height;
					//在左上侧的位置，渲染一个单位的文字
					chartCount.target.textAlign('start')
					.textBaseline('bottom')
					.textFont('600 11px 微软雅黑')
					.fillText('交易量',x+w+20,y+h+20,false,'#333')
					.textBaseline('top')
					.fillText( dateType == 'day' ? '(小时)' : '日', x-20, y-20,false,'#333');
					
				}
			}));
			
			chartCount.draw();
			
			// 禁用事件响应
			chartCount.eventOff();
			
			// 创建交易总额条形统计图
			var chartTotal = new iChart.BarMulti2D({
				render : 'totalChart',
				data: [{
	         		name : '交易额',
	         		value: chart_total_data,
	         		color:'#dd4b39'
		        }],
				labels: chart_total_labels,
				width : $("#totalChart").width(),
				height : $("#totalChart").height(),
				background_color : '#ffffff',
				border: null,
				legend:{
					enable:true,
					background_color : null,
					border : {
						enable : false
					}
				},
				coordinate:{
					scale:[{
						 position:'bottom',	
						 start_scale:0
					}],
					background_color : null,
					axis : {
						width : 0
					},
					// 外边距
					width: $("#totalChart").width() - 100,
					height: $("#totalChart").height() - 60
				},
				offsetx: -20
			});
			
			//利用自定义组件构造左侧说明文本
			chartTotal.plugin(new iChart.Custom({
				drawFn:function(){
					//计算位置
					var coo = chartTotal.getCoordinate(),
						x = coo.get('originx'),
						y = coo.get('originy'),
						w = coo.width,
						h = coo.height;
					//在左上侧的位置，渲染一个单位的文字
					chartTotal.target.textAlign('start')
					.textBaseline('bottom')
					.textFont('600 11px 微软雅黑')
					.fillText('交易额',x+w+20,y+h+20,false,'#333')
					.textBaseline('top')
					.fillText( dateType == 'day' ? '(小时)' : '日', x-20, y-20, false, '#333');
					
				}
			}));
			
			chartTotal.draw();
			
			// 禁用事件响应
			chartTotal.eventOff();
			
			// 创建经销商分成条形统计图
			var chartRebate = new iChart.BarMulti2D({
				render : 'rebateChart',
				data: [{
	         		name : '分成额',
	         		value: chart_rebate_data,
	         		color:'#ff851b'
		        }],
				labels: chart_rebate_labels,
				width : $("#rebateChart").width(),
				height : $("#rebateChart").height(),
				background_color : '#ffffff',
				border: null,
				legend:{
					enable:true,
					background_color : null,
					border : {
						enable : false
					}
				},
				coordinate:{
					scale:[{
						 position:'bottom',	
						 start_scale:0,
						 end_scale: chart_rebate_max > 100 ? null : 100,
					}],
					axis : {
						width : 0
					},
					background_color : null,
					// 外边距
					width: $("#rebateChart").width() - 100,
					height: $("#rebateChart").height() - 60
				},
				offsetx: -20
			});
			
			//利用自定义组件构造左侧说明文本
			chartRebate.plugin(new iChart.Custom({
				drawFn:function(){
					//计算位置
					var coo = chartRebate.getCoordinate(),
						x = coo.get('originx'),
						y = coo.get('originy'),
						w = coo.width,
						h = coo.height;
					//在左上侧的位置，渲染一个单位的文字
					chartRebate.target.textAlign('start')
					.textBaseline('bottom')
					.textFont('600 11px 微软雅黑')
					.fillText('分成额',x+w+20,y+h+20,false,'#333')
					.textBaseline('top')
					.fillText( dateType == 'day' ? '(小时)' : '日', x-20, y-20, false,'#333');
					
				}
			}));
			
			chartRebate.draw();
			
			// 禁用事件响应
			chartRebate.eventOff();
		}
	}// end renderChart()
	
	$(window).on('resize', function(){
		renderChart();
	});
	renderChart();
	
});
