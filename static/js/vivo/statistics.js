$(document).ready(function(){
	
	// 更改日期和更改年月
	$("#inputDay, #selectYear, #selectMonth").change(function(){
		$(this).parents("form").submit();
	});
	
	// 检查表单
	$("form").submit(function(){
		
		// 检查日期输入
		var inputDay = $("#inputDay");
		if (inputDay.length)
		{
			var dateSelected = new Date(inputDay.val());
			var now = new Date();
			if (dateSelected.getTime() > now.getTime() )
			{
				log('表单检查失败：选择的日期不能超过当天。');
				inputDay.parent().addClass("has-error");
				return false;
			}
		}
		
		// 检查年月输入
		var selectYear = $("#selectYear");
		var selectMonth = $("#selectMonth");
		if ( selectYear.length && selectMonth.length )
		{
			var selectYearValue = selectYear.val();
			var selectMonthValue = selectMonth.val();
			
			var now = new Date();
			
			// 如果选择了该月以后的月份，则转到当前月份。
			if ( selectYearValue == now.getFullYear() && selectMonthValue > now.getMonth() + 1 ) {
				selectMonth.val(now.getMonth());
				selectMonth.change();
			}
		}
		
	});
});

	