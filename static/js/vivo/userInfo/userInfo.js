(function($){
	$.fn.userInfo = function(inputOptions){
		var self = this,
			options = {},
			mainContainer = $("<div></div>").attr({"class":"user-personalinfo-container"});
		
		this.Init = function(){
			self.InitOptions();
			self.Delay();
		}
		
		this.Delay = function(){
			self.addClass("hover");
			self.bind("mouseleave",function(){
				self.removeClass("hover");
			});
			setTimeout(function(){
				if(self.hasClass("hover")){
					self.unbind("mouseleave");
					self.removeClass("hover");
					self.InitData();
				}
			},options.delay);
		}
		
		this.InitOptions = function(){
			options.userId = (inputOptions.userId)?inputOptions.userId:0;
			options.delay = (inputOptions.delay)?inputOptions.delay:500;
			options.addFollow = (inputOptions.addFollow)?inputOptions.addFollow:'<i class="fa fa-plus"></i>加好友';
			options.delFollow = (inputOptions.delFollow)?inputOptions.delFollow:'<i class="fa fa-minus"></i>取消关注';
		}
		
		this.InitData = function(){
			self.shareId = options.shareId;
			self.templateUrl = "/static/js/vivo/userInfo/userInfo.html?" + new Date().getTime();
			self.mouseOn = true;
			$.ajax({
				type:"POST",
				url:"forum.php?mod=ajax&action=test",
				data:{
					"userId":options.userId
				},
				dataType:"json",
				async:"false",
				success:function(data){
					if(data.result == 1){
						self.user = data.data;
						self.InitContainer();
					}
				}
			})
		}
		
		this.InitContainer = function(){
			$("body>.user-personalinfo-container").remove();
			mainContainer.load(self.templateUrl , function(){
				mainContainer.find(".user-img-container").html(self.user['avatar']);
				mainContainer.find(".user-info>.user-name").html(self.user['user_name']);
				mainContainer.find(".user-info>.user-state>.share-count>.data").html(self.user['share_count']);
				mainContainer.find(".user-info>.user-state>.credits>.data").html(self.user['credits']);
				mainContainer.find(".user-info>.user-state>.credits>.comment").html(self.user['post_count']);
				mainContainer.find(".user-info>.user-group").html(self.user['group']);

				var userDesc = (self.user['user_desc'].length > 30) ? self.user['user_desc'].substr(0,30) : self.user['user_desc'];
				mainContainer.find(".user-info>.user-desc>.data").html(userDesc);
				self.followBtn = mainContainer.find(".user-info>.user-btn>.user-btn-item.follow");
				
				if(self.user['is_follow'] > 0){
					self.followBtn.addClass("del");
					self.followBtn.html(options.delFollow);
				}else{
					self.followBtn.addClass("add");
					self.followBtn.html(options.addFollow);
				}
				self.InitAppend();
				self.InitEvent();
			});
		}
		
		this.InitEvent = function(){
			// 鼠标离开,消失
			self.unbind("mouseleave");
			self.mouseleave(function(){
				self.mouseOn = false;
				self._checkMouse();
			});
			
			mainContainer.mouseenter(function(){
				self.mouseOn = true;
			});
			
			mainContainer.mouseleave(function(){
				self.mouseOn = false;
				self._checkMouse();
			});
			
			// 加关注
			self.followBtn.click(function(){
				var href = "home.php?mod=spacecp&ac=friend&op=add&handlekey=addfriendhk_" + options.userId + "&uid=" + options.userId;
				showWindow('follow', href, 'get', 0);
			});
			
			// 看分享
			mainContainer.find(".user-btn>.user-btn-item.viewshare").click(function(){
				window.open("/home.php?mod=space&uid=" + options.userId,"_blank");
			});
			// 入主页
			mainContainer.find(".user-info>.user-name").click(function(){
				window.open("/forum.php?mod=personal&uid=" + options.userId, "_blank");
			});
		}
		
		this.InitAppend = function(){
			mainContainer.appendTo("body");
			
			var t_x = self.offset().left,
			t_y = self.offset().top,
			t_w = self.width(),
			t_h = self.height(),
			c_w = mainContainer.width(),
			c_h = mainContainer.height(),
			w_w = $(window).width(),
			p_x = t_x + ((t_w - c_w)/2),
			p_y = 0;
		
			if(t_y - (c_h + $(window).scrollTop())< 0){
				//放target下面
				p_y = t_y + t_h;
			} else{
				p_y = t_y - c_h;
			}
			
			if(p_x < 0){
				p_x = 0;
			}else if(p_x + c_w > w_w){
				p_x = w_w - c_w;
			}
			mainContainer.css({
				"top":p_y + "px",
				"left":p_x + "px"
			})
		}
		
		this._checkMouse = function(){
			setTimeout(function(){
				if(!self.mouseOn){
					self.destroyMyself();
				}
			},200);
		}
		
		this.destroyMyself = function(){
			mainContainer.fadeOut("300",function(){
				mainContainer.remove();
			});
		}
		
		this.Init();
	}
})(jQuery);