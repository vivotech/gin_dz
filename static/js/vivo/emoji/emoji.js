(function($){
	$.fn.emoji = function(inputOptions){
		var self = this,
			options = {},
			mainContainer = $("<div>");
		
		self.Init = function(){
			self.InitOptions();
			self.InitContainer();
		}
		
		self.InitOptions = function(){
			options.target = inputOptions.target;
			options.emojiActiveClassName = "emojiActive";
			self.templateUrl = "/Public/js/community/pc/emoji/emoji.html";
		}
		
		self.InitContainer = function(){
			mainContainer.attr({
				"class":"vivo-emoji-input-container"
			});
			mainContainer.load(self.templateUrl, function(){
				mainContainer.appendTo("body");
				
				self.InitEvent();
			});
		}
		
		self.InitEvent = function(){
			// 显示
			self.click(function(e){
				e.stopPropagation();
				self._changeDisplay();
			});
			
			// 点击关闭按钮
			mainContainer.find(".emoji-header>.emoji-window-btn-list>.emoji-window-btn-item.remove").click(function(){
				self._changeDisplay();
			});
			
			// 点击添加表情
			mainContainer.find(".emoji-body>.emoji-list>.emoji-item").click(function(e){
				e.stopPropagation();
				var data = $(this).attr("data-content"),
					emojiStr = "[" + data + "]";
				options.target.val(options.target.val() + emojiStr);
			});
			
			// 点击上一页下一页
			mainContainer.find(".emoji-header>.emoji-page-btn-list>.emoji-page-btn-item").click(function(e){
				e.stopPropagation();
				// 判断是否能按
				var _this = $(this),
					isable = !_this.hasClass("disable"),
					emojiContainer = mainContainer.find(".emoji-body"),
					emojiContainer_h = parseInt(emojiContainer.css("height")),
					emojiList = emojiContainer.find(".emoji-list"),
					emojiList_y = parseInt(emojiList.css("top"));
				if(isable){
					if(_this.hasClass("prev")){
						// 上一页
						emojiList.css("top" ,emojiList_y + emojiContainer_h + "px");
						self._resetPageBtnState();
					}else if(_this.hasClass("next")){
						// 下一页
						emojiList.css("top" ,emojiList_y - emojiContainer_h + "px");
						self._resetPageBtnState();
					}else{
						return false;
					}
				}else{
					return false;
				}
			});
			
			// 点击非本窗口关闭
			$(document).click(function(){
				mainContainer.css({
					"display":"none"
				});
				self.removeClass(options.emojiActiveClassName);
			});
		}
		
		// 重设表情框位置
		self.resetPosition = function(){
			var s_x = self.offset().left,
				s_y = self.offset().top,
				s_h = self.height();
			
			mainContainer.css({
				"top":s_y + s_h + "px",
				"left":s_x + "px"
			});
		}
		
		// 销毁表情框
		self.destroyMyself = function(){
			mainContainer.remove();
		}
		
		/* 私有方法 */
		// 改变表情框显示状态
		self._changeDisplay = function(){
			var state = false,
				className = options.emojiActiveClassName;
			if(self.hasClass(className)){
				mainContainer.css({
					"display":"none"
				});
				self.removeClass(className);
			}else{
				self.resetPosition();
				mainContainer.css({
					"display":"block"
				});
				self.addClass(className);
				self._resetPageBtnState();
			}
		}
		
		// 重设翻页键状态
		self._resetPageBtnState = function(){
			var emojiContainer = mainContainer.find(".emoji-body"),
				emojiContainer_h = parseInt(emojiContainer.css("height")),
				emojiList = emojiContainer.find(".emoji-list"),
				emojiList_h = emojiList.height(),
				emojiList_y = parseInt(emojiList.css("top")),
				prevBtn = mainContainer.find(".emoji-header>.emoji-page-btn-list>.emoji-page-btn-item.prev"),
				nextBtn = mainContainer.find(".emoji-header>.emoji-page-btn-list>.emoji-page-btn-item.next");
			
			if(emojiList_y != 0){
				prevBtn.removeClass("disable");
			}else{
				prevBtn.addClass("disable");
			}
			if(emojiList_h + emojiList_y > emojiContainer_h){
				nextBtn.removeClass("disable");
			}else{
				nextBtn.addClass("disable");
			}
		}
		
		self.Init();
	}
})(jQuery);