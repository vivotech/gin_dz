jQuery(function(){	
	//加载更多动作
	jQuery("#get-more").click(function(){
		var _this = jQuery(this);
		if(previousCursor > 0){
			jQuery.ajax({
				"type":"POST",
				"url":"/community/ajax/ajaxLoadShare",
				"data":{
					"user_id":thisPageUserId,
					"max_id":previousCursor,
					"pagesize":20
				},
				"dataType":"json",
				"success":function(data){
					if(data.result == 1){
						var shareList = data.data['share_list'];
						
						for(var i = 0; i < shareList.length ; i++){
							var s = shareList[i],
								li = '',
								mediaList = jQuery.parseJSON(jQuery("<div/>").html(shareList[i]['media_list']).text()),
								sayGoodClassName = "fa-thumbs-o-up";
							if(s['is_saygood'] == "1"){
								sayGoodClassName = "fa-thumbs-up";
							}
							li = '<li class="share-list-item">\
											<div class="share-header">\
										<div class="user-img-container" data-content="' + s['user_id'] + '">\
											<img class="user-img" src="' + s['avatar'] + '">\
										</div>\
										<div class="user-info-container">\
											<h4 class="user-name" data-content="' + s['user_id'] + '">' + s['user_name'] + '</h4>\
											<p class="share-title" onclick="window.open(\'/community/share/sharedetail?share_id=' + s['share_id'] + '\',\'_blank\')">' + s['share_title'] + '</p>\
										</div>\
									</div>\
									<div class="share-content" onclick="window.open(\'/community/share/sharedetail?share_id=' + s['share_id'] + '\',\'_blank\')">' + s['share_content_withouthtml'] + '</div>\
									<div class="share-info">\
										<p class="share-time">' + s['share_time'] + '</p>\
										<p class="from-source">来自' + s['from_source'] + '</p>\
									</div>\
									<ul class="btn-list">\
										<li class="btn-list-item comment" data-content="' + s['share_id'] + '" data-type="1"><i class="fa fa-wechat icon"></i>评论(<span class="data">' + s['comment_count'] + '</span>)</li>\
										<li class="btn-list-item share" data-content="' + s['share_id'] + '" data-type="1"><i class="fa fa-share-alt icon"></i>分享</li>\
										<li class="btn-list-item saygood" data-content="' + s['share_id'] + '" data-type="' + s['is_saygood'] + '"><i class="fa icon ' + sayGoodClassName + '"></i>赞(<span class="data">' + s['saygood_count'] + '</span>)</li>\
										<li class="btn-list-item share" data-content="' + s['share_id'] + '" data-type="1"><i class="fa fa-exclamation icon"></i>举报</li>\
									</ul>\
								</li>';
							
							var liObj = jQuery(li);
							if(mediaList.length > 0){
								var imgListContainer = jQuery("<div></div>").attr("class","img-list-container");
								jQuery("<ul></ul>").attr({
									"class":"img-list"
								}).appendTo(imgListContainer);
								var imgList = imgListContainer.find(".img-list");
								for(var j = 0; j < mediaList.length; j++){
									var m = mediaList[j];
									if(m['media_type'] == 0){
										var img = '<img class="img" src="' + m['media_url'] + '">';
										jQuery("<li></li>").attr({
											"class":"img-list-item",
											"data-content":j
										}).html(img).appendTo(imgList);
									}
								}
								imgListContainer.insertAfter(liObj.find(".share-content"));
							}
							liObj.insertBefore(_this);
						}
						previousCursor = data.data['previous_cursor'];
						if(previousCursor <= 0){
							_this.remove();
						}
					}else{
						vivoAlert("服务器正在卖命跑，等会吧..",1000);
					}
				}
			});
		}else{
			_this.remove();
		}
	});
	
	
	// 背景图上传
	var uploadPersonalBgBtn = jQuery("#uploadBeforePersonalBg");
	uploadPersonalBgBtn.setBannerImg({
		
	});
	uploadPersonalBgBtn.parent().mouseenter(function(){
		uploadPersonalBgBtn.css("display","block");
	});
	uploadPersonalBgBtn.parent().mouseleave(function(){
		uploadPersonalBgBtn.css("display","none");
	});
	
});

