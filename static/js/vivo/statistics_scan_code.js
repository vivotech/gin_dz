$(document).ready(function(){
	
	function renderChart() {
		
		var chartMinHeight = 320;
		
		// 设置扫码量统计图高度
		var scanCodeChart = $("#scanCodeChart");
		if ( scanCodeChart.width() * 0.5625 > chartMinHeight ) {
			//scanCodeChart.css("height", (scanCodeChart.width() * 0.5625) + "px");	// 9:16 比例
		} else {
			//scanCodeChart.css("height", chartMinHeight + "px");
		}
		
		// get document width
		var bodyWidth = $(document.body).width();
		
		// iPad 以上使用折线图
		if (bodyWidth >= 768) {
			
			// 设置统计图插件数据
			var scanCodeChart_data = [{
		 		name : '扫码量',
		 		value: chart_scan_code_data,
		 		color:'#605ca8',
		 		line_width:2
		    }];
		    
		    // 渲染扫码量统计图
			var chartScanCode = new iChart.LineBasic2D({
				render : 'scanCodeChart',
				data: scanCodeChart_data,
				align:'center',
				width : scanCodeChart.width(),
				height : 600,
				border: null,
				
				sub_option : {
					smooth : true,
					hollow:false,
					hollow_inside:false,
					point_size:8
				},
				
				animation : true,//开启过渡动画
				animation_duration:800,//600ms完成动画
				
				tip:{
					enable:true,
					shadow:true,
					listeners:{
						 //tip:提示框对象、name:数据名称、value:数据值、text:当前文本、i:数据点的索引
						parseText:function(tip,name,value,text,i){

							if (dateType=='day') {
								text = (chart_scan_code_labels[i]-1) + ':00-' + chart_scan_code_labels[i] + ':00扫码量:<br/>';
							} else {
								text = monthSelected + '-' + chart_scan_code_labels[i] + ' 扫码量:<br/>';
							}
							
							return "<span style='color:#005268;font-size:12px;'>"+text+
								"</span><span style='color:#005268;font-size:20px;'>"+value+"</span>";
						}
					}
				},
				legend:{
					enable : false
				},
				crosshair:{
					enable:true,
					line_color:'#62bce9'
				},
				
				coordinate:{
					width: scanCodeChart.width() -100,
					height: 480,
					axis:{
						color:'#9f9f9f',
						width:[0,0,2,2]
					},
					scale:[{
						 position:'left',
						 start_scale:0,
						 end_scale: chart_scan_code_max >= 10 ? null : 10,
						 scale_enable : true,
						 scale_color:'#9f9f9f'
					},{
						 position:'bottom',
						 //scale_enable : false,
						 labels: chart_scan_code_labels
					}]
				}
			});
			
			//开始画图
			chartScanCode.draw();
			
		}
		// 超小屏幕使用条形图
		else {
			// 创建扫码量条形统计图
			var chartScanCode = new iChart.BarMulti2D({
				render : 'scanCodeChart',
				data: [{
	         		name : '扫码量',
	         		value: chart_scan_code_data,
	         		color:'#605ca8'
		        }],
				labels: chart_scan_code_labels,
				width : scanCodeChart.width(),
				height : 500,
				background_color : '#fff',
				border: null,
				legend:{
					enable:true,
					background_color : null,
					border : {
						enable : false
					}
				},
				coordinate:{
					scale:[{
						 position:'bottom',	
						 start_scale:0,
						 end_scale: chart_scan_code_max >= 100 ? null : 100,
					}],
					axis : {
						width : 0
					},
					// 外边据各30px
					width: scanCodeChart.width() - 60,
					height: 450
				}
				
			});
			
			//利用自定义组件构造左侧说明文本
			chartScanCode.plugin(new iChart.Custom({
				drawFn:function(){
					//计算位置
					var coo = chartScanCode.getCoordinate(),
						x = coo.get('originx'),
						y = coo.get('originy'),
						h = coo.height;
					//在左下侧的位置，渲染一个单位的文字
					chartScanCode.target.textAlign('start')
					.textBaseline('top')
					//.textFont('600 11px Verdana')
					.fillText(dateType == 'day' ? '小时' : '日',x-20,y-20,false,'#000');
					
				}
			}));
			
			chartScanCode.draw();
			
			// 禁用事件响应
			chartScanCode.eventOff();
		}
		
		// 设置商品流量分布图
		var chartProductRatio = $("#chartProductRatio");
		//chartProductRatio.css("height", chartProductRatio.width());
		
		
		var chart_product_Ratio = new iChart.Pie2D({
			render : 'chartProductRatio',
			data: pie_product_ratio_data,
			title : '商品类别分布',
			border: null,
			showpercent: true,
			decimalsnum:2,
			width : chartProductRatio.width(),
			height : 320,
			legend : {
				enable : true,
				valign : 'bottom'
			},
			sub_option : {
				mini_label_threshold_angle : 30,//迷你label的阀值,单位:角度
				mini_label:{	//迷你label配置项
					fontsize:12,
					color : '#ffffff'
				},
				label : {
					background_color:null,
					//sign:false,//设置禁用label的小图标
					padding:'0 4',
					border:{
						enable:false,
						color:'#666666'
					},
					fontsize:11,
					fontweight:600,
					color : '#4572a7'
				},
				border : {
					width : 2,
					color : '#ffffff'
				},
				listeners:{
					parseText:function(d, t){
						return d.get('value');//自定义label文本
					}
				} 
			},
		});
		chart_product_Ratio.draw();
		
		// 禁用事件响应
		chart_product_Ratio.eventOff();
	}
	
	$(window).on('resize', function(){
		renderChart();
	});
	renderChart();
	
		
});

