//全局配置类
function VivoOptions(){};
VivoOptions.options = {
	"WebDomain":"http://192.168.1.58",
	"qiNiuUploadObjName":"file",
	"qiNiuUploadUrl":"http://upload.qiniu.com/",					// 七牛上传URL
	"qiNiuUrlPrefix":"http://7u2qq8.com1.z0.glb.clouddn.com/",		// 七牛访问前缀
	"qiNiuFilePrefixCommunity":"community/",						// 七牛社区上传前缀
	"qiNiuFilePrefixShop":"shop/",									// 七牛商城上传前缀
	"qiNiuFilePrefixUser":"user/",									// 七牛用户上传前缀
	"userPersonalIndexUrl":"/community/user/userPersonalIndex",		// 访问个人主页的URL
	"userPersonalIndexClassName":"userPersonalIndex",				// 访问个人主页的标签类名
	"actionLogin":"/community/login/loginaction",					// 登陆接口URL
	"actionRegister":"/community/ajax/ajaxRegister",				// 注册接口URL
	"shareReportActionUrl":"/community/ajax/ajaxShareReport",		// 举报接口URL
	"loadShareUrl":"/community/ajax/ajaxLoadShare",
	"errcode":{
		  "10001":"没有此用户账户",
		  "10002":"登陆密码错误",
		  "10003":"登出失败",
		  "10004":"登陆名重复",
		  "10005":"电话非法",
		  "10006":"密码非法",
		  "10007":"创建账户失败",
		
		  "10020":"图片库插入图片失败",
		  "10021":"新增图片库失败",
		
		  "10030":"新增用户好友分组失败",
		  "10031":"新增用户关注失败",
		
		  "10051":"获取用户社区通知失败",
		
		  "20001":"_sign错误",
		  "20002":"非法参数",
		  "20003":"频繁生成短信验证码",
		  "20004":"生成短信验证码失败",
		  "20005":"验证码验证失败",
		
		  "30001":"获取分享类别失败",
		  "30002":"新增分享失败",
		  "30003":"修改分享失败",
		  "30004":"删除分享失败",
		  "30005":"重复点赞",
		  "30006":"点赞失败",
		  "30007":"取消点赞失败",
		  "30008":"新增评论失败",
		  "30009":"新增分享时间频率超出服务器限制",
		  "30010":"新增阅读记录时间频率超出服务器限制",
		  "30011":"新增阅读记录失败"
	}
};
VivoOptions.getOption = function(optionsKey){
	return VivoOptions.options[optionsKey];
}
VivoOptions.getErrMsg = function(errno){
	var errObj = VivoOptions.getOption("errcode"),
		msg = "";
	msg = (errObj[errno]) ? errObj[errno] : "Exception";
	return msg;
}