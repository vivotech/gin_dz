/**
 * script for register dialog
 * @author: xiaowen
 */

(function($)
{
  $.fn.registerDialog = (function()
  {
    // get dialog
    var dialog = $("#registerDialog");
    
    if (dialog.length < 1)
    {
      dialog = $("<div>");
      dialog.attr("id", "registerDialog");
      dialog.attr("class", "dialog");
      dialog.appendTo(document.body);
    }
    
    // path
    var url_registerDialog = "/Public/js/community/pc/registerDialog";
    
    // data
    var data = new Date();
    
    // load
    var url = url_registerDialog + "/registerDialog.html" + "?time=" + data.getTime().toString();
    dialog.load(url, function()
    {
      // get dialog
      var dialog = $("#registerDialog");
      
      // check placeholder
      dialog.find('input.form_control').each(function(){
        if ( !('placeholder' in document.createElement('input')) )
        {
          if (this.type === "text")
          {
            $(this).val($(this).attr("placeholder")).focus(function(){
              if ( this.value === $(this).attr("placeholder") )
              {
                this.value = "";
              }
            });
          }
        }
      });
      
      // set input control
      /*
      dialog.find(".form_control").focus(function(){
        var form_group = this.parentNode;
        var label = form_group.getElementsByTagName("label")[0];
        label.innerHTML = "";
      });
      dialog.find(".form_control").blur(function(){
        if (this.value === "")
        {
          var form_group = this.parentNode;
          var label = form_group.getElementsByTagName("label")[0];
          label.innerHTML = $(this).data("default");
        }
      });*/
      
      // set action
      var form = dialog.find("form");
      form.attr("action", VivoOptions.getOption("actionRegister"));
      form.submit(function()
      {
        // check required input  检查必填输入
        var requireds = $(this).find(".required");
        for (var i=0; i<requireds.length; i++)
        {
          if (requireds[i].value === "")
          {
            requireds[i].focus();
            return false;
          }
        }
        
        // check declaration  检查接受声明
        var declaration_accept = $(this).find(".declaration input[type=checkbox]");
        if ( declaration_accept.length && declaration_accept[0].checked === false )
        {
          vivoAlert("请认真阅读并接受《意大利原生活网免责声明》。");
          return false;
        }
        
        // send request
        $.ajax( $(this).attr("action"), {
          "type": "post",
          "dataType": "json",
          "data": $(this).serialize(),
          "success": (function(result){
            if (result.result == "1")
            {
              // remove register dialog
              $("#registerDialog").remove();
              $("#registerMask").remove();
              
              // update user info
              var u = result.data;
              $("#header-container>.header>.user-info").html('<button class="user-info-btn" title="发帖" onclick="location.href=\'/community/share/shareadd\'"><i class="fa fa-edit"></i></button><div class="user-img-container"><img class="user-img" src="' + u['avatar'] + '"></div><p class="username" id="active_myMenu">' + u['user_name'] + '<i class="fa fa-caret-down icon-down"></i></p>');
              
              // create complete data dialog
              $(document).completeDataDialog();
            }
            else
            {
              var error_message = VivoOptions.getErrMsg(result.result);
              vivoAlert(error_message, 500, "fail");
            }
          })
        });
        
        return false;
      });
      
      // set send verify code button
      dialog.find(".sendVerifyCodeButton").click(function()
      {
        // check lock
        if ( typeof(lock_send_verify_code) !== "undefined" && lock_send_verify_code === true )
        {
          return false;
        }
        
        var url = "/community/ajax/ajaxSendRegisterIdentifyingCode";
        $.ajax(url, {
          "dataType": "json",
          "data": {
            "mobile": $(".phoneInput").val()
          },
          "success": (function(result){
            if (result.result == "1")
            {
              $("#registerDialog .sendVerifyCodeButton").countSec(60);
              
              // lock
              lock_send_verify_code = true;
              window.setInterval(function(){
                lock_send_verify_code = false;
              }, 60*1000);
            }
            else if (result.result == "20003")
            {
              vivoAlert("请过60秒后再操作。", 3000, "fail");
              // lock
              lock_send_verify_code = true;
              window.setInterval(function()
              {
                lock_send_verify_code = false;
              }, 60*1000);
            }
            else if (result.result == "10004")
            {
              vivoAlert("该手机号码已注册。", 3000, "fail");
            }
            else
            {
              var error_message = VivoOptions.getErrMsg(result.result);
              vivoAlert(error_message, 3000, "fail");
            }
          })
        });
      });
      
      // set close button
      dialog.find(".dialog_buttons .closeButton").click(function(){
        $("#registerDialog").remove();
        $("#registerMask").remove();
      });
      
      // set declaration
      /*
      dialog.find(".declaration").click(function()
      {
        var checkbox = $(this).find("input")[0];
        if ( checkbox.checked === true )
        {
          checkbox.checked = false;
          $(this).removeClass("checked");
        }
        else
        {
          checkbox.checked = true;
          $(this).addClass("checked");
        }
        
        return false;
      });*/
      
      dialog.find(".declaration label").click(function()
      {
        var checkbox = $(this).find("input")[0];
        if ( checkbox.checked === true )
        {
          checkbox.checked = false;
          $(this).removeClass("checked");
        }
        else
        {
          checkbox.checked = true;
          $(this).addClass("checked");
        }
        
        return false;
      });
      
    });
    
    // get mask
    var mask = $("#registerMask");
    if (mask.length < 1)
    {
      mask = $("<div>").attr("id", "registerMask");
      mask.appendTo(document.body);
      mask.click(function(){
        $("#registerDialog").remove();
        $("#registerMask").remove();
      });
    }
    mask.show();
    
    // return
    return this;
  });
})(jQuery);

