$(function(){
	$("#get-identifying-code").click(function(){
		var _this = $(this),
			mobile = $("#mobile").val();
		mobile = $.trim(mobile);
		
		if(!_this.hasClass("disable")){
			if(mobile.length > 0){
				$.ajax({
					"type":"post",
					"url":"/community/ajax/ajaxSendRegisterIdentifyingCode",
					"data":{
						"mobile":mobile
					},
					"dataType":"json",
					"success":function(data){
						var r = data.result,
							d = data.data;
						if(r == 1){
							vivoAlert("短信已发送，请查收",2000);
							_this.countSec({
								"sec":60,
								"cb":function(btn){
									btn.removeClass("disable");
								}
							});
							_this.addClass("disable");
						}else if(r == 20003){
							vivoAlert("短信已发送，等等吧",2000);
							_this.countSec({
								"sec":d['second'],
								"cb":function(btn){
									btn.removeClass("disable");
								}
							});
							_this.addClass("disable");
						}else{
							var msg = VivoOptions.getErrMsg(r);
							vivoAlert(msg);
						}
					}
				});
			}
		}
	});
});

