$(function(){
	var commentPrefix = ".main-container>.comment-list>.comment-list-item>";
	//点击回复，出现回复
	$(commentPrefix + ".comment-operation>.comment-operation-item.reply").click(function(){
		var _this = $(this);
		_this.siblings(".comment-operation-item").removeClass("active");
		
		var sender = $(this).parent().siblings(".user-comment-sender-container"),
		input = sender.find(".user-comment-sender>.user-comment-input-container>.user-comment-input");
		
		if(_this.hasClass("active")){
			_this.removeClass("active");
			sender.css("display","none");
			input.val("");
		}else{
			var	userId = input.attr("data-from-id"),
				userName = input.attr("data-from");
			_this.addClass("active");
			sender.slideDown("300",function(){
				input.attr("placeholder","回复@" + userName + ":");
				input.focus();
			});
		}
	});
	
	//点击评论，发送回复
	$(commentPrefix + ".user-comment-sender-container>.user-comment-sender>.control-bar>.btn-list>.btn-list-item.send-btn").click(function(){
		var _this = $(this),
			input = $(this).parent().parent().siblings(".user-comment-input-container").find(".user-comment-input"),
			shareId = input.attr("data-parent"),
			id = input.attr("data-content"),
			username = input.attr("data-from"),
			userId = input.attr("data-from-id"),
			commentContent = input.val();
		
		commentContent = $.trim(commentContent);
		_this.prop("disabled",true).addClass("disabled");
		
		if(commentContent.length){
			var inputContent = commentContent;
			$.ajax({
				type:"POST",
				url:"/community/ajax/ajaxAddcomment",
				data:{
					"share_id":shareId,
					"content":inputContent,
					"share_comment_id":id,
					"from_source":"PC端网页版"
				},
				dataType:"json",
				success:function(data){
					if(data.id > 0){
						vivoAlert("回复评论成功!",1000);
						//清除输入框
						input.val("");
						//隐藏输入框
						var sender = input.parent().parent().parent();
						sender.siblings(".comment-operation").find(".comment-operation-item.reply").removeClass("active");
						sender.css("display","none");
					}else{
						vivoAlert("发送评论失败!");
					}
					_this.prop("disabled",false).removeClass("disabled");
				}
			});
		}else{
			_this.prop("disabled",false).removeClass("disabled");
			vivoAlert("不能发送空回复哦~",1000);
		}
	});
	
	//点击查看对话，出现查看对话
	
});