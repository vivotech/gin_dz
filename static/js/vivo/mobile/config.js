/**
 * Created by wen on 2015/7/20.
 */

$(document).ready(function(){

    // change nickname
    $('.config_item.nickname').click(function(){

        // 获取昵称
        var nickname = $('.config_index .config_item.nickname .config_item_content span').html();
        $('#change_nickname_dialog .input').val(nickname);

        // 显示对话框
        $('#change_nickname_dialog').fadeIn();

        // 显示遮罩
        showMask('change_nickname_dialog_mask', function(){
            $('#change_nickname_dialog').fadeOut();
            hideMaskLayer('change_nickname_dialog_mask');
        });

    });

    // cancel change nickname
    $('#change_nickname_dialog .button.cancel').click(function(){
        $('#change_nickname_dialog').fadeOut();
        hideMaskLayer('change_nickname_dialog_mask');
    });

    // submit change nickname
    $('#change_nickname_dialog .button.submit').click(function(){

        var nickname = $('#change_nickname_dialog .input').val();

        $.ajax('/app.php?mod=member&act=profile&profilesubmit=1&nickname=' + nickname, {
            'success' : function(){

                var nickname = $('#change_nickname_dialog .input').val();
                $('.config_index .config_item.nickname .config_item_content span').html(nickname);

                // 隐藏对话框和遮罩层
                $('#change_nickname_dialog').fadeOut();
                hideMaskLayer('change_nickname_dialog_mask');
            }
        });
    });

    // change gender
    $('.config_item.gender').click(function(){

        $('#change_gender_dialog').fadeIn();

        // 显示遮罩
        showMask('change_gender_dialog_mask', function(){
            $('#change_gender_dialog').fadeOut();
            hideMaskLayer('change_gender_dialog_mask');
        });
    });

    // submit change gender
    $('#change_gender_dialog .dialog_body li').click(function(){
        var index = $('#change_gender_dialog .dialog_body li').index(this);
        $.ajax('/app.php?mod=member&act=profile&profilesubmit=1&gender=' + index, {
            'success' : function(){

                var gender_text = $('#change_gender_dialog .dialog_body li').eq(index).html();
                $('.config_item.gender .config_item_content span').html(gender_text);

                // 隐藏对话框和遮罩层
                $('#change_gender_dialog').fadeOut();
                hideMaskLayer('change_gender_dialog_mask');
            }
        });
    });

    // change birthday
    

    $('.config_item.birthday').click(function(){

        $('#change_birthday_dialog').fadeIn();

        // get date
        var birthday = $(this).find('.config_item_content span').text();
        var birthday_items = birthday.split('-');

        // 获取当前生日的年月日
        var year    = typeof(birthday_items[0]) !== "undefined" ? parseInt(birthday_items[0]) : new Date().getFullYear();
        var month   = typeof(birthday_items[1]) !== "undefined" ? parseInt(birthday_items[1]) : new Date().getMonth() + 1;
        var d       = typeof(birthday_items[2]) !== "undefined" ? parseInt(birthday_items[2]) : new Date().getDate();

        // 设置日期对象
        var current_date = new Date();
        year ? current_date.setFullYear(year) : null;
        month ? current_date.setMonth(month-1) : null;
        d ? current_date.setDate(d) : null;

        var default_date = new Date();
        default_date.setFullYear(default_date.getFullYear() - 25);

        // 设置生日描述
        var desc = (year ? current_date.getFullYear() : default_date.getFullYear() ) + '年' + (current_date.getMonth() + 1) + '月' + current_date.getDate() + '日';
        $('#change_birthday_dialog .desc').html(desc);

        // 获取当前月共有多少天
        log(current_date.getMonth());
        var month_days = Date.getDaysInMonth(current_date.getFullYear(), current_date.getMonth() + 1);

        // set current birthday year
        var year_select = $('#change_birthday_dialog .control_group .input[name="year"]');
        year_select.find('option').removeAttr('selected');
        var selected_year_value = year ? current_date.getFullYear() : default_date.getFullYear();
        var select_year_options = year_select.find('option');
        for (var i=0; i<select_year_options.length; i++) {
            if ( select_year_options[i].value == selected_year_value ) {
                select_year_options[i].selected = true;
            }
        }

        // set current birthday month
        var month_select = $('#change_birthday_dialog .control_group .input[name="month"]');
        month_select.find('option').removeAttr('selected');
        var selected_month_value = current_date.getMonth()+1;
        var select_month_options = month_select.find('option');
        for (var i=0; i<select_month_options.length; i++) {
            if ( select_month_options[i].value == selected_month_value ) {
                select_month_options[i].selected = true;
            }
        }

        // render day select
        var day_select = $('#change_birthday_dialog .control_group .input[name="day"]');
        day_select.addClass('ok');
        day_select.html('');
        for (var i=1; i<=month_days; i++)
        {
            var i_option = createElement('option', {
                'value' : i
            });
            i_option.innerHTML = i;
            if ( i == current_date.getDate() )
            {
                i_option.setAttribute('selected', true);
            }

            day_select.append(i_option);
        }

        // 显示遮罩
        showMask('change_birthday_dialog_mask', function(){
            $('#change_birthday_dialog').fadeOut();
            hideMaskLayer('change_birthday_dialog_mask');
        });

    });

    // submit change birthday
    $('#change_birthday_dialog .button_box .button').click(function(){

        var birthyear = $('#change_birthday_dialog .control_group .input[name=year]').val();
        var birthmonth = $('#change_birthday_dialog .control_group .input[name=month]').val();
        var birthday = $('#change_birthday_dialog .control_group .input[name=day]').val();

        var url = '/app.php?mod=member&act=profile&profilesubmit=1'
        $.ajax(url + '&birthyear=' + birthyear + '&birthmonth=' + birthmonth + '&birthday=' + birthday, {
            'success' : function(){

                // 隐藏对话框和遮罩层
                $('#change_birthday_dialog').fadeOut();
                hideMaskLayer('change_birthday_dialog_mask');

                var birthyear = $('#change_birthday_dialog .control_group .input[name=year]').val();
                var birthmonth = $('#change_birthday_dialog .control_group .input[name=month]').val();
                var birthday = $('#change_birthday_dialog .control_group .input[name=day]').val();

                $('.config_index .config_item.birthday .config_item_content span').html(birthyear + '-' + birthmonth + '-' + birthday);
            }
        });

    });

    var progress = 0;
    var bgUploader = Qiniu.uploader({
        runtimes: 'html5,flash,html4',    //上传模式,依次退化
        browse_button: 'change_avatar_item',       //上传选择的点选按钮ID，**必需**
        uptoken_url: 'forum.php?mod=ajax&action=getQiNiuToken',
        domain: 'http://7u2qq8.com1.z0.glb.clouddn.com/',
        //container: '',           //上传区域DOM ID，默认是browser_button的父元素,
        max_file_size: '2mb',           //最大文件体积限制
        flash_swf_url: '/static/js/vivo/plupload-2.1.2/js/Moxie.swf',  //引入flash,相对路径
        max_retries: 3,                   //上传失败最大重试次数
        chunk_size: '4mb',                //分块上传时，每片的体积
        auto_start: true,                 //选择文件后自动上传，若关闭需要自己绑定事件触发上传
        init: {
            'FileUploaded': function(up, file, info) {

                log('上传图片成功');

                var domain = up.getOption('domain');
                var res = jQuery.parseJSON(info);
                var sourceLink = domain + res.key;
                sourceLink += '?imageView2/1/w/96/h/96';

                $.ajax('/app.php?mod=member&act=updateavatar', {
                    'data': {
                        'avatar': sourceLink
                    },
                    'type': 'post',
                    'success' : function(){
                        log('更改头像成功');
                        $('.config_index .config_item.avatar_item .config_item_content img').attr('src', sourceLink);
                    }
                });

            },
            'Error': function(up, err, errTip) {
                var htmlButton = '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>';
                var alert = jQuery('<div class="alert hide"></div>');
                alert.html(htmlButton+errTip)
                    .addClass('alert-danger').removeClass('hide')
                    .attr('role', 'alert');
                jQuery("#wrapperError").append(alert);
            },
            'BeforeUpload': function(up, file){
                if (!file)
                {
                    showAlertWindow('error', 5000);
                    return;
                }
                createProgressDialog('image_upload_progress_' + file.id, 0);
            },
            'UploadComplete': function() {
                return;
            },
            'UploadProgress': function(up, file)
            {
                updateProgressDialog('image_upload_progress_' + file.id, file.percent);
                if (file.percent == 100) {
                    removeProgressDialog('image_upload_progress_' + file.id);
                }
            },
            'Key': function(up, file) {
                // 若想在前端对每个文件的key进行个性化处理，可以配置该函数
                // 该配置必须要在 unique_names: false , save_key: false 时才生效

                var date = new Date();
                var key = "avatar/uid_"+uid+"_"+date.getFullYear()+(date.getMonth()+1)+date.getDate()+date.getTime();

                // do something with key here
                return key;
            }
        }
    });

    // change password
    $('.config_index .config_item.change_password').click(function(){

        $('#change_password_dialog').fadeIn();

        // 显示遮罩
        showMask('change_password_dialog_mask', function(){
            $('#change_password_dialog').fadeOut();
            hideMaskLayer('change_password_dialog_mask');
        });

    });

    // submit change password
    $('#change_password_dialog .button_box button').click(function(){

        var newpassword     = $('#change_password_dialog input[name=newpassword]').val();
        var newpassword2    = $('#change_password_dialog input[name=newpassword2]').val();

        // 没有填写新密码
        if ( newpassword == '' )
        {
            $('#change_password_dialog input[name=newpassword]').focus();
            return;
        }

        // 没有填写重复新密码
        if ( newpassword2 == '' )
        {
            $('#change_password_dialog input[name=newpassword2]').focus();
            return;
        }

        // 密码不匹配
        if ( newpassword != newpassword2 )
        {
            $('#change_password_dialog input[name=newpassword]').val('');
            $('#change_password_dialog input[name=newpassword2]').val('');
            $('#change_password_dialog input[name=newpassword]').focus();
            return;
        }

        $.ajax('/app.php?mod=member&op=change_password', {

            'data': {
                'mod' : 'member',
                'act' : 'profile',
                'op'  : 'change_password',
                'newpassword' : newpassword,
                'newpassword2': newpassword2
            },
            'dataType' : 'json',
            'success' : function(result){
                if (result.result) {
                    showAlertWindow({
                        'content': '你已成功更改密码',
                        'auto_close': 1500
                    });
                } else {
                    showAlertWindow({
                        'content': '更改失败',
                        'auto_close': 1500
                    });
                }

                $('#change_password_dialog').fadeOut().find('input').val('');
                hideMaskLayer('change_password_dialog_mask');
            }

        });
    });



});