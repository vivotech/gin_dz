/*
 * actions for mobile view thread page  手机版帖子详情页动作
 */

jQuery(document).ready(function(){
  
  // 查看帖子图片
  jQuery(".postlist .postMessage img").attr('rel', 'postimages')
    .each(function(){
  	  setViewImageAction(this);
    }
  );
  
  // 加载评论
  jQuery(".replylist").ready(function(){
    // load comment
    loadMoreComment();
  });

    /*
  // 点赞
  jQuery(".buttonLike").click(function()
  {
  	  app.praise();
  	  this.blur();
  });*/
  
  // 加载更多评测
  jQuery('#buttonLoadMoreProductFeel').click(function(){
  	  var spids = jQuery(this).data('content');
  	  if (!spids) { return false; }
  	  app.loadMoreProductFeel(spids, 2, jQuery('#productFeelList'));
  });
  var spids = jQuery('#buttonLoadMoreProductFeel').data('content');
  if (spids) {
  	app.loadMoreProductFeel(spids, 1, jQuery('#productFeelList'));
  }
  
});

