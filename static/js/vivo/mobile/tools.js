/**
 * functions
 */

/**
 * add url param  添加网址参数
 * @param url String  网址
 * @param name String  param name  参数名
 * @param value String  param alue  参数的值
 */
function addUrlParam(url, name, value) {
	var new_url = url;
	if (url.indexOf("?") < 0) {
		new_url += "?";
	} else {
		new_url += "&";
	}
	new_url += name + "=" + value;
	return new_url;
}

/**
 * get url param  获取网址参数
 * @param name String  param name  参数名
 * @returns fixed  param value  参数的值
 */
function getUrlParam(name) {
	var default_value = arguments[1] ? arguments[1] : null;

	var url = location.href;

	if (url.indexOf("?") < 0) {
		return default_value;
	}

	// url?a=1&b=2 get a=1&b=2 string
	var url_query = url.substring(url.indexOf("?") + 1);

	//
	var url_query_params = url_query.split("&");

	// no any param
	if (!url_query_params.length) {
		return default_value;
	}

	var index_name = -1;
	for (var i = 0; i < url_query_params.length; i++) {
		index_name = url_query_params[i].indexOf(name + "=");

		if (index_name === 0) {
			return url_query_params[i].substring(index_name + 1 + name.length);
		}
	}

	return default_value;
}

/**
 * check value type  检查变量类型
 * @param variable fixed  变量
 * @param type String  变量类型名称
 * @returns Boolean
 */
function is(variable, type) {
	if (typeof(variable) === type)
		return true;
	else
		return false;
}

/**
 * check if a value in array  检查数组是否存在某个值
 * @param fixed value
 * @param Array  array
 * @returns {Boolean}
 */
function inArray(value, array) {
	for (var i = 0; i < array.length; i++) {
		if (array[i] === value) {
			return true;
		}
	}
	return false;
}

/**
 * print content in page  在网页里打印内容
 * @param content String  内容
 */
function print(content) {
	var wrapper = createElement("div");
	wrapper.innerHTML = content;
	document.body.appendChild(wrapper);
}

/**
 * print array  打印数组详情
 * @param Array  array
 */
function printArray(array) {
	
	// create list element
	var list = document.createElement("ul");

	// define item
	var item = null;

	// add item element
	for (var i = 0; i < array.length; i++) {
		item = createElement("li");
		item.innerHTML = array[i];
		list.appendChild(item);
	}

	// show list
	document.body.appendChild(list);
}
var print_r = printArray;

/**
 * 打印对象详情
 * @param object  Object
 */
function printObject(object) {
	
	// create list element
	var list = document.createElement("ul");

	// define item
	var item = null;

	// add item element
	for (name in object) {
		item = createElement("li");
		item.innerHTML = name + ": " + object[name];
		list.appendChild(item);
	}

	// show list
	document.body.appendChild(list);
}

function createImage(url, options) {
	var image = new Image();
	image.src = url;
	if (typeof(options) === "undefined") {
		options = new Array();
	}
	for (var i in options) {
		image[i] = options[i];
	}
	return image;
}

/**
 * Get operating system  获取客户端的操作系统
 * @returns string
 */
function getOperatingSystem() {
	var userAgent = window.navigator.userAgent.toLowerCase();
	var list = {
		"windows": "Windows",
		"windows phone": "Windows Phone",
		"iphone": "iOS",
		"ipad": "iOS",
		"mac os": "Mac OS",
		"android": "Android",
		"blackberry": "BlackBerry"
	};

	for (var i in list) {
		if (RegExp(i).test(userAgent)) {
			return list[i];
		}
	}

	return "";
}

/**
 * Get browser core  获取浏览器核心
 * @returns String
 */
function getBrowserCore() {
	// Find known core names from the userAgent data  搜索核心名称
	var userAgent = window.navigator.userAgent;
	var isGecko = RegExp("Gecko").test(userAgent);
	var isWebkit = RegExp("AppleWebKit").test(userAgent);
	var isPresto = RegExp("Presto").test(userAgent);
	var isTrident = RegExp("IE").test(userAgent);

	// Return result  返回结果
	if (isWebkit) {
		return "Webkit";
	}
	if (isGecko) {
		return "Gecko";
	}
	if (isPresto) {
		return "Presto";
	}
	if (isTrident) {
		return "Trident";
	}

	// Return "unknown" as unknown core  不是主流浏览器
	return "unknown";
}

/**
 * Get IE browser core  获取 IE 的版本
 * @returns Number
 */
function getIEsVersion() {
	// Check whether the current browser is IE  检查是否 IE 浏览器
	var userAgent = window.navigator.userAgent;
	var isIE = userAgent.search(RegExp("MSIE [0-9.]+;"));
	if (!isIE) {
		return false;
	}

	// Get the browser version  获取版本
	var version = userAgent.match(RegExp("MSIE [0-9]+.[0-9]+;"))[0];
	version = version.replace(RegExp("MSIE ([0-9])+.[0-9]+;"), "$1");
	return Number(version);
}

/**
 * get element by id  根据ID获取元素
 * @param String id
 * @returns Object(HTMLElement) | null
 */
function getElement(id) {
	var element = document.getElementById(id);
	return element;
}

/**
 * get elements by class name
 * @param class_name String  class name
 * @param node Object(HTMLElement)
 * @return Object(NodeList) | Array | bool(false) if fail
 */
function getElementsByClassName(class_name, node) {
	
	if (!node) {
		node = document;
	}
	
	// use HTML5 DOM API
	if (node.getElementsByClassName) {
		return node.getElementsByClassName(class_name);
	}

	// use jQuery
	else if (typeof($) !== "undefined") {
		var jquery_elements = jQuery(node).find("." + class_name);
		var result = new Array();
		for (var i = 0; i < jquery_elements.length; i++) {
			result[result.length] = jquery_elements[i];
		}
		return result;
	}

	// return default
	return false;
}

function getChildByClassName(node_element, class_name) {
	// use HTML5 DOM API
	if (document.getElementsByClassName) {
		var childs = node_element.getElementsByClassName(class_name);
		return childs.length ? childs[0] : null;
	}
	// use jQuery
	else if (typeof($) !== "undefined") {
		var childs = jQuery(node_element).find("." + class_name);
		return childs.length ? childs[0] : null;
	}

	// can not do it on current browser
	return false;
}

/**
 * query all dom from selector  从选择器查询所有元素节点
 * @param selector  String  selector expression
 * @param node  Object(HTMLElement)  which node to query, default document
 * @return Object(NodeList) | Array | Bool(false) if fail
 */
function querySelectorAll(selector) {
	// variable
	var elements = null;
	var node = arguments[1] ? arguments[1] : null;

	if (document.querySelectorAll) // use HTML5 DOM API
	{
		// try to query
		try {
			if (node) {
				elements = node.querySelectorAll(selector);
			} else {
				elements = document.querySelectorAll(selector);
			}

			// return result NodeList
			return elements;
		}
		// catch exception
		catch (exception) {
			// log
			if (window.console) {
				window.console.log("querySelector function error: syntax exception.");
			}
		}
	} else if (!elements.length && typeof(jQuery) !== "undefined") // use jQuery
	{
		// query
		if (node) {
			var jquery_elements = jQuery(node).find(selector);
		} else {
			var jquery_elements = jQuery(selector);
		}

		// set result and return
		if (jquery_elements.length) {
			var result = new Array();
			for (var i = 0; i < jquery_elements.length; i++) {
				result[result.length] = jquery_elements[i];
			}
			return result;
		}
	}

	// return default
	return false;
}

/**
 * @name create element  创建元素
 * @param String  element name  元素名称
 * @param Object  element attributes  元素属性
 * @param Object(HTMLElement) | String  内容，可以是字符串或节点元素
 * @returns Object(HTMLElement) | false
 */
function createElement(name) {
	// create element
	var element = document.createElement(name);

	// set attributes
	var options = arguments[1] ? arguments[1] : null;
	if (options !== null) {
		for (var attr_name in options) {
			element.setAttribute(attr_name, options[attr_name]);
		}
	}

	// set content
	var content = arguments[2] ? arguments[2] : "";
	if (typeof(content) === "object") {
		element.append(content);
	} else {
		element.innerHTML = content;
	}

	// return
	return element;
}

function insertElement(wrapper, name) {
	// create new element
	var element = createElement(name, true);
	if (!element) {
		return false;
	}
	wrapper.appendChild(element);

	// set attributes
	var options = arguments[2] ? arguments[2] : null;
	if (options !== null) {
		for (var attr_name in options) {
			element.setAttribute(attr_name, options[attr_name]);
		}
	}

	// set content
	var content = arguments[3] ? arguments[3] : "";
	if (content) {
		element.innerHTML = content;
	}

	// return
	return element;
}

function html_encode(str) {
	var s = "";
	if (str.length == 0)
		return "";
	s = str.replace(/&/g, "&gt;");
	s = s.replace(/</g, "&lt;");
	s = s.replace(/>/g, "&gt;");
	s = s.replace(/ /g, "&nbsp;");
	s = s.replace(/\'/g, "&#39;");
	s = s.replace(/\"/g, "&quot;");
	s = s.replace(/\n/g, "<br>");
	return s;
}

function html_decode(str) {
	var s = "";
	if (str.length == 0)
		return "";
	s = str.replace(/&gt;/g, "&");
	s = s.replace(/&lt;/g, "<");
	s = s.replace(/&gt;/g, ">");
	s = s.replace(/&nbsp;/g, " ");
	s = s.replace(/&#39;/g, "\'");
	s = s.replace(/&quot;/g, "\"");
	s = s.replace(/<br>/g, "\n");
	return s;
}

/**
 * Get scroll top  获取视图到顶部的滚动距离
 * @returns Int
 */
function getScrollTop() {
	
	// Firefox & Internet Explorer
	if (document.documentElement && document.documentElement.scrollTop) {
		if (window.console) {
			window.console.log('get document.documentElement.scrollTop');
		}
		return document.documentElement.scrollTop;
	}

	// Google Chrome
	else if (document.body) {
		if (window.console) {
			window.console.log('get document.body.scrollTop');
		}
		return document.body.scrollTop;
	}

	return false;
}

/**
 * @name Get scroll height  获取元素的可滚动高度
 * @returns Int|false
 */
function getScrollHeight() {
	// Google Chrome & Firefox & Internet Explorer 11
	if (document.documentElement && document.documentElement.scrollHeight) {
		if (window.console) {
			window.console.log('get document.documentElement.scrollHeight');
		}
		return document.documentElement.scrollHeight;
	}

	// old version browser
	else if (document.body) {
		if (window.console) {
			window.console.log('get document.body.scrollHeight');
		}
		return document.body.scrollHeight;
	}
	return false;
}

/**
 * @name Get window width  获取窗口的宽度
 * @returns Int
 * 依赖 log()
 */
function getWindowWidth() {
	// Google Chrome & Firefox & Internet Explorer 11
	if (document.compatMode == "CSS1Compat") {
		if (window.console) {
			window.console.log('get document.documentElement.clientWidth');
		}
		return document.documentElement.clientWidth;
	}

	// old version browser
	else {
		if (window.console) {
			window.console.log('get document.body.clientWidth');
		}
		return document.body.clientWidth;
	}
	return false;
}

/**
 * @name Get window height  获取窗口的高度
 * @returns Int
 * 依赖 log()
 */
function getWindowHeight() {
	// Google Chrome & Firefox & Internet Explorer 11
	if (document.compatMode == "CSS1Compat") {
		if (window.console) {
			window.console.log('get document.documentElement.clientHeight');
		}
		return document.documentElement.clientHeight;
	}

	// old version browser
	else {
		if (window.console) {
			window.console.log('get document.body.clientHeight');
		}
		return document.body.clientHeight;
	}
	return false;
}

function back() {
	window.history.back();
}

/**
 * @name set element height equal to width  设置元素的高度与宽度一致
 * @param {Object} element
 */
function setHeightEqualToWidth(element) {
	// clear inline style
	element.style.height = "";

	// set height equal to width
	element.style.height = element.offsetWidth + "px";
}

/**
 * @name set element height match window height
 * @param {Object} element
 * 依赖 getWindowHeight()
 */
function setHeightMatchWindow(element) {
	element.style.height = getWindowHeight() + 'px';
}

var lightbox_list = null;
var lightbox_current = null;

function lightbox(element, list) {

}

/**
 * set textarea auto increase height
 * @param textarea Object(TextareaHTMLElement)
 */
function setAutoIncreaseHeight(textarea) {
	// set input event
	textarea.oninput = (function() {
		if (this.scrollHeight > this.offsetHeight) {
			this.style.height = this.scrollHeight + 'px';
		}
	});
}

/**
 * view image
 * @param image  String of image url or Object of Image
 */
function viewImage(image) {
	// create ImageViewer
	var wrapper = createElement("div", {
		id: "ImageViewer"
	});
	document.body.appendChild(wrapper);
	wrapper.onclick = (function() // click to remove
		{
			this.parentNode.removeChild(this);
		});

	// create mask
	var mask = createElement("div", {
		"class": "mask"
	});
	wrapper.appendChild(mask);

	// create new image object
	var o_image = createElement("img", {
		"src": url
	});
	debugTouch(o_image);
	o_image.onload = (function() {
		// get viewer
		var wrapper = getElement("ImageViewer");
		if (!wrapper) {
			return;
		}

		// default fit to viewer size
		var wrapper_width = wrapper.offsetWidth;
		var wrapper_height = wrapper.offsetHeight;
		var image_width = this.width;
		var image_height = this.height;
		if (image_width > wrapper_width) {
			image_width = wrapper_width;
			image_height = image_width * this.height / this.width;
		}
		if (image_height > wrapper_height) {
			image_height = wrapper_height;
			image_width = image_height * this.width / this.height;
		}
		this.style.width = image_width + "px";
		this.style.height = image_height + "px";
		this.style.top = ((wrapper_height / 2) + "px");
		this.style.left = ((wrapper_width / 2) + "px");
		this.style.marginTop = ((image_height / -2) + "px");
		this.style.marginLeft = ((image_width / -2) + "px");
		this.style.display = "hidden";

		wrapper.appendChild(this);
		jQuery(this).fadeIn();
	});

}
//viewImage("http://192.168.1.58/public/images/user-nopic.jpg");

/**
 * console log text
 * @param text  String  log content
 */
function log(text) {
	if (window.console) {
		window.console.log(text);
	}
}

function goToUrl(url) {
	window.location.href = url;
}

/**
 * set image fill
 * 设置图片为等比例放大填充容器，前提要容器是固定高度和宽度，且图像的样式不能设置为固定宽度和固定高度。
 * @param image Object(ImageHTMLElement)
 */
function setImageFill(image) {
	
	image.onload = (function() // set 'onload' event  设置 'onload' 事件
	{
		// get image width & height  获取图像显示的宽度和高度
		var width = this.offsetWidth;
		var height = this.offsetHeight;

		if (width > height) {
			this.style.height = "100%";
			this.style.width = "auto";
		} else {
			this.style.width = "100%";
			this.style.height = "auto";
		}

		// cancel the max width limit  取消最大宽度限制
		this.style.maxWidth = "none";
	});

	// get image width & height  获取图像显示的宽度和高度
	var width = image.offsetWidth;
	var height = image.offsetHeight;

	// check if loaded
	if (width > 0 && height > 0) // already loaded  已经载入完毕
	{
		if (width > height) {
			image.style.height = "100%";
			image.style.width = "auto";
		} else {
			image.style.width = "100%";
			image.style.height = "auto";
		}
		image.style.maxWidth = "none";
		image.style.minHeight = "none";
	}

}

/**
 * set element vertical middle
 * 设置元素垂直居中,前提要容器是固定高度和宽度
 * @param element Object(HTMLELement)
 */
function setVerticalMiddle(element) {
	
	element.onload = (function() {
		
		// get wrapper height
		var wrapper = this.parentNode;
		var wrapper_height = wrapper.offsetHeight;

		// get element height
		var element_height = this.offsetHeight;
		if (!element_height) {
			return;
		}

		// 上边距为高度差的一半
		var margin_top = (wrapper_height - element_height) / 2;

		// 向下取整
		margin_top = Math.floor(margin_top);

		// 边距为正数时设置相对偏移定位
		if (margin_top > 0) {
			this.style.position = 'relative';
			this.style.top = margin_top + 'px';
		}
	});

	element.onload();
}

function setAutoWidth(element) {
	var childs = element.childNodes;
	var wrapper_width = 0;
	var margin_left = 0;
	var margin_right = 0;
	for (var i = 0; i < childs.length; i++) {
		if (childs[i].nodeType == 1) {
			margin_left = jQuery(childs[i]).css('margin-left');
			margin_left = parseInt(margin_left.replace('px', ''));
			margin_right = jQuery(childs[i]).css('margin-right');
			margin_right = parseInt(margin_right.replace('px', ''));

			wrapper_width += childs[i].offsetWidth + margin_left + margin_right;
		}
	}
	element.style.width = wrapper_width + 'px';
}

//调用APP的命令
function sendAppCommand(cmd, param) {
	//  alert(param);
	var UA = getOperatingSystem();
	if (isApp()) {
		if (UA == 'iOS') {
			var url = "vitalia:app*" + cmd + "*" + param;
			//      log(url);
			//      alert(url);
			document.location = url;
		} else if (UA == 'Android') {
			var _do = 'window.app.' + cmd + '(\'' + param + '\');';
			//      log(_do);
			//      alert(_do);
			try {
				eval(_do);
			} catch (e) {
				log(e);
			}
		}
	} else {
		window.location.href = param;
	}
	return true;
}

//判断是不是APP（IOS或者Android)
function isApp() {
	if (platform == 'app') {
		return true;
	} else {
		return false;
	}
}

//正则检查手机号码
function checkMobileFormat(str) {
	var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
	if (!myreg.test(str)) {
		return false;
	}
	return true;
}

/**
 * 钉住某个元素
 * @param {Object} element
 * 依赖 jQuery() 和 getScrollTop()
 */
function pin(element) {
	element.style.position = 'relative';
	element.style.top = getScrollTop() + 'px';

	jQuery(window).bind('scroll', function() {
		
		element.style.top = getScrollTop() + 'px';
		
	});

}

