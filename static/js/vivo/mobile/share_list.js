/**
 * scripts for share list
 * 已弃用
 */

/* set share item onclick event */
jQuery("#shareWrapper .item_share").each(function()
{
  setShareItemAction(this);
});

/* fix share list images */
var share_photos = querySelectorAll(".item_share .item_photos li");
if (share_photos)
{
  for (var i=0; i<share_photos.length; i++)
  {
    // fix image wrapper size
    setHeightEqualToWidth(share_photos[i]);
    
    // fix image size
    var images = share_photos[i].getElementsByTagName("img");
    
    for (var j=0; j<images.length; j++)
    {
      setImageFill(images[j]);
    }
  }
}

jQuery("#shareWrapper").ready(function()
{
  jQuery(".list_share .item_photos .image-list-item a").each(function()
  {
    setViewImageAction(this);
  });
});



// debug
(function()
{

})();

// share list request lock
var lock_request_share_list = false;

//
var share_list_section_index = 1;

//
window.onscroll = (function()
{
  var document_height = document.documentElement ? document.documentElement.offsetHeight : document.body.offsetHeight;
  if ( getScrollTop() + getWindowHeight() > getScrollHeight() - document_height * 0.2 )
  {
    // load more share
    loadShares();
  }
});

loadShares();

function onOrientationChange(event)
{
  /* fix share list images */
  var share_photos = querySelectorAll(".item_share .item_photos li");
  if (share_photos)
  {
    for (var i=0; i<share_photos.length; i++)
    {
      // fix image wrapper size
      setHeightEqualToWidth(share_photos[i]);

      // fix image size
      var images = share_photos[i].getElementsByTagName("img");

      for (var j=0; j<images.length; j++)
      {
        setImageFill(images[j]);
      }
    }
  }
}

if ( typeof(window.onorientationchange) !== "undefined" )
{
  window.addEventListener("orientationchange", onOrientationChange);
}
else if (typeof(window.onresize) !== "undefined")
{
  window.addEventListener("resize", onOrientationChange);
}

