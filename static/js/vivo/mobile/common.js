/* scripts for all pages */

jQuery(document).ready(function()
{
	// initialize all elements what need to auto increase height
	jQuery(".auto_add_height").each(function ()
	{
	  setAutoIncreaseHeight(this);
	});
	
  var forms = document.getElementsByTagName("form");
  for (var i=0; i<forms.length; i++)
  {
    // set submit event
    forms[i].onsubmit = (function()
    {
      // get required controls
      var required_controls;
      if ( document.getElementsByClassName )
      {
        // use HTML5 DOM API
        required_controls = this.getElementsByClassName("required");
      }
      
      // check required controls
      for (var i=0; i<required_controls.length; i++)
      {
        if (required_controls[i].value === "")
        {
          required_controls[i].focus();
          return false;
        }
      }
      
      // return
      return true;

    });
  }
  
  jQuery(window).scroll(function()
  {
    // auto load more post(comment)
    var replylist = jQuery(".replylist");
    if (replylist.length)
    {
      var window_bottom = getScrollTop() + getWindowHeight();
      var replylist_bottom = replylist[0].offsetTop + replylist[0].offsetHeight;
      
      if (window_bottom >= replylist_bottom)
      {
        loadMoreComment();
      }
      
    }
  });
  
  jQuery(window).bind('onorientationchange' in window ? 'onorientationchange' : 'onresize', function(){
  	  
  	  jQuery(document.body).css({
  	  	  'border-left': 'none',
		  'border-right': 'none',
  	  });
  	  
	  var window_width = getWindowWidth();
	  var window_height = getWindowHeight();
	  log(window_width);
	  log(window_height);
	  var orientation_type = window_width < window_height ? 'portrait' : 'landscape';	// 竖屏还是横屏？
	  
	  if ( orientation_type === 'landscape' && window_width > 640 ) {
	  	
	  	  /*var padding = (window_width - 640) / 2;
		  jQuery(document.body).css({
		  	  'border-left': 'solid ' + padding + 'px #f4f4f4',
		  	  'border-right': 'solid ' + padding + 'px #f4f4f4',
		  });*/
		 
	  }
	  
  }).trigger('onorientationchange' in window ? 'orientationchange' : 'resize');
  
  jQuery(window).on('resize', function(){
  	jQuery('.proportional').each(function(){
  		var width = jQuery(this).width();
  		var proportion = jQuery(this).data('proportion') || 1;
  		var height = width / proportion;
  		jQuery(this).css('height', height + 'px');
  	});
  }).resize();

    // 预加载图片
    preloadImage();
  
});

// 预加载的图片
preload_image = new Array(
    '/static/image/vivo/mobile/progress_bar.png',
    '/static/image/vivo/mobile/progress_bar_owe.png',
    '/static/image/vivo/mobile/go_to_earn_integral.png',

    '/static/image/vivo/mobile/spotlight.png',
    '/static/image/vivo/mobile/integral_icon.png'
);

/**
 * 预加载图片函数
 */
function preloadImage() {

    //global preload_image;

    if (!preload_image.length) {
        return;
    }

    var item = preload_image.shift();

    var image = new Image();
    image.src = item;
    if (image.complete) {
        preloadImage();
    }
    else {
        image.onload = function() {
            preloadImage();
        }
    }

}




