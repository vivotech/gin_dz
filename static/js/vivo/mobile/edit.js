/* scripts for edit page */

// add image item
function addImageItem(image_src)
{
  // get wrapper
  var wrapper = getElement("form_text_wrapper");
  
  // create image item
  var item_image = createElement("div", {"class": "item_image"});
  wrapper.appendChild(item_image);
  
  // create image
  var image = createElement("img", {
    "src": image_src,
    "alt": "",
    "class": "form_text_section"
  });
  item_image.appendChild(image);
  
  // remove image
  var item_remove = createElement("a", {
    "class": "item_remove",
    "href": "javascript:;"
  }, "X");
  item_remove.onclick = (function(){
    jQuery(this).parent().remove();
  });
  item_image.appendChild(item_remove);
  
  // create text item
  var item_text = createElement("div", {"class": "item_text"});
  var text = createElement("textarea", {"class": "form-control form_text_section auto_add_height"});
  text.oninput = (function()
  {
    autoAddHeight(this);
  });
  item_text.appendChild(text);
  wrapper.appendChild(item_text);
  
  // remove image
  var item_remove = document.createElement("a", {
    "class": "item_remove",
    "href": "javascript:;"
  }, "X");
  item_remove.onclick = (function(){
    jQuery(this).parent().remove();
  });
  item_text.appendChild(item_remove);
  
}

/*
jQuery("#upload_image").click(function(){
	jQuery("#form_upload_file").click();
});*/

var file_reader = new FileReader();
var image = new Image();
var canvas = createElement("canvas");
var max_width_image = 300;
var max_height_image = 300;

image.onload = (function()
{
  var ratio = 1;
  var canvasTemp = createElement("canvas");
  
  if (this.width > max_width_image)
  {
    ratio = max_width_image / this.width;
  }
  else if (this.height > max_height_image)
  {
    ratio = max_height_image / this.height;
  }
  
  canvasTemp.width = this.width;
  canvasTemp.height = image.height;
  canvasTemp.getContext("2d").drawImage(image, 0, 0);
  
  canvas.width = this.width * ratio;
  canvas.height = this.height * ratio;
  var ctx = canvas.getContext("2d");
  ctx.drawImage(image, 0, 0, canvasTemp.width, canvasTemp.height, 0, 0, canvas.width, canvas.height);
  
  var image_data = canvas.toDataURL("image/jpeg");
  
  image_data = image_data.replace("data:image/jpeg;base64,", "");
  image_data = image_data.replace(" ", "+");
  //var binaryData = new BinaryFile(atob(image_data));
  
  //alert(atob(image_data));
  
  /*
  for (var i in image_data_object)
  {
    image_data += image_data_object[i];
  }*/
  //alert(image_data);
  
  //document.body.appendChild(canvas);
  //var  image_data = ctx.getImageData(0, 0, canvas.width, canvas.height);
  //var new_image = createImage(image_data);
  //document.body.appendChild(new_image);
  
  var request = new XMLHttpRequest();
  request.open("POST", "http://upload.qiniu.com", true);
  
  var post_data_array = new Array();
  var frontier = "--ginye--";
  post_data_array["uploadToken"]  = getElement("form_token_qiniu").value;
  post_data_array["key"]          = key_image_upload;
  post_data_array["xVariableValue"] = "1";
  post_data_array["crc32"] = "";
  post_data_array["acceptContentType"] = "";
  post_data_array["fileBinaryData"] = "";
  
  // set post data
  var post_data = "";
  
  post_data += '--' + frontier + "\r\n";
  post_data += 'Content-Disposition:       form-data; name="token"' + "\r\n\r\n";
  post_data += post_data_array["uploadToken"] + "\r\n";
  
  post_data += '--' + frontier + "\r\n";
  post_data += 'Content-Disposition:       form-data; name="key"' + "\r\n\r\n";
  post_data += post_data_array["key"] + "\r\n";
  
  post_data += '--' + frontier + "\r\n";
  post_data += 'Content-Disposition:       form-data; name="accept"' + "\r\n\r\n";
  post_data += post_data_array["xVariableValue"] + "\r\n";
  
  post_data += '--' + frontier + "\r\n";
  post_data += 'Content-Disposition:       form-data; name="file"; filename="upload.jpg"' + "\r\n";
  post_data += 'Content-Type:              application/octet-stream' + "\r\n";
  post_data += 'Content-Transfer-Encoding: binary' + "\r\n\r\n";
  post_data += atob(image_data) + "\r\n";
  
  request.setRequestHeader("Host", "upload.qiniu.com");
  request.setRequestHeader("Content-Type", "multipart/form-data; boundary=" + frontier);
  request.setRequestHeader("Content-Length", post_data.length);
  
  post_data += '--' + frontier + "--";
  
  request.send(post_data);
  
  updateImageUploadKey();
});

var form_upload_file = getElement("form_upload_file");
if (form_upload_file)
{
  form_upload_file.onclick = (function()
  {
    // create new file reader
    
    file_reader.readAsDataURL(this.files[0]);
    
    file_reader.onloadend = (function()
    {
      image.src = this.result;
    });
    
  });
}

// file upload
var qiniu_uploader;
jQuery(function ()
{
  'use strict';
  //document.getElementById("form_token_qiniu").value = "udnFsYdB2Rdj-szWela1VHlP53GY-EtuXYVJXrtH:pGqyMzMTu1R-ojR7vm-1eEynnLA=:eyJzY29wZSI6InZpdm8iLCJkZWFkbGluZSI6MTQyMjQxMjM0MH0=";
  
  // get image upload token
  
  $.ajax(url_image_upload_token, {
    success: (function(result)
    {
      getElement("form_token_qiniu").value = result;
      
      // qiniu image uploader
      qiniu_uploader = Qiniu.uploader({
        runtimes: 'html5,flash,html4',
        browse_button: 'upload_image',
        //uptoken_url: url_image_upload_token,
        uptoken: result,
        domain: 'http://7u2qq8.com1.z0.glb.clouddn.com/',
        max_file_size: '5mb',
        flash_swf_url: '../plupload-2.1.2/js/Moxie.swf',
        max_retries: 1,
        chunk_size: '1mb',
        auto_start: true,
        //unique_names: true,
        init: {
          'FileUploaded': function(up, file, info)
          {
            var dialog_id = "progressDialog-" + file.id;
            removeProgressDialog(dialog_id);
            
            var domain = up.getOption('domain');
            var res = $.parseJSON(info);
            var sourceLink = domain + res.key;
            addImageItem(sourceLink);
            updateImageUploadKey();
          },
          'BeforeUpload': function(up, file)
          {
            var dialog_id = "progressDialog-" + file.id;
            createProgressDialog(dialog_id, file.percent);
          },
          'UploadProgress': function(up, file)
          {
            var dialog_id = "progressDialog-" + file.id;
            updateProgressDialog(dialog_id, file.percent)
          },
          'Error': function(up, err, errTip) {
            //alert(errTip);
          },
          'Key': function(up, file) {
            return generateImageKey(file);
          }
        }
      });
      
    })
  });
  
});

// send form
jQuery("#form_share_edit").ready(function()
{
	jQuery("#form_share_edit .button_send").click(function()
  {
		var form_content = getElement("form_content");
		form_content.value = "";
		var text_sections = jQuery("#form_share_edit .form_text_section");
		for (var i=0; i<text_sections.length; i++)
		{
			if ( text_sections[i].nodeName === "TEXTAREA" )
			{
        if (text_sections[i].value !== "")
        {
          form_content.value += "<div>" + text_sections[i].value + "<br /></div>";
        }
			}
			else if ( text_sections[i].nodeName === "IMG" )
			{
				form_content.value += "<div>" + text_sections[i].outerHTML + "</div>";
			}
		}
    
    // submit
		getElement("form_share_edit").submit();
    
	});
  
});





