/* actions for mobile product page */

jQuery(document).ready(function ()
{
	
  // 设置商品倒数
  if ( jQuery("#productCountTimer").length ) {
  	  jQuery("#productCountTimer").countTimer({
	    "beginTime": TIMESTAMP,
	    "endTime": product_expiration
	  });
  }
	  
  // 设置购买数量按钮组
  jQuery(".productDetail .decreaseButton").click(function ()
  {
    var group = jQuery(this).parent();
    var input = group.find("input");
    var count = parseInt(input.val());
    if (count > 0)
    {
      count -= 1;
    }
    input.val(count);
    input.change();
  });
  jQuery(".productDetail .increaseButton").click(function ()
  {
    var group = jQuery(this).parent();
    var input = group.find("input");
    var count = parseInt(input.val());
    input.val(count + 1);
    input.change();
  });
  // 检查购买数量
  jQuery("#buyCount").change(function () {
    var count = parseInt(this.value);
    var buyAbleNum = parseInt(document.getElementById("buyAbleNum").value);
    if (buyAbleNum == -1)
    {
      return;
    }
    if (count > buyAbleNum)
    {
      this.value = buyAbleNum;
    }
  });
  
  // product images drag  商品图片拖动
  var productImagesSwiper = new Swiper('.swiper-container', {
    // Optional parameters
    //direction: 'vertical',
    //loop: true,
    pagination: '.swiper-container .swiper-pagination'
  });
  jQuery(".swiper-container .swiper-slide a").each(function(){
  	
  	// 设置专辑标记
  	jQuery(this).attr('rel', 'productThumbnail');
  	
  	// 设置图片查看器
    setViewImageAction(this);
  });
  
  // 延迟加载图片
  if (jQuery(".productDetail .productDescription").length) {
	  jQuery(".productDetail .productDescription img").lazyload({
	  	placeholder: "/static/jquery/jquery_lazyload/img/transparent.gif",
	  	load: function(){
	  		jQuery(this).removeClass('notready');
	  	}
	  });
  }
  
  // product detail images click to view  商品详情图片点击查看
  jQuery(".productDetail .productDescription img").each(function(){
  	
  	// 设置专辑标记
  	jQuery(this).attr('rel', 'productDescription');
  	
  	// 设置图片查看器
    setViewImageAction(this);
  });
  
});

