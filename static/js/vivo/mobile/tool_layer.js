/**
 * Created by wen on 2015/8/1.
 */

jQuery('.tool_layer').ready(function(){

    // 动作按钮
    jQuery('.tool_layer .main_buttons .action_button .icon').click(function(){

        var status = jQuery(this).data('status');
        var object_id = jQuery(this).data('object');
        var object = jQuery('#'+object_id);

        if (status == 'closed') {
            jQuery(this).parents('.main_buttons .button').addClass('active');
            object.show().addClass('active');
            jQuery(this).data('status', 'opened');
        } else {
            jQuery(this).parents('.main_buttons .button').removeClass('active');
            object.hide().removeClass('active');
            jQuery(this).data('status', 'closed');
        }
    });

    // 喜欢
    jQuery('.tool_layer .action_sub_buttons .saygood_button .icon').click(function(){

        var parent = jQuery(this).parent();
        var is_liked = parent.hasClass('active');

        if (!is_liked)
        {
            jQuery.ajax('/app.php', {
                'data': {
                    'mod': 'forum',
                    'act': 'saygood',
                    'id': threadId
                },
                'dataType': 'json',
                'success': function(result){

                    if (result.result == 1) {
                        jQuery('.tool_layer .action_sub_buttons .saygood_button').addClass('active');

                    }

                    // 检查积分奖励
                    checkCreditPrompt();
                }
            });
        }
        else {
            jQuery.ajax('/app.php', {
                'data': {
                    'mod': 'forum',
                    'act': 'saygood',
                    'id': threadId,
                    'op': 'delete'
                },
                'dataType': 'json',
                'success': function(result){
                    if (result.result == 1)
                    {
                        jQuery('.tool_layer .action_sub_buttons .saygood_button').removeClass('active');
                    }

                }
            });
        }
    });

    // 收藏
    jQuery('.tool_layer .action_sub_buttons .favorite_button .icon').click(function(){

        var parent = jQuery(this).parent();
        var is_favorited = parent.hasClass('active');

        if (!is_favorited)
        {
            jQuery.ajax('/app.php', {
                'data': {
                    'mod': 'forum',
                    'act': 'favorite',
                    'id': '{$thread[tid]}'
                },
                'success': function(){
                    jQuery('.tool_layer .action_sub_buttons .favorite_button').addClass('active');
                }
            });
        }
        else {
            jQuery.ajax('/app.php', {
                'data': {
                    'mod': 'forum',
                    'act': 'favorite',
                    'id': '{$thread[tid]}',
                    'op': 'delete'
                },
                'success': function(){
                    jQuery('.tool_layer .action_sub_buttons .favorite_button').removeClass('active');
                }
            });
        }

    });
});