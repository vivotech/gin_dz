/**
 * community function
 * 社区功能
 */

/**
 *  set view image action  设置查看图片的动作
 *  @param element  Object(HTMLElement)
 */
function setViewImageAction(element)
{
	if ( element.nodeName != "A" && element.nodeName != "IMG" ) {
		log('不是链接或图片');
		return false;
	}
	
	// app 的点击
	if ( getUrlParam("platform") === "app" ) {
		
		element.onclick = (function (event) {
			
			var url_image = "";
		    if ( this.nodeName === "A" )
		    {
		      url_image = $(this).find("img").attr("src");
		    }
		    else
		    {
		      url_image = $(this).attr("src");
		    }
		    
		    if ( url_image.indexOf("http") < 0 )
		    {
		      url_image = location.origin + "/" + url_image;
		    }
		    sendAppCommand("showImage", url_image);
		    return false;
		});
		
	}
    else
    {
    	if ( element.nodeName == 'A') {
    		jQuery(element).touchTouch();
    	}
    	else {
    		element.onclick = (function (event) {
    		
	    		var items = new Array();
	    		
	    		var item_index = 0;
	    		
	    		var options = {};
	    		
	    		// get gallery ID  获取画廊ID
			    var gallery_id = this.getAttribute("rel") || jQuery(this).data('gallery_id');
			    
			    // 获取画廊的项目
			    var gallery_items = gallery_id ? jQuery("*[rel='" + gallery_id + "']") : jQuery(this);
		    	gallery_items.length ? gallery_items = gallery_items.not(".notready") : null;
		    	
		    	var item_index = gallery_id ? gallery_items.index(this) : 0;
		    	options.index = item_index;
		    	
		    	for (var i = 0; i<gallery_items.length; i++) {
					  
					var image_source = gallery_items[i].nodeName === "A" ?
					    gallery_items.eq(i).attr('href') :
					    gallery_items.eq(i).attr('src');
					if ( image_source.indexOf('?') >= 0 ) {
						image_source = image_source.substr(0, image_source.indexOf('?'));
					}
					
					var image_small = gallery_items[i].nodeName === "A" ?
					    gallery_items.eq(i).find('img').attr('src') :
					    gallery_items.eq(i).attr('src');
					
				    var oImage = new Image();
					oImage.src = image_small;
					
					var item = {
						src: image_source,
						msrc: image_small,
						w: oImage.width,
						h: oImage.height,
						loading: false,
						needsUpdate: true
					};
					
					items.push(item);
				}
				
				// Initializes and opens PhotoSwipe
				var gallery = new PhotoSwipe( document.querySelectorAll('.pswp')[0],
					PhotoSwipeUI_Default, items, options);
				
				gallery.init();
				
				return false;
				
	    	});
    	}
    	
    }
}

function setShareAction(element)
{
  element.onclick = (function()
  {
    var item = jQuery(this).parents("#shareWrapper .item_share");
    var share_url = item.data("href");
    var share_title = item.find(".item_title").text();
    
    var dialog = createDialog("分享");
    document.body.appendChild(dialog);
    
    var dialog_body = getChildByClassName(dialog, "dialog_body");
    var link_weibo = createElement("a", {
      "href": "javascript:;",
      "class": "link_weibo_share"
    }, "微博");
    link_weibo.onclick = (function()
    {
      shareToWeibo(share_url, share_title);
    });
    dialog_body.appendChild(link_weibo);
    
    /*
    shareToWeibo(item.data("href"), item_title.text());
    */
  
    // return
    return false;
  });
}

/**
 * set share item click action
 * @param item  Object(HTMLElement)
 */
function setShareItemAction(item)
{
  item.onclick = (function (event)
  {
    if (!jQuery(event.srcElement).parents(".image-list-item").length &&
      !jQuery(event.srcElement).parents(".item_actions").length
      )
    {
      var href = this.dataset.href;
      if (href)
      {
        goToUrl(href);
      }
    }

  });
}

/**
 * set reply action for reply link
 * @param element  Object(HTMLElement)
 */
function setReplyAction(element)
{
  // set click event
  element.onclick = (function ()
  {
    // get reply item  获取回复项目
    var item = jQuery(this).parents("#commentList > li");

    // get comment id  获取评论ID
    var comment_id = item.data("comment-id");

    // set reply comment id
    reply_comment_id = comment_id;

    // open the reply panel
    openReplyPanel(item[0], {
      "share_id": getElement("ShareId").value,
      "share_comment_id": comment_id
    });

    // return
    return false;
  });
}

function setCommentAction(element)
{
  element.onclick = (function ()
  {
    // get form
    var form = jQuery(this).parents(".form_comment");

    // record comment content
    comment_content = form.find("textarea[name='content']").val();

    var url = "/index.php/Community/ajax/ajaxaddcomment";

    $.ajax(url, {
      type: "post",
      data: form.serialize(),
      dataType: "json",
      success: function (result)
      {
        if (typeof (result.reply_time) === "undefined")
        {
          if (typeof (result.id) !== "undefined" && result.id === "-1")
          {
            location.href = addUrlParam(url_login, "redirect", location.href);
          }
          return;
        }

        // get reply panel
        var reply_panel = getElement("replyPanel");
        if (reply_panel)
        {
          reply_panel.style.display = "none";
        }

        renderOneComment(result);

        /*
         
         // get list
         var list_comment = getElement("commentList");
         
         // create item
         var item = createElement("li", {
         "id": "item_comment_" + result.id
         });
         item.setAttribute("data-comment-id", result.id);
         if ( list_comment.childNodes.length < 1 )
         {
         list_comment.appendChild(item);
         }
         else
         {
         list_comment.insertBefore(item, list_comment.childNodes[0]);
         }
         
         // create item image
         var item_image = createElement("div", {'class': "item_image"},
         createElement("a", {href: url_user},
         createElement("img", {src: result.avatar})
         )
         );
         item.appendChild(item_image);
         
         // create item content
         var item_content = createElement("p", {'class': "item_content"});
         item_content.innerHTML = '<a class="comment_author" href="' + url_user + '">@' + user.name + '</a>：';
         
         if (reply_comment_id)
         {
         var reply_comment_item = getElement("item_comment_" + reply_comment_id);
         if (reply_comment_item)
         {
         var reply_comment_author = jQuery(reply_comment_item).find(".comment_author");
         item_content.innerHTML += "回复" + reply_comment_author[0].outerHTML + "：";
         }
         }
         
         item_content.innerHTML += comment_content;
         item.appendChild(item_content);
         
         // .item_info
         var item_info = createElement("div", {'class': "item_info"});
         item.appendChild(item_info);
         
         // .publish_time
         item_info.appendChild(createElement("div", {'class': "publish_time"}, result.reply_time));
         
         // .opt
         item_info_opt = createElement("div", {'class': "opt"});
         item_info.appendChild(item_info_opt);
         opt_list = createElement("ul");
         item_info_opt.appendChild(opt_list);
         
         // reply item
         var opt_list_reply_item = createElement("li");
         var link_reply = createElement("a", {href: "javascript:;"}, "回复");
         setReplyAction(link_reply);
         opt_list_reply_item.appendChild(link_reply);
         opt_list.appendChild(opt_list_reply_item);
         
         // praise
         var opt_list_praise_item = createElement("li");
         var link_praise = createElement("a", {href: "javascript:;"}, "赞<span>0</span>");
         setPraiseAction(link_praise);
         opt_list_praise_item.appendChild(link_praise);
         opt_list.appendChild(opt_list_praise_item);
         */

        // unlock comment
        comment_lock = false;
      },
      error: function ()
      {
        comment_lock = false;
      }
    });
  });
}

/**
 * render one share  渲染一个分享
 * @param data  Object  options of the share
 */
function renderOneShare(data)
{
  if (first_share_id < 1)
  {
    first_share_id = parseInt(data.share_id);
  }
  last_share_id = parseInt(data.share_id);

  // create item
  var item = createElement("li", {// item
    "class": "item_share default-border default-radius",
    "id": "item_share_" + data.share_id,
    "data-href": "/community/share/sharedetail?share_id=" + data.share_id
  });
  var wrapper = getElement("shareWrapper");
  wrapper.getElementsByTagName("ul")[0].appendChild(item);
  setShareItemAction(item);

  // create item header
  var item_header = createElement("div", {// item_header
    "class": "item_header"
  });
  item.appendChild(item_header);
  
  // set user url
  var url_user_detail = addUrlParam("/community/user/userPersonalIndex", "user_id", data.user_id);

  // item author image
  var item_author_image = createElement("div", {"class": "item_author_image"}, // item_author_image
    createElement("a", {
      "href": url_user_detail
      },
      createElement("img", {
        "src": data.avatar ? data.avatar : "/public/images/brand-img.jpg",
        "alt": "头像"
      })
    )
  );
  item_header.appendChild(item_author_image);

  // item_author_name
  var item_author_name = createElement("h2", {"class": "item_author_name"},
    createElement("a", {
      "href": url_user_detail
    }, data.user_name)
  );

  // item publish time
  var item_publish_time = createElement("p", {"class": "item_publish_time"}, data.share_time);    // item_publish_time

  // add form source
  if (data.from_source)
  {
    item_publish_time.appendChild(createElement("span", {}, "来自：" + data.from_source));
  }

  item_header.appendChild(item_author_name);
  item_header.appendChild(item_publish_time);

  var item_title = createElement("div", {"class": "item_title"}, data.share_title);
  var item_text = createElement("div", {"class": "item_text"}, data.share_content_withouthtml);

  item.appendChild(item_title);
  item.appendChild(item_text);

  var item_photos = createElement("div", {"class": "item_photos"});
  item.appendChild(item_photos);

  var wrapper_item_photos = createElement("div", {"class": "wrapper_item_photos"});
  item_photos.appendChild(wrapper_item_photos);

  var ul_item_photos = createElement("ul");
  wrapper_item_photos.appendChild(ul_item_photos);

  var media_list = $.parseJSON(jQuery("<div/>").html(data.media_list).text());

  var count_pic = 0;
  for (var i = 0; i < media_list.length; i++)
  {
    if (media_list[i].media_type == "0")
    {
      var item_photo = createElement("li", {"class": "image-list-item default-border default-radius"});
      ul_item_photos.appendChild(item_photo);
      var item_photo_link = createElement("a", {
        "rel": "gallery-share-" + data.share_id,
        "href": media_list[i].media_url
      });
      item_photo.appendChild(item_photo_link);
      setViewImageAction(item_photo_link);

      var item_photo_img = createElement("img", {
        "src": media_list[i].media_url,
        "alt": data.share_title
      });
      item_photo_link.appendChild(item_photo_img);

      setHeightEqualToWidth(item_photo);
      setImageFill(item_photo_img);

      count_pic++;
    }
  }

  if (count_pic > 3)
  {
    wrapper_item_photos.appendChild(createElement("span", {}, "共" + count_pic + "张"));
  }

  var item_actions = createElement("p", {"class": "item_actions"});
  var a_share = createElement("a", {"href": "/community/share/sharedetail?share_id=" + data.share_id});
  a_share.innerHTML += '<i class="icon icon-share"></i>分享';
  setShareAction(a_share);
  var a_comment = createElement("a", {"href": "/community/share/sharedetail?share_id=" + data.share_id});
  a_comment.innerHTML += '<i class="icon icon-comment"></i>评论(' + data.comment_count + ')';
  var a_praise = createElement("a", {
    "href": "javascript:;",
    "class": data.is_saygood > 0 ? "praise praised" : "praise",
    "data-content": data.share_id,
    "data-type": data.is_saygood
  });
  setPraiseAction(a_praise);
  a_praise.innerHTML += '<i class="icon icon-praise"></i>(<span class="number">' + data.saygood_count + '</span>)';
  item_actions.appendChild(a_share);
  item_actions.appendChild(a_comment);
  item_actions.appendChild(a_praise);

  item.appendChild(item_actions);

  //alert(getElement("shareWrapper").childNodes[0]);

}

/**
 * render no more
 */
function renderNoMore()
{
  var item = createElement("li", {"class": "item_share noMore-item default-border default-radius"}, '<p class="text-center">没有更多了。</p>');
  getElement("shareWrapper").getElementsByTagName("ul")[0].appendChild(item);
}

var lock_saygood = false;
/**
 * set say good click action  设置点赞动作
 * @param element object(HTMLElement)
 */
function setSayGood(element)
{
  element.onclick = (function()
  {
    if (lock_saygood)
    {
      return;
    }
    
    lock_saygood = true;
    jQuery.ajax('forum.php', {
      'data' : {
        'mod' : 'ajax', 'action'  : 'saygood', 'type': 'tid',
        'pid' : jQuery(this).data('content')
      },
      'dataType' : 'json',
      'success' : function(result){
        if (result.state === 'success')
        {
          updateSayGoodState(result.pid, 1);
        }
        else
        {
          checkAjaxError(result);
        }
        lock_saygood = false;
      }
    });
  });
}

/**
 * update say good state  更新点赞状态
 * @param pid number  post ID
 * @param state number 0|1  0 为取消点赞 1 为点赞
 */
function updateSayGoodState(pid, state)
{
  var j_saygood = jQuery("#pid"+pid+" .postOpt .saygood");
  
  // update saygood number
  var j_saygood_number = j_saygood.find(".number");
  var number = parseInt(j_saygood_number.html());
  state ? j_saygood_number.html(number+1) : j_saygood_number.html(number-1);

  // update image
  var j_image = j_saygood.find("img");
  var image_src = j_image.attr("src");
  if (state)
  {
    image_src = image_src.replace('ico_good_1.gif', 'ico_good_2.gif');
  }
  else
  {
    image_src = image_src.replace('ico_good_2.gif', 'ico_good_1.gif');
  }
  j_image.attr("src", image_src);
}

/**
 * @name render a reply item  渲染一个回复项目
 * @param data array  reply data 新回复的数据
 * @return HTMLElement  element node
 */
function renderReply(data)
{
  var model = getElement("replyModel");
  if (!model) { return null; }
  var item = jQuery(model.outerHTML);
  
  // set ID
  item.attr("id", "pid"+data.pid);
  item.removeClass("model").data('pid', data.pid);

  // set author link  设置作者链接
  item.find(".author").attr("href", "home.php?mod=space&mobile=2&uid="+data.authorid)
  .click(function()
  {
    var author_id = data.authorid;
    var url = "forum.php?mod=personal&uid=" + author_id;
    
    // for mobile App
    if ( getUrlParam("platform") === "app" )
    {
      sendAppCommand("viewAuthor", author_id);
    }
    // normal web platform
    else
    {
      location.href = url;
    }
    
    return false;
  });

  // set avatar  设置作者头像
  item.find(".author .avatar").attr("src", data.avatar);

  // set author name  设置作者昵称
  item.find(".author .name").html(data.author)
  
  // set image  设置图片
  if ( data.images ) {
  	
  	  item.find(".imagesWrapper").append(jQuery('<ul></ul>'));
  	
  	  for (var i=0; i<data.images.length; i++) {
  	  	var imageItem = jQuery('<li></li>');
  	  	var imageLink = jQuery('<a href="' + data.images[i] + '"></a>');
	  	var newImage = jQuery('<img src="'+data.images[i]+'" alt="" />');
	  	
	  	item.find(".imagesWrapper ul").append(imageItem);
	  	imageItem.append(imageLink);
	  	imageLink.append(newImage);
	  	
	  	imageLink.data('gallery_id', 'gallery_post_' + data.pid);
	  }
  	  
  } else {
  	item.find(".imagesWrapper").addClass('hidden');
  }

  // set time
  item.find(".date").html(data.dateline);

  // set message
  var message = data.message;
  //alert(message);
  if ( data.rep_author )
  {
    //message += '<br />回复：' + data.rep_author + "<br />" + data.rep_text;
    message = '回复<a href="/home.php?mod=space&mobile=2&uid=' + data.rep_authorid + '">' + data.rep_author + '</a>：' + message;
  }
  item.find('.message').html(message);

  // set operation  设置操作对应的 Pid
  item.find(".postOpt a").attr("data-content", data.pid);

  // set saygood  设置点赞
  item.find(".postOpt .saygood img").attr("src", '/static/image/vivo/'+ (data.is_saygood ? 'ico_good_2.gif' : 'ico_good_1.gif'));
  item.find(".postOpt .saygood .number").html(data.saygood_number);

  // reply  设置回复
  item.find(".postOpt .reply").attr("href", 'forum.php?mod=post&action=reply&tid='+threadId+'&mobile=2&repquote='+data.pid);


  item.find('.date, .message').click(function(){
  	var pid = jQuery(this).parents('.plc').data('pid');
  	if (!pid) { return false; }
	location.href = '/forum.php?mod=post&action=reply&tid=4491&mobile=2&repquote=' + pid;
  });

  return item[0];
}

function showWaitLayer()
{
  var waitLayer = jQuery("#waitLayer");
  if (!waitLayer.length)
  {
    waitLayer = jQuery("<div></div>").attr("id", "waitLayer").appendTo(document.body);
  }
  
  waitLayer.show();
}

function hideWaitLayer()
{
  jQuery("#waitLayer").hide();
}

var lock_ajax_postlist = false;
var noMorePost = false;
function loadComment()
{
  var options = arguments[0] ? arguments[0] : {};
  loadNewComment(options);
}

/**
 * load new comment  加载新的评论
 * param 1 object  comment options  评论的选项
 *   'pid' number  the special post ID  特别的帖子ID
 */
function loadNewComment()
{
  if (lock_ajax_postlist)
  {
    return;
  }

  // show the wait to loading more comment  显示“正在加载更多评论”
  jQuery("#loadingMoreComment").fadeIn();

  // lock ajax get postlist  锁定“获取帖子列表”的请求
  lock_ajax_postlist = true;
  
  // get params
  var options = arguments[0] ? arguments[0] : {};
  var pid = options.pid ? options.pid : 0;

  // set page number
  jQuery.ajax("/forum.php", {
    "data": {
      "tid": threadId,
      "mod": "ajax",
      "action": "postlist",
      "pid": pid
    },
    "dataType": "json",
    "success": function(result)
    {
      var replyList = new Array();
      
      // 项目数大于0 (有回复项目数据)
      if (result.length)
      {
        for (var i=0; i<result.length; i++)
        {
          // create new reply  创建新的回复项目
          replyList[replyList.length] = renderReply(result[i]);
        }
        
        for (i=0; i<replyList.length; i++)
        {
          if (!replyList[i])
          {
            continue;
          }
          replyList[i].style.display = "none";
          jQuery(".replylist").prepend(replyList[i]);   // 最前面插入
          jQuery(replyList[i]).fadeIn();            // 淡入
          location.href = "#"+replyList[i].id;      // 定位到新插入的元素节点
        }

        lock_ajax_postlist = false;
      }

      // hide the wait to loading more comment
      jQuery("#loadingMoreComment").fadeOut();
      
      checkComment();
    }
  });
}

/**
 * load more comment (older post)
 */
function loadMoreComment()
{
  if (lock_ajax_postlist || noMorePost)
  {
    return;
  }

  // show the wait to loading more comment  显示“正在加载更多评论”
  jQuery("#loadingMoreComment").fadeIn();

  // lock ajax get postlist  锁定“获取帖子列表”的请求
  lock_ajax_postlist = true;

  // send request get post list
  jQuery.ajax("/forum.php", {
    "data": {
      "tid": threadId,
      "mod": "ajax",
      "action": "postlist",
      "pidline": comment_oldest_id,
      "timeline": comment_oldest_timeline
    },
    "dataType": "json",
    "success": function(result)
    {
      var replyList = new Array();
      
      // 项目数大于0
      if (result.length)
      {
        for (var i=0; i<result.length; i++)
        {
          // create new reply  创建新的回复项目
          replyList[replyList.length] = renderReply(result[i]);
          
          // update oldest pid  更新最旧的帖子ID
          comment_oldest_id = result[i].pid;
          
          // update oldest timeline  更新最旧的时间线
          if ( result[i].timeline < comment_oldest_timeline )
          {
            comment_oldest_timeline = result[i].timeline;
          }
        }
        
        // append to reply list wrapper  插入新的节点到回复列表的容器里
        for (i=0; i<replyList.length; i++)
        {
          if (!replyList[i])
          {
            continue;
          }
          jQuery(".replylist").append(replyList[i]);
          
          //log(jQuery(replyList[i]).find('.imagesWrapper a').length);
          var imageLinks = jQuery(replyList[i]).find('.imagesWrapper a');
          
          /*
          for (var j=0; j<imageLinks.length; j++)
          {
          	log(imageLinks[j].href);
          }*/
         
          //jQuery(.touchTouch();
          //$('.replylist .imagesWrapper a').touchTouch();
          
          // 设置图片垂直居中
          jQuery(replyList[i]).find('.imagesWrapper img').each(function(){
          	  setVerticalMiddle(this);
          });
  	
        }
        
        $('.replylist .imagesWrapper a').touchTouch();

        // unlock ajax post list  解除锁定获取帖子列表的 ajax 请求
        lock_ajax_postlist = false;
      }
      // 项目数少于每页的数量则标记没有更多帖子
      if ( result.length < numberPerPage )
      {
        noMorePost = true;
      }

      // hide the wait to loading more comment  隐藏“正在加载更多评论”的提示。
      jQuery("#loadingMoreComment").fadeOut();
      
      checkComment();
    }
  });
}

function checkComment()
{
  var comment_list = jQuery(".replylist > .plc");
  var comment_count = 0;
  
  for (var i=0; i<comment_list.length; i++)
  {
    if (comment_list.eq(i).hasClass("model"))
    {
      continue;
    }
    
    comment_count++;
  }
  
  if ( comment_count < 1 )
  {
    jQuery(".notice_has_shafa").show();
  }
}

function checkAjaxError(ajaxResult)
{
  if ( ajaxResult.error === "not login" )
  {
    if ( getUrlParam("platform") === "app" )
    {
      sendAppCommand("login", '');
    }
    else
    {
      location.href = "member.php?mod=logging&action=login&mobile=2";
    }
  }
  else
  {
    alert(ajaxResult.message);
  }
}

/**
 * @name 显示积分奖励提醒
 * @param String message
 */
function showCreditPrompt(message) {

	var layer = jQuery('<div class="creditPromptLayer">\
		<p class="message">' +message+ '</p>\
		<div class="box_image"><img src="/static/image/vivo/mobile/integral_icon.png" alt="" /></div>\
	</div>');

	jQuery(document.body).append(layer);

	var window_width = jQuery(document.body).width();

	if (window_width < 640) {
		layer.find('.message').css({
			'opacity': '0.5'
		}).animate({
			'opacity': '1',
            'top': '58px'
		}, 1000);
	}
	else {
		layer.find('.message').css({
			'opacity': '0.5'
		}).animate({
			'opacity': '1',
            'top': '120px'
		}, 1000);
	}

    // 显示遮罩
	showMask('creditPromptMask', function(){
        jQuery('.creditPromptLayer').fadeOut(function(){
            jQuery(this).remove();
        });
        hideMaskLayer('creditPromptMask');
    });

	jQuery('#creditPromptMask').css('background-color', 'rgba(0, 0, 0, 0.75)');

    // 点击关闭
    layer.click(function(){
        jQuery(this).fadeOut(function(){
            jQuery(this).remove();
        });
        hideMaskLayer('creditPromptMask');
    });
	
	// N 秒后淡出隐藏
	window.setTimeout(function(){
		jQuery('.creditPromptLayer').fadeOut(function(){
			jQuery(this).remove();
		});
		hideMaskLayer('creditPromptMask');
	}, 2000);
	
}

function checkCreditPrompt() {
	
	// 显示提示信息
	var creditPrompt = getCreditPrompt();
	if (creditPrompt) {
		showCreditPrompt(creditPrompt);
	}
}

/**
 * 显示积分交换商品介绍
 */
function showIntegralExchangeInfo() {
	
	jQuery('.integral_exchange_info').fadeIn('fast', function(){
		
		// 进度条动画
		jQuery('.integral_exchange_info .progress_wrapper .progress_bar .owe').css({
			'margin-left': '2%',
			'width': '98%'
		}).animate({
			'margin-left': '50%',
			'width': '50%'
		}, 'slow');
		
		// 指针动画
		jQuery('.integral_exchange_info .progress_wrapper .pointer').css({
			'left': '2%'
		}).animate({
			'left': '50%'
		}, 'slow');
		
	});
	
	
	
}

