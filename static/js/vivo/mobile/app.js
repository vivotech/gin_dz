/**
 * functions for application
 * author: Kevin Kuan
 */

/**
 * create a dialog  创建一个对话框
 * @param title  String  dialog title  对话框标题
 * @param text  String  dialog content text  对话框内容文本
 * argument 3  Object  dialog options  对话框选项
 * @return Object(DivHTMLElement)  返回 div 节点元素
 * 依赖 createElement()
 */
function createDialog(title, text)
{
  // get arguments
  var options  = arguments[3] ? arguments[3] : new Object();
  
  // set dialog attributes
  var dialog_attributes = {
    "class": "dialog"
  };
  
  if (options.id)
  {
    dialog_attributes.id = options.id;
  }
  
  // create new dialog
  var dialog = createElement("div", dialog_attributes);
  
  // dialog header
  var dialog_header = createElement("div", {
    "class": "dialog_header"
  });
  dialog.appendChild(dialog_header);
  
  // dialog title
  var dialog_title = createElement("div", {
    "class": "dialog_title"
  }, title);
  dialog_header.appendChild(dialog_title);
  
  // dialog buttons
  var dialog_buttons = createElement("div", {
    "class": "dialog_buttons"
  });
  dialog_header.appendChild(dialog_buttons);
  var button_close = createElement("button", {
    "class": "button_close dialog_close"
  }, "x");
  dialog_buttons.appendChild(button_close);
  button_close.onclick = (function(){
    var dialog = this.parentNode.parentNode.parentNode;
    dialog.parentNode.removeChild(dialog);
  });
  
  // dialog body
  var dialog_body = createElement("div", {
    "class": "dialog_body"
  }, text);
  dialog.appendChild(dialog_body);
  
  // return
  return dialog;
}

/**
 * remove dialog  移除对话框
 * @param dialog  String|Object(HTMLElement)  dialog ID or dialog node  对话框节点或ID
 * 依赖 getElement()
 */
function removeDialog(dialog)
{
  if ( !dialog || !dialog.parentNode )
  {
    return;
  }
  
  // get dialog node
  if ( typeof(dialog) !== "object" )
  {
    dialog = getElement(dialog);
  }
  
  // remove
  dialog.parentNode.removeChild(dialog);
}

/**
 * @name show panel  显示面板
 * 依赖 jQuery() 和 getElement()
 * @param id  panel ID  面板ID
 * @return bool  true if success or false if fail.  成功返回true，否则返回false。
 */
function showPanel(id)
{
  var panel = getElement(id);
  if (!panel)
  {
    return false;
  }
  jQuery(panel).addClass("panel-open");
  return true;
}

/**
 * @name hidden panel  隐藏面板
 * 依赖 jQuery() 和 getElement()
 * @param {Object} id  element id
 */
function hidePanel(id)
{
  var panel = getElement(id);
  if (!panel)
  {
    return false;
  }
  jQuery(panel).removeClass("panel-open");
  return true;
}

/**
 * @name create mask layer  创建遮罩层
 * 依赖 getElement()
 * @param {Object} id  element id
 * @return Object(HTMLElement)
 */
function createMaskLayer(id)
{
  var layer = createElement("div", {
    "id": id,
    "class": "maskLayer"
  });
  return layer;
}

/**
 * @name show mask layer  显示遮罩层
 * 依赖 getElement() 和 createMaskLayer()
 * @param {Object} id  element id
 * @return Object(HTMLElement)
 */
function showMaskLayer(id)
{
  var layer = getElement(id);
  
  // 如果不存在则创建一个
  if (!layer)
  {
    layer = createMaskLayer(id);
    document.body.appendChild(layer);
  }
  layer.style.display = "block";
  
  // return
  return layer;
}

/**
 * @name hidden mask layer  隐藏遮罩层
 * 依赖 getElement()
 * @param {Object} id  element id
 * @return Object(HTMLElement)
 */
function hideMaskLayer(id)
{
  var layer = getElement(id);
  if (layer)
  {
    layer.style.display = "none";
  }
}

/**
 * create a progress dialog  创建一个进度条对话框
 * @param id  String  dialog ID string  对话框ID
 * @param percent  Number  progress percent  进度条百分比
 */
function createProgressDialog(id, percent)
{
	var title = arguments[2] ? arguments[2] : '上传中...';
	
  // create a wrapper
  var dialog = createElement("div", {
    "id": id,
    "class": "dialog dialog_progress"
  });
  
  // create a wrapper to contain progress
  var box_progress = createElement("div", {
    "class": "box_progress"
  });
  
  dialog.appendChild(box_progress);
  
  var Eletitle = createElement("p",{});
  Eletitle.innerHTML = title;
  box_progress.appendChild(Eletitle);
  
  // create progress
  var progress = createElement("div", {
    "class": "progress"
  });
  box_progress.appendChild(progress);
  
  // create progress percent display
  var progress_bar = createElement("div", {
    "class": "progress-bar progress-bar-striped active"
  });
  progress.appendChild(progress_bar);
  
  // set width
  progress_bar.style.width = percent + "%";
  progress_bar.innerHTML = percent + "%";
  
  // display
  document.body.appendChild(dialog);
}

/**
 * update progress dialog  更新一个进度条对话框
 * @param id  String  dialog ID string  对话框ID
 * @param percent  Number  progress percent  进度条百分比
 * @returns Bool  true if success or false if fail
 */
function updateProgressDialog(id, percent)
{
  // get dialog node
  var dialog = getElement(id);
  if (!dialog)
  {
    return false;
  }
  
  var progress_bar = null;
  if ( document.getElementsByClassName )
  {
    var list_progress_bar = dialog.getElementsByClassName("progress-bar");
    if (list_progress_bar.length)
    {
      progress_bar = list_progress_bar[0];
      progress_bar.style.width = percent + "%";
      progress_bar.innerHTML = percent + "%";
    }
  }
  else
  {
    jQuery(dialog).find(".progress-bar").css("width", percent + "%").html(percent + "%");
  }
  
  return true;
}

/**
 * remove progress dialog  移除进度条对话框
 * @param id String  dialog ID string  对话框ID
 * @returns Bool  true if success or false if fail
 */
function removeProgressDialog(id)
{
  // get dialog node
  var dialog = getElement(id);
  if (!dialog)
  {
    return false;
  }
  
  // remove
  dialog.parentNode.removeChild(dialog);
  
  // return
  return true;
}

/**
 * @name share to weibo
 * 依赖 addUrlParam()
 * @param url  String
 * @param title  String
 * param 3  String  appKey
 * param 4  String  if search picture ('true' or 'false')
 */
function shareToWeibo(url, title)
{
  // get arguments
  var appkey    = arguments[3] ? arguments[3] : "";
  var searchPic = arguments[4] ? arguments[4] : "";
  
  // check url param
  if ( url.substring(0, 4) !== "http" )
  {
    url = location.protocol + "//" + location.hostname + url;
  }
  
  // set request url
  var url_weibo_share = "http://service.weibo.com/share/share.php";
  url = addUrlParam(url_weibo_share, "url", url);
  url = addUrlParam(url, "title", title);
  if (appkey)
  {
    url = addUrlParam(url_weibo_share, "appkey", appkey);
  }
  if (searchPic)
  {
    url = addUrlParam(url_weibo_share, "searchPic", searchPic);
  }
  
  // open a new url
  window.open(url);
  
}

/**
 * show alert window
 * @param Opject  options
 *   content  String  警告框内容
 *   auto_close  Number  自动关闭的时间
 */
function showAlertWindow(options)
{
    var content = typeof(options['content']) !== 'undefined' ? options['content'] : '';
    var auto_close = typeof(options['auto_close']) !== 'undefined' ? options['auto_close'] : 0;

    // 窗口
    var alert_window = $('<div class="alert_window"></div>');

    // 窗口内容
    var alert_window_content_box = $('<div class="content_box"></div>').html(content);

    alert_window.append(alert_window_content_box);

    $(document.body).append(alert_window);

    // 调整定位
    var offset_height = alert_window.height();
    alert_window.css('margin-top', '-' + (offset_height / 2) + 'px');

    // 自动关闭
    if (auto_close)
    {
        window.setTimeout(function(){

            // 窗口淡出
            var target = alert_window;
            $(target).fadeOut('normal', function(){
                $(this).remove();
            });

        }, parseInt(auto_close));
    }
}

/**
 * 手机版网页的应用功能
 */
var app = new Object();
(function(app){
	
	// 点赞的锁定
	app.lock_praise = false;
	
	// 点赞
	app.praise = (function(){
		
		if (this.lock_praise)
	    {
	        return;
	    }
	    
	    this.lock_praise = true;
	    jQuery.ajax('forum.php', {
	      'data' : {
	        'mod' : 'ajax', 'action'  : 'saygood', 'type': 'tid',
	        'tid' : threadId
	      },
	      'dataType' : 'json',
	      'success' : function(result){
	        if (result.state === 'success')
	        {
	            jQuery(".buttonLike").addClass("active");
	            var number = parseInt(jQuery(".buttonLike .number").html());
	            jQuery(".buttonLike .number").html(++number);
	        }
	        else
	        {
	            //alert(result.message);
	        }
	        
	        // unlock praise  解锁点赞
	        app.lock_praise = false;

            // 检查积分奖励
            checkCreditPrompt();
	      }
	    });
	});	// end praise()
	
	app.product_feel_start = 0;
	app.product_feel_end = 0;
	app.product_feel_pagesize = 3;
	app.product_feel_load_lock = false;
	
	app.loadMoreProductFeel = (function(spids, type, obj){
		
		if ( this.product_feel_load_lock ) { return false; }
		
		this.product_feel_load_lock = true;
		
		var getData = {
			"mod": "ajax",
			"action": "getThreadByspid",
			"tid":tid,
			"spids": spids,
			'pagesize': 1
		};
		if(type == 1){
			// 初始化开始两条
			getData.start = this.product_feel_end;
			getData.pagesize = 1;
			this.product_feel_end += 1;
		}else{
			// 每次获取N条
			getData.start = this.product_feel_end;
			getData.pagesize = this.product_feel_pagesize;
			this.product_feel_end += this.product_feel_pagesize;
		}
		jQuery.ajax({
			url:"forum.php",
			data: getData,
			dataType: "json",
			success:function(data){
				
				if(data.result == 1){
					
					var tl = data.data;
					
					
					
					for(var i = 0; i < tl.length; i++){
						var item = app.formatTestThread(tl[i]);
						jQuery(obj).append(item);
					}
					
					// 没有更多数据则隐藏加载更多的按钮
					if ( !tl.length ) {
						jQuery('#buttonLoadMoreProductFeel').fadeOut();
					}
					else {
						// 显示商品评测的容器
						jQuery('.product_feel_list').show();
						
						// 解锁
						app.product_feel_load_lock = false;
						
						// 显示加载更多的按钮
						jQuery('#buttonLoadMoreProductFeel').removeClass('hide');
					}
					
				}
				
			}
		});
	});
	
	app.formatTestThread = (function(t){
		var item = jQuery('<li class="product_feel_item">\
  	  	  	  <div class="item_header">\
  	  	  	  	<div class="name">商品测评：</div>\
  	  	  	  	<div class="date">2015-02-05</div>\
  	  	  	  </div>\
  	  	  	  <div class="item_body">\
  	  	  	  	<p>' + t.summary + '</p>\
  	  	  	  </div>\
  	  	  	  <div class="item_info">\
  	  	  		<span class="item like">\
  	  	  			<img src="/static/image/vivo/mobile/like_ico.png" alt="" />\
  	  	  			' + t.saygood + '\
  	  	  		</span>\
  	  	  		<span class="item reply">\
  	  	  			<img src="/static/image/vivo/mobile/comment_ico.gif" alt="" />\
  	  	  			' + t.replies + '\
  	  	  		</span>\
  	  	  	  </div>\
  	  	  	  <div class="item_images">\
  	  	  	    <ul>\
  	  	  	    </ul>\
  	  	  	  </div>\
  	  	</li>');
  	  	
  	  	var item_images_list = item.find('.item_images ul');
  	  	
  	  	var image_item = null;
  	  	var image_link = '';
  	  	for(var i = 0; i < t.media_list.length; i++){
  	  		
  	  		image_item = jQuery('<li class="thread-image-item"></li>');
  	  		image_link = jQuery('<a><img src="' + t.media_list[i]['media_url'] + '?imageView2/1/w/200"></a>');
  	  		image_link.attr('href', t.media_list[i]['media_url']).data('gallery', 'gallery-thread-'+t.tid);
  	  		//setViewImageAction(image_link[0]);
  	  		image_item.append(image_link);
			item_images_list.append(image_item);
  	  	}
  	  	
  	  	//item_images_list.find('.item_images a').addClass('touchTouch-image').touchTouch();
  	  	//jQuery('.replylist .item_images a').touchTouch();
  	  	
  	  	return item[0];
  	  	
	});
	
})(app);

