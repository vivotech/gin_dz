/**
 * scripts for dealer  经销商后台脚本
 */

$(document).ready(function(){
	
	// 退出登录按钮
	$("#linkLogout").click(function(){
		$('<div id="logoutConfirmDialog" class="modal fade">\
			<div class="modal-dialog">\
		        <div class="modal-content">\
		          <div class="modal-header">\
		            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>\
		            <h4 class="modal-title">退出登录</h4>\
		          </div>\
		          <div class="modal-body">\
		            <p>你确定要退出吗？</p>\
		          </div>\
		          <div class="modal-footer">\
		            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">取消</button>\
		            <button type="button" class="btn btn-primary">确定</button>\
		          </div>\
		        </div><!-- /.modal-content -->\
		    </div>\
		</div>').appendTo(document.body).modal()
		.find('.btn-primary').click(function(){
			$("#logoutConfirmDialog").modal('hide');
			$.ajax({
				url:"/app.php?mod=member&act=logout",
				'dataType': 'json',
				success: function(data){
					if (data.result == 1) {
						location.href = '/dealer.php?mod=signin';
					}
					else {
						alert( data.msg );
					}
				}
			});
		});
		
	});
	
	// 初始化 checkbox
	$('input[type=checkbox]').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
	
	// iCheck 全选/取消全选
	$('form thead th:eq(0) input[type=checkbox]').on('ifChecked', function(event){
		var table = $(this).parents("form table");
		var checkboxs = table.find("tbody td input[type=checkbox]");
		checkboxs.iCheck('check');
	}).on('ifUnchecked', function(event){
		var table = $(this).parents("form table");
		var checkboxs = table.find("tbody td input[type=checkbox]");
		checkboxs.iCheck('uncheck');
	});
	
	// 有表单表格时，Ctrl+A 为全选表格项
	if ( $(".container-fluid .content-wrapper form table").length )
	{
		$(document).bind('keydown', "Ctrl+A", function() {
			$('form table').find('thead th input[type=checkbox]').iCheck('toggle');
			return false;
		});
	}
	
	// datepicker  日期选择
	var datepicker_options = {
		"monthNames": ["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],
		"dateFormat": "yy-mm-dd"
	};
	$(".datepicker").datepicker(datepicker_options);
	
});
