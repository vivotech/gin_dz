wx.ready(function(){
	
	//分享到朋友圈
	wx.onMenuShareTimeline({
	    title: window.wxshareTitle, // 分享标题
	    link: window.wxshareLink, // 分享链接
	    imgUrl: window.wxshareImg, // 分享图标
	    success: function () { 
	        // 用户确认分享后执行的回调函数
	    },
	    cancel: function () { 
	        // 用户取消分享后执行的回调函数
	    }
	});
	
	//分享给朋友
	wx.onMenuShareAppMessage({
	    title: window.wxshareTitle, // 分享标题
	    desc: window.wxshareDesc, // 分享描述
	    link: window.wxshareLink, // 分享链接
	    imgUrl: window.wxshareImg, // 分享图标
	    type: '', // 分享类型,music、video或link，不填默认为link
	    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
	    success: function () { 
	        // 用户确认分享后执行的回调函数
	    },
	    cancel: function () { 
	        // 用户取消分享后执行的回调函数
	    }
	});
	
	//分享到QQ
	wx.onMenuShareQQ({
	    title: window.wxshareTitle, // 分享标题
	    desc: window.wxshareDesc, // 分享描述
	    link: window.wxshareLink, // 分享链接
	    imgUrl: window.wxshareImg, // 分享图标
	    success: function () { 
	       // 用户确认分享后执行的回调函数
	    },
	    cancel: function () { 
	       // 用户取消分享后执行的回调函数
	    }
	});
	
	//分享到腾讯微博
	wx.onMenuShareWeibo({
	    title: window.wxshareTitle, // 分享标题
	    desc: window.wxshareDesc, // 分享描述
	    link: window.wxshareLink, // 分享链接
	    imgUrl: window.wxshareImg, // 分享图标
	    success: function () { 
	       // 用户确认分享后执行的回调函数
	    },
	    cancel: function () { 
	        // 用户取消分享后执行的回调函数
	    }
	});
	
});