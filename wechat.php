<?php

/**
 * 微信接口
 */

define('APPTYPEID', 0);
define('CURSCRIPT', 'wechat');

//error_reporting(E_ERROR);
require './source/class/class_minicore.php';

$modarray = array(
				'msg',			//消息类接口
//				'notice',		//支付回调提醒
				'alarm',		//告警提醒URL
			);

$mod = !in_array($_REQUEST['mod'], $modarray) ? 'msg' : $_REQUEST['mod'];
$act = $_REQUEST['act'];

require_once libfile('function/wechat');

require DISCUZ_ROOT.'./source/module/wechat/wechat_'.$mod.'.php';


?>

