<?php
/**
 * 支付宝直接返回处理（非异步）
 *
 * File: alipay_return.php
 * User: goodspb
 * Date: 2015/6/29-11:23
 */

define('APPTYPEID', 0);
define('CURSCRIPT', 'alipay');

//error_reporting(E_ERROR);
require './source/class/class_minicore.php';

require_once libfile('function/payment');

$alipayNotify = new alipay_notify();
$verify_result = $alipayNotify->verifyReturn();


if($verify_result) {//验证成功
    //商户订单号
    $out_trade_no = $_GET['out_trade_no'];

    //交易状态
    $trade_status = $_GET['trade_status'];

    if($_GET['trade_status'] == 'TRADE_FINISHED' || $_GET['trade_status'] == 'TRADE_SUCCESS') {
        //判断该笔订单是否在商户网站中已经做过处理
        //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
        //如果有做过处理，不执行商户的业务程序

        $order = getOrderByOutTradeNo($out_trade_no);
        //未支付
        if(!$order && $order['paystate']==0){
            setAlipayState($order['oid'],$_GET);
        }
    }

}
//else {
//    //验证失败
//    //如要调试，请看alipay_notify.php页面的verifyReturn函数
//    echo "参数错误,请联系管理员！";
//}

header("location:".U('forum/order/state',array('oid'=>$order['oid'])));
exit();